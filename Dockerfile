# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM debian:testing

# dependent libraries/tools
RUN apt-get update \
    && \
    apt-get --yes install \
        cmake \
        doxygen \
        g++ \
        graphviz \
        libasio-dev \
        libboost-filesystem-dev \
        libboost-system-dev \
        libboost-test-dev \
        libgnutls28-dev \
        libgtest-dev \
        libyaml-cpp-dev \
        ninja-build \
    && \
    rm -rf /var/lib/apt/lists/*

# dependent development tools
RUN apt-get update \
    && \
    apt-get --yes install \
        doxygen-gui \
        git \
        git-gui \
        gitk \
        qmake6 \
        qtcreator \
    && \
    rm -rf /var/lib/apt/lists/*

RUN useradd \
        --comment "Local User" \
        --home-dir /home/local \
        --password local \
        --shell /bin/bash \
        --user-group \
        local

USER local
