# KNX

This KNX library has the following features:
- Full KNX spec implementation in C++20.
- Full traceability to spec chapters. Folder structure follows spec structure.
- Full implementation of all specified tests.
- Full static code check.

Although the KNX spec should be device-agnostic, it seems to serve devices more than tools.
Nevertheless, OpenETS builds on top of it.

## Compilation instructions

### Docker environment

There is a `Dockerfile` containing all source code dependencies and used development tools used.
The environment can be started with the `docker-run.sh` script.

For those who prefer to manually install dependencies, the chapter "Standard dependencies" contains the instructions.

### Standard dependencies

You need the following dependencies:

- Asio
- Threads
- Google Test (for testing)

You can install these dependencies under Debian/Ubuntu using
```
apt install libc6-dev libasio-dev
apt install libgtest-dev
```

### Build

Compilation is done as usual with cmake:
```
mkdir build
cd build
cmake ..
cmake --build .
```

## Developer information

Directory Structure follows KNX spec chapter structure.

Traceability to spec chapters is provided via `doxygen`.
`doxygen.dox` resemble the spec chapters.
Declarations are referenced to chapters by using i.e. `@ingroup KNX_03_03_03_02_01`, which means
KNX spec 3/3/3 chapter 2.1.

## ToDos

- all @todo's in the source code
- Network and Device Management, both not ideal as potential of asio is not fully used
