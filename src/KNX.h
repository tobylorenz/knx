// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 3 System Specifications */
#include <KNX/03_System_Specifications.h>

/* 9 Basic and System Components/Devices */
#include <KNX/09_Basic_and_System_Components.h>
