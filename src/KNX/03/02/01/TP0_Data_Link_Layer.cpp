// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/01/TP0_Data_Link_Layer.h>

namespace KNX {

TP0_Data_Link_Layer::TP0_Data_Link_Layer(TP0_Physical_Layer & tp0_physical_layer, asio::io_context & io_context) :
    Data_Link_Layer(),
    io_context(io_context)
{
    Ph_Data_req = std::bind(&TP0_Physical_Layer::Ph_Data_req, &tp0_physical_layer, std::placeholders::_1);
    tp0_physical_layer.Ph_Data_con = std::bind(&TP0_Data_Link_Layer::Ph_Data_con, this, std::placeholders::_1);
    tp0_physical_layer.Ph_Data_ind = std::bind(&TP0_Data_Link_Layer::Ph_Data_ind, this, std::placeholders::_1);

    Ph_Reset_req = std::bind(&TP0_Physical_Layer::Ph_Reset_req, &tp0_physical_layer);
    tp0_physical_layer.Ph_Reset_con = std::bind(&TP0_Data_Link_Layer::Ph_Reset_con, this);
}

TP0_Data_Link_Layer::~TP0_Data_Link_Layer()
{
}

void TP0_Data_Link_Layer::L_Data_req(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)
{
    assert(group_address_table);

    TP0_L_Data_Frame tp0_frame;
    tp0_frame.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
    tp0_frame.control.repeat = Repeat::not_repeated;
    tp0_frame.control.priority = priority;
    tp0_frame.control.acknowledge = ack_request;
    tp0_frame.extended_control.address_type = address_type;
    tp0_frame.extended_control.extended_frame_format = frame_format.extended_frame_format;
    tp0_frame.source_address = source_address;
    tp0_frame.destination_address = destination_address;
    tp0_frame.lsdu = lsdu;

    assert(Ph_Data_req);
    Ph_Data_req(tp0_frame.toData());
}

void TP0_Data_Link_Layer::L_SystemBroadcast_req(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority)
{
    assert(group_address_table);

    TP0_L_Data_Frame tp0_frame;
    tp0_frame.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
    tp0_frame.control.repeat = Repeat::not_repeated;
    tp0_frame.control.system_broadcast = System_Broadcast::System_Broadcast;
    tp0_frame.control.priority = priority;
    tp0_frame.control.acknowledge = ack_request;
    tp0_frame.extended_control.address_type = Address_Type::Group;
    tp0_frame.extended_control.extended_frame_format = frame_format.extended_frame_format;
    tp0_frame.source_address = group_address_table->individual_address;
    tp0_frame.destination_address = destination_address;
    tp0_frame.lsdu = lsdu;

    assert(Ph_Data_req);
    Ph_Data_req(tp0_frame.toData());
}

void TP0_Data_Link_Layer::L_Poll_Data_req(const Group_Address /*destination_address*/, const uint8_t /*no_of_expected_poll_data*/)
{
    assert(false);
}

void TP0_Data_Link_Layer::L_Plain_Data_req(const uint32_t /*time_stamp*/, const std::vector<uint8_t> data)
{
    assert(Ph_Data_req);
    Ph_Data_req(data);
}

void TP0_Data_Link_Layer::Ph_Data_con(std::vector<uint8_t> data)
{
    TP0_L_Data_Frame frame;
    frame.fromData(std::cbegin(data), std::cend(data));
    Frame_Format frame_format;
    frame_format.frame_type_parameter = to_frame_type_parameter(frame.control.frame_type);
    frame_format.extended_frame_format = frame.extended_control.extended_frame_format;

    switch(frame.control.system_broadcast) {
    case System_Broadcast::Broadcast:
        if (L_Data_con) {
            L_Data_con(frame.extended_control.address_type, frame.destination_address, frame_format, frame.control.priority, frame.source_address, frame.lsdu, Status::ok);
        }
        break;
    case System_Broadcast::System_Broadcast:
        if (L_SystemBroadcast_con) {
            L_SystemBroadcast_con(Group_Address(frame.destination_address), frame_format, frame.control.priority, frame.source_address, frame.lsdu, Status::ok);
        }
        break;
    }
}

void TP0_Data_Link_Layer::Ph_Data_ind(std::vector<uint8_t> data)
{
    if (mode == Mode::Normal) {
        TP0_L_Data_Frame frame;
        frame.fromData(std::cbegin(data), std::cend(data));
        Frame_Format frame_format;
        frame_format.frame_type_parameter = to_frame_type_parameter(frame.control.frame_type);
        frame_format.extended_frame_format = frame.extended_control.extended_frame_format;

        switch(frame.control.system_broadcast) {
        case System_Broadcast::Broadcast:
            if (L_Data_ind) {
                L_Data_ind(frame.control.acknowledge, frame.extended_control.address_type, frame.destination_address, frame_format, frame.lsdu, frame.control.priority, frame.source_address);
            }
            break;
        case System_Broadcast::System_Broadcast:
            if (L_SystemBroadcast_ind) {
                L_SystemBroadcast_ind(frame.control.acknowledge, Group_Address(frame.destination_address), frame_format, frame.lsdu, frame.control.priority, frame.source_address);
            }
            break;
        }
    } else { // Mode::Busmonitor
        if (L_Busmon_ind) {
            L_Busmon_ind(Status::ok, 0, data);
        }
    }
}

void TP0_Data_Link_Layer::Ph_Reset_con()
{
}

/** is frame addressed to data link layer */
bool TP0_Data_Link_Layer::is_addressed(const TP0_L_Data_Frame & frame) const
{
    assert(group_address_table);

    bool addressed{false};
    switch (frame.extended_control.address_type) {
    case Address_Type::Individual:
        addressed =
            (group_address_table->individual_address == Individual_Address(frame.destination_address));
        break;
    case Address_Type::Group:
        addressed =
            (group_address_table->group_addresses.count(Group_Address(frame.destination_address)) > 0) ||
            Group_Address(frame.destination_address).is_broadcast();
        break;
    }
    return addressed;
}

}
