// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <asio/io_context.hpp>

#include <KNX/03/02/01/TP0_Frame.h>
#include <KNX/03/02/01/TP0_L_Data_Frame.h>
#include <KNX/03/02/01/TP0_Physical_Layer.h>
#include <KNX/03/03/02/Data_Link_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * TP0 Data Link Layer
 */
class KNX_EXPORT TP0_Data_Link_Layer :
    public Data_Link_Layer
{
public:
    explicit TP0_Data_Link_Layer(TP0_Physical_Layer & tp0_physical_layer, asio::io_context & io_context);
    virtual ~TP0_Data_Link_Layer();

    void L_Data_req(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address) override;
    void L_SystemBroadcast_req(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority) override;
    void L_Poll_Data_req(const Group_Address destination_address, const uint8_t no_of_expected_poll_data) override;
    void L_Plain_Data_req(const uint32_t time_stamp, const std::vector<uint8_t> data) override;

protected:
    asio::io_context & io_context;

    /* Ph_Data */
    std::function<void(const std::vector<uint8_t> data)> Ph_Data_req;
    virtual void Ph_Data_con(std::vector<uint8_t>);
    virtual void Ph_Data_ind(std::vector<uint8_t>);

    /* Ph_Reset */
    std::function<void()> Ph_Reset_req;
    virtual void Ph_Reset_con();

    /** is frame addressed to data link layer */
    bool is_addressed(const TP0_L_Data_Frame & frame) const;
};

}
