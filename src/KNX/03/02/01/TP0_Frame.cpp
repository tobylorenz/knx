// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/01/TP0_Frame.h>

#include <KNX/03/02/01/TP0_L_Data_Frame.h>

namespace KNX {

TP0_Frame::~TP0_Frame()
{
}

std::shared_ptr<TP0_Frame> make_TP0_Frame(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
     // everything else is unknown and not needed for TSSA test
    std::shared_ptr<TP0_L_Data_Frame> frame = std::make_shared<TP0_L_Data_Frame>();
    frame->fromData(first, last);
    return frame;
}

}
