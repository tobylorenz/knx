// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/03/02/LPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * TP0 Frame
 */
class KNX_EXPORT TP0_Frame
{
public:
    virtual ~TP0_Frame();
};

KNX_EXPORT std::shared_ptr<TP0_Frame> make_TP0_Frame(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
