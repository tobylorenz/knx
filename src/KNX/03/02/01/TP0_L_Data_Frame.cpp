// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/01/TP0_L_Data_Frame.h>

#include <cassert>

#include <KNX/03/03/03/NPDU.h>

namespace KNX {

TP0_L_Data_Frame::TP0_L_Data_Frame() :
    TP0_Frame(),
    L_Data_PDU()
{
}

TP0_L_Data_Frame::~TP0_L_Data_Frame()
{
}

void TP0_L_Data_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 10);

    // preamble
    preamble = *first++;
    assert(preamble == 0xCB);

    // control
    assert(*first == 0xEF); // everything else is unknown and not needed for TSSA test
    ++first;
    control.system_broadcast = System_Broadcast::System_Broadcast;

    // source
    source_address.fromData(first, first + 2);
    first += 2;

    // destination
    destination_address.fromData(first, first + 2);
    first += 2;

    // at/npci/lg
    const uint8_t at_npci_lg = *first++;
    extended_control.address_type = static_cast<Address_Type>(at_npci_lg >> 7);
    // hop_count passed to NPDU
    length = at_npci_lg & 0x0f;

    // npdu
    lsdu = make_NPDU_Individual(control, destination_address, at_npci_lg & 0xf0, first, first + 1 + length);
    first += length + 1;

    // postamble
    postamble = (*first++ << 8) | *first++;
    assert(postamble == 0x0A65); // everything else is unknown and not needed for TSSA test

    assert(first == last);
}

std::vector<uint8_t> TP0_L_Data_Frame::toData() const
{
    assert(lsdu);

    std::vector<uint8_t> data;

    const Hop_Count hop_count = lsdu->hop_count;
    const std::vector<uint8_t> lsdu_data = lsdu->toData();

    // preamble
    data.push_back(preamble);

    // control
    data.push_back(0xEF); // everything else is unknown and not needed for TSSA test

    // source address
    data.push_back(source_address >> 8);
    data.push_back(source_address & 0xff);

    // destination address
    data.push_back(destination_address >> 8);
    data.push_back(destination_address & 0xff);

    // at/npci/lg
    data.push_back(
        (static_cast<uint8_t>(extended_control.address_type) << 7) |
        (hop_count << 4) |
        length_calculated());

    // npdu
    data.insert(std::cend(data), std::cbegin(lsdu_data), std::cend(lsdu_data));

    // postamble
    data.push_back(postamble >> 8);
    data.push_back(postamble & 0xff);

    return data;
}

}
