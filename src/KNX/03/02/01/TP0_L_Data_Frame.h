// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/02/01/TP0_Frame.h>
#include <KNX/03/02/01/TP0_Preamble.h>
#include <KNX/03/02/01/TP0_Postamble.h>
#include <KNX/03/03/02/L_Data/L_Data_PDU.h>

namespace KNX {

/**
 * TP0 L_Data Frame
 */
class KNX_EXPORT TP0_L_Data_Frame :
    public TP0_Frame,
    public L_Data_PDU
{
public:
    TP0_L_Data_Frame();
    virtual ~TP0_L_Data_Frame();

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /** preamble */
    TP0_Preamble preamble{0x0000};

    /** postamble */
    TP0_Postamble postamble{0x0A66};
};

}
