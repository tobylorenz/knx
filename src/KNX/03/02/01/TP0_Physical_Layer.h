// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>

#include <asio/io_context.hpp>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

class KNX_EXPORT TP0_Physical_Layer
{
public:
    explicit TP0_Physical_Layer(asio::io_context & io_context);
    virtual ~TP0_Physical_Layer() = default;

    /* Ph_Data */
    virtual void Ph_Data_req(const std::vector<uint8_t> data) = 0;
    std::function<void(std::vector<uint8_t>)> Ph_Data_con;
    std::function<void(std::vector<uint8_t>)> Ph_Data_ind;

    /* Ph_Reset */
    virtual void Ph_Reset_req() = 0;
    std::function<void()> Ph_Reset_con;

protected:
    asio::io_context & io_context;
};

}
