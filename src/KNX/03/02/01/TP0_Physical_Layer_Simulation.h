// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <set>

#include <KNX/03/02/01/TP0_Physical_Layer.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/* forward declarations */
class TP0_Bus_Simulation;
class TP0_Physical_Layer_Simulation;

/** TP0 Bus Simulation */
class KNX_EXPORT TP0_Bus_Simulation
{
public:
    explicit TP0_Bus_Simulation(asio::io_context & io_context);
    virtual ~TP0_Bus_Simulation();

    /** connected devices */
    std::set<TP0_Physical_Layer_Simulation *> devices {};

protected:
    asio::io_context & io_context;
};

/** TP0 Physical Layer Simulation */
class KNX_EXPORT TP0_Physical_Layer_Simulation :
    public TP0_Physical_Layer
{
public:
    explicit TP0_Physical_Layer_Simulation(asio::io_context & io_context);
    virtual ~TP0_Physical_Layer_Simulation();

    /** connect physical layer to bus */
    void connect(TP0_Bus_Simulation & bus);

    /** disconnect physical layer from bus */
    void disconnect();

    void Ph_Data_req(const std::vector<uint8_t> data) override;
    void Ph_Reset_req() override;

protected:
    /** bus connection */
    TP0_Bus_Simulation * m_bus{};
};

}
