// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/02/TP1_Acknowledgement_Frame.h>

#include <cassert>

namespace KNX {

void TP1_Acknowledgement_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    /* octet 0 */
    acknowledge = static_cast<Acknowledge>(*first++);

    assert(first == last);
}

std::vector<uint8_t> TP1_Acknowledgement_Frame::toData() const
{
    std::vector<uint8_t> data;

    /* octet 0 */
    data.push_back(static_cast<uint8_t>(acknowledge));

    return data;
}

}
