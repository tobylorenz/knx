// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/02/02/TP1_Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * TP1 Acknowledgement Frame
 *
 * @ingroup KNX_03_02_02_02_02_07
 */
class KNX_EXPORT TP1_Acknowledgement_Frame :
    public TP1_Frame
{
public:
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /**
     * Acknowledge
     *
     * This is sent by all devices at the same time.
     * Zero bits are dominant, hence NAK and BUSY overwrite ACK.
     */
    enum class Acknowledge : uint8_t {
        /** acknowledge */
        ACK = 0xcc,

        /** not acknowledge */
        NAK = 0x0c,

        /** busy */
        BUSY = 0xc0,

        /** not acknowledge and busy */
        NAK_and_BUSY = 0x00
    };

    /** @copydoc Acknowledge */
    Acknowledge acknowledge{Acknowledge::NAK_and_BUSY};
};

}
