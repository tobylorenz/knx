// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/02/TP1_Data_Link_Layer.h>

#include <cassert>

#include <KNX/03/02/02/TP1_Acknowledgement_Frame.h>
#include <KNX/03/02/02/TP1_Frame.h>
#include <KNX/03/02/02/TP1_L_Data_Frame.h>
#include <KNX/03/02/02/TP1_L_Poll_Data_Frame.h>

namespace KNX {

TP1_Data_Link_Layer::TP1_Data_Link_Layer(TP1_Physical_Layer & tp1_physical_layer, asio::io_context & io_context) :
    Data_Link_Layer(),
    io_context(io_context)
{
    Ph_Data_req = std::bind(&TP1_Physical_Layer::Ph_Data_req, &tp1_physical_layer, std::placeholders::_1, std::placeholders::_2);
    tp1_physical_layer.Ph_Data_con = std::bind(&TP1_Data_Link_Layer::Ph_Data_con, this, std::placeholders::_1);
    tp1_physical_layer.Ph_Data_ind = std::bind(&TP1_Data_Link_Layer::Ph_Data_ind, this, std::placeholders::_1, std::placeholders::_2);

    Ph_Reset_req = std::bind(&TP1_Physical_Layer::Ph_Reset_req, &tp1_physical_layer);
    tp1_physical_layer.Ph_Reset_con = std::bind(&TP1_Data_Link_Layer::Ph_Reset_con, this, std::placeholders::_1);
}

TP1_Data_Link_Layer::~TP1_Data_Link_Layer()
{
}

void TP1_Data_Link_Layer::L_Data_req(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)
{
    assert(group_address_table);

    E_L_Data_req evt{ack_request, address_type, destination_address, frame_format, lsdu, priority, source_address ? source_address : group_address_table->individual_address};

    io_context.post([this, evt]() {
        process_event(evt);
    });
}

void TP1_Data_Link_Layer::L_SystemBroadcast_req(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority)
{
    E_L_SystemBroadcast_req evt{ack_request, destination_address, frame_format, lsdu, priority};

    io_context.post([this, evt]() {
        process_event(evt);
    });
}

void TP1_Data_Link_Layer::L_Poll_Data_req(const Group_Address destination_address, const uint8_t no_of_expected_poll_data)
{
    E_L_Poll_Data_req evt{destination_address, no_of_expected_poll_data};

    io_context.post([this, evt]() {
        process_event(evt);
    });
}

void TP1_Data_Link_Layer::L_Plain_Data_req(const uint32_t time, const std::vector<uint8_t> data)
{
    E_L_Plain_Data_req evt{time, data};

    io_context.post([this, evt]() {
        process_event(evt);
    });
}

void TP1_Data_Link_Layer::Ph_Data_con(const P_Status p_status)
{
    switch (p_status) {
    case P_Status::ok:
        process_event(E_Ph_Data_con_ok{current_Ph_Data_req_class});
        break;
    case P_Status::bus_not_free:
        process_event(E_Ph_Data_con_bus_not_free{current_Ph_Data_req_class});
        break;
    case P_Status::collision_detected:
        process_event(E_Ph_Data_con_collision_detected{current_Ph_Data_req_class});
        break;
    case P_Status::transceiver_fault:
        process_event(E_Ph_Data_con_transceiver_fault{current_Ph_Data_req_class});
        break;
    }
}

void TP1_Data_Link_Layer::Ph_Reset_con(const P_Status p_status)
{
    switch (p_status) {
    case P_Status::ok:
        process_event(E_Ph_Reset_con_ok{});
        break;
    case P_Status::bus_not_free:
        assert(false);
        break;
    case P_Status::collision_detected:
        assert(false);
        break;
    case P_Status::transceiver_fault:
        process_event(E_Ph_Reset_con_transceiver_fault{});
        break;
    }
}

void TP1_Data_Link_Layer::Ph_Data_ind(const P_Class p_class, const uint8_t p_data)
{
    switch (p_class) {
    case P_Class::start_of_frame:
        process_event(E_Ph_Data_ind_start_of_frame{p_data});
        break;
    case P_Class::inner_frame_char:
        process_event(E_Ph_Data_ind_inner_frame_char{p_data});
        break;
    case P_Class::ack_char:
        process_event(E_Ph_Data_ind_ack_char{p_data});
        break;
    case P_Class::poll_data_char:
        process_event(E_Ph_Data_ind_poll_data_char{p_data});
        break;
    case P_Class::fill_char:
        process_event(E_Ph_Data_ind_fill_char{p_data});
        break;
    case P_Class::parity_error:
        process_event(E_Ph_Data_ind_parity_error{p_data});
        break;
    case P_Class::framing_error:
        process_event(E_Ph_Data_ind_framing_error{p_data});
        break;
    case P_Class::bit_error:
        process_event(E_Ph_Data_ind_bit_error{p_data});
        break;
    }
}

bool TP1_Data_Link_Layer::is_addressed(const TP1_L_Data_Frame & frame) const
{
    assert(group_address_table);

    bool addressed{false};
    switch (frame.extended_control.address_type) {
    case Address_Type::Individual:
        addressed =
            (group_address_table->individual_address == Individual_Address(frame.destination_address));
        break;
    case Address_Type::Group:
        addressed =
            (group_address_table->group_addresses.count(Group_Address(frame.destination_address)) > 0) ||
            Group_Address(frame.destination_address).is_broadcast();
        break;
    }
    return addressed;
}

/* Actions */

void TP1_Data_Link_Layer::A_L_Data_con(const Status l_status)
{
    /* frame was already confirmed, hence no data available anymore. */
    if (tx.buffer.empty()) {
        return;
    }

    /* confirm frame */
    if (L_Data_con) {
        TP1_L_Data_Frame frame;
        frame.fromData(std::cbegin(tx.buffer), std::cend(tx.buffer));
        Frame_Format frame_format;
        frame_format.frame_type_parameter = to_frame_type_parameter(frame.control.frame_type);
        frame_format.extended_frame_format = frame.extended_control.extended_frame_format;
        L_Data_con(frame.extended_control.address_type, frame.destination_address, frame_format, frame.control.priority, frame.source_address, frame.lsdu, l_status);
    }

    /* forget frame */
    tx.buffer.clear();
    if (l_status == Status::ok) {
        tx.queue.pop_front();
        tx.queue_plain_data.pop_front();
    }
};

void TP1_Data_Link_Layer::A_L_Data_ind()
{
    /* frame was already indicated, hence no data available anymore. */
    if (rx.buffer.empty()) {
        return;
    }

    /* indicate frame */
    if (L_Data_ind) {
        TP1_L_Data_Frame frame;
        frame.fromData(std::cbegin(rx.buffer), std::cend(rx.buffer));
        assert(frame.control.poll == Poll::Data);
        if (is_addressed(frame)) {
            Frame_Format frame_format;
            frame_format.frame_type_parameter = to_frame_type_parameter(frame.control.frame_type);
            frame_format.extended_frame_format = frame.extended_control.extended_frame_format;
            L_Data_ind(frame.control.acknowledge, frame.extended_control.address_type, frame.destination_address, frame_format, frame.lsdu, frame.control.priority, frame.source_address);
        }
    }

    /* forget frame */
    rx.buffer.clear();
};

void TP1_Data_Link_Layer::A_L_SystemBroadcast_con(const Status l_status)
{
    /* frame was already confirmed, hence no data available anymore. */
    if (tx.buffer.empty()) {
        return;
    }

    /* confirm frame */
    if (L_SystemBroadcast_con) {
        TP1_L_Data_Frame frame;
        frame.fromData(std::cbegin(tx.buffer), std::cend(tx.buffer));
        Frame_Format frame_format;
        frame_format.frame_type_parameter = to_frame_type_parameter(frame.control.frame_type);
        L_SystemBroadcast_con(Group_Address(frame.destination_address), frame_format, frame.control.priority, frame.source_address, frame.lsdu, l_status);
    }

    /* forget frame */
    tx.buffer.clear();
    if (l_status == Status::ok) {
        tx.queue.pop_front();
        tx.queue_plain_data.pop_front();
    }
};

void TP1_Data_Link_Layer::A_L_SystemBroadcast_ind()
{
    /* frame was already indicated, hence no data available anymore. */
    if (rx.buffer.empty()) {
        return;
    }

    /* indicate frame */
    if (L_SystemBroadcast_ind) {
        TP1_L_Data_Frame frame;
        frame.fromData(std::cbegin(rx.buffer), std::cend(rx.buffer));
        Frame_Format frame_format;
        frame_format.frame_type_parameter = to_frame_type_parameter(frame.control.frame_type);
        L_SystemBroadcast_ind(frame.control.acknowledge, Group_Address(frame.destination_address), frame_format, frame.lsdu, frame.control.priority, frame.source_address);
    }

    /* forget frame */
    rx.buffer.clear();
};

void TP1_Data_Link_Layer::A_L_Poll_Data_con(const Status l_status)
{
    /* frame was already confirmed, hence no data available anymore. */
    if (rx.buffer.empty() && tx.buffer.empty()) {
        return;
    }

    /* confirm frame */
    if (L_Poll_Data_con) {
        TP1_L_Poll_Data_Frame frame;
        frame.fromData(std::cbegin(tx.buffer), std::cend(tx.buffer));
        L_Poll_Data_con(Group_Address(frame.destination_address), rx.buffer, l_status);
    }

    /* forget frame */
    tx.buffer.clear();
    rx.buffer.clear();
    if (l_status == Status::ok) {
        tx.queue.pop_front();
        tx.queue_plain_data.pop_front();
    }
};

void TP1_Data_Link_Layer::A_L_Plain_Data_con(const Status l_status)
{
    /* frame was already confirmed, hence no data available anymore. */
    if (tx.buffer.empty()) {
        return;
    }

    /* confirm frame */
    if (L_Plain_Data_con) {
        L_Plain_Data_con(tx.buffer, l_status);
    }

    /* forget frame */
    tx.buffer.clear();
    if (l_status == Status::ok) {
        tx.queue.pop_front();
        tx.queue_plain_data.pop_front();
    }
}

void TP1_Data_Link_Layer::A_L_Busmon_ind()
{
    /* frame was already indicated, hence no data available anymore. */
    if (rx.buffer.empty()) {
        return;
    }

    /* indicate frame */
    if (L_Plain_Data_ind) {
        /* omit ACK/NAK/BUSY/NAK+BUSY */
        if (rx.buffer.size() > 1) {
            const Status l_status{Status::ok};
            const std::vector<uint8_t> lpdu = rx.buffer;
            L_Plain_Data_ind(lpdu, l_status);
        }
    }
    if (L_Busmon_ind) {
        const Status l_status{Status::ok};
        const uint16_t time_stamp{};
        const std::vector<uint8_t> lpdu = rx.buffer;
        L_Busmon_ind(l_status, time_stamp, lpdu);
    }

    /* forget frame */
    rx.buffer.clear();
};

void TP1_Data_Link_Layer::A_L_Service_Information_ind()
{
    if (L_Service_Information_ind) {
        L_Service_Information_ind();
    }
};

void TP1_Data_Link_Layer::A_Ph_Data_req_start_of_frame()
{
    assert(state == State::Idle);
    assert(!tx.queue.empty());
    assert(!tx.queue_plain_data.empty());

    /* set start of frame */
    tx.buffer = tx.queue.front();
    tx.buffer_it = std::cbegin(tx.buffer);
    tx.plain_data = tx.queue_plain_data.front();
    assert(!tx.buffer.empty());

    current_Ph_Data_req_class = P_Class::start_of_frame;
    Ph_Data_req(P_Class::start_of_frame, *tx.buffer_it++);
};

void TP1_Data_Link_Layer::A_Ph_Data_req_inner_frame_char()
{
    assert(state == State::Busy);
    assert(!tx.buffer.empty());
    assert(tx.buffer_it != std::cbegin(tx.buffer));

    current_Ph_Data_req_class = P_Class::inner_frame_char;
    Ph_Data_req(P_Class::inner_frame_char, *tx.buffer_it++);
};

void TP1_Data_Link_Layer::A_Ph_Data_req_ack_char(const bool nak, const bool busy)
{
    assert(state == State::Busy);

    /* set ack status */
    TP1_Acknowledgement_Frame::Acknowledge ack;
    if (nak) {
        if (busy) {
            ack = TP1_Acknowledgement_Frame::Acknowledge::NAK_and_BUSY;
        } else {
            ack = TP1_Acknowledgement_Frame::Acknowledge::NAK;
        }
    } else {
        if (busy) {
            ack = TP1_Acknowledgement_Frame::Acknowledge::BUSY;
        } else {
            ack = TP1_Acknowledgement_Frame::Acknowledge::ACK;
        }
    }

    /* get frame from data */
    assert(!rx.buffer.empty());
    TP1_L_Data_Frame frame;
    frame.fromData(std::cbegin(rx.buffer), std::cend(rx.buffer));

    /* check if addressed */
    if (is_addressed(frame)) {
        current_Ph_Data_req_class = P_Class::ack_char;
        Ph_Data_req(P_Class::ack_char, static_cast<uint8_t>(ack));
    }
};

void TP1_Data_Link_Layer::A_Ph_Data_req_poll_data_char()
{
    current_Ph_Data_req_class = P_Class::poll_data_char;
    Ph_Data_req(P_Class::poll_data_char, poll_data);
};

void TP1_Data_Link_Layer::A_Ph_Data_req_fill_char()
{
    current_Ph_Data_req_class = P_Class::fill_char;
    Ph_Data_req(P_Class::fill_char, 0xFE);
};

void TP1_Data_Link_Layer::A_Ph_Reset_req()
{
    Ph_Reset_req();
};

bool TP1_Data_Link_Layer::G_tx_queue_complete() const
{
    return tx.queue.empty();
}

bool TP1_Data_Link_Layer::G_tx_buffer_complete() const
{
    if (tx.buffer.empty()) {
        return true;
    }

    return tx.buffer_it == std::cend(tx.buffer);
}

bool TP1_Data_Link_Layer::G_rx_buffer_complete() const
{
    if (rx.buffer.empty()) {
        return true;
    }

    std::vector<uint8_t>::const_iterator first = std::cbegin(rx.buffer);
    std::vector<uint8_t>::const_iterator last = std::cend(rx.buffer);

    if (first == last) {
        return false;
    }

    /* octet 0 */
    const Control ctrl(*first);
    if (++first == last) {
        return false;
    }

    if (ctrl.frame_type == Frame_Type::Standard) {
        /* octet 1..2: source address */
        if (++first == last) {
            return false;
        }
        if (++first == last) {
            return false;
        }

        /* octet 3..4: destination address */
        if (++first == last) {
            return false;
        }
        if (++first == last) {
            return false;
        }

        if (ctrl.poll == Poll::Data) {
            /* octet 5: AT, NPCI, LG */
            const uint4_t length = *first & 0x0f;
            if (++first == last) {
                return false;
            }

            // length counts octets 7..N
            // check octet is N+1
            return rx.buffer.size() == (7 + length + 1);
        } else { // Poll::Poll_Data
            // check octet is N+1
            return rx.buffer.size() == (6 + 1);
        }
    } else { // Frame_Type::Extended
        /* octet 1: extended control field */
        if (++first == last) {
            return false;
        }

        /* octet 2..3: source address */
        if (++first == last) {
            return false;
        }
        if (++first == last) {
            return false;
        }

        /* octet 4..5: destination address */
        if (++first == last) {
            return false;
        }
        if (++first == last) {
            return false;
        }

        /* octet 6: length */
        const uint8_t length = *first;
        if (++first == last) {
            return false;
        }

        // length counts octets 8..N
        // check octet is N+1
        return rx.buffer.size() == (8 + length + 1);
    }

    return false;
}

void TP1_Data_Link_Layer::process_event()
{
    switch (state) {
    case State::Bus_Off:
        break;
    case State::Idle:
        // start next transmission
        if (!G_tx_queue_complete()) {
            A_Ph_Data_req_start_of_frame();
            state = State::Busy;
        }
        break;
    case State::Busy:
        // frame transmission ongoing.
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_L_Data_req evt)
{
    /* assemble frame */
    TP1_L_Data_Frame tp1_frame;
    tp1_frame.control.frame_type = to_frame_type(evt.frame_format.frame_type_parameter);
    tp1_frame.control.repeat = Repeat::not_repeated;
    tp1_frame.control.priority = evt.priority;
    tp1_frame.control.acknowledge = evt.ack_request;
    tp1_frame.extended_control.address_type = evt.address_type;
    tp1_frame.extended_control.extended_frame_format = evt.frame_format.extended_frame_format;
    tp1_frame.source_address = evt.source_address;
    tp1_frame.destination_address = evt.destination_address;
    tp1_frame.lsdu = evt.lsdu;

    /* enqueue this first */
    tx.queue.push_back(tp1_frame.toData());
    tx.queue_plain_data.push_back(false);

    /* request */
    switch (state) {
    case State::Bus_Off:
        tx.queue.pop_back();
        tx.queue_plain_data.pop_back();
        A_L_Data_con(Status::not_ok);
        break;
    case State::Idle:
        A_Ph_Data_req_start_of_frame();
        state = State::Busy;
        break;
    case State::Busy:
        // frame transmission ongoing.
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_L_SystemBroadcast_req evt)
{
    /* assemble frame */
    TP1_L_Data_Frame tp1_frame;
    tp1_frame.control.repeat = Repeat::not_repeated;
    tp1_frame.control.system_broadcast = System_Broadcast::System_Broadcast;
    tp1_frame.control.priority = evt.priority;
    tp1_frame.control.acknowledge = evt.ack_request;
    tp1_frame.extended_control.address_type = Address_Type::Group;
    tp1_frame.extended_control.extended_frame_format = evt.frame_format.extended_frame_format;
    if (group_address_table) {
        tp1_frame.source_address = group_address_table->individual_address;
    }
    tp1_frame.destination_address = evt.destination_address;
    tp1_frame.lsdu = evt.lsdu;

    /* enqueue this first */
    tx.queue.push_back(tp1_frame.toData());
    tx.queue_plain_data.push_back(false);

    switch (state) {
    case State::Bus_Off:
        tx.queue.pop_back();
        tx.queue_plain_data.pop_back();
        A_L_SystemBroadcast_con(Status::not_ok);
        break;
    case State::Idle:
        A_Ph_Data_req_start_of_frame();
        state = State::Busy;
        break;
    case State::Busy:
        // frame transmission ongoing.
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_L_Poll_Data_req evt)
{
    /* assemble frame */
    TP1_L_Poll_Data_Frame tp1_frame;
    tp1_frame.control.poll = Poll::Poll_Data;
    tp1_frame.control.repeat = Repeat::not_repeated;
    if (group_address_table) {
        tp1_frame.source_address = group_address_table->individual_address;
    }
    tp1_frame.destination_address = evt.destination_address;
    tp1_frame.no_of_exp_poll_data = evt.no_of_expected_poll_data;

    /* enqueue this first */
    tx.queue.push_back(tp1_frame.toData());
    tx.queue_plain_data.push_back(false);

    switch (state) {
    case State::Bus_Off:
        tx.queue.pop_back();
        tx.queue_plain_data.pop_back();
        A_L_Poll_Data_con(Status::not_ok);
        break;
    case State::Idle:
        A_Ph_Data_req_start_of_frame();
        state = State::Busy;
        break;
    case State::Busy:
        // frame transmission ongoing.
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_L_Plain_Data_req evt)
{
    /* enqueue this first */
    tx.queue.push_back(evt.data);
    tx.queue_plain_data.push_back(true);

    switch (state) {
    case State::Bus_Off:
        tx.queue.pop_back();
        tx.queue_plain_data.pop_back();
        A_L_Plain_Data_con(Status::not_ok);
        break;
    case State::Idle:
        A_Ph_Data_req_start_of_frame();
        state = State::Busy;
        break;
    case State::Busy:
        // frame transmission ongoing.
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_con_ok evt)
{
    assert(group_address_table);

    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        assert(false);
        break;
    case State::Busy:
        switch (evt.p_class) {
        case P_Class::start_of_frame:
            // Transmitter sent start_of_frame. There is always an inner_frame_char following.
            A_Ph_Data_req_inner_frame_char();
            break;
        case P_Class::inner_frame_char:
            // Transmitter sent inner_frame_char. Check if frame is complete now.
            if (G_tx_buffer_complete()) {
                if (tx.plain_data) {
                    A_L_Plain_Data_con();
                    state = State::Idle;
                } else {
                    const Control control(tx.buffer.front());
                    if (control.poll == Poll::Data) {
                        if (control.acknowledge == Ack_Request::dont_care) {
                            if (control.system_broadcast == System_Broadcast::Broadcast) {
                                A_L_Data_con();
                            } else { // System_Broadcast::System_Broadcast
                                A_L_SystemBroadcast_con();
                            }
                            state = State::Idle;
                        } else { // Ack_Request::ack_requested
                            // stay busy to wait for ack_char
                        }
                    } else { // Poll::Poll_Data
                        TP1_L_Poll_Data_Frame frame;
                        frame.fromData(std::cbegin(tx.buffer), std::cend(tx.buffer));
                        if (frame.no_of_exp_poll_data > 0) {
                            A_Ph_Data_req_fill_char();
                        } else {
                            A_L_Poll_Data_con();
                            state = State::Idle;
                        }
                    }
                }

                // trigger next transmission
                io_context.post([this]() {
                    process_event();
                });
            } else { // not complete
                // Frame not complete. Send next inner_frame_char.
                A_Ph_Data_req_inner_frame_char();
            }
            break;
        case P_Class::ack_char:
            // Receiver sent ack_char. Indicate frame reception.
        {
            assert(rx.buffer.size() >= 3);
            const Control control(rx.buffer.front());
            const Individual_Address source_address((rx.buffer[1] << 8) | (rx.buffer[2]));
            if (control.system_broadcast == System_Broadcast::Broadcast) {
                A_L_Data_ind();
            } else { // System_Broadcast::System_Broadcast
                A_L_SystemBroadcast_ind();
            }
            state = State::Idle;

            // trigger next transmission
            io_context.post([this]() {
                process_event();
            });
        }
        break;
        case P_Class::poll_data_char:
            // Receiver (poll slave) sent poll_data_char.
            rx.buffer.push_back(0); // received poll_data_char is irrelevant
            if (G_rx_buffer_complete()) {
                state = State::Idle;
            } else {
                // stay busy to wait for end of frame
            }
            break;
        case P_Class::fill_char:
            // Transmitter (poll master) sent fill_char.
        {
            // Poll Data Master
            TP1_L_Poll_Data_Frame frame;
            frame.fromData(std::cbegin(tx.buffer), std::cend(tx.buffer));
            rx.buffer.push_back(0xFE);
            const uint8_t poll_data_size = rx.buffer.size();
            if (poll_data_size < frame.no_of_exp_poll_data) {
                A_Ph_Data_req_fill_char();
            } else {
                A_L_Poll_Data_con();
                state = State::Idle;
            }
        }
        break;
        case P_Class::parity_error:
            assert(false);
            break;
        case P_Class::framing_error:
            assert(false);
            break;
        case P_Class::bit_error:
            assert(false);
            break;
        }
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_con_bus_not_free evt)
{
    // This is followed by a Ph_Data_ind.
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        assert(false);
        break;
    case State::Busy:
        switch (evt.p_class) {
        case P_Class::start_of_frame:
            // Transmitter sent start_of_frame, but ack_char, inner_frame_char, fill_char, poll_data_char was faster.
            assert(false);
            break;
        case P_Class::inner_frame_char:
            // Transmitter sent inner_frame_char, but fill_char, poll_data_char was faster.
            assert(false);
            break;
        case P_Class::ack_char:
            // Receiver sent ack_char, but fill_char, poll_data_char was faster.
            assert(false);
            break;
        case P_Class::poll_data_char:
            // Receiver (poll slave) sent poll_data_char. Nothing is faster.
            assert(false);
            break;
        case P_Class::fill_char:
            // Transmitter (poll master) send fill_char, but poll_data_char was faster.
            // Do nothing. This is handled in Ph_Data_ind.
            break;
        case P_Class::parity_error:
            assert(false);
            break;
        case P_Class::framing_error:
            assert(false);
            break;
        case P_Class::bit_error:
            assert(false);
            break;
        };
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_con_collision_detected evt)
{
    // This is followed by a Ph_Data_ind.
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        break;
    case State::Busy:
        switch (evt.p_class) {
        case P_Class::start_of_frame:
            // Transmitter sent start_of_frame, but another transmitter sent start_of_frame as well.
            assert(false);
            break;
        case P_Class::inner_frame_char:
            // Transmitter sent inner_frame_char, but another transmitter sent inner_frame_char as well.
            assert(false);
            break;
        case P_Class::ack_char:
            // Receiver sent ack_char, but another device sent NAK | BUSY.
            state = State::Idle;
            break;
        case P_Class::poll_data_char:
            // Receiver (poll slave) sent poll_data_char, but another receiver sent poll_data_char in same slot as well.
            assert(false);
            break;
        case P_Class::fill_char:
            // Transmitter (poll master) sent fill_char, but char was different.
            assert(false);
            break;
        case P_Class::parity_error:
            assert(false);
            break;
        case P_Class::framing_error:
            assert(false);
            break;
        case P_Class::bit_error:
            assert(false);
            break;
        };
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_con_transceiver_fault /*evt*/)
{
    switch (state) {
    case State::Bus_Off:
        break;
    case State::Idle:
        state = State::Bus_Off;
        break;
    case State::Busy:
        state = State::Bus_Off;
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_ind_start_of_frame evt)
{
    // Receiver received start_of_frame.
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        rx.buffer.clear();
        rx.buffer.push_back(evt.p_data);
        state = State::Busy;
        break;
    case State::Busy:
        // Transmitter: Next frame started, before expected ack_char was received
        if (G_tx_buffer_complete()) {
            const Control control(tx.buffer.front());
            assert(control.poll == Poll::Data);
            if (control.system_broadcast == System_Broadcast::Broadcast) {
                A_L_Data_con(Status::not_ok);
            } else {
                A_L_SystemBroadcast_con(Status::not_ok);
            }
        }

        rx.buffer.push_back(evt.p_data);
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_ind_inner_frame_char evt)
{
    // Receiver received inner_frame_char.
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        // ignore
        break;
    case State::Busy:
        rx.buffer.push_back(evt.p_data);

        if (G_rx_buffer_complete()) {
            const Control control(rx.buffer.front());
            if (control.poll == Poll::Data) {
                if (mode == Mode::Normal) {
                    TP1_L_Data_Frame frame;
                    frame.fromData(std::cbegin(rx.buffer), std::cend(rx.buffer));
                    if (frame.source_address == group_address_table->individual_address) {
                        A_L_Service_Information_ind();
                    }
                    if (is_addressed(frame)) {
                        A_Ph_Data_req_ack_char();
                        // stay busy to wait for ack_char
                    } else { // not addressed
                        state = State::Idle;
                    }
                } else { // Mode::Busmonitor
                    A_L_Busmon_ind();
                    state = State::Idle;

                    // trigger next transmission
                    io_context.post([this]() {
                        process_event();
                    });
                }
            } else { // Poll_Data
                // Poll Data Slave
                TP1_L_Poll_Data_Frame frame;
                frame.fromData(std::cbegin(rx.buffer), std::cbegin(rx.buffer) + 7);
                const uint8_t poll_data_size = rx.buffer.size() - 7;
                if ((frame.destination_address == device_object->polling_group_settings()->polling_group_address) && (poll_data_size == device_object->polling_group_settings()->polling_slot_number)) {
                    A_Ph_Data_req_poll_data_char();
                }
                if (poll_data_size == frame.no_of_exp_poll_data) {
                    if (mode == Mode::Normal) {
                        // No L_Poll_Data_ind
                    } else { // Mode::Busmonitor
                        A_L_Busmon_ind();
                    }
                    state = State::Idle;
                }
            }
        }
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_ind_ack_char evt)
{
    // Transmitter received ack_char.
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        if (mode == Mode::Normal) {
            // ignore ack_char. con() was already called as Acknowledge was don't care.
        } else { // Mode::Busmonitor
            rx.buffer = { evt.p_data };
            A_L_Busmon_ind();
        }

        // trigger next transmission
        io_context.post([this]() {
            process_event();
        });
        break;
    case State::Busy:
        if (mode == Mode::Normal) {
            switch (evt.p_data) {
            case 0x00: // NAK + BUSY
                tx.nak_retry++;
                tx.busy_retry++;
                if ((tx.nak_retry <= device_object->maximum_retry_count()->nak_retry) && (tx.busy_retry <= device_object->maximum_retry_count()->busy_retry)) {
                    A_Ph_Data_req_start_of_frame();
                } else {
                    A_L_Data_con(Status::not_ok);
                }
                break;
            case 0x0c: // NAK
                tx.nak_retry++;
                if (tx.nak_retry <= device_object->maximum_retry_count()->nak_retry) {
                    A_Ph_Data_req_start_of_frame();
                } else {
                    A_L_Data_con(Status::not_ok);
                }
                break;
            case 0xc0: // BUSY
                tx.busy_retry++;
                if (tx.busy_retry <= device_object->maximum_retry_count()->busy_retry) {
                    A_Ph_Data_req_start_of_frame();
                } else {
                    A_L_Data_con(Status::not_ok);
                }
                break;
            case 0xcc: // ACK
                A_L_Data_con(Status::ok);
                state = State::Idle;
                break;
            default:
                assert(false);
            }

            // trigger next transmission
            io_context.post([this]() {
                process_event();
            });
        } else { // Mode::Busmonitor
            assert(false);
        }
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_ind_poll_data_char evt)
{
    // Transmitter (Poll Master) or Receiver (Poll Slave) received poll_data_char.
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        assert(false);
        break;
    case State::Busy:
        rx.buffer.push_back(evt.p_data);
        if (tx.buffer.empty()) {
            // Poll Data Slave
            TP1_L_Poll_Data_Frame frame;
            frame.fromData(std::cbegin(rx.buffer), std::cbegin(rx.buffer) + 7);
            const uint8_t poll_data_size = rx.buffer.size() - 7;
            if ((frame.destination_address == device_object->polling_group_settings()->polling_group_address) && (poll_data_size == device_object->polling_group_settings()->polling_slot_number)) {
                A_Ph_Data_req_poll_data_char();
            }
            if (poll_data_size == frame.no_of_exp_poll_data) {
                if (mode == Mode::Normal) {
                    // No L_Poll_Data_ind
                } else { // Mode::Busmonitor
                    A_L_Busmon_ind();
                }
                state = State::Idle;
            }
        } else {
            // Poll Data Master
            TP1_L_Poll_Data_Frame frame;
            frame.fromData(std::cbegin(tx.buffer), std::cend(tx.buffer));
            const uint8_t poll_data_size = rx.buffer.size();
            if (poll_data_size < frame.no_of_exp_poll_data) {
                A_Ph_Data_req_fill_char();
            } else {
                A_L_Poll_Data_con();
                state = State::Idle;
            }
        }
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_ind_fill_char evt)
{
    // Transmitter (Poll Master) or Receiver (Poll Slave) received poll_data_char.
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        assert(false);
        break;
    case State::Busy:
        rx.buffer.push_back(evt.p_data);
        if (tx.buffer.empty()) {
            // Poll Data Slave
            TP1_L_Poll_Data_Frame frame;
            frame.fromData(std::cbegin(rx.buffer), std::cbegin(rx.buffer) + 7);
            const uint8_t poll_data_size = rx.buffer.size() - 7;
            if ((frame.destination_address == device_object->polling_group_settings()->polling_group_address) && (poll_data_size == device_object->polling_group_settings()->polling_slot_number)) {
                A_Ph_Data_req_poll_data_char();
            }
            if (poll_data_size == frame.no_of_exp_poll_data) {
                if (mode == Mode::Normal) {
                    // No L_Poll_Data_ind
                } else { // Mode::Busmonitor
                    A_L_Busmon_ind();
                }
                state = State::Idle;
            }
        } else {
            // Poll Data Master
            TP1_L_Poll_Data_Frame frame;
            frame.fromData(std::cbegin(tx.buffer), std::cend(tx.buffer));
            const uint8_t poll_data_size = rx.buffer.size();
            if (poll_data_size < frame.no_of_exp_poll_data) {
                A_Ph_Data_req_fill_char();
            } else {
                A_L_Poll_Data_con();
                state = State::Idle;
            }
        }
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_ind_parity_error /*evt*/)
{
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        assert(false);
        break;
    case State::Busy:
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_ind_framing_error /*evt*/)
{
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        assert(false);
        break;
    case State::Busy:
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Data_ind_bit_error /*evt*/)
{
    switch (state) {
    case State::Bus_Off:
        assert(false);
        break;
    case State::Idle:
        assert(false);
        break;
    case State::Busy:
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Reset_con_ok /*evt*/)
{
    switch (state) {
    case State::Bus_Off:
        state = State::Idle;
        break;
    case State::Idle:
        break;
    case State::Busy:
        break;
    }
}

void TP1_Data_Link_Layer::process_event(const E_Ph_Reset_con_transceiver_fault /*evt*/)
{
    switch (state) {
    case State::Bus_Off:
        break;
    case State::Idle:
        state = State::Bus_Off;
        break;
    case State::Busy:
        state = State::Bus_Off;
        break;
    }
}

}
