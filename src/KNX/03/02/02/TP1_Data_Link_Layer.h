// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <deque>
#include <functional>

#include <asio/io_context.hpp>

#include <KNX/03/02/02/TP1_Acknowledgement_Frame.h>
#include <KNX/03/02/02/TP1_L_Data_Frame.h>
#include <KNX/03/02/02/TP1_Physical_Layer.h>
#include <KNX/03/03/02/Ack_Request.h>
#include <KNX/03/03/02/Data_Link_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * TP1 Data Link Layer
 *
 * @ingroup KNX_03_02_02_02_04
 */
class KNX_EXPORT TP1_Data_Link_Layer :
    public Data_Link_Layer
{
public:
    explicit TP1_Data_Link_Layer(TP1_Physical_Layer & tp1_physical_layer, asio::io_context & io_context);
    virtual ~TP1_Data_Link_Layer();

    void L_Data_req(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address) override;
    void L_SystemBroadcast_req(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority) override;
    void L_Poll_Data_req(const Group_Address destination_address, const uint8_t no_of_expected_poll_data) override;
    void L_Plain_Data_req(const uint32_t time_stamp, const std::vector<uint8_t> data) override;

protected:
    asio::io_context & io_context;

    /* Ph_Data */
    std::function<void(const P_Class p_class, const uint8_t p_data)> Ph_Data_req;
    virtual void Ph_Data_con(const P_Status p_status);
    virtual void Ph_Data_ind(const P_Class p_class, const uint8_t p_data);
    P_Class current_Ph_Data_req_class{};

    /* Ph_Reset */
    std::function<void()> Ph_Reset_req;
    virtual void Ph_Reset_con(const P_Status p_status);

    /** is frame addressed to data link layer */
    bool is_addressed(const TP1_L_Data_Frame & frame) const;

    /* States */

    /** State */
    enum class State {
        /** Bus off */
        Bus_Off,

        /** Bus idle */
        Idle,

        /** Bus busy */
        Busy,
    };

    /** @copydoc State */
    State state{State::Idle};

    /* Events */

    struct E_L_Data_req {
        // L_Data.req(ack_request, address_type, destination_address, frame_format, lsdu, priority, source_address)
        const Ack_Request ack_request;
        const Address_Type address_type;
        const Address destination_address;
        const Frame_Format frame_format;
        std::shared_ptr<LSDU> lsdu;
        const Priority priority;
        const Individual_Address source_address;
    };
    struct E_L_SystemBroadcast_req {
        // L_SystemBroadcast.req(ack_request, destination_address, frame_format, lsdu, priority, source_address)
        const Ack_Request ack_request;
        const Group_Address destination_address;
        const Frame_Format frame_format;
        std::shared_ptr<LSDU> lsdu;
        const Priority priority;
    };
    struct E_L_Poll_Data_req {
        // L_Poll_Data.req(destination, no_of_expected_poll_data)
        const Group_Address destination_address;
        const uint8_t no_of_expected_poll_data;
    };
    struct E_L_Plain_Data_req {
        // L_Plain_Data.req(data)
        const uint32_t time;
        const std::vector<uint8_t> data;
    };
    struct E_Ph_Data_con_ok {
        // Ph_Data.con(p_status=ok)
        const P_Class p_class;
    };
    struct E_Ph_Data_con_bus_not_free {
        // Ph_Data.con(p_status=bus_not_free)
        const P_Class p_class;
    };
    struct E_Ph_Data_con_collision_detected {
        // Ph_Data.con(p_status=collision_detected)
        const P_Class p_class;
    };
    struct E_Ph_Data_con_transceiver_fault {
        // Ph_Data.con(p_status=transceiver_fault)
        const P_Class p_class;
    };
    struct E_Ph_Data_ind_start_of_frame {
        // Ph_Data.ind(p_class=start_of_frame, p_data)
        uint8_t p_data;
    };
    struct E_Ph_Data_ind_inner_frame_char {
        // Ph_Data.ind(p_class=inner_frame_char, p_data)
        uint8_t p_data;
    };
    struct E_Ph_Data_ind_ack_char {
        // Ph_Data.ind(p_class=ack_char, p_data)
        uint8_t p_data;
    };
    struct E_Ph_Data_ind_poll_data_char {
        // Ph_Data.ind(p_class=poll_data_char, p_data)
        uint8_t p_data;
    };
    struct E_Ph_Data_ind_fill_char {
        // Ph_Data.ind(p_class=fill_char, p_data)
        uint8_t p_data;
    };
    struct E_Ph_Data_ind_parity_error {
        // Ph_Data.ind(p_class=parity_error, p_data)
        uint8_t p_data;
    };
    struct E_Ph_Data_ind_framing_error {
        // Ph_Data.ind(p_class=framing_error, p_data)
        uint8_t p_data;
    };
    struct E_Ph_Data_ind_bit_error {
        // Ph_Data.ind(p_class=bit_error, p_data)
        uint8_t p_data;
    };
    struct E_Ph_Reset_con_ok {
        // Ph_Reset.con(p_status=ok)
    };
    struct E_Ph_Reset_con_transceiver_fault {
        // Ph_Reset.con(p_status=transceiver_fault)
    };

    /* Actions */

    /** confirm data */
    void A_L_Data_con(const Status l_status = Status::ok);

    /** indicate data */
    void A_L_Data_ind();

    /** confirm system broadcast */
    void A_L_SystemBroadcast_con(const Status l_status = Status::ok);

    /** indicate system broadcast */
    void A_L_SystemBroadcast_ind();

    /** confirm poll data */
    void A_L_Poll_Data_con(const Status l_status = Status::ok);

    /** confirm plain data */
    void A_L_Plain_Data_con(const Status l_status = Status::ok);

    /** indicate busmon */
    void A_L_Busmon_ind();

    /** indicate service information */
    void A_L_Service_Information_ind();

    /** request start of frame */
    void A_Ph_Data_req_start_of_frame();

    /** request inner frame char */
    void A_Ph_Data_req_inner_frame_char();

    /** request ack char */
    void A_Ph_Data_req_ack_char(const bool nak = false, const bool busy = false);

    /** request poll data char */
    void A_Ph_Data_req_poll_data_char();

    /** request fill char */
    void A_Ph_Data_req_fill_char();

    /** request reset */
    void A_Ph_Reset_req();

    /* Guards */

    /** tx queue complete */
    bool G_tx_queue_complete() const;

    /** tx buffer complete */
    bool G_tx_buffer_complete() const;

    /** rx buffer complete */
    bool G_rx_buffer_complete() const;

    /* Transition Table */

    void process_event();
    void process_event(const E_L_Data_req evt);
    void process_event(const E_L_SystemBroadcast_req evt);
    void process_event(const E_L_Poll_Data_req evt);
    void process_event(const E_L_Plain_Data_req evt);
    void process_event(const E_Ph_Data_con_ok evt);
    void process_event(const E_Ph_Data_con_bus_not_free evt);
    void process_event(const E_Ph_Data_con_collision_detected evt);
    void process_event(const E_Ph_Data_con_transceiver_fault evt);
    void process_event(const E_Ph_Data_ind_start_of_frame evt);
    void process_event(const E_Ph_Data_ind_inner_frame_char evt);
    void process_event(const E_Ph_Data_ind_ack_char evt);
    void process_event(const E_Ph_Data_ind_poll_data_char evt);
    void process_event(const E_Ph_Data_ind_fill_char evt);
    void process_event(const E_Ph_Data_ind_parity_error evt);
    void process_event(const E_Ph_Data_ind_framing_error evt);
    void process_event(const E_Ph_Data_ind_bit_error evt);
    void process_event(const E_Ph_Reset_con_ok evt);
    void process_event(const E_Ph_Reset_con_transceiver_fault evt);

    /** Tx State Machine */
    struct Tx {
        /** tx buffer between L_*_req and L_*_con */
        std::vector<uint8_t> buffer{};

        /** tx buffer iterator for L_*_req */
        std::vector<uint8_t>::const_iterator buffer_it;

        /** tx plain data / raw */
        bool plain_data{false};

        /** number of retries in case of a NAK response or a acknowledgement time-out */
        uint8_t nak_retry{}; // @see Data_Link_Layer::nak_retry

        /** number of retries in case of a BUSY/FULL response */
        uint8_t busy_retry{}; // @see Data_Link_Layer::busy_retry

        /** transmit queue - data */
        std::deque<std::vector<uint8_t>> queue{};

        /** transmit queue - plain data / raw */
        std::deque<bool> queue_plain_data{};
    } tx;

    /** Rx State Machine */
    struct Rx {
        /** rx buffer for L_*_ind */
        std::vector<uint8_t> buffer{};
    } rx;
};

}
