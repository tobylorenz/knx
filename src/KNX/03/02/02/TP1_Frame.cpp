// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/02/TP1_Frame.h>

#include <cassert>

#include <KNX/03/02/02/TP1_Acknowledgement_Frame.h>
#include <KNX/03/02/02/TP1_L_Data_Frame.h>
#include <KNX/03/02/02/TP1_L_Poll_Data_Frame.h>

namespace KNX {

TP1_Frame::~TP1_Frame()
{
}

std::shared_ptr<TP1_Frame> make_TP1_Frame(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    /* octet 1 */
    const uint8_t ctrl = *first;

    /* octet 1..N */
    if (ctrl == 0xf0) {
        std::shared_ptr<TP1_L_Poll_Data_Frame> frame = std::make_shared<TP1_L_Poll_Data_Frame>();
        frame->fromData(first, last);
        return frame;
    }
    if ((ctrl & 0x33) == 0) {
        std::shared_ptr<TP1_Acknowledgement_Frame> frame = std::make_shared<TP1_Acknowledgement_Frame>();
        frame->fromData(first, last);
        return frame;
    }
    std::shared_ptr<TP1_L_Data_Frame> frame = std::make_shared<TP1_L_Data_Frame>();
    frame->fromData(first, last);
    return frame;
}

}
