// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/03/02/LPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * TP1 Frame
 */
class KNX_EXPORT TP1_Frame
{
public:
    virtual ~TP1_Frame();
};

KNX_EXPORT std::shared_ptr<TP1_Frame> make_TP1_Frame(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
