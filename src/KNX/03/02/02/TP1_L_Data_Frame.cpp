// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/02/TP1_L_Data_Frame.h>

#include <cassert>

#include <KNX/03/03/03/NPDU.h>

namespace KNX {

TP1_L_Data_Frame::TP1_L_Data_Frame() :
    TP1_Frame(),
    L_Data_PDU()
{
}

TP1_L_Data_Frame::~TP1_L_Data_Frame()
{
}

void TP1_L_Data_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    /* octet 0 */
    control = *first++;

    switch (control.frame_type) {
    case Frame_Type::Standard: {
        /* octet 1..2 */
        source_address.fromData(first, first + 2);
        first += 2;

        /* octet 3..4 */
        destination_address.fromData(first, first + 2);
        first += 2;

        /* octet 5 */
        const uint8_t at_npci_lg = *first++;
        extended_control.address_type = static_cast<Address_Type>(at_npci_lg >> 7);
        // hop_count passed to NPDU
        length = at_npci_lg & 0x0f;

        /* octet 6..N */
        lsdu = make_NPDU_Individual(control, destination_address, at_npci_lg & 0xf0, first, first + 1 + length);
        first += length + 1;

        /* octet N+1 */
        check_octet = *first++;
    }
    break;
    case Frame_Type::Extended: {
        /* octet 1 */
        const uint8_t ctrle  = *first++;
        extended_control = ctrle;

        /* octet 2..3 */
        source_address.fromData(first, first + 2);
        first += 2;

        /* octet 4..5 */
        destination_address.fromData(first, first + 2);
        first += 2;

        /* octet 6 */
        length = *first++;

        /* octet 7..N */
        lsdu = make_NPDU_Individual(control, destination_address, ctrle & 0xf0, first, first + 1 + length);
        first += length + 1;

        /* octet N+1 */
        check_octet = *first++;
    }
    break;
    }
}

std::vector<uint8_t> TP1_L_Data_Frame::toData() const
{
    assert(lsdu);

    std::vector<uint8_t> data;

    /* octet 0 */
    data.push_back(control);

    const Hop_Count hop_count = lsdu->hop_count;
    const std::vector<uint8_t> lsdu_data = lsdu->toData();

    switch (control.frame_type) {
    case Frame_Type::Standard: {
        /* octet 1..2 */
        data.push_back(source_address >> 8);
        data.push_back(source_address & 0xff);

        /* octet 3..4 */
        data.push_back(destination_address >> 8);
        data.push_back(destination_address & 0xff);

        /* octet 5 */
        data.push_back(
            (static_cast<uint8_t>(extended_control.address_type) << 7) |
            (hop_count << 4) |
            length_calculated());

        /* octet 6..N */
        data.insert(std::cend(data), std::cbegin(lsdu_data), std::cend(lsdu_data));

        /* octet N+1 */
        data.push_back(check_octet);
    }
    break;
    case Frame_Type::Extended: {
        /* octet 1 */
        data.push_back((extended_control & 0x8f) | (hop_count << 4));

        /* octet 2..3 */
        data.push_back(source_address >> 8);
        data.push_back(source_address & 0xff);

        /* octet 4..5 */
        data.push_back(destination_address >> 8);
        data.push_back(destination_address & 0xff);

        /* octet 6 */
        data.push_back(length_calculated());

        /* octet 7..N */
        data.insert(std::cend(data), std::cbegin(lsdu_data), std::cend(lsdu_data));

        /* octet N+1 */
        data.push_back(check_octet);
    }
    break;
    }

    return data;
}

}
