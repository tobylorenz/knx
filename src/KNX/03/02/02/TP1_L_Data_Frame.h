// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/02/02/TP1_Check_Octet.h>
#include <KNX/03/02/02/TP1_Frame.h>
#include <KNX/03/03/02/L_Data/L_Data_PDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * TP1 L_Data Frame
 *
 * @ingroup KNX_03_02_02_02_02_03
 * @ingroup KNX_03_02_02_02_02_04
 * @ingroup KNX_03_02_02_02_02_05
 */
class KNX_EXPORT TP1_L_Data_Frame :
    public TP1_Frame,
    public L_Data_PDU
{
public:
    TP1_L_Data_Frame();
    virtual ~TP1_L_Data_Frame();

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /** check octet / frame check sequence */
    TP1_Check_Octet check_octet{};
};

}
