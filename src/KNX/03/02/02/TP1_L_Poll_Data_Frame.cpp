// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/02/TP1_L_Poll_Data_Frame.h>

#include <cassert>

#include <KNX/03/03/03/NPDU.h>

namespace KNX {

void TP1_L_Poll_Data_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 7);

    /* octet 0 */
    control = *first++;

    /* octet 1..2 */
    source_address.fromData(first, first + 2);
    first += 2;

    /* octet 3..4 */
    destination_address.fromData(first, first + 2);
    first += 2;

    /* octet 5 */
    no_of_exp_poll_data = *first++ & 0x0f;

    /* octet 6 */
    check_octet = *first++;

    assert(first == last);
}

std::vector<uint8_t> TP1_L_Poll_Data_Frame::toData() const
{
    std::vector<uint8_t> data;

    /* octet 0 */
    data.push_back(control);

    /* octet 1..2 */
    data.push_back(source_address >> 8);
    data.push_back(source_address & 0xff);

    /* octet 3..4 */
    data.push_back(destination_address >> 8);
    data.push_back(destination_address & 0xff);

    /* octet 5 */
    data.push_back(no_of_exp_poll_data);

    /* octet 6 */
    data.push_back(check_octet);

    return data;
}

}
