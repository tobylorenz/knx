// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/02/02/TP1_Check_Octet.h>
#include <KNX/03/02/02/TP1_Frame.h>
#include <KNX/03/03/02/L_Poll_Data/L_Poll_Data_PDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * TP1 L_Poll_Data Request Frame
 *
 * @ingroup KNX_03_02_02_02_02_06
 */
class KNX_EXPORT TP1_L_Poll_Data_Frame :
    public TP1_Frame,
    public L_Poll_Data_PDU
{
public:
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /** check octet / frame check sequence */
    TP1_Check_Octet check_octet{};
};

}
