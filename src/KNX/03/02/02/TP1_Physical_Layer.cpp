// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/02/TP1_Physical_Layer.h>

namespace KNX {

TP1_Physical_Layer::TP1_Physical_Layer(asio::io_context & io_context) :
    io_context(io_context)
{
}

TP1_Physical_Layer::~TP1_Physical_Layer()
{
}

}
