// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>

#include <asio/io_context.hpp>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * p_class
 *
 * @ingroup KNX_03_02_03_01_04
 */
enum class P_Class : uint8_t {
    /** first character of a frame with bus free detection */
    start_of_frame, // local

    /** character after 2 bit times without bus free detection */
    inner_frame_char, // local

    /** character 15 bit times after the preceding character without bus free detection */
    ack_char, // remote

    /** character 5 bit times after the preceding character without bus free detection */
    poll_data_char, // remote

    /** character 6 bit times after the preceding character with bus free detection */
    fill_char, // local

    /** wrong parity bit detected in the character received */
    parity_error,

    /** wrong stop bit detected in the character received */
    framing_error,

    /** wrong data bit detected in the character */
    bit_error
};

/**
 * p_status
 *
 * @ingroup KNX_03_02_03_01_04
 */
enum class P_Status : uint8_t {
    /** character transmission succeeded */
    ok,

    /** no transmission, another device is transmitting */
    bus_not_free,

    /** a collision was detected (logical '1') transmitted, but logical '0' received */
    collision_detected,

    /** transceiver fault detected */
    transceiver_fault
};

/**
 * TP1 Physical Layer
 *
 * @ingroup KNX_03_02_02_01_04
 */
class KNX_EXPORT TP1_Physical_Layer
{
public:
    explicit TP1_Physical_Layer(asio::io_context & io_context);
    virtual ~TP1_Physical_Layer();

    /* Ph_Data */
    virtual void Ph_Data_req(const P_Class p_class, const uint8_t p_data) = 0;
    std::function<void(const P_Status p_status)> Ph_Data_con;
    std::function<void(const P_Class p_class, const uint8_t data)> Ph_Data_ind;

    /* Ph_Reset */
    virtual void Ph_Reset_req() = 0;
    std::function<void(const P_Status p_status)> Ph_Reset_con;

protected:
    asio::io_context & io_context;
};

}
