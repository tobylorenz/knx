// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/02/TP1_Physical_Layer_Simulation.h>

#undef OUTPUT_ACTIONS

#include <cassert>
#ifdef OUTPUT_ACTIONS
#include <iomanip>
#include <iostream>
#endif

#include <asio.hpp>

namespace KNX {

/* TP1_Character */

void TP1_Character::clear()
{
    time = 255;
    p_data = 0xFF;
}

/* TP1_Bus_Simulation */

TP1_Bus_Simulation::TP1_Bus_Simulation(asio::io_context & io_context) :
    io_context(io_context)
{
}

TP1_Bus_Simulation::~TP1_Bus_Simulation()
{
}

void TP1_Bus_Simulation::handle_requests()
{
    TP1_Character tp1_character;

    /* calculate next character */
    for (const TP1_Physical_Layer_Simulation * device : devices) {
        assert(device);

        if (device->request.time < tp1_character.time) {
            tp1_character = device->request;
        } else if (device->request.time == tp1_character.time) {
            tp1_character.p_data &= device->request.p_data;
        }
    }

#ifdef OUTPUT_ACTIONS
    if (tp1_character.time == 255) {
        std::cerr << "(idle)" << std::endl;
    } else {
        switch (tp1_character.p_class) {
        case P_Class::start_of_frame:
            std::cerr << "(start_of_frame)";
            break;
        case P_Class::inner_frame_char:
            std::cerr << "(inner_frame_char)";
            break;
        case P_Class::ack_char:
            std::cerr << "(ack_char)";
            break;
        case P_Class::poll_data_char:
            std::cerr << "(poll_data_char)";
            break;
        case P_Class::fill_char:
            std::cerr << "(fill_char)";
            break;
        case P_Class::parity_error:
            std::cerr << "(parity_error)";
            break;
        case P_Class::framing_error:
            std::cerr << "(framing_error)";
            break;
        case P_Class::bit_error:
            std::cerr << "(bit_error)";
            break;
        }
        std::cerr << " " << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(tp1_character.p_data) << std::endl;
    }
#endif
    /* check for idle */
    if (tp1_character.time == 0xff) {
        return;
    }

    /* inform devices */
    for (TP1_Physical_Layer_Simulation * device : devices) {
        assert(device);

        if (device->request.time < 255) {
            /* request/confirm */
            if (device->Ph_Data_con) {
                if (device->request.time == tp1_character.time) {
                    if (device->request.p_data == tp1_character.p_data) {
                        device->request.clear();
                        device->Ph_Data_con(P_Status::ok);
                    } else {
                        bool collision_detected{false};
                        bool transceiver_fault{false};
                        for (uint8_t bit = 0; bit < 8; bit++) {
                            const bool tx = (device->request.p_data >> bit) & 0x01;
                            const bool rx = (tp1_character.p_data >> bit) & 0x01;
                            if ((tx == 1) && (rx == 0)) {
                                collision_detected = true;
                            }
                            if ((tx == 0) && (rx == 1)) {
                                transceiver_fault = true;
                            }
                        }
                        device->request.clear();
                        // transceiver fault has higher priority than collision detected
                        if (transceiver_fault) {
                            device->Ph_Data_con(P_Status::transceiver_fault);
                        }
                        if (collision_detected) {
                            device->Ph_Data_con(P_Status::collision_detected);
                            if (device->Ph_Data_ind) {
                                device->Ph_Data_ind(tp1_character.p_class, tp1_character.p_data);
                            }
                        }
                        assert(false);
                    }
                } else {
                    device->request.clear();
                    device->Ph_Data_con(P_Status::bus_not_free);
                    if (device->Ph_Data_ind) {
                        device->Ph_Data_ind(tp1_character.p_class, tp1_character.p_data);
                    }
                }
            }
        } else {
            /* indication */
            if (device->Ph_Data_ind) {
                device->Ph_Data_ind(tp1_character.p_class, tp1_character.p_data);
            }
        }
    }
}

/* TP1_Physical_Layer */

TP1_Physical_Layer_Simulation::TP1_Physical_Layer_Simulation(asio::io_context & io_context) :
    TP1_Physical_Layer(io_context)
{
}

TP1_Physical_Layer_Simulation::~TP1_Physical_Layer_Simulation()
{
}

void TP1_Physical_Layer_Simulation::connect(TP1_Bus_Simulation & bus)
{
    /* register device */
    m_bus = &bus;
    m_bus->devices.insert(this);
}

void TP1_Physical_Layer_Simulation::disconnect()
{
    /* return if not connected */
    if (!m_bus) {
        return;
    }

    /* unregister device */
    m_bus->devices.erase(this);
    m_bus = nullptr;
}

void TP1_Physical_Layer_Simulation::Ph_Data_req(const P_Class p_class, const uint8_t p_data)
{
    /* time */
    switch (p_class) {
    case P_Class::start_of_frame:
        request.time = 50;
        break;
    case P_Class::inner_frame_char:
        request.time = 13;
        break;
    case P_Class::ack_char:
        request.time = 15;
        break;
    case P_Class::poll_data_char:
        request.time = 5;
        break;
    case P_Class::fill_char:
        request.time = 6;
        break;
    case P_Class::parity_error:
        assert(false); // cannot be sent
        break;
    case P_Class::framing_error:
        assert(false); // cannot be sent
        break;
    case P_Class::bit_error:
        assert(false); // cannot be sent
        break;
    }
    request.p_class = p_class;
    request.p_data = p_data;

    /* initiator */
    io_context.post([this]() {
        if (!m_bus) {
            /* no bus, so confirm a transceiver fault */
            if (Ph_Data_con) {
                Ph_Data_con(P_Status::transceiver_fault);
            }
        } else {
            /* inform all devices */
            m_bus->handle_requests();
        }
    });
}

void TP1_Physical_Layer_Simulation::Ph_Reset_req()
{
    /* initiator */
    io_context.post([this]() {
        /* inform device */
        if (Ph_Reset_con) {
            if (!m_bus) {
                /* no bus, so confirm a transceiver fault */
                Ph_Reset_con(P_Status::transceiver_fault);
            } else {
                /* confirm successful reset */
                Ph_Reset_con(P_Status::ok);
            }
        }
    });
}

}
