// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <set>

#include <KNX/03/02/02/TP1_Physical_Layer.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/* forward declarations */
class TP1_Bus_Simulation;
class TP1_Physical_Layer_Simulation;

/** TP1_Character */
struct TP1_Character {
    /** bit time delay based on p_class, but also indicates idle */
    uint8_t time{255}; // bit times

    /** class */
    P_Class p_class{};

    /** data */
    uint8_t p_data{0xFF};

    /** clear */
    void clear();
};

/**
 * TP1 Bus Simulation
 */
class KNX_EXPORT TP1_Bus_Simulation
{
public:
    explicit TP1_Bus_Simulation(asio::io_context & io_context);
    virtual ~TP1_Bus_Simulation();

    /** connected devices */
    std::set<TP1_Physical_Layer_Simulation *> devices {};

    /**
     * handle next requests
     *
     * This basically calculates the requests from all devices.
     */
    void handle_requests();

protected:
    asio::io_context & io_context;
};

/**
 * TP1 Physical Layer Simulation
 *
 * @ingroup KNX_03_02_02_01_04
 */
class KNX_EXPORT TP1_Physical_Layer_Simulation :
    public TP1_Physical_Layer
{
public:
    explicit TP1_Physical_Layer_Simulation(asio::io_context & io_context);
    virtual ~TP1_Physical_Layer_Simulation();

    /** connect physical layer to bus */
    void connect(TP1_Bus_Simulation & bus);

    /** disconnect physical layer from bus */
    void disconnect();

    void Ph_Data_req(const P_Class p_class, const uint8_t p_data) override;
    void Ph_Reset_req() override;

    /** send request */
    TP1_Character request{};

protected:
    /** bus connection */
    TP1_Bus_Simulation * m_bus{};
};

}
