// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/04/PL132_Frame.h>

#include <KNX/03/02/04/PL132_L_Data_Frame.h>

namespace KNX {

PL132_Frame::~PL132_Frame()
{
}

std::shared_ptr<PL132_Frame> make_PL132_Frame(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
     // everything else is unknown and not needed for TSSA test
    std::shared_ptr<PL132_L_Data_Frame> frame = std::make_shared<PL132_L_Data_Frame>();
    frame->fromData(first, last);
    return frame;
}

}
