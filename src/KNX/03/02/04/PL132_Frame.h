// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/03/02/LPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PL132 Frame
 */
class KNX_EXPORT PL132_Frame
{
public:
    virtual ~PL132_Frame();
};

KNX_EXPORT std::shared_ptr<PL132_Frame> make_PL132_Frame(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
