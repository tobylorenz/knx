// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/02/04/PL132_Frame.h>
#include <KNX/03/02/04/PL132_Preamble.h>
#include <KNX/03/02/04/PL132_Postamble.h>
#include <KNX/03/03/02/L_Data/L_Data_PDU.h>

namespace KNX {

/**
 * PL132 L_Data Frame
 *
 * Example frames:
 * 00 00 EF 03 78 00 00 66 03 E3 12 34 03 00 FF 0A 66 // A_DomainAddressSelective_Read
 * 00 00 EF 03 69 00 00 63 03 E2 12 34          0A 66 // A_DomainAddress_Response
 * 00 00 EF 03 78 00 00 66 03 E3 12 34 03 00 FF 0A 66 // A_DomainAddressSelective_Read
 * 00 00 EF 03 78 00 00 63 03 E0 12 35          0A 66 // A_DomainAddress_Write
 * 00 00 EF 03 FF 00 00 61 03 E1                0A 66 // A_DomainAddress_Read
 * 00 00 EF 03 78 00 00 63 03 E2 12 35          0A 66 // A_DomainAddress_Response
 */
class KNX_EXPORT PL132_L_Data_Frame :
    public PL132_Frame,
    public L_Data_PDU
{
public:
    PL132_L_Data_Frame();
    virtual ~PL132_L_Data_Frame();

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /** preamble */
    PL132_Preamble preamble{0x0000};

    /** postamble */
    PL132_Postamble postamble{0x0A66};
};

}
