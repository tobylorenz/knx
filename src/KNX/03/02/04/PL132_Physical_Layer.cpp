// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/04/PL132_Physical_Layer.h>

namespace KNX {

PL132_Physical_Layer::PL132_Physical_Layer(asio::io_context & io_context) :
    io_context(io_context)
{
}

}
