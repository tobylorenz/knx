// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/02/04/PL132_Physical_Layer_Simulation.h>

namespace KNX {

/* PL132_Bus_Simulation */

PL132_Bus_Simulation::PL132_Bus_Simulation(asio::io_context & io_context) :
    io_context(io_context)
{
}

PL132_Bus_Simulation::~PL132_Bus_Simulation()
{
}

/* PL132_Physical_Layer_Simulation */

PL132_Physical_Layer_Simulation::PL132_Physical_Layer_Simulation(asio::io_context & io_context) :
    PL132_Physical_Layer(io_context)
{
}

PL132_Physical_Layer_Simulation::~PL132_Physical_Layer_Simulation()
{
}

void PL132_Physical_Layer_Simulation::connect(PL132_Bus_Simulation & bus)
{
    /* register device */
    m_bus = &bus;
    m_bus->devices.insert(this);
}

void PL132_Physical_Layer_Simulation::disconnect()
{
    /* return if not connected */
    if (!m_bus) {
        return;
    }

    /* unregister device */
    m_bus->devices.erase(this);
    m_bus = nullptr;
}

void PL132_Physical_Layer_Simulation::Ph_Data_req(const std::vector<uint8_t> data)
{
    io_context.post([this, data]() {
        assert(m_bus);

        for(auto device: m_bus->devices) {
            if (device == this) {
                assert(device->Ph_Data_con);
                device->Ph_Data_con(data);
            } else {
                assert(device->Ph_Data_ind);
                device->Ph_Data_ind(data);
            }
        }
    });
}

void PL132_Physical_Layer_Simulation::Ph_Reset_req()
{
}

}
