// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>
#include <set>

#include <KNX/03/02/04/PL132_Physical_Layer.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/* forward declarations */
class PL132_Bus_Simulation;
class PL132_Physical_Layer_Simulation;

/** PL132 Bus Simulation */
class KNX_EXPORT PL132_Bus_Simulation
{
public:
    explicit PL132_Bus_Simulation(asio::io_context & io_context);
    virtual ~PL132_Bus_Simulation();

    /** connected devices */
    std::set<PL132_Physical_Layer_Simulation *> devices {};

protected:
    asio::io_context & io_context;
};

/** PL132 Physical Layer Simulation */
class KNX_EXPORT PL132_Physical_Layer_Simulation :
    public PL132_Physical_Layer
{
public:
    explicit PL132_Physical_Layer_Simulation(asio::io_context & io_context);
    virtual ~PL132_Physical_Layer_Simulation();

    /** connect physical layer to bus */
    void connect(PL132_Bus_Simulation & bus);

    /** disconnect physical layer from bus */
    void disconnect();

    void Ph_Data_req(const std::vector<uint8_t> data) override;
    void Ph_Reset_req() override;

protected:
    /** bus connection */
    PL132_Bus_Simulation * m_bus{};
};

}
