// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/CEMI_Client.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * IP Data Link Layer
 *
 * @ingroup KNX_03_02_06_04_03
 */
using IP_Data_Link_Layer = CEMI_Client;

// actually this consists of
// KNX/03/06/03/cEMI/CEMI_Client.h, resp. CEMI_Server.h
// KNX/03/08/IP_Communication_Channel

}
