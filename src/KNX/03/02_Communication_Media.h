// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 Twisted Pair 0 */
#include <KNX/03/02/01_Twisted_Pair_0.h>

/* 2 Twister Pair 1 */
#include <KNX/03/02/02_Twisted_Pair_1.h>

/* 3 Powerline */
#include <KNX/03/02/03_Powerline.h>

/* 4 Powerline 132 */
#include <KNX/03/02/04_Powerline_132.h>

/* 5 Radio Frequency */
#include <KNX/03/02/05_Radio_Frequency.h>

/* 6 KNX IP */
#include <KNX/03/02/06_KNX_IP.h>
