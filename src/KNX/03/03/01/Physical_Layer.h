// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>
#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/** Physical Layer */
class KNX_EXPORT Physical_Layer
{
public:
    virtual void Ph_Data_req(const std::vector<uint8_t> data) = 0;
    std::function<void(const std::vector<uint8_t> data)> Ph_Data_con;
    std::function<void(const std::vector<uint8_t> data)> Ph_Data_ind;
};

}
