// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Acknowledge request
 *
 * @ingroup KNX_03_03_02_02_02_02
 */
enum class Ack_Request : uint1_t {
    /** no acknowledge is requested */
    dont_care = 0,

    /** acknowledge requested */
    ack_requested = 1,
};

}
