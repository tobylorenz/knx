// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/Address.h>

namespace KNX {

Address::Address(const uint16_t address) :
    m_address(address)
{
}

Address & Address::operator=(const uint16_t & address)
{
    m_address = address;

    return *this;
}

Address::operator uint16_t() const
{
    return m_address;
}

void Address::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    m_address = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> Address::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(m_address >> 8);
    data.push_back(m_address & 0xff);

    return data;
}

}
