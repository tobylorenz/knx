// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Address
 *
 * @ingroup KNX_03_03_02_01_04
 */
class KNX_EXPORT Address
{
public:
    Address() = default;
    explicit Address(const uint16_t address);
    virtual ~Address() = default;
    Address & operator=(const uint16_t & address);
    operator uint16_t() const;
    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    std::vector<uint8_t> toData() const;

protected:
    /** address */
    uint16_t m_address{};
};

}
