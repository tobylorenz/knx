// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Address Type (AT)
 *
 * @ingroup KNX_03_02_02_02_02_05_03
 */
enum class Address_Type : uint1_t {
    /** Individual Address */
    Individual = 0,

    /** Group Address */
    Group = 1
};

}
