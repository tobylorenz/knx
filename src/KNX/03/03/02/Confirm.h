// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/** Confirm */
enum class Confirm : uint1_t {
    /** no error */
    no_error = 0,

    /** error */
    error = 1
};

}
