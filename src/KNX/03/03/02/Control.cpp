// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/Control.h>

namespace KNX {

Control::Control(const uint8_t control) :
    frame_type(static_cast<Frame_Type>((control >> 7) & 0x01)),
    poll(static_cast<Poll>((control >> 6) & 0x01)),
    repeat(static_cast<Repeat>((control >> 5) & 0x01)),
    system_broadcast(static_cast<System_Broadcast>((control >> 4) & 0x01)),
    priority(static_cast<Priority>((control >> 2) & 0x03)),
    acknowledge(static_cast<Ack_Request>((control >> 1) & 0x01)),
    confirm(static_cast<Confirm>(control & 0x01))
{
}

Control & Control::operator=(const uint8_t & control)
{
    frame_type = static_cast<Frame_Type>((control >> 7) & 0x01);
    poll = static_cast<Poll>((control >> 6) & 0x01);
    repeat = static_cast<Repeat>((control >> 5) & 0x01);
    system_broadcast = static_cast<System_Broadcast>((control >> 4) & 0x01);
    priority = static_cast<Priority>((control >> 2) & 0x03);
    acknowledge = static_cast<Ack_Request>((control >> 1) & 0x01);
    confirm = static_cast<Confirm>(control & 0x01);

    return *this;
}

Control::operator uint8_t() const
{
    return
        (static_cast<uint8_t>(frame_type) << 7) |
        (static_cast<uint8_t>(poll) << 6) |
        (static_cast<uint8_t>(repeat) << 5) |
        (static_cast<uint8_t>(system_broadcast) << 4) |
        (static_cast<uint8_t>(priority) << 2) |
        (static_cast<uint8_t>(acknowledge) << 1) |
        (static_cast<uint8_t>(confirm) << 0);
}

}
