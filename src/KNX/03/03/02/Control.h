// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Ack_Request.h>
#include <KNX/03/03/02/Confirm.h>
#include <KNX/03/03/02/Frame_Type.h>
#include <KNX/03/03/02/Poll.h>
#include <KNX/03/03/02/Priority.h>
#include <KNX/03/03/02/Repeat.h>
#include <KNX/03/03/02/System_Broadcast.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Control (CTRL) field
 *
 * @ingroup KNX_03_02_02_02_02_02
 * @ingroup KNX_03_02_03_04_02_03_03_02
 */
struct KNX_EXPORT Control
{
    Control() = default;
    explicit Control(const uint8_t control);
    Control & operator=(const uint8_t & control);
    operator uint8_t() const;

    Frame_Type frame_type{Frame_Type::Standard};
    Poll poll{Poll::Data};
    Repeat repeat{Repeat::not_repeated};
    System_Broadcast system_broadcast{System_Broadcast::Broadcast};
    Priority priority{Priority::system};
    Ack_Request acknowledge{Ack_Request::dont_care};
    Confirm confirm{Confirm::no_error};
};

}
