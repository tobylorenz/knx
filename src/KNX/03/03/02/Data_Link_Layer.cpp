// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/Data_Link_Layer.h>

namespace KNX {

Data_Link_Layer::~Data_Link_Layer()
{
}

void Data_Link_Layer::L_Poll_Update_req(const uint8_t poll_data)
{
    this->poll_data = poll_data;

    if (L_Poll_Update_con) {
        L_Poll_Update_con();
    }
}

}
