// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>
#include <memory>
#include <vector>

//#include <KNX/03/03/01/Physical_Layer.h"
#include <KNX/03/03/02/Ack_Request.h>
#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Address_Type.h>
#include <KNX/03/03/02/Frame_Format.h>
#include <KNX/03/03/02/Group_Address.h>
#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/02/LSAP.h>
#include <KNX/03/03/02/LSDU.h>
#include <KNX/03/03/02/Priority.h>
#include <KNX/03/03/02/Status.h>
#include <KNX/03/03/07/Domain_Address.h>
#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Data Link Layer
 *
 * @ingroup KNX_03_03_02_02
 */
class KNX_EXPORT Data_Link_Layer
{
public:
    virtual ~Data_Link_Layer();

    /* 2.2 L_Data service */

    // @note in all following slots/signals, the octet_count was dropped, as it's part of lsdu anyway.

    virtual void L_Data_req(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address) = 0;
    std::function<void(const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status)> L_Data_con;
    std::function<void(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)> L_Data_ind;

    /* 2.3 L_SystemBroadcast service */

    virtual void L_SystemBroadcast_req(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority) = 0;
    std::function<void(const Group_Address destination_address, const Frame_Format frame_format, const Priority priority, Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status)> L_SystemBroadcast_con;
    std::function<void(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)> L_SystemBroadcast_ind;

    /* 2.4 L_Poll_Data-service and protocol */

    virtual void L_Poll_Data_req(const Group_Address destination_address, const uint8_t no_of_expected_poll_data) = 0;
    std::function<void(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status)> L_Poll_Data_con;
    virtual void L_Poll_Update_req(const uint8_t poll_data);
    std::function<void()> L_Poll_Update_con;

    /* 2.5 L_Busmon service */

    virtual void L_Plain_Data_req(const uint32_t time_stamp, const std::vector<uint8_t> data) = 0; // L_Raw.req equivalent
    std::function<void(const std::vector<uint8_t> data, const Status l_status)> L_Plain_Data_con; // L_Raw.con equivalent
    std::function<void(const std::vector<uint8_t> data, const Status l_status)> L_Plain_Data_ind; // L_Raw.ind equivalent
    std::function<void(const Status l_status, const uint16_t time_stamp, const std::vector<uint8_t> lpdu)> L_Busmon_ind;

    /* 2.6 L_Service_Information service */

    std::function<void()> L_Service_Information_ind;

    /* 2.7 L_Management service */
    // @note Management services are not defined in L, but mentioned more in External Message Interface (EMI)

    /* 4 Parameters */

    /** (OT-0) Device Object */
    std::shared_ptr<Device_Object> device_object{};
    // includes
    // - (18) Polling Group Settings (poll group address, response slot number)
    // - (52) Maximum (NAK/BUSY) Retry Count
    // - (57/58) Individual Address
    // - (70) Domain Address
    // - (82) RF Domain Address

    /** (OT-1) Group Address Table */
    std::shared_ptr<Group_Address_Table> group_address_table{};

    /** Zone Address Table */
    // @todo LTE Zones
    // Zones basically extend the Group Address by two additional "LTE" bits in the Extended_Frame_Format.
    // For this to work, there need to be up to four Group Address Tables - Realisation Type 6, as used by System 300.
    // Which group table is applied to which LTE extension, is defined in the PID_EXT_FRAMEFORMAT.

    /** poll data */
    uint8_t poll_data{};

    /** Data Link Layer Mode */
    enum class Mode {
        Normal,
        Busmonitor
    };

    /** @copydoc Mode */
    Mode mode{Mode::Normal};
};

}
