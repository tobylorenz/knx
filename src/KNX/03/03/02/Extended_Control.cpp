// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/Extended_Control.h>

namespace KNX {

Extended_Control::Extended_Control(const uint8_t extended_control) :
    address_type(static_cast<Address_Type>(extended_control >> 7)),
    // hop_count((extended_control >> 4) & 0x07),
    extended_frame_format(static_cast<Extended_Frame_Format>(extended_control & 0x0f))
{
}

Extended_Control & Extended_Control::operator=(const uint8_t & extended_control)
{
    address_type = static_cast<Address_Type>(extended_control >> 7);
    // hop_count = (extended_control >> 4) & 0x07;
    extended_frame_format = static_cast<Extended_Frame_Format>(extended_control & 0x0f);

    return *this;
}

Extended_Control::operator uint8_t() const
{
    return
        (static_cast<uint8_t>(address_type) << 7) |
        // (static_cast<uint8_t>(hop_count) << 4) |
        (static_cast<uint8_t>(extended_frame_format) << 0);
}

}
