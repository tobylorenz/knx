// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Address_Type.h>
#include <KNX/03/03/02/Extended_Frame_Format.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Extended Control (CTRLE) field
 *
 * @ingroup KNX_03_02_02_02_02_05_03
 */
class KNX_EXPORT Extended_Control
{
public:
    Extended_Control() = default;
    explicit Extended_Control(const uint8_t extended_control);
    Extended_Control & operator=(const uint8_t & extended_control);
    operator uint8_t() const;

    /** Address Type (AT) */
    Address_Type address_type{Address_Type::Individual};

    // Hop_Count hop_count{};

    /** Extended Frame Format (EFF) */
    Extended_Frame_Format extended_frame_format{Extended_Frame_Format::Standard};
};

}
