// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Extended Frame Format (EFF)
 *
 * @ingroup KNX_03_03_02_02_02_04
 * @ingroup KNX_10_01_07_01
 * @ingroup KNX_10_01_07_02
 */
enum class Extended_Frame_Format : uint8_t { // actually uint4_t
    /** Standard Group or Individual */
    Standard = 0x0, // 0000b

    // 0001b: Reserved
    // 0010b: Reserved
    // 0011b: Reserved

    /* LTE-HEE extended address type */

    /** Geographical tags: Apartment/Floor 01...63.R.S */
    LTE0 = 0x4, // 0100b

    /** Geographical tags: Apartment/Floor 64...126.R.S */
    LTE1 = 0x5, // 0101b

    /** range of Application specific tags */
    LTE2 = 0x6, // 0110b

    /** Unassigned (peripheral) tags & broadcast */
    LTE3 = 0x7, // 0111b

    // 1000b: Reserved
    // 1001b: Reserved
    // 1010b: Reserved
    // 1011b: Reserved
    // 1100b: Reserved
    // 1101b: Reserved
    // 1110b: Reserved

    /** Escape */
    Escape = 0xF // 1111b
};

}
