// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/Frame_Format.h>

#include <cassert>

namespace KNX {

Frame_Format::Frame_Format(const uint8_t frame_format) :
    frame_type_parameter(static_cast<Frame_Type_Parameter>(frame_format >> 7)),
    // hop_count((frame_format >> 4) & 0x07),
    extended_frame_format(static_cast<Extended_Frame_Format>(frame_format & 0x0f))
{
}

Frame_Format & Frame_Format::operator=(const uint8_t & frame_format)
{
    frame_type_parameter = static_cast<Frame_Type_Parameter>(frame_format >> 7);
    // hop_count = (frame_format >> 4) & 0x07;
    extended_frame_format = static_cast<Extended_Frame_Format>(frame_format & 0x0f);

    return *this;
}

Frame_Format::operator uint8_t() const
{
    return
        (static_cast<uint8_t>(frame_type_parameter) << 7) |
        // (static_cast<uint8_t>(hop_count) << 4) |
        (static_cast<uint8_t>(extended_frame_format) << 0);
}

bool operator==(const Frame_Format & a, const Frame_Format & b)
{
    if (a.frame_type_parameter != b.frame_type_parameter) {
        return false;
    }
    assert(a.frame_type_parameter == b.frame_type_parameter);

    if (a.frame_type_parameter == Frame_Type_Parameter::Standard) {
        return true;
    }
    assert(a.frame_type_parameter == Frame_Type_Parameter::Extended);
    assert(b.frame_type_parameter == Frame_Type_Parameter::Extended);

    return (a.extended_frame_format == b.extended_frame_format);
}

}
