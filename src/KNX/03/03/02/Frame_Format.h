// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Extended_Frame_Format.h>
#include <KNX/03/03/02/Frame_Type_Parameter.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Frame Format
 *
 * @ingroup KNX_03_03_02_02_02_04
 */
class KNX_EXPORT Frame_Format
{
public:
    Frame_Format() = default;
    explicit Frame_Format(const uint8_t frame_format);
    Frame_Format & operator=(const uint8_t & frame_format);
    operator uint8_t() const;

    /** Frame Type Parameter */
    Frame_Type_Parameter frame_type_parameter{Frame_Type_Parameter::Standard};

    // Hop_Count hop_count{};

    /** Extended Frame Format */
    Extended_Frame_Format extended_frame_format{Extended_Frame_Format::Standard};
};

KNX_EXPORT bool operator==(const Frame_Format & a, const Frame_Format & b);

}
