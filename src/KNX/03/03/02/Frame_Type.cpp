// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/Frame_Type.h>

#include <KNX/03/03/02/Frame_Type_Parameter.h>

namespace KNX {

Frame_Type to_frame_type(const Frame_Type_Parameter & frame_type_parameter)
{
    Frame_Type ft{};
    switch (frame_type_parameter) {
    case Frame_Type_Parameter::Standard:
        ft = Frame_Type::Standard;
        break;
    case Frame_Type_Parameter::Extended:
        ft = Frame_Type::Extended;
        break;
    }
    return ft;
}

}
