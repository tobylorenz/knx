// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/* forward declarations */
enum class Frame_Type_Parameter : uint1_t;

/**
 * Frame Type (FT)
 *
 * @ingroup KNX_03_02_02_02_02_02
 * @ingroup KNX_03_02_03_04_02_03_03_02
 */
enum class Frame_Type : uint1_t {
    /** Extended */
    Extended = 0,

    /** Standard */
    Standard = 1
};

KNX_EXPORT Frame_Type to_frame_type(const Frame_Type_Parameter & frame_type_parameter);

}
