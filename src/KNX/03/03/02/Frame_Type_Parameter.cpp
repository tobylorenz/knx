// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/Frame_Type_Parameter.h>

#include <KNX/03/03/02/Frame_Type.h>

namespace KNX {

Frame_Type_Parameter to_frame_type_parameter(const Frame_Type & frame_type)
{
    Frame_Type_Parameter ftp;
    switch (frame_type) {
    case Frame_Type::Standard:
        ftp = Frame_Type_Parameter::Standard;
        break;
    case Frame_Type::Extended:
        ftp = Frame_Type_Parameter::Extended;
        break;
    }
    return ftp;
}

}
