// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/* forward declarations */
enum class Frame_Type : uint1_t;

/**
 * Frame Type Parameter (FTP)
 *
 * @ingroup KNX_03_03_02_02_02_04
 */
enum class Frame_Type_Parameter : uint1_t {
    /** Standard Frame format (L_Data_Standard) */
    Standard = 0,

    /** Extended Frame Format (L_Data_Extended) */
    Extended = 1,
};

KNX_EXPORT Frame_Type_Parameter to_frame_type_parameter(const Frame_Type & frame_type);

}
