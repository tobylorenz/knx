// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/Group_Address.h>

#include <sstream>

namespace KNX {

Group_Address::Group_Address(const uint16_t group_address) :
    Address(group_address)
{
}

Group_Address::Group_Address(const std::string & group_address) :
    Address()
{
    std::istringstream iss(group_address);
    std::string str;
    std::vector<uint16_t> elements;
    while(getline(iss, str, '/') && (elements.size() < 3)) {
        elements.push_back(std::stoul(str));
    }
    // @todo parse Group Address
#if 0
    switch(elements.size()) {
    case 1:
        m_address = elements[0];
        break;
    case 2:
        set_address(elements[0], elements[1]);
        break;
    case 3:
        set_address(elements[0], elements[1], elements[2]);
        break;
    }
#endif
}

Group_Address & Group_Address::operator=(const uint16_t & group_address)
{
    m_address = group_address;

    return *this;
}

std::string Group_Address::text() const
{
    std::ostringstream oss;

    // @todo take all three forms into account
#if 0
    oss << std::dec << static_cast<uint16_t>(area_address())
        << "/"
        << std::dec << static_cast<uint16_t>(line_address())
        << "/"
        << std::dec << static_cast<uint16_t>(device_address());
#endif

    return oss.str();
}

bool Group_Address::is_broadcast() const
{
    return m_address == 0;
}

}
