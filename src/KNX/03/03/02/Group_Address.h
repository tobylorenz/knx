// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <string>

#include <KNX/03/03/02/Address.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Group Address (GA)
 *
 * @ingroup KNX_03_03_02_01_04_03
 */
class KNX_EXPORT Group_Address :
    public Address
{
public:
    Group_Address() = default;
    explicit Group_Address(const uint16_t group_address);
    explicit Group_Address(const std::string & group_address);
    Group_Address & operator=(const uint16_t & group_address);
    virtual std::string text() const;

    /** Broadcast */
    bool is_broadcast() const;
};

}
