// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/Individual_Address.h>

#include <sstream>

namespace KNX {

Individual_Address::Individual_Address(const uint16_t individual_address) :
    Address(individual_address)
{
}

Individual_Address::Individual_Address(const std::string & individual_address) :
    Address()
{
    std::istringstream iss(individual_address);
    std::string str;
    std::vector<uint16_t> elements;
    while(getline(iss, str, '.') && (elements.size() < 3)) {
        elements.push_back(std::stoul(str));
    }
    switch(elements.size()) {
    case 1:
        m_address = elements[0];
        break;
    case 2:
        set_address(elements[0], elements[1]);
        break;
    case 3:
        set_address(elements[0], elements[1], elements[2]);
        break;
    }
}

Individual_Address::Individual_Address(const Subnetwork_Address subnetwork_address, const Device_Address device_address) :
    Address((subnetwork_address << 8) | (device_address))
{
}

Individual_Address::Individual_Address(const Area_Address area_address, const Line_Address line_address, const Device_Address device_address) :
    Address(((area_address & 0x0f) << 12) | ((line_address & 0x0f) << 8) | (device_address))
{
}

Individual_Address & Individual_Address::operator=(const uint16_t & individual_address)
{
    m_address = individual_address;

    return *this;
}

std::string Individual_Address::text() const
{
    std::ostringstream oss;

    // @todo take all three forms into account
    oss << std::dec << static_cast<uint16_t>(area_address())
        << "."
        << std::dec << static_cast<uint16_t>(line_address())
        << "."
        << std::dec << static_cast<uint16_t>(device_address());

    return oss.str();
}

void Individual_Address::set_address(const Subnetwork_Address subnetwork_address, const Device_Address device_address)
{
    set_subnetwork_address(subnetwork_address);
    set_device_address(device_address);
}

void Individual_Address::set_address(const Area_Address area_address, const Line_Address line_address, const Device_Address device_address)
{
    set_area_address(area_address);
    set_line_address(line_address);
    set_device_address(device_address);
}

Subnetwork_Address Individual_Address::subnetwork_address() const
{
    return m_address >> 8;
}

void Individual_Address::set_subnetwork_address(const Subnetwork_Address & subnetwork_address)
{
    m_address = (subnetwork_address << 8) | (m_address & 0xff);
}

Area_Address Individual_Address::area_address() const
{
    return m_address >> 12;
}

void Individual_Address::set_area_address(const Area_Address & area_address)
{
    assert((area_address & 0xf0) == 0);

    m_address = static_cast<uint16_t>(area_address << 12) | (m_address & 0x0fff);
}

Line_Address Individual_Address::line_address() const
{
    return (m_address >> 8) & 0x0f;
}

void Individual_Address::set_line_address(const Line_Address & line_address)
{
    assert((line_address & 0xf0) == 0);

    m_address = (m_address & 0xf0ff) | static_cast<uint16_t>(line_address << 8);
}

Device_Address Individual_Address::device_address() const
{
    return m_address & 0xff;
}

void Individual_Address::set_device_address(const Device_Address & device_address)
{
    m_address = (m_address & 0xff00) | device_address;
}

}
