// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <string>

#include <KNX/03/03/02/Address.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/** Subnetwork Address (SNA) */
using Subnetwork_Address = uint8_t;

/** Area Address (AA) */
using Area_Address = uint4_t;

/** Line Address (LA) */
using Line_Address = uint4_t;

/** Device Address (DA) */
using Device_Address = uint8_t;

/**
 * Individual Address (IA)
 *
 * @ingroup KNX_03_03_02_01_04_02
 */
class KNX_EXPORT Individual_Address :
    public Address
{
public:
    Individual_Address() = default;
    explicit Individual_Address(const uint16_t individual_address);
    explicit Individual_Address(const std::string & individual_address);
    Individual_Address(const Subnetwork_Address subnetwork_address, const Device_Address device_address);
    Individual_Address(const Area_Address area_address, const Line_Address line_address, const Device_Address device_address);
    Individual_Address & operator=(const uint16_t & individual_address);
    virtual std::string text() const;

    virtual void set_address(const Subnetwork_Address subnetwork_address, const Device_Address device_address);
    virtual void set_address(const Area_Address area_address, const Line_Address line_address, const Device_Address device_address);

    /* Subnetwork Address (SNA) */
    virtual Subnetwork_Address subnetwork_address() const;
    virtual void set_subnetwork_address(const Subnetwork_Address & subnetwork_address);

    /* Area Address (AA) */
    virtual Area_Address area_address() const;
    virtual void set_area_address(const Area_Address & area_address);

    /* Line Address (LA) */
    virtual Line_Address line_address() const;
    virtual void set_line_address(const Line_Address & line_address);

    /* Device Address (DA) */
    virtual Device_Address device_address() const;
    virtual void set_device_address(const Device_Address & device_address);
};

}
