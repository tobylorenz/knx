// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Control.h>
#include <KNX/03/03/02/Extended_Control.h>
#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/02/LSDU.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/** LPDU */
class KNX_EXPORT LPDU
{
public:
    bool operator==(const LPDU & other) const = default;

    /** Control field (CTRL, Ctrl1) */
    Control control{};

    /** Extended Control field (CTRLE, Ctrl2) */
    Extended_Control extended_control{};

    /** Source Address */
    Individual_Address source_address{};

    /** Destination Address */
    Address destination_address{};
};

}
