// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Group_Address.h>

namespace KNX {

/** Data Link Layer Service Access Point (LSAP) */
using LSAP = Group_Address;

}
