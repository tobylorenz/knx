// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/L_Data/L_Data_PDU.h>

#include <KNX/03/03/03/NPDU.h>

namespace KNX {

L_Data_PDU::L_Data_PDU() :
    LPDU()
{
}

uint8_t L_Data_PDU::length_calculated() const
{
    assert(lsdu);

    return lsdu->length_calculated();
}

}
