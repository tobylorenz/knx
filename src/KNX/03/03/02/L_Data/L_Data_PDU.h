// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/03/02/LPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Data
 *
 * @ingroup KNX_03_03_02_02_02
 */
class KNX_EXPORT L_Data_PDU :
    public LPDU
{
public:
    L_Data_PDU();

    bool operator==(const L_Data_PDU & other) const = default;

    /* NPDU */

    /** Length (L) */
    uint8_t length{};
    // 4 bit for Standard Frame
    // 8 bit for Extended Frame
    // 0xFF as an escape code

    /** calculate Length (L) */
    virtual uint8_t length_calculated() const;

    /** LSDU = NPDU */
    std::shared_ptr<LSDU> lsdu{};
};

}
