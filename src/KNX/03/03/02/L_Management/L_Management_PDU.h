// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Management
 *
 * @ingroup KNX_03_03_02_02_07
 */
class KNX_EXPORT L_Management_PDU
{
public:
    bool operator==(const L_Management_PDU & other) const = default;
};

}
