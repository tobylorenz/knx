// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/L_Poll_Data/L_Poll_Data_PDU.h>

namespace KNX {

L_Poll_Data_PDU::L_Poll_Data_PDU() :
    LPDU()
{
    control.frame_type = Frame_Type::Standard;
    control.poll = Poll::Poll_Data;
}

}
