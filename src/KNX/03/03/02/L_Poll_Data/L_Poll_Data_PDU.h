// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/LPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Poll_Data
 *
 * @ingroup KNX_03_03_02_02_04
 */
class KNX_EXPORT L_Poll_Data_PDU :
    public LPDU
{
public:
    L_Poll_Data_PDU();

    bool operator==(const L_Poll_Data_PDU & other) const = default;

    /** Number of Expected Poll Data */
    uint4_t no_of_exp_poll_data{};
};

}
