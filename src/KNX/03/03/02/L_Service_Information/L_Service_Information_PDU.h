// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Service_Information
 *
 * @ingroup KNX_03_03_02_02_06
 */
class KNX_EXPORT L_Service_Information_PDU
{
public:
    bool operator==(const L_Service_Information_PDU & other) const = default;
};

}
