// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/02/L_SystemBroadcast/L_SystemBroadcast_PDU.h>

namespace KNX {

L_SystemBroadcast_PDU::L_SystemBroadcast_PDU() :
    LPDU()
{
    control.system_broadcast = System_Broadcast::System_Broadcast;
}

}
