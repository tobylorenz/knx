// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/LPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_SystemBroadcast
 *
 * @ingroup KNX_03_03_02_02_03
 */
class KNX_EXPORT L_SystemBroadcast_PDU :
    public LPDU
{
public:
    L_SystemBroadcast_PDU();

    bool operator==(const L_SystemBroadcast_PDU & other) const = default;
};

}
