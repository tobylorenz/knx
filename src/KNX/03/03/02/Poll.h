// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Poll (commonly used in TP1 DLL, EMI2, ...)
 *
 * @ingroup KNX_03_02_02_02_02_02
 */
enum class Poll : uint1_t {
    /** L_Data-Frame */
    Data = 0,

    /** L_Poll_Data-Frame */
    Poll_Data = 1
};

}
