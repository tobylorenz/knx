// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Priority
 *
 * @ingroup KNX_03_02_02_02_02_02
 * @ingroup KNX_03_03_02_02_02_03
 */
enum class Priority : uint8_t { // actually uint2_t
    /** system */
    system = 0, // 00b

    /** urgent */
    urgent = 2, // 10b

    /** normal */
    normal = 1, // 01b

    /** low */
    low = 3, // 11b
};

}
