// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Repeated (commonly used in TP1 DLL, EMI2, ...)
 *
 * @ingroup KNX_03_02_02_02_02_02
 */
enum class Repeat : uint1_t {
    /** repeated L_Data-Frame */
    repeated = 0,

    /** not repeated L_Data-Frame */
    not_repeated = 1
};

}
