// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

namespace KNX {

/** Service Status */
enum class Status {
    /* positive confirmation */
    ok,

    /* negative confirmation */
    not_ok
};

}
