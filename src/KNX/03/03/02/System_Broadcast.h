// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * System Broadcast (SB) flag
 */
enum class System_Broadcast : uint1_t {
    /** System Broadcast */
    System_Broadcast = 0,

    /** Broadcast */
    Broadcast = 1
};

}
