// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/** Hop Count Type */
using Hop_Count_Type = uint3_t;

/**
 * Hop Count Type - Hop Count 7
 *
 * - sending direction: hop_count shall be set to 7
 * - receiving direction: hop_count equal to 7
 */
static const Hop_Count_Type Hop_Count_7 = 7;

/**
 * Hop Count Type - Network Layer Parameter
 *
 * - sending direction: hop_count shall be set to the value of the Network Layer parameter 'hop_count'
 * - receiving direction: hop_count not equal to 7
 */
static const Hop_Count_Type Network_Layer_Parameter = 0;

}
