// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/03/NPDU.h>

#include <cassert>

#include <KNX/03/03/02/Address_Type.h>
#include <KNX/03/03/03/N_Data_Broadcast/N_Data_Broadcast_PDU.h>
#include <KNX/03/03/03/N_Data_Group/N_Data_Group_PDU.h>
#include <KNX/03/03/03/N_Data_Individual/N_Data_Individual_PDU.h>
#include <KNX/03/03/03/N_Data_SystemBroadcast/N_Data_SystemBroadcast_PDU.h>
#include <KNX/03/03/04/TPDU.h>

namespace KNX {

void NPDU::fromData(const Hop_Count hop_count, std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 0);

    /* octet 5 */
    this->hop_count = hop_count;
}

std::vector<uint8_t> NPDU::toData() const
{
    assert(nsdu);

    return nsdu->toData();
}

uint8_t NPDU::length_calculated() const
{
    return nsdu ? nsdu->length_calculated() : 0;
}

std::shared_ptr<NPDU> make_NPDU_Individual(const Control & control, const Address & destination_address, const uint8_t at_npci_lg, std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    /* octet 5 */
    const Address_Type address_type = static_cast<Address_Type>(at_npci_lg >> 7);
    const Hop_Count hop_count = (at_npci_lg >> 4) & 0x07;

    /* octet 6 */
    switch (address_type) {
    case Address_Type::Individual: {
        std::shared_ptr<N_Data_Individual_PDU> npdu = std::make_shared<N_Data_Individual_PDU>();
        npdu->fromData(hop_count, first, last);
        return npdu;
    }
    break;
    case Address_Type::Group: {
        if (destination_address == 0) {
            if (control.system_broadcast == System_Broadcast::Broadcast) {
                std::shared_ptr<N_Data_Broadcast_PDU> npdu = std::make_shared<N_Data_Broadcast_PDU>();
                npdu->fromData(hop_count, first, last);
                return npdu;
            } else {
                std::shared_ptr<N_Data_SystemBroadcast_PDU> npdu = std::make_shared<N_Data_SystemBroadcast_PDU>();
                npdu->fromData(hop_count, first, last);
                return npdu;
            }
        } else {
            std::shared_ptr<N_Data_Group_PDU> npdu = std::make_shared<N_Data_Group_PDU>();
            npdu->fromData(hop_count, first, last);
            return npdu;
        }
    }
    break;
    }
    return std::make_shared<NPDU>();
}

}
