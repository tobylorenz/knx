// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Control.h>
#include <KNX/03/03/03/Hop_Count.h>
#include <KNX/03/03/03/NSDU.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * NPDU
 *
 * @ingroup KNX_03_03_03_02_01
 */
class KNX_EXPORT NPDU
{
public:
    bool operator==(const NPDU & other) const = default;

    /**
     * from data
     *
     * @param[in] hop_count contained in octet 5 (at_npci_lg)
     * @param[in] first octet 6
     * @param[in] last octet N
     */
    virtual void fromData(const Hop_Count hop_count, std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

    /**
     * to data
     *
     * @return octet 6..N
     */
    virtual std::vector<uint8_t> toData() const;

    /** calculate Length (L) */
    virtual uint8_t length_calculated() const;

    /** Hop Count (HC) */
    Hop_Count hop_count{};

    /** NSDU = TPDU */
    std::shared_ptr<NSDU> nsdu{};
};

/**
 * make NPDU from L_Data (
 *
 * @param[in] control Control octet
 * @param[in] destination_address Group/Broadcast address
 * @param[in] at_npci_lg octet 5, which contains the network control field (address_type, hop_count, length)
 * @param[in] first octet 6
 * @param[in] last octet N
 * @return N_Data_Broadcast, N_Data_Group, N_Data_Individual
 */
KNX_EXPORT std::shared_ptr<NPDU> make_NPDU_Individual(const Control & control, const Address & destination_address, const uint8_t at_npci_lg, std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
