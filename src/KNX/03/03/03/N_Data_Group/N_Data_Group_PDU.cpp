// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/03/N_Data_Group/N_Data_Group_PDU.h>

#include <cassert>

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/04/TPDU.h>
#include <KNX/03/03/04/T_Data_Group/T_Data_Group_PDU.h>
#include <KNX/03/03/04/T_Data_Tag_Group/T_Data_Tag_Group_PDU.h>

namespace KNX {

void N_Data_Group_PDU::fromData(const Hop_Count hop_count, std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    /* octet 5, 6..N */
    NPDU::fromData(hop_count, first, first);

    /* octet 6..N */
    TPDU tpdu;
    tpdu.fromData(first, first + 1);
    if (tpdu.sequence_number == 0) {
        nsdu = std::make_shared<T_Data_Group_PDU>();
    } else {
        nsdu = std::make_shared<T_Data_Tag_Group_PDU>();
    }
    nsdu->fromData(first, last);
}

}
