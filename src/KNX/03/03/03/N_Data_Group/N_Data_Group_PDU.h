// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/03/NPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/** N_Data_Group */
class KNX_EXPORT N_Data_Group_PDU :
    public NPDU
{
public:
    bool operator==(const N_Data_Group_PDU & other) const = default;

    void fromData(const Hop_Count hop_count, std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
};

}
