// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/03/N_Data_Individual/N_Data_Individual_PDU.h>

#include <cassert>

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/04/TPDU.h>

namespace KNX {

void N_Data_Individual_PDU::fromData(const Hop_Count hop_count, std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    /* octet 5, 6..N */
    NPDU::fromData(hop_count, first, first);

    /* octet 6..N */
    nsdu = make_TPDU_Individual(first, last);
}

}
