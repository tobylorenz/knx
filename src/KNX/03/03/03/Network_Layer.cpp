// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/03/Network_Layer.h>

#include <KNX/03/03/03/N_Data_Broadcast/N_Data_Broadcast_PDU.h>
#include <KNX/03/03/03/N_Data_Group/N_Data_Group_PDU.h>
#include <KNX/03/03/03/N_Data_Individual/N_Data_Individual_PDU.h>
#include <KNX/03/03/03/N_Data_SystemBroadcast/N_Data_SystemBroadcast_PDU.h>

namespace KNX {

/* Network Layer */

void Network_Layer::connect(Data_Link_Layer & data_link_layer)
{
    L_Data_req = std::bind(&Data_Link_Layer::L_Data_req, &data_link_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
    data_link_layer.L_Data_con = std::bind(&Network_Layer::L_Data_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
    data_link_layer.L_Data_ind = std::bind(&Network_Layer::L_Data_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);

    L_SystemBroadcast_req = std::bind(&Data_Link_Layer::L_SystemBroadcast_req, &data_link_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    data_link_layer.L_SystemBroadcast_con = std::bind(&Network_Layer::L_SystemBroadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    data_link_layer.L_SystemBroadcast_ind = std::bind(&Network_Layer::L_SystemBroadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
}

/* N_Data_Individual service */

void Network_Layer::N_Data_Individual_req(const Ack_Request ack_request, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu)
{
    if (L_Data_req) {
        std::shared_ptr<N_Data_Individual_PDU> npdu = std::make_shared<N_Data_Individual_PDU>();
        npdu->hop_count = (hop_count_type == Hop_Count_7) ? Hop_Count(7) : device_object->routing_count()->routing_count;
        npdu->nsdu = nsdu;
        Frame_Format frame_format;
        frame_format.frame_type_parameter = (npdu->length_calculated() <= 15) ? Frame_Type_Parameter::Standard : Frame_Type_Parameter::Extended;
        L_Data_req(ack_request, Address_Type::Individual, destination_address, frame_format, npdu, priority, Individual_Address(0));
    }
}

/* N_Data_Group service */

void Network_Layer::N_Data_Group_req(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu)
{
    if (L_Data_req) {
        std::shared_ptr<N_Data_Group_PDU> npdu = std::make_shared<N_Data_Group_PDU>();
        npdu->hop_count = (hop_count_type == Hop_Count_7) ? Hop_Count(7) : device_object->routing_count()->routing_count;
        npdu->nsdu = nsdu;
        Frame_Format frame_format;
        frame_format.frame_type_parameter = (npdu->length_calculated() <= 15) ? Frame_Type_Parameter::Standard : Frame_Type_Parameter::Extended;
        frame_format.extended_frame_format = extended_frame_format;
        L_Data_req(ack_request, Address_Type::Group, destination_address, frame_format, npdu, priority, Individual_Address(0));
    }
}

/* N_Data_Broadcast service */

void Network_Layer::N_Data_Broadcast_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu)
{
    if (L_Data_req) {
        std::shared_ptr<N_Data_Broadcast_PDU> npdu = std::make_shared<N_Data_Broadcast_PDU>();
        npdu->hop_count = (hop_count_type == Hop_Count_7) ? Hop_Count(7) : device_object->routing_count()->routing_count;
        npdu->nsdu = nsdu;
        Frame_Format frame_format;
        frame_format.frame_type_parameter = (npdu->length_calculated() <= 15) ? Frame_Type_Parameter::Standard : Frame_Type_Parameter::Extended;
        Group_Address destination_address(0); // Broadcast
        L_Data_req(ack_request, Address_Type::Group, destination_address, frame_format, npdu, priority, Individual_Address(0));
    }
}

/* N_Data_SystemBroadcast service */

void Network_Layer::N_Data_SystemBroadcast_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu)
{
    if (L_SystemBroadcast_req) {
        std::shared_ptr<N_Data_SystemBroadcast_PDU> npdu = std::make_shared<N_Data_SystemBroadcast_PDU>();
        npdu->hop_count = (hop_count_type == Hop_Count_7) ? Hop_Count(7) : device_object->routing_count()->routing_count;
        npdu->nsdu = nsdu;
        Frame_Format frame_format;
        frame_format.frame_type_parameter = (npdu->length_calculated() <= 15) ? Frame_Type_Parameter::Standard : Frame_Type_Parameter::Extended;
        Group_Address destination_address(0); // Broadcast
        L_SystemBroadcast_req(ack_request, destination_address, frame_format, npdu, priority);
    }
}

/* L_Data service */

void Network_Layer::L_Data_con(const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address /*source_address*/, std::shared_ptr<LSDU> lsdu, const Status l_status)
{
    const Hop_Count_Type hop_count_type = lsdu->hop_count;
    switch (address_type) {
    case Address_Type::Individual:
        if (N_Data_Individual_con) {
            N_Data_Individual_con(Ack_Request::dont_care, Individual_Address(destination_address), hop_count_type, priority, lsdu->nsdu, l_status);
        }
        break;
    case Address_Type::Group: {
        Group_Address destination_group_address(destination_address);
        if (destination_group_address.is_broadcast()) {
            if (N_Data_Broadcast_con) {
                N_Data_Broadcast_con(Ack_Request::dont_care, hop_count_type, priority, lsdu->nsdu, l_status);
            }
        } else {
            if (N_Data_Group_con) {
                N_Data_Group_con(Ack_Request::dont_care, destination_group_address, frame_format.extended_frame_format, hop_count_type, priority, lsdu->nsdu, l_status);
            }
        }
    }
    break;
    }
}

void Network_Layer::L_Data_ind(const Ack_Request /*ack_request*/, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, Priority priority, const Individual_Address source_address)
{
    const Hop_Count_Type hop_count_type = lsdu->hop_count;
    switch (address_type) {
    case Address_Type::Individual: {
        if (N_Data_Individual_ind) {
            Individual_Address destination_individual_address(destination_address);
            N_Data_Individual_ind(destination_individual_address, hop_count_type, priority, source_address, lsdu->nsdu);
        }
    }
    break;
    case Address_Type::Group: {
        Group_Address destination_group_address(destination_address);
        if (destination_group_address.is_broadcast()) {
            if (N_Data_Broadcast_ind) {
                N_Data_Broadcast_ind(hop_count_type, priority, source_address, lsdu->nsdu);
            }
        } else {
            if (N_Data_Group_ind) {
                N_Data_Group_ind(destination_group_address, frame_format.extended_frame_format, hop_count_type, priority, source_address, lsdu->nsdu);
            }
        }
    }
    break;
    }
}

/* from L_SystemBroadcast service */

void Network_Layer::L_SystemBroadcast_con(const Group_Address /*destination_address*/, const Frame_Format /*frame_format*/, const Priority priority, const Individual_Address /*source_address*/, std::shared_ptr<LSDU> lsdu, const Status l_status)
{
    const Hop_Count_Type hop_count_type = lsdu->hop_count;
    if (N_Data_SystemBroadcast_con) {
        N_Data_SystemBroadcast_con(Ack_Request::dont_care, hop_count_type, lsdu->nsdu, priority, l_status);
    }
}

void Network_Layer::L_SystemBroadcast_ind(const Ack_Request /*ack_request*/, const Group_Address /*destination_address*/, const Frame_Format /*frame_format*/, std::shared_ptr<LSDU> lsdu, Priority priority, const Individual_Address source_address)
{
    const Hop_Count_Type hop_count_type = lsdu->hop_count;
    if (N_Data_SystemBroadcast_ind) {
        N_Data_SystemBroadcast_ind(hop_count_type, lsdu->nsdu, priority, source_address);
    }
}

}
