// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Data_Link_Layer.h>
#include <KNX/03/03/03/Hop_Count.h>
#include <KNX/03/03/03/Hop_Count_Type.h>
#include <KNX/03/03/03/NSDU.h>
#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/** Network Layer */
class KNX_EXPORT Network_Layer
{
public:
    /** connect to Data Link Layer */
    void connect(Data_Link_Layer & data_link_layer);

    /* 2.2 Network Layer services */

    // @note in all following slots/signals, the octet_count was dropped, as it's part of nsdu anyway.

    /* 2.2.1 N_Data_Individual service */

    virtual void N_Data_Individual_req(const Ack_Request ack_request, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu);
    std::function<void(const Ack_Request ack_request, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status)> N_Data_Individual_con;
    std::function<void(const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)> N_Data_Individual_ind;

    /* 2.2.2 N_Data_Group service */

    // @note "Extended_Frame_Format extended_frame_format" added
    virtual void N_Data_Group_req(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu);
    std::function<void(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status)> N_Data_Group_con;
    std::function<void(const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)> N_Data_Group_ind;

    /* 2.2.3 N_Data_Broadcast service */

    virtual void N_Data_Broadcast_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu);
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status)> N_Data_Broadcast_con;
    std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)> N_Data_Broadcast_ind;

    /* 2.2.4 N_Data_SystemBroadcast service */

    virtual void N_Data_SystemBroadcast_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu);
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Status n_status)> N_Data_SystemBroadcast_con;
    std::function<void(const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Individual_Address source_address)> N_Data_SystemBroadcast_ind;

    /* 2.3 Parameters of Network Layer */

    std::shared_ptr<Device_Object> device_object{};
    // includes
    // - (61) Hop Count

    /** Device Type */
    enum Device_Type {
        Normal_Device,

        /* Couplers */
        Repeater, // TP1 Repeater
        Bridge, // TP1 Bridge
        Router // Router
    };

    /** @copydoc Device_Type */
    Device_Type device_type{Device_Type::Normal_Device};

protected:
    /* L_Data service */
    std::function<void(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)> L_Data_req;
    virtual void L_Data_con(const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status);
    virtual void L_Data_ind(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address);

    /* L_SystemBroadcast service */
    std::function<void(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority)> L_SystemBroadcast_req;
    virtual void L_SystemBroadcast_con(const Group_Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status);
    virtual void L_SystemBroadcast_ind(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, Priority priority, const Individual_Address source_address);
};

}
