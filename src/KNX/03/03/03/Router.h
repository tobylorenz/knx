// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/03/03/03/Network_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * States of Network Layer for Routers
 *
 * @ingroup 03_03_03_02_04_02_04
 */
enum {
    ROUTE_UNMODIFIED,
    ROUTE_DECREMENTED,
    ROUTE_LAST,
    FORWARD_LOCALLY,
    IGNORE_TOTALLY,
    IGNORE_ACKED,
};

class KNX_EXPORT Router
{
public:
    std::vector<Network_Layer> network_layers;

    /* Filter algorithm for N_Data_Group req/ind */

    /* Individual Address for N_Data_Individual req/ind */

    /* N_Data_Broadcase req/ind */
};

}
