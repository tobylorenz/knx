// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

namespace KNX {

/**
 * State
 *
 * @ingroup KNX_03_03_04_05_01
 */
enum Connection_State {
    /** no connection */
    Closed,

    /** connection open */
    Open_Idle,

    /** waiting for T_ACK */
    Open_Wait,

    /** waiting for I_ACK */
    Connecting
};

}
