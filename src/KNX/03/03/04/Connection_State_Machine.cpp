// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/04/Connection_State_Machine.h>

#undef OUTPUT_ACTIONS

#include <cassert>
#ifdef OUTPUT_ACTIONS
#include <iostream>
#endif

namespace KNX {

Transport_Layer::Connection_State_Machine::Connection_State_Machine(Transport_Layer & transport_layer, asio::io_context & io_context, const TSAP_Connected & tsap, Transport_Layer::Connection_Style style) :
    transport_layer(&transport_layer),
    connection_address(tsap),
    connection_timeout_timer(new asio::steady_timer(io_context)),
    acknowledgement_timeout_timer(new asio::steady_timer(io_context)),
    style(style)
{
}

Transport_Layer::Connection_State_Machine::~Connection_State_Machine()
{
    connection_timeout_timer->cancel();
    delete connection_timeout_timer;
    connection_timeout_timer = nullptr;

    acknowledgement_timeout_timer->cancel();
    delete acknowledgement_timeout_timer;
    acknowledgement_timeout_timer = nullptr;
}

void Transport_Layer::Connection_State_Machine::T_Connect_req(const Individual_Address destination_address, const Priority priority)
{
    assert(destination_address == connection_address);

    process_event(E25({destination_address, priority}));
}

void Transport_Layer::Connection_State_Machine::T_Disconnect_req(const Priority priority, const TSAP_Connected tsap)
{
    assert(tsap == connection_address);

    process_event(E26({priority}));
}

void Transport_Layer::Connection_State_Machine::T_Data_Connected_req(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)
{
    assert(tsap == connection_address);

    process_event(E15({priority, tsdu}));
}

void Transport_Layer::Connection_State_Machine::N_Data_Individual_con(const Ack_Request /*ack_request*/, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, Status n_status)
{
    if (nsdu->data_control_flag == Data_Control_Flag::Data) {
        if (nsdu->numbered == Numbered::Has_No_SeqNo) {
            // T_Data_Individual_con
        } else {
            // T_Data_Connected_con
            process_event(E22({hop_count_type, priority}));
        }
    } else {
        switch (nsdu->control) {
        case TPDU::Control::T_Connect:
            // T_Connect_con
            if (n_status == Status::ok) {
                process_event(E19({destination_address, hop_count_type, priority}));
            } else {
                process_event(E20({hop_count_type, priority}));
            }
            break;
        case TPDU::Control::T_Disconnect:
            // T_Disconnect_con
            process_event(E21({hop_count_type, priority}));
            break;
        case TPDU::Control::T_ACK:
            // T_ACK_con
            process_event(E23({hop_count_type, priority}));
            break;
        case TPDU::Control::T_NAK:
            // T_NAK_con
            process_event(E24({hop_count_type, priority}));
            break;
        }
    }
}

void Transport_Layer::Connection_State_Machine::N_Data_Individual_ind(const Individual_Address /*destination_address*/, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)
{
    assert(transport_layer);

    if (nsdu->data_control_flag == Data_Control_Flag::Data) {
        if (nsdu->numbered == Numbered::Has_No_SeqNo) {
            // T_Data_Individual_ind
            assert(false);
        } else {
            // T_Data_Connected_ind
            if (source_address == connection_address) {
                std::shared_ptr<T_Data_Connected_PDU> tpdu = std::dynamic_pointer_cast<T_Data_Connected_PDU>(nsdu);
                if (tpdu->sequence_number == sequence_number_receive) {
                    process_event(E04({hop_count_type, priority, source_address, tpdu->tsdu}));
                } else {
                    if (tpdu->sequence_number == ((sequence_number_receive - 1) & 0x0f)) {
                        process_event(E05({hop_count_type, priority, source_address, tpdu->sequence_number}));
                    } else {
                        process_event(E06({hop_count_type, priority, source_address, tpdu->sequence_number}));
                    }
                }
            } else {
                process_event(E07({hop_count_type, priority, source_address}));
            }
        }
    } else {
        switch (nsdu->control) {
        case TPDU::Control::T_Connect:
            // T_Connect_ind
            if (source_address == connection_address) {
                process_event(E00({hop_count_type, priority, source_address}));
            } else {
                process_event(E01({hop_count_type, priority, source_address}));
            }
            break;
        case TPDU::Control::T_Disconnect:
            // T_Disconnect_ind
            if (source_address == connection_address) {
                process_event(E02({hop_count_type, priority, source_address}));
            } else {
                process_event(E03({hop_count_type, priority, source_address}));
            }
            break;
        case TPDU::Control::T_ACK:
            // T_ACK_ind
            if (source_address == connection_address) {
                std::shared_ptr<T_ACK_PDU> tpdu = std::dynamic_pointer_cast<T_ACK_PDU>(nsdu);
                if (tpdu->sequence_number == sequence_number_send) {
                    process_event(E08({hop_count_type, priority, source_address}));
                } else {
                    process_event(E09({hop_count_type, priority, source_address}));
                }
            } else {
                process_event(E10({hop_count_type, priority, source_address}));
            }
            break;
        case TPDU::Control::T_NAK:
            // T_NAK_ind
            if (source_address == connection_address) {
                std::shared_ptr<T_NAK_PDU> tpdu = std::dynamic_pointer_cast<T_NAK_PDU>(nsdu);
                process_event(E11b({hop_count_type, priority, source_address}));
                if (tpdu->sequence_number != sequence_number_send) {
                    process_event(E11({hop_count_type, priority, source_address}));
                } else {
                    if (rep_count < transport_layer->max_rep_count) {
                        process_event(E12({hop_count_type, priority, source_address}));
                    } else {
                        process_event(E13({hop_count_type, priority, source_address}));
                    }
                }
            } else {
                process_event(E14({hop_count_type, priority, source_address}));
            }
            break;
        }
    }
}

void Transport_Layer::Connection_State_Machine::connection_timeout_timer_restart()
{
    assert(connection_timeout_timer);

    connection_timeout_timer->expires_after(transport_layer->connection_timeout); // cancel pending timers
    connection_timeout_timer->async_wait(
        std::bind(&Transport_Layer::Connection_State_Machine::connection_timeout_timer_expired, this, std::placeholders::_1));
}

void Transport_Layer::Connection_State_Machine::connection_timeout_timer_stop()
{
    assert(connection_timeout_timer);

    connection_timeout_timer->cancel();
}

void Transport_Layer::Connection_State_Machine::connection_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // success (timer expired)
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    process_event(E16());
}

void Transport_Layer::Connection_State_Machine::acknowledgement_timeout_timer_restart()
{
    assert(acknowledgement_timeout_timer);

    acknowledgement_timeout_timer->expires_after(transport_layer->acknowledgement_timeout); // cancel pending timers
    acknowledgement_timeout_timer->async_wait(
        std::bind(&Transport_Layer::Connection_State_Machine::acknowledgement_timeout_timer_expired, this, std::placeholders::_1));
}

void Transport_Layer::Connection_State_Machine::acknowledgement_timeout_timer_stop()
{
    assert(acknowledgement_timeout_timer);

    acknowledgement_timeout_timer->cancel();
}

void Transport_Layer::Connection_State_Machine::acknowledgement_timeout_timer_expired(const std::error_code & error)
{
    assert(transport_layer);

    switch (error.value()) {
    case 0: // success (timer expired)
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (rep_count < transport_layer->max_rep_count) {
        process_event(E17({}));
    } else  {
        process_event(E18({}));
    }
}

/* 5.3 Actions */

void Transport_Layer::Connection_State_Machine::A0()
{
    last_action = 0;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    // Do nothing
};

void Transport_Layer::Connection_State_Machine::A1(const Individual_Address source_address)
{
    last_action = 1;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Connection_address = source address of received message
    connection_address = source_address;

    // Send a T_CONNECT_ind to the user.
    const TSAP_Connected tsap = connection_address;
    transport_layer->T_Connect_ind(tsap);

    // SeqNoSend=0; SeqNoRcv=0;
    sequence_number_send = 0;
    sequence_number_receive = 0;

    // Start connection timeout timer
    connection_timeout_timer_restart();
};

void Transport_Layer::Connection_State_Machine::A2(const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu)
{
    last_action = 2;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a N_Data_Individual.req with
    //   T_ACK_PDU,
    //   priority=SYSTEM,
    //   destination=connection_address,
    //   sequence=SeqNoRcv
    // to the Network Layer (remote device).
    std::shared_ptr<T_ACK_PDU> tpdu = std::make_shared<T_ACK_PDU>();
    tpdu->sequence_number = sequence_number_receive;
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, connection_address, hop_count_type, Priority::system, tpdu);

    // Increment the SeqNoRcv.
    sequence_number_receive = (sequence_number_receive + 1) & 0x0f;

    // Send the received buffer as a T_Data_Connected.ind to the user.
    TSAP_Connected tsap = connection_address;
    transport_layer->T_Data_Connected_ind(hop_count_type, priority, tsap, tsdu);

    // Restart the connection timeout timer.
    connection_timeout_timer_restart();
};

void Transport_Layer::Connection_State_Machine::A3(const Hop_Count_Type hop_count_type, const Sequence_Number sequence_number)
{
    last_action = 3;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a N_Data_Individual.req with
    //   T_ACK_PDU,
    //   priority=SYSTEM,
    //   destination=connection_address,
    //   sequence=sequence of received message
    // to the Network Layer (remote device).
    std::shared_ptr<T_ACK_PDU> tpdu = std::make_shared<T_ACK_PDU>();
    tpdu->sequence_number = sequence_number;
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, connection_address, hop_count_type, Priority::system, tpdu);

    // Restart the connection timeout timer.
    connection_timeout_timer_restart();
};

void Transport_Layer::Connection_State_Machine::A4(const Hop_Count_Type hop_count_type, const Sequence_Number sequence_number)
{
    last_action = 4;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a N_Data_Individual.req with
    //   T_NAK_PDU,
    //   priority=SYSTEM,
    //   destination=connection_address,
    //   sequence=sequence of received message
    // to the Network Layer (remote device).
    std::shared_ptr<T_NAK_PDU> tpdu = std::make_shared<T_NAK_PDU>();
    tpdu->sequence_number = sequence_number;
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, connection_address, hop_count_type, Priority::system, tpdu);

    // Restart the connection timeout timer.
    connection_timeout_timer_restart();
};

void Transport_Layer::Connection_State_Machine::A5()
{
    last_action = 5;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a T_Disconnect.ind to the user.
    transport_layer->T_Disconnect_ind(connection_address);

    // Stop the acknowledge timeout timer.
    acknowledgement_timeout_timer_stop();

    // Stop the connection timeout timer.
    connection_timeout_timer_stop();
};

void Transport_Layer::Connection_State_Machine::A6()
{
    last_action = 6;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a N_Data_Individual.req with
    //   T_DISCONNECT_REQ_PDU,
    //   priority=SYSTEM,
    //   destination=connection_address,
    //   sequence=0
    // to the Network Layer (remote device).
    std::shared_ptr<T_Disconnect_PDU> tpdu = std::make_shared<T_Disconnect_PDU>();
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, connection_address, Network_Layer_Parameter, Priority::system, tpdu);

    // Send a T_Disconnect.ind to the user.
    TSAP_Connected tsap = connection_address;
    transport_layer->T_Disconnect_ind(tsap);

    // Stop the acknowledge timeout timer.
    acknowledgement_timeout_timer_stop();

    // Stop the connection timeout timer.
    connection_timeout_timer_stop();
};

void Transport_Layer::Connection_State_Machine::A7(const Priority priority, std::shared_ptr<TSDU> tsdu)
{
    last_action = 7;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Store the received T_Data_Connected.req and send as a N_Data_Individual.req with
    //   T_DATA_CONNECTED_REQ_PDU,
    //   destination=connection_address,
    //   sequence=SeqNoSend
    // to the Network Layer (remote device).
    std::shared_ptr<T_Data_Connected_PDU> tpdu = std::make_shared<T_Data_Connected_PDU>();
    tpdu->tsdu = tsdu;
    tpdu->sequence_number = sequence_number_send;
    current_transmission.tpdu = tpdu;
    current_transmission.priority = priority;
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, connection_address, Network_Layer_Parameter, priority, tpdu);

    // Clear the rep_count.
    rep_count = 0;

    // Start the acknowledge timeout timer.
    acknowledgement_timeout_timer_restart();

    // Restart the connection timeout timer.
    connection_timeout_timer_restart();
};

void Transport_Layer::Connection_State_Machine::A8()
{
    last_action = 8;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Stop the acknowledge timeout timer.
    acknowledgement_timeout_timer_stop();

    // Increment the SeqNoSend.
    sequence_number_send = (sequence_number_send + 1) & 0x0f;

    // Send the stored buffer as a T_Data_Connected.con with
    //   cleared errorbits,
    //   connection_number=0
    // to the user.
    std::shared_ptr<T_Data_Connected_PDU> tpdu2 = std::dynamic_pointer_cast<T_Data_Connected_PDU>(current_transmission.tpdu);
    transport_layer->T_Data_Connected_con(current_transmission.priority, connection_address, tpdu2->tsdu); // @note Spec says 0, but connection_address is more appropriate.

    // Restart the connection timeout timer.
    connection_timeout_timer_restart();
};

void Transport_Layer::Connection_State_Machine::A8b()
{
    last_action = 8;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Stop the acknowledge timeout timer.
    acknowledgement_timeout_timer_stop();

    // Increment the SeqNoSend.
    sequence_number_send = (sequence_number_send + 1) & 0x0f;

    // Restart the connection timeout timer.
    connection_timeout_timer_restart();
};

void Transport_Layer::Connection_State_Machine::A9()
{
    last_action = 9;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send the stored message as a N_Data_Individual.req to the Network Layer (remote device).
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, connection_address, Network_Layer_Parameter, current_transmission.priority, current_transmission.tpdu);

    // Increment the rep_count.
    rep_count++;

    // Start the acknowledge timeout timer.
    acknowledgement_timeout_timer_restart();

    // Restart the connection timeout timer.
    connection_timeout_timer_restart();
};

void Transport_Layer::Connection_State_Machine::A10(const Individual_Address source_address)
{
    last_action = 10;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a N_Data_Individual.req with
    //   T_DISCONNECT_REQ_PDU
    //   Priority=System,
    //   Destination=source (rbuffer),
    //   sequence = 0
    // back to the sender.
    std::shared_ptr<T_Disconnect_PDU> tpdu = std::make_shared<T_Disconnect_PDU>();
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, source_address, Network_Layer_Parameter, Priority::system, tpdu);
};

void Transport_Layer::Connection_State_Machine::A11(const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/)
{
    last_action = 11;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    // Store event back and handle after next event.
    // Don't change order of T_Data_Connected.req events
    //    if (tsdu) {
    //        Stored_TPDU tpdu;
    //        tpdu.priority = priority;
    //        tpdu.tpdu = std::make_shared<T_Data_Connected_PDU>();
    //        tpdu.tpdu->tsdu = tsdu;
    //        transmit_queue.push_back(tpdu);
    //    }
    // @todo Not used in Style 1
};

void Transport_Layer::Connection_State_Machine::A12(const Individual_Address destination_address, const Priority priority)
{
    last_action = 12;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // connection_address = address from T_CONNECT_req
    connection_address = destination_address;

    // send N_Data_Individual.req with
    //   T_CONNECT_REQ_PDU
    std::shared_ptr<T_Connect_PDU> tpdu = std::make_shared<T_Connect_PDU>();
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, connection_address, Network_Layer_Parameter, priority, tpdu);

    // SeqNoSend=0; SeqNoRcv=0;
    sequence_number_send = 0;
    sequence_number_receive = 0;

    // Start connection timeout timer
    connection_timeout_timer_restart();
};

void Transport_Layer::Connection_State_Machine::A13(const Individual_Address destination_address)
{
    last_action = 13;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a T_Connect.con to the user.
    const TSAP_Connected tsap = connection_address;
    transport_layer->T_Connect_con(destination_address, tsap, Status::ok);
};

void Transport_Layer::Connection_State_Machine::A14(const Priority priority)
{
    last_action = 14;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a N_Data_Individual.req with
    //   T_DISCONNECT_REQ_PDU,
    //   priority=SYSTEM,
    //   destination=connection_address,
    //   sequence=0
    // to the Network Layer (remote device).
    std::shared_ptr<T_Disconnect_PDU> tpdu = std::make_shared<T_Disconnect_PDU>();
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, connection_address, Network_Layer_Parameter, Priority::system, tpdu);

    // Send a T_Disconnect.con to the user.
    const TSAP_Connected tsap = connection_address;
    transport_layer->T_Disconnect_con(priority, tsap, Status::ok);

    // Stop the acknowledge timeout timer.
    acknowledgement_timeout_timer_stop();

    // Stop the connection timeout timer.
    connection_timeout_timer_stop();
};

void Transport_Layer::Connection_State_Machine::A14b()
{
    last_action = 14;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a N_Data_Individual.req with
    //   T_DISCONNECT_REQ_PDU,
    //   priority=SYSTEM,
    //   destination=connection_address,
    //   sequence=0
    // to the Network Layer (remote device).
    std::shared_ptr<T_Disconnect_PDU> tpdu = std::make_shared<T_Disconnect_PDU>();
    transport_layer->N_Data_Individual_req(Ack_Request::dont_care, connection_address, Network_Layer_Parameter, Priority::system, tpdu);

    // Stop the acknowledge timeout timer.
    acknowledgement_timeout_timer_stop();

    // Stop the connection timeout timer.
    connection_timeout_timer_stop();
};

void Transport_Layer::Connection_State_Machine::A15(const Priority priority)
{
    last_action = 15;
#ifdef OUTPUT_ACTIONS
    std::cerr << "S" << static_cast<uint16_t>(state) << " E" << static_cast<uint16_t>(last_event) << " A" << static_cast<uint16_t>(last_action) << std::endl;
#endif

    assert(transport_layer);

    // Send a T_Disconnect.con to the management user
    const TSAP_Connected tsap = connection_address;
    transport_layer->T_Disconnect_con(priority, tsap, Status::ok);

    // Stop the acknowledge timeout timer.
    acknowledgement_timeout_timer_stop();

    // Stop the connection timeout timer.
    connection_timeout_timer_stop();
};

/* Guards */

bool Transport_Layer::Connection_State_Machine::G_client_accepts_connection() const
{
    assert(transport_layer);

    return transport_layer->client_accepts_connection;
}

//void Transport_Layer::Connection_State_Machine::process_event() {
//    last_event = 0;

//    switch(state) {
//    case Connection_State::Closed:
//        break;
//    case Connection_State::Open_Idle:
//        break;
//    case Connection_State::Open_Wait:
//        break;
//    case Connection_State::Connecting:
//        break;
//    }
//}

}
