// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <deque>

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/02/Priority.h>
#include <KNX/03/03/04/Connection_State.h>
#include <KNX/03/03/04/Sequence_Number.h>
#include <KNX/03/03/04/T_ACK/T_ACK_PDU.h>
#include <KNX/03/03/04/T_Connect/T_Connect_PDU.h>
#include <KNX/03/03/04/T_Data_Connected/T_Data_Connected_PDU.h>
#include <KNX/03/03/04/T_Disconnect/T_Disconnect_PDU.h>
#include <KNX/03/03/04/T_NAK/T_NAK_PDU.h>
#include <KNX/03/03/04/Transport_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Connection State Machine
 *
 * @ingroup KNX_03_03_04_05
 */
class KNX_EXPORT Transport_Layer::Connection_State_Machine
{
public:
    Connection_State_Machine(Transport_Layer & transport_layer, asio::io_context & io_context, const TSAP_Connected & tsap, const Transport_Layer::Connection_Style style);
    virtual ~Connection_State_Machine();

    /* T_Connect service */

    virtual void T_Connect_req(const Individual_Address destination_address, const Priority priority);

    /* T_Disconnect service */

    virtual void T_Disconnect_req(const Priority priority, const TSAP_Connected tsap);

    /* T_Data_Connected service */

    virtual void T_Data_Connected_req(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu);

    /* N_Data_Individual service */

    virtual void N_Data_Individual_con(const Ack_Request ack_request, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, Status n_status);
    virtual void N_Data_Individual_ind(const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu);

    /* Connection_Time_Out service */
    void connection_timeout_timer_restart();
    void connection_timeout_timer_stop();
    void connection_timeout_timer_expired(const std::error_code & error);

    /* Acknowledge_Time_Out service */
    void acknowledgement_timeout_timer_restart();
    void acknowledgement_timeout_timer_stop();
    void acknowledgement_timeout_timer_expired(const std::error_code & error);

    /** transport layer */
    Transport_Layer * transport_layer{};

    /* 5 Parameters */

    Individual_Address connection_address{};
    Sequence_Number sequence_number_send{};
    Sequence_Number sequence_number_receive{};
    asio::steady_timer * connection_timeout_timer{}; // @see Transport_Layer::connection_timeout
    asio::steady_timer * acknowledgement_timeout_timer{}; // @see Transport_Layer::acknowledgement_timeout
    uint8_t rep_count{}; // @see Transport_Layer::max_rep_count

    /**
     * stored T_Data_Connected for
     *
     * - A7: store the received T_Data_Connected.req and send as a N_Data_Individual.req
     * - A8: send the stored buffer as a T_Data_Connected.con
     * - A9: send the stored message as a N_Data_Individual.req
     * - A11: store event back and handle after next event
     */
    struct Stored_TPDU {
        Priority priority;
        std::shared_ptr<TPDU> tpdu;
    };

    /** current TPDU */
    Stored_TPDU current_transmission{};

    /** last event */
    uint8_t last_event{};

    /** last action */
    uint8_t last_action{};

    /** Connection Style */
    const Transport_Layer::Connection_Style style;

    /* 5.1 States */

    /** @copydoc Connection_State */
    Connection_State state{Connection_State::Closed};

protected:

    /* 5.2 Events */

    struct E00 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_CONNECT_REQ_PDU
        // (source_address == connection_address)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E01 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_CONNECT_REQ_PDU
        // (source_address != connection_address)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E02 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_DISCONNECT_REQ_PDU
        // (source_address == connection_address)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E03 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_DISCONNECT_REQ_PDU
        // (source_address != connection_address)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E04 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_DATA_CONNECTED_REQ_PDU
        // (source_address == connection_address) and
        // (SeqNo_of_PDU == SeqNoRcv)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
        std::shared_ptr<TSDU> tsdu;
    };

    struct E05 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_DATA_CONNECTED_REQ_PDU
        // (source_address == connection_address) and
        // (SeqNo_of_PDU == ((SeqNoRcv - 1) & 0x0f))
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
        Sequence_Number sequence_number;
    };

    struct E06 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_DATA_CONNECTED_REQ_PDU
        // (source_address == connection_address) and
        // (SeqNo_of_PDU != SeqNoRcv) and
        // (SeqNo_of_PDU != ((SeqNoRcv - 1) & 0x0f))
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
        Sequence_Number sequence_number;
    };

    struct E07 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_DATA_CONNECTED_REQ_PDU
        // (source_address != connection_address)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E08 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_ACK_PDU
        // (source_address == connection_address) and
        // (SeqNo_of_PDU == SeqNoSend)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E09 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_ACK_PDU
        // (source_address == connection_address) and
        // (SeqNo_of_PDU != SeqNoSend)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E10 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_ACK_PDU
        // (source_address != connection_address)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E11 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_NAK_PDU
        // (source_address == connection_address) and
        // (SeqNo_of_PDU != SeqNoSend)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E11b {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_NAK_PDU
        // (source_address == connection_address)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E12 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_NAK_PDU
        // (source_address == connection_address) and
        // (SeqNo_of_PDU == SeqNoSend) and
        // (rep_count < max_rep_count)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E13 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_NAK_PDU
        // (source_address == connection_address) and
        // (SeqNo_of_PDU == SeqNoSend) and
        // (rep_count >= max_rep_count)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E14 {
        // N_DATA_INDIVIDUAL_ind(destination_address, hop_count_type, priority, source_address, nsdu)
        // T_NAK_PDU
        // (source_address != connection_address)
        Hop_Count_Type hop_count_type;
        Priority priority;
        Individual_Address source_address;
    };

    struct E15 {
        // T_DATA_CONNECTED_req(priority, tsap, tsdu)
        Priority priority;
        std::shared_ptr<TSDU> tsdu;
    };

    struct E16 {
        // CONNECTION_TIME_OUT_ind
    };

    struct E17 {
        // ACKNOWLEDGE_TIME_OUT_ind
        // (rep_count < max_rep_count)
    };

    struct E18 {
        // ACKNOWLEDGE_TIME_OUT_ind
        // (rep_count >= max_rep_count)
    };

    struct E19 {
        // N_DATA_INDIVIDUAL_con(ack_request, destination_address, hop_count_type, priority, nsdu, n_status)
        // T_CONNECT_REQ_PDU
        // IAK=OK (client only)
        Individual_Address destination_address;
        Hop_Count_Type hop_count_type;
        Priority priority;
    };

    struct E20 {
        // N_DATA_INDIVIDUAL_con(ack_request, destination_address, hop_count_type, priority, nsdu, n_status)
        // T_CONNECT_REQ_PDU
        // IAK=NOT OK (client only)
        Hop_Count_Type hop_count_type;
        Priority priority;
    };

    struct E21 {
        // N_DATA_INDIVIDUAL_con(ack_request, destination_address, hop_count_type, priority, nsdu, n_status)
        // T_DISCONNECT_REQ_PDU
        Hop_Count_Type hop_count_type;
        Priority priority;
    };

    struct E22 {
        // N_DATA_INDIVIDUAL_con(ack_request, destination_address, hop_count_type, priority, nsdu, n_status)
        // T_DATA_CONNECTED_REQ_PDU
        Hop_Count_Type hop_count_type;
        Priority priority;
    };

    struct E23 {
        // N_DATA_INDIVIDUAL_con(ack_request, destination_address, hop_count_type, priority, nsdu, n_status)
        // T_ACK_PDU
        Hop_Count_Type hop_count_type;
        Priority priority;
    };

    struct E24 {
        // N_DATA_INDIVIDUAL_con(ack_request, destination_address, hop_count_type, priority, nsdu, n_status)
        // T_NAK_PDU
        Hop_Count_Type hop_count_type;
        Priority priority;
    };

    struct E25 {
        // T_CONNECT_req(destination_address, priority) (client only)
        Individual_Address destination_address;
        Priority priority;
    };

    struct E26 {
        // T_DISCONNECT_req(priority, tsap) (client only)
        Priority priority;
    };

    struct E27 {
        // all other
    };

    /* 5.3 Actions */

    void A0();
    void A1(const Individual_Address source_address);
    void A2(const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu);
    void A3(const Hop_Count_Type hop_count_type, const Sequence_Number sequence_number);
    void A4(const Hop_Count_Type hop_count_type, const Sequence_Number sequence_number);
    void A5();
    void A6();
    void A7(const Priority priority, std::shared_ptr<TSDU> tsdu);
    void A8();
    void A8b();
    void A9();
    void A10(const Individual_Address source_address);
    void A11(const Priority priority, std::shared_ptr<TSDU> tsdu);
    void A12(const Individual_Address destination_address, const Priority priority);
    void A13(const Individual_Address destination_address);
    void A14(const Priority priority);
    void A14b();
    void A15(const Priority priority);

    /* Guards */

    bool G_client_accepts_connection() const;

    /* Transition Table */

    //    virtual void process_event();
    virtual void process_event(const E00 & evt) = 0;
    virtual void process_event(const E01 & evt) = 0;
    virtual void process_event(const E02 & evt) = 0;
    virtual void process_event(const E03 & evt) = 0;
    virtual void process_event(const E04 & evt) = 0;
    virtual void process_event(const E05 & evt) = 0;
    virtual void process_event(const E06 & evt) = 0;
    virtual void process_event(const E07 & evt) = 0;
    virtual void process_event(const E08 & evt) = 0;
    virtual void process_event(const E09 & evt) = 0;
    virtual void process_event(const E10 & evt) = 0;
    virtual void process_event(const E11 & evt) = 0;
    virtual void process_event(const E11b & evt) = 0;
    virtual void process_event(const E12 & evt) = 0;
    virtual void process_event(const E13 & evt) = 0;
    virtual void process_event(const E14 & evt) = 0;
    virtual void process_event(const E15 & evt) = 0;
    virtual void process_event(const E16 & evt) = 0;
    virtual void process_event(const E17 & evt) = 0;
    virtual void process_event(const E18 & evt) = 0;
    virtual void process_event(const E19 & evt) = 0;
    virtual void process_event(const E20 & evt) = 0;
    virtual void process_event(const E21 & evt) = 0;
    virtual void process_event(const E22 & evt) = 0;
    virtual void process_event(const E23 & evt) = 0;
    virtual void process_event(const E24 & evt) = 0;
    virtual void process_event(const E25 & evt) = 0;
    virtual void process_event(const E26 & evt) = 0;
    virtual void process_event(const E27 & evt) = 0;
};

}
