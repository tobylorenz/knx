// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/04/Connection_State_Machine.h>

namespace KNX {

/**
 * Connection State Machine - Style 1
 */
class Transport_Layer::Connection_State_Machine_Style_1 :
    public Transport_Layer::Connection_State_Machine
{
public:
    Connection_State_Machine_Style_1(Transport_Layer & transport_layer, asio::io_context & io_context, const TSAP_Connected & tsap);

    void process_event(const E00 & evt) override;
    void process_event(const E01 & evt) override;
    void process_event(const E02 & evt) override;
    void process_event(const E03 & evt) override;
    void process_event(const E04 & evt) override;
    void process_event(const E05 & evt) override;
    void process_event(const E06 & evt) override;
    void process_event(const E07 & evt) override;
    void process_event(const E08 & evt) override;
    void process_event(const E09 & evt) override;
    void process_event(const E10 & evt) override;
    void process_event(const E11 & evt) override;
    void process_event(const E11b & evt) override;
    void process_event(const E12 & evt) override;
    void process_event(const E13 & evt) override;
    void process_event(const E14 & evt) override;
    void process_event(const E15 & evt) override;
    void process_event(const E16 & evt) override;
    void process_event(const E17 & evt) override;
    void process_event(const E18 & evt) override;
    void process_event(const E19 & evt) override;
    void process_event(const E20 & evt) override;
    void process_event(const E21 & evt) override;
    void process_event(const E22 & evt) override;
    void process_event(const E23 & evt) override;
    void process_event(const E24 & evt) override;
    void process_event(const E25 & evt) override;
    void process_event(const E26 & evt) override;
    void process_event(const E27 & evt) override;
};

}
