// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/04/Connection_State_Machine_Style_3.h>

namespace KNX {

Transport_Layer::Connection_State_Machine_Style_3::Connection_State_Machine_Style_3(Transport_Layer & transport_layer, asio::io_context & io_context, const TSAP_Connected & tsap) :
    Transport_Layer::Connection_State_Machine(transport_layer, io_context, tsap, Style_3)
{
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E00 & evt)
{
    last_event = 0;

    switch (state) {
    case Connection_State::Closed:
        A1(evt.source_address);
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A0();
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E01 & evt)
{
    last_event = 1;

    switch (state) {
    case Connection_State::Closed:
        A1(evt.source_address);
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Idle:
        A10(evt.source_address);
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A10(evt.source_address);
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A10(evt.source_address);
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E02 & /*evt*/)
{
    last_event = 2;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A5();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Wait:
        A5();
        state = Connection_State::Closed;
        break;
    case Connection_State::Connecting:
        A5();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E03 & /*evt*/)
{
    last_event = 3;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A0();
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E04 & evt)
{
    last_event = 4;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A2(evt.hop_count_type, evt.priority, evt.tsdu);
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A2(evt.hop_count_type, evt.priority, evt.tsdu);
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A6();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E05 & evt)
{
    last_event = 5;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A3(evt.hop_count_type, evt.sequence_number);
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A3(evt.hop_count_type, evt.sequence_number);
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A3(evt.hop_count_type, evt.sequence_number);
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E06 & evt)
{
    last_event = 6;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A4(evt.hop_count_type, evt.sequence_number);
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A4(evt.hop_count_type, evt.sequence_number);
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A6();
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E07 & evt)
{
    last_event = 7;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A10(evt.source_address);
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E08 & /*evt*/)
{
    last_event = 8;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A8();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Connecting:
        A6();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E09 & /*evt*/)
{
    last_event = 9;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A6();
        state = Connection_State::Closed;
        break;
    case Connection_State::Connecting:
        A6();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E10 & evt)
{
    last_event = 10;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A10(evt.source_address);
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E11 & /*evt*/)
{
    last_event = 11;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A6();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E11b & /*evt*/)
{
    last_event = 11;

    switch (state) {
    case Connection_State::Closed:
        break;
    case Connection_State::Open_Idle:
        break;
    case Connection_State::Open_Wait:
        break;
    case Connection_State::Connecting:
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E12 & /*evt*/)
{
    last_event = 12;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A6();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Wait:
        A9();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A6();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E13 & /*evt*/)
{
    last_event = 13;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A6();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Wait:
        A6();
        state = Connection_State::Closed;
        break;
    case Connection_State::Connecting:
        A6();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E14 & evt)
{
    last_event = 14;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A10(evt.source_address);
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E15 & evt)
{
    last_event = 15;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A7(evt.priority, evt.tsdu);
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Open_Wait:
        A11(evt.priority, evt.tsdu);
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A11(evt.priority, evt.tsdu);
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E16 & /*evt*/)
{
    last_event = 16;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A6();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Wait:
        A6();
        state = Connection_State::Closed;
        break;
    case Connection_State::Connecting:
        A6();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E17 & /*evt*/)
{
    last_event = 17;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A9();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A0();
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E18 & /*evt*/)
{
    last_event = 18;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A6();
        state = Connection_State::Closed;
        break;
    case Connection_State::Connecting:
        A0();
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E19 & evt)
{
    last_event = 19;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A13(evt.destination_address);
        state = Connection_State::Open_Idle;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E20 & /*evt*/)
{
    last_event = 20;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A5();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E21 & /*evt*/)
{
    last_event = 21;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A0();
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E22 & /*evt*/)
{
    last_event = 22;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A0();
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E23 & /*evt*/)
{
    last_event = 23;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A0();
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E24 & /*evt*/)
{
    last_event = 24;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A0();
        state = Connection_State::Connecting;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E25 & evt)
{
    last_event = 25;

    switch (state) {
    case Connection_State::Closed:
        A12(evt.destination_address, evt.priority);
        state = Connection_State::Connecting;
        break;
    case Connection_State::Open_Idle:
        A6();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Wait:
        A6();
        state = Connection_State::Closed;
        break;
    case Connection_State::Connecting:
        A6();
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E26 & evt)
{
    last_event = 26;

    switch (state) {
    case Connection_State::Closed:
        A15(evt.priority);
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A14(evt.priority);
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Wait:
        A14(evt.priority);
        state = Connection_State::Closed;
        break;
    case Connection_State::Connecting:
        A14(evt.priority);
        state = Connection_State::Closed;
        break;
    }
}

void Transport_Layer::Connection_State_Machine_Style_3::process_event(const Transport_Layer::Connection_State_Machine::E27 & /*evt*/)
{
    last_event = 27;

    switch (state) {
    case Connection_State::Closed:
        A0();
        state = Connection_State::Closed;
        break;
    case Connection_State::Open_Idle:
        A0();
        state = Connection_State::Open_Idle;
        break;
    case Connection_State::Open_Wait:
        A0();
        state = Connection_State::Open_Wait;
        break;
    case Connection_State::Connecting:
        A0();
        state = Connection_State::Connecting;
        break;
    }
}

}
