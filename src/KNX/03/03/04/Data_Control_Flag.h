// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Data/Control Flag
 *
 * @ingroup KNX_03_03_04_02
 */
enum class Data_Control_Flag : uint1_t {
    /**
     * Data frame
     * - T_Data_Broadcast
     * - T_Data_Group
     * - T_Data_Tag_Group
     * - T_Data_Individual
     * - T_Data_Connected
     */
    Data = 0,

    /**
     * Control frame
     * - T_Connect
     * - T_Disconnect
     * - T_ACK
     * - T_NAK
     */
    Control = 1
};

}
