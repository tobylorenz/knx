// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Numbered
 *
 * @ingroup KNX_03_03_04_02
 */
enum class Numbered : uint1_t {
    /** has no sequence number */
    Has_No_SeqNo = 0,

    /** has sequence number */
    Has_SeqNo = 1
};

}
