// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * sequence number (SeqNo)
 *
 * @ingroup KNX_03_03_04_02
 */
using Sequence_Number = uint4_t;

}
