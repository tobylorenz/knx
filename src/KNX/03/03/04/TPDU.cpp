// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/04/TPDU.h>

#include <cassert>

#include <KNX/03/03/04/T_ACK/T_ACK_PDU.h>
#include <KNX/03/03/04/T_Connect/T_Connect_PDU.h>
#include <KNX/03/03/04/T_Data_Connected/T_Data_Connected_PDU.h>
#include <KNX/03/03/04/T_Data_Individual/T_Data_Individual_PDU.h>
#include <KNX/03/03/04/T_Disconnect/T_Disconnect_PDU.h>
#include <KNX/03/03/04/T_NAK/T_NAK_PDU.h>

namespace KNX {

void TPDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    /* octet 6 */
    const uint8_t tpci = *first; // no ++ as tsdu need to parse it as well
    data_control_flag = static_cast<Data_Control_Flag>(tpci >> 7);
    numbered = static_cast<Numbered>((tpci >> 6) & 0x01);
    sequence_number = (tpci >> 2) & 0x0f;
    control = static_cast<Control>(tpci & 0x03);
}

std::vector<uint8_t> TPDU::toData() const
{
    std::vector<uint8_t> data;

    /* octet 6 */
    data.push_back(
        (static_cast<uint8_t>(data_control_flag) << 7) |
        (static_cast<uint8_t>(numbered) << 6) |
        (sequence_number << 2) |
        ((data_control_flag == Data_Control_Flag::Control) ? static_cast<uint8_t>(control) : 0));

    return data;
}

uint8_t TPDU::length_calculated() const
{
    return 0;
}

std::shared_ptr<TPDU> make_TPDU_Individual(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 1);

    /* octet 6 */
    TPDU tpdu;
    tpdu.fromData(first, first + 1);

    /* octet 6..N */
    if (tpdu.data_control_flag == Data_Control_Flag::Data) {
        if (tpdu.numbered == Numbered::Has_No_SeqNo) {
            std::shared_ptr<T_Data_Individual_PDU> tpdu2 = std::make_shared<T_Data_Individual_PDU>();
            tpdu2->fromData(first, last);
            return tpdu2;
        } else {
            std::shared_ptr<T_Data_Connected_PDU> tpdu2 = std::make_shared<T_Data_Connected_PDU>();
            tpdu2->fromData(first, last);
            return tpdu2;
        }
    } else {
        switch (tpdu.control) {
        case TPDU::Control::T_Connect: {
            std::shared_ptr<T_Connect_PDU> tpdu2 = std::make_shared<T_Connect_PDU>();
            tpdu2->fromData(first, last);
            return tpdu2;
        }
        break;
        case TPDU::Control::T_Disconnect: {
            std::shared_ptr<T_Disconnect_PDU> tpdu2 = std::make_shared<T_Disconnect_PDU>();
            tpdu2->fromData(first, last);
            return tpdu2;
        }
        break;
        case TPDU::Control::T_ACK: {
            std::shared_ptr<T_ACK_PDU> tpdu2 = std::make_shared<T_ACK_PDU>();
            tpdu2->fromData(first, last);
            return tpdu2;
        }
        break;
        case TPDU::Control::T_NAK: {
            std::shared_ptr<T_NAK_PDU> tpdu2 = std::make_shared<T_NAK_PDU>();
            tpdu2->fromData(first, last);
            return tpdu2;
        }
        break;
        }
    }
    return std::make_shared<TPDU>(tpdu);
}

}
