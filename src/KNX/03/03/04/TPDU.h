// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/03/04/Data_Control_Flag.h>
#include <KNX/03/03/04/Numbered.h>
#include <KNX/03/03/04/Sequence_Number.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/** TPDU */
class KNX_EXPORT TPDU
{
public:
    bool operator==(const TPDU & other) const = default;

    /**
     * from data
     *
     * @param[in] first octet 6
     * @param[in] last octet N
     */
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

    /**
     * to data
     *
     * @return octet 6..N
     */
    virtual std::vector<uint8_t> toData() const;

    /** calculate Length (L) */
    virtual uint8_t length_calculated() const;

    /** Data/Control Flag */
    Data_Control_Flag data_control_flag{Data_Control_Flag::Data};

    /** Numbered */
    Numbered numbered{Numbered::Has_No_SeqNo};

    /** Sequence Number (SeqNo) */
    Sequence_Number sequence_number{};

    /** Control */
    enum class Control : uint8_t { // actually uint2_t
        T_Connect = 0,
        T_Disconnect = 1,
        T_ACK = 2,
        T_NAK = 3
    };

    /** @copydoc Control */
    Control control{Control::T_Connect};
};

/**
 * make TPDU from N_Data_Individual (address_type=0)
 *
 * @param[in] first octet 6
 * @param[in] last octet N
 * @return T_Data_Individual, T_Data_Connected, T_Connect, T_Disconnect, T_ACK, T_NAK
 */
KNX_EXPORT std::shared_ptr<TPDU> make_TPDU_Individual(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
