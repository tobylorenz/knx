// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Group_Address.h>
#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/types.h>

namespace KNX {

/* Transport Layer Service Access Point (TSAP) */

/** Transport Layer Service Access Point (TSAP) for Group */
using TSAP_Group = Group_Address;

/** Transport Layer Service Access Point (TSAP) for Individual */
using TSAP_Individual = Individual_Address;

/** Transport Layer Service Access Point (TSAP) for Connected */
using TSAP_Connected = Individual_Address; // @todo identifier of the Connection Address

}
