// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/04/T_ACK/T_ACK_PDU.h>

namespace KNX {

T_ACK_PDU::T_ACK_PDU() :
    TPDU()
{
    data_control_flag = Data_Control_Flag::Control;
    numbered = Numbered::Has_SeqNo;
    control = Control::T_ACK;
}

bool T_ACK_PDU::operator==(const T_ACK_PDU & other) const
{
    return TPDU::operator==(other);
}

}
