// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/04/TPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/** T_ACK */
class KNX_EXPORT T_ACK_PDU : public TPDU
{
public:
    T_ACK_PDU();

    bool operator==(const T_ACK_PDU & other) const;

protected:
    using TPDU::data_control_flag; // always 1
    using TPDU::numbered; // always 1
    using TPDU::control; // always 2
};

}
