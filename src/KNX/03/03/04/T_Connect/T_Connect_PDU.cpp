// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/04/T_Connect/T_Connect_PDU.h>

namespace KNX {

T_Connect_PDU::T_Connect_PDU() :
    TPDU()
{
    data_control_flag = Data_Control_Flag::Control;
}

bool T_Connect_PDU::operator==(const T_Connect_PDU & other) const
{
    return TPDU::operator==(other);
}

}
