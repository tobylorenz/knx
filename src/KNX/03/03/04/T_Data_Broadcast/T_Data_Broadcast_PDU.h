// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/04/TPDU.h>
#include <KNX/03/03/04/TSDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/** T_Data_Broadcast */
class KNX_EXPORT T_Data_Broadcast_PDU :
    public TPDU
{
public:
    T_Data_Broadcast_PDU();

    bool operator==(const T_Data_Broadcast_PDU & other) const;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** TSDU = APDU */
    std::shared_ptr<TSDU> tsdu{};

protected:
    using TPDU::data_control_flag; // always 0
    using TPDU::numbered; // always 0
    using TPDU::sequence_number; // always 0
    using TPDU::control; // has no control
};

}
