// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/04/T_Data_Individual/T_Data_Individual_PDU.h>

#include <KNX/03/03/07/APDU.h>

namespace KNX {

T_Data_Individual_PDU::T_Data_Individual_PDU() :
    TPDU()
{
}

bool T_Data_Individual_PDU::operator==(const T_Data_Individual_PDU & other) const
{
    return TPDU::operator==(other) && (*tsdu == *other.tsdu);
}

void T_Data_Individual_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 1);

    /* octet 6 */
    TPDU::fromData(first, first + 1);

    /* octet 6..N */
    tsdu = make_APDU(first, last);
}

std::vector<uint8_t> T_Data_Individual_PDU::toData() const
{
    assert(tsdu);

    std::vector<uint8_t> data;

    /* octet 6..N */
    data = tsdu->toData();

    // add TPCI data into octet 6
    data[0] |= TPDU::toData()[0] & 0xfc;

    return data;
}

uint8_t T_Data_Individual_PDU::length_calculated() const
{
    assert(tsdu);

    return tsdu->length_calculated();
}

}
