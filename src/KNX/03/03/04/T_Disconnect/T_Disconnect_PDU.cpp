// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/04/T_Disconnect/T_Disconnect_PDU.h>

namespace KNX {

T_Disconnect_PDU::T_Disconnect_PDU() :
    TPDU()
{
    data_control_flag = Data_Control_Flag::Control;
    control = Control::T_Disconnect;
}

bool T_Disconnect_PDU::operator==(const T_Disconnect_PDU & other) const
{
    return TPDU::operator==(other);
}

}
