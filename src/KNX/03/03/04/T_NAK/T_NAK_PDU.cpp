// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/04/T_NAK/T_NAK_PDU.h>

namespace KNX {

T_NAK_PDU::T_NAK_PDU() :
    TPDU()
{
    data_control_flag = Data_Control_Flag::Control;
    numbered = Numbered::Has_SeqNo;
    control = Control::T_NAK;
}

bool T_NAK_PDU::operator==(const T_NAK_PDU & other) const
{
    return TPDU::operator==(other);
}

}
