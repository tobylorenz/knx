// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/04/Transport_Layer.h>

#include <cassert>

#include <KNX/03/03/04/Connection_State_Machine.h>
#include <KNX/03/03/04/Connection_State_Machine_Style_1.h>
#include <KNX/03/03/04/Connection_State_Machine_Style_1_Rationalised.h>
#include <KNX/03/03/04/Connection_State_Machine_Style_2.h>
#include <KNX/03/03/04/Connection_State_Machine_Style_3.h>
#include <KNX/03/03/04/T_ACK/T_ACK_PDU.h>
#include <KNX/03/03/04/T_Connect/T_Connect_PDU.h>
#include <KNX/03/03/04/T_Data_Broadcast/T_Data_Broadcast_PDU.h>
#include <KNX/03/03/04/T_Data_Connected/T_Data_Connected_PDU.h>
#include <KNX/03/03/04/T_Data_Group/T_Data_Group_PDU.h>
#include <KNX/03/03/04/T_Data_Individual/T_Data_Individual_PDU.h>
#include <KNX/03/03/04/T_Data_SystemBroadcast/T_Data_SystemBroadcast_PDU.h>
#include <KNX/03/03/04/T_Data_Tag_Group/T_Data_Tag_Group_PDU.h>
#include <KNX/03/03/04/T_Disconnect/T_Disconnect_PDU.h>
#include <KNX/03/03/04/T_NAK/T_NAK_PDU.h>

namespace KNX {

Transport_Layer::Transport_Layer(Network_Layer & network_layer, asio::io_context & io_context) :
    io_context(io_context)
{
    N_Data_Individual_req = std::bind(&Network_Layer::N_Data_Individual_req, &network_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    network_layer.N_Data_Individual_con = std::bind(&Transport_Layer::N_Data_Individual_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    network_layer.N_Data_Individual_ind = std::bind(&Transport_Layer::N_Data_Individual_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);

    N_Data_Group_req = std::bind(&Network_Layer::N_Data_Group_req, &network_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    network_layer.N_Data_Group_con = std::bind(&Transport_Layer::N_Data_Group_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
    network_layer.N_Data_Group_ind = std::bind(&Transport_Layer::N_Data_Group_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);

    N_Data_Broadcast_req = std::bind(&Network_Layer::N_Data_Broadcast_req, &network_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    network_layer.N_Data_Broadcast_con = std::bind(&Transport_Layer::N_Data_Broadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    network_layer.N_Data_Broadcast_ind = std::bind(&Transport_Layer::N_Data_Broadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);

    N_Data_SystemBroadcast_req = std::bind(&Network_Layer::N_Data_SystemBroadcast_req, &network_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    network_layer.N_Data_SystemBroadcast_con = std::bind(&Transport_Layer::N_Data_SystemBroadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    network_layer.N_Data_SystemBroadcast_ind = std::bind(&Transport_Layer::N_Data_SystemBroadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
}

Transport_Layer::~Transport_Layer()
{
    connections.clear();
}

/* T_Data_Group service */

void Transport_Layer::T_Data_Group_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu)
{
    if (N_Data_Group_req) {
        assert(group_address_table);
        const Group_Address destination_address = group_address_table->group_address(tsap);
        std::shared_ptr<T_Data_Group_PDU> tpdu = std::make_shared<T_Data_Group_PDU>();
        tpdu->tsdu = tsdu;
        N_Data_Group_req(ack_request, destination_address, Extended_Frame_Format::Standard, hop_count_type, priority, tpdu);
    }
}

/* T_Data_Tag_Group service */

void Transport_Layer::T_Data_Tag_Group_req(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu)
{
    if (N_Data_Group_req) {
        std::shared_ptr<T_Data_Tag_Group_PDU> tpdu = std::make_shared<T_Data_Tag_Group_PDU>();
        tpdu->tsdu = tsdu;
        N_Data_Group_req(ack_request, destination_address, extended_frame_format, hop_count_type, priority, tpdu);
    }
}

/* T_Data_Broadcast service */

void Transport_Layer::T_Data_Broadcast_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu)
{
    if (N_Data_Broadcast_req) {
        std::shared_ptr<T_Data_Broadcast_PDU> tpdu = std::make_shared<T_Data_Broadcast_PDU>();
        tpdu->tsdu = tsdu;
        N_Data_Broadcast_req(ack_request, hop_count_type, priority, tpdu);
    }
}

/* T_Data_SystemBroadcast service */

void Transport_Layer::T_Data_SystemBroadcast_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu)
{
    if (N_Data_SystemBroadcast_req) {
        std::shared_ptr<T_Data_SystemBroadcast_PDU> tpdu = std::make_shared<T_Data_SystemBroadcast_PDU>();
        tpdu->tsdu = tsdu;
        N_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, tpdu);
    }
}

/* T_Data_Individual service */

void Transport_Layer::T_Data_Individual_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu)
{
    if (N_Data_Individual_req) {
        const Individual_Address destination_address = tsap;
        std::shared_ptr<T_Data_Individual_PDU> tpdu = std::make_shared<T_Data_Individual_PDU>();
        tpdu->tsdu = tsdu;
        N_Data_Individual_req(ack_request, destination_address, hop_count_type, priority, tpdu);
    }
}

/* T_Connect service */

void Transport_Layer::T_Connect_req(const Individual_Address destination_address, const Priority priority)
{
    io_context.post([this, destination_address, priority]() {
        const TSAP_Connected tsap = destination_address;
        connection(tsap)->T_Connect_req(destination_address, priority);
    });
}

/* T_Disconnect service */

void Transport_Layer::T_Disconnect_req(const Priority priority, const TSAP_Connected tsap)
{
    io_context.post([this, priority, tsap]() {
        connection(tsap)->T_Disconnect_req(priority, tsap);
    });
}

/* T_Data_Connected service */

void Transport_Layer::T_Data_Connected_req(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)
{
    io_context.post([this, priority, tsap, tsdu]() {
        connection(tsap)->T_Data_Connected_req(priority, tsap, tsdu);
    });
}

/* state */

Connection_State Transport_Layer::connection_state(const TSAP_Connected tsap)
{
    return connection(tsap)->state;
}

/* event */

uint8_t Transport_Layer::connection_last_event(const TSAP_Connected tsap)
{
    return connection(tsap)->last_event;
}

/* action */

uint8_t Transport_Layer::connection_last_action(const TSAP_Connected tsap)
{
    return connection(tsap)->last_action;
}

/* style */

Transport_Layer::Connection_Style Transport_Layer::connection_style(const TSAP_Connected tsap)
{
    return connection(tsap)->style;
}

/* test */

void Transport_Layer::test_expire_connection_timeout_timer(const TSAP_Connected tsap)
{
    connection(tsap)->connection_timeout_timer_stop();
    // calls connnection_timeout_timer_expired(asio::error::operation_aborted), which returns actionless

    const std::error_code error;
    connection(tsap)->connection_timeout_timer_expired(error);
}

void Transport_Layer::test_expire_acknowledgement_timeout_timer(const TSAP_Connected tsap)
{
    if (connection(tsap)->style == Connection_Style::Style_1_Rationalised) {
        // Style 1 Rationalised has no acknowledgement timer, so use connection timer instead.
        test_expire_connection_timeout_timer(tsap);
    } else {
        connection(tsap)->acknowledgement_timeout_timer_stop();
        // calls acknowledge_timeout_timer_expired(asio::error::operation_aborted), which returns actionless

        const std::error_code error;
        connection(tsap)->acknowledgement_timeout_timer_expired(error);
    }
}

/* N_Data_Individual service */

void Transport_Layer::N_Data_Individual_con(const Ack_Request ack_request, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, Status n_status)
{
    if (nsdu->data_control_flag == Data_Control_Flag::Data) {
        if (nsdu->numbered == Numbered::Has_No_SeqNo) {
            if (T_Data_Individual_con) {
                std::shared_ptr<T_Data_Individual_PDU> tpdu = std::dynamic_pointer_cast<T_Data_Individual_PDU>(nsdu);
                assert(tpdu);
                const TSAP_Individual tsap = destination_address;
                T_Data_Individual_con(ack_request, hop_count_type, priority, tsap, tpdu->tsdu, n_status);
            }
        } else {
            const TSAP_Connected tsap = destination_address;
            connection(tsap)->N_Data_Individual_con(ack_request, destination_address, hop_count_type, priority, nsdu, n_status);
        }
    } else {
        const TSAP_Connected tsap = destination_address;
        connection(tsap)->N_Data_Individual_con(ack_request, destination_address, hop_count_type, priority, nsdu, n_status);
    }
}

void Transport_Layer::N_Data_Individual_ind(const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)
{
    if (nsdu->data_control_flag == Data_Control_Flag::Data) {
        if (nsdu->numbered == Numbered::Has_No_SeqNo) {
            if (T_Data_Individual_ind) {
                std::shared_ptr<T_Data_Individual_PDU> tpdu = std::dynamic_pointer_cast<T_Data_Individual_PDU>(nsdu);
                assert(tpdu);
                const TSAP_Individual tsap = source_address;
                T_Data_Individual_ind(hop_count_type, priority, tsap, tpdu->tsdu);
            }
        } else {
            const TSAP_Connected tsap = source_address;
            connection(tsap)->N_Data_Individual_ind(destination_address, hop_count_type, priority, source_address, nsdu);
        }
    } else {
        const TSAP_Connected tsap = source_address;
        connection(tsap)->N_Data_Individual_ind(destination_address, hop_count_type, priority, source_address, nsdu);
    }
}

/* N_Data_Group service */

void Transport_Layer::N_Data_Group_con(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status)
{
    Individual_Address source_address;
    if (group_address_table) {
        source_address = group_address_table->individual_address;
    }
    if (nsdu->sequence_number == 0) {
        if (T_Data_Group_con) {
            std::shared_ptr<T_Data_Group_PDU> tpdu = std::dynamic_pointer_cast<T_Data_Group_PDU>(nsdu);
            assert(tpdu);
            const TSAP_Group tsap = group_address_table->tsap(destination_address);
            T_Data_Group_con(ack_request, hop_count_type, priority, source_address, tsap, tpdu->tsdu, n_status);
        }
    } else {
        if (T_Data_Tag_Group_con) {
            std::shared_ptr<T_Data_Tag_Group_PDU> tpdu = std::dynamic_pointer_cast<T_Data_Tag_Group_PDU>(nsdu);
            assert(tpdu);
            T_Data_Tag_Group_con(hop_count_type, priority, source_address, destination_address, extended_frame_format, tpdu->tsdu, n_status);
        }
    }
}

void Transport_Layer::N_Data_Group_ind(const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)
{
    if (nsdu->sequence_number == 0) {
        if (T_Data_Group_ind) {
            std::shared_ptr<T_Data_Group_PDU> tpdu = std::dynamic_pointer_cast<T_Data_Group_PDU>(nsdu);
            assert(tpdu);
            const TSAP_Group tsap = group_address_table->tsap(destination_address);
            T_Data_Group_ind(hop_count_type, priority, source_address, tsap, tpdu->tsdu);
        }
    } else {
        if (T_Data_Tag_Group_ind) {
            std::shared_ptr<T_Data_Tag_Group_PDU> tpdu = std::dynamic_pointer_cast<T_Data_Tag_Group_PDU>(nsdu);
            assert(tpdu);
            T_Data_Tag_Group_ind(hop_count_type, priority, source_address, destination_address, extended_frame_format, tpdu->tsdu);
        }
    }
}

/* N_Data_Broadcast service */

void Transport_Layer::N_Data_Broadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status)
{
    if (T_Data_Broadcast_con) {
        std::shared_ptr<T_Data_Broadcast_PDU> tpdu = std::dynamic_pointer_cast<T_Data_Broadcast_PDU>(nsdu);
        assert(tpdu);
        T_Data_Broadcast_con(ack_request, hop_count_type, priority, tpdu->tsdu, n_status);
    }
}

void Transport_Layer::N_Data_Broadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)
{
    if (T_Data_Broadcast_ind) {
        std::shared_ptr<T_Data_Broadcast_PDU> tpdu = std::dynamic_pointer_cast<T_Data_Broadcast_PDU>(nsdu);
        assert(tpdu);
        T_Data_Broadcast_ind(hop_count_type, priority, source_address, tpdu->tsdu);
    }
}

/* N_Data_SystemBroadcast service */

void Transport_Layer::N_Data_SystemBroadcast_con(const Ack_Request /*ack_request*/, const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Status n_status)
{
    if (T_Data_SystemBroadcast_con) {
        std::shared_ptr<T_Data_SystemBroadcast_PDU> tpdu = std::dynamic_pointer_cast<T_Data_SystemBroadcast_PDU>(nsdu);
        assert(tpdu);
        T_Data_SystemBroadcast_con(hop_count_type, priority, tpdu->tsdu, n_status);
    }
}

void Transport_Layer::N_Data_SystemBroadcast_ind(const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Individual_Address source_address)
{
    if (T_Data_SystemBroadcast_ind) {
        std::shared_ptr<T_Data_SystemBroadcast_PDU> tpdu = std::dynamic_pointer_cast<T_Data_SystemBroadcast_PDU>(nsdu);
        assert(tpdu);
        T_Data_SystemBroadcast_ind(hop_count_type, priority, source_address, tpdu->tsdu);
    }
}

std::shared_ptr<Transport_Layer::Connection_State_Machine> Transport_Layer::connection(const TSAP_Connected tsap)
{
    std::shared_ptr<Connection_State_Machine> connection = connections[tsap];
    if (!connection) {
        switch (default_connection_style) {
        case Connection_Style::Style_1:
            connection = std::make_shared<Connection_State_Machine_Style_1>(*this, io_context, tsap);
            break;
        case Connection_Style::Style_2:
            connection = std::make_shared<Connection_State_Machine_Style_2>(*this, io_context, tsap);
            break;
        case Connection_Style::Style_3:
            connection = std::make_shared<Connection_State_Machine_Style_3>(*this, io_context, tsap);
            break;
        case Connection_Style::Style_1_Rationalised:
            connection = std::make_shared<Connection_State_Machine_Style_1_Rationalised>(*this, io_context, tsap);
            break;
        }
        connections[tsap] = connection;
    }

    assert(connection);
    return connection;
}

}
