// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <chrono>
#include <functional>
#include <map>

#include <asio/io_context.hpp>
#include <asio/steady_timer.hpp>

#include <KNX/03/03/03/Network_Layer.h>
#include <KNX/03/03/04/Connection_State.h>
#include <KNX/03/03/04/TSAP.h>
#include <KNX/03/03/04/TSDU.h>
#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>
#include <KNX/knx_export.h>

namespace KNX {

/** Transport Layer */
class KNX_EXPORT Transport_Layer
{
public:
    explicit Transport_Layer(Network_Layer & network_layer, asio::io_context & io_context);
    virtual ~Transport_Layer();

    /* T_Data_Group service */

    // @note in all following slots/signals, the octet_count was dropped, as it's part of tsdu anyway.

    virtual void T_Data_Group_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu);
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu, const Status t_status)> T_Data_Group_con;
    std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu)> T_Data_Group_ind;

    /* T_Data_Tag_Group service */

    // @note "Frame_Format frame_format" reduced to "Extended_Frame_Format extended_frame_format"

    virtual void T_Data_Tag_Group_req(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu);
    std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu, const Status t_status)> T_Data_Tag_Group_con;
    std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu)> T_Data_Tag_Group_ind;

    /* T_Data_Broadcast service */

    virtual void T_Data_Broadcast_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu);
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status)> T_Data_Broadcast_con;
    std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu)> T_Data_Broadcast_ind;

    /* T_Data_SystemBroadcast service */

    virtual void T_Data_SystemBroadcast_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu);
    std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status)> T_Data_SystemBroadcast_con;
    std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu)> T_Data_SystemBroadcast_ind;

    /* T_Data_Individual service */

    virtual void T_Data_Individual_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu);
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu, const Status t_status)> T_Data_Individual_con;
    std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu)> T_Data_Individual_ind;

    /* T_Connect service */

    virtual void T_Connect_req(const Individual_Address destination_address, const Priority priority);
    std::function<void(const Individual_Address destination_address, const TSAP_Connected tsap, const Status t_status)> T_Connect_con;
    std::function<void(const TSAP_Connected tsap)> T_Connect_ind;

    /* T_Disconnect service */

    virtual void T_Disconnect_req(const Priority priority, const TSAP_Connected tsap);
    std::function<void(const Priority priority, const TSAP_Connected tsap, const Status t_status)> T_Disconnect_con;
    std::function<void(const TSAP_Connected tsap)> T_Disconnect_ind;

    /* T_Data_Connected service */

    virtual void T_Data_Connected_req(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu);
    std::function<void(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)> T_Data_Connected_con;
    std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)> T_Data_Connected_ind;

    /** get connection state */
    virtual Connection_State connection_state(const TSAP_Connected tsap);

    /** get last event */
    virtual uint8_t connection_last_event(const TSAP_Connected tsap);

    /** get last action */
    virtual uint8_t connection_last_action(const TSAP_Connected tsap);

    /** style */
    enum Connection_Style {
        Style_1,
        Style_2,
        Style_3,
        Style_1_Rationalised
    };

    /** connection style for new connections */
    Connection_Style default_connection_style{Connection_Style::Style_1};

    /** get connection style */
    virtual Connection_Style connection_style(const TSAP_Connected tsap);

    /* Parameters */

    std::shared_ptr<Group_Address_Table> group_address_table{}; // connection number list
    std::chrono::seconds connection_timeout{6};
    std::chrono::seconds acknowledgement_timeout{3};
    uint8_t max_rep_count{3};

    /**
     * Client accepts connection.
     *
     * Used in Transport_Layer::Connection_State_Machine::G_client_accepts_connection.
     */
    bool client_accepts_connection{true};

    /* test */

    /** for test purposes only: let the connection timeout timer expire */
    void test_expire_connection_timeout_timer(const TSAP_Connected tsap);

    /** for test purposes only: let the acknowledgement timeout_timer expire */
    void test_expire_acknowledgement_timeout_timer(const TSAP_Connected tsap);

protected:
    asio::io_context & io_context;

    /* N_Data_Individual service */
    std::function<void(const Ack_Request ack_request, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu)> N_Data_Individual_req;
    virtual void N_Data_Individual_con(const Ack_Request ack_request, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, Status n_status);
    virtual void N_Data_Individual_ind(const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu);

    /* N_Data_Group service */
    std::function<void(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu)> N_Data_Group_req;
    virtual void N_Data_Group_con(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status);
    virtual void N_Data_Group_ind(const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu);

    /* N_Data_Broadcast service */
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu)> N_Data_Broadcast_req;
    virtual void N_Data_Broadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status);
    virtual void N_Data_Broadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu);

    /* N_Data_SystemBroadcast service */
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu)> N_Data_SystemBroadcast_req;
    virtual void N_Data_SystemBroadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Status n_status);
    virtual void N_Data_SystemBroadcast_ind(const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Individual_Address source_address);

    /* forward declarations */
    class Connection_State_Machine;
    class Connection_State_Machine_Style_1;
    class Connection_State_Machine_Style_2;
    class Connection_State_Machine_Style_3;
    class Connection_State_Machine_Style_1_Rationalised;

    /** connections */
    std::map<TSAP_Connected, std::shared_ptr<Connection_State_Machine>> connections{};

    virtual std::shared_ptr<Connection_State_Machine> connection(const TSAP_Connected tsap);
};

}
