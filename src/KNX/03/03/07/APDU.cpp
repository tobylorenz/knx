// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/APDU.h>

#include <cassert>

#include <KNX/03/03/07/A_GroupValue_Read/A_GroupValue_Read_PDU.h>
#include <KNX/03/03/07/A_GroupValue_Response/A_GroupValue_Response_PDU.h>
#include <KNX/03/03/07/A_GroupValue_Write/A_GroupValue_Write_PDU.h>
#include <KNX/03/03/07/A_IndividualAddress_Write/A_IndividualAddress_Write_PDU.h>
#include <KNX/03/03/07/A_IndividualAddress_Read/A_IndividualAddress_Read_PDU.h>
#include <KNX/03/03/07/A_IndividualAddress_Response/A_IndividualAddress_Response_PDU.h>
#include <KNX/03/03/07/A_ADC_Read/A_ADC_Read_PDU.h>
#include <KNX/03/03/07/A_ADC_Response/A_ADC_Response_PDU.h>
#include <KNX/03/03/07/A_SystemNetworkParameter_Read/A_SystemNetworkParameter_Read_PDU.h>
#include <KNX/03/03/07/A_SystemNetworkParameter_Response/A_SystemNetworkParameter_Response_PDU.h>
#include <KNX/03/03/07/A_SystemNetworkParameter_Write/A_SystemNetworkParameter_Write_PDU.h>
#include <KNX/03/03/07/A_Memory_Read/A_Memory_Read_PDU.h>
#include <KNX/03/03/07/A_Memory_Response/A_Memory_Response_PDU.h>
#include <KNX/03/03/07/A_Memory_Write/A_Memory_Write_PDU.h>
#include <KNX/03/03/07/A_UserMemory_Read/A_UserMemory_Read_PDU.h>
#include <KNX/03/03/07/A_UserMemory_Response/A_UserMemory_Response_PDU.h>
#include <KNX/03/03/07/A_UserMemory_Write/A_UserMemory_Write_PDU.h>
#include <KNX/03/03/07/A_UserMemoryBit_Write/A_UserMemoryBit_Write_PDU.h>
#include <KNX/03/03/07/A_UserManufacturerInfo_Read/A_UserManufacturerInfo_Read_PDU.h>
#include <KNX/03/03/07/A_UserManufacturerInfo_Response/A_UserManufacturerInfo_Response_PDU.h>
#include <KNX/03/03/07/A_FunctionPropertyCommand/A_FunctionPropertyCommand_PDU.h>
#include <KNX/03/03/07/A_FunctionPropertyState_Read/A_FunctionPropertyState_Read_PDU.h>
#include <KNX/03/03/07/A_FunctionPropertyState_Response/A_FunctionPropertyState_Response_PDU.h>
#include <KNX/03/03/07/A_DeviceDescriptor_Read/A_DeviceDescriptor_Read_PDU.h>
#include <KNX/03/03/07/A_DeviceDescriptor_Response/A_DeviceDescriptor_Response_PDU.h>
#include <KNX/03/03/07/A_Restart/A_Restart_PDU.h>
#include <KNX/03/03/07/A_Open_Routing_Table_Req/A_Open_Routing_Table_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Routing_Table_Req/A_Read_Routing_Table_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Routing_Table_Res/A_Read_Routing_Table_Res_PDU.h>
#include <KNX/03/03/07/A_Write_Routing_Table_Req/A_Write_Routing_Table_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Router_Memory_Req/A_Read_Router_Memory_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Router_Memory_Res/A_Read_Router_Memory_Res_PDU.h>
#include <KNX/03/03/07/A_Write_Router_Memory_Req/A_Write_Router_Memory_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Router_Status_Req/A_Read_Router_Status_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Router_Status_Res/A_Read_Router_Status_Res_PDU.h>
#include <KNX/03/03/07/A_Write_Router_Status_Req/A_Write_Router_Status_Req_PDU.h>
#include <KNX/03/03/07/A_MemoryBit_Write/A_MemoryBit_Write_PDU.h>
#include <KNX/03/03/07/A_Authorize_Request/A_Authorize_Request_PDU.h>
#include <KNX/03/03/07/A_Authorize_Response/A_Authorize_Response_PDU.h>
#include <KNX/03/03/07/A_Key_Write/A_Key_Write_PDU.h>
#include <KNX/03/03/07/A_Key_Response/A_Key_Response_PDU.h>
#include <KNX/03/03/07/A_PropertyValue_Read/A_PropertyValue_Read_PDU.h>
#include <KNX/03/03/07/A_PropertyValue_Response/A_PropertyValue_Response_PDU.h>
#include <KNX/03/03/07/A_PropertyValue_Write/A_PropertyValue_Write_PDU.h>
#include <KNX/03/03/07/A_PropertyDescription_Read/A_PropertyDescription_Read_PDU.h>
#include <KNX/03/03/07/A_PropertyDescription_Response/A_PropertyDescription_Response_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_Read/A_NetworkParameter_Read_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_Response/A_NetworkParameter_Response_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_InfoReport/A_NetworkParameter_InfoReport_PDU.h>
#include <KNX/03/03/07/A_IndividualAddressSerialNumber_Read/A_IndividualAddressSerialNumber_Read_PDU.h>
#include <KNX/03/03/07/A_IndividualAddressSerialNumber_Response/A_IndividualAddressSerialNumber_Response_PDU.h>
#include <KNX/03/03/07/A_IndividualAddressSerialNumber_Write/A_IndividualAddressSerialNumber_Write_PDU.h>
#include <KNX/03/03/07/A_DomainAddress_Write/A_DomainAddress_Write_PDU.h>
#include <KNX/03/03/07/A_DomainAddress_Read/A_DomainAddress_Read_PDU.h>
#include <KNX/03/03/07/A_DomainAddress_Response/A_DomainAddress_Response_PDU.h>
#include <KNX/03/03/07/A_DomainAddressSelective_Read/A_DomainAddressSelective_Read_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_Write/A_NetworkParameter_Write_PDU.h>
#include <KNX/03/03/07/A_Link_Read/A_Link_Read_PDU.h>
#include <KNX/03/03/07/A_Link_Response/A_Link_Response_PDU.h>
#include <KNX/03/03/07/A_Link_Write/A_Link_Write_PDU.h>
#include <KNX/03/03/07/A_GroupPropValue_Read/A_GroupPropValue_Read_PDU.h>
#include <KNX/03/03/07/A_GroupPropValue_Response/A_GroupPropValue_Response_PDU.h>
#include <KNX/03/03/07/A_GroupPropValue_Write/A_GroupPropValue_Write_PDU.h>
#include <KNX/03/03/07/A_GroupPropValue_InfoReport/A_GroupPropValue_InfoReport_PDU.h>
#include <KNX/03/03/07/A_DomainAddressSerialNumber_Read/A_DomainAddressSerialNumber_Read_PDU.h>
#include <KNX/03/03/07/A_DomainAddressSerialNumber_Response/A_DomainAddressSerialNumber_Response_PDU.h>
#include <KNX/03/03/07/A_DomainAddressSerialNumber_Write/A_DomainAddressSerialNumber_Write_PDU.h>
#include <KNX/03/03/07/A_FileStream_InfoReport/A_FileStream_InfoReport_PDU.h>
#include <KNX/03/03/07/A_ServiceInformation_Indication/A_ServiceInformation_Indication_PDU.h>
#include <KNX/03/03/07/A_Unknown/A_Unknown_PDU.h>

namespace KNX {

APDU::APDU()
{
}

APDU::APDU(const APCI apci) :
    apci(apci)
{
}

void APDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    /* octet 6..7 */
    const uint16_t apci_data = ((*first++ & 0x03) << 8) | *first++;

    uint8_t data_bits = 0;
    switch (apci_data & 0x3c0) {
    case 0x040: // A_GroupValue_Response
    case 0x080: // A_GroupValue_Write
    case 0x180: // A_ADC_Read
    case 0x1c0: // A_ADC_Response
    case 0x300: // A_DeviceDescriptor_Read
    case 0x340: // A_DeviceDescriptor_Response
        data_bits = 6;
        break;
    default:
        break;
    }
    switch (apci_data & 0x3f0) {
    case 0x200: // A_Memory_Read
    case 0x240: // A_Memory_Response
    case 0x280: // A_Memory_Write
        data_bits = 4;
        break;
    default:
        break;
    }

    switch (data_bits) {
    case 4:
        apci = static_cast<APCI>(apci_data & 0x3f0);
        data_short = apci_data & 0x0f;
        break;
    case 6:
        apci = static_cast<APCI>(apci_data & 0x3c0);
        data_short = apci_data & 0x3f;
        break;
    default:
        apci = static_cast<APCI>(apci_data);
        break;
    }

    assert(first == last);
}

std::vector<uint8_t> APDU::toData() const
{
    std::vector<uint8_t> data;

    uint8_t data_bits = 0;
    switch (apci) {
    case APCI::A_GroupValue_Response:
    case APCI::A_GroupValue_Write:
    case APCI::A_ADC_Read:
    case APCI::A_ADC_Response:
    case APCI::A_DeviceDescriptor_Read:
    case APCI::A_DeviceDescriptor_Response:
        data_bits = 6;
        break;
    case APCI::A_Memory_Read:
    case APCI::A_Memory_Response:
    case APCI::A_Memory_Write:
        data_bits = 4;
        break;
    default:
        break;
    }

    /* octet 6 */
    data.push_back(static_cast<uint16_t>(apci) >> 8);

    /* octet 7 */
    switch (data_bits) {
    case 4:
        data.push_back((static_cast<uint16_t>(apci) & 0xf0) | (data_short & 0x0f));
        break;
    case 6:
        data.push_back((static_cast<uint16_t>(apci) & 0xc0) | (data_short & 0x3f));
        break;
    default:
        data.push_back(static_cast<uint16_t>(apci) & 0xff);
        break;
    }

    return data;
}

uint8_t APDU::length_calculated() const
{
    return 1;
}

std::shared_ptr<APDU> make_APDU(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    /* octet 6..7 */
    APDU apdu;
    apdu.fromData(first, first + 2);

    /* octet 6..N */
    switch (apdu.apci) {
    case APCI::A_GroupValue_Read: {
        std::shared_ptr<A_GroupValue_Read_PDU> apdu2 = std::make_shared<A_GroupValue_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_GroupValue_Response: {
        std::shared_ptr<A_GroupValue_Response_PDU> apdu2 = std::make_shared<A_GroupValue_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_GroupValue_Write: {
        std::shared_ptr<A_GroupValue_Write_PDU> apdu2 = std::make_shared<A_GroupValue_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_IndividualAddress_Write: {
        std::shared_ptr<A_IndividualAddress_Write_PDU> apdu2 = std::make_shared<A_IndividualAddress_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_IndividualAddress_Read: {
        std::shared_ptr<A_IndividualAddress_Read_PDU> apdu2 = std::make_shared<A_IndividualAddress_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_IndividualAddress_Response: {
        std::shared_ptr<A_IndividualAddress_Response_PDU> apdu2 = std::make_shared<A_IndividualAddress_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_ADC_Read: {
        std::shared_ptr<A_ADC_Read_PDU> apdu2 = std::make_shared<A_ADC_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_ADC_Response: {
        std::shared_ptr<A_ADC_Response_PDU> apdu2 = std::make_shared<A_ADC_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_SystemNetworkParameter_Read: {
        std::shared_ptr<A_SystemNetworkParameter_Read_PDU> apdu2 = std::make_shared<A_SystemNetworkParameter_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_SystemNetworkParameter_Response: {
        std::shared_ptr<A_SystemNetworkParameter_Response_PDU> apdu2 = std::make_shared<A_SystemNetworkParameter_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_SystemNetworkParameter_Write: {
        std::shared_ptr<A_SystemNetworkParameter_Write_PDU> apdu2 = std::make_shared<A_SystemNetworkParameter_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Memory_Read: {
        std::shared_ptr<A_Memory_Read_PDU> apdu2 = std::make_shared<A_Memory_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Memory_Response: {
        std::shared_ptr<A_Memory_Response_PDU> apdu2 = std::make_shared<A_Memory_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Memory_Write: {
        std::shared_ptr<A_Memory_Write_PDU> apdu2 = std::make_shared<A_Memory_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_UserMemory_Read: {
        std::shared_ptr<A_UserMemory_Read_PDU> apdu2 = std::make_shared<A_UserMemory_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_UserMemory_Response: {
        std::shared_ptr<A_UserMemory_Response_PDU> apdu2 = std::make_shared<A_UserMemory_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_UserMemory_Write: {
        std::shared_ptr<A_UserMemory_Write_PDU> apdu2 = std::make_shared<A_UserMemory_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_UserMemoryBit_Write: {
        std::shared_ptr<A_UserMemoryBit_Write_PDU> apdu2 = std::make_shared<A_UserMemoryBit_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_UserManufacturerInfo_Read: {
        std::shared_ptr<A_UserManufacturerInfo_Read_PDU> apdu2 = std::make_shared<A_UserManufacturerInfo_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_UserManufacturerInfo_Response: {
        std::shared_ptr<A_UserManufacturerInfo_Response_PDU> apdu2 = std::make_shared<A_UserManufacturerInfo_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_FunctionPropertyCommand: {
        std::shared_ptr<A_FunctionPropertyCommand_PDU> apdu2 = std::make_shared<A_FunctionPropertyCommand_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_FunctionPropertyState_Read: {
        std::shared_ptr<A_FunctionPropertyState_Read_PDU> apdu2 = std::make_shared<A_FunctionPropertyState_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_FunctionPropertyState_Response: {
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> apdu2 = std::make_shared<A_FunctionPropertyState_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_DeviceDescriptor_Read: {
        std::shared_ptr<A_DeviceDescriptor_Read_PDU> apdu2 = std::make_shared<A_DeviceDescriptor_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_DeviceDescriptor_Response: { // = A_DeviceDescriptor_InfoReport
        std::shared_ptr<A_DeviceDescriptor_Response_PDU> apdu2 = std::make_shared<A_DeviceDescriptor_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Restart: { // = A_Restart_Response
        std::shared_ptr<A_Restart_PDU> apdu2 = std::make_shared<A_Restart_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Open_Routing_Table_Req: {
        std::shared_ptr<A_Open_Routing_Table_Req_PDU> apdu2 = std::make_shared<A_Open_Routing_Table_Req_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Read_Routing_Table_Req: {
        std::shared_ptr<A_Read_Routing_Table_Req_PDU> apdu2 = std::make_shared<A_Read_Routing_Table_Req_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Read_Routing_Table_Res: {
        std::shared_ptr<A_Read_Routing_Table_Res_PDU> apdu2 = std::make_shared<A_Read_Routing_Table_Res_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Write_Routing_Table_Req: {
        std::shared_ptr<A_Write_Routing_Table_Req_PDU> apdu2 = std::make_shared<A_Write_Routing_Table_Req_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Read_Router_Memory_Req: {
        std::shared_ptr<A_Read_Router_Memory_Req_PDU> apdu2 = std::make_shared<A_Read_Router_Memory_Req_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Read_Router_Memory_Res: {
        std::shared_ptr<A_Read_Router_Memory_Res_PDU> apdu2 = std::make_shared<A_Read_Router_Memory_Res_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Write_Router_Memory_Req: {
        std::shared_ptr<A_Write_Router_Memory_Req_PDU> apdu2 = std::make_shared<A_Write_Router_Memory_Req_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Read_Router_Status_Req: {
        std::shared_ptr<A_Read_Router_Status_Req_PDU> apdu2 = std::make_shared<A_Read_Router_Status_Req_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Read_Router_Status_Res: {
        std::shared_ptr<A_Read_Router_Status_Res_PDU> apdu2 = std::make_shared<A_Read_Router_Status_Res_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Write_Router_Status_Req: {
        std::shared_ptr<A_Write_Router_Status_Req_PDU> apdu2 = std::make_shared<A_Write_Router_Status_Req_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_MemoryBit_Write: {
        std::shared_ptr<A_MemoryBit_Write_PDU> apdu2 = std::make_shared<A_MemoryBit_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Authorize_Request: {
        std::shared_ptr<A_Authorize_Request_PDU> apdu2 = std::make_shared<A_Authorize_Request_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Authorize_Response: {
        std::shared_ptr<A_Authorize_Response_PDU> apdu2 = std::make_shared<A_Authorize_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Key_Write: {
        std::shared_ptr<A_Key_Write_PDU> apdu2 = std::make_shared<A_Key_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Key_Response: {
        std::shared_ptr<A_Key_Response_PDU> apdu2 = std::make_shared<A_Key_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_PropertyValue_Read: {
        std::shared_ptr<A_PropertyValue_Read_PDU> apdu2 = std::make_shared<A_PropertyValue_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_PropertyValue_Response: {
        std::shared_ptr<A_PropertyValue_Response_PDU> apdu2 = std::make_shared<A_PropertyValue_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_PropertyValue_Write: {
        std::shared_ptr<A_PropertyValue_Write_PDU> apdu2 = std::make_shared<A_PropertyValue_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_PropertyDescription_Read: {
        std::shared_ptr<A_PropertyDescription_Read_PDU> apdu2 = std::make_shared<A_PropertyDescription_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_PropertyDescription_Response: {
        std::shared_ptr<A_PropertyDescription_Response_PDU> apdu2 = std::make_shared<A_PropertyDescription_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_NetworkParameter_Read: {
        std::shared_ptr<A_NetworkParameter_Read_PDU> apdu2 = std::make_shared<A_NetworkParameter_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_NetworkParameter_Response: { // = A_NetworkParameter_InfoReport
        std::shared_ptr<A_NetworkParameter_Response_PDU> apdu2 = std::make_shared<A_NetworkParameter_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_IndividualAddressSerialNumber_Read: {
        std::shared_ptr<A_IndividualAddressSerialNumber_Read_PDU> apdu2 = std::make_shared<A_IndividualAddressSerialNumber_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_IndividualAddressSerialNumber_Response: {
        std::shared_ptr<A_IndividualAddressSerialNumber_Response_PDU> apdu2 = std::make_shared<A_IndividualAddressSerialNumber_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_IndividualAddressSerialNumber_Write: {
        std::shared_ptr<A_IndividualAddressSerialNumber_Write_PDU> apdu2 = std::make_shared<A_IndividualAddressSerialNumber_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_ServiceInformation_Indication: {
        std::shared_ptr<A_ServiceInformation_Indication_PDU> apdu2 = std::make_shared<A_ServiceInformation_Indication_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_DomainAddress_Write: {
        std::shared_ptr<A_DomainAddress_Write_PDU> apdu2 = std::make_shared<A_DomainAddress_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_DomainAddress_Read: {
        std::shared_ptr<A_DomainAddress_Read_PDU> apdu2 = std::make_shared<A_DomainAddress_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_DomainAddress_Response: {
        std::shared_ptr<A_DomainAddress_Response_PDU> apdu2 = std::make_shared<A_DomainAddress_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_DomainAddressSelective_Read: {
        std::shared_ptr<A_DomainAddressSelective_Read_PDU> apdu2 = std::make_shared<A_DomainAddressSelective_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_NetworkParameter_Write: {
        std::shared_ptr<A_NetworkParameter_Write_PDU> apdu2 = std::make_shared<A_NetworkParameter_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Link_Read: {
        std::shared_ptr<A_Link_Read_PDU> apdu2 = std::make_shared<A_Link_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Link_Response: {
        std::shared_ptr<A_Link_Response_PDU> apdu2 = std::make_shared<A_Link_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_Link_Write: {
        std::shared_ptr<A_Link_Write_PDU> apdu2 = std::make_shared<A_Link_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_GroupPropValue_Read: {
        std::shared_ptr<A_GroupPropValue_Read_PDU> apdu2 = std::make_shared<A_GroupPropValue_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_GroupPropValue_Response: {
        std::shared_ptr<A_GroupPropValue_Response_PDU> apdu2 = std::make_shared<A_GroupPropValue_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_GroupPropValue_Write: {
        std::shared_ptr<A_GroupPropValue_Write_PDU> apdu2 = std::make_shared<A_GroupPropValue_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_GroupPropValue_InfoReport: {
        std::shared_ptr<A_GroupPropValue_InfoReport_PDU> apdu2 = std::make_shared<A_GroupPropValue_InfoReport_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_DomainAddressSerialNumber_Read: {
        std::shared_ptr<A_DomainAddressSerialNumber_Read_PDU> apdu2 = std::make_shared<A_DomainAddressSerialNumber_Read_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_DomainAddressSerialNumber_Response: {
        std::shared_ptr<A_DomainAddressSerialNumber_Response_PDU> apdu2 = std::make_shared<A_DomainAddressSerialNumber_Response_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_DomainAddressSerialNumber_Write: {
        std::shared_ptr<A_DomainAddressSerialNumber_Write_PDU> apdu2 = std::make_shared<A_DomainAddressSerialNumber_Write_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    case APCI::A_FileStream_InfoReport: {
        std::shared_ptr<A_FileStream_InfoReport_PDU> apdu2 = std::make_shared<A_FileStream_InfoReport_PDU>();
        apdu2->fromData(first, last);
        return apdu2;
    }
    break;
    }

    std::shared_ptr<A_Unknown_PDU> apdu2 = std::make_shared<A_Unknown_PDU>(apdu.apci);
    apdu2->fromData(first, last);
    return apdu2;
}

}
