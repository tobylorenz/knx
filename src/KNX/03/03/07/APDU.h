// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * APCI Service Type Identifier
 *
 * @ingroup KNX_03_03_07_02
 * @ingroup KNX_10_01_07_06_03
 */
enum class APCI : uint16_t { // actually uint10_t
    A_GroupValue_Read = 0x000,
    A_GroupValue_Response = 0x040,
    A_GroupValue_Write = 0x080,
    A_IndividualAddress_Write = 0x0c0,
    A_IndividualAddress_Read = 0x100,
    A_IndividualAddress_Response = 0x140,
    A_ADC_Read = 0x180,
    A_ADC_Response = 0x1c0,
    A_SystemNetworkParameter_Read = 0x1c8,
    A_SystemNetworkParameter_Response = 0x1c9,
    A_SystemNetworkParameter_Write = 0x1ca,
    // 0x1cb: planned for future system broadcast service
    // PropExtValueRead = 0x1cc,
    // PropExtValueResp = 0x1cd,
    // PropExtValueWriteCon = 0x1ce,
    // PropExtValueWriteConRes = 0x1cf,
    // PropExtValueWriteUnCon = 0x1d0,
    // PropExtValueInfoReport = 0x1d1,
    // PropExtDescrRead = 0x1d2,
    // PropExtDescrResp = 0x1d3,
    // FuncPropExtCmd = 0x1d4,
    // FuncPropExtRead = 0x1d5,
    // FuncPropExtResp = 0x1d6,
    // MemExtWrite = 0x1fb,
    // MemExtWriteResp = 0x1fc,
    // MemExtRead = 0x1fd,
    // MemExtReadResp = 0x1fe,
    A_Memory_Read = 0x200,
    A_Memory_Response = 0x240,
    A_Memory_Write = 0x280,
    A_UserMemory_Read = 0x2c0,
    A_UserMemory_Response = 0x2c1,
    A_UserMemory_Write = 0x2c2,
    A_UserMemoryBit_Write = 0x2c4,
    A_UserManufacturerInfo_Read = 0x2c5,
    A_UserManufacturerInfo_Response = 0x2c6,
    A_FunctionPropertyCommand = 0x2c7,
    A_FunctionPropertyState_Read = 0x2c8,
    A_FunctionPropertyState_Response = 0x2c9,
    // 0x2ca .. 0x2f7: Reserved USERMSG
    // 0x2f8 .. 0x2fe: manufacturer specific area for USERMSG
    A_DeviceDescriptor_Read = 0x300,
    A_DeviceDescriptor_Response = 0x340,
    A_DeviceDescriptor_InfoReport = A_DeviceDescriptor_Response,
    A_Restart = 0x380,
    A_Restart_Response = A_Restart,
    // RestartReq = 0x381,
    // RestartResp = 0x3A1,
    A_Open_Routing_Table_Req = 0x3c0,
    A_Read_Routing_Table_Req = 0x3c1,
    A_Read_Routing_Table_Res = 0x3c2,
    A_Write_Routing_Table_Req = 0x3c3,
    A_Read_Router_Memory_Req = 0x3c8,
    A_Read_Router_Memory_Res = 0x3c9,
    A_Write_Router_Memory_Req = 0x3ca,
    A_Read_Router_Status_Req = 0x3cd,
    A_Read_Router_Status_Res = 0x3ce,
    A_Write_Router_Status_Req = 0x3cf,
    A_MemoryBit_Write = 0x3d0,
    A_Authorize_Request = 0x3d1,
    A_Authorize_Response = 0x3d2,
    A_Key_Write = 0x3d3,
    A_Key_Response = 0x3d4,
    A_PropertyValue_Read = 0x3d5,
    A_PropertyValue_Response = 0x3d6,
    A_PropertyValue_Write = 0x3d7,
    A_PropertyDescription_Read = 0x3d8,
    A_PropertyDescription_Response = 0x3d9,
    A_NetworkParameter_Read = 0x3da,
    A_NetworkParameter_Response = 0x3db,
    A_NetworkParameter_InfoReport = A_NetworkParameter_Response,
    A_IndividualAddressSerialNumber_Read = 0x3dc,
    A_IndividualAddressSerialNumber_Response = 0x3dd,
    A_IndividualAddressSerialNumber_Write = 0x3de,
    A_ServiceInformation_Indication = 0x3df,
    A_DomainAddress_Write = 0x3e0,
    A_DomainAddress_Read = 0x3e1,
    A_DomainAddress_Response = 0x3e2,
    A_DomainAddressSelective_Read = 0x3e3,
    A_NetworkParameter_Write = 0x3e4,
    A_Link_Read = 0x3e5,
    A_Link_Response = 0x3e6,
    A_Link_Write = 0x3e7,
    A_GroupPropValue_Read = 0x3e8,
    A_GroupPropValue_Response = 0x3e9,
    A_GroupPropValue_Write = 0x3ea,
    A_GroupPropValue_InfoReport = 0x3eb,
    A_DomainAddressSerialNumber_Read = 0x3ec,
    A_DomainAddressSerialNumber_Response = 0x3ed,
    A_DomainAddressSerialNumber_Write = 0x3ee,
    A_FileStream_InfoReport = 0x3f0,
    // DataSec = 0x3f1,
};

/** APDU */
class KNX_EXPORT APDU
{
public:
    APDU();
    APDU(const APCI apci);

    auto operator<=>(const APDU&) const = default;

    /**
     * from data
     *
     * @param[in] first octet 6
     * @param[in] last octet N
     */
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

    /**
     * to data
     *
     * @return octet 6..N
     */
    virtual std::vector<uint8_t> toData() const;

    /** calculate Length (L) */
    virtual uint8_t length_calculated() const;

    APCI apci{};

    /** 6 bit data in octet 7, if not occupied by APCI */
    uint6_t data_short{};
};

/**
 * make APDU
 *
 * @param[in] first octet 6
 * @param[in] last octet N
 * @return APDU
 */
KNX_EXPORT std::shared_ptr<APDU> make_APDU(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
