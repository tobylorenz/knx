// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/ASAP.h>

#include <cassert>

namespace KNX {

KNX_EXPORT bool operator==(const ASAP_Individual & a, const ASAP_Individual & b)
{
    return (a.address == b.address) && (a.connected == b.connected);
}

}
