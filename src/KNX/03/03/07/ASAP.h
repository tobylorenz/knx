// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Group_Address.h>
#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/* Application Layer Service Access Point (ASAP) */

/** Application Layer Service Access Point (ASAP) for Group */
using ASAP_Group = Group_Address;

/** Application Layer Service Access Point (ASAP) for Individual */
struct KNX_EXPORT ASAP_Individual {
    Individual_Address address{};
    bool connected{false}; // @todo redundant with Comm_Mode ?
};

KNX_EXPORT bool operator==(const ASAP_Individual & a, const ASAP_Individual & b);

/** Application Layer Service Access Point (ASAP) for Connected */
using ASAP_Connected = ASAP_Individual; // @todo identifier of the Connection Address

}
