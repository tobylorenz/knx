// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/types.h>

namespace KNX {

/** Application Layer Service Data Unit */
using ASDU = std::vector<uint8_t>;

}
