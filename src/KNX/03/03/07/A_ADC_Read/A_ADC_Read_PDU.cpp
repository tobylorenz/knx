// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_ADC_Read/A_ADC_Read_PDU.h>

#include <cassert>

namespace KNX {

A_ADC_Read_PDU::A_ADC_Read_PDU() :
    APDU(APCI::A_ADC_Read)
{
}

bool A_ADC_Read_PDU::operator==(const A_ADC_Read_PDU & other) const
{
    return APDU::operator==(other) && (read_count == other.read_count);
}

void A_ADC_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 3);

    APDU::fromData(first, first + 2);
    first += 2;

    read_count = *first++;

    assert(first == last);
}

std::vector<uint8_t> A_ADC_Read_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(read_count);

    return data;
}

uint8_t A_ADC_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        1;
}

}
