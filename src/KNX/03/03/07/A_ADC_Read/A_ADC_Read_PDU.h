// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/ADC_Channel_Nr.h>
#include <KNX/03/03/07/ADC_Read_Count.h>
#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_ADC_Read
 *
 * @ingroup KNX_03_03_07_03_05_02
 */
class KNX_EXPORT A_ADC_Read_PDU : public APDU
{
public:
    A_ADC_Read_PDU();

    bool operator==(const A_ADC_Read_PDU & other) const;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** channel hr */
    ADC_Channel_Nr & channel_nr{data_short};

    /** read count */
    ADC_Read_Count read_count{};
};

}
