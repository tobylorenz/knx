// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_ADC_Response/A_ADC_Response_PDU.h>

#include <cassert>

namespace KNX {

A_ADC_Response_PDU::A_ADC_Response_PDU() :
    APDU(APCI::A_ADC_Response)
{
}

bool A_ADC_Response_PDU::operator==(const A_ADC_Response_PDU & other) const
{
    return APDU::operator==(other) && (read_count == other.read_count) && (sum == other.sum);
}

void A_ADC_Response_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    APDU::fromData(first, first + 2);
    first += 2;

    read_count = *first++;
    sum = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> A_ADC_Response_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(read_count);
    data.push_back(sum >> 8);
    data.push_back(sum & 0xff);

    return data;
}

uint8_t A_ADC_Response_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        3;
}

}
