// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Key.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Authorize_Request
 *
 * @ingroup KNX_03_03_07_03_05_07
 */
class KNX_EXPORT A_Authorize_Request_PDU : public APDU
{
public:
    A_Authorize_Request_PDU();

    bool operator==(const A_Authorize_Request_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** key */
    Key key{};
};

}
