// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/A_DeviceDescriptor_Response/A_DeviceDescriptor_Response_PDU.h>

namespace KNX {

/**
 * A_DeviceDescriptor_InfoReport
 *
 * @ingroup KNX_03_03_07_03_03_02
 */
using A_DeviceDescriptor_InfoReport_PDU = A_DeviceDescriptor_Response_PDU;

}
