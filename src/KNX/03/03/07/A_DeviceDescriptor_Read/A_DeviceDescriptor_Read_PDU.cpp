// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_DeviceDescriptor_Read/A_DeviceDescriptor_Read_PDU.h>

#include <cassert>

namespace KNX {

A_DeviceDescriptor_Read_PDU::A_DeviceDescriptor_Read_PDU() :
    APDU(APCI::A_DeviceDescriptor_Read)
{
}

bool A_DeviceDescriptor_Read_PDU::operator==(const A_DeviceDescriptor_Read_PDU & other) const
{
    return APDU::operator==(other);
}

}
