// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Descriptor_Type.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_DeviceDescriptor_Read
 *
 * @ingroup KNX_03_03_07_03_04_02_01
 */
class KNX_EXPORT A_DeviceDescriptor_Read_PDU : public APDU
{
public:
    A_DeviceDescriptor_Read_PDU();

    bool operator==(const A_DeviceDescriptor_Read_PDU & other) const;

    /** descriptor type */
    Descriptor_Type & descriptor_type{data_short};
};

}
