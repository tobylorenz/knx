// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_DeviceDescriptor_Response/A_DeviceDescriptor_Response_PDU.h>

#include <cassert>

namespace KNX {

A_DeviceDescriptor_Response_PDU::A_DeviceDescriptor_Response_PDU() :
    APDU(APCI::A_DeviceDescriptor_Response)
{
}

bool A_DeviceDescriptor_Response_PDU::operator==(const A_DeviceDescriptor_Response_PDU & other) const
{
    return APDU::operator==(other) && (device_descriptor == other.device_descriptor);
}

void A_DeviceDescriptor_Response_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    APDU::fromData(first, first + 2);
    first += 2;

    device_descriptor.assign(first, last);
}

std::vector<uint8_t> A_DeviceDescriptor_Response_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.insert(std::cend(data), std::cbegin(device_descriptor), std::cend(device_descriptor));

    return data;
}

uint8_t A_DeviceDescriptor_Response_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        static_cast<uint8_t>(device_descriptor.size());
}

}
