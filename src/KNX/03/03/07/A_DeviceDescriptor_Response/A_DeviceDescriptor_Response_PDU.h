// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Descriptor_Type.h>
#include <KNX/03/03/07/Device_Descriptor_Data.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_DeviceDescriptor_Response
 *
 * @ingroup KNX_03_03_07_03_04_02_01
 */
class KNX_EXPORT A_DeviceDescriptor_Response_PDU : public APDU
{
public:
    A_DeviceDescriptor_Response_PDU();

    bool operator==(const A_DeviceDescriptor_Response_PDU & other) const;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** descriptor type */
    Descriptor_Type & descriptor_type{data_short};

    /** device descriptor */
    Device_Descriptor_Data device_descriptor{};
};

}
