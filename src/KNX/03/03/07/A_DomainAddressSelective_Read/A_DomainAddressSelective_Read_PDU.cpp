// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_DomainAddressSelective_Read/A_DomainAddressSelective_Read_PDU.h>

#include <cassert>

namespace KNX {

A_DomainAddressSelective_Read_PDU::A_DomainAddressSelective_Read_PDU() :
    APDU(APCI::A_DomainAddressSelective_Read)
{
}

void A_DomainAddressSelective_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    APDU::fromData(first, first + 2);
    first += 2;

    asdu.assign(first, last);

#if 0
    type = *first++;
    assert(type < 2);

    if (type == 0) {
        // 2 byte DoA
        domain_address = *first++;
        range = *first++;
    }
    if (type == 1) {
        // 6 byte DoA
        std::copy(first, first + 6, std::begin(domain_address_start));
        first += 6;
        std::copy(first, first + 6, std::begin(domain_address_end));
        first += 6;
        reserved = *first++;
    }

    assert(first == last);
#endif
}

std::vector<uint8_t> A_DomainAddressSelective_Read_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.insert(std::cend(data), std::cbegin(asdu), std::cend(asdu));

#if 0
    data.push_back(type);
    if (type == 0) {
        // 2 byte DoA
        assert((domain_address >> 8) == 0);
        data.push_back(domain_address & 0xff);
        data.push_back(range);
    }
    if (type == 1) {
        // 6 byte DoA
        data.insert(std::cend(data), std::cbegin(domain_address_start), std::cend(domain_address_start));
        data.insert(std::cend(data), std::cbegin(domain_address_end), std::cend(domain_address_end));
        data.push_back(reserved);
    }
#endif

    return data;
}

uint8_t A_DomainAddressSelective_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        static_cast<uint8_t>(asdu.size());

#if 0
    if (type == 0) {
        return
            APDU::length_calculated() +
            1 + 1 + 1;
    }
    if (type == 1) {
        return
            APDU::length_calculated() +
            1 + 6 + 6 + 1;
    }
    assert(false);
#endif
}

}
