// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Domain_Address_2.h>
#include <KNX/03/03/07/Domain_Address_6.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_DomainAddressSelective_Read
 *
 * @ingroup KNX_03_03_07_03_03_05
 * @ingroup KNX_03_05_02_02_12
 */
class KNX_EXPORT A_DomainAddressSelective_Read_PDU : public APDU
{
public:
    A_DomainAddressSelective_Read_PDU();

    bool operator==(const A_DomainAddressSelective_Read_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** ASDU */
    std::vector<uint8_t> asdu{};

#if 0
    /** Type */
    uint8_t type{};

    /** Type 0: Domain Address */
    Domain_Address_2 domain_address{}; // @note High byte must be 0x00. It overlaps with type=0x00.

    /** Type 0: Range */
    uint8_t range{};

    /** Type 1: Domain Address Start */
    Domain_Address_6 domain_address_start{};

    /** Type 1: Domain Address End */
    Domain_Address_6 domain_address_end{};

    /** Type 1: reserved */
    uint8_t reserved{};
#endif

    // This is what 3/3/7 specs:
    // octet 8..n: ASDU

    // This is what 3/5/2 specs:

    // Type 00h - single octet DoA
    // octet 8: type = 00h
    // octet 9: domain_address (1 byte)
    // octet 10..11: start_address (2 bytes)
    // octer 12: range

    // Type 01h - 6 byte DoA
    // octet 8: type = 01h
    // octet 9..14: domain_address_start (6 bytes)
    // octet 15..20: domain_address_end (6 bytes)
    // octet 21: reserved

    // This is what 8/3/7 tests:

    // octet 8..9: domain_address (2 bytes), always with high byte=0x00
    // octet 10..11: start_address
    // octet 12: range
};

}
