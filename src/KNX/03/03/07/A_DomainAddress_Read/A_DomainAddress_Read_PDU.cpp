// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_DomainAddress_Read/A_DomainAddress_Read_PDU.h>

namespace KNX {

A_DomainAddress_Read_PDU::A_DomainAddress_Read_PDU() :
    APDU(APCI::A_DomainAddress_Read)
{
}

}
