// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Domain_Address.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_DomainAddress_Response
 *
 * @ingroup KNX_03_03_07_03_03_04
 * @ingroup KNX_03_05_02_02_12
 *
 * @todo Specifications 3/3/7 and 3/5/2 are contradicting. 3/3/7 implemented.
 */
class KNX_EXPORT A_DomainAddress_Response_PDU : public APDU
{
public:
    A_DomainAddress_Response_PDU();

    bool operator==(const A_DomainAddress_Response_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Domain Address */
    Domain_Address domain_address{};

    // This is what 3/3/7 specs:

    // 2 octet DoA
    // octet 8..9: domain_address (2 bytes)

    // 6 octet DoA
    // octet 8..13: domain_address (6 bytes)

    // This is what 3/5/2 specs:

    // type 0 (1 octet DoA)
    // octet 8: type = 00h
    // octet 9: domain_address (1 byte)

    // type 1 (6 octet DoA)
    // octet 8: type = 01h
    // octet 9..14: domain_address (6 bytes)

    // This is what 8/3/7 tests:
    // type 0
    // octet 8..9: domain_address (2 byte), always with high byte=0x00
};

}
