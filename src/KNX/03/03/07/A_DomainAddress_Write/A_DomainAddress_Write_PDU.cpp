// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_DomainAddress_Write/A_DomainAddress_Write_PDU.h>

#include <cassert>

namespace KNX {

A_DomainAddress_Write_PDU::A_DomainAddress_Write_PDU() :
    APDU(APCI::A_DomainAddress_Write)
{
}

void A_DomainAddress_Write_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 4);

    APDU::fromData(first, first + 2);
    first += 2;

    domain_address.fromData(first, last);
    assert((domain_address.size() == 2) || (domain_address.size() == 6));
}

std::vector<uint8_t> A_DomainAddress_Write_PDU::toData() const
{
    assert((domain_address.size() == 2) || (domain_address.size() == 6));

    std::vector<uint8_t> data = APDU::toData();

    const std::vector<uint8_t> domain_address_data = domain_address.toData();
    data.insert(std::cend(data), std::cbegin(domain_address_data), std::cend(domain_address_data));

    return data;
}

uint8_t A_DomainAddress_Write_PDU::length_calculated() const
{
    assert((domain_address.size() == 2) || (domain_address.size() == 6));

    return
        APDU::length_calculated() +
        static_cast<uint8_t>(domain_address.size());
}

}
