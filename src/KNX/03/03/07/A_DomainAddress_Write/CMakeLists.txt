# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/A_DomainAddress_Write_PDU.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/A_DomainAddress_Write_PDU.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/A_DomainAddress_Write_PDU.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/03/07/A_DomainAddress_Write)
