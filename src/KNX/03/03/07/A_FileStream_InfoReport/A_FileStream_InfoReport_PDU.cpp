// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_FileStream_InfoReport/A_FileStream_InfoReport_PDU.h>

#include <cassert>

namespace KNX {

A_FileStream_InfoReport_PDU::A_FileStream_InfoReport_PDU() :
    APDU(APCI::A_FileStream_InfoReport)
{
}

void A_FileStream_InfoReport_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 3);

    APDU::fromData(first, first + 2);
    first += 2;

    const uint8_t file = *first++;
    file_handle = file >> 4;
    file_block_sequence_number = file & 0xff;

    file_block.assign(first, last);
}

std::vector<uint8_t> A_FileStream_InfoReport_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back((file_handle << 4) | file_block_sequence_number);
    data.insert(std::cend(data), std::cbegin(file_block), std::cend(file_block));

    return data;
}

uint8_t A_FileStream_InfoReport_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        2 +
        static_cast<uint8_t>(file_block.size());
}

}
