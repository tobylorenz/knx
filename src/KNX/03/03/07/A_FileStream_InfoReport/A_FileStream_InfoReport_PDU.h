// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/File_Block.h>
#include <KNX/03/03/07/File_Block_Sequence_Number.h>
#include <KNX/03/03/07/File_Handle.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * A_FileStream_InfoReport
 *
 * @ingroup KNX_03_03_07_03_04_02_03
 */
class KNX_EXPORT A_FileStream_InfoReport_PDU : public APDU
{
public:
    A_FileStream_InfoReport_PDU();

    bool operator==(const A_FileStream_InfoReport_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** File Handle */
    File_Handle file_handle{};

    /** File Block Sequence Number */
    File_Block_Sequence_Number file_block_sequence_number{};

    /** File Block */
    File_Block file_block{};
};

}
