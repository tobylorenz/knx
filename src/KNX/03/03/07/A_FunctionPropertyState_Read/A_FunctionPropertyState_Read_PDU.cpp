// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_FunctionPropertyState_Read/A_FunctionPropertyState_Read_PDU.h>

#include <cassert>

namespace KNX {

A_FunctionPropertyState_Read_PDU::A_FunctionPropertyState_Read_PDU() :
    APDU(APCI::A_FunctionPropertyState_Read)
{
}

void A_FunctionPropertyState_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 4);

    APDU::fromData(first, first + 2);
    first += 2;

    object_index = *first++;

    property_id = *first++;

    data.assign(first, last);
}

std::vector<uint8_t> A_FunctionPropertyState_Read_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(object_index);
    data.push_back(property_id);
    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

uint8_t A_FunctionPropertyState_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        2 +
        static_cast<uint8_t>(data.size());
}

}
