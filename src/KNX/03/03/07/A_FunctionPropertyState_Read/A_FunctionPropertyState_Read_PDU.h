// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Object_Index.h>
#include <KNX/03/03/07/Property_Value.h>
#include <KNX/03/03/07/Property_Id.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_FunctionPropertyState_Read
 *
 * @ingroup KNX_03_03_07_03_04_05_02
 */
class KNX_EXPORT A_FunctionPropertyState_Read_PDU : public APDU
{
public:
    A_FunctionPropertyState_Read_PDU();

    bool operator==(const A_FunctionPropertyState_Read_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Object Index */
    Object_Index object_index{};

    /** Property ID */
    Property_Id property_id{};

    /** Data */
    Property_Value  data{};
};

}
