// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_GroupPropValue_Read/A_GroupPropValue_Read_PDU.h>

namespace KNX {

A_GroupPropValue_Read_PDU::A_GroupPropValue_Read_PDU() :
    APDU(APCI::A_GroupPropValue_Read)
{
}

void A_GroupPropValue_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 6);

    APDU::fromData(first, first + 2);
    first += 2;

    object_type = (*first++ << 8) | *first++;

    object_instance = *first++;

    property_id = *first++;

    assert(first == last);
}

std::vector<uint8_t> A_GroupPropValue_Read_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(object_type >> 8);
    data.push_back(object_type & 0xff);

    data.push_back(object_instance);

    data.push_back(property_id);

    return data;
}

uint8_t A_GroupPropValue_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        4;
}

}
