// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_GroupValue_Read/A_GroupValue_Read_PDU.h>

namespace KNX {

A_GroupValue_Read_PDU::A_GroupValue_Read_PDU() :
    APDU(APCI::A_GroupValue_Read)
{
}

}
