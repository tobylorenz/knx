// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_GroupValue_Response/A_GroupValue_Response_PDU.h>

#include <cassert>

namespace KNX {

A_GroupValue_Response_PDU::A_GroupValue_Response_PDU() :
    APDU(APCI::A_GroupValue_Response)
{
}

void A_GroupValue_Response_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);
    assert(std::distance(first, last) <= 16);

    APDU::fromData(first, first + 2);
    first += 2;

    data_long.assign(first, last);
}

std::vector<uint8_t> A_GroupValue_Response_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.insert(std::cend(data), std::cbegin(data_long), std::cend(data_long));

    return data;
}

uint8_t A_GroupValue_Response_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        static_cast<uint8_t>(data_long.size());
}

Group_Value A_GroupValue_Response_PDU::data() const
{
    if (data_long.size() == 0) {
        std::vector<uint8_t> data;
        data.push_back(data_short);
        return data;
    }

    return data_long;
}

void A_GroupValue_Response_PDU::set_data(const Group_Value & data)
{
    if ((data.size() == 1) && ((data[0] & 0xc0) == 0)) {
        data_short = data[0] & 0x3f;
        data_long.clear();
    } else {
        data_short = 0;
        data_long = data;
    }
}

}
