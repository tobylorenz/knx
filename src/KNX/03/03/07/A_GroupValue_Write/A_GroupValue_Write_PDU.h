// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Group_Value.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_GroupValue_Write
 *
 * @ingroup KNX_03_03_07_03_01_03
 */
class KNX_EXPORT A_GroupValue_Write_PDU : public APDU
{
public:
    A_GroupValue_Write_PDU();

    bool operator==(const A_GroupValue_Write_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Data */
    std::vector<uint8_t> data_long{};

    virtual Group_Value data() const;
    virtual void set_data(const Group_Value & data);
};

}
