// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_IndividualAddressSerialNumber_Read/A_IndividualAddressSerialNumber_Read_PDU.h>

#include <cassert>

namespace KNX {

A_IndividualAddressSerialNumber_Read_PDU::A_IndividualAddressSerialNumber_Read_PDU() :
    APDU(APCI::A_IndividualAddressSerialNumber_Read)
{
}

void A_IndividualAddressSerialNumber_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 8);

    APDU::fromData(first, first + 2);
    first += 2;

    std::copy(first, first + 6, std::begin(serial_number.serial_number));
    first += 6;

    assert(first == last);
}

std::vector<uint8_t> A_IndividualAddressSerialNumber_Read_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.insert(std::cend(data), std::cbegin(serial_number.serial_number), std::cend(serial_number.serial_number));

    return data;
}

uint8_t A_IndividualAddressSerialNumber_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        static_cast<uint8_t>(serial_number.serial_number.size());
}

}
