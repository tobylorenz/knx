// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_IndividualAddressSerialNumber_Response/A_IndividualAddressSerialNumber_Response_PDU.h>

#include <cassert>

namespace KNX {

A_IndividualAddressSerialNumber_Response_PDU::A_IndividualAddressSerialNumber_Response_PDU() :
    APDU(APCI::A_IndividualAddressSerialNumber_Response)
{
}

void A_IndividualAddressSerialNumber_Response_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 12);

    APDU::fromData(first, first + 2);
    first += 2;

    std::copy(first, first + 6, std::begin(serial_number.serial_number));
    first += 6;

    domain_address.fromData(first, first + 2);
    first += 2;

    first += 2; // reserved

    assert(first == last);
}

std::vector<uint8_t> A_IndividualAddressSerialNumber_Response_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.insert(std::cend(data), std::cbegin(serial_number.serial_number), std::cend(serial_number.serial_number));

    const std::vector<uint8_t> domain_address_data = domain_address.toData();
    data.insert(std::cend(data), std::cbegin(domain_address_data), std::cend(domain_address_data));

    data.push_back(0);
    data.push_back(0);

    return data;
}

uint8_t A_IndividualAddressSerialNumber_Response_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        static_cast<uint8_t>(serial_number.serial_number.size()) +
        static_cast<uint8_t>(domain_address.size()) +
        2;
}

}
