// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_IndividualAddress_Read
 *
 * @ingroup KNX_03_03_07_03_02_03
 */
class KNX_EXPORT A_IndividualAddress_Read_PDU : public APDU
{
public:
    A_IndividualAddress_Read_PDU();

    bool operator==(const A_IndividualAddress_Read_PDU & other) const = default;
};

}
