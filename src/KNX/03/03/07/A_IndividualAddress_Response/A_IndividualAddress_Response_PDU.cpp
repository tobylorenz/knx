// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_IndividualAddress_Response/A_IndividualAddress_Response_PDU.h>

namespace KNX {

A_IndividualAddress_Response_PDU::A_IndividualAddress_Response_PDU() :
    APDU(APCI::A_IndividualAddress_Response)
{
}

}
