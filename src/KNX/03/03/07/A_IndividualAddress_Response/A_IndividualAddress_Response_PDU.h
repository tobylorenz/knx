// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_IndividualAddress_Response
 *
 * @ingroup KNX_03_03_07_03_02_03
 * @note This spec doesn't show the individual address, but tests show.
 */
class KNX_EXPORT A_IndividualAddress_Response_PDU : public APDU
{
public:
    A_IndividualAddress_Response_PDU();

    bool operator==(const A_IndividualAddress_Response_PDU & other) const = default;
};

}
