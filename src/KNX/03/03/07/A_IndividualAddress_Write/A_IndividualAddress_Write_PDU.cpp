// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_IndividualAddress_Write/A_IndividualAddress_Write_PDU.h>

#include <cassert>

namespace KNX {

A_IndividualAddress_Write_PDU::A_IndividualAddress_Write_PDU() :
    APDU(APCI::A_IndividualAddress_Write)
{
}

void A_IndividualAddress_Write_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    APDU::fromData(first, first + 2);
    first += 2;

    newaddress.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> A_IndividualAddress_Write_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(newaddress >> 8);
    data.push_back(newaddress & 0xff);

    return data;
}

uint8_t A_IndividualAddress_Write_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        2;
}

}
