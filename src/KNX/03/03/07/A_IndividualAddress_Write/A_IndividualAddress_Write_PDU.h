// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_IndividualAddress_Write
 *
 * @ingroup KNX_03_03_07_03_02_02
 */
class KNX_EXPORT A_IndividualAddress_Write_PDU : public APDU
{
public:
    A_IndividualAddress_Write_PDU();

    bool operator==(const A_IndividualAddress_Write_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** new address */
    Individual_Address newaddress{};
};

}
