// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Key_Response/A_Key_Response_PDU.h>

#include <cassert>

namespace KNX {

A_Key_Response_PDU::A_Key_Response_PDU() :
    APDU(APCI::A_Key_Response)
{
}

void A_Key_Response_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 3);

    APDU::fromData(first, first + 2);
    first += 2;

    level = *first++;

    assert(first == last);
}

std::vector<uint8_t> A_Key_Response_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(level);

    return data;
}

uint8_t A_Key_Response_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        1;
}

}
