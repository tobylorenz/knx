// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Key_Write/A_Key_Write_PDU.h>

#include <cassert>

namespace KNX {

A_Key_Write_PDU::A_Key_Write_PDU() :
    APDU(APCI::A_Key_Write)
{
}

void A_Key_Write_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 7);

    APDU::fromData(first, first + 2);
    first += 2;

    level = *first++;

    std::copy(first, first + 4, std::begin(key));
    first += 4;

    assert(first == last);
}

std::vector<uint8_t> A_Key_Write_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(level);
    data.insert(std::cend(data), std::cbegin(key), std::cend(key));

    return data;
}

uint8_t A_Key_Write_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        1 +
        static_cast<uint8_t>(key.size());
}

}
