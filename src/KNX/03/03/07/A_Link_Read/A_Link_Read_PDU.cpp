// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Link_Read/A_Link_Read_PDU.h>

#include <cassert>

namespace KNX {

A_Link_Read_PDU::A_Link_Read_PDU() :
    APDU(APCI::A_Link_Read)
{
}

void A_Link_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    APDU::fromData(first, first + 2);
    first += 2;

    group_object_number = Group_Object_Number(*first++);

    start_index = *first & 0x0f;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> A_Link_Read_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(group_object_number);
    data.push_back(start_index);

    return data;
}

uint8_t A_Link_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        2;
}

}
