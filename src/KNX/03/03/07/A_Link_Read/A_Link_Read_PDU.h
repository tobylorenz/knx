// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Group_Object_Number.h>
#include <KNX/03/03/07/Link_Start_Index.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Link_Read
 *
 * @ingroup KNX_03_03_07_03_04_04_01
 */
class KNX_EXPORT A_Link_Read_PDU : public APDU
{
public:
    A_Link_Read_PDU();

    bool operator==(const A_Link_Read_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Group Object Number */
    Group_Object_Number group_object_number{};

    /** Start Index */
    Link_Start_Index start_index{};
};

}
