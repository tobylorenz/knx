// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Link_Response/A_Link_Response_PDU.h>

#include <cassert>

namespace KNX {

A_Link_Response_PDU::A_Link_Response_PDU() :
    APDU(APCI::A_Link_Response)
{
}

void A_Link_Response_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 4);
    assert(std::distance(first, last) <= 16);

    APDU::fromData(first, first + 2);
    first += 2;

    group_object_number = Group_Object_Number(*first++);

    sending_address = *first >> 4;
    start_index = *first & 0x0f;
    ++first;

    while (first != last) {
        group_address_list.push_back(Group_Address((*first++ << 8) | *first++));
    }

    assert(first == last);
}

std::vector<uint8_t> A_Link_Response_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(group_object_number);
    data.push_back((sending_address << 4) | start_index);
    for (const Group_Address & group_address : group_address_list) {
        data.push_back(group_address >> 8);
        data.push_back(group_address & 0xff);
    }

    return data;
}

uint8_t A_Link_Response_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        2 +
        static_cast<uint8_t>(group_address_list.size() * 2);
}

}
