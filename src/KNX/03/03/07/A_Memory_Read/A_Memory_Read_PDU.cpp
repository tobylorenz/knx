// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Memory_Read/A_Memory_Read_PDU.h>

#include <cassert>

namespace KNX {

A_Memory_Read_PDU::A_Memory_Read_PDU() :
    APDU(APCI::A_Memory_Read)
{
}

bool A_Memory_Read_PDU::operator==(const A_Memory_Read_PDU & other) const
{
    return APDU::operator==(other) && (address == other.address);
}

void A_Memory_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    /* octet 6..7 */
    APDU::fromData(first, first + 2);
    first += 2;

    /* octet 8..9 */
    address = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> A_Memory_Read_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    /* octet 8..9 */
    data.push_back(address >> 8);
    data.push_back(address & 0xff);

    return data;
}

uint8_t A_Memory_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        2;
}

}
