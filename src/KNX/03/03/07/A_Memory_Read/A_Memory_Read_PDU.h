// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Memory_Address.h>
#include <KNX/03/03/07/Memory_Number.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * A_Memory_Read
 *
 * @ingroup KNX_03_03_07_03_05_03
 */
class KNX_EXPORT A_Memory_Read_PDU : public APDU
{
public:
    A_Memory_Read_PDU();

    bool operator==(const A_Memory_Read_PDU & other) const;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Number */
    Memory_Number & number{data_short};

    /** Address */
    Memory_Address address{};
};

}
