// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Memory_Write/A_Memory_Write_PDU.h>

#include <cassert>

namespace KNX {

A_Memory_Write_PDU::A_Memory_Write_PDU() :
    APDU(APCI::A_Memory_Write)
{
}

bool A_Memory_Write_PDU::operator==(const A_Memory_Write_PDU & other) const
{
    return APDU::operator==(other) && (address == other.address) && (data == other.data);
}

void A_Memory_Write_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 4);

    APDU::fromData(first, first + 2);
    first += 2;

    address = (*first++ << 8) | *first++;

    data.assign(first, last);
}

std::vector<uint8_t> A_Memory_Write_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(address >> 8);
    data.push_back(address & 0xff);
    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

uint8_t A_Memory_Write_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        2 +
        static_cast<uint8_t>(data.size());
}

}
