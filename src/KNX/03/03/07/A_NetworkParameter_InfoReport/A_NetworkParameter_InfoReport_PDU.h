// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/A_NetworkParameter_Response/A_NetworkParameter_Response_PDU.h>

namespace KNX {

/**
 * A_NetworkParameter_InfoReport
 *
 * @ingroup KNX_03_03_07_03_02_08
 */
using A_NetworkParameter_InfoReport_PDU = A_NetworkParameter_Response_PDU;

}
