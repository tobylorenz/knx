// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_NetworkParameter_Read/A_NetworkParameter_Read_PDU.h>

#include <cassert>

namespace KNX {

A_NetworkParameter_Read_PDU::A_NetworkParameter_Read_PDU() :
    APDU(APCI::A_NetworkParameter_Read)
{
}

void A_NetworkParameter_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 5);

    APDU::fromData(first, first + 2);
    first += 2;

    parameter_type.object_type = (*first++ << 8) | *first++;

    parameter_type.pid = *first++;

    test_info.assign(first, last);
}

std::vector<uint8_t> A_NetworkParameter_Read_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(parameter_type.object_type >> 8);
    data.push_back(parameter_type.object_type & 0xff);
    data.push_back(parameter_type.pid);
    data.insert(std::cend(data), std::cbegin(test_info), std::cend(test_info));

    return data;
}

uint8_t A_NetworkParameter_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        3 +
        static_cast<uint8_t>(test_info.size());
}

}
