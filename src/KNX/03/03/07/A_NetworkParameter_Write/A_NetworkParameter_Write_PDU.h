// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Parameter_Type.h>
#include <KNX/03/03/07/Parameter_Value.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_NetworkParameter_Write
 *
 * @ingroup KNX_03_03_07_03_02_07
 */
class KNX_EXPORT A_NetworkParameter_Write_PDU : public APDU
{
public:
    A_NetworkParameter_Write_PDU();

    bool operator==(const A_NetworkParameter_Write_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Parameter Type */
    Parameter_Type parameter_type{};

    /** Value */
    Parameter_Value value{};
};

}
