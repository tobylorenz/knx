// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Open_Routing_Table_Req/A_Open_Routing_Table_Req_PDU.h>

namespace KNX {

A_Open_Routing_Table_Req_PDU::A_Open_Routing_Table_Req_PDU() :
    APDU(APCI::A_Open_Routing_Table_Req)
{
}

}
