// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Open_Routing_Table_Req
 * M_LC_TAB_MEM_ENABLE / LCExtMemOpen
 * LcTabMemEnable
 *
 * @note This is reverse engineered from System Conformance Test 8.3.7
 *
 * @ingroup KNX_08_03_07_02_24
 */
class KNX_EXPORT A_Open_Routing_Table_Req_PDU : public APDU
{
public:
    A_Open_Routing_Table_Req_PDU();

    bool operator==(const A_Open_Routing_Table_Req_PDU & other) const = default;
};

}
