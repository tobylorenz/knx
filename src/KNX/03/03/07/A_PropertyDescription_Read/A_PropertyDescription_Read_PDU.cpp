// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_PropertyDescription_Read/A_PropertyDescription_Read_PDU.h>

#include <cassert>

namespace KNX {

A_PropertyDescription_Read_PDU::A_PropertyDescription_Read_PDU() :
    APDU(APCI::A_PropertyDescription_Read)
{
}

void A_PropertyDescription_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    /* octet 6..7 */
    APDU::fromData(first, first + 2);
    first += 2;

    /* octet 8 */
    object_index = *first++;

    /* octet 9 */
    property_id = *first++;

    /* octet 10 */
    property_index = *first++;

    assert(first == last);
}

std::vector<uint8_t> A_PropertyDescription_Read_PDU::toData() const
{
    /* octet 6..7 */
    std::vector<uint8_t> data = APDU::toData();

    /* octet 8 */
    data.push_back(object_index);

    /* octet 9 */
    data.push_back(property_id);

    /* octet 10 */
    data.push_back(property_index);

    return data;
}

uint8_t A_PropertyDescription_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        3;
}

}
