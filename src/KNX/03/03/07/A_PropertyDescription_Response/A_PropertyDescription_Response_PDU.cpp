// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_PropertyDescription_Response/A_PropertyDescription_Response_PDU.h>

#include <cassert>

namespace KNX {

A_PropertyDescription_Response_PDU::A_PropertyDescription_Response_PDU() :
    APDU(APCI::A_PropertyDescription_Response)
{
}

bool A_PropertyDescription_Response_PDU::operator==(const A_PropertyDescription_Response_PDU & other) const
{
    return APDU::operator==(other) && (object_index == other.object_index) && (property_id == other.property_id) && (property_index == other.property_index) && (write_enable == other.write_enable) && (type == other.type) && (max_nr_of_elem == other.max_nr_of_elem) && (access == other.access);
}

void A_PropertyDescription_Response_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 9);

    /* octet 6..7 */
    APDU::fromData(first, first + 2);
    first += 2;

    /* octet 8 */
    object_index = *first++;

    /* octet 9 */
    property_id = *first++;

    /* octet 10 */
    property_index = *first++;

    /* octet 11 */
    write_enable = *first >> 7;
    type = *first & 0x3f;
    ++first;

    /* octet 12..13 */
    max_nr_of_elem = ((*first++ & 0x0f) << 8) | *first++;

    /* octet 14 */
    access.read_level = *first >> 4;
    access.write_level = *first & 0x0f;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> A_PropertyDescription_Response_PDU::toData() const
{
    /* octet 6..7 */
    std::vector<uint8_t> data = APDU::toData();

    /* octet 8 */
    data.push_back(object_index);

    /* octet 9 */
    data.push_back(property_id);

    /* octet 10 */
    data.push_back(property_index);

    /* octet 11 */
    data.push_back(
        static_cast<uint8_t>(write_enable << 7) |
        type);

    /* octet 12..13 */
    data.push_back(max_nr_of_elem >> 8);
    data.push_back(max_nr_of_elem & 0xff);

    /* octet 14 */
    data.push_back(
        (access.read_level << 4) |
        (access.write_level & 0xf));

    return data;
}

uint8_t A_PropertyDescription_Response_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        7;
}

}
