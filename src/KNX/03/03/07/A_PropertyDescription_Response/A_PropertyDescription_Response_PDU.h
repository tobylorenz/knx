// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Access.h>
#include <KNX/03/03/07/Max_Nr_Of_Elem.h>
#include <KNX/03/03/07/Object_Index.h>
#include <KNX/03/03/07/Property_Id.h>
#include <KNX/03/03/07/Property_Index.h>
#include <KNX/03/03/07/Property_Type.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_PropertyDescription_Response
 *
 * @ingroup KNX_03_03_07_03_04_03_03
 */
class KNX_EXPORT A_PropertyDescription_Response_PDU : public APDU
{
public:
    A_PropertyDescription_Response_PDU();

    bool operator==(const A_PropertyDescription_Response_PDU & other) const;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Object Index */
    Object_Index object_index{};

    /** Property ID */
    Property_Id property_id{};

    /** Property Index */
    Property_Index property_index{};

    /** Write Enable */
    bool write_enable{false};

    /** Type */
    Property_Type type{};

    /** Max Number of Elements */
    Max_Nr_Of_Elem max_nr_of_elem{};

    /** Access */
    Access access{};
};

}
