// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_PropertyValue_Read/A_PropertyValue_Read_PDU.h>

#include <cassert>

namespace KNX {

A_PropertyValue_Read_PDU::A_PropertyValue_Read_PDU() :
    APDU(APCI::A_PropertyValue_Read)
{
}

void A_PropertyValue_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    /* octet 6..7 */
    APDU::fromData(first, first + 2);
    first += 2;

    /* octet 8 */
    object_index = *first++;

    /* octet 9 */
    property_id = *first++;

    /* octet 10..11 */
    nr_of_elem = *first >> 4;
    start_index = ((*first++ & 0x0f) << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> A_PropertyValue_Read_PDU::toData() const
{
    /* octet 6..7 */
    std::vector<uint8_t> data = APDU::toData();

    /* octet 8 */
    data.push_back(object_index);

    /* octet 9 */
    data.push_back(property_id);

    /* octet 10 */
    data.push_back((nr_of_elem << 4) | (start_index >> 8));

    /* octet 11 */
    data.push_back(start_index & 0xff);

    return data;
}

uint8_t A_PropertyValue_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        4;
}

}
