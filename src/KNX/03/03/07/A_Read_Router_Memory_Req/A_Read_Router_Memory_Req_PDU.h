// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Read_Router_Memory_Req
 * M_LC_SLAVE_READ / LCSlaveMemRead
 * RouterMemoryRead
 *
 * @note This is reverse engineered from System Conformance Test 8.3.7
 *
 * @ingroup KNX_08_03_07_02_24
 */
class KNX_EXPORT A_Read_Router_Memory_Req_PDU : public APDU
{
public:
    A_Read_Router_Memory_Req_PDU();

    bool operator==(const A_Read_Router_Memory_Req_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Count */
    uint8_t count{};

    /** Address */
    uint16_t address{};
};

}
