// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Read_Router_Memory_Res/A_Read_Router_Memory_Res_PDU.h>

#include <cassert>

namespace KNX {

A_Read_Router_Memory_Res_PDU::A_Read_Router_Memory_Res_PDU() :
    APDU(APCI::A_Read_Router_Memory_Res)
{
}

void A_Read_Router_Memory_Res_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 5);

    APDU::fromData(first, first + 2);
    first += 2;

    count = *first++;

    address = (*first++ << 8) | *first++;

    data.assign(first, last);
}

std::vector<uint8_t> A_Read_Router_Memory_Res_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(count);

    data.push_back(address >> 8);
    data.push_back(address & 0xff);

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

uint8_t A_Read_Router_Memory_Res_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        3 +
        count;
}

}
