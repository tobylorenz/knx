// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Read_Router_Status_Req/A_Read_Router_Status_Req_PDU.h>

#include <cassert>

namespace KNX {

A_Read_Router_Status_Req_PDU::A_Read_Router_Status_Req_PDU() :
    APDU(APCI::A_Read_Router_Status_Req)
{
}

void A_Read_Router_Status_Req_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    APDU::fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> A_Read_Router_Status_Req_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    return data;
}

uint8_t A_Read_Router_Status_Req_PDU::length_calculated() const
{
    return
        APDU::length_calculated();
}

}
