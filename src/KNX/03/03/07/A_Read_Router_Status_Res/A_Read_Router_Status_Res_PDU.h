// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Read_Router_Status_Res
 * M_LC_GROUP_RESPONSE
 *
 * @todo Nothing is known yet. Implementation just copies A_Read_Router_Memory and A_Read_Routing_Table.
 */
class KNX_EXPORT A_Read_Router_Status_Res_PDU : public APDU
{
public:
    A_Read_Router_Status_Res_PDU();

    bool operator==(const A_Read_Router_Status_Res_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Route Table State */
    uint8_t route_table_state{};
};

}
