// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Restart/A_Restart_PDU.h>

#include <cassert>

namespace KNX {

A_Restart_PDU::A_Restart_PDU() :
    APDU(APCI::A_Restart)
{
}

bool A_Restart_PDU::operator==(const A_Restart_PDU & other) const
{
    return APDU::operator==(other) && (erase_code == other.erase_code) && (channel_number == other.channel_number);
}

void A_Restart_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);
    assert(std::distance(first, last) <= 4);

    APDU::fromData(first, first + 2);
    first += 2;

    if (restart_type == Master_Restart) {
        erase_code = static_cast<Restart_Erase_Code>(*first++);
        channel_number = *first++;
    }

    assert(first == last);
}

std::vector<uint8_t> A_Restart_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    if (restart_type == Master_Restart) {
        data.push_back(static_cast<uint8_t>(erase_code));
        data.push_back(channel_number);
    }

    return data;
}

uint8_t A_Restart_PDU::length_calculated() const
{
    uint8_t retval = APDU::length_calculated();

    if (restart_type == Master_Restart) {
        retval += 2;
    }

    return retval;
}

}
