// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Restart_Channel_Number.h>
#include <KNX/03/03/07/Restart_Erase_Code.h>
#include <KNX/03/03/07/Restart_Type.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Restart
 *
 * @ingroup KNX_03_03_07_03_04_02_02
 */
class KNX_EXPORT A_Restart_PDU : public APDU
{
public:
    A_Restart_PDU();

    bool operator==(const A_Restart_PDU & other) const;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Restart Type */
    Restart_Type & restart_type{data_short};

    /** Erase Code */
    Restart_Erase_Code erase_code{};

    /** Channel Number */
    Restart_Channel_Number channel_number{};
};

}
