// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Restart_Response/A_Restart_Response_PDU.h>

#include <cassert>

namespace KNX {

A_Restart_Response_PDU::A_Restart_Response_PDU() :
    APDU(APCI::A_Restart_Response)
{
    data_short = 0x21; // response | restart_type
}

void A_Restart_Response_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    APDU::fromData(first, first + 2);
    first += 2;

    error_code = static_cast<Restart_Error_Code>(*first++);

    process_time = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> A_Restart_Response_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(static_cast<uint8_t>(error_code));
    data.push_back(process_time >> 8);
    data.push_back(process_time & 0xff);

    return data;
}

uint8_t A_Restart_Response_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        3;
}

}
