// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/Restart_Error_Code.h>
#include <KNX/03/03/07/Restart_Process_Time.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Restart_Response
 *
 * @ingroup KNX_03_03_07_03_04_02_02
 */
class KNX_EXPORT A_Restart_Response_PDU : public APDU
{
public:
    A_Restart_Response_PDU();

    bool operator==(const A_Restart_Response_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Error Code */
    Restart_Error_Code error_code{Restart_Error_Code::No_Error};

    /** Process Time */
    Restart_Process_Time process_time{};
};

}
