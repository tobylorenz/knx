// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_ServiceInformation_Indication/A_ServiceInformation_Indication_PDU.h>

#include <cassert>

namespace KNX {

A_ServiceInformation_Indication_PDU::A_ServiceInformation_Indication_PDU() :
    APDU(APCI::A_ServiceInformation_Indication)
{
}

void A_ServiceInformation_Indication_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    APDU::fromData(first, first + 2);
    first += 2;

    info = (*first++ << 16) | (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> A_ServiceInformation_Indication_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back((info >> 16) & 0xff);
    data.push_back((info >> 8) & 0xff);
    data.push_back(info & 0xff);

    return data;
}

uint8_t A_ServiceInformation_Indication_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        3;
}

}
