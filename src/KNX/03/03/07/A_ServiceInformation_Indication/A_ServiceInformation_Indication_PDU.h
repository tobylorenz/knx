// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_ServiceInformation_Indication
 *
 * @note This is reverse engineered from System Conformance Test 8.3.7
 *
 * @ingroup KNX_08_03_07_02_25
 */
class KNX_EXPORT A_ServiceInformation_Indication_PDU : public APDU
{
public:
    A_ServiceInformation_Indication_PDU();

    bool operator==(const A_ServiceInformation_Indication_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Info */
    uint24_t info{};
};

}
