// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_Unknown/A_Unknown_PDU.h>

namespace KNX {

A_Unknown_PDU::A_Unknown_PDU(const APCI apci) :
    APDU(apci)
{
}

void A_Unknown_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    APDU::fromData(first, first + 2);
    first += 2;

    data.assign(first, last);
}

std::vector<uint8_t> A_Unknown_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

uint8_t A_Unknown_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        static_cast<uint8_t>(data.size());
}

}
