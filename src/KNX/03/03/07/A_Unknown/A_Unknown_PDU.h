// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Unknown
 *
 * In System Conformance Test EMI-IMI there is in chapter 10.2.1.2 Step 10 a
 * frame tested with an invalid APDU. This doesn't get ind'ed correctly without
 * having a container to carry its data. This is the purpose of A_Unknown_PDU.
 */
class KNX_EXPORT A_Unknown_PDU : public APDU
{
public:
    A_Unknown_PDU(const APCI apci);

    bool operator==(const A_Unknown_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Unknown Data */
    std::vector<uint8_t> data{};
};

}
