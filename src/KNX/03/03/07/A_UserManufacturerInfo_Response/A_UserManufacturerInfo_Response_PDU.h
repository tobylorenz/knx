// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/MFact_Info.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_UserManufacturerInfo_Response
 *
 * @ingroup KNX_03_03_07_03_05_06_05
 */
class KNX_EXPORT A_UserManufacturerInfo_Response_PDU : public APDU
{
public:
    A_UserManufacturerInfo_Response_PDU();

    bool operator==(const A_UserManufacturerInfo_Response_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Manufacturer Info */
    MFact_Info manufacturer_info{};
};

}
