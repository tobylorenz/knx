// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_UserMemoryBit_Write/A_UserMemoryBit_Write_PDU.h>

#include <cassert>

namespace KNX {

A_UserMemoryBit_Write_PDU::A_UserMemoryBit_Write_PDU() :
    APDU(APCI::A_UserMemoryBit_Write)
{
}

void A_UserMemoryBit_Write_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 5);

    APDU::fromData(first, first + 2);
    first += 2;

    number = *first++;

    address = (*first++ << 8) | *first++;

    const uint8_t n = std::distance(first, last);
    if ((n % 2 == 0) | (n / 2 == number)) {
        // valid
        and_data.assign(first, first + number);
        xor_data.assign(first + number, last);
    } else {
        // invalid
        and_data.assign(first, last);
        // no xor_data
    }
}

std::vector<uint8_t> A_UserMemoryBit_Write_PDU::toData() const
{
    std::vector<uint8_t> data = APDU::toData();

    data.push_back(number);
    data.push_back(address >> 8);
    data.push_back(address & 0xff);
    data.insert(std::cend(data), std::cbegin(and_data), std::cend(and_data));
    data.insert(std::cend(data), std::cbegin(xor_data), std::cend(xor_data));

    return data;
}

uint8_t A_UserMemoryBit_Write_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        3 +
        static_cast<uint8_t>(and_data.size()) +
        static_cast<uint8_t>(xor_data.size());
}

}
