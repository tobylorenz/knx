// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_UserMemory_Read/A_UserMemory_Read_PDU.h>

#include <cassert>

namespace KNX {

A_UserMemory_Read_PDU::A_UserMemory_Read_PDU() :
    APDU(APCI::A_UserMemory_Read)
{
}

void A_UserMemory_Read_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    /* octet 6..7 */
    APDU::fromData(first, first + 2);
    first += 2;

    /* octet 8..10 */
    number = *first & 0x0f;
    const uint4_t address_extension = *first >> 4;
    ++first;
    address = (address_extension << 16) | (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> A_UserMemory_Read_PDU::toData() const
{
    /* octet 6..7 */
    std::vector<uint8_t> data = APDU::toData();

    /* octet 8..10 */
    const uint4_t address_extension = address >> 16;
    data.push_back((address_extension << 4) | (number & 0x0f));
    data.push_back((address >> 8) & 0xff);
    data.push_back(address & 0xff);

    return data;
}

uint8_t A_UserMemory_Read_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        3;
}

}
