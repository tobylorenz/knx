// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/A_UserMemory_Response/A_UserMemory_Response_PDU.h>

#include <cassert>

namespace KNX {

A_UserMemory_Response_PDU::A_UserMemory_Response_PDU() :
    APDU(APCI::A_UserMemory_Response)
{
}

void A_UserMemory_Response_PDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 5);

    /* octet 6..7 */
    APDU::fromData(first, first + 2);
    first += 2;

    /* octet 8..10 */
    number = *first & 0x0f;
    const uint4_t address_extension = *first >> 4;
    ++first;
    address = (address_extension << 16) | (*first++ << 8) | (*first++);

    /* octet 11..N */
    data.assign(first, last);
}

std::vector<uint8_t> A_UserMemory_Response_PDU::toData() const
{
    /* octet 6..7 */
    std::vector<uint8_t> data = APDU::toData();

    /* octet 8..10 */
    const uint4_t address_extension = address >> 16;
    data.push_back((address_extension << 4) | (number & 0x0f));
    data.push_back((address >> 8) & 0xff);
    data.push_back(address & 0xff);

    /* octet 11..N */
    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

uint8_t A_UserMemory_Response_PDU::length_calculated() const
{
    return
        APDU::length_calculated() +
        3 +
        static_cast<uint8_t>(data.size());
}

}
