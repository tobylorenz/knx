// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Write_Router_Status_Req
 * M_LC_GROUP_WRITE / LCRouteTableStateWrite
 *
 * @todo Nothing is known yet. Implementation just copies A_Write_Router_Memory and A_Write_Routing_Table.
 */
class KNX_EXPORT A_Write_Router_Status_Req_PDU : public APDU
{
public:
    A_Write_Router_Status_Req_PDU();

    bool operator==(const A_Write_Router_Status_Req_PDU & other) const = default;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Route Table State */
    uint8_t route_table_state{};
};

}
