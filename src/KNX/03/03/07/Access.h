// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/** Access */
struct KNX_EXPORT Access
{
    bool operator==(const Access & other) const = default;

    uint4_t read_level{};
    uint4_t write_level{};
};

}
