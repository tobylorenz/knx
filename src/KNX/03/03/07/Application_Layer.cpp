// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/Application_Layer.h>

#include <KNX/03/03/07/A_GroupValue_Read/A_GroupValue_Read_PDU.h>
#include <KNX/03/03/07/A_GroupValue_Response/A_GroupValue_Response_PDU.h>
#include <KNX/03/03/07/A_GroupValue_Write/A_GroupValue_Write_PDU.h>
#include <KNX/03/03/07/A_IndividualAddress_Write/A_IndividualAddress_Write_PDU.h>
#include <KNX/03/03/07/A_IndividualAddress_Read/A_IndividualAddress_Read_PDU.h>
#include <KNX/03/03/07/A_IndividualAddress_Response/A_IndividualAddress_Response_PDU.h>
#include <KNX/03/03/07/A_ADC_Read/A_ADC_Read_PDU.h>
#include <KNX/03/03/07/A_ADC_Response/A_ADC_Response_PDU.h>
#include <KNX/03/03/07/A_SystemNetworkParameter_Read/A_SystemNetworkParameter_Read_PDU.h>
#include <KNX/03/03/07/A_SystemNetworkParameter_Response/A_SystemNetworkParameter_Response_PDU.h>
#include <KNX/03/03/07/A_SystemNetworkParameter_Write/A_SystemNetworkParameter_Write_PDU.h>
#include <KNX/03/03/07/A_Memory_Read/A_Memory_Read_PDU.h>
#include <KNX/03/03/07/A_Memory_Response/A_Memory_Response_PDU.h>
#include <KNX/03/03/07/A_Memory_Write/A_Memory_Write_PDU.h>
#include <KNX/03/03/07/A_UserMemory_Read/A_UserMemory_Read_PDU.h>
#include <KNX/03/03/07/A_UserMemory_Response/A_UserMemory_Response_PDU.h>
#include <KNX/03/03/07/A_UserMemory_Write/A_UserMemory_Write_PDU.h>
#include <KNX/03/03/07/A_UserMemoryBit_Write/A_UserMemoryBit_Write_PDU.h>
#include <KNX/03/03/07/A_UserManufacturerInfo_Read/A_UserManufacturerInfo_Read_PDU.h>
#include <KNX/03/03/07/A_UserManufacturerInfo_Response/A_UserManufacturerInfo_Response_PDU.h>
#include <KNX/03/03/07/A_FunctionPropertyCommand/A_FunctionPropertyCommand_PDU.h>
#include <KNX/03/03/07/A_FunctionPropertyState_Read/A_FunctionPropertyState_Read_PDU.h>
#include <KNX/03/03/07/A_FunctionPropertyState_Response/A_FunctionPropertyState_Response_PDU.h>
#include <KNX/03/03/07/A_DeviceDescriptor_Read/A_DeviceDescriptor_Read_PDU.h>
#include <KNX/03/03/07/A_DeviceDescriptor_Response/A_DeviceDescriptor_Response_PDU.h>
#include <KNX/03/03/07/A_DeviceDescriptor_InfoReport/A_DeviceDescriptor_InfoReport_PDU.h>
#include <KNX/03/03/07/A_Restart/A_Restart_PDU.h>
#include <KNX/03/03/07/A_Restart_Response/A_Restart_Response_PDU.h>
#include <KNX/03/03/07/A_Open_Routing_Table_Req/A_Open_Routing_Table_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Routing_Table_Req/A_Read_Routing_Table_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Routing_Table_Res/A_Read_Routing_Table_Res_PDU.h>
#include <KNX/03/03/07/A_Write_Routing_Table_Req/A_Write_Routing_Table_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Router_Memory_Req/A_Read_Router_Memory_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Router_Memory_Res/A_Read_Router_Memory_Res_PDU.h>
#include <KNX/03/03/07/A_Write_Router_Memory_Req/A_Write_Router_Memory_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Router_Status_Req/A_Read_Router_Status_Req_PDU.h>
#include <KNX/03/03/07/A_Read_Router_Status_Res/A_Read_Router_Status_Res_PDU.h>
#include <KNX/03/03/07/A_Write_Router_Status_Req/A_Write_Router_Status_Req_PDU.h>
#include <KNX/03/03/07/A_MemoryBit_Write/A_MemoryBit_Write_PDU.h>
#include <KNX/03/03/07/A_Authorize_Request/A_Authorize_Request_PDU.h>
#include <KNX/03/03/07/A_Authorize_Response/A_Authorize_Response_PDU.h>
#include <KNX/03/03/07/A_Key_Write/A_Key_Write_PDU.h>
#include <KNX/03/03/07/A_Key_Response/A_Key_Response_PDU.h>
#include <KNX/03/03/07/A_PropertyValue_Read/A_PropertyValue_Read_PDU.h>
#include <KNX/03/03/07/A_PropertyValue_Response/A_PropertyValue_Response_PDU.h>
#include <KNX/03/03/07/A_PropertyValue_Write/A_PropertyValue_Write_PDU.h>
#include <KNX/03/03/07/A_PropertyDescription_Read/A_PropertyDescription_Read_PDU.h>
#include <KNX/03/03/07/A_PropertyDescription_Response/A_PropertyDescription_Response_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_Read/A_NetworkParameter_Read_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_Response/A_NetworkParameter_Response_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_InfoReport/A_NetworkParameter_InfoReport_PDU.h>
#include <KNX/03/03/07/A_IndividualAddressSerialNumber_Read/A_IndividualAddressSerialNumber_Read_PDU.h>
#include <KNX/03/03/07/A_IndividualAddressSerialNumber_Response/A_IndividualAddressSerialNumber_Response_PDU.h>
#include <KNX/03/03/07/A_IndividualAddressSerialNumber_Write/A_IndividualAddressSerialNumber_Write_PDU.h>
#include <KNX/03/03/07/A_ServiceInformation_Indication/A_ServiceInformation_Indication_PDU.h>
#include <KNX/03/03/07/A_DomainAddress_Write/A_DomainAddress_Write_PDU.h>
#include <KNX/03/03/07/A_DomainAddress_Read/A_DomainAddress_Read_PDU.h>
#include <KNX/03/03/07/A_DomainAddress_Response/A_DomainAddress_Response_PDU.h>
#include <KNX/03/03/07/A_DomainAddressSelective_Read/A_DomainAddressSelective_Read_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_Write/A_NetworkParameter_Write_PDU.h>
#include <KNX/03/03/07/A_Link_Read/A_Link_Read_PDU.h>
#include <KNX/03/03/07/A_Link_Response/A_Link_Response_PDU.h>
#include <KNX/03/03/07/A_Link_Write/A_Link_Write_PDU.h>
#include <KNX/03/03/07/A_GroupPropValue_Read/A_GroupPropValue_Read_PDU.h>
#include <KNX/03/03/07/A_GroupPropValue_Response/A_GroupPropValue_Response_PDU.h>
#include <KNX/03/03/07/A_GroupPropValue_Write/A_GroupPropValue_Write_PDU.h>
#include <KNX/03/03/07/A_GroupPropValue_InfoReport/A_GroupPropValue_InfoReport_PDU.h>
#include <KNX/03/03/07/A_DomainAddressSerialNumber_Read/A_DomainAddressSerialNumber_Read_PDU.h>
#include <KNX/03/03/07/A_DomainAddressSerialNumber_Response/A_DomainAddressSerialNumber_Response_PDU.h>
#include <KNX/03/03/07/A_DomainAddressSerialNumber_Write/A_DomainAddressSerialNumber_Write_PDU.h>
#include <KNX/03/03/07/A_FileStream_InfoReport/A_FileStream_InfoReport_PDU.h>

namespace KNX {

Application_Layer::Application_Layer(Transport_Layer & transport_layer, asio::io_context & io_context) :
    io_context(io_context),
    T_Data_Group_con_bus(io_context),
    T_Data_Group_ind_bus(io_context),
    T_Data_Tag_Group_con_bus(io_context),
    T_Data_Tag_Group_ind_bus(io_context),
    T_Data_Broadcast_con_bus(io_context),
    T_Data_Broadcast_ind_bus(io_context),
    T_Data_SystemBroadcast_con_bus(io_context),
    T_Data_SystemBroadcast_ind_bus(io_context),
    T_Data_Individual_con_bus(io_context),
    T_Data_Individual_ind_bus(io_context),
    T_Connect_con_bus(io_context),
    T_Connect_ind_bus(io_context),
    T_Disconnect_con_bus(io_context),
    T_Disconnect_ind_bus(io_context),
    T_Data_Connected_con_bus(io_context),
    T_Data_Connected_ind_bus(io_context)
{
    T_Data_Group_req = std::bind(&Transport_Layer::T_Data_Group_req, &transport_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    transport_layer.T_Data_Group_con = std::bind(&Application_Layer::T_Data_Group_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
    transport_layer.T_Data_Group_ind = std::bind(&Application_Layer::T_Data_Group_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);

    T_Data_Tag_Group_req = std::bind(&Transport_Layer::T_Data_Tag_Group_req, &transport_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    transport_layer.T_Data_Tag_Group_ind = std::bind(&Application_Layer::T_Data_Tag_Group_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);

    T_Data_Broadcast_req = std::bind(&Transport_Layer::T_Data_Broadcast_req, &transport_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    transport_layer.T_Data_Broadcast_con = std::bind(&Application_Layer::T_Data_Broadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    transport_layer.T_Data_Broadcast_ind = std::bind(&Application_Layer::T_Data_Broadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);

    T_Data_SystemBroadcast_req = std::bind(&Transport_Layer::T_Data_SystemBroadcast_req, &transport_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    transport_layer.T_Data_SystemBroadcast_con = std::bind(&Application_Layer::T_Data_SystemBroadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    transport_layer.T_Data_SystemBroadcast_ind = std::bind(&Application_Layer::T_Data_SystemBroadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);

    T_Data_Individual_req = std::bind(&Transport_Layer::T_Data_Individual_req, &transport_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    transport_layer.T_Data_Individual_con = std::bind(&Application_Layer::T_Data_Individual_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    transport_layer.T_Data_Individual_ind = std::bind(&Application_Layer::T_Data_Individual_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);

    T_Connect_req = std::bind(&Transport_Layer::T_Connect_req, &transport_layer, std::placeholders::_1, std::placeholders::_2);
    transport_layer.T_Connect_con = std::bind(&Application_Layer::T_Connect_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    transport_layer.T_Connect_ind = std::bind(&Application_Layer::T_Connect_ind, this, std::placeholders::_1);

    T_Disconnect_req = std::bind(&Transport_Layer::T_Disconnect_req, &transport_layer, std::placeholders::_1, std::placeholders::_2);
    transport_layer.T_Disconnect_con = std::bind(&Application_Layer::T_Disconnect_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    transport_layer.T_Disconnect_ind = std::bind(&Application_Layer::T_Disconnect_ind, this, std::placeholders::_1);

    T_Data_Connected_req = std::bind(&Transport_Layer::T_Data_Connected_req, &transport_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    transport_layer.T_Data_Connected_con = std::bind(&Application_Layer::T_Data_Connected_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    transport_layer.T_Data_Connected_ind = std::bind(&Application_Layer::T_Data_Connected_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
}

Application_Layer::~Application_Layer()
{
}

void Application_Layer::A_Connect_req(const ASAP_Connected asap, const Priority priority, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Connect_req) {
        Lcon(Status::not_ok);
        return;
    }

    T_Connect_req(asap.address, priority);
}

void Application_Layer::A_Connect_ind(std::function<void(const ASAP_Connected asap)> ind)
{
    T_Connect_ind_bus.async_wait([ind](const TSAP_Connected tsap) -> void {
        const ASAP_Connected asap {tsap, true};
        ind(asap);
    }, T_Connect_ind_bus.true_predicate);
}

void Application_Layer::A_Disconnect_req(const Priority priority, const ASAP_Connected asap, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Disconnect_req) {
        Lcon(Status::not_ok);
        return;
    }

    T_Disconnect_req(priority, asap.address);
}

void Application_Layer::A_Disconnect_ind(std::function<void(const ASAP_Connected asap)> ind)
{
    T_Disconnect_ind_bus.async_wait([ind](const TSAP_Connected tsap) -> void {
        const ASAP_Connected asap {tsap, true};
        ind(asap);
    }, T_Disconnect_ind_bus.true_predicate);
}

/* A_GroupValue_Read */

void Application_Layer::A_GroupValue_Read_req(const Ack_Request ack_request, const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Group_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_GroupValue_Read_PDU> apdu = std::make_shared<A_GroupValue_Read_PDU>();
    const TSAP_Group tsap = group_object_association_table->tsap(asap);
    T_Data_Group_req(ack_request, hop_count_type, priority, tsap, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const TSAP_Group /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const Individual_Address /*pred_source_address*/, const TSAP_Group pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_GroupValue_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupValue_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Group_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_GroupValue_Read_ind(std::function<void(const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type)> ind)
{
    T_Data_Group_ind_bus.async_wait([this, ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address /*source_address*/, const TSAP_Group tsap, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        const ASAP_Group asap = group_object_association_table->asap(tsap);
        //std::shared_ptr<A_GroupValue_Read_PDU> apdu = std::dynamic_pointer_cast<A_GroupValue_Read_PDU>(pred_tsdu);
        //assert(apdu);
        ind(asap, priority, hop_count_type);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, const TSAP_Group /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_GroupValue_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupValue_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_GroupValue_Read_res(const Ack_Request ack_request, const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, const Group_Value data, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Group_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_GroupValue_Response_PDU> apdu = std::make_shared<A_GroupValue_Response_PDU>();
    apdu->set_data(data);
    const TSAP_Group tsap = group_object_association_table->tsap(asap);
    T_Data_Group_req(ack_request, hop_count_type, priority, tsap, apdu);

    auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const TSAP_Group /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Rcon(t_status);
    };
    auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const Individual_Address /*pred_source_address*/, const TSAP_Group pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_GroupValue_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupValue_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Group_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_GroupValue_Read_Acon(std::function<void(const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, const Group_Value data)> Acon)
{
    T_Data_Group_ind_bus.async_wait([this, Acon](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address /*source_address*/, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Group asap = group_object_association_table->asap(tsap);
        std::shared_ptr<A_GroupValue_Response_PDU> apdu = std::dynamic_pointer_cast<A_GroupValue_Response_PDU>(tsdu);
        assert(apdu);
        Acon(asap, priority, hop_count_type, apdu->data());
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, const TSAP_Group /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_GroupValue_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupValue_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_GroupValue_Write */

void Application_Layer::A_GroupValue_Write_req(const Ack_Request ack_request, const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, const Group_Value data, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Group_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_GroupValue_Write_PDU> apdu = std::make_shared<A_GroupValue_Write_PDU>();
    apdu->set_data(data);
    const TSAP_Group tsap = group_object_association_table->tsap(asap);
    T_Data_Group_req(ack_request, hop_count_type, priority, tsap, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const TSAP_Group /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const Individual_Address /*pred_source_address*/, const TSAP_Group pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_GroupValue_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupValue_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Group_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_GroupValue_Write_ind(std::function<void(const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, const Group_Value data)> ind)
{
    T_Data_Group_ind_bus.async_wait([this, ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address /*source_address*/, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Group asap = group_object_association_table->asap(tsap);
        std::shared_ptr<A_GroupValue_Write_PDU> apdu = std::dynamic_pointer_cast<A_GroupValue_Write_PDU>(tsdu);
        assert(apdu);
        ind(asap, priority, hop_count_type, apdu->data());
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, const TSAP_Group /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_GroupValue_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupValue_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_GroupPropValue_Read */

void Application_Layer::A_GroupPropValue_Read_req(const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Ack_Request ack_req, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Tag_Group_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_GroupPropValue_Read_PDU> apdu = std::make_shared<A_GroupPropValue_Read_PDU>();
    apdu->object_type = object_type;
    apdu->object_instance = object_instance;
    apdu->property_id = property_id;
    T_Data_Tag_Group_req(ack_req, zone.group_address, zone.extended_frame_format, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const Group_Address /*destination_address*/, const Extended_Frame_Format /*extended_frame_format*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [zone, ack_req, priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const Individual_Address /*pred_source_address*/, const Group_Address pred_destination_address, const Extended_Frame_Format pred_extended_frame_format, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_GroupPropValue_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupPropValue_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_destination_address == zone.group_address) && (pred_extended_frame_format == zone.extended_frame_format) && (*pred_apdu == *apdu);
    };
    T_Data_Tag_Group_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_GroupPropValue_Read_ind(std::function<void(const ASAP_Individual asap, const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Priority priority, const Hop_Count_Type hop_count_type)> ind)
{
    T_Data_Tag_Group_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu) -> void {
        const Zone zone{extended_frame_format, destination_address};
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_GroupPropValue_Read_PDU> apdu = std::dynamic_pointer_cast<A_GroupPropValue_Read_PDU>(tsdu);
        assert(apdu);
        ind(asap, zone, apdu->object_type, apdu->object_instance, apdu->property_id, priority, hop_count_type);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, const Group_Address /*pred_destination_Address*/, const Extended_Frame_Format /*pred_extended_frame_format*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_GroupPropValue_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupPropValue_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_GroupPropValue_Response */

void Application_Layer::A_GroupPropValue_Response_req(const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Ack_Request ack_req, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Tag_Group_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_GroupPropValue_Response_PDU> apdu = std::make_shared<A_GroupPropValue_Response_PDU>();
    apdu->object_type = object_type;
    apdu->object_instance = object_instance;
    apdu->property_id = property_id;
    apdu->data = data;
    T_Data_Tag_Group_req(ack_req, zone.group_address, zone.extended_frame_format, hop_count_type, priority, apdu);

    auto completion_handler = [Rcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const Group_Address /*destination_address*/, const Extended_Frame_Format /*extended_frame_format*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Rcon(t_status);
    };
    auto predicate = [zone, ack_req, priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const Individual_Address /*pred_source_address*/, const Group_Address pred_destination_address, const Extended_Frame_Format pred_extended_frame_format, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_GroupPropValue_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupPropValue_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_destination_address == zone.group_address) && (pred_extended_frame_format == zone.extended_frame_format) && (*pred_apdu == *apdu);
    };
    T_Data_Tag_Group_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_GroupPropValue_Response_ind(std::function<void(const ASAP_Individual source_address, const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Priority priority, const Hop_Count_Type hop_count_type)> ind)
{
    T_Data_Tag_Group_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu) -> void {
        const Zone zone{extended_frame_format, destination_address};
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_GroupPropValue_Response_PDU> apdu = std::dynamic_pointer_cast<A_GroupPropValue_Response_PDU>(tsdu);
        assert(apdu);
        ind(asap, zone, apdu->object_type, apdu->object_instance, apdu->property_id, apdu->data, priority, hop_count_type);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, const Group_Address /*pred_destination_Address*/, const Extended_Frame_Format /*pred_extended_frame_format*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_GroupPropValue_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupPropValue_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_GroupPropValue_Write */

void Application_Layer::A_GroupPropValue_Write_req(const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Ack_Request ack_req, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Tag_Group_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_GroupPropValue_Write_PDU> apdu = std::make_shared<A_GroupPropValue_Write_PDU>();
    apdu->object_type = object_type;
    apdu->object_instance = object_instance;
    apdu->property_id = property_id;
    apdu->data = data;
    T_Data_Tag_Group_req(ack_req, zone.group_address, zone.extended_frame_format, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const Group_Address /*destination_address*/, const Extended_Frame_Format /*extended_frame_format*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [zone, ack_req, priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const Individual_Address /*pred_source_address*/, const Group_Address pred_destination_address, const Extended_Frame_Format pred_extended_frame_format, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_GroupPropValue_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupPropValue_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_destination_address == zone.group_address) && (pred_extended_frame_format == zone.extended_frame_format) && (*pred_apdu == *apdu);
    };
    T_Data_Tag_Group_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_GroupPropValue_Write_ind(std::function<void(const ASAP_Individual source_address, const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Priority priority, const Hop_Count_Type hop_count_type)> ind)
{
    T_Data_Tag_Group_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu) -> void {
        const Zone zone{extended_frame_format, destination_address};
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_GroupPropValue_Write_PDU> apdu = std::dynamic_pointer_cast<A_GroupPropValue_Write_PDU>(tsdu);
        assert(apdu);
        ind(asap, zone, apdu->object_type, apdu->object_instance, apdu->property_id, apdu->data, priority, hop_count_type);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, const Group_Address /*pred_destination_Address*/, const Extended_Frame_Format /*pred_extended_frame_format*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_GroupPropValue_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupPropValue_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_GroupPropValue_InfoReport */

void Application_Layer::A_GroupPropValue_InfoReport_req(const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value data, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Tag_Group_req) {
        Lcon(Status::not_ok);
        return;
    }
    const Ack_Request ack_request { Ack_Request::dont_care };
    std::shared_ptr<A_GroupPropValue_InfoReport_PDU> apdu = std::make_shared<A_GroupPropValue_InfoReport_PDU>();
    apdu->object_type = object_type;
    apdu->object_instance = object_instance;
    apdu->property_id = property_id;
    apdu->data = data;
    T_Data_Tag_Group_req(ack_request, zone.group_address, zone.extended_frame_format, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const Group_Address /*destination_address*/, const Extended_Frame_Format /*extended_frame_format*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [zone, data, priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const Individual_Address /*pred_source_address*/, const Group_Address pred_destination_address, const Extended_Frame_Format pred_extended_frame_format, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_GroupPropValue_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupPropValue_InfoReport_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_destination_address == zone.group_address) && (pred_extended_frame_format == zone.extended_frame_format) && (*pred_apdu == *apdu);
    };
    T_Data_Tag_Group_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_GroupPropValue_InfoReport_ind(std::function<void(const ASAP_Individual source_address, const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Priority priority, const Hop_Count_Type hop_count_type)> ind)
{
    T_Data_Tag_Group_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu) -> void {
        const Zone zone{extended_frame_format, destination_address};
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_GroupPropValue_InfoReport_PDU> apdu = std::dynamic_pointer_cast<A_GroupPropValue_InfoReport_PDU>(tsdu);
        assert(apdu);
        ind(asap, zone, apdu->object_type, apdu->object_instance, apdu->property_id, apdu->data, priority, hop_count_type);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, const Group_Address /*pred_destination_Address*/, const Extended_Frame_Format /*pred_extended_frame_format*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_GroupPropValue_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_GroupPropValue_InfoReport_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_IndividualAddress_Write */

void Application_Layer::A_IndividualAddress_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Individual_Address newaddress, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Broadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_IndividualAddress_Write_PDU> apdu = std::make_shared<A_IndividualAddress_Write_PDU>();
    apdu->newaddress = newaddress;
    T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_IndividualAddress_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddress_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_IndividualAddress_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Individual_Address newaddress)> ind)
{
    T_Data_Broadcast_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_IndividualAddress_Write_PDU> apdu = std::dynamic_pointer_cast<A_IndividualAddress_Write_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, apdu->newaddress);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_IndividualAddress_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddress_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_IndividualAddress_Read */

void Application_Layer::A_IndividualAddress_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Broadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_IndividualAddress_Read_PDU> apdu = std::make_shared<A_IndividualAddress_Read_PDU>();
    T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_IndividualAddress_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddress_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_IndividualAddress_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type)> ind)
{
    T_Data_Broadcast_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        const ASAP_Individual asap{source_address, false};
        //std::shared_ptr<A_IndividualAddress_Read_PDU> apdu = std::dynamic_pointer_cast<A_IndividualAddress_Read_PDU>(tsdu);
        //assert(apdu);
        ind(priority, hop_count_type);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_IndividualAddress_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddress_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_IndividualAddress_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Individual_Address /*individual_address*/, std::function<void(const Status a_status)> Rcon)
{
    // individual_address is not needed as L_Data.req contains individual_address as source_address
    if (!T_Data_Broadcast_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_IndividualAddress_Response_PDU> apdu = std::make_shared<A_IndividualAddress_Response_PDU>();
    T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Rcon(t_status);
    };
    auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_IndividualAddress_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddress_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_IndividualAddress_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Individual_Address individual_address)> Acon)
{
    T_Data_Broadcast_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_IndividualAddress_Response_PDU> apdu = std::dynamic_pointer_cast<A_IndividualAddress_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, source_address);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_IndividualAddress_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddress_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_IndividualAddressSerialNumber_Read */

void Application_Layer::A_IndividualAddressSerialNumber_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Broadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_IndividualAddressSerialNumber_Read_PDU> apdu = std::make_shared<A_IndividualAddressSerialNumber_Read_PDU>();
    apdu->serial_number = serial_number;
    T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_IndividualAddressSerialNumber_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddressSerialNumber_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_IndividualAddressSerialNumber_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number)> ind)
{
    T_Data_Broadcast_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_IndividualAddressSerialNumber_Read_PDU> apdu = std::dynamic_pointer_cast<A_IndividualAddressSerialNumber_Read_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, apdu->serial_number);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_IndividualAddressSerialNumber_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddressSerialNumber_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_IndividualAddressSerialNumber_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, const Domain_Address_2 domain_address, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Broadcast_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_IndividualAddressSerialNumber_Response_PDU> apdu = std::make_shared<A_IndividualAddressSerialNumber_Response_PDU>();
    apdu->serial_number = serial_number;
    apdu->domain_address = domain_address;
    T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Rcon(t_status);
    };
    auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_IndividualAddressSerialNumber_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddressSerialNumber_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_IndividualAddressSerialNumber_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, const Individual_Address individual_address, const Domain_Address_2 domain_address)> Acon)
{
    T_Data_Broadcast_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_IndividualAddressSerialNumber_Response_PDU> apdu = std::dynamic_pointer_cast<A_IndividualAddressSerialNumber_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, apdu->serial_number, source_address, apdu->domain_address);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_IndividualAddressSerialNumber_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddressSerialNumber_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_IndividualAddressSerialNumber_Write */

void Application_Layer::A_IndividualAddressSerialNumber_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, const Individual_Address newaddress, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Broadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_IndividualAddressSerialNumber_Write_PDU> apdu = std::make_shared<A_IndividualAddressSerialNumber_Write_PDU>();
    apdu->serial_number = serial_number;
    apdu->newaddress = newaddress;
    T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_IndividualAddressSerialNumber_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddressSerialNumber_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_IndividualAddressSerialNumber_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, const Individual_Address newaddress)> ind)
{
    T_Data_Broadcast_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_IndividualAddressSerialNumber_Write_PDU> apdu = std::dynamic_pointer_cast<A_IndividualAddressSerialNumber_Write_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, apdu->serial_number, apdu->newaddress);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_IndividualAddressSerialNumber_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_IndividualAddressSerialNumber_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_NetworkParameter_Read */

void Application_Layer::A_NetworkParameter_Read_req(const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, std::function<void(const Status a_status)> Lcon)
{
    const Ack_Request ack_request{}; // @todo ack_request
    std::shared_ptr<A_NetworkParameter_Read_PDU> apdu = std::make_shared<A_NetworkParameter_Read_PDU>();
    apdu->parameter_type = parameter_type;
    apdu->test_info = test_info;
    switch (comm_mode_req) {
    case Comm_Mode::Broadcast:
    {
        if (!T_Data_Broadcast_req) {
            Lcon(Status::not_ok);
            return;
        }
        T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_NetworkParameter_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Comm_Mode::Individual:
    {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_NetworkParameter_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    default:
        // other comm modes are not supported
        break;
    }
}

void Application_Layer::A_NetworkParameter_Read_ind(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_NetworkParameter_Read_PDU> apdu = std::dynamic_pointer_cast<A_NetworkParameter_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, Comm_Mode::Broadcast, hop_count_type, apdu->parameter_type, priority, apdu->test_info);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_NetworkParameter_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Individual_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_NetworkParameter_Read_PDU> apdu = std::dynamic_pointer_cast<A_NetworkParameter_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, Comm_Mode::Individual, hop_count_type, apdu->parameter_type, priority, apdu->test_info);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_NetworkParameter_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_NetworkParameter_Read_res(const ASAP_Individual asap, const Comm_Mode comm_mode, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, const Parameter_Test_Result test_result, std::function<void(const Status a_status)> Rcon)
{
    const Ack_Request ack_request{}; // @todo ack_request
    std::shared_ptr<A_NetworkParameter_Response_PDU> apdu = std::make_shared<A_NetworkParameter_Response_PDU>();
    apdu->parameter_type = parameter_type;
    apdu->test_info_result.insert(std::cend(apdu->test_info_result), std::cbegin(test_info), std::cend(test_info));
    apdu->test_info_result.insert(std::cend(apdu->test_info_result), std::cbegin(test_result), std::cend(test_result));
    switch (comm_mode) {
    case Comm_Mode::Broadcast:
    {
        if (!T_Data_Broadcast_req) {
            Rcon(Status::not_ok);
            return;
        }
        T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_NetworkParameter_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Comm_Mode::Individual:
    {
        if (!T_Data_Individual_req) {
            Rcon(Status::not_ok);
            return;
        }
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_NetworkParameter_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    default:
        // other comm modes are not supported
        break;
    }
}

void Application_Layer::A_NetworkParameter_Read_Acon(std::function<void(const ASAP_Individual asap, const Hop_Count_Type hop_count_type, const Individual_Address individual_address, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info_Result test_info_result)> Acon)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_NetworkParameter_Response_PDU> apdu = std::dynamic_pointer_cast<A_NetworkParameter_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(asap, hop_count_type, source_address, apdu->parameter_type, priority, apdu->test_info_result);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_NetworkParameter_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Individual_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        const Individual_Address individual_address{tsap};
        std::shared_ptr<A_NetworkParameter_Response_PDU> apdu = std::dynamic_pointer_cast<A_NetworkParameter_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(asap, hop_count_type, individual_address, apdu->parameter_type, priority, apdu->test_info_result);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_NetworkParameter_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_NetworkParameter_Write */

void Application_Layer::A_NetworkParameter_Write_req(const ASAP_Individual asap, const Comm_Mode comm_mode, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Value value, std::function<void(const Status a_status)> Lcon)
{
    const Ack_Request ack_request{}; // @todo ack_request
    std::shared_ptr<A_NetworkParameter_Write_PDU> apdu = std::make_shared<A_NetworkParameter_Write_PDU>();
    apdu->parameter_type = parameter_type;
    apdu->value = value;
    switch (comm_mode) {
    case Comm_Mode::Broadcast:
    {
        if (!T_Data_Broadcast_req) {
            Lcon(Status::not_ok);
            return;
        }
        T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_NetworkParameter_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Write_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Comm_Mode::Individual:
    {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_NetworkParameter_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Write_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    default:
        // other comm modes are not supported
        break;
    }
}

void Application_Layer::A_NetworkParameter_Write_ind(std::function<void(const ASAP_Individual asap, const Parameter_Type parameter_type, const Priority priority, const Parameter_Value value)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type /*hop_count_type*/, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_NetworkParameter_Write_PDU> apdu = std::dynamic_pointer_cast<A_NetworkParameter_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, apdu->parameter_type, priority, apdu->value);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_NetworkParameter_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Individual_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type /*hop_count_type*/, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_NetworkParameter_Write_PDU> apdu = std::dynamic_pointer_cast<A_NetworkParameter_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, apdu->parameter_type, priority, apdu->value);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_NetworkParameter_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_NetworkParameter_InfoReport */

void Application_Layer::A_NetworkParameter_InfoReport_req(const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, const Parameter_Test_Result test_result, std::function<void(const Status a_status)> Lcon)
{
    const Ack_Request ack_request{}; // @todo ack_request
    std::shared_ptr<A_NetworkParameter_InfoReport_PDU> apdu = std::make_shared<A_NetworkParameter_InfoReport_PDU>();
    apdu->parameter_type = parameter_type;
    apdu->test_info_result.insert(std::cend(apdu->test_info_result), std::cbegin(test_info), std::cend(test_info));
    apdu->test_info_result.insert(std::cend(apdu->test_info_result), std::cbegin(test_result), std::cend(test_result));
    switch (comm_mode_req) {
    case Comm_Mode::Broadcast:
    {
        if (!T_Data_Broadcast_req) {
            Lcon(Status::not_ok);
            return;
        }
        T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_NetworkParameter_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_InfoReport_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Comm_Mode::Individual:
    {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_NetworkParameter_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_InfoReport_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    default:
        // other comm modes are not supported
        break;
    }
}

void Application_Layer::A_NetworkParameter_InfoReport_ind(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info_Result test_info_result)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_NetworkParameter_InfoReport_PDU> apdu = std::dynamic_pointer_cast<A_NetworkParameter_InfoReport_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, Comm_Mode::Broadcast, hop_count_type, apdu->parameter_type, priority, apdu->test_info_result);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_NetworkParameter_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_InfoReport_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Individual_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_NetworkParameter_InfoReport_PDU> apdu = std::dynamic_pointer_cast<A_NetworkParameter_InfoReport_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, Comm_Mode::Individual, hop_count_type, apdu->parameter_type, priority, apdu->test_info_result);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_NetworkParameter_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_NetworkParameter_InfoReport_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_DeviceDescriptor_InfoReport */

void Application_Layer::A_DeviceDescriptor_InfoReport_req(const Ack_Request ack_request, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor, const Hop_Count_Type hop_count_type, const Priority priority, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_SystemBroadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_DeviceDescriptor_InfoReport_PDU> apdu = std::make_shared<A_DeviceDescriptor_InfoReport_PDU>();
    apdu->descriptor_type = descriptor_type;
    apdu->device_descriptor = device_descriptor;
    T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_DeviceDescriptor_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_InfoReport_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_DeviceDescriptor_InfoReport_ind(std::function<void(const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor, const Hop_Count_Type hop_count_type, const Priority priority)> ind)
{
    T_Data_SystemBroadcast_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DeviceDescriptor_InfoReport_PDU> apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_InfoReport_PDU>(tsdu);
        assert(apdu);
        ind(apdu->descriptor_type, apdu->device_descriptor, hop_count_type, priority);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DeviceDescriptor_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_InfoReport_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_DomainAddress_Write */

void Application_Layer::A_DomainAddress_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Domain_Address domain_address_new, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_SystemBroadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_DomainAddress_Write_PDU> apdu = std::make_shared<A_DomainAddress_Write_PDU>();
    apdu->domain_address = domain_address_new;
    T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_DomainAddress_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddress_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_DomainAddress_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Domain_Address domain_address_new)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddress_Write_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddress_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, apdu->domain_address);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddress_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddress_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddress_Write_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddress_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, apdu->domain_address);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddress_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddress_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_DomainAddress_Read */

void Application_Layer::A_DomainAddress_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_SystemBroadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_DomainAddress_Read_PDU> apdu = std::make_shared<A_DomainAddress_Read_PDU>();
    T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_DomainAddress_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddress_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_DomainAddress_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        const ASAP_Individual asap{source_address, false};
        //std::shared_ptr<A_DomainAddress_Read_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddress_Read_PDU>(tsdu);
        //assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddress_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddress_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        const ASAP_Individual asap{source_address, false};
        //std::shared_ptr<A_DomainAddress_Read_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddress_Read_PDU>(tsdu);
        //assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddress_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddress_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_DomainAddress_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Domain_Address domain_address, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_SystemBroadcast_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_DomainAddress_Response_PDU> apdu = std::make_shared<A_DomainAddress_Response_PDU>();
    apdu->domain_address = domain_address;
    T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Rcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Rcon(t_status);
    };
    auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_DomainAddress_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddress_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_DomainAddress_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Domain_Address domain_address)> Acon)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddress_Response_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddress_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(priority, hop_count_type, asap, apdu->domain_address);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddress_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddress_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddress_Response_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddress_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(priority, hop_count_type, asap, apdu->domain_address);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddress_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddress_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_DomainAddressSelective_Read */

void Application_Layer::A_DomainAddressSelective_Read_req(const Priority priority, const Hop_Count_Type hop_count_type, const ASDU asdu, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_SystemBroadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    const Ack_Request ack_request { Ack_Request::dont_care };
    std::shared_ptr<A_DomainAddressSelective_Read_PDU> apdu = std::make_shared<A_DomainAddressSelective_Read_PDU>();
    apdu->asdu = asdu;
    T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_DomainAddressSelective_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSelective_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_DomainAddressSelective_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASDU asdu)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddressSelective_Read_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddressSelective_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, apdu->asdu);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddressSelective_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSelective_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddressSelective_Read_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddressSelective_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, apdu->asdu);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddressSelective_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSelective_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_DomainAddressSerialNumber_Read */

void Application_Layer::A_DomainAddressSerialNumber_Read_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_SystemBroadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_DomainAddressSerialNumber_Read_PDU> apdu = std::make_shared<A_DomainAddressSerialNumber_Read_PDU>();
    apdu->serial_number = serial_number;
    T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_DomainAddressSerialNumber_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_DomainAddressSerialNumber_Read_ind(std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddressSerialNumber_Read_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(hop_count_type, priority, apdu->serial_number);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddressSerialNumber_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddressSerialNumber_Read_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(hop_count_type, priority, apdu->serial_number);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddressSerialNumber_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_DomainAddressSerialNumber_Read_res(const Ack_Request ack_request, const Domain_Address domain_address, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_SystemBroadcast_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_DomainAddressSerialNumber_Response_PDU> apdu = std::make_shared<A_DomainAddressSerialNumber_Response_PDU>();
    apdu->serial_number = serial_number;
    apdu->domain_address = domain_address;
    T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Rcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Rcon(t_status);
    };
    auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_DomainAddressSerialNumber_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_DomainAddressSerialNumber_Read_Acon(std::function<void(const Domain_Address domain_address, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number)> Acon)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddressSerialNumber_Response_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(apdu->domain_address, hop_count_type, priority, apdu->serial_number);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddressSerialNumber_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddressSerialNumber_Response_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(apdu->domain_address, hop_count_type, priority, apdu->serial_number);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddressSerialNumber_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_DomainAddressSerialNumber_Write */

void Application_Layer::A_DomainAddressSerialNumber_Write_req(const Ack_Request ack_request, const Domain_Address domain_address, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_SystemBroadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_DomainAddressSerialNumber_Write_PDU> apdu = std::make_shared<A_DomainAddressSerialNumber_Write_PDU>();
    apdu->serial_number = serial_number;
    apdu->domain_address = domain_address;
    T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_DomainAddressSerialNumber_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_DomainAddressSerialNumber_Write_ind(std::function<void(const Domain_Address domain_address, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddressSerialNumber_Write_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(apdu->domain_address, hop_count_type, priority, apdu->serial_number);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddressSerialNumber_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_DomainAddressSerialNumber_Write_PDU> apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(apdu->domain_address, hop_count_type, priority, apdu->serial_number);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DomainAddressSerialNumber_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_DomainAddressSerialNumber_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_SystemNetworkParameter_Read */

void Application_Layer::A_SystemNetworkParameter_Read_req(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, std::function<void(const Status a_status)> Lcon)
{
    switch (communication_medium) {
    case Communication_Medium::open:
    {
        if (!T_Data_SystemBroadcast_req) {
            Lcon(Status::not_ok);
            return;
        }
        const Ack_Request ack_request { Ack_Request::dont_care };
        std::shared_ptr<A_SystemNetworkParameter_Read_PDU> apdu = std::make_shared<A_SystemNetworkParameter_Read_PDU>();
        apdu->parameter_type = parameter_type;
        apdu->test_info = test_info;
        T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_SystemNetworkParameter_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Communication_Medium::closed:
    {
        if (!T_Data_Broadcast_req) {
            Lcon(Status::not_ok);
            return;
        }
        const Ack_Request ack_request { Ack_Request::dont_care };
        std::shared_ptr<A_SystemNetworkParameter_Read_PDU> apdu = std::make_shared<A_SystemNetworkParameter_Read_PDU>();
        apdu->parameter_type = parameter_type;
        apdu->test_info = test_info;
        T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_SystemNetworkParameter_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    }
}

void Application_Layer::A_SystemNetworkParameter_Read_ind(std::function<void(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_SystemNetworkParameter_Read_PDU> apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(hop_count_type, apdu->parameter_type, priority, apdu->test_info);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_SystemNetworkParameter_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_SystemNetworkParameter_Read_PDU> apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(hop_count_type, apdu->parameter_type, priority, apdu->test_info);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_SystemNetworkParameter_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_SystemNetworkParameter_Read_res(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, const Parameter_Test_Result test_result, std::function<void(const Status a_status)> Rcon)
{
    switch (communication_medium) {
    case Communication_Medium::open:
    {
        if (!T_Data_SystemBroadcast_req) {
            Rcon(Status::not_ok);
            return;
        }
        const Ack_Request ack_request { Ack_Request::dont_care };
        std::shared_ptr<A_SystemNetworkParameter_Response_PDU> apdu = std::make_shared<A_SystemNetworkParameter_Response_PDU>();
        apdu->parameter_type = parameter_type;
        apdu->test_info_result.insert(std::cend(apdu->test_info_result), std::cbegin(test_info), std::cend(test_info));
        apdu->test_info_result.insert(std::cend(apdu->test_info_result), std::cbegin(test_result), std::cend(test_result));
        T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Rcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_SystemNetworkParameter_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Communication_Medium::closed:
    {
        if (!T_Data_Broadcast_req) {
            Rcon(Status::not_ok);
            return;
        }
        const Ack_Request ack_request { Ack_Request::dont_care };
        std::shared_ptr<A_SystemNetworkParameter_Response_PDU> apdu = std::make_shared<A_SystemNetworkParameter_Response_PDU>();
        apdu->parameter_type = parameter_type;
        apdu->test_info_result.insert(std::cend(apdu->test_info_result), std::cbegin(test_info), std::cend(test_info));
        apdu->test_info_result.insert(std::cend(apdu->test_info_result), std::cbegin(test_result), std::cend(test_result));
        T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_SystemNetworkParameter_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    }
}

void Application_Layer::A_SystemNetworkParameter_Read_Acon(std::function<void(const ASAP_Individual asap, const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info_Result test_info_result)> Acon)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_SystemNetworkParameter_Response_PDU> apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(asap, hop_count_type, apdu->parameter_type, priority, apdu->test_info_result);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_SystemNetworkParameter_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_SystemNetworkParameter_Response_PDU> apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(asap, hop_count_type, apdu->parameter_type, priority, apdu->test_info_result);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_SystemNetworkParameter_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_SystemNetworkParameter_Write */

void Application_Layer::A_SystemNetworkParameter_Write_req(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Value value, std::function<void(const Status a_status)> Lcon)
{
    switch (communication_medium) {
    case Communication_Medium::open:
    {
        if (!T_Data_SystemBroadcast_req) {
            Lcon(Status::not_ok);
            return;
        }
        const Ack_Request ack_request { Ack_Request::dont_care };
        std::shared_ptr<A_SystemNetworkParameter_Write_PDU> apdu = std::make_shared<A_SystemNetworkParameter_Write_PDU>();
        apdu->parameter_type = parameter_type;
        apdu->value = value;
        T_Data_SystemBroadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Lcon](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [priority, apdu](const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_SystemNetworkParameter_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Write_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_SystemBroadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Communication_Medium::closed:
    {
        if (!T_Data_Broadcast_req) {
            Lcon(Status::not_ok);
            return;
        }
        const Ack_Request ack_request { Ack_Request::dont_care };
        std::shared_ptr<A_SystemNetworkParameter_Write_PDU> apdu = std::make_shared<A_SystemNetworkParameter_Write_PDU>();
        apdu->parameter_type = parameter_type;
        apdu->value = value;
        T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_SystemNetworkParameter_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Write_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
        };
        T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    }
}

void Application_Layer::A_SystemNetworkParameter_Write_ind(std::function<void(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Value value)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Broadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_SystemNetworkParameter_Write_PDU> apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(hop_count_type, apdu->parameter_type, priority, apdu->value);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_SystemNetworkParameter_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_SystemBroadcast_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_SystemNetworkParameter_Write_PDU> apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(hop_count_type, apdu->parameter_type, priority, apdu->value);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_SystemNetworkParameter_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_SystemNetworkParameter_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_DeviceDescriptor_Read */

void Application_Layer::A_DeviceDescriptor_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Descriptor_Type descriptor_type, std::function<void(const Status a_status)> Lcon)
{
    if (!asap.connected) {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_DeviceDescriptor_Read_PDU> apdu = std::make_shared<A_DeviceDescriptor_Read_PDU>();
        apdu->descriptor_type = descriptor_type;
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_DeviceDescriptor_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    } else {
        if (!T_Data_Connected_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_DeviceDescriptor_Read_PDU> apdu = std::make_shared<A_DeviceDescriptor_Read_PDU>();
        apdu->descriptor_type = descriptor_type;
        const TSAP_Individual tsap = asap.address;
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Lcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_DeviceDescriptor_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
}

void Application_Layer::A_DeviceDescriptor_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Descriptor_Type descriptor_type)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_DeviceDescriptor_Read_PDU> apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, asap, apdu->descriptor_type);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DeviceDescriptor_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_DeviceDescriptor_Read_PDU> apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, asap, apdu->descriptor_type);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DeviceDescriptor_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_DeviceDescriptor_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor, std::function<void(const Status a_status)> Rcon)
{
    if (!asap.connected) {
        if (!T_Data_Individual_req) {
            Rcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_DeviceDescriptor_Response_PDU> apdu = std::make_shared<A_DeviceDescriptor_Response_PDU>();
        apdu->descriptor_type = descriptor_type;
        apdu->device_descriptor = device_descriptor;
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_DeviceDescriptor_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    } else {
        if (!T_Data_Connected_req) {
            Rcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_DeviceDescriptor_Response_PDU> apdu = std::make_shared<A_DeviceDescriptor_Response_PDU>();
        apdu->descriptor_type = descriptor_type;
        apdu->device_descriptor = device_descriptor;
        const TSAP_Individual tsap = asap.address;
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Rcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_DeviceDescriptor_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
}

void Application_Layer::A_DeviceDescriptor_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor)> Acon)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_DeviceDescriptor_Response_PDU> apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(priority, hop_count_type, asap, apdu->descriptor_type, apdu->device_descriptor);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DeviceDescriptor_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_DeviceDescriptor_Response_PDU> apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(priority, hop_count_type, asap, apdu->descriptor_type, apdu->device_descriptor);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_DeviceDescriptor_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_DeviceDescriptor_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Restart */

void Application_Layer::A_Restart_req(const Ack_Request ack_request, const Restart_Channel_Number channel_number, const Restart_Erase_Code erase_code, const Priority priority, const Hop_Count_Type hop_count_type, const Restart_Type restart_type, const ASAP_Individual asap, std::function<void(const Status a_status)> Lcon)
{
    if (!asap.connected) {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_Restart_PDU> apdu = std::make_shared<A_Restart_PDU>();
        apdu->restart_type = restart_type;
        apdu->erase_code = erase_code;
        apdu->channel_number = channel_number;
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_Restart_PDU> pred_apdu = std::dynamic_pointer_cast<A_Restart_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    } else {
        if (!T_Data_Connected_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_Restart_PDU> apdu = std::make_shared<A_Restart_PDU>();
        apdu->restart_type = restart_type;
        apdu->erase_code = erase_code;
        apdu->channel_number = channel_number;
        const TSAP_Individual tsap = asap.address;
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Lcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_Restart_PDU> pred_apdu = std::dynamic_pointer_cast<A_Restart_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
}

void Application_Layer::A_Restart_ind(std::function<void(const Restart_Erase_Code erase_code, const Restart_Channel_Number channel_number, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_Restart_PDU> apdu = std::dynamic_pointer_cast<A_Restart_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(apdu->erase_code, apdu->channel_number, priority, hop_count_type, asap);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Restart_PDU> pred_apdu = std::dynamic_pointer_cast<A_Restart_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Restart_PDU> apdu = std::dynamic_pointer_cast<A_Restart_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(apdu->erase_code, apdu->channel_number, priority, hop_count_type, asap);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Restart_PDU> pred_apdu = std::dynamic_pointer_cast<A_Restart_PDU>(pred_tsdu);
        return pred_apdu && !(pred_tsdu->data_short & (1 << 5));
    });
}

void Application_Layer::A_Restart_res(const Restart_Error_Code error_code, const Priority priority, const Restart_Process_Time process_time, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, std::function<void(const Status a_status)> Rcon)
{
    if (!asap.connected) {
        if (!T_Data_Individual_req) {
            Rcon(Status::not_ok);
            return;
        }
        const Ack_Request ack_request = Ack_Request::dont_care;
        std::shared_ptr<A_Restart_Response_PDU> apdu = std::make_shared<A_Restart_Response_PDU>();
        apdu->error_code = error_code;
        apdu->process_time = process_time;
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_Restart_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Restart_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    } else {
        if (!T_Data_Connected_req) {
            Rcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_Restart_Response_PDU> apdu = std::make_shared<A_Restart_Response_PDU>();
        apdu->error_code = error_code;
        apdu->process_time = process_time;
        const TSAP_Individual tsap = asap.address;
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Rcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_Restart_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Restart_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
}

void Application_Layer::A_Restart_Acon(std::function<void(const Restart_Error_Code error_code, const Priority priority, const Restart_Process_Time process_time, const Hop_Count_Type hop_count_type, const ASAP_Individual asap)> Acon)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, false};
        std::shared_ptr<A_Restart_Response_PDU> apdu = std::dynamic_pointer_cast<A_Restart_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(apdu->error_code, priority, apdu->process_time, hop_count_type, asap);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Restart_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Restart_Response_PDU>(pred_tsdu);
        return pred_apdu && !(pred_tsdu->data_short & (1 << 5));
    });

    T_Data_Connected_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Restart_Response_PDU> apdu = std::dynamic_pointer_cast<A_Restart_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(apdu->error_code, priority, apdu->process_time, hop_count_type, asap);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Restart_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Restart_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_tsdu->data_short & (1 << 5));
    });
}

/* A_FileStream_InfoReport */

void Application_Layer::A_FileStream_InfoReport_req(const Ack_Request ack_request, const ASAP_Individual asap, const File_Block file_block, const File_Block_Sequence_Number file_block_sequence_number, const File_Handle file_handle, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Individual_req) {
        Lcon(Status::not_ok);
        return;
    }
    const Priority priority = Priority::normal;
    std::shared_ptr<A_FileStream_InfoReport_PDU> apdu = std::make_shared<A_FileStream_InfoReport_PDU>();
    apdu->file_handle = file_handle;
    apdu->file_block_sequence_number = file_block_sequence_number;
    apdu->file_block = file_block;
    const TSAP_Individual tsap = asap.address;
    T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_FileStream_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_FileStream_InfoReport_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_FileStream_InfoReport_ind(std::function<void(const ASAP_Individual asap, const File_Block file_block, const File_Block_Sequence_Number file_block_sequence_number, const File_Handle file_handle, const Hop_Count_Type hop_count_type)> ind)
{
    T_Data_Individual_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority /*priority*/, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_FileStream_InfoReport_PDU> apdu = std::dynamic_pointer_cast<A_FileStream_InfoReport_PDU>(tsdu);
        assert(apdu);
        ind(asap, apdu->file_block, apdu->file_block_sequence_number, apdu->file_handle, hop_count_type);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_FileStream_InfoReport_PDU> pred_apdu = std::dynamic_pointer_cast<A_FileStream_InfoReport_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_PropertyValue_Read */

void Application_Layer::A_PropertyValue_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, std::function<void(const Status a_status)> Lcon)
{
    if (!asap.connected) {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyValue_Read_PDU> apdu = std::make_shared<A_PropertyValue_Read_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->nr_of_elem = nr_of_elem;
        apdu->start_index = start_index;
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_PropertyValue_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    } else {
        if (!T_Data_Connected_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyValue_Read_PDU> apdu = std::make_shared<A_PropertyValue_Read_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->nr_of_elem = nr_of_elem;
        apdu->start_index = start_index;
        const TSAP_Individual tsap = asap.address;
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Lcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_PropertyValue_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
}

void Application_Layer::A_PropertyValue_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_PropertyValue_Read_PDU> apdu = std::dynamic_pointer_cast<A_PropertyValue_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, asap, apdu->object_index, apdu->property_id, apdu->nr_of_elem, apdu->start_index);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_PropertyValue_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_PropertyValue_Read_PDU> apdu = std::dynamic_pointer_cast<A_PropertyValue_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, asap, apdu->object_index, apdu->property_id, apdu->nr_of_elem, apdu->start_index);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_PropertyValue_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_PropertyValue_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data, std::function<void(const Status a_status)> Rcon)
{
    if (!asap.connected) {
        if (!T_Data_Individual_req) {
            Rcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyValue_Response_PDU> apdu = std::make_shared<A_PropertyValue_Response_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->nr_of_elem = nr_of_elem;
        apdu->start_index = start_index;
        apdu->data = data;
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_PropertyValue_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    } else {
        if (!T_Data_Connected_req) {
            Rcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyValue_Response_PDU> apdu = std::make_shared<A_PropertyValue_Response_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->nr_of_elem = nr_of_elem;
        apdu->start_index = start_index;
        apdu->data = data;
        const TSAP_Individual tsap = asap.address;
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Rcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_PropertyValue_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
}

void Application_Layer::A_PropertyValue_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data)> Acon)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_PropertyValue_Response_PDU> apdu = std::dynamic_pointer_cast<A_PropertyValue_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(priority, hop_count_type, asap, apdu->object_index, apdu->property_id, apdu->nr_of_elem, apdu->start_index, apdu->data);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_PropertyValue_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_PropertyValue_Response_PDU> apdu = std::dynamic_pointer_cast<A_PropertyValue_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(priority, hop_count_type, asap, apdu->object_index, apdu->property_id, apdu->nr_of_elem, apdu->start_index, apdu->data);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_PropertyValue_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_PropertyValue_Write */

void Application_Layer::A_PropertyValue_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data, std::function<void(const Status a_status)> Lcon)
{
    if (!asap.connected) {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyValue_Write_PDU> apdu = std::make_shared<A_PropertyValue_Write_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->nr_of_elem = nr_of_elem;
        apdu->start_index = start_index;
        apdu->data = data;
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_PropertyValue_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Write_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    } else {
        if (!T_Data_Connected_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyValue_Write_PDU> apdu = std::make_shared<A_PropertyValue_Write_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->nr_of_elem = nr_of_elem;
        apdu->start_index = start_index;
        apdu->data = data;
        const TSAP_Individual tsap = asap.address;
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Lcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_PropertyValue_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Write_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
}

void Application_Layer::A_PropertyValue_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_PropertyValue_Write_PDU> apdu = std::dynamic_pointer_cast<A_PropertyValue_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, asap, apdu->object_index, apdu->property_id, apdu->nr_of_elem, apdu->start_index, apdu->data);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_PropertyValue_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_PropertyValue_Write_PDU> apdu = std::dynamic_pointer_cast<A_PropertyValue_Write_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(priority, hop_count_type, asap, apdu->object_index, apdu->property_id, apdu->nr_of_elem, apdu->start_index, apdu->data);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_PropertyValue_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyValue_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_PropertyDescription_Read */

void Application_Layer::A_PropertyDescription_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, std::function<void(const Status a_status)> Lcon)
{
    if (!asap.connected) {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyDescription_Read_PDU> apdu = std::make_shared<A_PropertyDescription_Read_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->property_index = property_index;
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_PropertyDescription_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyDescription_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    } else {
        if (!T_Data_Connected_req) {
            Lcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyDescription_Read_PDU> apdu = std::make_shared<A_PropertyDescription_Read_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->property_index = property_index;
        const TSAP_Individual tsap = asap.address;
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Lcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_PropertyDescription_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyDescription_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
}

void Application_Layer::A_PropertyDescription_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index)> ind)
{
    T_Data_Individual_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_PropertyDescription_Read_PDU> apdu = std::dynamic_pointer_cast<A_PropertyDescription_Read_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->object_index, apdu->property_id, apdu->property_index);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_PropertyDescription_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyDescription_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_PropertyDescription_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const bool write_enable, const Property_Type type, const Max_Nr_Of_Elem max_nr_of_elem, const Access access, std::function<void(const Status a_status)> Rcon)
{
    if (!asap.connected) {
        if (!T_Data_Individual_req) {
            Rcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyDescription_Response_PDU> apdu = std::make_shared<A_PropertyDescription_Response_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->property_index = property_index;
        apdu->write_enable = write_enable;
        apdu->type = type;
        apdu->max_nr_of_elem = max_nr_of_elem;
        apdu->access = access;
        const TSAP_Individual tsap = asap.address;
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_PropertyDescription_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyDescription_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    } else {
        if (!T_Data_Connected_req) {
            Rcon(Status::not_ok);
            return;
        }
        std::shared_ptr<A_PropertyDescription_Response_PDU> apdu = std::make_shared<A_PropertyDescription_Response_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->property_index = property_index;
        apdu->write_enable = write_enable;
        apdu->type = type;
        apdu->max_nr_of_elem = max_nr_of_elem;
        apdu->access = access;
        const TSAP_Individual tsap = asap.address;
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Rcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_PropertyDescription_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyDescription_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
}

void Application_Layer::A_PropertyDescription_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const bool write_enable, const Property_Type type, const Max_Nr_Of_Elem max_nr_of_elem, const Access access)> Acon)
{
    T_Data_Individual_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_PropertyDescription_Response_PDU> apdu = std::dynamic_pointer_cast<A_PropertyDescription_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->object_index, apdu->property_id, apdu->property_index, apdu->write_enable, apdu->type, apdu->max_nr_of_elem, apdu->access);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_PropertyDescription_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_PropertyDescription_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Link_Read */

void Application_Layer::A_Link_Read_req(const ASAP_Individual asap, const Group_Object_Number group_object_number, const Priority priority, const Link_Start_Index start_index, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Individual_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Link_Read_PDU> apdu = std::make_shared<A_Link_Read_PDU>();
    apdu->group_object_number = group_object_number;
    apdu->start_index = start_index;
    const TSAP_Individual tsap = asap.address;
    T_Data_Individual_req(Ack_Request::dont_care, Network_Layer_Parameter, priority, tsap, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_Link_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_Link_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Link_Read_ind(std::function<void(const ASAP_Individual asap, const Group_Object_Number group_object_number, const Priority priority, const Link_Start_Index start_index)> ind)
{
    T_Data_Individual_ind_bus.async_wait([ind](const Hop_Count_Type /*hop_count_type*/, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_Link_Read_PDU> apdu = std::dynamic_pointer_cast<A_Link_Read_PDU>(tsdu);
        assert(apdu);
        ind(asap, apdu->group_object_number, priority, apdu->group_object_number);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Link_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_Link_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_Link_Read_res(const ASAP_Individual asap, const std::vector<Group_Address> group_address_list, const Group_Object_Number group_object_number, const Priority priority, const Link_Sending_Address sending_address, const Link_Start_Index start_index, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Individual_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Link_Response_PDU> apdu = std::make_shared<A_Link_Response_PDU>();
    apdu->group_object_number = group_object_number;
    apdu->sending_address = sending_address;
    apdu->start_index = start_index;
    apdu->group_address_list = group_address_list;
    const TSAP_Individual tsap = asap.address;
    T_Data_Individual_req(Ack_Request::dont_care, Network_Layer_Parameter, priority, tsap, apdu);

    auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Rcon(t_status);
    };
    auto predicate = [priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_Link_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Link_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Link_Read_Acon(std::function<void(const ASAP_Individual asap, const std::vector<Group_Address> group_address_list, const Group_Object_Number group_object_number, const Priority priority, const Link_Sending_Address sending_address, const Link_Start_Index start_index)> Acon)
{
    T_Data_Individual_ind_bus.async_wait([Acon](const Hop_Count_Type /*hop_count_type*/, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_Link_Response_PDU> apdu = std::dynamic_pointer_cast<A_Link_Response_PDU>(tsdu);
        assert(apdu);
        Acon(asap, apdu->group_address_list, apdu->group_object_number, priority, apdu->sending_address, apdu->start_index);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Link_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Link_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Link_Write */

void Application_Layer::A_Link_Write_req(const ASAP_Individual asap, const Link_Write_Flags flags, const Group_Address group_address, const Group_Object_Number group_object_number, const Priority priority, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Individual_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Link_Write_PDU> apdu = std::make_shared<A_Link_Write_PDU>();
    apdu->group_object_number = group_object_number;
    apdu->flags = flags;
    apdu->group_address = group_address;
    const TSAP_Individual tsap = asap.address;
    T_Data_Individual_req(Ack_Request::dont_care, Network_Layer_Parameter, priority, tsap, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_Link_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_Link_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Link_Write_ind(std::function<void(const ASAP_Individual asap, const Link_Write_Flags flags, const Group_Address group_address, const Group_Object_Number group_object_number, const Priority priority)> ind)
{
    T_Data_Individual_ind_bus.async_wait([ind](const Hop_Count_Type /*hop_count_type*/, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_Link_Write_PDU> apdu = std::dynamic_pointer_cast<A_Link_Write_PDU>(tsdu);
        assert(apdu);
        ind(asap, apdu->flags, apdu->group_address, apdu->group_object_number, priority);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Link_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_Link_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_FunctionPropertyCommand */

void Application_Layer::A_FunctionPropertyCommand_req(const Ack_Request ack_request, const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, std::function<void(const Status a_status)> Lcon)
{
    std::shared_ptr<A_FunctionPropertyCommand_PDU> apdu = std::make_shared<A_FunctionPropertyCommand_PDU>();
    apdu->object_index = object_index;
    apdu->property_id = property_id;
    apdu->data = data;
    const TSAP_Individual tsap = asap.address;
    switch (comm_mode) {
    case Comm_Mode::Individual:
    {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_FunctionPropertyCommand_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyCommand_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Comm_Mode::Connected:
    {
        if (!T_Data_Connected_req) {
            Lcon(Status::not_ok);
            return;
        }
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Lcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_FunctionPropertyCommand_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyCommand_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    default:
        // other comm modes are not supported
        break;
    }
}

void Application_Layer::A_FunctionPropertyCommand_ind(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_FunctionPropertyCommand_PDU> apdu = std::dynamic_pointer_cast<A_FunctionPropertyCommand_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, Comm_Mode::Individual, apdu->data, hop_count_type, apdu->object_index, priority, apdu->property_id);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_FunctionPropertyCommand_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyCommand_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_FunctionPropertyCommand_PDU> apdu = std::dynamic_pointer_cast<A_FunctionPropertyCommand_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, Comm_Mode::Connected, apdu->data, hop_count_type, apdu->object_index, priority, apdu->property_id);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_FunctionPropertyCommand_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyCommand_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_FunctionPropertyCommand_res(const Ack_Request ack_request, const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, std::function<void(const Status a_status)> Rcon)
{
    std::shared_ptr<A_FunctionPropertyState_Response_PDU> apdu = std::make_shared<A_FunctionPropertyState_Response_PDU>();
    apdu->object_index = object_index;
    apdu->property_id = property_id;
    apdu->return_code = return_code;
    apdu->data = data;
    const TSAP_Individual tsap = asap.address;
    switch (comm_mode) {
    case Comm_Mode::Individual:
    {
        if (!T_Data_Individual_req) {
            Rcon(Status::not_ok);
            return;
        }
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_FunctionPropertyState_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Comm_Mode::Connected:
    {
        if (!T_Data_Connected_req) {
            Rcon(Status::not_ok);
            return;
        }
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Rcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_FunctionPropertyState_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    default:
        // other comm modes are not supported
        break;
    }
}

void Application_Layer::A_FunctionPropertyCommand_Acon(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const std::optional<Property_Return_Code> return_code)> Acon)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(asap, Comm_Mode::Individual, apdu->data, hop_count_type, apdu->object_index, priority, apdu->property_id, apdu->return_code);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(asap, Comm_Mode::Connected, apdu->data, hop_count_type, apdu->object_index, priority, apdu->property_id, apdu->return_code);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_FunctionPropertyState_Read */

void Application_Layer::A_FunctionPropertyState_Read_req(const ASAP_Individual asap, const Ack_Request ack_request, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, std::function<void(const Status a_status)> Lcon)
{
    std::shared_ptr<A_FunctionPropertyState_Read_PDU> apdu = std::make_shared<A_FunctionPropertyState_Read_PDU>();
    apdu->object_index = object_index;
    apdu->property_id = property_id;
    apdu->data = data;
    const TSAP_Individual tsap = asap.address;
    switch (comm_mode) {
    case Comm_Mode::Individual:
    {
        if (!T_Data_Individual_req) {
            Lcon(Status::not_ok);
            return;
        }
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Lcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_FunctionPropertyState_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Comm_Mode::Connected:
    {
        if (!T_Data_Connected_req) {
            Lcon(Status::not_ok);
            return;
        }
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Lcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_FunctionPropertyState_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Read_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    default:
        // other comm modes are not supported
        break;
    }
}

void Application_Layer::A_FunctionPropertyState_Read_ind(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id)> ind)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_FunctionPropertyState_Read_PDU> apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, Comm_Mode::Individual, apdu->data, hop_count_type, apdu->object_index, priority, apdu->property_id);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_FunctionPropertyState_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([ind, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_FunctionPropertyState_Read_PDU> apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Read_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            ind(asap, Comm_Mode::Connected, apdu->data, hop_count_type, apdu->object_index, priority, apdu->property_id);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_FunctionPropertyState_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_FunctionPropertyState_Read_res(const Ack_Request ack_request, const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, std::function<void(const Status a_status)> Rcon)
{
    std::shared_ptr<A_FunctionPropertyState_Response_PDU> apdu = std::make_shared<A_FunctionPropertyState_Response_PDU>();
    apdu->object_index = object_index;
    apdu->property_id = property_id;
    apdu->return_code = return_code;
    apdu->data = data;
    const TSAP_Individual tsap = asap.address;
    switch (comm_mode) {
    case Comm_Mode::Individual:
    {
        if (!T_Data_Individual_req) {
            Rcon(Status::not_ok);
            return;
        }
        T_Data_Individual_req(ack_request, hop_count_type, priority, tsap, apdu);

        auto completion_handler = [Rcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
            Rcon(t_status);
        };
        auto predicate = [ack_request, priority, tsap, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, const TSAP_Individual pred_tsap, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
            std::shared_ptr<A_FunctionPropertyState_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Individual_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    case Comm_Mode::Connected:
    {
        if (!T_Data_Connected_req) {
            Rcon(Status::not_ok);
            return;
        }
        T_Data_Connected_req(priority, tsap, apdu);

        auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
            Rcon(Status::ok);
        };
        auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
            std::shared_ptr<A_FunctionPropertyState_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(pred_tsdu);
            return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
        };
        T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
    }
        break;
    default:
        // other comm modes are not supported
        break;
    }
}

void Application_Layer::A_FunctionPropertyState_Read_Acon(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const std::optional<Property_Return_Code> return_code)> Acon)
{
    std::shared_ptr<int> one_shot{std::make_shared<int>(0)};

    T_Data_Individual_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap {tsap, false};
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(asap, Comm_Mode::Individual, apdu->data, hop_count_type, apdu->object_index, priority, apdu->property_id, apdu->return_code);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

    T_Data_Connected_ind_bus.async_wait([Acon, one_shot](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(tsdu);
        assert(apdu);
        if (*one_shot == 0) {
            *one_shot += 1;
            Acon(asap, Comm_Mode::Connected, apdu->data, hop_count_type, apdu->object_index, priority, apdu->property_id, apdu->return_code);
        }
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_ADC_Read */

void Application_Layer::A_ADC_Read_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_ADC_Read_PDU> apdu = std::make_shared<A_ADC_Read_PDU>();
    apdu->channel_nr = channel_nr;
    apdu->read_count = read_count;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_ADC_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_ADC_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_ADC_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_ADC_Read_PDU> apdu = std::dynamic_pointer_cast<A_ADC_Read_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->channel_nr, apdu->read_count);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_ADC_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_ADC_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_ADC_Read_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count, const ADC_Sum sum, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_ADC_Response_PDU> apdu = std::make_shared<A_ADC_Response_PDU>();
    apdu->channel_nr = channel_nr;
    apdu->read_count = read_count;
    apdu->sum = sum;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_ADC_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_ADC_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_ADC_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count, const ADC_Sum sum)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_ADC_Response_PDU> apdu = std::dynamic_pointer_cast<A_ADC_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->channel_nr, apdu->read_count, apdu->sum);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_ADC_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_ADC_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Memory_Read */

void Application_Layer::A_Memory_Read_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Memory_Read_PDU> apdu = std::make_shared<A_Memory_Read_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Memory_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_Memory_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Memory_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Memory_Read_PDU> apdu = std::dynamic_pointer_cast<A_Memory_Read_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->number, apdu->address);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Memory_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_Memory_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_Memory_Read_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Memory_Response_PDU> apdu = std::make_shared<A_Memory_Response_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Memory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Memory_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Memory_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Memory_Response_PDU> apdu = std::dynamic_pointer_cast<A_Memory_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->number, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Memory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Memory_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Memory_Write */

void Application_Layer::A_Memory_Write_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Memory_Write_PDU> apdu = std::make_shared<A_Memory_Write_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Memory_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_Memory_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Memory_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Memory_Write_PDU> apdu = std::dynamic_pointer_cast<A_Memory_Write_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->number, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Memory_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_Memory_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_Memory_Write_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Memory_Response_PDU> apdu = std::make_shared<A_Memory_Response_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Memory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Memory_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Memory_Write_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Memory_Response_PDU> apdu = std::dynamic_pointer_cast<A_Memory_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->number, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Memory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Memory_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_MemoryBit_Write */

void Application_Layer::A_MemoryBit_Write_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const MemoryBit_Number number, const MemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_MemoryBit_Write_PDU> apdu = std::make_shared<A_MemoryBit_Write_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->and_data = and_data;
    apdu->xor_data = xor_data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_MemoryBit_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_MemoryBit_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_MemoryBit_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const MemoryBit_Number number, const MemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_MemoryBit_Write_PDU> apdu = std::dynamic_pointer_cast<A_MemoryBit_Write_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->number, apdu->address, apdu->and_data, apdu->xor_data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_MemoryBit_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_MemoryBit_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_MemoryBit_Write_res(const ASAP_Connected asap, const Priority priority, const MemoryBit_Number number, const MemoryBit_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Memory_Response_PDU> apdu = std::make_shared<A_Memory_Response_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Memory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Memory_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

/* A_UserMemory_Read */

void Application_Layer::A_UserMemory_Read_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_UserMemory_Read_PDU> apdu = std::make_shared<A_UserMemory_Read_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_UserMemory_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_UserMemory_Read_PDU> apdu = std::dynamic_pointer_cast<A_UserMemory_Read_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->number, apdu->address);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_UserMemory_Read_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_UserMemory_Response_PDU> apdu = std::make_shared<A_UserMemory_Response_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_UserMemory_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_UserMemory_Response_PDU> apdu = std::dynamic_pointer_cast<A_UserMemory_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->number, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_UserMemory_Write */

void Application_Layer::A_UserMemory_Write_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_UserMemory_Write_PDU> apdu = std::make_shared<A_UserMemory_Write_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_UserMemory_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_UserMemory_Write_PDU> apdu = std::dynamic_pointer_cast<A_UserMemory_Write_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->number, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_UserMemory_Write_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_UserMemory_Response_PDU> apdu = std::make_shared<A_UserMemory_Response_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_UserMemory_Write_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_UserMemory_Response_PDU> apdu = std::dynamic_pointer_cast<A_UserMemory_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->number, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_UserMemoryBit_Write */

void Application_Layer::A_UserMemoryBit_Write_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const UserMemoryBit_Number number, const UserMemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_UserMemoryBit_Write_PDU> apdu = std::make_shared<A_UserMemoryBit_Write_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->and_data = and_data;
    apdu->xor_data = xor_data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemoryBit_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemoryBit_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_UserMemoryBit_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemoryBit_Number number, const UserMemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_UserMemoryBit_Write_PDU> apdu = std::dynamic_pointer_cast<A_UserMemoryBit_Write_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->number, apdu->address, apdu->and_data, apdu->xor_data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemoryBit_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemoryBit_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_UserMemoryBit_Write_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const UserMemoryBit_Number number, const UserMemoryBit_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_UserMemory_Response_PDU> apdu = std::make_shared<A_UserMemory_Response_PDU>();
    apdu->number = number;
    apdu->address = memory_address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_UserMemoryBit_Write_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemoryBit_Number number, const UserMemoryBit_Address memory_address, const Memory_Data data)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_UserMemory_Response_PDU> apdu = std::dynamic_pointer_cast<A_UserMemory_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->number, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserMemory_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserMemory_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });

}

/* A_UserManufacturerInfo_Read */

void Application_Layer::A_UserManufacturerInfo_Read_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_UserManufacturerInfo_Read_PDU> apdu = std::make_shared<A_UserManufacturerInfo_Read_PDU>();
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserManufacturerInfo_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserManufacturerInfo_Read_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_UserManufacturerInfo_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        const ASAP_Connected asap {tsap, true};
        //std::shared_ptr<A_UserManufacturerInfo_Read_PDU> apdu = std::dynamic_pointer_cast<A_UserManufacturerInfo_Read_PDU>(tsdu);
        //assert(apdu);
        ind(priority, hop_count_type, asap);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserManufacturerInfo_Read_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserManufacturerInfo_Read_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_UserManufacturerInfo_Read_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const MFact_Info mfact_info, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_UserManufacturerInfo_Response_PDU> apdu = std::make_shared<A_UserManufacturerInfo_Response_PDU>();
    apdu->manufacturer_info = mfact_info;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserManufacturerInfo_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserManufacturerInfo_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_UserManufacturerInfo_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const MFact_Info mfact_info)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_UserManufacturerInfo_Response_PDU> apdu = std::dynamic_pointer_cast<A_UserManufacturerInfo_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->manufacturer_info);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_UserManufacturerInfo_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_UserManufacturerInfo_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Authorize_Request */

void Application_Layer::A_Authorize_Request_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Key key, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Authorize_Request_PDU> apdu = std::make_shared<A_Authorize_Request_PDU>();
    apdu->key = key;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Authorize_Request_PDU> pred_apdu = std::dynamic_pointer_cast<A_Authorize_Request_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Authorize_Request_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Key key)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Authorize_Request_PDU> apdu = std::dynamic_pointer_cast<A_Authorize_Request_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->key);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Authorize_Request_PDU> pred_apdu = std::dynamic_pointer_cast<A_Authorize_Request_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_Authorize_Request_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Level level, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Authorize_Response_PDU> apdu = std::make_shared<A_Authorize_Response_PDU>();
    apdu->level = level;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Authorize_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Authorize_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Authorize_Request_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Authorize_Response_PDU> apdu = std::dynamic_pointer_cast<A_Authorize_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->level);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Authorize_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Authorize_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Key_Write */

void Application_Layer::A_Key_Write_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Level level, const Key key, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Key_Write_PDU> apdu = std::make_shared<A_Key_Write_PDU>();
    apdu->level = level;
    apdu->key = key;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Key_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_Key_Write_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Key_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level, const Key key)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Key_Write_PDU> apdu = std::dynamic_pointer_cast<A_Key_Write_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->level, apdu->key);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Key_Write_PDU> pred_apdu = std::dynamic_pointer_cast<A_Key_Write_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_Key_Write_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Level level, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Key_Response_PDU> apdu = std::make_shared<A_Key_Response_PDU>();
    apdu->level = level;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Key_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Key_Response_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Key_Write_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Key_Response_PDU> apdu = std::dynamic_pointer_cast<A_Key_Response_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->level);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Key_Response_PDU> pred_apdu = std::dynamic_pointer_cast<A_Key_Response_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Open_Routing_Table */

void Application_Layer::A_Open_Routing_Table_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Open_Routing_Table_Req_PDU> apdu = std::make_shared<A_Open_Routing_Table_Req_PDU>();
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Open_Routing_Table_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Open_Routing_Table_Req_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Open_Routing_Table_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        const ASAP_Connected asap {tsap, true};
        //std::shared_ptr<A_Open_Routing_Table_Req_PDU> apdu = std::dynamic_pointer_cast<A_Open_Routing_Table_Req_PDU>(tsdu);
        //assert(apdu);
        ind(priority, hop_count_type, asap);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Open_Routing_Table_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Open_Routing_Table_Req_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Read_Routing_Table */

void Application_Layer::A_Read_Routing_Table_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t count, const uint16_t address, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Read_Routing_Table_Req_PDU> apdu = std::make_shared<A_Read_Routing_Table_Req_PDU>();
    apdu->count = count;
    apdu->address = address;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Routing_Table_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Routing_Table_Req_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Read_Routing_Table_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Read_Routing_Table_Req_PDU> apdu = std::dynamic_pointer_cast<A_Read_Routing_Table_Req_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->count, apdu->address);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Routing_Table_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Routing_Table_Req_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_Read_Routing_Table_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Read_Routing_Table_Res_PDU> apdu = std::make_shared<A_Read_Routing_Table_Res_PDU>();
    apdu->count = count;
    apdu->address = address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Routing_Table_Res_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Routing_Table_Res_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Read_Routing_Table_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Read_Routing_Table_Res_PDU> apdu = std::dynamic_pointer_cast<A_Read_Routing_Table_Res_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->count, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Routing_Table_Res_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Routing_Table_Res_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Write_Routing_Table */

void Application_Layer::A_Write_Routing_Table_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Write_Routing_Table_Req_PDU> apdu = std::make_shared<A_Write_Routing_Table_Req_PDU>();
    apdu->count = count;
    apdu->address = address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Write_Routing_Table_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Write_Routing_Table_Req_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Write_Routing_Table_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Write_Routing_Table_Req_PDU> apdu = std::dynamic_pointer_cast<A_Write_Routing_Table_Req_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->count, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Write_Routing_Table_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Write_Routing_Table_Req_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Read_Router_Memory */

void Application_Layer::A_Read_Router_Memory_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t count, const uint16_t address, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Read_Router_Memory_Req_PDU> apdu = std::make_shared<A_Read_Router_Memory_Req_PDU>();
    apdu->count = count;
    apdu->address = address;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Router_Memory_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Router_Memory_Req_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Read_Router_Memory_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Read_Router_Memory_Req_PDU> apdu = std::dynamic_pointer_cast<A_Read_Router_Memory_Req_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->count, apdu->address);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Router_Memory_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Router_Memory_Req_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_Read_Router_Memory_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Read_Router_Memory_Res_PDU> apdu = std::make_shared<A_Read_Router_Memory_Res_PDU>();
    apdu->count = count;
    apdu->address = address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Router_Memory_Res_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Router_Memory_Res_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Read_Router_Memory_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Read_Router_Memory_Res_PDU> apdu = std::dynamic_pointer_cast<A_Read_Router_Memory_Res_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->count, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Router_Memory_Res_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Router_Memory_Res_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Write_Router_Memory */

void Application_Layer::A_Write_Router_Memory_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::ok);
        return;
    }
    std::shared_ptr<A_Write_Router_Memory_Req_PDU> apdu = std::make_shared<A_Write_Router_Memory_Req_PDU>();
    apdu->count = count;
    apdu->address = address;
    apdu->data = data;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Write_Router_Memory_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Write_Router_Memory_Req_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Write_Router_Memory_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Write_Router_Memory_Req_PDU> apdu = std::dynamic_pointer_cast<A_Write_Router_Memory_Req_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->count, apdu->address, apdu->data);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Write_Router_Memory_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Write_Router_Memory_Req_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Read_Router_Status */

void Application_Layer::A_Read_Router_Status_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Read_Router_Status_Req_PDU> apdu = std::make_shared<A_Read_Router_Status_Req_PDU>();
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Router_Status_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Router_Status_Req_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Read_Router_Status_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Read_Router_Status_Req_PDU> apdu = std::dynamic_pointer_cast<A_Read_Router_Status_Req_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Router_Status_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Router_Status_Req_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

void Application_Layer::A_Read_Router_Status_res(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t route_table_state, std::function<void(const Status a_status)> Rcon)
{
    if (!T_Data_Connected_req) {
        Rcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_Read_Router_Status_Res_PDU> apdu = std::make_shared<A_Read_Router_Status_Res_PDU>();
    apdu->route_table_state = route_table_state;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Rcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Rcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Router_Status_Res_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Router_Status_Res_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Read_Router_Status_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t route_table_state)> Acon)
{
    T_Data_Connected_ind_bus.async_wait([Acon](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Read_Router_Status_Res_PDU> apdu = std::dynamic_pointer_cast<A_Read_Router_Status_Res_PDU>(tsdu);
        assert(apdu);
        Acon(priority, hop_count_type, asap, apdu->route_table_state);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Read_Router_Status_Res_PDU> pred_apdu = std::dynamic_pointer_cast<A_Read_Router_Status_Res_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_Write_Router_Status */

void Application_Layer::A_Write_Router_Status_req(const Ack_Request /*ack_request*/, const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t route_table_state, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Connected_req) {
        Lcon(Status::ok);
        return;
    }
    std::shared_ptr<A_Write_Router_Status_Req_PDU> apdu = std::make_shared<A_Write_Router_Status_Req_PDU>();
    apdu->route_table_state = route_table_state;
    const TSAP_Connected tsap = asap.address;
    T_Data_Connected_req(priority, tsap, apdu);

    auto completion_handler = [Lcon](const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {
        Lcon(Status::ok);
    };
    auto predicate = [priority, tsap, apdu](const Priority pred_priority, const TSAP_Connected pred_tsap, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Write_Router_Status_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Write_Router_Status_Req_PDU>(pred_tsdu);
        return pred_apdu && (pred_priority == priority) && (pred_tsap == tsap) && (*pred_apdu == *apdu);
    };
    T_Data_Connected_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_Write_Router_Status_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t route_table_state)> ind)
{
    T_Data_Connected_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Connected asap {tsap, true};
        std::shared_ptr<A_Write_Router_Status_Req_PDU> apdu = std::dynamic_pointer_cast<A_Write_Router_Status_Req_PDU>(tsdu);
        assert(apdu);
        ind(priority, hop_count_type, asap, apdu->route_table_state);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const TSAP_Individual /*pred_tsap*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_Write_Router_Status_Req_PDU> pred_apdu = std::dynamic_pointer_cast<A_Write_Router_Status_Req_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* A_ServiceInformation_Indication */

void Application_Layer::A_ServiceInformation_Indication_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const uint24_t info, std::function<void(const Status a_status)> Lcon)
{
    if (!T_Data_Broadcast_req) {
        Lcon(Status::not_ok);
        return;
    }
    std::shared_ptr<A_ServiceInformation_Indication_PDU> apdu = std::make_shared<A_ServiceInformation_Indication_PDU>();
    apdu->info = info;
    T_Data_Broadcast_req(ack_request, hop_count_type, priority, apdu);

    auto completion_handler = [Lcon](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status t_status) -> void {
        Lcon(t_status);
    };
    auto predicate = [ack_request, priority, apdu](const Ack_Request pred_ack_request, const Hop_Count_Type /*pred_hop_count_type*/, const Priority pred_priority, std::shared_ptr<TSDU> pred_tsdu, const Status /*t_status*/) -> bool {
        std::shared_ptr<A_ServiceInformation_Indication_PDU> pred_apdu = std::dynamic_pointer_cast<A_ServiceInformation_Indication_PDU>(pred_tsdu);
        return pred_apdu && (pred_ack_request == ack_request) && (pred_priority == priority) && (*pred_apdu == *apdu);
    };
    T_Data_Broadcast_con_bus.async_wait(completion_handler, predicate);
}

void Application_Layer::A_ServiceInformation_Indication_ind(std::function<void(const ASAP_Individual asap, const Priority priority, const Hop_Count_Type hop_count_type, const uint24_t info)> ind)
{
    T_Data_Broadcast_ind_bus.async_wait([ind](const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu) -> void {
        const ASAP_Individual asap{source_address, false};
        std::shared_ptr<A_ServiceInformation_Indication_PDU> apdu = std::dynamic_pointer_cast<A_ServiceInformation_Indication_PDU>(tsdu);
        assert(apdu);
        ind(asap, priority, hop_count_type, apdu->info);
    }, [](const Hop_Count_Type /*pred_hop_count_type*/, const Priority /*pred_priority*/, const Individual_Address /*pred_source_address*/, std::shared_ptr<TSDU> pred_tsdu) -> bool {
        std::shared_ptr<A_ServiceInformation_Indication_PDU> pred_apdu = std::dynamic_pointer_cast<A_ServiceInformation_Indication_PDU>(pred_tsdu);
        return pred_apdu && true;
    });
}

/* T_Data_Group service */

void Application_Layer::T_Data_Group_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    T_Data_Group_con_bus.notify(ack_request, hop_count_type, priority, source_address, tsap, tsdu, t_status);
}

void Application_Layer::T_Data_Group_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu)
{
    T_Data_Group_ind_bus.notify(hop_count_type, priority, source_address, tsap, tsdu);
}

/* T_Data_Tag_Group service */

void Application_Layer::T_Data_Tag_Group_con(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    T_Data_Tag_Group_con_bus.notify(hop_count_type, priority, source_address, destination_address, extended_frame_format, tsdu, t_status);
}

void Application_Layer::T_Data_Tag_Group_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu)
{
    T_Data_Tag_Group_ind_bus.notify(hop_count_type, priority, source_address, destination_address, extended_frame_format, tsdu);
}

/* T_Data_Broadcast service */

void Application_Layer::T_Data_Broadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    T_Data_Broadcast_con_bus.notify(ack_request, hop_count_type, priority, tsdu, t_status);
}

void Application_Layer::T_Data_Broadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu)
{
    T_Data_Broadcast_ind_bus.notify(hop_count_type, priority, source_address, tsdu);
}

/* T_Data_SystemBroadcast service */

void Application_Layer::T_Data_SystemBroadcast_con(const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    T_Data_SystemBroadcast_con_bus.notify(hop_count_type, priority, tsdu, t_status);
}

void Application_Layer::T_Data_SystemBroadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu)
{
    T_Data_SystemBroadcast_ind_bus.notify(hop_count_type, priority, source_address, tsdu);
}

/* T_Data_Individual service */

void Application_Layer::T_Data_Individual_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    T_Data_Individual_con_bus.notify(ack_request, hop_count_type, priority, tsap, tsdu, t_status);
}

void Application_Layer::T_Data_Individual_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu)
{
    T_Data_Individual_ind_bus.notify(hop_count_type, priority, tsap, tsdu);
}

/* T_Connect service */

void Application_Layer::T_Connect_con(const Individual_Address destination_address, const TSAP_Connected tsap, const Status t_status)
{
    T_Connect_con_bus.notify(destination_address, tsap, t_status);
}

void Application_Layer::T_Connect_ind(const TSAP_Connected tsap)
{
    T_Connect_ind_bus.notify(tsap);
}

/* T_Disconnect service */

void Application_Layer::T_Disconnect_con(const Priority priority, const TSAP_Connected tsap, const Status t_status)
{
    T_Disconnect_con_bus.notify(priority, tsap, t_status);
}

void Application_Layer::T_Disconnect_ind(const TSAP_Connected tsap)
{
    T_Disconnect_ind_bus.notify(tsap);
}

/* T_Data_Connected service */

void Application_Layer::T_Data_Connected_con(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)
{
    T_Data_Connected_con_bus.notify(priority, tsap, tsdu);
}

void Application_Layer::T_Data_Connected_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)
{
    T_Data_Connected_ind_bus.notify(hop_count_type, priority, tsap, tsdu);
}

}
