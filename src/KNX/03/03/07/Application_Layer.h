// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>
#include <optional>

#include <KNX/03/03/04/Transport_Layer.h>
#include <KNX/03/03/07/ADC_Channel_Nr.h>
#include <KNX/03/03/07/ADC_Read_Count.h>
#include <KNX/03/03/07/ADC_Sum.h>
#include <KNX/03/03/07/ASAP.h>
#include <KNX/03/03/07/ASDU.h>
#include <KNX/03/03/07/Access.h>
#include <KNX/03/03/07/Comm_Mode.h>
#include <KNX/03/03/07/Descriptor_Type.h>
#include <KNX/03/03/07/Device_Descriptor_Data.h>
#include <KNX/03/03/07/Domain_Address.h>
#include <KNX/03/03/07/Domain_Address_2.h>
#include <KNX/03/03/07/File_Block.h>
#include <KNX/03/03/07/File_Block_Sequence_Number.h>
#include <KNX/03/03/07/File_Handle.h>
#include <KNX/03/03/07/Group_Object_Number.h>
#include <KNX/03/03/07/Group_Value.h>
#include <KNX/03/03/07/Key.h>
#include <KNX/03/03/07/Level.h>
#include <KNX/03/03/07/Link_Sending_Address.h>
#include <KNX/03/03/07/Link_Start_Index.h>
#include <KNX/03/03/07/Link_Write_Flags.h>
#include <KNX/03/03/07/MFact_Info.h>
#include <KNX/03/03/07/Max_Nr_Of_Elem.h>
#include <KNX/03/03/07/MemoryBit_Address.h>
#include <KNX/03/03/07/MemoryBit_Number.h>
#include <KNX/03/03/07/Memory_Address.h>
#include <KNX/03/03/07/Memory_Data.h>
#include <KNX/03/03/07/Memory_Number.h>
#include <KNX/03/03/07/Object_Index.h>
#include <KNX/03/03/07/Object_Instance.h>
#include <KNX/03/03/07/Parameter_Test_Info.h>
#include <KNX/03/03/07/Parameter_Test_Info_Result.h>
#include <KNX/03/03/07/Parameter_Test_Result.h>
#include <KNX/03/03/07/Parameter_Type.h>
#include <KNX/03/03/07/Parameter_Value.h>
#include <KNX/03/03/07/PropertyValue_Nr_Of_Elem.h>
#include <KNX/03/03/07/PropertyValue_Start_Index.h>
#include <KNX/03/03/07/Property_Id.h>
#include <KNX/03/03/07/Property_Return_Code.h>
#include <KNX/03/03/07/Property_Type.h>
#include <KNX/03/03/07/Property_Value.h>
#include <KNX/03/03/07/Restart_Channel_Number.h>
#include <KNX/03/03/07/Restart_Erase_Code.h>
#include <KNX/03/03/07/Restart_Error_Code.h>
#include <KNX/03/03/07/Restart_Process_Time.h>
#include <KNX/03/03/07/Restart_Type.h>
#include <KNX/03/03/07/Serial_Number.h>
#include <KNX/03/03/07/System_Parameter_Type.h>
#include <KNX/03/03/07/UserMemoryBit_Address.h>
#include <KNX/03/03/07/UserMemoryBit_Number.h>
#include <KNX/03/03/07/UserMemory_Address.h>
#include <KNX/03/03/07/UserMemory_Number.h>
#include <KNX/03/05/01/02_Group_Object_Association_Table/Group_Object_Association_Table.h>
#include <KNX/10/01/Zone.h>
#include <KNX/Message_Bus.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/** Application Layer */
class KNX_EXPORT Application_Layer
{
public:
    explicit Application_Layer(Transport_Layer & transport_layer, asio::io_context & io_context);
    virtual ~Application_Layer();

    /**
     * communication medium
     *
     * @note used in A_SystemNetworkParameter services.
     * @todo Unclear what it is and where it comes from. But it must come from Physical_Layer and be set by an interface object property.
     *
     * Only devices on open medium have a Domain Address (PL110, RF).
     */
    enum class Communication_Medium {
        /** open communication medium (system broadcast) */
        open,

        /** closed communication medium (broadcast) */
        closed
    };

    /** @copydoc Communication_Medium */
    Communication_Medium communication_medium{Communication_Medium::closed};

    virtual void A_Connect_req(const ASAP_Connected asap, const Priority priority, std::function<void(const Status a_status)> Lcon);
    virtual void A_Connect_ind(std::function<void(const ASAP_Connected asap)> ind);

    virtual void A_Disconnect_req(const Priority priority, const ASAP_Connected asap, std::function<void(const Status a_status)> Lcon);
    virtual void A_Disconnect_ind(std::function<void(const ASAP_Connected asap)> ind);

    /* Services (Group) */

    virtual void A_GroupValue_Read_req(const Ack_Request ack_request, const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon);
    virtual void A_GroupValue_Read_ind(std::function<void(const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type)> ind);
    virtual void A_GroupValue_Read_res(const Ack_Request ack_request, const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, const Group_Value data, std::function<void(const Status a_status)> Rcon);
    virtual void A_GroupValue_Read_Acon(std::function<void(const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, const Group_Value data)> Acon);

    virtual void A_GroupValue_Write_req(const Ack_Request ack_request, const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, const Group_Value data, std::function<void(const Status a_status)> Lcon);
    virtual void A_GroupValue_Write_ind(std::function<void(const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type, const Group_Value data)> ind);

    /* Services (Tag Group) */

    virtual void A_GroupPropValue_Read_req(const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Ack_Request ack_req, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon);
    virtual void A_GroupPropValue_Read_ind(std::function<void(const ASAP_Individual source_address, const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Priority priority, const Hop_Count_Type hop_count_type)> ind);
    virtual void A_GroupPropValue_Response_req(const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Ack_Request ack_req, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon);
    virtual void A_GroupPropValue_Response_ind(std::function<void(const ASAP_Individual source_address, const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Priority priority, const Hop_Count_Type hop_count_type)> ind);
    virtual void A_GroupPropValue_Write_req(const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Ack_Request ack_req, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon);
    virtual void A_GroupPropValue_Write_ind(std::function<void(const ASAP_Individual source_address, const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Priority priority, const Hop_Count_Type hop_count_type)> ind);
    virtual void A_GroupPropValue_InfoReport_req(const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon);
    virtual void A_GroupPropValue_InfoReport_ind(std::function<void(const ASAP_Individual source_address, const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const Priority priority, const Hop_Count_Type hop_count_type)> ind);

    /* Services (Broadcast) */

    virtual void A_IndividualAddress_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Individual_Address newaddress, std::function<void(const Status a_status)> Lcon);
    virtual void A_IndividualAddress_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Individual_Address newaddress)> ind);

    virtual void A_IndividualAddress_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon);
    virtual void A_IndividualAddress_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type)> ind);
    virtual void A_IndividualAddress_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Individual_Address individual_address, std::function<void(const Status a_status)> Rcon);
    virtual void A_IndividualAddress_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Individual_Address individual_address)> Acon);

    virtual void A_IndividualAddressSerialNumber_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, std::function<void(const Status a_status)> Lcon);
    virtual void A_IndividualAddressSerialNumber_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number)> ind);
    virtual void A_IndividualAddressSerialNumber_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, const Domain_Address_2 domain_address, std::function<void(const Status a_status)> Rcon);
    virtual void A_IndividualAddressSerialNumber_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, const Individual_Address individual_address, const Domain_Address_2 domain_address)> Acon);

    virtual void A_IndividualAddressSerialNumber_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, const Individual_Address newaddress, std::function<void(const Status a_status)> Lcon);
    virtual void A_IndividualAddressSerialNumber_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number, const Individual_Address newaddress)> ind);

    virtual void A_NetworkParameter_Read_req(const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, std::function<void(const Status a_status)> Lcon);
    virtual void A_NetworkParameter_Read_ind(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info)> ind);
    virtual void A_NetworkParameter_Read_res(const ASAP_Individual asap, const Comm_Mode comm_mode, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, const Parameter_Test_Result test_result, std::function<void(const Status a_status)> Rcon);
    virtual void A_NetworkParameter_Read_Acon(std::function<void(const ASAP_Individual asap, const Hop_Count_Type hop_count_type, const Individual_Address individual_address, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info_Result test_info_result)> Acon);

    virtual void A_NetworkParameter_Write_req(const ASAP_Individual asap, const Comm_Mode comm_mode, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Value value, std::function<void(const Status a_status)> Lcon);
    virtual void A_NetworkParameter_Write_ind(std::function<void(const ASAP_Individual asap, const Parameter_Type parameter_type, const Priority priority, const Parameter_Value value)> ind);

    virtual void A_NetworkParameter_InfoReport_req(const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, const Parameter_Test_Result test_result, std::function<void(const Status a_status)> Lcon);
    virtual void A_NetworkParameter_InfoReport_ind(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info_Result test_info_result)> ind);

    /* Services (System Broadcast) */

    virtual void A_DeviceDescriptor_InfoReport_req(const Ack_Request ack_request, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor, const Hop_Count_Type hop_count_type, const Priority priority, std::function<void(const Status a_status)> Lcon);
    virtual void A_DeviceDescriptor_InfoReport_ind(std::function<void(const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor, const Hop_Count_Type hop_count_type, const Priority priority)> ind);

    virtual void A_DomainAddress_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Domain_Address domain_address_new, std::function<void(const Status a_status)> Lcon);
    virtual void A_DomainAddress_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const Domain_Address domain_address_new)> ind);

    virtual void A_DomainAddress_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon);
    virtual void A_DomainAddress_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type)> ind);
    virtual void A_DomainAddress_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const Domain_Address domain_address, std::function<void(const Status a_status)> Rcon);
    virtual void A_DomainAddress_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Domain_Address domain_address)> Acon); // @note "ASAP_Individual asap" added

    virtual void A_DomainAddressSelective_Read_req(const Priority priority, const Hop_Count_Type hop_count_type, const ASDU asdu, std::function<void(const Status a_status)> Lcon);
    virtual void A_DomainAddressSelective_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASDU asdu)> ind);

    virtual void A_DomainAddressSerialNumber_Read_req(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number, std::function<void(const Status a_status)> Lcon);
    virtual void A_DomainAddressSerialNumber_Read_ind(std::function<void(const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number)> ind);
    virtual void A_DomainAddressSerialNumber_Read_res(const Ack_Request ack_request, const Domain_Address domain_address, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number, std::function<void(const Status a_status)> Rcon);
    virtual void A_DomainAddressSerialNumber_Read_Acon(std::function<void(const Domain_Address domain_address, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number)> Acon);

    virtual void A_DomainAddressSerialNumber_Write_req(const Ack_Request ack_request, const Domain_Address domain_address, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number, std::function<void(const Status a_status)> Lcon);
    virtual void A_DomainAddressSerialNumber_Write_ind(std::function<void(const Domain_Address domain_address, const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number)> ind);

    virtual void A_SystemNetworkParameter_Read_req(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, std::function<void(const Status a_status)> Lcon);
    virtual void A_SystemNetworkParameter_Read_ind(std::function<void(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info)> ind);
    virtual void A_SystemNetworkParameter_Read_res(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info test_info, const Parameter_Test_Result test_result, std::function<void(const Status a_status)> Rcon);
    virtual void A_SystemNetworkParameter_Read_Acon(std::function<void(const ASAP_Individual asap, const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Test_Info_Result test_info_result)> Acon);

    virtual void A_SystemNetworkParameter_Write_req(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Value value, std::function<void(const Status a_status)> Lcon);
    virtual void A_SystemNetworkParameter_Write_ind(std::function<void(const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const Parameter_Value value)> ind);

    /* Services (Individual) */

    virtual void A_DeviceDescriptor_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Descriptor_Type descriptor_type, std::function<void(const Status a_status)> Lcon);
    virtual void A_DeviceDescriptor_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Descriptor_Type descriptor_type)> ind);
    virtual void A_DeviceDescriptor_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor, std::function<void(const Status a_status)> Rcon);
    virtual void A_DeviceDescriptor_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor)> Acon);

    virtual void A_Restart_req(const Ack_Request ack_request, const Restart_Channel_Number channel_number, const Restart_Erase_Code erase_code, const Priority priority, const Hop_Count_Type hop_count_type, const Restart_Type restart_type, const ASAP_Individual asap, std::function<void(const Status a_status)> Lcon);
    virtual void A_Restart_ind(std::function<void(const Restart_Erase_Code erase_code, const Restart_Channel_Number channel_number, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap)> ind);
    virtual void A_Restart_res(const Restart_Error_Code error_code, const Priority priority, const Restart_Process_Time process_time, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, std::function<void(const Status a_status)> Rcon);
    virtual void A_Restart_Acon(std::function<void(const Restart_Error_Code error_code, const Priority priority, const Restart_Process_Time process_time, const Hop_Count_Type hop_count_type, const ASAP_Individual asap)> Acon);

    virtual void A_FileStream_InfoReport_req(const Ack_Request ack_request, const ASAP_Individual asap, const File_Block file_block, const File_Block_Sequence_Number file_block_sequence_number, const File_Handle file_handle, const Hop_Count_Type hop_count_type, std::function<void(const Status a_status)> Lcon);
    virtual void A_FileStream_InfoReport_ind(std::function<void(const ASAP_Individual asap, const File_Block file_block, const File_Block_Sequence_Number file_block_sequence_number, const File_Handle file_handle, const Hop_Count_Type hop_count_type)> ind);

    virtual void A_PropertyValue_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, std::function<void(const Status a_status)> Lcon);
    virtual void A_PropertyValue_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index)> ind);
    virtual void A_PropertyValue_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data, std::function<void(const Status a_status)> Rcon);
    virtual void A_PropertyValue_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data)> Acon);

    virtual void A_PropertyValue_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data, std::function<void(const Status a_status)> Lcon);
    virtual void A_PropertyValue_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data)> ind);

    virtual void A_PropertyDescription_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, std::function<void(const Status a_status)> Lcon);
    virtual void A_PropertyDescription_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index)> ind);
    virtual void A_PropertyDescription_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const bool write_enable, const Property_Type type, const Max_Nr_Of_Elem max_nr_of_elem, const Access access, std::function<void(const Status a_status)> Rcon);
    virtual void A_PropertyDescription_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const bool write_enable, const Property_Type type, const Max_Nr_Of_Elem max_nr_of_elem, const Access access)> Acon);

    virtual void A_Link_Read_req(const ASAP_Individual asap, const Group_Object_Number group_object_number, const Priority priority, const Link_Start_Index start_index, std::function<void(const Status a_status)> Lcon);
    virtual void A_Link_Read_ind(std::function<void(const ASAP_Individual asap, const Group_Object_Number group_object_number, const Priority priority, const Link_Start_Index start_index)> ind);
    virtual void A_Link_Read_res(const ASAP_Individual asap, const std::vector<Group_Address> group_address_list, const Group_Object_Number group_object_number, const Priority priority, const Link_Sending_Address sending_address, const Link_Start_Index start_index, std::function<void(const Status a_status)> Rcon);
    virtual void A_Link_Read_Acon(std::function<void(const ASAP_Individual asap, const std::vector<Group_Address> group_address_list, const Group_Object_Number group_object_number, const Priority priority, const Link_Sending_Address sending_address, const Link_Start_Index start_index)> ind);

    virtual void A_Link_Write_req(const ASAP_Individual asap, const Link_Write_Flags flags, const Group_Address group_address, const Group_Object_Number group_object_number, const Priority priority, std::function<void(const Status a_status)> Lcon);
    virtual void A_Link_Write_ind(std::function<void(const ASAP_Individual asap, const Link_Write_Flags flags, const Group_Address group_address, const Group_Object_Number group_object_number, const Priority priority)> ind);

    virtual void A_FunctionPropertyCommand_req(const Ack_Request ack_request, const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, std::function<void(const Status a_status)> Lcon);
    virtual void A_FunctionPropertyCommand_ind(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id)> ind);
    virtual void A_FunctionPropertyCommand_res(const Ack_Request ack_request, const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, std::function<void(const Status a_status)> Rcon);
    virtual void A_FunctionPropertyCommand_Acon(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const std::optional<Property_Return_Code> return_code)> Acon);

    virtual void A_FunctionPropertyState_Read_req(const ASAP_Individual asap, const Ack_Request ack_request, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, std::function<void(const Status a_status)> Lcon);
    virtual void A_FunctionPropertyState_Read_ind(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id)> ind);
    virtual void A_FunctionPropertyState_Read_res(const Ack_Request ack_request, const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, std::function<void(const Status a_status)> Rcon);
    virtual void A_FunctionPropertyState_Read_Acon(std::function<void(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const std::optional<Property_Return_Code> return_code)> Acon);

    /* Services (Connected) */

    virtual void A_ADC_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count, std::function<void(const Status a_status)> Lcon);
    virtual void A_ADC_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count)> ind);
    virtual void A_ADC_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count, const ADC_Sum sum, std::function<void(const Status a_status)> Rcon);
    virtual void A_ADC_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count, const ADC_Sum sum)> Acon);

    virtual void A_Memory_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, std::function<void(const Status a_status)> Lcon);
    virtual void A_Memory_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address)> ind);
    virtual void A_Memory_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon);
    virtual void A_Memory_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data)> Acon);

    virtual void A_Memory_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Lcon);
    virtual void A_Memory_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data)> ind);
    virtual void A_Memory_Write_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon);
    virtual void A_Memory_Write_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data)> Acon);

    virtual void A_MemoryBit_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const MemoryBit_Number number, const MemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data, std::function<void(const Status a_status)> Lcon);
    virtual void A_MemoryBit_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const MemoryBit_Number number, const MemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data)> ind);
    virtual void A_MemoryBit_Write_res(const ASAP_Connected asap, const Priority priority, const MemoryBit_Number number, const MemoryBit_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon);

    virtual void A_UserMemory_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, std::function<void(const Status a_status)> Lcon);
    virtual void A_UserMemory_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address)> ind);
    virtual void A_UserMemory_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon);
    virtual void A_UserMemory_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data)> Acon);

    virtual void A_UserMemory_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Lcon);
    virtual void A_UserMemory_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data)> ind);
    virtual void A_UserMemory_Write_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon);
    virtual void A_UserMemory_Write_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data)> Acon);

    virtual void A_UserMemoryBit_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemoryBit_Number number, const UserMemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data, std::function<void(const Status a_status)> Lcon);
    virtual void A_UserMemoryBit_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemoryBit_Number number, const UserMemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data)> ind);
    virtual void A_UserMemoryBit_Write_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemoryBit_Number number, const UserMemoryBit_Address memory_address, const Memory_Data data, std::function<void(const Status a_status)> Rcon);
    virtual void A_UserMemoryBit_Write_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemoryBit_Number number, const UserMemoryBit_Address memory_address, const Memory_Data data)> Acon);

    virtual void A_UserManufacturerInfo_Read_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, std::function<void(const Status a_status)> Lcon);
    virtual void A_UserManufacturerInfo_Read_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap)> ind);
    virtual void A_UserManufacturerInfo_Read_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const MFact_Info mfact_info, std::function<void(const Status a_status)> Rcon);
    virtual void A_UserManufacturerInfo_Read_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const MFact_Info mfact_info)> Acon);

    virtual void A_Authorize_Request_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Key key, std::function<void(const Status a_status)> Lcon);
    virtual void A_Authorize_Request_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Key key)> ind);
    virtual void A_Authorize_Request_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level, std::function<void(const Status a_status)> Rcon);
    virtual void A_Authorize_Request_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level)> Acon);

    virtual void A_Key_Write_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level, const Key key, std::function<void(const Status a_status)> Lcon);
    virtual void A_Key_Write_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level, const Key key)> ind);
    virtual void A_Key_Write_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level, std::function<void(const Status a_status)> Rcon);
    virtual void A_Key_Write_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level)> Acon);

    virtual void A_Open_Routing_Table_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, std::function<void(const Status a_status)> Lcon);
    virtual void A_Open_Routing_Table_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap)> ind);

    virtual void A_Read_Routing_Table_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, std::function<void(const Status a_status)> Lcon);
    virtual void A_Read_Routing_Table_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address)> ind);
    virtual void A_Read_Routing_Table_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data, std::function<void(const Status a_status)> Rcon);
    virtual void A_Read_Routing_Table_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)> Acon);

    virtual void A_Write_Routing_Table_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data, std::function<void(const Status a_status)> Lcon);
    virtual void A_Write_Routing_Table_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)> ind);

    virtual void A_Read_Router_Memory_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, std::function<void(const Status a_status)> Lcon);
    virtual void A_Read_Router_Memory_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address)> ind);
    virtual void A_Read_Router_Memory_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data, std::function<void(const Status a_status)> Rcon);
    virtual void A_Read_Router_Memory_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)> Acon);

    virtual void A_Write_Router_Memory_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data, std::function<void(const Status a_status)> Lcon);
    virtual void A_Write_Router_Memory_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)> ind);

    virtual void A_Read_Router_Status_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, std::function<void(const Status a_status)> Lcon);
    virtual void A_Read_Router_Status_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap)> ind);
    virtual void A_Read_Router_Status_res(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t router_table_state, std::function<void(const Status a_status)> Rcon);
    virtual void A_Read_Router_Status_Acon(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t router_table_state)> Acon);

    virtual void A_Write_Router_Status_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t router_table_state, std::function<void(const Status a_status)> Lcon);
    virtual void A_Write_Router_Status_ind(std::function<void(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t router_table_state)> ind);

    virtual void A_ServiceInformation_Indication_req(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const uint24_t info, std::function<void(const Status a_status)> Lcon);
    virtual void A_ServiceInformation_Indication_ind(std::function<void(const ASAP_Individual asap, const Priority priority, const Hop_Count_Type hop_count_type, const uint24_t info)> ind);

    /* Parameters */

    std::shared_ptr<Group_Object_Association_Table> group_object_association_table{};

protected:
    asio::io_context & io_context;

    /* T_Data_Group service */
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu)> T_Data_Group_req;
    virtual void T_Data_Group_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void T_Data_Group_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu);
    Message_Bus<const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const TSAP_Group /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status /*t_status*/> T_Data_Group_con_bus;
    Message_Bus<const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const TSAP_Group /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/> T_Data_Group_ind_bus;

    /* T_Data_Tag_Group service */
    std::function<void(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu)> T_Data_Tag_Group_req;
    virtual void T_Data_Tag_Group_con(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void T_Data_Tag_Group_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, std::shared_ptr<TSDU> tsdu);
    Message_Bus<const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const Group_Address /*destination_address*/, const Extended_Frame_Format /*extended_frame_format*/, std::shared_ptr<TSDU> /*tsdu*/, const Status /*t_status*/> T_Data_Tag_Group_con_bus;
    Message_Bus<const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, const Group_Address /*destination_address*/, const Extended_Frame_Format /*extended_frame_format*/, std::shared_ptr<TSDU> /*tsdu*/> T_Data_Tag_Group_ind_bus;

    /* T_Data_Broadcast service */
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu)> T_Data_Broadcast_req;
    virtual void T_Data_Broadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void T_Data_Broadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu);
    Message_Bus<const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status /*t_status*/> T_Data_Broadcast_con_bus;
    Message_Bus<const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, std::shared_ptr<TSDU> /*tsdu*/> T_Data_Broadcast_ind_bus;

    /* T_Data_SystemBroadcast service */
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu)> T_Data_SystemBroadcast_req;
    virtual void T_Data_SystemBroadcast_con(const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void T_Data_SystemBroadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu);
    Message_Bus<const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, std::shared_ptr<TSDU> /*tsdu*/, const Status /*t_status*/> T_Data_SystemBroadcast_con_bus;
    Message_Bus<const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Individual_Address /*source_address*/, std::shared_ptr<TSDU> /*tsdu*/> T_Data_SystemBroadcast_ind_bus;

    /* T_Data_Individual service */
    std::function<void(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu)> T_Data_Individual_req;
    virtual void T_Data_Individual_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void T_Data_Individual_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu);
    Message_Bus<const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status /*t_status*/> T_Data_Individual_con_bus;
    Message_Bus<const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/> T_Data_Individual_ind_bus;

    /* T_Connect service */
    std::function<void(const Individual_Address destination_address, const Priority priority)> T_Connect_req;
    virtual void T_Connect_con(const Individual_Address destination_address, const TSAP_Connected tsap, const Status t_status);
    virtual void T_Connect_ind(const TSAP_Connected tsap);
    Message_Bus<const Individual_Address /*destination_address*/, const TSAP_Connected /*tsap*/, const Status /*t_status*/> T_Connect_con_bus;
    Message_Bus<const TSAP_Connected /*tsap*/> T_Connect_ind_bus;

    /* T_Disconnect service */
    std::function<void(const Priority priority, const TSAP_Connected tsap)> T_Disconnect_req;
    virtual void T_Disconnect_con(const Priority priority, const TSAP_Connected tsap, const Status t_status);
    virtual void T_Disconnect_ind(const TSAP_Connected tsap);
    Message_Bus<const Priority /*priority*/, const TSAP_Connected /*tsap*/, const Status /*t_status*/> T_Disconnect_con_bus;
    Message_Bus<const TSAP_Connected /*tsap*/> T_Disconnect_ind_bus;

    /* T_Data_Connected service */
    std::function<void(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)> T_Data_Connected_req;
    virtual void T_Data_Connected_con(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu);
    virtual void T_Data_Connected_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu);
    Message_Bus<const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/> T_Data_Connected_con_bus;
    Message_Bus<const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/> T_Data_Connected_ind_bus;
};

}
