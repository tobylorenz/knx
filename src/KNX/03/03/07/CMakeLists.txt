# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/ADC_Channel_Nr.h
        ${CMAKE_CURRENT_SOURCE_DIR}/ADC_Read_Count.h
        ${CMAKE_CURRENT_SOURCE_DIR}/ADC_Sum.h
        ${CMAKE_CURRENT_SOURCE_DIR}/APDU.h
        ${CMAKE_CURRENT_SOURCE_DIR}/ASAP.h
        ${CMAKE_CURRENT_SOURCE_DIR}/ASDU.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Access.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Application_Layer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Comm_Mode.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Descriptor_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Device_Descriptor_Data.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Domain_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Domain_Address_2.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Domain_Address_6.h
        ${CMAKE_CURRENT_SOURCE_DIR}/File_Block.h
        ${CMAKE_CURRENT_SOURCE_DIR}/File_Block_Sequence_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/File_Handle.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Group_Object_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Group_Value.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Key.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Level.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Sending_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Start_Index.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Write_Flags.h
        ${CMAKE_CURRENT_SOURCE_DIR}/MFact_Info.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Max_Nr_Of_Elem.h
        ${CMAKE_CURRENT_SOURCE_DIR}/MemoryBit_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/MemoryBit_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Memory_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Memory_Data.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Memory_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Object_Index.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Object_Instance.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Object_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Test_Info.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Test_Info_Result.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Test_Result.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Value.h
        ${CMAKE_CURRENT_SOURCE_DIR}/PropertyValue_Nr_Of_Elem.h
        ${CMAKE_CURRENT_SOURCE_DIR}/PropertyValue_Start_Index.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Id.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Index.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Return_Code.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Value.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Channel_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Erase_Code.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Error_Code.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Process_Time.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Serial_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/System_Parameter_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/System_Property_Id.h
        ${CMAKE_CURRENT_SOURCE_DIR}/UserMemoryBit_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/UserMemoryBit_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/UserMemory_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/UserMemory_Number.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/APDU.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/ASAP.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Application_Layer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Domain_Address.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Domain_Address_2.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Domain_Address_6.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Write_Flags.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Type.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Serial_Number.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/System_Parameter_Type.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/ADC_Channel_Nr.h
        ${CMAKE_CURRENT_SOURCE_DIR}/ADC_Read_Count.h
        ${CMAKE_CURRENT_SOURCE_DIR}/ADC_Sum.h
        ${CMAKE_CURRENT_SOURCE_DIR}/APDU.h
        ${CMAKE_CURRENT_SOURCE_DIR}/ASAP.h
        ${CMAKE_CURRENT_SOURCE_DIR}/ASDU.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Access.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Application_Layer.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Comm_Mode.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Descriptor_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Device_Descriptor_Data.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Domain_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Domain_Address_2.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Domain_Address_6.h
        ${CMAKE_CURRENT_SOURCE_DIR}/File_Block.h
        ${CMAKE_CURRENT_SOURCE_DIR}/File_Block_Sequence_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/File_Handle.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Group_Object_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Group_Value.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Key.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Level.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Sending_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Start_Index.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Write_Flags.h
        ${CMAKE_CURRENT_SOURCE_DIR}/MFact_Info.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Max_Nr_Of_Elem.h
        ${CMAKE_CURRENT_SOURCE_DIR}/MemoryBit_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/MemoryBit_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Memory_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Memory_Data.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Memory_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Object_Index.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Object_Instance.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Object_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Test_Info.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Test_Info_Result.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Test_Result.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Parameter_Value.h
        ${CMAKE_CURRENT_SOURCE_DIR}/PropertyValue_Nr_Of_Elem.h
        ${CMAKE_CURRENT_SOURCE_DIR}/PropertyValue_Start_Index.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Id.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Index.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Return_Code.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Property_Value.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Channel_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Erase_Code.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Error_Code.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Process_Time.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Restart_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Serial_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/System_Parameter_Type.h
        ${CMAKE_CURRENT_SOURCE_DIR}/System_Property_Id.h
        ${CMAKE_CURRENT_SOURCE_DIR}/UserMemoryBit_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/UserMemoryBit_Number.h
        ${CMAKE_CURRENT_SOURCE_DIR}/UserMemory_Address.h
        ${CMAKE_CURRENT_SOURCE_DIR}/UserMemory_Number.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/03/07)

# sub directories
add_subdirectory(A_ADC_Read)
add_subdirectory(A_ADC_Response)
add_subdirectory(A_Authorize_Request)
add_subdirectory(A_Authorize_Response)
add_subdirectory(A_DeviceDescriptor_InfoReport)
add_subdirectory(A_DeviceDescriptor_Read)
add_subdirectory(A_DeviceDescriptor_Response)
add_subdirectory(A_DomainAddressSelective_Read)
add_subdirectory(A_DomainAddressSerialNumber_Read)
add_subdirectory(A_DomainAddressSerialNumber_Response)
add_subdirectory(A_DomainAddressSerialNumber_Write)
add_subdirectory(A_DomainAddress_Read)
add_subdirectory(A_DomainAddress_Response)
add_subdirectory(A_DomainAddress_Write)
add_subdirectory(A_FileStream_InfoReport)
add_subdirectory(A_FunctionPropertyCommand)
add_subdirectory(A_FunctionPropertyState_Read)
add_subdirectory(A_FunctionPropertyState_Response)
add_subdirectory(A_GroupPropValue_InfoReport)
add_subdirectory(A_GroupPropValue_Read)
add_subdirectory(A_GroupPropValue_Response)
add_subdirectory(A_GroupPropValue_Write)
add_subdirectory(A_GroupValue_Read)
add_subdirectory(A_GroupValue_Response)
add_subdirectory(A_GroupValue_Write)
add_subdirectory(A_IndividualAddressSerialNumber_Read)
add_subdirectory(A_IndividualAddressSerialNumber_Response)
add_subdirectory(A_IndividualAddressSerialNumber_Write)
add_subdirectory(A_IndividualAddress_Read)
add_subdirectory(A_IndividualAddress_Response)
add_subdirectory(A_IndividualAddress_Write)
add_subdirectory(A_Key_Response)
add_subdirectory(A_Key_Write)
add_subdirectory(A_Link_Read)
add_subdirectory(A_Link_Response)
add_subdirectory(A_Link_Write)
add_subdirectory(A_MemoryBit_Write)
add_subdirectory(A_Memory_Read)
add_subdirectory(A_Memory_Response)
add_subdirectory(A_Memory_Write)
add_subdirectory(A_NetworkParameter_InfoReport)
add_subdirectory(A_NetworkParameter_Read)
add_subdirectory(A_NetworkParameter_Response)
add_subdirectory(A_NetworkParameter_Write)
add_subdirectory(A_Open_Routing_Table_Req)
add_subdirectory(A_PropertyDescription_Read)
add_subdirectory(A_PropertyDescription_Response)
add_subdirectory(A_PropertyValue_Read)
add_subdirectory(A_PropertyValue_Response)
add_subdirectory(A_PropertyValue_Write)
add_subdirectory(A_Read_Router_Memory_Req)
add_subdirectory(A_Read_Router_Memory_Res)
add_subdirectory(A_Read_Router_Status_Req)
add_subdirectory(A_Read_Router_Status_Res)
add_subdirectory(A_Read_Routing_Table_Req)
add_subdirectory(A_Read_Routing_Table_Res)
add_subdirectory(A_Restart)
add_subdirectory(A_Restart_Response)
add_subdirectory(A_ServiceInformation_Indication)
add_subdirectory(A_SystemNetworkParameter_Read)
add_subdirectory(A_SystemNetworkParameter_Response)
add_subdirectory(A_SystemNetworkParameter_Write)
add_subdirectory(A_UserManufacturerInfo_Read)
add_subdirectory(A_UserManufacturerInfo_Response)
add_subdirectory(A_UserMemoryBit_Write)
add_subdirectory(A_UserMemory_Read)
add_subdirectory(A_UserMemory_Response)
add_subdirectory(A_UserMemory_Write)
add_subdirectory(A_Write_Router_Memory_Req)
add_subdirectory(A_Write_Router_Status_Req)
add_subdirectory(A_Write_Routing_Table_Req)
add_subdirectory(A_Unknown)
