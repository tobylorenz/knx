// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

namespace KNX {

/** Communcation Mode */
enum class Comm_Mode {
    /** point-to-multi-point, connection-less (multicast) */
    Group,

    /** point-to-all-points, connection-less (broadcast) */
    Broadcast,

    /** point-to-all-points, connection-less (system broadcast) */
    SystemBroadcast,

    /** point-to-point connection-less */
    Individual,

    /** point-to-point connection-oriented */
    Connected
};

}
