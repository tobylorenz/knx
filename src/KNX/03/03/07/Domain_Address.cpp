// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/Domain_Address.h>

namespace KNX {

Domain_Address::Domain_Address(const std::vector<uint8_t> domain_address) :
    m_domain_address(domain_address)
{
}

Domain_Address::~Domain_Address()
{
}

Domain_Address & Domain_Address::operator=(const std::vector<uint8_t> domain_address)
{
    m_domain_address = domain_address;

    return *this;
}

Domain_Address::operator std::vector<uint8_t>() const
{
    return m_domain_address;
}

void Domain_Address::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    m_domain_address.assign(first, last);
}

std::vector<uint8_t> Domain_Address::toData() const
{
    return m_domain_address;
}

size_t Domain_Address::size() const
{
    return m_domain_address.size();
}

}
