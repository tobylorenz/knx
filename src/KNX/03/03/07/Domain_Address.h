// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/types.h>

namespace KNX {

/**
 * Domain Address (DoA)
 *
 * @ingroup KNX_03_03_02_01_04_01
 * @ingroup KNX_03_05_01_03_02
 * @ingroup KNX_03_05_02_02_12_01
 */
class KNX_EXPORT Domain_Address
{
public:
    Domain_Address() = default;
    explicit Domain_Address(const std::vector<uint8_t> domain_address);
    virtual ~Domain_Address();
    Domain_Address & operator=(const std::vector<uint8_t> domain_address);
    operator std::vector<uint8_t>() const;
    bool operator==(const Domain_Address &) const = default;
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;
    virtual size_t size() const;

protected:
    /** domain address */
    std::vector<uint8_t> m_domain_address;
};

}
