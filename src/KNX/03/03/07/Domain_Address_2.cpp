// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/Domain_Address_2.h>

#include <iomanip>
#include <sstream>

namespace KNX {

Domain_Address_2::Domain_Address_2() :
    Domain_Address()
{
    m_domain_address.push_back(0);
    m_domain_address.push_back(0);

    assert(m_domain_address.size() == 2);
}

Domain_Address_2::Domain_Address_2(const uint16_t domain_address) :
    Domain_Address()
{
    m_domain_address.push_back(domain_address >> 8);
    m_domain_address.push_back(domain_address & 0xff);

    assert(m_domain_address.size() == 2);
}

Domain_Address_2 & Domain_Address_2::operator=(const std::vector<uint8_t> domain_address)
{
    assert(domain_address.size() == 2);

    m_domain_address = domain_address;

    return *this;
}

Domain_Address_2 & Domain_Address_2::operator=(const uint16_t domain_address)
{
    m_domain_address.clear();
    m_domain_address.push_back(domain_address >> 8);
    m_domain_address.push_back(domain_address & 0xff);

    return *this;
}

Domain_Address_2::operator uint16_t() const
{
    assert(m_domain_address.size() == 2);

    return (m_domain_address[0] << 8) | m_domain_address[1];
}

void Domain_Address_2::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    Domain_Address::fromData(first, last);
}

std::string Domain_Address_2::text() const
{
    assert(m_domain_address.size() == 2);

    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex
        << static_cast<uint16_t>(m_domain_address[0])
        << static_cast<uint16_t>(m_domain_address[1]);

    return oss.str();
}

}
