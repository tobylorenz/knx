// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <string>

#include <KNX/03/03/07/Domain_Address.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Domain Address (DoA with 2 octets) - KNX PL110
 *
 * @ingroup KNX_03_02_03_04_02_02
 * @ingroup KNX_03_05_01_03_02_03
 * @ingroup KNX_03_05_01_04_03_21
 * @ingroup KNX_03_05_02_02_12_01_01
 *
 * @note The first octet (high) is always 0x00.
 */
class KNX_EXPORT Domain_Address_2 : public Domain_Address
{
public:
    Domain_Address_2();
    explicit Domain_Address_2(const uint16_t domain_address);
    Domain_Address_2 & operator=(const std::vector<uint8_t> domain_address);
    Domain_Address_2 & operator=(const uint16_t domain_address);
    operator uint16_t() const;
    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    virtual std::string text() const;
};

}
