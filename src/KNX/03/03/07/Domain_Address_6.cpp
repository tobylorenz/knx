// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/Domain_Address_6.h>

#include <iomanip>
#include <sstream>

namespace KNX {

Domain_Address_6::Domain_Address_6() :
    Domain_Address()
{
    m_domain_address.push_back(0);
    m_domain_address.push_back(0);
    m_domain_address.push_back(0);
    m_domain_address.push_back(0);
    m_domain_address.push_back(0);
    m_domain_address.push_back(0);

    assert(m_domain_address.size() == 6);
}

Domain_Address_6 & Domain_Address_6::operator=(const std::vector<uint8_t> domain_address)
{
    assert(domain_address.size() == 6);

    m_domain_address = domain_address;

    return *this;
}

void Domain_Address_6::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    Domain_Address::fromData(first, last);
}

std::string Domain_Address_6::text() const
{
    assert(m_domain_address.size() == 6);

    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex
        << static_cast<uint16_t>(m_domain_address[0])
        << static_cast<uint16_t>(m_domain_address[1])
        << static_cast<uint16_t>(m_domain_address[2])
        << static_cast<uint16_t>(m_domain_address[3])
        << static_cast<uint16_t>(m_domain_address[4])
        << static_cast<uint16_t>(m_domain_address[5]);

    return oss.str();
}

}
