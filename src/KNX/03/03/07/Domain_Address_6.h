// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <string>

#include <KNX/03/03/07/Domain_Address.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Domain Address (DoA with 6 octets) - KNX RF
 *
 * @ingroup KNX_03_02_05_06_01_01_04
 * @ingroup KNX_03_05_01_03_02_04
 * @ingroup KNX_03_05_02_02_12_01_02
 */
class KNX_EXPORT Domain_Address_6 : public Domain_Address
{
public:
    Domain_Address_6();
    Domain_Address_6 & operator=(const std::vector<uint8_t> domain_address);
    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    virtual std::string text() const;
};

}
