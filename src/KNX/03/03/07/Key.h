// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/types.h>

namespace KNX {

/** Key */
using Key = std::array<uint8_t, 4>;

const Key free_access_key{0xFF, 0xFF, 0xFF, 0xFF};

}
