// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/Link_Write_Flags.h>

namespace KNX {

Link_Write_Flags::Link_Write_Flags(const uint8_t flags) :
    d(static_cast<D>((flags >> 1) & 0x01)),
    s(static_cast<S>((flags >> 0) & 0x01))
{
}

Link_Write_Flags & Link_Write_Flags::operator=(const uint8_t & flags)
{
    d = static_cast<D>((flags >> 1) & 0x01);
    s = static_cast<S>((flags >> 0) & 0x01);

    return *this;
}

Link_Write_Flags::operator uint8_t() const
{
    return
        (static_cast<uint8_t>(d) << 1) |
        (static_cast<uint8_t>(s) << 0);
}

bool Link_Write_Flags::operator==(const Link_Write_Flags & other) const
{
    return (d == other.d) && (s == other.s);
}

}
