// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Link Write Flags
 *
 * @ingroup KNX_03_03_07_03_04_04_02
 */
class KNX_EXPORT Link_Write_Flags
{
public:
    Link_Write_Flags() = default;
    explicit Link_Write_Flags(const uint8_t flags);
    Link_Write_Flags & operator=(const uint8_t & flags);
    operator uint8_t() const;
    bool operator==(const Link_Write_Flags & other) const;

    /** d flag */
    enum class D : bool {
        /** add */
        add = 0,

        /** delete */
        delete_ = 1,
    };

    D d{D::add};

    /** s flag */
    enum class S : bool {
        /** not sending */
        not_sending = 0,

        /** sending */
        sending = 1
    };

    S s{S::not_sending};
};

}
