// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/** Manufacturer Info */
struct KNX_EXPORT MFact_Info {
    bool operator==(const MFact_Info & other) const = default;

    /** Manufacturer ID */
    uint8_t manufacturer_id{};

    /** Manufacturer Specific */
    uint16_t manufacturer_specific{};
};

}
