// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * (Interface) Object Instance
 *
 * 0 = reserved (not used); 1 = 1st instance; 2 = 2nd instance; 3 = 3rd instance
 *
 * @ingroup KNX_03_06_01_04_01_07
 */
using Object_Instance = uint8_t;

}
