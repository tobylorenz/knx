// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * (Interface) Object Type
 *
 * @ingroup KNX_03_05_03_02_09_02_02_01
 */
using Object_Type = uint16_t;

}
