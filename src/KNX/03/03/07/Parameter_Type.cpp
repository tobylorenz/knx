// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/Parameter_Type.h>

namespace KNX {

bool operator==(const Parameter_Type & a, const Parameter_Type & b)
{
    return (a.object_type == b.object_type) && (a.pid == b.pid);
}

}
