// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Object_Type.h>
#include <KNX/03/03/07/Property_Id.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/** Parameter Type */
struct KNX_EXPORT Parameter_Type {
    /** Interface Object Type */
    Object_Type object_type{};

    /** Property Identifier */
    Property_Id pid{};
};

KNX_EXPORT bool operator==(const Parameter_Type & a, const Parameter_Type & b);

}
