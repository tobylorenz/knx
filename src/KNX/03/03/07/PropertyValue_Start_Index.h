// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Property Value Start Index
 *
 * @ingroup KNX_03_03_07_03_04_03
 */
using PropertyValue_Start_Index = uint12_t;

}
