// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Property Identifier
 *
 * @ingroup KNX_03_03_07_03_04_03
 * @ingroup KNX_03_03_07_03_04_05
 * @ingroup KNX_03_04_01_04_03_01_02
 */
using Property_Id = uint8_t;

}
