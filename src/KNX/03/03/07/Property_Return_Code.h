// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Property Return Code
 *
 * - 0: No Error
 * - 0xFF: Error
 */
using Property_Return_Code = uint8_t;

}
