// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <cstdint>
#include <vector>

#include <KNX/knx_export.h>

namespace KNX {

/**
 * Property Value
 *
 * @ingroup KNX_03_04_01_04_03_01_03
 */
using Property_Value = std::vector<uint8_t>;

}
