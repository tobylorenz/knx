// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Restart Channel Number
 *
 * @ingroup KNX_03_05_02_03_07_01_02
 */
using Restart_Channel_Number = uint8_t;

}
