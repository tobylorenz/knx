// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Restart Erase Code
 *
 * @ingroup KNX_03_05_02_03_07_01_02
 */
enum class Restart_Erase_Code : uint8_t {
    // 0x00: reserved

    /** Confirmed Restart */
    Confirmed_Restart = 0x01,

    /** Factory Reset */
    Factory_Reset = 0x02,

    /** ResetIA */
    ResetIA = 0x03,

    /** ResetAP */
    ResetAP = 0x04,

    /** ResetParam */
    ResetParam = 0x05,

    /** ResetLinks */
    ResetLinks = 0x06,

    /** Factory Reset without IA */
    Factory_Reset_without_IA = 0x07,

    // 0x08 .. 0xFF: reserved
};

}
