// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Restart Error Code
 *
 * @ingroup KNX_03_05_02_03_07_01_02
 */
enum class Restart_Error_Code : uint8_t {
    /** No Error */
    No_Error = 0x00,

    /** Access denied */
    Access_denied = 0x01,

    /** Unsupported Erase Code */
    Unsupported_Erase_Code = 0x02,

    /** Invalid Channel Number */
    Invalid_Channel_Number = 0x03,

    // 0x04 .. 0xFF: reserved
};

}
