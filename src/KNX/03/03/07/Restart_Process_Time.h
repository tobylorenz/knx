// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Restart Process Time
 *
 * @ingroup @ingroup KNX_03_05_02_03_07_01_02
 */
using Restart_Process_Time = uint16_t;

}
