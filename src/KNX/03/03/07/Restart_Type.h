// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Restart Type
 *
 * @ingroup KNX_03_05_02_03_07_01
 */
using Restart_Type = uint6_t;
enum : uint8_t {
    /** Basic Restart */
    Basic_Restart = 0,

    /** Master Restart */
    Master_Restart = 1,
};

}
