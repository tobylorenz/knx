// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/Serial_Number.h>

#include <iomanip>
#include <sstream>

namespace KNX {

Serial_Number::Serial_Number(const std::array<uint8_t, 6> & serial_number) :
    serial_number(serial_number)
{
}

Serial_Number::Serial_Number(const std::string & serial_number)
{
    std::istringstream iss(serial_number);
    std::string str;
    int i = 0;
    while(getline(iss, str, ':') && (i < 6)) {
        this->serial_number[i++] = std::stoul(str, nullptr, 16);
    }
}

Serial_Number & Serial_Number::operator=(const std::array<uint8_t, 6> & serial_number)
{
    this->serial_number = serial_number;
    return *this;
}

Serial_Number::operator std::array<uint8_t, 6>() const
{
    return serial_number;
}

std::string Serial_Number::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex
        << static_cast<uint16_t>(serial_number[0])
        << static_cast<uint16_t>(serial_number[1])
        << static_cast<uint16_t>(serial_number[2])
        << static_cast<uint16_t>(serial_number[3])
        << static_cast<uint16_t>(serial_number[4])
        << static_cast<uint16_t>(serial_number[5]);

    return oss.str();
}

bool operator==(const Serial_Number & a, const Serial_Number & b)
{
    return a.serial_number == b.serial_number;
}

}
