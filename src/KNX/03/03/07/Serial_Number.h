// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>
#include <string>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Serial Number
 *
 * @ingroup KNX_03_05_01_04_16
 */
struct KNX_EXPORT Serial_Number
{
    Serial_Number() = default;
    explicit Serial_Number(const std::array<uint8_t, 6> & serial_number);
    explicit Serial_Number(const std::string & serial_number);

    Serial_Number & operator=(const std::array<uint8_t, 6> & serial_number);
    virtual operator std::array<uint8_t, 6>() const;

    virtual std::string text() const;

    /** Serial Number */
    std::array<uint8_t, 6> serial_number{};
};

KNX_EXPORT bool operator==(const Serial_Number & a, const Serial_Number & b);

}
