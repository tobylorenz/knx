// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/03/07/System_Parameter_Type.h>

namespace KNX {

bool operator==(const System_Parameter_Type & a, const System_Parameter_Type & b)
{
    return (a.object_type == b.object_type) && (a.pid == b.pid);
}

}
