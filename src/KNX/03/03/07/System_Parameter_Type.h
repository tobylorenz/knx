// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Object_Type.h>
#include <KNX/03/03/07/System_Property_Id.h>
#include <KNX/knx_export.h>

namespace KNX {

/** System Parameter Type */
struct KNX_EXPORT System_Parameter_Type {
    /** Interface Object Type */
    Object_Type object_type{};

    /** Property Identifier */
    System_Property_Id pid{};

};

KNX_EXPORT bool operator==(const System_Parameter_Type & a, const System_Parameter_Type & b);

}
