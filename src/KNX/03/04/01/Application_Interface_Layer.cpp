// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/04/01/Application_Interface_Layer.h>

namespace KNX {

Application_Interface_Layer::Application_Interface_Layer(Application_Layer & application_layer) :
    group_object_server(application_layer),
    interface_object_server(application_layer),
    file_server(application_layer)
{
}

}
