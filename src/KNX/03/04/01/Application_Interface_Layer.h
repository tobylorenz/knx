// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/04/01/File_Server.h>
#include <KNX/03/04/01/Group_Object_Server.h>
#include <KNX/03/04/01/Interface_Object_Server.h>
#include <KNX/knx_export.h>

namespace KNX {

/** Application Interface Layer */
class Application_Interface_Layer
{
public:
    explicit Application_Interface_Layer(Application_Layer & application_layer);

    /** Group Object Server */
    Group_Object_Server group_object_server;

    /** Interface Object Server */
    Interface_Object_Server interface_object_server;

    /** File Server */
    File_Server file_server;
};

}
