// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/04/01/Data_Property.h>

#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Data_Property::Data_Property(const Property_Id property_id) :
    Property(property_id)
{
}

void Data_Property::write(const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data, std::optional<CEMI_Error_Code> & error_code)
{
    const uint8_t property_datatype_size = size(description.property_datatype);

    std::vector<uint8_t> new_data = toData();

    const PropertyValue_Nr_Of_Elem new_nr_of_elem = (start_index - 1 + nr_of_elem);
    if (this->nr_of_elem < new_nr_of_elem) {
        new_data.resize(new_nr_of_elem * property_datatype_size);
        this->nr_of_elem = new_nr_of_elem;
    }

    std::copy(std::cbegin(data), std::cend(data), std::begin(new_data) + (start_index - 1) * property_datatype_size);

    fromData(std::cbegin(new_data), std::cend(new_data));

    if (new_data == toData()) {
        error_code = std::nullopt;
    } else {
        error_code = CEMI_Error_Code::Unspecified_Error;
    }
}

}
