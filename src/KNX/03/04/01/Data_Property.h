// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <optional>

#include <KNX/03/03/07/Property_Value.h>
#include <KNX/03/04/01/Property.h>
#include <KNX/03/06/03/cEMI/Management/CEMI_Error_Codes.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Data Property
 *
 * This is accessed using
 * - A_PropertyValue_Read service
 * - A_PropertyValue_Write service
 * - A_PropertyDescription_Read service
 *
 * @ingroup KNX_03_04_01_04_04_01
 */
class KNX_EXPORT Data_Property :
    public Property
{
public:
    explicit Data_Property(const Property_Id property_id);

    /* Property value */
    // ... is in derived classes and accessed via fromData/toData

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) = 0;
    virtual std::vector<uint8_t> toData() const = 0;
    virtual std::string text() const = 0;

    /**
     * Gets executed on M_PropWrite
     *
     * @param[in] nr_of_elem number of elements
     * @param[in] start_index start index
     * @param[in] data Data
     * @param[out] error_code Error information
     *
     * This method provides a default implementation.
     * It can be overriden to check for, e.g. value out of range errors.
     */
    virtual void write(const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data, std::optional<CEMI_Error_Code> & error_code);
};

}
