// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/04/01/File_Server.h>

namespace KNX {

File_Server::File_Server(Application_Layer & application_layer) :
    file_server_object(std::make_shared<File_Server_Object>()),
    application_layer(application_layer)
{
    file_server_object->file_command()->command = std::bind(&File_Server::file_command, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);

    A_FileStream_InfoReport_ind_initiator();
}

File_Server::~File_Server()
{
}

void File_Server::A_FileStream_InfoReport_ind_initiator()
{
    application_layer.A_FileStream_InfoReport_ind([this](const ASAP_Individual /*asap*/, const File_Block /*file_block*/, const File_Block_Sequence_Number /*file_block_sequence_number*/, const File_Handle /*file_handle*/, const Hop_Count_Type /*hop_count_type*/){
        // @todo A_FileStream_InfoReport_ind

        A_FileStream_InfoReport_ind_initiator();
    });
}

void File_Server::file_command(const Property_Value input_data, std::optional<Property_Return_Code> & return_code, Property_Value & output_data)
{
    std::vector<uint8_t>::const_iterator first = std::cbegin(input_data);
    std::vector<uint8_t>::const_iterator last = std::cend(input_data);

    // @todo File Server command interpreter incomplete. Connection to data transfer processor missing.

    File_Command_Property::Command command = static_cast<File_Command_Property::Command>(*first++);

    switch (command) {
    case File_Command_Property::Command::Get_File_Handle: {
        assert(first == last);

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
        const File_Handle file_handle{};
        output_data.push_back(file_handle);
    }
    break;
    case File_Command_Property::Command::Retrieve_File: {
        std::string file_path{};
        if (first != last) {
            file_path.assign(first, last);
        } else {
            file_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    case File_Command_Property::Command::Store_File: {
        std::string file_path{};
        if (first != last) {
            file_path.assign(first, last);
        } else {
            file_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    case File_Command_Property::Command::List_Directory: {
        std::string directory_path{};
        if (first != last) {
            directory_path.assign(first, last);
        } else {
            directory_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    case File_Command_Property::Command::Rename_From: {
        std::string file_path{};
        if (first != last) {
            file_path.assign(first, last);
        } else {
            file_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    case File_Command_Property::Command::Rename_To: {
        std::string file_path{};
        if (first != last) {
            file_path.assign(first, last);
        } else {
            file_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    case File_Command_Property::Command::Delete: {
        std::string file_path{};
        if (first != last) {
            file_path.assign(first, last);
        } else {
            file_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    case File_Command_Property::Command::Remove_Directory: {
        std::string directory_path{};
        if (first != last) {
            directory_path.assign(first, last);
        } else {
            directory_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    case File_Command_Property::Command::Make_Directory: {
        std::string directory_path{};
        if (first != last) {
            directory_path.assign(first, last);
        } else {
            directory_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    case File_Command_Property::Command::Get_File_Size: {
        std::string file_path{};
        if (first != last) {
            file_path.assign(first, last);
        } else {
            file_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
        const File_Command_Property::File_Size file_size{};
        output_data.push_back((file_size >> 24) & 0xff);
        output_data.push_back((file_size >> 16) & 0xff);
        output_data.push_back((file_size >> 8) & 0xff);
        output_data.push_back(file_size & 0xff);
    }
    break;
    case File_Command_Property::Command::Get_Empty_Disk_Space: {
        assert(first == last);

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
        const File_Command_Property::Disk_Space empty_disk_Space{};
        output_data.push_back((empty_disk_Space >> 24) & 0xff);
        output_data.push_back((empty_disk_Space >> 16) & 0xff);
        output_data.push_back((empty_disk_Space >> 8) & 0xff);
        output_data.push_back(empty_disk_Space & 0xff);
    }
    break;
    case File_Command_Property::Command::Abort: {
        assert(first == last);

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    case File_Command_Property::Command::Get_File: {
        std::string file_path{};
        if (first != last) {
            file_path.assign(first, last);
        } else {
            file_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
        const File_Command_Property::Media_Type media_type{};
        const File_Command_Property::Media_Subtype subtype{};
        output_data.push_back(media_type);
        output_data.push_back(subtype);
    }
    break;
    case File_Command_Property::Command::Post_File: {
        std::string file_path{};
        if (first != last) {
            file_path.assign(first, last);
        } else {
            file_path = file_server_object->file_path()->file_path;
        }

        return_code = static_cast<uint8_t>(File_Command_Property::Return_Code::Command_successful);
    }
    break;
    }
}

}
