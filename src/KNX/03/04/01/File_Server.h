// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <filesystem>

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/01/13_File_Server/File_Server_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * File Server
 *
 * @ingroup KNX_03_04_01_05
 */
class KNX_EXPORT File_Server
{
public:
    explicit File_Server(Application_Layer & application_layer);
    virtual ~File_Server();

    /** File Server Object */
    std::shared_ptr<File_Server_Object> file_server_object{};

    /** File Server data transfer process */
    // @todo File Server data transfer process

    /** File System */
    std::filesystem::path filesystem_base{};

protected:
    Application_Layer & application_layer;

    void A_FileStream_InfoReport_ind_initiator();

    /**
     * File Server command interpreter
     *
     * @ingroup KNX_03_05_01_04_08_03
     */
    virtual void file_command(const Property_Value input_data, std::optional<Property_Return_Code> & return_code, Property_Value & output_data);
};

}
