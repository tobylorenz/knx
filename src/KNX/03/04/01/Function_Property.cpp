// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/04/01/Function_Property.h>

#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Function_Property::Function_Property(const Property_Id property_id) :
    Property(property_id)
{
    description.property_datatype = PDT_FUNCTION;
}

}
