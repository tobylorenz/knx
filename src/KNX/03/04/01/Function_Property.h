// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>
#include <optional>

#include <KNX/03/03/07/Property_Value.h>
#include <KNX/03/03/07/Property_Return_Code.h>
#include <KNX/03/04/01/Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Function Property
 *
 * This is accessed using
 * - A_FunctionPropertyCommand service
 * - A_FunctionPropertyState_Read service
 * - M_FuncPropCommand service
 * - M_FuncPropStateRead service
 *
 * Identified by description.property_datatype = PDT_FUNCTION
 *
 * @ingroup KNX_03_04_01_04_04_02
 */
class Function_Property :
    public Property
{
public:
    explicit Function_Property(const Property_Id property_id);

    /**
     * Gets executed on A_FunctionPropertyCommand or M_FuncPropCommand
     *
     * @param[in] input_data input data
     * @param[out] return_code return code
     * @param[out] output_data output data
     */
    std::function<void(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)> command{};

    /**
     * Gets executed on A_FunctionPropertyState_Read or M_FuncPropStateRead
     *
     * @param[in] input_data input data
     * @param[out] return_code return code
     * @param[out] output_data output data
     */
    std::function<void(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)> state_read{};
};

}
