// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/04/01/Group_Object_Server.h>

#include <cassert>

namespace KNX {

Group_Object_Server::Group_Object_Server(Application_Layer & application_layer) :
    application_layer(application_layer)
{
    group_object_association_table = application_layer.group_object_association_table;

    A_GroupValue_Read_ind_initiator();
    A_GroupValue_Read_Acon_initiator();
    A_GroupValue_Write_ind_initiator();
}

Group_Object_Server::~Group_Object_Server()
{
}

void Group_Object_Server::U_Value_Read_req(const Group_Object_Number group_object_number)
{
    assert(group_object_table);

    const ASAP_Group asap = group_object_table->asap(group_object_number);
    Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
    Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];
    // Group_Object_Config & config_flags = descriptor.config;

    /* read and use updated Group Object Value */
    if (U_Value_Read_con) {
        U_Value_Read_con(group_object_number, communication_flags, *descriptor.data);

        /* reset communication flags */
        communication_flags.update = false;
    }

    /* process requests */
    process_requests();
}

void Group_Object_Server::U_Flags_Read_req(const Group_Object_Number group_object_number)
{
    assert(group_object_table);

    const ASAP_Group asap = group_object_table->asap(group_object_number);
    Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
    Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];
    Group_Object_Config & config_flags = descriptor.config;

    if (U_Flags_Read_con) {
        U_Flags_Read_con(group_object_number, communication_flags, config_flags, descriptor.type);
    }
}

void Group_Object_Server::U_Value_Write_req(const Group_Object_Number group_object_number, const EMI_U_Value_Write_req::Write_Mask write_mask, const Group_Object_Communication_Flags ram_flags, const std::vector<uint8_t> value)
{
    assert(group_object_table);

    const ASAP_Group asap = group_object_table->asap(group_object_number);
    Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
    Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];

    /* set communication flags */
    if (write_mask.value_write_enable) {
        assert(descriptor.data);
        *descriptor.data = value;
    }
    if (write_mask.update_flag_write_enable) {
        communication_flags.update = ram_flags.update;
    }
    if (write_mask.data_request_flag_write_enable) {
        communication_flags.data_request = ram_flags.data_request;
    }
    if (write_mask.transmission_status_write_enable) {
        communication_flags.transmission_status = ram_flags.transmission_status;
    }

    process_requests();
}

void Group_Object_Server::process_requests()
{
    assert(group_object_table);

    for (const auto & it : group_object_table->communication_flags) {
        const ASAP_Group & asap = it.first;
        Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
        Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];
        Group_Object_Config & config_flags = descriptor.config;

        if (!config_flags.communication_enable) {
            continue;
        }
        if (!config_flags.transmit_enable) {
            continue;
        }
        if (communication_flags.transmission_status != Group_Object_Communication_Flags::Transmission_Status::transmit_request) {
            continue;
        }

        if (communication_flags.data_request) {
            /* set communication flag to transmitting */
            communication_flags.transmission_status = Group_Object_Communication_Flags::Transmission_Status::transmitting;

            application_layer.A_GroupValue_Read_req(Ack_Request::dont_care, asap, config_flags.transmission_priority, Network_Layer_Parameter, [this, asap](const Status a_status){
                assert(group_object_table);

                Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
                //Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];
                //Group_Object_Config & config_flags = descriptor.config;

                switch (a_status) {
                case Status::ok:
                    /* set communication flag to ok */
                    communication_flags.transmission_status = Group_Object_Communication_Flags::Transmission_Status::idle_ok;
                    break;
                case Status::not_ok:
                    /* communication status shall be set to error */
                    communication_flags.transmission_status = Group_Object_Communication_Flags::Transmission_Status::idle_error;
                    // the actions following will not happen (no new value, no update flag)
                    break;
                }
            });
        } else {
            /* set communication flag to transmitting */
            communication_flags.transmission_status = Group_Object_Communication_Flags::Transmission_Status::transmitting;

            assert(descriptor.data);
            application_layer.A_GroupValue_Write_req(Ack_Request::dont_care, asap, config_flags.transmission_priority, Network_Layer_Parameter, *descriptor.data, [this, asap](const Status a_status){
                assert(group_object_table);

                Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
                //Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];
                //Group_Object_Config & config_flags = descriptor.config;

                switch (a_status) {
                case Status::ok:
                    /* set communication flag to ok */
                    communication_flags.transmission_status = Group_Object_Communication_Flags::Transmission_Status::idle_ok;
                    break;
                case Status::not_ok:
                    /* communication status shall be set to error */
                    communication_flags.transmission_status = Group_Object_Communication_Flags::Transmission_Status::idle_error;
                    break;
                }
            });
        }
    }
}

void Group_Object_Server::A_GroupValue_Read_ind_initiator()
{
    application_layer.A_GroupValue_Read_ind([this](const ASAP_Group asap, const Priority priority, const Hop_Count_Type hop_count_type) -> void {
        assert(group_object_table);

        //Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
        Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];
        Group_Object_Config & config_flags = descriptor.config;

        if (config_flags.communication_enable) {
            if (config_flags.read_enable) {
                assert(descriptor.data);
                // @note priority = config_flags.transmission_priority is an option
                application_layer.A_GroupValue_Read_res(Ack_Request::dont_care, asap, priority, hop_count_type, *descriptor.data, [this, asap](const Status a_status) -> void {
                    assert(group_object_table);

                    Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
                    //Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];
                    //Group_Object_Config & config_flags = descriptor.config;

                    switch(a_status) {
                    case Status::ok:
                        communication_flags.transmission_status = Group_Object_Communication_Flags::Transmission_Status::idle_ok;
                        break;
                    case Status::not_ok:
                        communication_flags.transmission_status = Group_Object_Communication_Flags::Transmission_Status::idle_error;
                        break;
                    }
                });

                if (U_Event_ind && U_Event_ind_message_generation) {
                    assert(group_object_association_table);

                    const TSAP_Group tsap = group_object_association_table->tsap(asap);
                    const Connection_Number cr_id = tsap;
                    U_Event_ind(cr_id);
                }
            }
        }

        A_GroupValue_Read_ind_initiator();
    });
}

void Group_Object_Server::A_GroupValue_Read_Acon_initiator()
{
    application_layer.A_GroupValue_Read_Acon([this](const ASAP_Group asap, const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const std::vector<uint8_t> data) -> void {
        assert(group_object_table);

        Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
        Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];
        Group_Object_Config & config_flags = descriptor.config;

        if (config_flags.communication_enable) {
            if (config_flags.write_enable) {
                // @note Spec doesn't contain it, but test shows this.
                if (config_flags.update_enable) {
                    /* set new Group Object Value */
                    assert(descriptor.data);
                    *descriptor.data = data;

                    /* set communication flag to update */
                    communication_flags.update = true;

                    if (U_Event_ind && U_Event_ind_message_generation) {
                        const TSAP_Group tsap = group_object_association_table->tsap(asap);
                        const Connection_Number cr_id = tsap;
                        U_Event_ind(cr_id);
                    }
                }
            }
        }

        A_GroupValue_Read_Acon_initiator();
    });
}

void Group_Object_Server::A_GroupValue_Write_ind_initiator()
{
    application_layer.A_GroupValue_Write_ind([this](const ASAP_Group asap, const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const std::vector<uint8_t> data) -> void {
        assert(group_object_table);

        Group_Object_Communication_Flags & communication_flags = group_object_table->communication_flags[asap];
        Group_Object_Descriptor & descriptor = group_object_table->group_object_descriptors[asap];
        Group_Object_Config & config_flags = descriptor.config;

        if (config_flags.communication_enable) {
            if (config_flags.write_enable) {
                assert(descriptor.data);
                if (data.size() == descriptor.data->size()) {
                    /* set new Group Object Value */
                    *descriptor.data = data;

                    /* set communication flag to update */
                    communication_flags.update = true;

                    if (U_Event_ind && U_Event_ind_message_generation) {
                        const TSAP_Group tsap = group_object_association_table->tsap(asap);
                        const Connection_Number cr_id = tsap;
                        U_Event_ind(cr_id);
                    }
                }
            }
        }

        A_GroupValue_Write_ind_initiator();
    });
}

}
