// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Control.h>
#include <KNX/03/03/03/Hop_Count_Type.h>
#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/01/02_Group_Object_Association_Table/Group_Object_Association_Table.h>
#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table.h>
#include <KNX/03/06/03/EMI/Connection_Number.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Write/EMI_U_Value_Write_req.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Group Object Server
 *
 * @ingroup KNX_03_04_01_03
 * @ingroup KNX_03_05_01_04_12
 */
class KNX_EXPORT Group_Object_Server
{
public:
    explicit Group_Object_Server(Application_Layer & application_layer);
    virtual ~Group_Object_Server();

    /** group object association table */
    std::shared_ptr<Group_Object_Association_Table> group_object_association_table;

    /** Group Object Table */
    std::shared_ptr<Group_Object_Table> group_object_table{};

    /** U_Event.ind message generation (bit 6 in BCU1/BCU2 ConfigDes) */
    bool U_Event_ind_message_generation{false};

    virtual void U_Value_Read_req(const Group_Object_Number group_object_number);
    std::function<void(const Group_Object_Number group_object_number, const Group_Object_Communication_Flags ram_flags, const std::vector<uint8_t> value)> U_Value_Read_con;

    virtual void U_Flags_Read_req(const Group_Object_Number group_object_number);
    std::function<void(const Group_Object_Number group_object_number, const Group_Object_Communication_Flags ram_flags, const Group_Object_Config eeprom_flags, const Group_Object_Type value_type)> U_Flags_Read_con;

    std::function<void(const Connection_Number cr_id)> U_Event_ind;

    virtual void U_Value_Write_req(const Group_Object_Number group_object_number, const EMI_U_Value_Write_req::Write_Mask write_mask, const Group_Object_Communication_Flags ram_flags, const std::vector<uint8_t> value);

    /** check for read requests */
    virtual void process_requests();

protected:
    Application_Layer & application_layer;

    void A_GroupValue_Read_ind_initiator();
    void A_GroupValue_Read_Acon_initiator();
    void A_GroupValue_Write_ind_initiator();
};

}
