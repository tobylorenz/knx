// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/04/01/Interface_Object_Server.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

/** failure property return code */
static const Property_Return_Code failure{0xFF}; // -1

Interface_Object_Server::Interface_Object_Server(Application_Layer & application_layer) :
    application_layer(application_layer)
{
    A_GroupPropValue_Read_ind_initiator();
    A_GroupPropValue_Write_ind_initiator();
    A_GroupPropValue_InfoReport_ind_initiator();
    A_NetworkParameter_Read_ind_initiator();
    A_NetworkParameter_Write_ind_initiator();
    A_NetworkParameter_InfoReport_ind_initiator();
    A_SystemNetworkParameter_Read_ind_initiator();
    A_SystemNetworkParameter_Write_ind_initiator();
    A_PropertyValue_Read_ind_initiator();
    A_PropertyValue_Write_ind_initiator();
    A_PropertyDescription_Read_ind_initiator();
    A_FunctionPropertyCommand_ind_initiator();
    A_FunctionPropertyState_Read_ind_initiator();
}

Interface_Object_Server::~Interface_Object_Server()
{
}

void Interface_Object_Server::A_GroupPropValue_Read_ind_initiator()
{
    application_layer.A_GroupPropValue_Read_ind([this](const ASAP_Individual /*source_address*/, const Zone zone, const Object_Type object_type, const Object_Instance object_instance, const Property_Id property_id, const Priority priority, const Hop_Count_Type hop_count_type) -> void {
        /* check if object is valid */
        std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_type_instance(object_type, object_instance);
        if (interface_object) {
            /* check if property is valid */
            std::shared_ptr<Property> property = interface_object->property_by_id(property_id);
            if (property) {
                /* check if property is a Data Property */
                std::shared_ptr<Data_Property> data_property = std::dynamic_pointer_cast<Data_Property>(property);
                if (data_property) {
                    application_layer.A_GroupPropValue_Response_req(zone, object_type, object_instance, property_id, data_property->toData(), Ack_Request::dont_care, priority, hop_count_type, [](const Status a_status) -> void {
                        // do nothing
                    });
                }
            }
        }

        A_GroupPropValue_Read_ind_initiator();
    });
}

void Interface_Object_Server::A_GroupPropValue_Write_ind_initiator()
{
    application_layer.A_GroupPropValue_Write_ind([this](const ASAP_Individual /*source_address*/, const Zone /*zone*/, const Object_Type /*object_type*/, const Object_Instance /*object_instance*/, const Property_Id /*property_id*/, const Property_Value  /*data*/, const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/) -> void {
        // @todo Interface_Object_Server::A_GroupPropValue_Write_ind

        A_GroupPropValue_Write_ind_initiator();
    });
}

void Interface_Object_Server::A_GroupPropValue_InfoReport_ind_initiator()
{
    application_layer.A_GroupPropValue_InfoReport_ind([this](const ASAP_Individual /*source_address*/, const Zone /*zone*/, const Object_Type /*object_type*/, const Object_Instance /*object_instance*/, const Property_Id /*property_id*/, const Property_Value  /*data*/, const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/) -> void {
        // @todo Interface_Object_Server::A_GroupPropValue_InfoReport_ind

        A_GroupPropValue_InfoReport_ind_initiator();
    });
}

void Interface_Object_Server::A_NetworkParameter_Read_ind_initiator()
{
    application_layer.A_NetworkParameter_Read_ind([this](const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Hop_Count_Type hop_count_type, const Parameter_Type parameter_type, const Priority priority, const std::vector<uint8_t> test_info) -> void {
        switch (parameter_type.object_type) {
        case OT_DEVICE:
            switch (parameter_type.pid) {
            case Interface_Object::PID_SERIAL_NUMBER: {
                const uint8_t operand = test_info[0];
                switch (operand) {
                case 0x01: {
                    // NM_Read_SerialNumber_By_ProgrammingMode
                    if (interface_objects->device()->programming_mode()->prog_mode) {
                        Parameter_Test_Result test_result{};
                        const Serial_Number & serial_number = interface_objects->device()->serial_number()->serial_number;
                        test_result.assign(std::cbegin(serial_number.serial_number), std::cend(serial_number.serial_number));
                        application_layer.A_NetworkParameter_Read_res(asap, comm_mode_req, hop_count_type, parameter_type, priority, test_info, test_result, [](const Status /*a_status*/){
                            // do nothing
                        });
                    }
                }
                break;
                case 0x02: {
                    // NM_Read_SerialNumber_By_ExFactoryState
                    const uint8_t wait_time = test_info[1];
                    // @todo NM_Read_SerialNumber_By_ExFactoryState
                }
                break;
                case 0x03: {
                    // NM_Read_SerialNumber_By_PowerReset
                    const uint8_t wait_time = test_info[1];
                    // @todo NM_Read_SerialNumber_By_PowerReset
                }
                break;
                default:
                    break;
                }
                break;
            }
            default:
                break;
            }
        case OT_ADDRESS_TABLE:
            switch (parameter_type.pid) {
            case Interface_Object::PID_TABLE: {
                const uint8_t range = test_info[0];
                const Group_Address start_address((test_info[1] << 8) | test_info[2]);
                const Group_Address end_address = Group_Address(start_address + range);
                bool found{false};
                for(const TSAP_Group & group_address: interface_objects->group_address_table()->group_addresses) {
                    if ((start_address <= group_address) && (group_address <= end_address)) {
                            found = true;
                            break;
                    }
                }
                if (found) {
                    application_layer.A_NetworkParameter_Read_res(asap, comm_mode_req, hop_count_type, parameter_type, priority, test_info, {}, [](const Status /*a_status*/){
                        // do nothing
                    });
                }
            }
            break;
            default:
                break;
            }
        default:
            break;
        }

        A_NetworkParameter_Read_ind_initiator();
    });
}

void Interface_Object_Server::A_NetworkParameter_Write_ind_initiator()
{
    application_layer.A_NetworkParameter_Write_ind([this](const ASAP_Individual /*asap*/, const Parameter_Type parameter_type, const Priority /*priority*/, const std::vector<uint8_t> value) -> void {
        /* check if object is valid */
        std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_type(parameter_type.object_type);
        if (interface_object) {
            /* check if property is valid */
            std::shared_ptr<Property> property = interface_object->property_by_id(parameter_type.pid);
            if (property) {
                /* check if property is a Data Property */
                std::shared_ptr<Data_Property> data_property = std::dynamic_pointer_cast<Data_Property>(property);
                if (data_property) {
                    /* write data */
                    if (data_property->description.write_enable) {
                        /* write data */
                        data_property->fromData(std::cbegin(value), std::cend(value));
                    }

                    // @todo Property Response
                }
            }
        }

        A_NetworkParameter_Write_ind_initiator();
    });
}

void Interface_Object_Server::A_NetworkParameter_InfoReport_ind_initiator()
{
    application_layer.A_NetworkParameter_InfoReport_ind([this](const ASAP_Individual /*asap*/, const Comm_Mode /*comm_mode_req*/, const Hop_Count_Type /*hop_count_type*/, const Parameter_Type /*parameter_type*/, const Priority /*priority*/, const Parameter_Test_Info_Result /*test_info_result*/) -> void {
        // @todo A_NetworkParameter_InfoReport_ind

        A_NetworkParameter_InfoReport_ind_initiator();
    });
}

void Interface_Object_Server::A_SystemNetworkParameter_Read_ind_initiator()
{
    application_layer.A_SystemNetworkParameter_Read_ind([this](const Hop_Count_Type hop_count_type, const System_Parameter_Type parameter_type, const Priority priority, const std::vector<uint8_t> test_info) -> void {
        switch (parameter_type.object_type) {
        case OT_DEVICE:
            switch (parameter_type.pid) {
            case Interface_Object::PID_SERIAL_NUMBER: {
                const uint8_t operand = test_info[0];
                switch (operand) {
                case 0x01: {
                    // NM_Read_SerialNumber_By_ProgrammingMode
                    if (interface_objects->device()->programming_mode()->prog_mode) {
                        Parameter_Test_Result test_result{};
                        const Serial_Number & serial_number = interface_objects->device()->serial_number()->serial_number;
                        test_result.assign(std::cbegin(serial_number.serial_number), std::cend(serial_number.serial_number));
                        application_layer.A_SystemNetworkParameter_Read_res(hop_count_type, parameter_type, priority, test_info, test_result, [](const Status /*a_status*/){
                            // do nothing
                        });
                    }
                }
                break;
                case 0x02: {
                    // NM_Read_SerialNumber_By_ExFactoryState
                    const uint8_t wait_time = test_info[1];
                    // @todo NM_Read_SerialNumber_By_ExFactoryState
                }
                break;
                case 0x03: {
                    // NM_Read_SerialNumber_By_PowerReset
                    const uint8_t wait_time = test_info[1];
                    // @todo NM_Read_SerialNumber_By_PowerReset
                }
                break;
                default:
                    break;
                }
                break;
            }
            default:
                break;
            }
        case OT_ADDRESS_TABLE:
            switch (parameter_type.pid) {
            case Interface_Object::PID_TABLE: {
                const uint8_t range = test_info[0];
                const Group_Address start_address((test_info[1] << 8) | test_info[2]);
                const Group_Address end_address = Group_Address(start_address + range);
                bool found{false};
                for(const TSAP_Group & group_address: interface_objects->group_address_table()->group_addresses) {
                    if ((start_address <= group_address) && (group_address <= end_address)) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    application_layer.A_SystemNetworkParameter_Read_res(hop_count_type, parameter_type, priority, test_info, {}, [](const Status /*a_status*/){
                        // do nothing
                    });
                }
            }
            break;
            default:
                break;
            }
        default:
            break;
        }

        A_SystemNetworkParameter_Read_ind_initiator();
    });
}

void Interface_Object_Server::A_SystemNetworkParameter_Write_ind_initiator()
{
    application_layer.A_SystemNetworkParameter_Write_ind([this](const Hop_Count_Type /*hop_count_type*/, const System_Parameter_Type parameter_type, const Priority /*priority*/, const std::vector<uint8_t> value) -> void {
        /* check if object is valid */
        std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_type(parameter_type.object_type);
        if (interface_object) {
            /* check if property is valid */
            std::shared_ptr<Property> property = interface_object->property_by_id(parameter_type.pid);
            if (property) {
                /* check if property is a Data Property */
                std::shared_ptr<Data_Property> data_property = std::dynamic_pointer_cast<Data_Property>(property);
                if (data_property) {
                    /* write data */
                    if (data_property->description.write_enable) {
                        /* write data */
                        data_property->fromData(std::cbegin(value), std::cend(value));
                    }

                    // @todo Property Response
                }
            }
        }

        A_SystemNetworkParameter_Write_ind_initiator();
    });
}

void Interface_Object_Server::A_PropertyValue_Read_ind_initiator()
{
    application_layer.A_PropertyValue_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index) -> void {
        /* check if object is valid */
        std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_index(object_index);
        if (!interface_object) {
            application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                // do nothing
            });
        } else {
            /* check if property is valid */
            std::shared_ptr<Property> property = interface_object->property_by_id(property_id);
            if (!property) {
                application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                    // do nothing
                });
            } else {
                /* check if property is a Data Property */
                std::shared_ptr<Data_Property> data_property = std::dynamic_pointer_cast<Data_Property>(property);
                if (!data_property) {
                    application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                        // do nothing
                    });
                } else {
                    /* check if start index is valid */
                    if (start_index - 1 >= data_property->nr_of_elem) {
                        application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                            // do nothing
                        });
                    } else {
                        /* check if count is valid */
                        if (start_index - 1 + nr_of_elem > data_property->nr_of_elem) {
                            application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                                // do nothing
                            });
                        } else {
                            /* index = 0: return max. number of elements */
                            if (start_index == 0) {
                                std::vector<uint8_t> data{};
                                data.push_back(data_property->nr_of_elem >> 8);
                                data.push_back(data_property->nr_of_elem & 0xff);
                                application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, object_index, property_id, 1, start_index, data, [](const Status /*a_status*/){
                                    // do nothing
                                });
                            } else {
                                /* read */
                                const uint8_t property_datatype_size = (data_property->description.property_datatype == PDT_CONTROL) ? 1 : size(property->description.property_datatype);
                                std::vector<uint8_t> data = data_property->toData();
                                std::vector<uint8_t> return_data;
                                return_data.assign(std::cbegin(data) + (start_index - 1) * property_datatype_size,
                                                   std::cbegin(data) + (start_index - 1 + nr_of_elem) * property_datatype_size);
                                application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, object_index, property_id, nr_of_elem, start_index, return_data, [](const Status /*a_status*/){
                                    // do nothing
                                });
                            }
                        }
                    }
                }
            }
        }

        A_PropertyValue_Read_ind_initiator();
    });
}

void Interface_Object_Server::A_PropertyValue_Write_ind_initiator()
{
    application_layer.A_PropertyValue_Write_ind([this](const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        /* check if object is valid */
        std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_index(object_index);
        if (!interface_object) {
            application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                // do nothing
            });
        } else {
            /* check if property is valid */
            std::shared_ptr<Property> property = interface_object->property_by_id(property_id);
            if (!property) {
                application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                    // do nothing
                });
            } else {
                /* check if property is a Data Property */
                std::shared_ptr<Data_Property> data_property = std::dynamic_pointer_cast<Data_Property>(property);
                if (!data_property) {
                    application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                        // do nothing
                    });
                } else {
                    /* check if start index is valid */
                    if (start_index - 1 >= data_property->description.max_nr_of_elem) {
                        application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                            // do nothing
                        });
                    } else {
                        /* check if count is valid */
                        if (start_index - 1 + nr_of_elem > data_property->description.max_nr_of_elem) {
                            application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                                // do nothing
                            });
                        } else {
                            /* index = 0: write max. number of elements */
                            if (start_index == 0) {
                                if (data_property->description.write_enable) {
                                    data_property->nr_of_elem = (data[0] << 8) | (data[1]);
                                }

                                std::vector<uint8_t> return_data{};
                                return_data.push_back(data_property->nr_of_elem >> 8);
                                return_data.push_back(data_property->nr_of_elem & 0xff);
                                application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 1, start_index, return_data, [](const Status /*a_status*/){
                                    // do nothing
                                });
                            } else {
                                /* write enable? */
                                if (!data_property->description.write_enable) {
                                    application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                                        // do nothing
                                    });
                                } else {
                                    /* authorization level? */
                                    if (data_property->description.access.write_level < *current_level) {
                                        application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                                            // do nothing
                                        });
                                    } else {
                                        /* write*/
                                        if (data_property->description.property_datatype == PDT_CONTROL) {
                                            if (data.size() != 10) {
                                                application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                                                    // do nothing
                                                });
                                            } else {
                                                data_property->fromData(std::cbegin(data), std::cend(data));
                                                application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, nr_of_elem, start_index, data_property->toData(), [](const Status /*a_status*/){
                                                    // do nothing
                                                });
                                            }
                                        } else {
                                            const uint8_t property_datatype_size = size(property->description.property_datatype);
                                            std::vector<uint8_t> new_data = data_property->toData();
                                            Max_Nr_Of_Elem new_nr_of_elem = (start_index - 1 + nr_of_elem);
                                            if (data_property->nr_of_elem < new_nr_of_elem) {
                                                data_property->nr_of_elem = new_nr_of_elem;
                                                new_data.resize(new_nr_of_elem * property_datatype_size);
                                            }
                                            std::copy(std::cbegin(data), std::cend(data), std::begin(new_data) + (start_index - 1) * property_datatype_size);
                                            data_property->fromData(std::cbegin(new_data), std::cend(new_data));

                                            std::vector<uint8_t> data_property_data = data_property->toData();
                                            if (new_data == data_property_data) {
                                                std::vector<uint8_t> return_data;
                                                return_data.assign(std::cbegin(new_data) + (start_index - 1) * property_datatype_size,
                                                                   std::cbegin(new_data) + (start_index - 1 + nr_of_elem) * property_datatype_size);
                                                application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, nr_of_elem, start_index, return_data, [](const Status /*a_status*/){
                                                    // do nothing
                                                });
                                            } else {
                                                application_layer.A_PropertyValue_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, 0, start_index, {}, [](const Status /*a_status*/){
                                                    // do nothing
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        A_PropertyValue_Write_ind_initiator();
    });
}

void Interface_Object_Server::A_PropertyDescription_Read_ind_initiator()
{
    application_layer.A_PropertyDescription_Read_ind([this](const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index) -> void {
        /* check if object exists */
        std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_index(object_index);
        if (!interface_object) {
            const bool write_enable = false;
            const Property_Type type{};
            const Max_Nr_Of_Elem max_nr_of_elem{};
            const Access access{};
            application_layer.A_PropertyDescription_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, property_index, write_enable, type, max_nr_of_elem, access, [](const Status a_status) -> void {
                // do nothing
            });
        } else {
            /* check if property exists and return it */
            std::shared_ptr<Property> property;
            if (property_id > 0) {
                // use property id
                property = interface_object->property_by_id(property_id);
            } else {
                // use property index
                property = interface_object->property_by_index(property_index);
            }
            if (!property) {
                const bool write_enable = false;
                const Property_Type type{};
                const Max_Nr_Of_Elem max_nr_of_elem{};
                const Access access{};
                application_layer.A_PropertyDescription_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property_id, property_index, write_enable, type, max_nr_of_elem, access, [](const Status a_status) -> void {
                    // do nothing
                });
            } else {
                application_layer.A_PropertyDescription_Read_res(Ack_Request::dont_care, priority, Network_Layer_Parameter, asap, object_index, property->description.property_id, property_index, property->description.write_enable, property->description.property_datatype, property->description.max_nr_of_elem, property->description.access, [](const Status a_status) -> void {
                    // do nothing
                });
            }
        }

        A_PropertyDescription_Read_ind_initiator();
    });
}

void Interface_Object_Server::A_FunctionPropertyCommand_ind_initiator()
{
    application_layer.A_FunctionPropertyCommand_ind([this](const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type /*hop_count_type*/, const Object_Index object_index, const Priority priority, const Property_Id property_id) -> void {
        /* check if object exists */
        std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_index(object_index);
        if (!interface_object) {
            Property_Value output_data{};
            output_data.insert(std::cend(output_data), std::cbegin(data) + 1, std::cend(data));
            application_layer.A_FunctionPropertyCommand_res(Ack_Request::dont_care, asap, comm_mode, data, Network_Layer_Parameter, object_index, priority, property_id, failure, [](const Status a_status) -> void {
                // do nothing
            });
        } else {
            /* check if property exists */
            std::shared_ptr<Property> property = interface_object->property_by_id(property_id);
            if (!property) {
                Property_Value output_data{};
                output_data.insert(std::cend(output_data), std::cbegin(data) + 1, std::cend(data));
                application_layer.A_FunctionPropertyCommand_res(Ack_Request::dont_care, asap, comm_mode, data, Network_Layer_Parameter, object_index, priority, property_id, failure, [](const Status a_status) -> void {
                    // do nothing
                });
            } else {
                /* check if property is a Function Property */
                std::shared_ptr<Function_Property> function_property = std::dynamic_pointer_cast<Function_Property>(property);
                if (!function_property || !function_property->command) {
                    Property_Value output_data{};
                    output_data.insert(std::cend(output_data), std::cbegin(data) + 1, std::cend(data));
                    application_layer.A_FunctionPropertyCommand_res(Ack_Request::dont_care, asap, comm_mode, data, Network_Layer_Parameter, object_index, priority, property_id, failure, [](const Status a_status) -> void {
                        // do nothing
                    });
                } else {
                    std::optional<Property_Return_Code> return_code{};
                    Property_Value  output_data{};
                    function_property->command(data, return_code, output_data);
                    application_layer.A_FunctionPropertyCommand_res(Ack_Request::dont_care, asap, comm_mode, output_data, Network_Layer_Parameter, object_index, priority, property_id, return_code, [](const Status a_status) -> void {
                        // do nothing
                    });
                }
            }
        }

        A_FunctionPropertyCommand_ind_initiator();
    });
}

void Interface_Object_Server::A_FunctionPropertyState_Read_ind_initiator()
{
    application_layer.A_FunctionPropertyState_Read_ind([this](const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type /*hop_count_type*/, const Object_Index object_index, const Priority priority, const Property_Id property_id) -> void {
        /* check if object exists */
        std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_index(object_index);
        if (!interface_object) {
            Property_Value output_data{};
            output_data.insert(std::cend(output_data), std::cbegin(data) + 1, std::cend(data));
            application_layer.A_FunctionPropertyState_Read_res(Ack_Request::dont_care, asap, comm_mode, data, Network_Layer_Parameter, object_index, priority, property_id, failure, [](const Status a_status) -> void {
                // do nothing
            });
        } else {
            /* check if property exists */
            std::shared_ptr<Property> property = interface_object->property_by_id(property_id);
            if (!property) {
                Property_Value output_data{};
                output_data.insert(std::cend(output_data), std::cbegin(data) + 1, std::cend(data));
                application_layer.A_FunctionPropertyCommand_res(Ack_Request::dont_care, asap, comm_mode, data, Network_Layer_Parameter, object_index, priority, property_id, failure, [](const Status a_status) -> void {
                    // do nothing
                });
            } else {
                /* check if property is a Function Property */
                std::shared_ptr<Function_Property> function_property = std::dynamic_pointer_cast<Function_Property>(property);
                if (!function_property || !function_property->state_read) {
                    Property_Value output_data{};
                    output_data.insert(std::cend(output_data), std::cbegin(data) + 1, std::cend(data));
                    application_layer.A_FunctionPropertyCommand_res(Ack_Request::dont_care, asap, comm_mode, data, Network_Layer_Parameter, object_index, priority, property_id, failure, [](const Status a_status) -> void {
                        // do nothing
                    });
                } else {
                    std::optional<Property_Return_Code> return_code{};
                    Property_Value  output_data{};
                    function_property->state_read(data, return_code, output_data);
                    application_layer.A_FunctionPropertyState_Read_res(Ack_Request::dont_care, asap, comm_mode, output_data, Network_Layer_Parameter, object_index, priority, property_id, return_code, [](const Status a_status) -> void {
                        // do nothing
                    });
                }
            }
        }

        A_FunctionPropertyState_Read_ind_initiator();
    });
}

}
