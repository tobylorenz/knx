// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Control.h>
#include <KNX/03/03/03/Hop_Count_Type.h>
#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/01/Interface_Objects.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Interface Object Server
 *
 * @ingroup KNX_03_04_01_04
 */
class KNX_EXPORT Interface_Object_Server
{
public:
    explicit Interface_Object_Server(Application_Layer & application_layer);
    virtual ~Interface_Object_Server();

    /** Interface Objects */
    std::shared_ptr<Interface_Objects> interface_objects{};

    /** current level */
    std::shared_ptr<Level> current_level{};

protected:
    Application_Layer & application_layer;

    void A_GroupPropValue_Read_ind_initiator();
    void A_GroupPropValue_Write_ind_initiator();
    void A_GroupPropValue_InfoReport_ind_initiator();

    // 4.4.3 Network Parameter Properties
    void A_NetworkParameter_Read_ind_initiator();
    void A_NetworkParameter_Write_ind_initiator();
    void A_NetworkParameter_InfoReport_ind_initiator();
    void A_SystemNetworkParameter_Read_ind_initiator();
    void A_SystemNetworkParameter_Write_ind_initiator();

    // 4.4.1 Data Properties
    void A_PropertyValue_Read_ind_initiator();
    void A_PropertyValue_Write_ind_initiator();
    void A_PropertyDescription_Read_ind_initiator();

    // 4.4.2 Function Properties
    void A_FunctionPropertyCommand_ind_initiator();
    void A_FunctionPropertyState_Read_ind_initiator();
};

}
