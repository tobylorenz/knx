// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Maximum Number of Elements
 *
 * @ingroup KNX_03_04_01_04_03_01_02
 */
struct KNX_EXPORT Max_Nr_Of_Elem {
    /** exponent */
    uint4_t exponent;

    /** value */
    uint12_t value;
};

}
