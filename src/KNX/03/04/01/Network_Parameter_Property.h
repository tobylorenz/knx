// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Network Parameter Property
 *
 * This is accessed using
 * - A_NetworkParameter_Read service
 * - A_NetworkParameter_Write service
 *
 * @ingroup KNX_03_04_01_04_04_03
 */
class Network_Parameter_Property
{
public:
    explicit Network_Parameter_Property();
};

}
