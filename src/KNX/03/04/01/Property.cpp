// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/04/01/Property.h>

namespace KNX {

Property::Property(const Property_Id property_id) :
    description(property_id)
{
}

}
