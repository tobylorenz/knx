// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <string>

#include <KNX/03/03/07/PropertyValue_Nr_Of_Elem.h>
#include <KNX/03/03/07/PropertyValue_Start_Index.h>
#include <KNX/03/03/07/Property_Id.h>
#include <KNX/03/03/07/Property_Value.h>
#include <KNX/03/04/01/Property_Description.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Property Description
 *
 * @ingroup KNX_03_04_01_04_03_01
 * @ingroup KNX_03_04_01_04_03_02
 */
class KNX_EXPORT Property
{
public:
    explicit Property(const Property_Id property_id);
    virtual ~Property() = default;

    /** Current Nr of Elements */
    PropertyValue_Nr_Of_Elem nr_of_elem{1};

    /** Property description */
    Property_Description description;
};

}
