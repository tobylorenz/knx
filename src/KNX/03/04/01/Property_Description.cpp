// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/04/01/Property_Description.h>

namespace KNX {

Property_Description::Property_Description(const Property_Id property_id) :
    property_id(property_id)
{
}

Property_Description::Property_Description(const Property_Id property_id, const Property_Type property_datatype, Max_Nr_Of_Elem max_nr_of_elem) :
    property_id(property_id),
    property_datatype(property_datatype),
    max_nr_of_elem(max_nr_of_elem)
{
}

}
