// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Access.h>
#include <KNX/03/03/07/Max_Nr_Of_Elem.h>
#include <KNX/03/03/07/Property_Id.h>
#include <KNX/03/03/07/Property_Index.h>
#include <KNX/03/03/07/Property_Type.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Property Description
 *
 * @ingroup KNX_03_04_01_04_03_01_02
 */
class KNX_EXPORT Property_Description
{
public:
    explicit Property_Description(const Property_Id property_id);
    Property_Description(const Property_Id property_id, const Property_Type property_datatype, Max_Nr_Of_Elem max_nr_of_elem = 1);

    /** Property Identifier */
    Property_Id property_id{};

    /* following only available in Full Interface Object */

    /** Write Enable flag */
    bool write_enable{false};

    /** Property Datatype */
    Property_Type property_datatype{};

    /** Maximum Number of Elements information */
    Max_Nr_Of_Elem max_nr_of_elem{1};

    /** definition of the read- and write access levels */
    Access access{15, 15};
};

}
