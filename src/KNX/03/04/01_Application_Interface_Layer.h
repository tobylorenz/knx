// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 3 Group Object Server */
#include <KNX/03/04/01/Group_Object_Server.h>

/* 4 Interface Object Server */
#include <KNX/03/04/01/Interface_Object_Server.h>

/* 5 File Server */
#include <KNX/03/04/01/File_Server.h>
