# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/01_Application_Interface_Layer.h)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/01_Application_Interface_Layer.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/04)

# sub directories
add_subdirectory(01)
