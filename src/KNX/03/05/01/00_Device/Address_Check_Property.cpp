// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Address_Check_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Address_Check_Property::Address_Check_Property() :
    Data_Property(Device_Object::PID_ADDR_CHECK)
{
    description.property_datatype = PDT_GENERIC_01;
}

void Address_Check_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    ++first; // @note always 00h

    assert(first == last);
}

std::vector<uint8_t> Address_Check_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(0x00);

    return data;
}

std::string Address_Check_Property::text() const
{
    return "";
}

}
