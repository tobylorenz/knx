// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Function_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Application
 *
 * @ingroup KNX_03_05_01_04_03_15
 */
class Application_Property :
    public Function_Property
{
public:
    Application_Property();
};

}
