// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/BiBat_Master_Individual_Address_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

BiBat_Master_Individual_Address_Property::BiBat_Master_Individual_Address_Property() :
    Data_Property(Device_Object::PID_BIBAT_MASTER_ADDRESS)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void BiBat_Master_Individual_Address_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    master_individual_address.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> BiBat_Master_Individual_Address_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(master_individual_address >> 8);
    data.push_back(master_individual_address & 0xff);

    return data;
}

std::string BiBat_Master_Individual_Address_Property::text() const
{
    return master_individual_address.text();
}

}
