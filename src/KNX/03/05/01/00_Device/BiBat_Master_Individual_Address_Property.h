// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * BiBat Master Individual Address
 *
 * @ingroup KNX_03_05_01_04_03_31
 */
class BiBat_Master_Individual_Address_Property :
    public Data_Property
{
public:
    BiBat_Master_Individual_Address_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** BiBat Master Individual Address */
    Individual_Address master_individual_address{};
};

}
