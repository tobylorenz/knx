// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/BiBat_Random_Pause_Table_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

BiBat_Random_Pause_Table_Property::BiBat_Random_Pause_Table_Property() :
    Data_Property(Device_Object::PID_RANDOM_PAUSE_TABLE)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
    description.max_nr_of_elem = 12;
    nr_of_elem = 12;
}

void BiBat_Random_Pause_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 12);

    for (int i = 0; i < 12; ++i) {
        random_pause_table[i] = *first++;
    }

    assert(first == last);
}

std::vector<uint8_t> BiBat_Random_Pause_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    for (int i = 0; i < 12; ++i) {
        data.push_back(random_pause_table[i]);
    }

    return data;
}

std::string BiBat_Random_Pause_Table_Property::text() const
{
    std::ostringstream oss;

    for (uint4_t random_pause : random_pause_table) {
        oss << ", " << std::dec << static_cast<uint16_t>(random_pause);
    }

    return oss.str().substr(2);
}

}
