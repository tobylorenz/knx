// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * BiBat Random Pause Table
 *
 * @ingroup KNX_03_05_01_04_03_26
 */
class BiBat_Random_Pause_Table_Property :
    public Data_Property
{
public:
    BiBat_Random_Pause_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** BiBat Random Pause Table */
    std::array<uint4_t, 12> random_pause_table{};
};

}
