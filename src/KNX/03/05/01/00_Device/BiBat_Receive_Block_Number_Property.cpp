// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/BiBat_Receive_Block_Number_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

BiBat_Receive_Block_Number_Property::BiBat_Receive_Block_Number_Property() :
    Data_Property(Device_Object::PID_RECEIVE_BLOCK_NR)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void BiBat_Receive_Block_Number_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    receive_block_number = *first++;

    assert(first == last);
}

std::vector<uint8_t> BiBat_Receive_Block_Number_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(receive_block_number);

    return data;
}

std::string BiBat_Receive_Block_Number_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(receive_block_number) << " additional receive-blocks";

    return oss.str();
}

}
