// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * BiBat Receive Block Number
 *
 * @ingroup KNX_03_05_01_04_03_27
 */
class BiBat_Receive_Block_Number_Property :
    public Data_Property
{
public:
    BiBat_Receive_Block_Number_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** BiBat Receive Block Number */
    uint7_t receive_block_number{}; // 0..126
};

}
