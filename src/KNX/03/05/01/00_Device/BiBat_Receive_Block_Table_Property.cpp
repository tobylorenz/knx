// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/BiBat_Receive_Block_Table_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

BiBat_Receive_Block_Table_Property::BiBat_Receive_Block_Table_Property() :
    Data_Property(Device_Object::PID_RECEIVE_BLOCK_TABLE)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
    description.max_nr_of_elem = 16;
    nr_of_elem = 16;
}

void BiBat_Receive_Block_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 16);

    std::copy(first, first + 16, std::begin(receive_block_table));
    first += 16;

    assert(first == last);
}

std::vector<uint8_t> BiBat_Receive_Block_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(receive_block_table), std::cend(receive_block_table));

    return data;
}

std::string BiBat_Receive_Block_Table_Property::text() const
{
    std::ostringstream oss;

    for (auto block : receive_block_table) {
        oss << ", " << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(block);
    }

    return oss.str().substr(2);
}

}
