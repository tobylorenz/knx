// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * BiBat Retransmitter Number
 *
 * @ingroup KNX_03_05_01_04_03_29
 */
class BiBat_Retransmitter_Number_Property :
    public Data_Property
{
public:
    BiBat_Retransmitter_Number_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** BiBat Retransmitter Number */
    uint2_t retransmitter_number{};
};

}
