// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Channel_Parameter_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Channel_Parameter_Property::Channel_Parameter_Property(const uint8_t channel_nr) :
    Data_Property(Device_Object::PID_CHANNEL_01_PARAM + (channel_nr - 1))
{
    description.property_datatype = PDT_GENERIC_01;
}

void Channel_Parameter_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Channel_Parameter_Property::fromData

    // assert(first == last);
}

std::vector<uint8_t> Channel_Parameter_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Channel_Parameter_Property::toData

    return data;
}

std::string Channel_Parameter_Property::text() const
{
    // @todo Channel_Parameter_Property::text
    return "";
}

}
