// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * E-Mode parameters for channel 01 to 32
 *
 * @ingroup KNX_03_05_01_04_03_36
 */
class Channel_Parameter_Property :
    public Data_Property
{
public:
    explicit Channel_Parameter_Property(const uint8_t channel_nr); // 1..32

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;
};

}
