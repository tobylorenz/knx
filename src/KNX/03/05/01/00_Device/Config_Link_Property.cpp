// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Config_Link_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Config_Link_Property::Config_Link_Property() :
    Data_Property(Device_Object::PID_CONFIG_LINK)
{
    description.property_datatype = PDT_GENERIC_04;
}

void Config_Link_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    command = static_cast<Command>(*first >> 4);
    switch (command) {
    case Command::Action_0:
        first += 4;
        break;
    case Command::Enter_Config_Mode:
        first += 4;
        break;
    case Command::Start_Link:
        data.start_link.flags = (*first >> 2) & 0x03;
        data.start_link.sub_function = *first & 0x03;
        ++first;
        data.start_link.manufacturer_code = (*first++ << 8) | (*first++);
        data.start_link.number_of_group_objects_to_link = *first++;
        break;
    case Command::Channel_Function_Actuator:
        ++first;
        data.channel_function_actuator.channel_code = (*first++ << 8) | (*first++);
        ++first;
        break;
    case Command::Channel_Function_Sensor:
        ++first;
        data.channel_function_sensor.channel_code = (*first++ << 8) | (*first++);
        ++first;
        break;
    case Command::Set_Channel_Param:
        data.set_channel_param.flags = *first & 0x0f;
        ++first;
        data.set_channel_param.parameter_index = *first++;
        data.set_channel_param.value = (*first++ << 8) | (*first++);
        break;
    case Command::Channel_Param_Response:
        data.channel_param_response.flags = *first & 0x0f;
        ++first;
        data.channel_param_response.parameter_index = *first++;
        data.channel_param_response.value = (*first++ << 8) | (*first++);
        break;
    case Command::Begin_Connection:
        first += 4;
        break;
    case Command::Set_Delete_Link:
        data.set_delete_link.sub_function = *first & 0x0f;
        ++first;
        data.set_delete_link.data = *first++;
        data.set_delete_link.group_address = (*first++ << 8) | (*first++);
        break;
    case Command::Link_Response:
        data.link_response.flags = *first & 0x0f;
        ++first;
        data.link_response.data = *first++;
        data.link_response.group_address = (*first++ << 8) | (*first++);
        break;
    case Command::Stop_Link:
        data.stop_link.flags = *first & 0x0f;
        first += 4;
        break;
    case Command::Quit_Config_Mode:
        first += 8;
        break;
    case Command::Reset_Installation:
        first += 8;
        break;
    case Command::Features:
        data.features.sub_function = *first & 0x0f;
        ++first;
        data.features.physical_requirements = *first++;
        data.features.reserved_1 = *first++;
        data.features.reserved_2 = *first++;
        break;
    case Command::Action_14:
        first += 4;
        break;
    case Command::Action_15:
        first += 4;
        break;
    }

    assert(first == last);
}

std::vector<uint8_t> Config_Link_Property::toData() const
{
    std::vector<uint8_t> ret_data;

    switch (command) {
    case Command::Action_0:
        ret_data.push_back(static_cast<uint8_t>(command) << 4);
        ret_data.push_back(0);
        ret_data.push_back(0);
        ret_data.push_back(0);
        break;
    case Command::Enter_Config_Mode:
        ret_data.push_back(static_cast<uint8_t>(command) << 4);
        ret_data.push_back(0);
        ret_data.push_back(0);
        ret_data.push_back(0);
        break;
    case Command::Start_Link:
        ret_data.push_back(
            (static_cast<uint8_t>(command) << 4) |
            (data.start_link.flags << 2) |
            (data.start_link.sub_function));
        ret_data.push_back(data.start_link.manufacturer_code >> 8);
        ret_data.push_back(data.start_link.manufacturer_code & 0xff);
        ret_data.push_back(data.start_link.number_of_group_objects_to_link);
        break;
    case Command::Channel_Function_Actuator:
        ret_data.push_back(static_cast<uint8_t>(command) << 4);
        ret_data.push_back(data.channel_function_actuator.channel_code >> 8);
        ret_data.push_back(data.channel_function_actuator.channel_code & 0xff);
        ret_data.push_back(0);
        break;
    case Command::Channel_Function_Sensor:
        ret_data.push_back(static_cast<uint8_t>(command) << 4);
        ret_data.push_back(data.channel_function_sensor.channel_code >> 8);
        ret_data.push_back(data.channel_function_sensor.channel_code & 0xff);
        ret_data.push_back(0);
        break;
    case Command::Set_Channel_Param:
        ret_data.push_back(
            (static_cast<uint8_t>(command) << 4) |
            data.set_channel_param.flags);
        ret_data.push_back(data.set_channel_param.parameter_index);
        ret_data.push_back(data.set_channel_param.value >> 8);
        ret_data.push_back(data.set_channel_param.value & 0xff);
        break;
    case Command::Channel_Param_Response:
        ret_data.push_back(
            (static_cast<uint8_t>(command) << 4) |
            data.channel_param_response.flags);
        ret_data.push_back(data.channel_param_response.parameter_index);
        ret_data.push_back(data.channel_param_response.value >> 8);
        ret_data.push_back(data.channel_param_response.value & 0xff);
        break;
    case Command::Begin_Connection:
        ret_data.push_back(static_cast<uint8_t>(command) << 4);
        ret_data.push_back(0);
        ret_data.push_back(0);
        ret_data.push_back(0);
        break;
    case Command::Set_Delete_Link:
        ret_data.push_back(
            (static_cast<uint8_t>(command) << 4) |
            data.set_delete_link.sub_function);
        ret_data.push_back(data.set_delete_link.data);
        ret_data.push_back(data.set_delete_link.group_address >> 8);
        ret_data.push_back(data.set_delete_link.group_address & 0xff);
        break;
    case Command::Link_Response:
        ret_data.push_back(
            (static_cast<uint8_t>(command) << 4) |
            data.link_response.flags);
        ret_data.push_back(data.link_response.data);
        ret_data.push_back(data.link_response.group_address >> 8);
        ret_data.push_back(data.link_response.group_address & 0xff);
        break;
    case Command::Stop_Link:
        ret_data.push_back(
            (static_cast<uint8_t>(command) << 4) |
            data.stop_link.flags);
        ret_data.push_back(0);
        ret_data.push_back(0);
        ret_data.push_back(0);
        break;
    case Command::Quit_Config_Mode:
        ret_data.push_back(static_cast<uint8_t>(command) << 4);
        ret_data.push_back(0);
        ret_data.push_back(0);
        ret_data.push_back(0);
        break;
    case Command::Reset_Installation:
        ret_data.push_back(static_cast<uint8_t>(command) << 4);
        ret_data.push_back(0);
        ret_data.push_back(0);
        ret_data.push_back(0);
        break;
    case Command::Features:
        ret_data.push_back(
            (static_cast<uint8_t>(command) << 4) |
            data.features.sub_function);
        ret_data.push_back(data.features.physical_requirements);
        ret_data.push_back(data.features.reserved_1);
        ret_data.push_back(data.features.reserved_2);
        break;
    case Command::Action_14:
        ret_data.push_back(static_cast<uint8_t>(command) << 4);
        ret_data.push_back(0);
        ret_data.push_back(0);
        ret_data.push_back(0);
        break;
    case Command::Action_15:
        ret_data.push_back(static_cast<uint8_t>(command) << 4);
        ret_data.push_back(0);
        ret_data.push_back(0);
        ret_data.push_back(0);
        break;
    }

    // @todo Config_Link_Property::toData

    return ret_data;
}

std::string Config_Link_Property::text() const
{
    // @todo Config_Link_Property::text
    return "";
}

Config_Link_Property::Data::Data()
{
    // @todo any initialization needed?
}

}
