// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Config Link
 *
 * @ingroup KNX_03_05_01_04_03_10
 */
class Config_Link_Property :
    public Data_Property
{
public:
    Config_Link_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Command */
    enum class Command : uint8_t { // actually uint4_t
        Action_0 = 0,
        Enter_Config_Mode = 1,
        Start_Link = 2,
        Channel_Function_Actuator = 3,
        Channel_Function_Sensor = 4,
        Set_Channel_Param = 5,
        Channel_Param_Response = 6,
        Begin_Connection = 7,
        Set_Delete_Link = 8,
        Link_Response = 9,
        Stop_Link = 10,
        Quit_Config_Mode = 11,
        Reset_Installation = 12,
        Features = 13,
        Action_14 = 14,
        Action_15 = 15
    };
    Command command{Command::Action_0};

    union Data {
        Data();

        /** Enter_Config_Mode */
        struct Enter_Config_Mode {
        } enter_config_mode;

        /** Start_Link */
        struct Start_Link {
            uint2_t flags;
            uint2_t sub_function;
            uint16_t manufacturer_code;
            uint8_t number_of_group_objects_to_link;
        } start_link;

        /** Channel_Function_Actuator */
        struct Channel_Function_Actuator {
            uint13_t channel_code;
        } channel_function_actuator;

        /** Channel_Function_Sensor */
        struct Channel_Function_Sensor {
            uint13_t channel_code;
        } channel_function_sensor;

        /** Set_Channel_Param */
        struct Set_Channel_Param {
            uint4_t flags;
            uint8_t parameter_index;
            uint16_t value;
        } set_channel_param;

        /** Channel_Param_Response */
        struct Channel_Param_Response {
            uint4_t flags;
            uint8_t parameter_index;
            uint16_t value;
        } channel_param_response;

        /** Begin_Connection */
        struct Begin_Connection {
        } begin_connection;

        /** Set_Delete_Link */
        struct Set_Delete_Link {
            uint4_t sub_function;
            uint8_t data;
            uint16_t group_address;
        } set_delete_link;

        /** Link_Response */
        struct Link_Response {
            uint4_t flags;
            uint8_t data;
            uint16_t group_address;
        } link_response;

        /** Stop_Link */
        struct Stop_Link {
            uint4_t flags;
        } stop_link;

        /** Quit_Config_Mode */
        struct Quit_Config_Mode {
        } quit_config_mode;

        /** Reset_Installation */
        struct Reset_Installation {
        } reset_installation;

        /** Features */
        struct Features {
            uint4_t sub_function;
            uint8_t physical_requirements;
            uint8_t reserved_1;
            uint8_t reserved_2;
        } features;
    } data;
};

}
