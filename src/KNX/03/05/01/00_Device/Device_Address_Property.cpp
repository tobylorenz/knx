// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Device_Address_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Device_Address_Property::Device_Address_Property() :
    Data_Property(Device_Object::PID_DEVICE_ADDR)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Device_Address_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    device_address = *first++;

    assert(first == last);
}

std::vector<uint8_t> Device_Address_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(device_address);

    return data;
}

std::string Device_Address_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(device_address);

    return oss.str();
}

}
