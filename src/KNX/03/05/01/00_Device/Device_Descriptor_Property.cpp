// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Device_Descriptor_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Device_Descriptor_Property::Device_Descriptor_Property() :
    Data_Property(Device_Object::PID_DEVICE_DESCRIPTOR)
{
    description.property_datatype = PDT_GENERIC_02;
}

void Device_Descriptor_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    device_descriptor.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> Device_Descriptor_Property::toData() const
{
    return device_descriptor.toData();
}

std::string Device_Descriptor_Property::text() const
{
    return device_descriptor.text();
}

}
