// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/05/01/Device_Descriptor_Type_0.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Device Descriptor
 *
 * @ingroup KNX_03_05_01_04_03_33
 */
class Device_Descriptor_Property :
    public Data_Property
{
public:
    Device_Descriptor_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Devuce Descriptor Type 0 */
    Device_Descriptor_Type_0 device_descriptor{};
};

}
