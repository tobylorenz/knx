// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Device_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Device_Object::Device_Object() :
    System_Interface_Object(OT_DEVICE)
{
}

std::shared_ptr<Property> Device_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_ROUTING_COUNT:
        return std::make_shared<Routing_Count_Property>();
    case PID_MAX_RETRY_COUNT:
        return std::make_shared<Maximum_Retry_Count_Property>();
    case PID_ERROR_FLAGS:
        return std::make_shared<Error_Flags_Property>();
    case PID_PROGMODE:
        return std::make_shared<Programming_Mode_Property>();
    case PID_PRODUCT_ID:
        return std::make_shared<Product_Identification_Property>();
    case PID_MAX_APDU_LENGTH:
        return std::make_shared<Max_APDU_Length_Property>();
    case PID_SUBNET_ADDR:
        return std::make_shared<Subnetwork_Address_Property>();
    case PID_DEVICE_ADDR:
        return std::make_shared<Device_Address_Property>();
    case PID_CONFIG_LINK:
        return std::make_shared<Config_Link_Property>();
    case PID_ADDR_REPORT:
        return std::make_shared<Address_Report_Property>();
    case PID_ADDR_CHECK:
        return std::make_shared<Address_Check_Property>();
    case PID_OBJECT_VALUE:
        return std::make_shared<Object_Value_Property>();
    case PID_OBJECTLINK:
        return std::make_shared<Object_Link_Property>();
    case PID_APPLICATION:
        return std::make_shared<Application_Property>();
    case PID_PARAMETER:
        return std::make_shared<Parameter_Property>();
    case PID_OBJECT_ADDRESS:
        return std::make_shared<Object_Address_Property>();
    case PID_PSU_TYPE:
        return std::make_shared<PSU_Type_Property>();
    case PID_PSU_STATUS:
        return std::make_shared<PSU_Status_Property>();
    case PID_PSU_ENABLE:
        return std::make_shared<PSU_Enable_Property>();
    case PID_DOMAIN_ADDRESS:
        return std::make_shared<Domain_Address_Property>();
    case PID_IO_LIST:
        return std::make_shared<Interface_Object_List_Property>();
    case PID_MGT_DESCRIPTOR_01:
        return std::make_shared<Management_Descriptor_1_Property>();
    case PID_PL110_PARAM:
        return std::make_shared<PL110_Parameters_Property>();
    case PID_RF_REPEAT_COUNTER:
        return std::make_shared<RF_Repeat_Counter_Property>();
    case PID_RECEIVE_BLOCK_TABLE:
        return std::make_shared<BiBat_Receive_Block_Table_Property>();
    case PID_RANDOM_PAUSE_TABLE:
        return std::make_shared<BiBat_Random_Pause_Table_Property>();
    case PID_RECEIVE_BLOCK_NR:
        return std::make_shared<BiBat_Receive_Block_Number_Property>();
    case PID_HARDWARE_TYPE:
        return std::make_shared<Hardware_Type_Property>();
    case PID_RETRANSMITTER_NUMBER:
        return std::make_shared<BiBat_Retransmitter_Number_Property>();
    case PID_SERIAL_NR_TABLE:
        return std::make_shared<Serial_Number_Table_Property>();
    case PID_BIBAT_MASTER_ADDRESS:
        return std::make_shared<BiBat_Master_Individual_Address_Property>();
    case PID_RF_DOMAIN_ADDRESS:
        return std::make_shared<RF_Domain_Address_Property>();
    case PID_DEVICE_DESCRIPTOR:
        return std::make_shared<Device_Descriptor_Property>();
    case PID_METERING_FILTER_TABLE:
        return std::make_shared<Metering_Filter_Table_Property>();
    case PID_GROUP_TELEGR_RATE_LIMIT_TIME_BASE:
        return std::make_shared<Group_Telegram_Rate_Limitation_Time_Base_Property>();
    case PID_GROUP_TELEGR_RATE_LIMIT_NO_OF_TELEGR:
        return std::make_shared<Group_Telegram_Rate_Limitation_Number_Of_Telegrams_Property>();
    case PID_CHANNEL_01_PARAM: // 101
    case PID_CHANNEL_02_PARAM:
    case PID_CHANNEL_03_PARAM:
    case PID_CHANNEL_04_PARAM:
    case PID_CHANNEL_05_PARAM:
    case PID_CHANNEL_06_PARAM:
    case PID_CHANNEL_07_PARAM:
    case PID_CHANNEL_08_PARAM:
    case PID_CHANNEL_09_PARAM:
    case PID_CHANNEL_10_PARAM:
    case PID_CHANNEL_11_PARAM:
    case PID_CHANNEL_12_PARAM:
    case PID_CHANNEL_13_PARAM:
    case PID_CHANNEL_14_PARAM:
    case PID_CHANNEL_15_PARAM:
    case PID_CHANNEL_16_PARAM:
    case PID_CHANNEL_17_PARAM:
    case PID_CHANNEL_18_PARAM:
    case PID_CHANNEL_19_PARAM:
    case PID_CHANNEL_20_PARAM:
    case PID_CHANNEL_21_PARAM:
    case PID_CHANNEL_22_PARAM:
    case PID_CHANNEL_23_PARAM:
    case PID_CHANNEL_24_PARAM:
    case PID_CHANNEL_25_PARAM:
    case PID_CHANNEL_26_PARAM:
    case PID_CHANNEL_27_PARAM:
    case PID_CHANNEL_28_PARAM:
    case PID_CHANNEL_29_PARAM:
    case PID_CHANNEL_30_PARAM:
    case PID_CHANNEL_31_PARAM:
    case PID_CHANNEL_32_PARAM: // 132
        return std::make_shared<Channel_Parameter_Property>(property_id - 100);
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Routing_Count_Property> Device_Object::routing_count()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_ROUTING_COUNT);
    if (!property) {
        property = std::make_shared<Routing_Count_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Routing_Count_Property>(property);
}

std::shared_ptr<Maximum_Retry_Count_Property> Device_Object::maximum_retry_count()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_MAX_RETRY_COUNT);
    if (!property) {
        property = std::make_shared<Maximum_Retry_Count_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Maximum_Retry_Count_Property>(property);
}

std::shared_ptr<Error_Flags_Property> Device_Object::error_flags()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_ERROR_FLAGS);
    if (!property) {
        property = std::make_shared<Error_Flags_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Error_Flags_Property>(property);
}

std::shared_ptr<Programming_Mode_Property> Device_Object::programming_mode()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_PROGMODE);
    if (!property) {
        property = std::make_shared<Programming_Mode_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Programming_Mode_Property>(property);
}

std::shared_ptr<Product_Identification_Property> Device_Object::product_id()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_PRODUCT_ID);
    if (!property) {
        property = std::make_shared<Product_Identification_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Product_Identification_Property>(property);
}

std::shared_ptr<Max_APDU_Length_Property> Device_Object::max_apdu_length()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_MAX_APDU_LENGTH);
    if (!property) {
        property = std::make_shared<Max_APDU_Length_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Max_APDU_Length_Property>(property);
}

std::shared_ptr<Subnetwork_Address_Property> Device_Object::subnetwork_address()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_SUBNET_ADDR);
    if (!property) {
        property = std::make_shared<Subnetwork_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Subnetwork_Address_Property>(property);
}

std::shared_ptr<Device_Address_Property> Device_Object::device_address()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_DEVICE_ADDR);
    if (!property) {
        property = std::make_shared<Device_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Device_Address_Property>(property);
}

Individual_Address Device_Object::individual_addr()
{
    Individual_Address retval{};
    retval.set_subnetwork_address(subnetwork_address()->subnetwork_address);
    retval.set_device_address(device_address()->device_address);
    return retval;
}

std::shared_ptr<Config_Link_Property> Device_Object::config_link()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_CONFIG_LINK);
    if (!property) {
        property = std::make_shared<Config_Link_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Config_Link_Property>(property);
}

std::shared_ptr<Address_Report_Property> Device_Object::address_report()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_ADDR_REPORT);
    if (!property) {
        property = std::make_shared<Address_Report_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Address_Report_Property>(property);
}

std::shared_ptr<Address_Check_Property> Device_Object::address_check()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_ADDR_CHECK);
    if (!property) {
        property = std::make_shared<Address_Check_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Address_Check_Property>(property);
}

std::shared_ptr<Object_Value_Property> Device_Object::object_value()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_OBJECT_VALUE);
    if (!property) {
        std::shared_ptr<Object_Value_Property> property2 = std::make_shared<Object_Value_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property2;

        property2->command = std::bind(&Device_Object::object_value_control_command, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        property2->state_read = std::bind(&Device_Object::object_value_control_state_read, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }
    return std::dynamic_pointer_cast<Object_Value_Property>(property);
}

std::shared_ptr<Object_Link_Property> Device_Object::object_link()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_OBJECTLINK);
    if (!property) {
        std::shared_ptr<Object_Link_Property> property2 = std::make_shared<Object_Link_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property2;

        property2->command = std::bind(&Device_Object::object_link_control_command, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        property2->state_read = std::bind(&Device_Object::object_link_control_state_read, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }
    return std::dynamic_pointer_cast<Object_Link_Property>(property);
}

std::shared_ptr<Application_Property> Device_Object::application()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_APPLICATION);
    if (!property) {
        std::shared_ptr<Application_Property> property2 = std::make_shared<Application_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property2;

        property2->command = std::bind(&Device_Object::application_control_command, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        property2->state_read = std::bind(&Device_Object::application_control_state_read, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }
    return std::dynamic_pointer_cast<Application_Property>(property);
}

std::shared_ptr<Parameter_Property> Device_Object::parameter()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_PARAMETER);
    if (!property) {
        std::shared_ptr<Parameter_Property> property2 = std::make_shared<Parameter_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property2;

        property2->command = std::bind(&Device_Object::parameter_control_command, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        property2->state_read = std::bind(&Device_Object::parameter_control_state_read, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }
    return std::dynamic_pointer_cast<Parameter_Property>(property);
}

std::shared_ptr<Object_Address_Property> Device_Object::object_address()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_OBJECT_ADDRESS);
    if (!property) {
        std::shared_ptr<Object_Address_Property> property2 = std::make_shared<Object_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;

        property2->command = std::bind(&Device_Object::object_address_control_command, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        property2->state_read = std::bind(&Device_Object::object_address_control_state_read, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }
    return std::dynamic_pointer_cast<Object_Address_Property>(property);
}

std::shared_ptr<PSU_Type_Property> Device_Object::psu_type()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_PSU_TYPE);
    if (!property) {
        property = std::make_shared<PSU_Type_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<PSU_Type_Property>(property);
}

std::shared_ptr<PSU_Status_Property> Device_Object::psu_status()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_PSU_STATUS);
    if (!property) {
        property = std::make_shared<PSU_Status_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<PSU_Status_Property>(property);
}

std::shared_ptr<PSU_Enable_Property> Device_Object::psu_enable()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_PSU_ENABLE);
    if (!property) {
        property = std::make_shared<PSU_Enable_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<PSU_Enable_Property>(property);
}

std::shared_ptr<Domain_Address_Property> Device_Object::domain_address()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_DOMAIN_ADDRESS);
    if (!property) {
        property = std::make_shared<Domain_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Domain_Address_Property>(property);
}

std::shared_ptr<Interface_Object_List_Property> Device_Object::interface_object_list()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_IO_LIST);
    if (!property) {
        property = std::make_shared<Interface_Object_List_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Interface_Object_List_Property>(property);
}

std::shared_ptr<Management_Descriptor_1_Property> Device_Object::management_descriptor_1()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_MGT_DESCRIPTOR_01);
    if (!property) {
        property = std::make_shared<Management_Descriptor_1_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Management_Descriptor_1_Property>(property);
}

std::shared_ptr<PL110_Parameters_Property> Device_Object::pl110_parameters()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_PL110_PARAM);
    if (!property) {
        property = std::make_shared<PL110_Parameters_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<PL110_Parameters_Property>(property);
}

std::shared_ptr<RF_Repeat_Counter_Property> Device_Object::rf_repeat_counter()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_RF_REPEAT_COUNTER);
    if (!property) {
        property = std::make_shared<RF_Repeat_Counter_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Repeat_Counter_Property>(property);
}

std::shared_ptr<BiBat_Receive_Block_Table_Property> Device_Object::bibat_receive_block_table()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_RECEIVE_BLOCK_TABLE);
    if (!property) {
        property = std::make_shared<BiBat_Receive_Block_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<BiBat_Receive_Block_Table_Property>(property);
}

std::shared_ptr<BiBat_Random_Pause_Table_Property> Device_Object::bibat_random_pause_table()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_RANDOM_PAUSE_TABLE);
    if (!property) {
        property = std::make_shared<BiBat_Random_Pause_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<BiBat_Random_Pause_Table_Property>(property);
}

std::shared_ptr<BiBat_Receive_Block_Number_Property> Device_Object::bibat_receive_block_number()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_RECEIVE_BLOCK_NR);
    if (!property) {
        property = std::make_shared<BiBat_Receive_Block_Number_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<BiBat_Receive_Block_Number_Property>(property);
}

std::shared_ptr<Hardware_Type_Property> Device_Object::hardware_type()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_HARDWARE_TYPE);
    if (!property) {
        property = std::make_shared<Hardware_Type_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Hardware_Type_Property>(property);
}

std::shared_ptr<BiBat_Retransmitter_Number_Property> Device_Object::bibat_retransmitter_number()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_RETRANSMITTER_NUMBER);
    if (!property) {
        property = std::make_shared<BiBat_Retransmitter_Number_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<BiBat_Retransmitter_Number_Property>(property);
}

std::shared_ptr<Serial_Number_Table_Property> Device_Object::serial_number_table()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_SERIAL_NR_TABLE);
    if (!property) {
        property = std::make_shared<Serial_Number_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Serial_Number_Table_Property>(property);
}

std::shared_ptr<BiBat_Master_Individual_Address_Property> Device_Object::bibat_master_individual_address()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_BIBAT_MASTER_ADDRESS);
    if (!property) {
        property = std::make_shared<BiBat_Master_Individual_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<BiBat_Master_Individual_Address_Property>(property);
}

std::shared_ptr<RF_Domain_Address_Property> Device_Object::rf_domain_address()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_RF_DOMAIN_ADDRESS);
    if (!property) {
        property = std::make_shared<RF_Domain_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Domain_Address_Property>(property);
}

std::shared_ptr<Device_Descriptor_Property> Device_Object::device_descriptor()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_DEVICE_DESCRIPTOR);
    if (!property) {
        property = std::make_shared<Device_Descriptor_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Device_Descriptor_Property>(property);
}

std::shared_ptr<Metering_Filter_Table_Property> Device_Object::metering_filter_table()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_METERING_FILTER_TABLE);
    if (!property) {
        property = std::make_shared<Metering_Filter_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Metering_Filter_Table_Property>(property);
}

std::shared_ptr<Group_Telegram_Rate_Limitation_Time_Base_Property> Device_Object::group_telegr_rate_limit_time_base()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_GROUP_TELEGR_RATE_LIMIT_TIME_BASE);
    if (!property) {
        property = std::make_shared<Group_Telegram_Rate_Limitation_Time_Base_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Group_Telegram_Rate_Limitation_Time_Base_Property>(property);
}

std::shared_ptr<Group_Telegram_Rate_Limitation_Number_Of_Telegrams_Property> Device_Object::group_telegr_rate_limit_no_of_telegr()
{
    std::shared_ptr<Property> property = property_by_id(Device_Object::PID_GROUP_TELEGR_RATE_LIMIT_NO_OF_TELEGR);
    if (!property) {
        property = std::make_shared<Group_Telegram_Rate_Limitation_Number_Of_Telegrams_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Group_Telegram_Rate_Limitation_Number_Of_Telegrams_Property>(property);
}

std::shared_ptr<Channel_Parameter_Property> Device_Object::channel_param(const uint8_t channel_nr)
{
    assert(channel_nr >= 1);
    assert(channel_nr <= 32);

    // PID_CHANNEL_01_PARAM (101) .. PID_CHANNEL_32_PARAM (132)
    std::shared_ptr<Property> property = property_by_id(100 + channel_nr);
    if (!property) {
        property = std::make_shared<Channel_Parameter_Property>(100 + channel_nr);
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Channel_Parameter_Property>(property);
}

void Device_Object::object_value_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

void Device_Object::object_value_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

void Device_Object::object_link_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

void Device_Object::object_link_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

void Device_Object::application_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

void Device_Object::application_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

void Device_Object::parameter_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

void Device_Object::parameter_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

void Device_Object::object_address_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

void Device_Object::object_address_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{

}

}
