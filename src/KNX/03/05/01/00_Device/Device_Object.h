// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/00_Device/Address_Check_Property.h>
#include <KNX/03/05/01/00_Device/Address_Report_Property.h>
#include <KNX/03/05/01/00_Device/Application_Property.h>
#include <KNX/03/05/01/00_Device/BiBat_Master_Individual_Address_Property.h>
#include <KNX/03/05/01/00_Device/BiBat_Random_Pause_Table_Property.h>
#include <KNX/03/05/01/00_Device/BiBat_Receive_Block_Number_Property.h>
#include <KNX/03/05/01/00_Device/BiBat_Receive_Block_Table_Property.h>
#include <KNX/03/05/01/00_Device/BiBat_Retransmitter_Number_Property.h>
#include <KNX/03/05/01/00_Device/Channel_Parameter_Property.h>
#include <KNX/03/05/01/00_Device/Config_Link_Property.h>
#include <KNX/03/05/01/00_Device/Device_Address_Property.h>
#include <KNX/03/05/01/00_Device/Device_Descriptor_Property.h>
#include <KNX/03/05/01/00_Device/Domain_Address_Property.h>
#include <KNX/03/05/01/00_Device/Error_Flags_Property.h>
#include <KNX/03/05/01/00_Device/Group_Telegram_Rate_Limitation_Number_Of_Telegrams_Property.h>
#include <KNX/03/05/01/00_Device/Group_Telegram_Rate_Limitation_Time_Base_Property.h>
#include <KNX/03/05/01/00_Device/Hardware_Type_Property.h>
#include <KNX/03/05/01/00_Device/Interface_Object_List_Property.h>
#include <KNX/03/05/01/00_Device/Management_Descriptor_1_Property.h>
#include <KNX/03/05/01/00_Device/Max_APDU_Length_Property.h>
#include <KNX/03/05/01/00_Device/Maximum_Retry_Count_Property.h>
#include <KNX/03/05/01/00_Device/Metering_Filter_Table_Property.h>
#include <KNX/03/05/01/00_Device/Object_Address_Property.h>
#include <KNX/03/05/01/00_Device/Object_Link_Property.h>
#include <KNX/03/05/01/00_Device/Object_Value_Property.h>
#include <KNX/03/05/01/00_Device/PL110_Parameters_Property.h>
#include <KNX/03/05/01/00_Device/PSU_Enable_Property.h>
#include <KNX/03/05/01/00_Device/PSU_Status_Property.h>
#include <KNX/03/05/01/00_Device/PSU_Type_Property.h>
#include <KNX/03/05/01/00_Device/Parameter_Property.h>
#include <KNX/03/05/01/00_Device/Product_Identification_Property.h>
#include <KNX/03/05/01/00_Device/Programming_Mode_Property.h>
#include <KNX/03/05/01/00_Device/RF_Domain_Address_Property.h>
#include <KNX/03/05/01/00_Device/RF_Repeat_Counter_Property.h>
#include <KNX/03/05/01/00_Device/Routing_Count_Property.h>
#include <KNX/03/05/01/00_Device/Serial_Number_Table_Property.h>
#include <KNX/03/05/01/00_Device/Subnetwork_Address_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Device Object (Object Type 0)
 *
 * @ingroup KNX_03_05_01_04_03
 * @ingroup KNX_03_06_03_04_02_02_02
 * @ingroup KNX_09_04_01_05_01_02_13_01
 */
class KNX_EXPORT Device_Object :
    public System_Interface_Object
{
public:
    explicit Device_Object();

    /**
     * Device Object (Object Type 0)
     *
     * @ingroup KNX_03_05_01_04_03
     * @ingroup KNX_03_07_03_03_03_01
     */
    enum : Property_Id {
        /** Routing Count */
        PID_ROUTING_COUNT = 51,

        /** Maximum Retry Count */
        PID_MAX_RETRY_COUNT = 52,

        /** Error Flags */
        PID_ERROR_FLAGS = 53,

        /** Programming Mode */
        PID_PROGMODE = 54,

        /** Product Identification */
        PID_PRODUCT_ID = 55,

        /** Max. APDU-Length */
        PID_MAX_APDU_LENGTH = 56,

        /** Subnetwork Address */
        PID_SUBNET_ADDR = 57,

        /** Device Address */
        PID_DEVICE_ADDR = 58,

        /** Config Link */
        PID_CONFIG_LINK = 59, // former PID_PB_CONFIG

        /** Address report */
        PID_ADDR_REPORT = 60,

        /** Address Check */
        PID_ADDR_CHECK = 61,

        /** Object Value */
        PID_OBJECT_VALUE = 62,

        /** Object Link */
        PID_OBJECTLINK = 63,

        /** Application */
        PID_APPLICATION = 64,

        /** Parameter */
        PID_PARAMETER = 65,

        /** Object Address */
        PID_OBJECT_ADDRESS = 66,

        /** PSU Type */
        PID_PSU_TYPE = 67,

        /** PSU Status */
        PID_PSU_STATUS = 68,

        /** PSU Enable */
        PID_PSU_ENABLE = 69,

        /** Domain Address */
        PID_DOMAIN_ADDRESS = 70,

        /** Interface Object List */
        PID_IO_LIST = 71,

        /** Management Descriptor 1 */
        PID_MGT_DESCRIPTOR_01 = 72,

        /** PL110 Parameters */
        PID_PL110_PARAM = 73,

        /** RF Repeat Counter */
        PID_RF_REPEAT_COUNTER = 74,

        /** BiBat Receive Block Table */
        PID_RECEIVE_BLOCK_TABLE = 75,

        /** BiBat Random Pause Table */
        PID_RANDOM_PAUSE_TABLE = 76,

        /** BiBat Receive Block Number */
        PID_RECEIVE_BLOCK_NR = 77,

        /** Hardware Type */
        PID_HARDWARE_TYPE = 78,

        /** BiBat Retransmitter Number */
        PID_RETRANSMITTER_NUMBER = 79,

        /** Serial Number Table */
        PID_SERIAL_NR_TABLE = 80,

        /** BiBat Master Individual Address */
        PID_BIBAT_MASTER_ADDRESS = 81,

        /** RF Domain Address */
        PID_RF_DOMAIN_ADDRESS = 82,

        /** Device Descriptor */
        PID_DEVICE_DESCRIPTOR = 83,

        /** Metering Filter Table */
        PID_METERING_FILTER_TABLE = 84,

        /** group telegram rate limitation time base */
        PID_GROUP_TELEGR_RATE_LIMIT_TIME_BASE = 85,

        /** group telegram rate limitation number of telegrams */
        PID_GROUP_TELEGR_RATE_LIMIT_NO_OF_TELEGR = 86,

        /** E-Mode parameters for channel 01 */
        PID_CHANNEL_01_PARAM = 101,

        /** E-Mode parameters for channel 02 */
        PID_CHANNEL_02_PARAM = 102,

        /** E-Mode parameters for channel 03 */
        PID_CHANNEL_03_PARAM = 103,

        /** E-Mode parameters for channel 04 */
        PID_CHANNEL_04_PARAM = 104,

        /** E-Mode parameters for channel 05 */
        PID_CHANNEL_05_PARAM = 105,

        /** E-Mode parameters for channel 06 */
        PID_CHANNEL_06_PARAM = 106,

        /** E-Mode parameters for channel 07 */
        PID_CHANNEL_07_PARAM = 107,

        /** E-Mode parameters for channel 08 */
        PID_CHANNEL_08_PARAM = 108,

        /** E-Mode parameters for channel 09 */
        PID_CHANNEL_09_PARAM = 109,

        /** E-Mode parameters for channel 10 */
        PID_CHANNEL_10_PARAM = 110,

        /** E-Mode parameters for channel 11 */
        PID_CHANNEL_11_PARAM = 111,

        /** E-Mode parameters for channel 12 */
        PID_CHANNEL_12_PARAM = 112,

        /** E-Mode parameters for channel 13 */
        PID_CHANNEL_13_PARAM = 113,

        /** E-Mode parameters for channel 14 */
        PID_CHANNEL_14_PARAM = 114,

        /** E-Mode parameters for channel 15 */
        PID_CHANNEL_15_PARAM = 115,

        /** E-Mode parameters for channel 16 */
        PID_CHANNEL_16_PARAM = 116,

        /** E-Mode parameters for channel 17 */
        PID_CHANNEL_17_PARAM = 117,

        /** E-Mode parameters for channel 18 */
        PID_CHANNEL_18_PARAM = 118,

        /** E-Mode parameters for channel 19 */
        PID_CHANNEL_19_PARAM = 119,

        /** E-Mode parameters for channel 20 */
        PID_CHANNEL_20_PARAM = 120,

        /** E-Mode parameters for channel 21 */
        PID_CHANNEL_21_PARAM = 121,

        /** E-Mode parameters for channel 22 */
        PID_CHANNEL_22_PARAM = 122,

        /** E-Mode parameters for channel 23 */
        PID_CHANNEL_23_PARAM = 123,

        /** E-Mode parameters for channel 24 */
        PID_CHANNEL_24_PARAM = 124,

        /** E-Mode parameters for channel 25 */
        PID_CHANNEL_25_PARAM = 125,

        /** E-Mode parameters for channel 26 */
        PID_CHANNEL_26_PARAM = 126,

        /** E-Mode parameters for channel 27 */
        PID_CHANNEL_27_PARAM = 127,

        /** E-Mode parameters for channel 28 */
        PID_CHANNEL_28_PARAM = 128,

        /** E-Mode parameters for channel 29 */
        PID_CHANNEL_29_PARAM = 129,

        /** E-Mode parameters for channel 30 */
        PID_CHANNEL_30_PARAM = 130,

        /** E-Mode parameters for channel 31 */
        PID_CHANNEL_31_PARAM = 131,

        /** E-Mode parameters for channel 32 */
        PID_CHANNEL_32_PARAM = 132,
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<Routing_Count_Property> routing_count();
    std::shared_ptr<Maximum_Retry_Count_Property> maximum_retry_count();
    std::shared_ptr<Error_Flags_Property> error_flags();
    std::shared_ptr<Programming_Mode_Property> programming_mode();
    std::shared_ptr<Product_Identification_Property> product_id();
    std::shared_ptr<Max_APDU_Length_Property> max_apdu_length();
    std::shared_ptr<Subnetwork_Address_Property> subnetwork_address();
    std::shared_ptr<Device_Address_Property> device_address();
    Individual_Address individual_addr(); // combines subnet_addr and device_addr
    std::shared_ptr<Config_Link_Property> config_link();
    std::shared_ptr<Address_Report_Property> address_report();
    std::shared_ptr<Address_Check_Property> address_check();
    std::shared_ptr<Object_Value_Property> object_value();
    std::shared_ptr<Object_Link_Property> object_link();
    std::shared_ptr<Application_Property> application();
    std::shared_ptr<Parameter_Property> parameter();
    std::shared_ptr<Object_Address_Property> object_address();
    std::shared_ptr<PSU_Type_Property> psu_type();
    std::shared_ptr<PSU_Status_Property> psu_status();
    std::shared_ptr<PSU_Enable_Property> psu_enable();
    std::shared_ptr<Domain_Address_Property> domain_address();
    std::shared_ptr<Interface_Object_List_Property> interface_object_list();
    std::shared_ptr<Management_Descriptor_1_Property> management_descriptor_1();
    std::shared_ptr<PL110_Parameters_Property> pl110_parameters();
    std::shared_ptr<RF_Repeat_Counter_Property> rf_repeat_counter();
    std::shared_ptr<BiBat_Receive_Block_Table_Property> bibat_receive_block_table();
    std::shared_ptr<BiBat_Random_Pause_Table_Property> bibat_random_pause_table();
    std::shared_ptr<BiBat_Receive_Block_Number_Property> bibat_receive_block_number();
    std::shared_ptr<Hardware_Type_Property> hardware_type();
    std::shared_ptr<BiBat_Retransmitter_Number_Property> bibat_retransmitter_number();
    std::shared_ptr<Serial_Number_Table_Property> serial_number_table();
    std::shared_ptr<BiBat_Master_Individual_Address_Property> bibat_master_individual_address();
    std::shared_ptr<RF_Domain_Address_Property> rf_domain_address();
    std::shared_ptr<Device_Descriptor_Property> device_descriptor();
    std::shared_ptr<Metering_Filter_Table_Property> metering_filter_table();
    std::shared_ptr<Group_Telegram_Rate_Limitation_Time_Base_Property> group_telegr_rate_limit_time_base();
    std::shared_ptr<Group_Telegram_Rate_Limitation_Number_Of_Telegrams_Property> group_telegr_rate_limit_no_of_telegr();
    std::shared_ptr<Channel_Parameter_Property> channel_param(const uint8_t channel_nr); // 1..32

protected:
    void object_value_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void object_value_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void object_link_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void object_link_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void application_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void application_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void parameter_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void parameter_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void object_address_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void object_address_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
};

}
