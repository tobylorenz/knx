// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Domain_Address_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Domain_Address_Property::Domain_Address_Property() :
    Data_Property(Device_Object::PID_DOMAIN_ADDRESS)
{
    description.write_enable = true;
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Domain_Address_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    domain_address.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> Domain_Address_Property::toData() const
{
    return domain_address.toData();
}

std::string Domain_Address_Property::text() const
{
    return domain_address.text();
}

}
