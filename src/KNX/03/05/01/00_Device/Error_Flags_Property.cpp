// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Error_Flags_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Error_Flags_Property::Error_Flags_Property() :
    Data_Property(Device_Object::PID_ERROR_FLAGS)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Error_Flags_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    system_1 = (*first >> 0) & 0x01;
    app = (*first >> 1) & 0x01;
    memory_error = (*first >> 2) & 0x01;
    stack = (*first >> 3) & 0x01;
    table_error = (*first >> 4) & 0x01;
    trans = (*first >> 5) & 0x01;
    system_2 = (*first >> 6) & 0x01;
    system_3 = (*first >> 7) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Error_Flags_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (system_1 << 0) |
        (app << 1) |
        (memory_error << 2) |
        (stack << 3) |
        (table_error << 4) |
        (trans << 5) |
        (system_2 << 6) |
        (system_3 << 7));

    return data;
}

std::string Error_Flags_Property::text() const
{
    std::ostringstream oss;

    oss << "System 1: " << (system_1 ? "ok" : "error")
        << ", App: " << (app ? "ok" : "error")
        << ", Memory Error: " << (memory_error ? "ok" : "error")
        << ", Stack: " << (stack ? "ok" : "error")
        << ", Table Error: " << (table_error ? "ok" : "error")
        << ", Trans: " << (trans ? "ok" : "error")
        << ", System 2: " << (system_2 ? "ok" : "error")
        << ", System 3: " << (system_3 ? "ok" : "error");

    return oss.str();
}

}
