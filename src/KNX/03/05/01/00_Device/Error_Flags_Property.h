// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Error Flags
 *
 * @ingroup KNX_03_05_01_04_03_04
 */
class Error_Flags_Property :
    public Data_Property
{
public:
    Error_Flags_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** System 1 */
    uint1_t system_1{1};

    /** App */
    uint1_t app{1};

    /** Memory Error */
    uint1_t memory_error{1};

    /** Stack */
    uint1_t stack{1};

    /** Table Error */
    uint1_t table_error{1};

    /** Trans */
    uint1_t trans{1};

    /** System 2 */
    uint1_t system_2{1};

    /** System 3 */
    uint1_t system_3{1};
};

}
