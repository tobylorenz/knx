// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * group telegram rate limitation number of telegrams
 *
 * @ingroup KNX_03_05_01_04_03_35
 * @ingroup KNX_03_05_01_04_21
 */
class Group_Telegram_Rate_Limitation_Number_Of_Telegrams_Property :
    public Data_Property
{
public:
    Group_Telegram_Rate_Limitation_Number_Of_Telegrams_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** group telegram rate limitation number of telegrams */
    uint16_t rate_limit{};
};

}
