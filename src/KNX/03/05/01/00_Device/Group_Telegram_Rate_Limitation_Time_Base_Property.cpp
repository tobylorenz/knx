// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Group_Telegram_Rate_Limitation_Time_Base_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Group_Telegram_Rate_Limitation_Time_Base_Property::Group_Telegram_Rate_Limitation_Time_Base_Property() :
    Data_Property(Device_Object::PID_GROUP_TELEGR_RATE_LIMIT_TIME_BASE)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Group_Telegram_Rate_Limitation_Time_Base_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    rate_limit = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Group_Telegram_Rate_Limitation_Time_Base_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(rate_limit >> 8);
    data.push_back(rate_limit & 0xff);

    return data;
}

std::string Group_Telegram_Rate_Limitation_Time_Base_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << rate_limit << " ms";

    return oss.str();
}

}
