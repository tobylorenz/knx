// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * group telegram rate limitation time base
 *
 * @ingroup KNX_03_05_01_04_03_34
 * @ingroup KNX_03_05_01_04_21
 */
class Group_Telegram_Rate_Limitation_Time_Base_Property :
    public Data_Property
{
public:
    Group_Telegram_Rate_Limitation_Time_Base_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** group telegram rate limitation time base */
    uint16_t rate_limit{};
};

}
