// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Hardware_Type_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Hardware_Type_Property::Hardware_Type_Property() :
    Data_Property(Device_Object::PID_HARDWARE_TYPE)
{
    description.property_datatype = PDT_GENERIC_06;
}

void Hardware_Type_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    std::copy(first, first + 6, std::begin(hardware_type));
    first += 6;

    assert(first == last);
}

std::vector<uint8_t> Hardware_Type_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(hardware_type), std::cend(hardware_type));

    return data;
}

std::string Hardware_Type_Property::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(hardware_type[0])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(hardware_type[1])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(hardware_type[2])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(hardware_type[3])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(hardware_type[4])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(hardware_type[5]);

    return oss.str();
}

}
