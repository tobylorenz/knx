// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Hardware Type
 *
 * @ingroup KNX_03_05_01_04_03_28
 */
class Hardware_Type_Property :
    public Data_Property
{
public:
    Hardware_Type_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Hardware Type */
    std::array<uint8_t, 6> hardware_type{};
};

}
