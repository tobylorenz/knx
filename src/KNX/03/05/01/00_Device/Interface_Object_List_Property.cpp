// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Interface_Object_List_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Interface_Object_List_Property::Interface_Object_List_Property() :
    Data_Property(Device_Object::PID_IO_LIST)
{
    description.property_datatype = PDT_UNSIGNED_INT;
    // description.max_nr_of_elem = 1 + x;
    // nr_of_elem = 1 + x;
}

void Interface_Object_List_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    list.clear();
    while (first != last) {
        uint16_t entry = (*first++ << 8) | (*first++);
        list.push_back(entry);
    }

    assert(first == last);
}

std::vector<uint8_t> Interface_Object_List_Property::toData() const
{
    std::vector<uint8_t> data;

    for (auto entry : list) {
        data.push_back(entry >> 8);
        data.push_back(entry & 0xff);
    }

    return data;
}

std::string Interface_Object_List_Property::text() const
{
    std::ostringstream oss;

    for (auto entry : list) {
        oss << ", " << std::dec << entry;
    }

    return oss.str().substr(2);
}

}
