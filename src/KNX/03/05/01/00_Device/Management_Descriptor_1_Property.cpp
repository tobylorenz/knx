// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Management_Descriptor_1_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Management_Descriptor_1_Property::Management_Descriptor_1_Property() :
    Data_Property(Device_Object::PID_MGT_DESCRIPTOR_01)
{
    description.property_datatype = PDT_GENERIC_10;
}

void Management_Descriptor_1_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 10);

    structure_indication = *first++;

    management_model = *first++;

    bibat_feature = *first++;

    group_address_table_management = *first >> 4;
    association_table_management = *first & 0x0f;
    ++first;

    first += 6;

    assert(first == last);
}

std::vector<uint8_t> Management_Descriptor_1_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(structure_indication);

    data.push_back(management_model);

    data.push_back(bibat_feature);

    data.push_back((group_address_table_management << 4) | association_table_management);

    data.push_back(0); // Reserved
    data.push_back(0); // Reserved
    data.push_back(0); // Reserved
    data.push_back(0); // Reserved
    data.push_back(0); // Reserved
    data.push_back(0); // Reserved

    return data;
}

std::string Management_Descriptor_1_Property::text() const
{
    // @todo Management_Descriptor_1_Property::text
    return "";
}

}
