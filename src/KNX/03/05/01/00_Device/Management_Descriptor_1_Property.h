// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Management Descriptor 1
 *
 * @ingroup KNX_03_05_01_04_03_23
 */
class Management_Descriptor_1_Property :
    public Data_Property
{
public:
    Management_Descriptor_1_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Structure indication */
    uint8_t structure_indication{};

    /** Management Model */
    uint8_t management_model{};

    /** BiBat Feature */
    uint8_t bibat_feature{};

    /* Table Management */
    uint4_t group_address_table_management{};
    uint4_t association_table_management{};
};

}
