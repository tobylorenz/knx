// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Maximum_Retry_Count_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Maximum_Retry_Count_Property::Maximum_Retry_Count_Property() :
    Data_Property(Device_Object::PID_MAX_RETRY_COUNT)
{
    description.write_enable = true;
    description.property_datatype = PDT_GENERIC_01; // test: PDT_Unsigned_Char
}

void Maximum_Retry_Count_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    busy_retry = (*first >> 4) & 0x07;
    nak_retry = *first & 0x07;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Maximum_Retry_Count_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((busy_retry << 4) | nak_retry);

    return data;
}

std::string Maximum_Retry_Count_Property::text() const
{
    std::ostringstream oss;

    oss << "Busy Retry: " << std::dec << static_cast<uint16_t>(busy_retry)
        << ", NAK Retry: " << std::dec << static_cast<uint16_t>(nak_retry);

    return oss.str();
}

}
