// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Max Retry Count
 *
 * @ingroup KNX_03_05_01_04_03_03
 */
class Maximum_Retry_Count_Property :
    public Data_Property
{
public:
    Maximum_Retry_Count_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** number of retries in case of a BUSY/FULL response */
    uint3_t busy_retry{3};

    /** number of retries in case of a NAK response or a acknowledgement time-out */
    uint3_t nak_retry{3};
};

}
