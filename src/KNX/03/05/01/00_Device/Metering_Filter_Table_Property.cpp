// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Metering_Filter_Table_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Metering_Filter_Table_Property::Metering_Filter_Table_Property() :
    Data_Property(Device_Object::PID_METERING_FILTER_TABLE)
{
    description.property_datatype = PDT_GENERIC_06; // @todo datatype?
}

void Metering_Filter_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 6);

    // @todo Metering_Filter_Table_Property::fromData

    assert(first == last);
}

std::vector<uint8_t> Metering_Filter_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Metering_Filter_Table_Property::toData

    return data;
}

std::string Metering_Filter_Table_Property::text() const
{
    // @todo Metering_Filter_Table_Property::text
    return "";
}

}
