// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/PL110_Parameters_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

PL110_Parameters_Property::PL110_Parameters_Property() :
    Data_Property(Device_Object::PID_PL110_PARAM)
{
    description.property_datatype = PDT_GENERIC_01;
}

void PL110_Parameters_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    frequency_definition = (*first >> 5) & 0x02;
    repeater_installed = (*first >> 4) & 0x01;
    repeater_mode = (*first >> 2) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> PL110_Parameters_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (frequency_definition << 5) |
        (repeater_installed << 4) |
        (repeater_mode << 2));

    return data;
}

std::string PL110_Parameters_Property::text() const
{
    std::ostringstream oss;

    oss << "Frequency definition: ";
    switch (frequency_definition) {
    case 0:
        oss << "channel B";
        break;
    case 1:
        oss << "reserved";
        break;
    case 2:
        oss << "reserved";
        break;
    case 3:
        oss << "channel A";
        break;
    }

    oss << ", Repeater installed: "
        << (repeater_installed ? "disabled" : "enabled")
        << ", Repeater mode: "
        << (repeater_mode ? "device is no Repeater" : "device is Repeater");

    return oss.str();
}

}
