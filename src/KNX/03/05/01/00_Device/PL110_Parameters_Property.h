// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PL110 Parameters
 *
 * @ingroup KNX_03_05_01_04_03_24
 */
class PL110_Parameters_Property :
    public Data_Property
{
public:
    PL110_Parameters_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Frequency definition */
    uint2_t frequency_definition{};

    /** Repeater installed */
    uint1_t repeater_installed{};

    /** Repeater mode */
    uint1_t repeater_mode{};
};

}
