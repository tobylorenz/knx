// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/PSU_Enable_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

PSU_Enable_Property::PSU_Enable_Property() :
    Data_Property(Device_Object::PID_PSU_ENABLE)
{
    description.write_enable = true;
    description.property_datatype = PDT_ENUM8;
}

void PSU_Enable_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    psu_enable.fromData(first, first + 1);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> PSU_Enable_Property::toData() const
{
    return psu_enable.toData();
}

std::string PSU_Enable_Property::text() const
{
    return psu_enable.text();
}

}
