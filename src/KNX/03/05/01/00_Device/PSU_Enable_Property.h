// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/07/02/20/DPT_20_008.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PSU Enable
 *
 * @ingroup KNX_03_05_01_04_03_20
 */
class PSU_Enable_Property :
    public Data_Property
{
public:
    PSU_Enable_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** PSU Enable */
    DPT_PSUMode psu_enable{};
};

}
