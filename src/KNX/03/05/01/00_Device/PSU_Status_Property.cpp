// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/PSU_Status_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

PSU_Status_Property::PSU_Status_Property() :
    Data_Property(Device_Object::PID_PSU_STATUS)
{
    description.property_datatype = PDT_BINARY_INFORMATION;
}

void PSU_Status_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    psu_status.fromData(first, first + 1);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> PSU_Status_Property::toData() const
{
    return psu_status.toData();
}

std::string PSU_Status_Property::text() const
{
    return psu_status.text();
}

}
