// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/PSU_Type_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

PSU_Type_Property::PSU_Type_Property() :
    Data_Property(Device_Object::PID_PSU_TYPE)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void PSU_Type_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    psu_type.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> PSU_Type_Property::toData() const
{
    return psu_type.toData();
}

std::string PSU_Type_Property::text() const
{
    return psu_type.text();
}

}
