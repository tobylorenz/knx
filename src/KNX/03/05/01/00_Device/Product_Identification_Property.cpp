// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Product_Identification_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Product_Identification_Property::Product_Identification_Property() :
    Data_Property(Device_Object::PID_PRODUCT_ID)
{
    description.property_datatype = PDT_GENERIC_10;
}

void Product_Identification_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 10);

    std::copy(first, last, std::begin(product_id));

    assert(first == last);
}

std::vector<uint8_t> Product_Identification_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(product_id), std::cend(product_id));

    return data;
}

std::string Product_Identification_Property::text() const
{
    std::ostringstream oss;

    for (auto i : product_id) {
        oss << ":" << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(i);
    }

    return oss.str().substr(1);
}

}
