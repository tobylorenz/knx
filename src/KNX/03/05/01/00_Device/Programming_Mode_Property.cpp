// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Programming_Mode_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Programming_Mode_Property::Programming_Mode_Property() :
    Data_Property(Device_Object::PID_PROGMODE)
{
    description.write_enable = true;
    description.property_datatype = PDT_BITSET8; // test: PDT_UNSIGNED_CHAR
}

void Programming_Mode_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    /* we have to reject writes with invalid parity */
    bool new_p_parity = (*first >> 7) & 0x01;
    bool new_reset = (*first >> 5) & 0x01;
    bool new_prog_mode = (*first >> 0) & 0x01;
    if (new_p_parity != (new_reset ^ new_prog_mode)) {
        return;
    }

    p_parity = new_p_parity;
    reset = new_reset;
    prog_mode = new_prog_mode;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Programming_Mode_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (parity_calculated() << 7) |
        (reset << 5) |
        (prog_mode << 0));

    return data;
}

bool Programming_Mode_Property::parity_calculated() const
{
    return reset ^ prog_mode;
}

std::string Programming_Mode_Property::text() const
{
    return prog_mode ? "device is in Programming Mode" : "device is not in Programming Mode";
}

}
