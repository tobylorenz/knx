// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Programming Mode
 *
 * @ingroup KNX_03_05_01_04_03_05
 * @ingroup KNX_03_05_01_04_20
 * @ingroup KNX_03_05_02_03_07_04
 *
 * @todo looks same as System_State
 * @ingroup KNX_09_04_01_03_01_10_02_01_02
 */
class Programming_Mode_Property :
    public Data_Property
{
public:
    Programming_Mode_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** calculate Parity */
    bool parity_calculated() const;

    /** Parity */
    bool p_parity{false};

    /** Reset */
    bool reset{false};

    /** Programming Mode */
    bool prog_mode{false};
};

}
