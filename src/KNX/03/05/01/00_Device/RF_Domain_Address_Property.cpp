// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/RF_Domain_Address_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Domain_Address_Property::RF_Domain_Address_Property() :
    Data_Property(Device_Object::PID_RF_DOMAIN_ADDRESS)
{
    description.property_datatype = PDT_GENERIC_06;
}

void RF_Domain_Address_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    rf_domain_address.fromData(first, first + 6);
    first += 6;

    assert(first == last);
}

std::vector<uint8_t> RF_Domain_Address_Property::toData() const
{
    return rf_domain_address.toData();
}

std::string RF_Domain_Address_Property::text() const
{
    return rf_domain_address.text();
}

}
