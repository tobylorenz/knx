// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Domain_Address_6.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Domain Address
 *
 * @ingroup KNX_03_05_01_04_03_32
 */
class RF_Domain_Address_Property :
    public Data_Property
{
public:
    RF_Domain_Address_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** RF Domain Address */
    Domain_Address_6 rf_domain_address{};
};

}
