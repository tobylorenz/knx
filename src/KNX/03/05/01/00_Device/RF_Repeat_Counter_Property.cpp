// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/RF_Repeat_Counter_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Repeat_Counter_Property::RF_Repeat_Counter_Property() :
    Data_Property(Device_Object::PID_RF_REPEAT_COUNTER)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void RF_Repeat_Counter_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    repeat_counter = *first++;

    assert(first == last);
}

std::vector<uint8_t> RF_Repeat_Counter_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(repeat_counter);

    return data;
}

std::string RF_Repeat_Counter_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(repeat_counter);

    return oss.str();
}

}
