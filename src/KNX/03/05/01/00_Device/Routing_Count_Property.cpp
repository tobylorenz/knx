// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Routing_Count_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Routing_Count_Property::Routing_Count_Property() :
    Data_Property(Device_Object::PID_ROUTING_COUNT)
{
    description.write_enable = true;
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Routing_Count_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    routing_count = (*first++ >> 4) & 0x07;

    assert(first == last);
}

std::vector<uint8_t> Routing_Count_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(routing_count << 4);

    return data;
}

std::string Routing_Count_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(routing_count);

    return oss.str();
}

}
