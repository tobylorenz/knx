// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/03/Hop_Count.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Routing Count
 *
 * @ingroup KNX_03_05_01_04_03_02
 */
class Routing_Count_Property :
    public Data_Property
{
public:
    Routing_Count_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Routing Count */
    Hop_Count routing_count{6};
};

}
