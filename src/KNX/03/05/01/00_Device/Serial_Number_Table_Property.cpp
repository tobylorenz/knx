// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Serial_Number_Table_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Serial_Number_Table_Property::Serial_Number_Table_Property() :
    Data_Property(Device_Object::PID_SERIAL_NR_TABLE)
{
    description.property_datatype = PDT_GENERIC_06;
}

void Serial_Number_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    serial_number_table.clear();
    while (first != last) {
        DPT_SerNum serial_number;
        serial_number.fromData(first, first + 6);
        first += 6;
        serial_number_table.push_back(serial_number);
    }

    assert(first == last);
}

std::vector<uint8_t> Serial_Number_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    for (const DPT_SerNum & serial_number : serial_number_table) {
        std::vector<uint8_t> serial_number_data = serial_number.toData();
        data.insert(std::cend(data), std::cbegin(serial_number_data), std::cend(serial_number_data));
    }

    return data;
}

std::string Serial_Number_Table_Property::text() const
{
    std::ostringstream oss;

    for (const auto & serial_number : serial_number_table) {
        oss << ", " << serial_number.text();
    }

    return oss.str().substr(2);
}

}
