// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Serial_Number.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/07/02/221/DPT_221_001.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Serial Number Table
 *
 * @ingroup KNX_03_05_01_04_03_30
 */
class Serial_Number_Table_Property :
    public Data_Property
{
public:
    Serial_Number_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Serial Number Table */
    std::vector<DPT_SerNum> serial_number_table{};
};

}
