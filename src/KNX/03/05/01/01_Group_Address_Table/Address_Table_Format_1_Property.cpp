// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/01_Group_Address_Table/Address_Table_Format_1_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Address_Table_Format_1_Property::Address_Table_Format_1_Property() :
    Data_Property(Group_Address_Table::PID_ADDRTAB1)
{
    description.property_datatype = PDT_GENERIC_04;
}

void Address_Table_Format_1_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    while (first != last) {
        Address_Mask address_mask;
        address_mask.base_address = (*first++ << 8) | (*first++);
        address_mask.mask = (*first++ << 8) | (*first++);
        address_table.push_back(address_mask);
    }

    assert(first == last);
}

std::vector<uint8_t> Address_Table_Format_1_Property::toData() const
{
    std::vector<uint8_t> data;

    for (auto address_mask : address_table) {
        data.push_back(address_mask.base_address >> 8);
        data.push_back(address_mask.base_address & 0xff);
        data.push_back(address_mask.mask >> 8);
        data.push_back(address_mask.mask & 0xff);
    }

    return data;
}

std::string Address_Table_Format_1_Property::text() const
{
    std::ostringstream oss;

    for (auto address_mask : address_table) {
        oss << ", "
            << std::setfill('0') << std::setw(4) << std::hex << address_mask.base_address
            << " & "
            << std::setfill('0') << std::setw(4) << std::hex << address_mask.mask;
    }

    return oss.str().substr(2);
}

}
