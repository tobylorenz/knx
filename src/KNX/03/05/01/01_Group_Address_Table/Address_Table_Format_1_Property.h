// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Address Table Format 1 Property
 *
 * @ingroup KNX_03_05_01_04_10_06_02_06
 */
class Address_Table_Format_1_Property :
    public Data_Property
{
public:
    Address_Table_Format_1_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Address Table Value */
    struct Address_Mask {
        /** Base Address */
        uint16_t base_address{};

        /** Mask */
        uint16_t mask{};
    };

    /** Address Table */
    std::vector<Address_Mask> address_table{};
};

}
