// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/01_Group_Address_Table/Extended_Frame_Format_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Extended_Frame_Format_Property::Extended_Frame_Format_Property() :
    Data_Property(Group_Address_Table::PID_EXT_FRAMEFORMAT)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Extended_Frame_Format_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    extended_frame_format = *first++;

    assert(first == last);
}

std::vector<uint8_t> Extended_Frame_Format_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(extended_frame_format);

    return data;
}

std::string Extended_Frame_Format_Property::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(extended_frame_format);

    return oss.str();
}

}
