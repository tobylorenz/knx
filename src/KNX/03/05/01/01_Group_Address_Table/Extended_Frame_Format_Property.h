// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Extended Frame Format Property
 *
 * @ingroup KNX_03_05_01_04_10_06_02_05
 */
class Extended_Frame_Format_Property :
    public Data_Property
{
public:
    Extended_Frame_Format_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Extended Frame Format (EFF) */
    uint4_t extended_frame_format{};
};

}
