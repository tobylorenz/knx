// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>

#include <cassert>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

Group_Address_Table::Group_Address_Table() :
    System_Interface_Object(OT_ADDRESS_TABLE),
    load_state_machine(std::make_shared<Load_State_Machine>())
{
}

std::shared_ptr<Property> Group_Address_Table::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_EXT_FRAMEFORMAT:
        return std::make_shared<Extended_Frame_Format_Property>();
    case PID_ADDRTAB1:
        return std::make_shared<Address_Table_Format_1_Property>();
    case PID_GROUP_RESPONDER_TABLE:
        return std::make_shared<Group_Responder_Table_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

Group_Address Group_Address_Table::group_address(const TSAP_Group & tsap) const
{
    /* default implementation for empty table */
    if (group_addresses.empty()) {
        // Group_Address = TSAP
        return tsap;
    }

    /* find location of group address */
    assert(tsap >= 1);
    assert(tsap <= group_addresses.size());
    std::set<TSAP_Group>::const_iterator it = std::cbegin(group_addresses);
    for (auto i = 0; i < (tsap - 1); ++i) {
        ++it;
    }
    return *it;
}

TSAP_Group Group_Address_Table::tsap(const Group_Address & group_address) const
{
    /* default implementation for empty table */
    if (group_addresses.empty()) {
        // TSAP = Group_Address
        return group_address;
    }

    /* find location of TSAP */
    assert(group_address != 0);
    std::set<TSAP_Group>::const_iterator it = group_addresses.find(group_address);

    /* tsap is position + 1 */
    const TSAP_Group tsap(std::distance(std::cbegin(group_addresses), it) + 1);
    assert(tsap >= 1);
    assert(tsap <= group_addresses.size());

    return tsap;
}

std::shared_ptr<Extended_Frame_Format_Property> Group_Address_Table::extended_frame_format()
{
    std::shared_ptr<Property> property = property_by_id(Group_Address_Table::PID_EXT_FRAMEFORMAT);
    if (!property) {
        property = std::make_shared<Extended_Frame_Format_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Extended_Frame_Format_Property>(property);
}

std::shared_ptr<Address_Table_Format_1_Property> Group_Address_Table::address_table_format_1()
{
    std::shared_ptr<Property> property = property_by_id(Group_Address_Table::PID_ADDRTAB1);
    if (!property) {
        property = std::make_shared<Address_Table_Format_1_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Address_Table_Format_1_Property>(property);
}

std::shared_ptr<Group_Responder_Table_Property> Group_Address_Table::group_responder_table()
{
    std::shared_ptr<Property> property = property_by_id(Group_Address_Table::PID_GROUP_RESPONDER_TABLE);
    if (!property) {
        property = std::make_shared<Group_Responder_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Group_Responder_Table_Property>(property);
}


}
