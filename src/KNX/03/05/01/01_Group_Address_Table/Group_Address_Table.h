// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <set>

#include <KNX/03/03/02/Group_Address.h>
#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/04/TSAP.h>
#include <KNX/03/05/01/01_Group_Address_Table/Address_Table_Format_1_Property.h>
#include <KNX/03/05/01/01_Group_Address_Table/Extended_Frame_Format_Property.h>
#include <KNX/03/05/01/01_Group_Address_Table/Group_Responder_Table_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Group Address Table (GrAT) - Realisation Type 7 (System B)
 *
 * maps connection numbers to Destination Addresses and vice versa
 *
 * @ingroup KNX_03_05_01_04_10
 * @ingroup KNX_09_04_01_05_01_02_13_02
 */
class KNX_EXPORT Group_Address_Table :
    public System_Interface_Object
{
public:
    explicit Group_Address_Table();

    /** Load State Machine */
    std::shared_ptr<Load_State_Machine> load_state_machine{};

    /* TSAP (Connection Nr.) 0 */
    Individual_Address individual_address{};

    /* TSAP (Connection Nr.) 1..N */
    std::set<TSAP_Group> group_addresses{};

    virtual Group_Address group_address(const TSAP_Group & tsap) const;
    virtual TSAP_Group tsap(const Group_Address & group_address) const;

    enum : Property_Id {
        PID_EXT_FRAMEFORMAT = 51,
        PID_ADDRTAB1 = 52,
        PID_GROUP_RESPONDER_TABLE = 53
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<Extended_Frame_Format_Property> extended_frame_format();
    std::shared_ptr<Address_Table_Format_1_Property> address_table_format_1();
    std::shared_ptr<Group_Responder_Table_Property> group_responder_table();
};

}
