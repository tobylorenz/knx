// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/01_Group_Address_Table/Group_Responder_Table_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Group_Responder_Table_Property::Group_Responder_Table_Property() :
    Data_Property(Group_Address_Table::PID_GROUP_RESPONDER_TABLE)
{
    description.property_datatype = PDT_UNSIGNED_CHAR; // @todo datatype?
}

void Group_Responder_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    group_responder_flag_table.assign(first, last);
}

std::vector<uint8_t> Group_Responder_Table_Property::toData() const
{
    return group_responder_flag_table;
}

std::string Group_Responder_Table_Property::text() const
{
    std::ostringstream oss;

    for (auto grf : group_responder_flag_table) {
        oss << ", " << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(grf);
    }

    return oss.str().substr(2);
}

}
