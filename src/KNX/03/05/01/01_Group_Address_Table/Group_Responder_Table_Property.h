// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Group Responder Table Property
 *
 * @ingroup KNX_03_05_01_04_10_07_02_05
 */
class Group_Responder_Table_Property :
    public Data_Property
{
public:
    Group_Responder_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Group Responder Flag (GRF) Table */
    std::vector<uint8_t> group_responder_flag_table{};
};

}
