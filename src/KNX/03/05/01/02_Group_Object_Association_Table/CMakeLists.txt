# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/Group_Object_Association_Table.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/Group_Object_Association_Table.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/Group_Object_Association_Table.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/05/01/02_Group_Object_Association_Table)
