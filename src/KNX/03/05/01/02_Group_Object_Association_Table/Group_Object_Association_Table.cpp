// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/02_Group_Object_Association_Table/Group_Object_Association_Table.h>

#include <algorithm>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

Group_Object_Association_Table::Group_Object_Association_Table() :
    System_Interface_Object(OT_ASSOCIATION_TABLE),
    load_state_machine(std::make_shared<Load_State_Machine>())
{
}

std::shared_ptr<Property> Group_Object_Association_Table::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

void Group_Object_Association_Table::add_entry(const TSAP_Group & tsap, const ASAP_Group & asap)
{
    std::pair<TSAP_Group, ASAP_Group> entry = {tsap, asap};
    m_table.push_back(entry);
}

TSAP_Group Group_Object_Association_Table::tsap(const ASAP_Group & asap) const
{
    if (m_table.empty()) {
        return asap;
    }

    auto entry = std::find_if(std::begin(m_table), std::end(m_table), [asap](const std::pair<TSAP_Group, ASAP_Group> q) {
        return q.second == asap;
    });
    if (entry != std::end(m_table)) {
        return entry->first;
    }
    return TSAP_Group(0);
}

ASAP_Group Group_Object_Association_Table::asap(const TSAP_Group & tsap) const
{
    if (m_table.empty()) {
        return tsap;
    }

    auto entry = std::find_if(std::begin(m_table), std::end(m_table), [tsap](const std::pair<TSAP_Group, ASAP_Group> q) {
        return q.first == tsap;
    });
    if (entry != std::end(m_table)) {
        return entry->second;
    }
    return ASAP_Group(0);
}

}
