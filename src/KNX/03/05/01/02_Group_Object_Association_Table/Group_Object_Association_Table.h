// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/03/03/04/TSAP.h>
#include <KNX/03/03/07/ASAP.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Group Object Association Table (GrOAT)
 *
 * @ingroup KNX_03_05_01_04_11
 * @ingroup KNX_09_04_01_05_01_02_13_03
 */
class KNX_EXPORT Group_Object_Association_Table :
    public System_Interface_Object
{
public:
    explicit Group_Object_Association_Table();

    /** Load State Machine */
    std::shared_ptr<Load_State_Machine> load_state_machine{};

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    virtual void add_entry(const TSAP_Group & tsap, const ASAP_Group & asap);

    virtual TSAP_Group tsap(const ASAP_Group & asap) const;
    virtual ASAP_Group asap(const TSAP_Group & tsap) const;

protected:
    /** Association Nr. 0..N */
    std::vector<std::pair<TSAP_Group, ASAP_Group>> m_table{};
};

}
