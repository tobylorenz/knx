// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/03_Application_Program/Application_Program.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

Application_Program::Application_Program() :
    System_Interface_Object(OT_APPLICATION_PROGRAM),
    load_state_machine(std::make_shared<Load_State_Machine>()),
    run_state_machine(std::make_shared<Run_State_Machine>())
{
    run_state_machine->load_state_machine = load_state_machine;
    load_state_machine->state_updated = std::bind(&Run_State_Machine::load_state_updated, run_state_machine, std::placeholders::_1, std::placeholders::_2);
}

std::shared_ptr<Property> Application_Program::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_PARAM_REFERENCE:
        return std::make_shared<Parameter_Reference_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Parameter_Reference_Property> Application_Program::parameter_reference()
{
    std::shared_ptr<Property> property = property_by_id(Application_Program::PID_PARAM_REFERENCE);
    if (!property) {
        property = std::make_shared<Parameter_Reference_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Parameter_Reference_Property>(property);
}

}
