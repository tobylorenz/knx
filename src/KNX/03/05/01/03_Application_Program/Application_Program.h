// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/03_Application_Program/Parameter_Reference_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Application Program
 *
 * @ingroup KNX_03_05_01_04_14
 * @ingroup KNX_09_04_01_05_01_02_13_04
 */
class KNX_EXPORT Application_Program :
    public System_Interface_Object
{
public:
    explicit Application_Program();

    enum : Property_Id {
        PID_PARAM_REFERENCE = 51
    };

    /** Load State Machine */
    std::shared_ptr<Load_State_Machine> load_state_machine{};

    /** Run State Machine */
    std::shared_ptr<Run_State_Machine> run_state_machine{};

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<Parameter_Reference_Property> parameter_reference();
};

}
