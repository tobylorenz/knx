// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/03_Application_Program/Parameter_Reference_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/03_Application_Program/Application_Program.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Parameter_Reference_Property::Parameter_Reference_Property() :
    Data_Property(Application_Program::PID_PARAM_REFERENCE)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Parameter_Reference_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    base_address = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Parameter_Reference_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(base_address >> 8);
    data.push_back(base_address & 0xff);

    return data;
}

std::string Parameter_Reference_Property::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex << base_address;

    return oss.str();
}

}
