// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Parameter Reference
 *
 * @ingroup KNX_03_05_01_04_13_04_02
 */
class Parameter_Reference_Property :
    public Data_Property
{
public:
    Parameter_Reference_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** base address of Parameter Block */
    uint16_t base_address{};
};

}
