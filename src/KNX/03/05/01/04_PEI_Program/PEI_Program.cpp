// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/04_PEI_Program/PEI_Program.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

PEI_Program::PEI_Program() :
    System_Interface_Object(OT_INTERFACE_PROGRAM)
{
}

std::shared_ptr<Property> PEI_Program::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

}
