// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PEI Program
 *
 * @ingroup KNX_03_05_01_04_15
 * @ingroup KNX_09_04_01_05_01_02_13_05
 * @ingroup KNX_09_04_01_05_01_02_13_06
 */
class KNX_EXPORT PEI_Program :
    public System_Interface_Object
{
public:
    explicit PEI_Program();

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;
};

}
