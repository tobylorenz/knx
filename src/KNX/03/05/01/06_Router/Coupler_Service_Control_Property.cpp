// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Coupler_Service_Control_Property.h>

#include <sstream>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Coupler_Service_Control_Property::Coupler_Service_Control_Property() :
    Data_Property(Router_Object::PID_COUPL_SERV_CONTROL)
{
    description.property_datatype = PDT_GENERIC_01;
}

void Coupler_Service_Control_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    en_sna_inconsistency_check = (*first >> 0) & 0x01;
    en_sna_heartbeat = (*first >> 1) & 0x01;
    en_sna_update_write = (*first >> 2) & 0x01;
    en_sna_read = (*first >> 3) & 0x01;
    en_subline_status = (*first >> 4) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Coupler_Service_Control_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (en_sna_inconsistency_check << 0) |
        (en_sna_heartbeat << 1) |
        (en_sna_update_write << 2) |
        (en_sna_read << 3) |
        (en_subline_status << 4));

    return data;
}

std::string Coupler_Service_Control_Property::text() const
{
    std::ostringstream oss;

    oss << "SNA inconsistency check: " << (en_sna_inconsistency_check ? "enable" : "disable")
        << ", SNA heartbeat: " << (en_sna_heartbeat ? "enable" : "disable")
        << ", SNA update write: " << (en_sna_update_write ? "enable" : "disable")
        << ", SNA read: " << (en_sna_read ? "enable" : "disable")
        << ", subline status: " << (en_subline_status ? "enable" : "disable");

    return oss.str();
}

}
