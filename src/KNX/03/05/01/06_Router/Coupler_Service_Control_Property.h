// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Coupler Service Control
 *
 * @ingroup KNX_03_05_01_04_04_07
 */
class Coupler_Service_Control_Property :
    public Data_Property
{
public:
    Coupler_Service_Control_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    bool en_sna_inconsistency_check{};
    bool en_sna_heartbeat{};
    bool en_sna_update_write{};
    bool en_sna_read{};
    bool en_subline_status{};
};

}
