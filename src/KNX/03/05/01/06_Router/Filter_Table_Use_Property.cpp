// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Filter_Table_Use_Property.h>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Filter_Table_Use_Property::Filter_Table_Use_Property() :
    Data_Property(Router_Object::PID_FILTER_TABLE_USE)
{
    description.property_datatype = PDT_BINARY_INFORMATION;
}

void Filter_Table_Use_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Filter_Table_Use_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Filter_Table_Use_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Filter_Table_Use_Property::toData

    return data;
}

std::string Filter_Table_Use_Property::text() const
{
    // @todo Filter_Table_Use_Property::text
    return "";
}

}
