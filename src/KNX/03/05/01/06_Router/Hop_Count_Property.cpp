// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Hop_Count_Property.h>

#include <sstream>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Hop_Count_Property::Hop_Count_Property() :
    Data_Property(Router_Object::PID_HOP_COUNT)
{
    description.property_datatype = PDT_GENERIC_01; // @todo datatype?
}

void Hop_Count_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    // @todo Hop_Count_Property::fromData
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Hop_Count_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Hop_Count_Property::toData
    data.push_back(0);

    return data;
}

std::string Hop_Count_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(hop_count);

    return oss.str();
}

}
