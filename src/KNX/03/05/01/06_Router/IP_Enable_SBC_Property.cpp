// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/IP_Enable_SBC_Property.h>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

IP_Enable_SBC_Property::IP_Enable_SBC_Property() :
    Data_Property(Router_Object::PID_IP_ENABLE_SBC)
{
    description.property_datatype = PDT_GENERIC_01; // @todo datatype?
}

void IP_Enable_SBC_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo IP_Enable_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> IP_Enable_SBC_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo IP_Enable_Property::toData

    return data;
}

std::string IP_Enable_SBC_Property::text() const
{
    // @todo IP_Enable_SBC_Property::text
    return "";
}

}
