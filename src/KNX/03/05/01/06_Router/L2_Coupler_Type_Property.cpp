// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/L2_Coupler_Type_Property.h>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

L2_Coupler_Type_Property::L2_Coupler_Type_Property() :
    Data_Property(Router_Object::PID_L2_COUPLER_TYPE)
{
    description.property_datatype = PDT_BITSET8;
}

void L2_Coupler_Type_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    is_repeater = *first++;

    assert(first == last);
}

std::vector<uint8_t> L2_Coupler_Type_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(is_repeater);

    return data;
}

std::string L2_Coupler_Type_Property::text() const
{
    return is_repeater ? "TP1 Repeater" : "TP1 Bridge";
}

}
