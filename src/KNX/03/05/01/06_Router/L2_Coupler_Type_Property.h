// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L2 Coupler Type
 *
 * @ingroup KNX_03_05_01_04_04_09
 */
class L2_Coupler_Type_Property :
    public Data_Property
{
public:
    L2_Coupler_Type_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Is repeater */
    bool is_repeater{1}; // 0=TP1 Bridge; 1=TP1 Repeater
};

}
