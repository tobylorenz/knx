// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Line_Status_Property.h>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Line_Status_Property::Line_Status_Property() :
    Data_Property(Router_Object::PID_LINE_STATUS)
{
    description.property_datatype = PDT_GENERIC_01;
}

void Line_Status_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    power_down_subline = *first++;

    assert(first == last);
}

std::vector<uint8_t> Line_Status_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(power_down_subline);

    return data;
}

std::string Line_Status_Property::text() const
{
    return power_down_subline ? "power down" : "power up";
}

}
