// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Main_Line_Coupler_Config_Property.h>

#include <sstream>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Main_Line_Coupler_Config_Property::Main_Line_Coupler_Config_Property() :
    Data_Property(Router_Object::PID_MAIN_LCCONFIG)
{
    description.property_datatype = PDT_GENERIC_01;
}

void Main_Line_Coupler_Config_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    phys_frame = *first & 0x03;
    phys_repeat = (*first >> 2) & 0x01;
    broadcast_lock = (*first >> 3) & 0x01;
    broadcast_repeat = (*first >> 4) & 0x01;
    group_iack_rout = (*first >> 5) & 0x01;
    phys_iack = (*first >> 6) & 0x03;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Main_Line_Coupler_Config_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (phys_frame << 0) |
        (phys_repeat << 2) |
        (broadcast_lock << 3) |
        (broadcast_repeat << 4) |
        (group_iack_rout << 5) |
        (phys_iack << 6));

    return data;
}

std::string Main_Line_Coupler_Config_Property::text() const
{
    std::ostringstream oss;

    oss << "Phys_frame: ";
    switch (phys_frame) {
    case 0:
        oss << "not used";
        break;
    case 1:
        oss << "PHYS_UNLOCK";
        break;
    case 2:
        oss << "PHYS_LOCK";
        break;
    case 3:
        oss << "PHYS_ROUT";
        break;
    }

    oss << ", Phys Repeat: " << (phys_repeat ? "repeated" : "no repetitions")
        << ", Broadcast Lock: " << (broadcast_lock ? "blocked" : "normal")
        << ", Broadcast Repeat: " << (broadcast_repeat ? "repeated" : "no repetition")
        << ", Group IAck Rout: " << (group_iack_rout ? "normal mode" : "all frames acknowledged");

    oss << ", Phys IAck: ";
    switch (phys_iack) {
    case 0:
        oss << "not used";
        break;
    case 1:
        oss << "normal mode";
        break;
    case 2:
        oss << "all frames acknowledged";
        break;
    case 3:
        oss << "all negatively acknowledged";
        break;
    }

    return oss.str();
}

}
