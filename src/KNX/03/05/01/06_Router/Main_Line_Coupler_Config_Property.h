// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Main Line Coupler Config
 *
 * @ingroup KNX_03_05_01_04_04_04
 */
class Main_Line_Coupler_Config_Property :
    public Data_Property
{
public:
    Main_Line_Coupler_Config_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    uint2_t phys_frame{};
    uint1_t phys_repeat{};
    uint1_t broadcast_lock{};
    uint1_t broadcast_repeat{};
    uint1_t group_iack_rout{};
    uint2_t phys_iack{};
};

}
