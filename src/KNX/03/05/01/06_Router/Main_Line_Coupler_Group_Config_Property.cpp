// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Main_Line_Coupler_Group_Config_Property.h>

#include <sstream>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Main_Line_Coupler_Group_Config_Property::Main_Line_Coupler_Group_Config_Property() :
    Data_Property(Router_Object::PID_MAIN_LCGRPCONFIG)
{
    description.property_datatype = PDT_GENERIC_01;
}

void Main_Line_Coupler_Group_Config_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    group_6fff = (*first >> 0) & 0x03;
    group_7000 = (*first >> 2) & 0x03;
    group_repeat = (*first >> 4) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Main_Line_Coupler_Group_Config_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (group_6fff << 0) |
        (group_7000 << 2) |
        (group_repeat << 4));

    return data;
}

std::string Main_Line_Coupler_Group_Config_Property::text() const
{
    std::ostringstream oss;

    oss << "GROUP_6FFF: ";
    switch (group_6fff) {
    case 0:
        oss << "not used";
        break;
    case 1:
        oss << "GROUP_UNLOCK6FFF";
        break;
    case 2:
        oss << "GROUP_LOCK6FFF";
        break;
    case 3:
        oss << "GROUP_ROUT6FFF";
        break;
    }

    oss << ", GROUP_7000: ";
    switch (group_6fff) {
    case 0:
        oss << "not used";
        break;
    case 1:
        oss << "GROUP_UNLOCK7000";
        break;
    case 2:
        oss << "GROUP_LOCK7000";
        break;
    case 3:
        oss << "GROUP_ROUT7000";
        break;
    }

    oss << ", GROUP_REPEAT: " << (group_repeat ? "repeition" : "no repetition");

    return oss.str();
}

}
