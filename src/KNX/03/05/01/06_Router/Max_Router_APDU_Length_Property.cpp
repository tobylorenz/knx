// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Max_Router_APDU_Length_Property.h>

#include <sstream>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Max_Router_APDU_Length_Property::Max_Router_APDU_Length_Property() :
    Data_Property(Router_Object::PID_MAX_ROUTER_APDU_LENGTH)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Max_Router_APDU_Length_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    max_apdu_length = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Max_Router_APDU_Length_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(max_apdu_length >> 8);
    data.push_back(max_apdu_length & 0xff);

    return data;
}

std::string Max_Router_APDU_Length_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << max_apdu_length;

    return oss.str();
}

}
