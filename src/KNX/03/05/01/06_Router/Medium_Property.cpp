// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Medium_Property.h>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Medium_Property::Medium_Property() :
    Data_Property(Router_Object::PID_MEDIUM)
{
    description.property_datatype = PDT_ENUM8;
}

void Medium_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    // @todo Medium_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Medium_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Medium_Property::toData

    return data;
}

std::string Medium_Property::text() const
{
    // @todo Medium_Property::text
    return "";
}

}
