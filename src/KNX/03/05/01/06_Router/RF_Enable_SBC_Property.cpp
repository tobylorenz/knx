// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/RF_Enable_SBC_Property.h>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Enable_SBC_Property::RF_Enable_SBC_Property() :
    Function_Property(Router_Object::PID_RF_ENABLE_SBC)
{
}

}
