// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Router_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

Router_Object::Router_Object() :
    System_Interface_Object(OT_ROUTER)
{
}

std::shared_ptr<Property> Router_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_LINE_STATUS:
        return std::make_shared<Line_Status_Property>();
    case PID_MAIN_LCCONFIG:
        return std::make_shared<Main_Line_Coupler_Config_Property>();
    case PID_SUB_LCCONFIG:
        return std::make_shared<Sub_Line_Coupler_Config_Property>();
    case PID_MAIN_LCGRPCONFIG:
        return std::make_shared<Main_Line_Coupler_Group_Config_Property>();
    case PID_SUB_LCGRPCONFIG:
        return std::make_shared<Sub_Line_Coupler_Group_Config_Property>();
    case PID_ROUTETABLE_CONTROL:
        return std::make_shared<Routing_Table_Control_Property>();
    case PID_COUPL_SERV_CONTROL:
        return std::make_shared<Coupler_Service_Control_Property>();
    case PID_MAX_ROUTER_APDU_LENGTH:
        return std::make_shared<Max_Router_APDU_Length_Property>();
    case PID_L2_COUPLER_TYPE:
        return std::make_shared<L2_Coupler_Type_Property>();
    case PID_HOP_COUNT:
        return std::make_shared<Hop_Count_Property>();
    case PID_MEDIUM:
        return std::make_shared<Medium_Property>();
    case PID_FILTER_TABLE_USE:
        return std::make_shared<Filter_Table_Use_Property>();
    case PID_RF_ENABLE_SBC:
        return std::make_shared<RF_Enable_SBC_Property>();
    case PID_IP_ENABLE_SBC:
        return std::make_shared<IP_Enable_SBC_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Line_Status_Property> Router_Object::line_status()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_LINE_STATUS);
    if (!property) {
        property = std::make_shared<Line_Status_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Line_Status_Property>(property);
}

std::shared_ptr<Main_Line_Coupler_Config_Property> Router_Object::main_line_coupler_cconfig()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_MAIN_LCCONFIG);
    if (!property) {
        property = std::make_shared<Main_Line_Coupler_Config_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Main_Line_Coupler_Config_Property>(property);
}

std::shared_ptr<Sub_Line_Coupler_Config_Property> Router_Object::sub_line_coupler_config()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_SUB_LCCONFIG);
    if (!property) {
        property = std::make_shared<Sub_Line_Coupler_Config_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Sub_Line_Coupler_Config_Property>(property);
}

std::shared_ptr<Main_Line_Coupler_Group_Config_Property> Router_Object::main_line_coupler_group_config()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_MAIN_LCGRPCONFIG);
    if (!property) {
        property = std::make_shared<Main_Line_Coupler_Group_Config_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Main_Line_Coupler_Group_Config_Property>(property);
}

std::shared_ptr<Sub_Line_Coupler_Group_Config_Property> Router_Object::sub_line_coupler_group_config()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_SUB_LCGRPCONFIG);
    if (!property) {
        property = std::make_shared<Sub_Line_Coupler_Group_Config_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Sub_Line_Coupler_Group_Config_Property>(property);
}

std::shared_ptr<Routing_Table_Control_Property> Router_Object::routing_table_control()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_ROUTETABLE_CONTROL);
    if (!property) {
        std::shared_ptr<Routing_Table_Control_Property> property2 = std::make_shared<Routing_Table_Control_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property2;

        property2->command = std::bind(&Router_Object::routing_table_control_command, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        property2->state_read = std::bind(&Router_Object::routing_table_control_state_read, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }
    return std::dynamic_pointer_cast<Routing_Table_Control_Property>(property);
}

std::shared_ptr<Coupler_Service_Control_Property> Router_Object::coupler_service_control()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_COUPL_SERV_CONTROL);
    if (!property) {
        property = std::make_shared<Coupler_Service_Control_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Coupler_Service_Control_Property>(property);
}

std::shared_ptr<Max_Router_APDU_Length_Property> Router_Object::max_router_apdu_length()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_MAX_ROUTER_APDU_LENGTH);
    if (!property) {
        property = std::make_shared<Max_Router_APDU_Length_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Max_Router_APDU_Length_Property>(property);
}

std::shared_ptr<L2_Coupler_Type_Property> Router_Object::l2_coupler_type()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_L2_COUPLER_TYPE);
    if (!property) {
        property = std::make_shared<L2_Coupler_Type_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<L2_Coupler_Type_Property>(property);
}

std::shared_ptr<Hop_Count_Property> Router_Object::hop_count()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_HOP_COUNT);
    if (!property) {
        property = std::make_shared<Hop_Count_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Hop_Count_Property>(property);
}

std::shared_ptr<Medium_Property> Router_Object::medium()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_MEDIUM);
    if (!property) {
        property = std::make_shared<Medium_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Medium_Property>(property);
}

std::shared_ptr<Filter_Table_Use_Property> Router_Object::filter_table_use()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_FILTER_TABLE_USE);
    if (!property) {
        property = std::make_shared<Filter_Table_Use_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Filter_Table_Use_Property>(property);
}

std::shared_ptr<RF_Enable_SBC_Property> Router_Object::rf_enable_sbc()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_RF_ENABLE_SBC);
    if (!property) {
        property = std::make_shared<RF_Enable_SBC_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Enable_SBC_Property>(property);
}

std::shared_ptr<IP_Enable_SBC_Property> Router_Object::ip_enable_sbc()
{
    std::shared_ptr<Property> property = property_by_id(Router_Object::PID_IP_ENABLE_SBC);
    if (!property) {
        property = std::make_shared<IP_Enable_SBC_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<IP_Enable_SBC_Property>(property);
}

void Router_Object::routing_table_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{
    std::vector<uint8_t>::const_iterator first = std::cbegin(input_data);
    std::vector<uint8_t>::const_iterator last = std::cend(input_data);

    assert(std::distance(first, last) >= 2);

    const uint8_t zero = *first++;
    assert(zero == 0);
    const uint8_t service_id = *first++;

    switch (service_id) {
    case Routing_Table_Control_Property::SRVID_CLEAR_ROUTINGTABLE:
        assert(first == last);

        return_code = 0x00; // success
        output_data.push_back(service_id);
        break;
    case Routing_Table_Control_Property::SRVID_SET_ROUTINGTABLE:
        assert(first == last);

        return_code = 0x00; // success
        output_data.push_back(service_id);
        break;
    case Routing_Table_Control_Property::SRVID_CLEAR_GROUPADDRESS: {
        assert(std::distance(first, last) == 4);
        const uint16_t start_address = (*first++ << 8) | *first++;
        const uint16_t end_address = (*first++ << 8) | *first++;
        assert(first == last);

        return_code = 0x00; // success
        output_data.push_back(service_id);
        output_data.push_back(start_address >> 8);
        output_data.push_back(start_address & 0xff);
        output_data.push_back(end_address >> 8);
        output_data.push_back(end_address & 0xff);
    }
    break;
    case Routing_Table_Control_Property::SRVID_SET_GROUPADDRESS: {
        assert(std::distance(first, last) == 4);
        const uint16_t start_address = (*first++ << 8) | *first++;
        const uint16_t end_address = (*first++ << 8) | *first++;
        assert(first == last);

        return_code = 0x00; // success
        output_data.push_back(service_id);
        output_data.push_back(start_address >> 8);
        output_data.push_back(start_address & 0xff);
        output_data.push_back(end_address >> 8);
        output_data.push_back(end_address & 0xff);
    }
    break;
    default:
        return_code = 0xFF; // error
        output_data.push_back(service_id);
        break;
    }
}

void Router_Object::routing_table_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data)
{
    std::vector<uint8_t>::const_iterator first = std::cbegin(input_data);
    std::vector<uint8_t>::const_iterator last = std::cend(input_data);

    assert(std::distance(first, last) >= 2);
    const uint8_t zero = *first++;
    assert(zero == 0);
    const uint8_t service_id = *first++;

    switch (service_id) {
    case Routing_Table_Control_Property::SRVID_CLEAR_ROUTINGTABLE:
        assert(first == last);

        return_code = 0x00; // success
        output_data.push_back(service_id);
        break;
    case Routing_Table_Control_Property::SRVID_SET_ROUTINGTABLE:
        assert(first == last);

        return_code = 0x00; // success
        output_data.push_back(service_id);
        break;
    case Routing_Table_Control_Property::SRVID_CLEAR_GROUPADDRESS: {
        assert(std::distance(first, last) == 4);
        const uint16_t start_address = (*first++ << 8) | *first++;
        const uint16_t end_address = (*first++ << 8) | *first++;
        assert(first == last);

        return_code = 0x00; // success
        output_data.push_back(service_id);
        output_data.push_back(start_address >> 8);
        output_data.push_back(start_address & 0xff);
        output_data.push_back(end_address >> 8);
        output_data.push_back(end_address & 0xff);
    }
    break;
    case Routing_Table_Control_Property::SRVID_SET_GROUPADDRESS: {
        assert(std::distance(first, last) == 4);
        const uint16_t start_address = (*first++ << 8) | *first++;
        const uint16_t end_address = (*first++ << 8) | *first++;
        assert(first == last);

        return_code = 0x00; // success
        output_data.push_back(service_id);
        output_data.push_back(start_address >> 8);
        output_data.push_back(start_address & 0xff);
        output_data.push_back(end_address >> 8);
        output_data.push_back(end_address & 0xff);
    }
    break;
    default:
        return_code = 0xFF; // error
        output_data.push_back(service_id);
        break;
    }
}

}
