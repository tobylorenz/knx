// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/06_Router/Coupler_Service_Control_Property.h>
#include <KNX/03/05/01/06_Router/Filter_Table_Use_Property.h>
#include <KNX/03/05/01/06_Router/Hop_Count_Property.h>
#include <KNX/03/05/01/06_Router/IP_Enable_SBC_Property.h>
#include <KNX/03/05/01/06_Router/L2_Coupler_Type_Property.h>
#include <KNX/03/05/01/06_Router/Line_Status_Property.h>
#include <KNX/03/05/01/06_Router/Main_Line_Coupler_Config_Property.h>
#include <KNX/03/05/01/06_Router/Main_Line_Coupler_Group_Config_Property.h>
#include <KNX/03/05/01/06_Router/Max_Router_APDU_Length_Property.h>
#include <KNX/03/05/01/06_Router/Medium_Property.h>
#include <KNX/03/05/01/06_Router/RF_Enable_SBC_Property.h>
#include <KNX/03/05/01/06_Router/Routing_Table_Control_Property.h>
#include <KNX/03/05/01/06_Router/Sub_Line_Coupler_Config_Property.h>
#include <KNX/03/05/01/06_Router/Sub_Line_Coupler_Group_Config_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {


/**
 * Router Object (Object Type 6)
 *
 * @ingroup KNX_03_05_01_04_04
 */
class KNX_EXPORT Router_Object :
    public System_Interface_Object
{
public:
    explicit Router_Object();

    /**
     * Router Object (Object Type 6)
     *
     * @ingroup KNX_03_05_01_04_04
     */
    enum : Property_Id {
        PID_LINE_STATUS = 51,
        PID_MAIN_LCCONFIG = 52,
        PID_SUB_LCCONFIG = 53,
        PID_MAIN_LCGRPCONFIG = 54,
        PID_SUB_LCGRPCONFIG = 55,
        PID_ROUTETABLE_CONTROL = 56,
        PID_COUPL_SERV_CONTROL = 57,
        PID_MAX_ROUTER_APDU_LENGTH = 58,
        PID_L2_COUPLER_TYPE = 59,

        PID_HOP_COUNT = 61,

        PID_MEDIUM = 63,

        PID_FILTER_TABLE_USE = 67,

        PID_RF_ENABLE_SBC = 112,

        PID_IP_ENABLE_SBC = 120
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<Line_Status_Property> line_status();
    std::shared_ptr<Main_Line_Coupler_Config_Property> main_line_coupler_cconfig();
    std::shared_ptr<Sub_Line_Coupler_Config_Property> sub_line_coupler_config();
    std::shared_ptr<Main_Line_Coupler_Group_Config_Property> main_line_coupler_group_config();
    std::shared_ptr<Sub_Line_Coupler_Group_Config_Property> sub_line_coupler_group_config();
    std::shared_ptr<Routing_Table_Control_Property> routing_table_control();
    std::shared_ptr<Coupler_Service_Control_Property> coupler_service_control(); // Subnetwork Address (SNA) management
    std::shared_ptr<Max_Router_APDU_Length_Property> max_router_apdu_length();
    std::shared_ptr<L2_Coupler_Type_Property> l2_coupler_type();
    std::shared_ptr<Hop_Count_Property> hop_count();
    std::shared_ptr<Medium_Property> medium();
    std::shared_ptr<Filter_Table_Use_Property> filter_table_use();
    std::shared_ptr<RF_Enable_SBC_Property> rf_enable_sbc();
    std::shared_ptr<IP_Enable_SBC_Property> ip_enable_sbc();

protected:
    void routing_table_control_command(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
    void routing_table_control_state_read(const Property_Value  input_data, std::optional<Property_Return_Code> & return_code, Property_Value  & output_data);
};

}
