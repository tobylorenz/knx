// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/06_Router/Routing_Table_Control_Property.h>

#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Routing_Table_Control_Property::Routing_Table_Control_Property() :
    Function_Property(Router_Object::PID_ROUTETABLE_CONTROL)
{
}

}
