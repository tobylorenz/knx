// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Function_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Routing Table Control
 *
 * @ingroup KNX_03_05_01_04_04_06
 */
class Routing_Table_Control_Property :
    public Function_Property
{
public:
    Routing_Table_Control_Property();

    enum ServiceID : uint8_t {
        SRVID_CLEAR_ROUTINGTABLE = 0x01,
        SRVID_SET_ROUTINGTABLE = 0x02,
        SRVID_CLEAR_GROUPADDRESS = 0x03,
        SRVID_SET_GROUPADDRESS = 0x04
    };
};

}
