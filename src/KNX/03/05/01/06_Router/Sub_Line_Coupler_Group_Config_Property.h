// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Sub Line Coupler Group Config
 *
 * @ingroup KNX_03_05_01_04_04_05
 */
class Sub_Line_Coupler_Group_Config_Property :
    public Data_Property
{
public:
    Sub_Line_Coupler_Group_Config_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    uint2_t group_6fff{};
    uint2_t group_7000{};
    uint1_t group_repeat{};
};

}
