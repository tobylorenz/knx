// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/07_LTE_Address_Routing_Table/LTE_Address_Routing_Table_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

LTE_Address_Routing_Table_Object::LTE_Address_Routing_Table_Object() :
    System_Interface_Object(OT_LTE_ADDRESS_ROUTING_TABLE)
{
}

std::shared_ptr<Property> LTE_Address_Routing_Table_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_LTE_ROUTESELECT:
        return std::make_shared<LTE_Routing_Select_Property>();
    case PID_LTE_ROUTETABLE:
        return std::make_shared<LTE_Routing_Table_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<LTE_Routing_Select_Property> LTE_Address_Routing_Table_Object::lte_routing_select()
{
    std::shared_ptr<Property> property = property_by_id(LTE_Address_Routing_Table_Object::PID_LTE_ROUTESELECT);
    if (!property) {
        property = std::make_shared<LTE_Routing_Select_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<LTE_Routing_Select_Property>(property);
}

std::shared_ptr<LTE_Routing_Table_Property> LTE_Address_Routing_Table_Object::lte_routing_table()
{
    std::shared_ptr<Property> property = property_by_id(LTE_Address_Routing_Table_Object::PID_LTE_ROUTETABLE);
    if (!property) {
        property = std::make_shared<LTE_Routing_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<LTE_Routing_Table_Property>(property);
}

}
