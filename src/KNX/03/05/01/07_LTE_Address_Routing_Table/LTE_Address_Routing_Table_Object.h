// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/07_LTE_Address_Routing_Table/LTE_Routing_Select_Property.h>
#include <KNX/03/05/01/07_LTE_Address_Routing_Table/LTE_Routing_Table_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * LTE Address Routing Table Object (Object Type 7)
 *
 * @ingroup KNX_03_05_01_04_05
 */
class KNX_EXPORT LTE_Address_Routing_Table_Object :
    public System_Interface_Object
{
public:
    explicit LTE_Address_Routing_Table_Object();

    /**
     * LTE Address Routing Table Object (Object Table 7)
     *
     * @ingroup KNX_03_05_01_04_05
     */
    enum : Property_Id {
        PID_LTE_ROUTESELECT = 51,
        PID_LTE_ROUTETABLE = 52
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<LTE_Routing_Select_Property> lte_routing_select();
    std::shared_ptr<LTE_Routing_Table_Property> lte_routing_table();
};

}
