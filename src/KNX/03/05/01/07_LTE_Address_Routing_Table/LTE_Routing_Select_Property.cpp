// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/07_LTE_Address_Routing_Table/LTE_Routing_Select_Property.h>

#include <sstream>

#include <KNX/03/05/01/07_LTE_Address_Routing_Table/LTE_Address_Routing_Table_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

LTE_Routing_Select_Property::LTE_Routing_Select_Property() :
    Data_Property(LTE_Address_Routing_Table_Object::PID_LTE_ROUTESELECT)
{
    description.property_datatype = PDT_GENERIC_01; // @todo datatype?
}

void LTE_Routing_Select_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    ltesub = (*first >> 0) & 0x03;
    ltemain = (*first >> 4) & 0x03;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> LTE_Routing_Select_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (ltesub << 0) |
        (ltemain << 4));

    return data;
}

std::string LTE_Routing_Select_Property::text() const
{
    std::ostringstream oss;

    oss << "LTEMAIN: ";
    switch (ltemain) {
    case 0:
        oss << "not used";
        break;
    case 1:
        oss << "LTEMAIN_PASS";
        break;
    case 2:
        oss << "LTEMAIN_BLOCK";
        break;
    case 3:
        oss << "LTEMAIN_NORMAL";
        break;
    }

    oss << ", LTESUB: ";
    switch (ltesub) {
    case 0:
        oss << "not used";
        break;
    case 1:
        oss << "LTESUB_PASS";
        break;
    case 2:
        oss << "LTESUB_BLOCK";
        break;
    case 3:
        oss << "LTESUB_NORMAL";
        break;
    }

    return oss.str();
}

}
