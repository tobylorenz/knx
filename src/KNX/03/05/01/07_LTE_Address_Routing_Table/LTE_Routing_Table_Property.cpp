// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/07_LTE_Address_Routing_Table/LTE_Routing_Table_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/07_LTE_Address_Routing_Table/LTE_Address_Routing_Table_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

LTE_Routing_Table_Property::LTE_Routing_Table_Property() :
    Data_Property(LTE_Address_Routing_Table_Object::PID_LTE_ROUTETABLE)
{
    description.property_datatype = PDT_GENERIC_05;
}

void LTE_Routing_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    direction = (*first >> 6) & 0x03;
    extended_frame_format = *first & 0x0f;
    ++first;

    base_address = (*first++ << 8) | (*first++);

    mask = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> LTE_Routing_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (direction << 6) |
        (extended_frame_format));

    data.push_back(base_address >> 8);
    data.push_back(base_address & 0xff);

    data.push_back(mask >> 8);
    data.push_back(mask & 0xff);

    return data;
}

std::string LTE_Routing_Table_Property::text() const
{
    std::ostringstream oss;

    oss << "Data Direction: " << std::dec << static_cast<uint16_t>(direction);
    oss << ", EFF: " << std::setfill('0') << std::setw(1) << std::hex << static_cast<uint16_t>(extended_frame_format);

    oss << ", Base: " << std::setfill('0') << std::setw(4) << std::hex << base_address;

    oss << ", Mask: " << std::setfill('0') << std::setw(4) << std::hex << mask;

    return oss.str();
}

}
