// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * LTE Routing Table
 *
 * @ingroup KNX_03_05_01_04_05_04
 */
class LTE_Routing_Table_Property :
    public Data_Property
{
public:
    LTE_Routing_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    uint2_t direction{};
    uint4_t extended_frame_format{};
    uint16_t base_address{};
    uint16_t mask{};
};

}
