// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Additional_Information_Types_Property.h>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Additional_Information_Types_Property::Additional_Information_Types_Property() :
    Data_Property(CEMI_Server_Object::PID_ADD_INFO_TYPES)
{
    description.property_datatype = PDT_ENUM8;
}

void Additional_Information_Types_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    additional_info_types.fromData(first, first + 1);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Additional_Information_Types_Property::toData() const
{
    return additional_info_types.toData();
}

std::string Additional_Information_Types_Property::text() const
{
    return additional_info_types.text();
}

}
