// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/07/02/20/DPT_20_1001.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Additional Information Types
 *
 * @ingroup KNX_03_05_01_04_06_04
 * @ingroup KNX_AN160_02_03_03_02
 */
class Additional_Information_Types_Property :
    public Data_Property
{
public:
    Additional_Information_Types_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Additional Information Types */
    DPT_AddInfoTypes additional_info_types{};
};

}
