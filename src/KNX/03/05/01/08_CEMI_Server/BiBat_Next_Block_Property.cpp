// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/BiBat_Next_Block_Property.h>

#include <sstream>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

BiBat_Next_Block_Property::BiBat_Next_Block_Property() :
    Data_Property(CEMI_Server_Object::PID_BIBAT_NEXTBLOCK)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void BiBat_Next_Block_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    next_block.fromData(first, first + 1);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> BiBat_Next_Block_Property::toData() const
{
    return next_block.toData();
}

std::string BiBat_Next_Block_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(next_block.unsigned_value);

    return oss.str();
}

}
