// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/07/02/5/DPT_5_010.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * BiBat Next Block
 *
 * @ingroup KNX_03_05_01_04_06_07
 */
class BiBat_Next_Block_Property :
    public Data_Property
{
public:
    BiBat_Next_Block_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** BiBat Next Block */
    DPT_Value_1_Ucount next_block{};
};

}
