// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

CEMI_Server_Object::CEMI_Server_Object() :
    System_Interface_Object(OT_CEMI_SERVER)
{
}

std::shared_ptr<Property> CEMI_Server_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_MEDIUM_TYPE:
        return std::make_shared<Medium_Type_Property>();
    case PID_COMM_MODE:
        return std::make_shared<Communication_Mode_Property>();
    case PID_MEDIUM_AVAILABILITY:
        return std::make_shared<Medium_Availability_Property>();
    case PID_ADD_INFO_TYPES:
        return std::make_shared<Additional_Information_Types_Property>();
    case PID_TIME_BASE:
        return std::make_shared<Time_Base_Property>();
    case PID_TRANSP_ENABLE:
        return std::make_shared<Link_Layer_Transparency_Control_Property>();
    case PID_CLIENT_SNA:
        return std::make_shared<Client_Subnetwork_Address_Property>();
    case PID_CLIENT_DEVICE_ADDRESS:
        return std::make_shared<Client_Device_Address_Property>();
    case PID_BIBAT_NEXTBLOCK:
        return std::make_shared<BiBat_Next_Block_Property>();
    case PID_RF_MODE_SELECT:
        return std::make_shared<RF_Communication_Mode_Select_Property>();
    case PID_RF_MODE_SUPPORT:
        return std::make_shared<Supported_RF_Communication_Modes_Property>();
    case PID_RF_FILTERING_MODE_SELECT:
        return std::make_shared<RF_Filtering_Mode_Selection_Server_Property>();
    case PID_RF_FILTERING_MODE_SUPPORT:
        return std::make_shared<Supported_RF_Filtering_Modes_Server_Property>();
    case PID_COMM_MODES_SUPPORTED:
        return std::make_shared<Supported_Communication_Modes_Property>();
    case PID_FILTERING_MODE_SUPPORT:
        return std::make_shared<Supported_Filtering_Modes_Property>();
    case PID_FILTERING_MODE_SELECT:
        return std::make_shared<Filtering_Mode_Selection_Property>();
    case PID_MAX_INTERFACE_APDU_LENGTH:
        return std::make_shared<Max_Interface_APDU_Length_Property>();
    case PID_MAX_LOCAL_APDU_LENGTH:
        return std::make_shared<Max_Local_APDU_Length_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Medium_Type_Property> CEMI_Server_Object::medium_type()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_MEDIUM_TYPE);
    if (!property) {
        property = std::make_shared<Medium_Type_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Medium_Type_Property>(property);
}

std::shared_ptr<Communication_Mode_Property> CEMI_Server_Object::communication_mode()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_COMM_MODE);
    if (!property) {
        property = std::make_shared<Communication_Mode_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Communication_Mode_Property>(property);
}

std::shared_ptr<Medium_Availability_Property> CEMI_Server_Object::medium_availability()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_MEDIUM_AVAILABILITY);
    if (!property) {
        property = std::make_shared<Medium_Availability_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Medium_Availability_Property>(property);
}

std::shared_ptr<Additional_Information_Types_Property> CEMI_Server_Object::additional_information_types()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_ADD_INFO_TYPES);
    if (!property) {
        property = std::make_shared<Additional_Information_Types_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Additional_Information_Types_Property>(property);
}

std::shared_ptr<Time_Base_Property> CEMI_Server_Object::time_base()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_TIME_BASE);
    if (!property) {
        property = std::make_shared<Time_Base_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Time_Base_Property>(property);
}

std::shared_ptr<Link_Layer_Transparency_Control_Property> CEMI_Server_Object::link_layer_transparency_control()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_TRANSP_ENABLE);
    if (!property) {
        property = std::make_shared<Link_Layer_Transparency_Control_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Link_Layer_Transparency_Control_Property>(property);
}

std::shared_ptr<Client_Subnetwork_Address_Property> CEMI_Server_Object::client_subnetwork_address()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_CLIENT_SNA);
    if (!property) {
        property = std::make_shared<Client_Subnetwork_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Client_Subnetwork_Address_Property>(property);
}

std::shared_ptr<Client_Device_Address_Property> CEMI_Server_Object::client_device_address()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_CLIENT_DEVICE_ADDRESS);
    if (!property) {
        property = std::make_shared<Client_Device_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Client_Device_Address_Property>(property);
}

std::shared_ptr<BiBat_Next_Block_Property> CEMI_Server_Object::bibat_next_block()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_BIBAT_NEXTBLOCK);
    if (!property) {
        property = std::make_shared<BiBat_Next_Block_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<BiBat_Next_Block_Property>(property);
}

std::shared_ptr<RF_Communication_Mode_Select_Property> CEMI_Server_Object::rf_communication_mode_select()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_RF_MODE_SELECT);
    if (!property) {
        property = std::make_shared<RF_Communication_Mode_Select_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Communication_Mode_Select_Property>(property);
}

std::shared_ptr<Supported_RF_Communication_Modes_Property> CEMI_Server_Object::supported_rf_communication_modes()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_RF_MODE_SUPPORT);
    if (!property) {
        property = std::make_shared<Supported_RF_Communication_Modes_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Supported_RF_Communication_Modes_Property>(property);
}

std::shared_ptr<RF_Filtering_Mode_Selection_Server_Property> CEMI_Server_Object::rf_filtering_mode_selection()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_RF_FILTERING_MODE_SELECT);
    if (!property) {
        property = std::make_shared<RF_Filtering_Mode_Selection_Server_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Filtering_Mode_Selection_Server_Property>(property);
}

std::shared_ptr<Supported_RF_Filtering_Modes_Server_Property> CEMI_Server_Object::supported_rf_filtering_modes()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_RF_FILTERING_MODE_SUPPORT);
    if (!property) {
        property = std::make_shared<Supported_RF_Filtering_Modes_Server_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Supported_RF_Filtering_Modes_Server_Property>(property);
}

std::shared_ptr<Supported_Communication_Modes_Property> CEMI_Server_Object::supported_communication_modes()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_COMM_MODES_SUPPORTED);
    if (!property) {
        property = std::make_shared<Supported_Communication_Modes_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Supported_Communication_Modes_Property>(property);
}

std::shared_ptr<Supported_Filtering_Modes_Property> CEMI_Server_Object::supported_filtering_modes()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_FILTERING_MODE_SUPPORT);
    if (!property) {
        property = std::make_shared<Supported_Filtering_Modes_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Supported_Filtering_Modes_Property>(property);
}

std::shared_ptr<Filtering_Mode_Selection_Property> CEMI_Server_Object::filtering_mode_selection()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_FILTERING_MODE_SELECT);
    if (!property) {
        property = std::make_shared<Filtering_Mode_Selection_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Filtering_Mode_Selection_Property>(property);
}

std::shared_ptr<Max_Interface_APDU_Length_Property> CEMI_Server_Object::max_interface_apdu_length()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_MAX_INTERFACE_APDU_LENGTH);
    if (!property) {
        property = std::make_shared<Max_Interface_APDU_Length_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Max_Interface_APDU_Length_Property>(property);
}

std::shared_ptr<Max_Local_APDU_Length_Property> CEMI_Server_Object::max_local_apdu_length()
{
    std::shared_ptr<Property> property = property_by_id(CEMI_Server_Object::PID_MAX_LOCAL_APDU_LENGTH);
    if (!property) {
        property = std::make_shared<Max_Local_APDU_Length_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Max_Local_APDU_Length_Property>(property);
}

}
