// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/08_CEMI_Server/Additional_Information_Types_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/BiBat_Next_Block_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Client_Device_Address_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Client_Subnetwork_Address_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Communication_Mode_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Filtering_Mode_Selection_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Link_Layer_Transparency_Control_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Max_Interface_APDU_Length_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Max_Local_APDU_Length_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Medium_Availability_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Medium_Type_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/RF_Communication_Mode_Select_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/RF_Filtering_Mode_Selection_Server_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Supported_Communication_Modes_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Supported_Filtering_Modes_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Supported_RF_Communication_Modes_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Supported_RF_Filtering_Modes_Server_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Time_Base_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * cEMI Server Object (Object Type 8)
 *
 * @ingroup KNX_03_05_01_04_06
 * @ingroup KNX_03_06_03_04_02_02_04
 * @ingroup KNX_AN160_02_03_03
 */
class KNX_EXPORT CEMI_Server_Object :
    public System_Interface_Object
{
public:
    explicit CEMI_Server_Object();

    /**
     * cEMI Server Object (Object Type 8)
     *
     * @ingroup KNX_03_05_01_04_06
     * @ingroup KNX_03_06_03_04_02_02_04
     * @ingroup KNX_AN185_02_03_01_01
     */
    enum : Property_Id {
        PID_MEDIUM_TYPE = 51,
        PID_COMM_MODE = 52,
        PID_MEDIUM_AVAILABILITY = 53,
        PID_ADD_INFO_TYPES = 54,
        PID_TIME_BASE = 55,
        PID_TRANSP_ENABLE = 56,
        PID_CLIENT_SNA = 57,
        PID_CLIENT_DEVICE_ADDRESS = 58,
        PID_BIBAT_NEXTBLOCK = 59,
        PID_RF_MODE_SELECT = 60,
        PID_RF_MODE_SUPPORT = 61,
        PID_RF_FILTERING_MODE_SELECT = 62, // Server
        PID_RF_FILTERING_MODE_SUPPORT = 63, // Server
        PID_COMM_MODES_SUPPORTED = 64,
        PID_FILTERING_MODE_SUPPORT = 65,
        PID_FILTERING_MODE_SELECT = 66,
        PID_MAX_INTERFACE_APDU_LENGTH = 68,
        PID_MAX_LOCAL_APDU_LENGTH = 69
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<Medium_Type_Property> medium_type();
    std::shared_ptr<Communication_Mode_Property> communication_mode();
    std::shared_ptr<Medium_Availability_Property> medium_availability();
    std::shared_ptr<Additional_Information_Types_Property> additional_information_types();
    std::shared_ptr<Time_Base_Property> time_base();
    std::shared_ptr<Link_Layer_Transparency_Control_Property> link_layer_transparency_control();
    std::shared_ptr<Client_Subnetwork_Address_Property> client_subnetwork_address();
    std::shared_ptr<Client_Device_Address_Property> client_device_address();
    std::shared_ptr<BiBat_Next_Block_Property> bibat_next_block();
    std::shared_ptr<RF_Communication_Mode_Select_Property> rf_communication_mode_select();
    std::shared_ptr<Supported_RF_Communication_Modes_Property> supported_rf_communication_modes();
    std::shared_ptr<RF_Filtering_Mode_Selection_Server_Property> rf_filtering_mode_selection();
    std::shared_ptr<Supported_RF_Filtering_Modes_Server_Property> supported_rf_filtering_modes();
    std::shared_ptr<Supported_Communication_Modes_Property> supported_communication_modes();
    std::shared_ptr<Supported_Filtering_Modes_Property> supported_filtering_modes();
    std::shared_ptr<Filtering_Mode_Selection_Property> filtering_mode_selection();
    std::shared_ptr<Max_Interface_APDU_Length_Property> max_interface_apdu_length();
    std::shared_ptr<Max_Local_APDU_Length_Property> max_local_apdu_length();
};

}
