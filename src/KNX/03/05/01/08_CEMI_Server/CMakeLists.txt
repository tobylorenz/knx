# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/Additional_Information_Types_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/BiBat_Next_Block_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_Server_Object.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Client_Device_Address_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Client_Subnetwork_Address_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Communication_Mode_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Filtering_Mode_Selection_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Layer_Transparency_Control_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Max_Interface_APDU_Length_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Max_Local_APDU_Length_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Medium_Availability_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Medium_Type_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/RF_Communication_Mode_Select_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/RF_Filtering_Mode_Selection_Server_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_Communication_Modes_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_Filtering_Modes_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_RF_Communication_Modes_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_RF_Filtering_Modes_Server_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Time_Base_Property.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/Additional_Information_Types_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/BiBat_Next_Block_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_Server_Object.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Client_Device_Address_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Client_Subnetwork_Address_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Communication_Mode_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Filtering_Mode_Selection_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Layer_Transparency_Control_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Max_Interface_APDU_Length_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Max_Local_APDU_Length_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Medium_Availability_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Medium_Type_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/RF_Communication_Mode_Select_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/RF_Filtering_Mode_Selection_Server_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_Communication_Modes_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_Filtering_Modes_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_RF_Communication_Modes_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_RF_Filtering_Modes_Server_Property.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/Time_Base_Property.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/Additional_Information_Types_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/BiBat_Next_Block_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_Server_Object.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Client_Device_Address_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Client_Subnetwork_Address_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Communication_Mode_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Filtering_Mode_Selection_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Link_Layer_Transparency_Control_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Max_Interface_APDU_Length_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Max_Local_APDU_Length_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Medium_Availability_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Medium_Type_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/RF_Communication_Mode_Select_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/RF_Filtering_Mode_Selection_Server_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_Communication_Modes_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_Filtering_Modes_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_RF_Communication_Modes_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Supported_RF_Filtering_Modes_Server_Property.h
        ${CMAKE_CURRENT_SOURCE_DIR}/Time_Base_Property.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/05/01/08_CEMI_Server)
