// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Client_Device_Address_Property.h>

#include <sstream>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Client_Device_Address_Property::Client_Device_Address_Property() :
    Data_Property(CEMI_Server_Object::PID_CLIENT_DEVICE_ADDRESS)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Client_Device_Address_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    client_device_address = *first++;

    assert(first == last);
}

std::vector<uint8_t> Client_Device_Address_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(client_device_address);

    return data;
}

std::string Client_Device_Address_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(client_device_address);

    return oss.str();
}

}
