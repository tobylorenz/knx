// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Client_Subnetwork_Address_Property.h>

#include <sstream>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Client_Subnetwork_Address_Property::Client_Subnetwork_Address_Property() :
    Data_Property(CEMI_Server_Object::PID_CLIENT_SNA)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Client_Subnetwork_Address_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    client_subnetwork_address = *first++;

    assert(first == last);
}

std::vector<uint8_t> Client_Subnetwork_Address_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(client_subnetwork_address);

    return data;
}

std::string Client_Subnetwork_Address_Property::text() const
{
    Individual_Address address;
    address.set_subnetwork_address(client_subnetwork_address);

    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(address.area_address())
        << "."
        << std::dec << static_cast<uint16_t>(address.line_address());

    return oss.str();
}

}
