// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Communication_Mode_Property.h>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Communication_Mode_Property::Communication_Mode_Property() :
    Data_Property(CEMI_Server_Object::PID_COMM_MODE)
{
    description.property_datatype = PDT_ENUM8;
}

void Communication_Mode_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    communication_mode.fromData(first, first + 1);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Communication_Mode_Property::toData() const
{
    return communication_mode.toData();
}

std::string Communication_Mode_Property::text() const
{
    return communication_mode.text();
}

void Communication_Mode_Property::write(const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data, std::optional<CEMI_Error_Code> & error_code)
{
    // @todo maybe we can move some things back into CEMI_Management_Server::M_PropWrite_req

    if (nr_of_elem != 1) {
        error_code = CEMI_Error_Code::Unspecified_Error;
        return;
    }

    if (start_index != 1) {
        error_code = CEMI_Error_Code::Unspecified_Error;
        return;
    }

    const uint8_t property_datatype_size = size(description.property_datatype);
    if (data.size() != property_datatype_size) {
        error_code = CEMI_Error_Code::Type_Conflict;
        return;
    }

    const uint8_t new_communication_mode = data[0];
    switch (new_communication_mode) {
    case 0x00: // Data Link Layer
    case 0x01: // Data Link Layer Busmonitor
    case 0x02: // Data Link Layer Raw Frames
    case 0x06: // cEMI Transport Layer
    case 0xFF: // No Layer
        communication_mode.field1 = new_communication_mode;
        error_code = std::nullopt;
        break;
    default:
        error_code = CEMI_Error_Code::Out_of_Range;
        break;
    }
}

}
