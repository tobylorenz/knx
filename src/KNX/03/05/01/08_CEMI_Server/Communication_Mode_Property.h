// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/07/02/20/DPT_20_1000.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Communication Mode
 *
 * @ingroup KNX_03_05_01_04_06_02
 * @ingroup KNX_03_06_03_04_02_02_04_02
 */
class Communication_Mode_Property :
    public Data_Property
{
public:
    Communication_Mode_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;
    void write(const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data, std::optional<CEMI_Error_Code> & error_code) override;

    /** Communication Mode */
    DPT_CommMode communication_mode{};
};

}
