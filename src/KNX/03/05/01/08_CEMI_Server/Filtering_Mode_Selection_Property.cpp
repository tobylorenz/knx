// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Filtering_Mode_Selection_Property.h>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Filtering_Mode_Selection_Property::Filtering_Mode_Selection_Property() :
    Data_Property(CEMI_Server_Object::PID_FILTERING_MODE_SELECT)
{
    description.property_datatype = PDT_ENUM8; // alt: PDT_UNSIGNED_CHAR
}

void Filtering_Mode_Selection_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    rf_filtering_mode_selection = *first++;

    assert(first == last);
}

std::vector<uint8_t> Filtering_Mode_Selection_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(rf_filtering_mode_selection);

    return data;
}

std::string Filtering_Mode_Selection_Property::text() const
{
    // @todo Filtering_Mode_Selection_Property::text
    return "";
}

}
