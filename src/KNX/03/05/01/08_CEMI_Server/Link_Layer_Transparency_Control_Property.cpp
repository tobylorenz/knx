// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Link_Layer_Transparency_Control_Property.h>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Link_Layer_Transparency_Control_Property::Link_Layer_Transparency_Control_Property() :
    Data_Property(CEMI_Server_Object::PID_TRANSP_ENABLE)
{
    description.property_datatype = PDT_BINARY_INFORMATION;
}

void Link_Layer_Transparency_Control_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    full_transparency.fromData(first, first + 1);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Link_Layer_Transparency_Control_Property::toData() const
{
    return full_transparency.toData();
}

std::string Link_Layer_Transparency_Control_Property::text() const
{
    return full_transparency.b ? "Full Transparency" : "Normal Transparency";
}

}
