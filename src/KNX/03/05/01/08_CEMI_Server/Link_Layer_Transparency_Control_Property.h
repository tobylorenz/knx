// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/07/02/1/DPT_1_003.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * cEMI Server Link Layer Transparency Control
 *
 * @ingroup KNX_03_05_01_04_06_06
 */
class Link_Layer_Transparency_Control_Property :
    public Data_Property
{
public:
    Link_Layer_Transparency_Control_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    DPT_Enable full_transparency{};
};

}
