// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Max_Interface_APDU_Length_Property.h>

#include <sstream>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Max_Interface_APDU_Length_Property::Max_Interface_APDU_Length_Property() :
    Data_Property(CEMI_Server_Object::PID_MAX_LOCAL_APDU_LENGTH)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Max_Interface_APDU_Length_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    max_apdu_length = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Max_Interface_APDU_Length_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(max_apdu_length >> 8);
    data.push_back(max_apdu_length & 0xff);

    return data;
}

std::string Max_Interface_APDU_Length_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << max_apdu_length;

    return oss.str();
}

}
