// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Medium_Type_Property.h>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Medium_Type_Property::Medium_Type_Property() :
    Data_Property(CEMI_Server_Object::PID_MEDIUM_TYPE)
{
    description.property_datatype = PDT_BITSET16;
}

void Medium_Type_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    medium_type.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> Medium_Type_Property::toData() const
{
    return medium_type.toData();
}

std::string Medium_Type_Property::text() const
{
    return medium_type.text();
}

}
