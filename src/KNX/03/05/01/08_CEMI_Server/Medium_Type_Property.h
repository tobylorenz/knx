// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/07/02/22/DPT_22_1000.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Medium Type
 *
 * @ingroup KNX_03_05_01_04_06_01
 */
class Medium_Type_Property :
    public Data_Property
{
public:
    Medium_Type_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Medium Type */
    DPT_Media medium_type{};
};

}
