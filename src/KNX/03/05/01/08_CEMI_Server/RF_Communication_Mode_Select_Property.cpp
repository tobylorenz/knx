// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/RF_Communication_Mode_Select_Property.h>

#include <sstream>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Communication_Mode_Select_Property::RF_Communication_Mode_Select_Property() :
    Data_Property(CEMI_Server_Object::PID_RF_MODE_SELECT)
{
    description.property_datatype = PDT_ENUM8;
}

void RF_Communication_Mode_Select_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    rf_communication_mode = *first++;

    assert(first == last);
}

std::vector<uint8_t> RF_Communication_Mode_Select_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(rf_communication_mode);

    return data;
}

std::string RF_Communication_Mode_Select_Property::text() const
{
    std::ostringstream oss;

    switch (rf_communication_mode) {
    case 0:
        oss << "asynchronous";
        break;
    case 1:
        oss << "asynchronous + BiBat Master";
        break;
    case 2:
        oss << "asynchronous + BiBat Slave";
        break;
    }

    return oss.str();
}

}
