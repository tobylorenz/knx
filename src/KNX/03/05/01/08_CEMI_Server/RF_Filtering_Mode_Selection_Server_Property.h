// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Filtering Mode Selection
 *
 * @ingroup KNX_03_05_01_04_06_10
 */
class RF_Filtering_Mode_Selection_Server_Property :
    public Data_Property
{
public:
    RF_Filtering_Mode_Selection_Server_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** RF Filtering Mode Selection */
    uint8_t rf_filtering_mode_selection{};
};

}
