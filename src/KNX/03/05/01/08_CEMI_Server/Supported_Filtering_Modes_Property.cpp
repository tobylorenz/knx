// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Supported_Filtering_Modes_Property.h>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Supported_Filtering_Modes_Property::Supported_Filtering_Modes_Property() :
    Data_Property(CEMI_Server_Object::PID_FILTERING_MODE_SUPPORT)
{
    description.property_datatype = PDT_UNSIGNED_CHAR; // @todo datatype?
}

void Supported_Filtering_Modes_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Supported_Filtering_Modes_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Supported_Filtering_Modes_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Supported_Filtering_Modes_Property::toData

    return data;
}

std::string Supported_Filtering_Modes_Property::text() const
{
    // @todo Supported_Filtering_Modes_Property::text
    return "";
}

}
