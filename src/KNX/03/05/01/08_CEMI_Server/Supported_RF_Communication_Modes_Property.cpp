// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Supported_RF_Communication_Modes_Property.h>

#include <sstream>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Supported_RF_Communication_Modes_Property::Supported_RF_Communication_Modes_Property() :
    Data_Property(CEMI_Server_Object::PID_RF_MODE_SUPPORT)
{
    description.property_datatype = PDT_BITSET8; // alt.: PDT_GENERIC_01
}

void Supported_RF_Communication_Modes_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    asynchronous = (*first >> 0) & 0x01;
    bibat_master = (*first >> 1) & 0x01;
    bibat_slave = (*first >> 2) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Supported_RF_Communication_Modes_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (asynchronous << 0) |
        (bibat_master << 1) |
        (bibat_slave << 2));

    return data;
}

std::string Supported_RF_Communication_Modes_Property::text() const
{
    std::ostringstream oss;

    oss << "Asynchronous: " << (asynchronous ? "true" : "false")
        << ", BiBat Master: " << (bibat_master ? "true" : "false")
        << ", BiBat Slave: " << (bibat_slave ? "true" : "false");

    return oss.str();
}

}
