// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Supported RF Communication Modes
 *
 * @ingroup KNX_03_05_01_04_06_09
 */
class Supported_RF_Communication_Modes_Property :
    public Data_Property
{
public:
    Supported_RF_Communication_Modes_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** asynchronous mode support */
    bool asynchronous{true};

    /** BiBat Master mode supported */
    bool bibat_master{};

    /** BiBat Slave mode supported */
    bool bibat_slave{};
};

}
