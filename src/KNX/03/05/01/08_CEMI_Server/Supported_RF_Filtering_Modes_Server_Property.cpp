// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Supported_RF_Filtering_Modes_Server_Property.h>

#include <sstream>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Supported_RF_Filtering_Modes_Server_Property::Supported_RF_Filtering_Modes_Server_Property() :
    Data_Property(CEMI_Server_Object::PID_RF_FILTERING_MODE_SUPPORT)
{
    description.property_datatype = PDT_BITSET8;
}

void Supported_RF_Filtering_Modes_Server_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    filter_by_DoA_SerialNr = (*first >> 2) & 0x01;
    filter_by_SerialNr = (*first >> 1) & 0x01;
    filter_by_DoA = (*first >> 0) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Supported_RF_Filtering_Modes_Server_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (filter_by_DoA_SerialNr << 2) |
        (filter_by_SerialNr << 1) |
        (filter_by_DoA << 0));

    return data;
}

std::string Supported_RF_Filtering_Modes_Server_Property::text() const
{
    std::ostringstream oss;

    oss << "Filter by DoA: " << (filter_by_DoA ? "true" : "false")
        << ", Filter by SerialNr: " << (filter_by_SerialNr ? "true" : "false")
        << ", Filter by DoA & SerialNr: " << (filter_by_DoA_SerialNr ? "true" : "false");

    return oss.str();
}

}
