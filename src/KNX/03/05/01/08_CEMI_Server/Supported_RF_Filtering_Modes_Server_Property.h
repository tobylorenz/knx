// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Supported RF Filtering Modes
 *
 * @ingroup KNX_03_05_01_04_06_11
 */
class Supported_RF_Filtering_Modes_Server_Property :
    public Data_Property
{
public:
    Supported_RF_Filtering_Modes_Server_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Filter by DoA & SerialNr */
    bool filter_by_DoA_SerialNr{};

    /** Filter by SerialNr */
    bool filter_by_SerialNr{};

    /** Filter by DoA */
    bool filter_by_DoA{};
};

}
