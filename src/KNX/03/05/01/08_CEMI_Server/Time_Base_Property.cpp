// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/08_CEMI_Server/Time_Base_Property.h>

#include <sstream>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Time_Base_Property::Time_Base_Property() :
    Data_Property(CEMI_Server_Object::PID_TIME_BASE)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Time_Base_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    time_base.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> Time_Base_Property::toData() const
{
    return time_base.toData();
}

std::string Time_Base_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << time_base.unsigned_value << " ns";

    return oss.str();
}

}
