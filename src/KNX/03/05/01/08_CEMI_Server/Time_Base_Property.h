// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/7/DPT_7_001.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Time Base
 *
 * @ingroup KNX_03_05_01_04_06_05
 */
class Time_Base_Property :
    public Data_Property
{
public:
    Time_Base_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Time Base */
    DPT_Value_2_Ucount time_base{};
};

}
