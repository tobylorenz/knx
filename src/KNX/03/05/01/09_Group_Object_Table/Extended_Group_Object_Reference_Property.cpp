// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/09_Group_Object_Table/Extended_Group_Object_Reference_Property.h>

#include <sstream>

#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Extended_Group_Object_Reference_Property::Extended_Group_Object_Reference_Property() :
    Data_Property(Group_Object_Table::PID_EXT_GRPOBJREFERENCE)
{
    description.property_datatype = PDT_GENERIC_08; // @note not specified
}

void Extended_Group_Object_Reference_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    object_type = (*first++ << 8) | (*first++);
    object_instance = *first++;
    property_identifier = *first++;
    start_index = (*first++ << 8) | (*first++);
    bit_offset = *first++;
    conversion = *first++;

    assert(first == last);
}

std::vector<uint8_t> Extended_Group_Object_Reference_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(object_type >> 8);
    data.push_back(object_type & 0xff);
    data.push_back(object_instance);
    data.push_back(property_identifier);
    data.push_back(start_index >> 8);
    data.push_back(start_index & 0xff);
    data.push_back(bit_offset);
    data.push_back(conversion);

    return data;
}

std::string Extended_Group_Object_Reference_Property::text() const
{
    std::ostringstream oss;

    oss << "Object Type: " << std::dec << object_type
        << ", Object Instance: " << std::dec << static_cast<uint16_t>(object_instance)
        << ", Property Identifier: " << std::dec << static_cast<uint16_t>(property_identifier)
        << ", Start Index: " << std::dec << start_index
        << ", Bit Offset: " << std::dec << static_cast<uint16_t>(bit_offset)
        << ", Conversion: " << std::dec << static_cast<uint16_t>(conversion);

    return oss.str();
}

}
