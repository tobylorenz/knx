// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Object_Instance.h>
#include <KNX/03/03/07/Object_Type.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Extended Group Object Reference Property
 *
 * @ingroup KNX_03_05_01_04_12_04_02_05
 */
class Extended_Group_Object_Reference_Property :
    public Data_Property
{
public:
    Extended_Group_Object_Reference_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Object Type */
    Object_Type object_type{};

    /** Object Instance */
    Object_Instance object_instance{};

    /** Property Identifier */
    Property_Id property_identifier{};

    /** Start Index */
    uint12_t start_index{};

    /** Bit Offset */
    uint8_t bit_offset{};

    /** Conversion */
    uint8_t conversion{};
};

}
