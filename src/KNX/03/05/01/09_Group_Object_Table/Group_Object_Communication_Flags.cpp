// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Communication_Flags.h>

namespace KNX {

Group_Object_Communication_Flags::Group_Object_Communication_Flags(const uint8_t communication_flags) :
    update((communication_flags >> 3) & 0x01),
    data_request((communication_flags >> 2) & 0x01),
    transmission_status(static_cast<Transmission_Status>((communication_flags >> 0) & 0x03))
{
}

Group_Object_Communication_Flags & Group_Object_Communication_Flags::operator=(const uint8_t & communication_flags)
{
    update = (communication_flags >> 3) & 0x01;
    data_request = (communication_flags >> 2) & 0x01;
    transmission_status = static_cast<Transmission_Status>((communication_flags >> 0) & 0x03);

    return *this;
}

Group_Object_Communication_Flags::operator uint8_t() const
{
    return
        (static_cast<uint8_t>(update) << 3) |
        (static_cast<uint8_t>(data_request) << 2) |
        (static_cast<uint8_t>(transmission_status) << 0);
}

}
