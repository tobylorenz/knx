// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Group Object communication flags / RAM flags
 *
 * @ingroup KNX_03_04_01_03_02
 * @ingroup KNX_03_05_01_04_12_02_01_01
 * @ingroup KNX_03_06_01_02_01_01
 * @ingroup KNX_03_06_03_03_03_08_03
 * @ingroup KNX_03_06_03_03_03_08_05
 * @ingroup KNX_03_06_03_03_03_08_07
 */
class Group_Object_Communication_Flags
{
public:
    Group_Object_Communication_Flags() = default;
    explicit Group_Object_Communication_Flags(const uint8_t communication_flags);
    Group_Object_Communication_Flags & operator=(const uint8_t & communication_flags);
    operator uint8_t() const;

    /* RAM flags */

    /**
     * update flag (bit 3)
     *
     * The GO value has been updated (i.e. a Value_Write or a Value_Response has successfully
     * been received).
     *
     * - 0: not updated
     * - 1: the value of the group object has been from the bus by an A_GroupValue_Write.ind or an A_GroupValue_Read.res.
     */
    bool update{false};

    /**
     * data request flag (bit 2)
     *
     * The GO will send/has sent a Value_Read message and (in the latter case) is waiting for
     * the Value_Response.
     *
     * - 0: idle/response
     * - 1: data-request
     */
    bool data_request{false};

    /**
     * transmission status (bit 1..0)
     *
     * Reflects the status of a Value_Read or Value_Write message sent by the GO.
     */
    enum class Transmission_Status : uint8_t {
        /** idle/OK */
        idle_ok = 0,

        /** idle/error */
        idle_error = 1,

        /** transmitting */
        transmitting = 2,

        /** transmit request */
        transmit_request = 3
    };

    /** @copydoc Transmission_Status */
    Transmission_Status transmission_status{Transmission_Status::idle_ok};
};

}
