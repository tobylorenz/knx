// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Config.h>

namespace KNX {

Group_Object_Config::Group_Object_Config(const uint8_t group_object_config) :
    update_enable((group_object_config >> 7) & 0x01),
    transmit_enable((group_object_config >> 6) & 0x01),
    read_on_init((group_object_config >> 5) & 0x01),
    write_enable((group_object_config >> 4) & 0x01),
    read_enable((group_object_config >> 3) & 0x01),
    communication_enable((group_object_config >> 2) & 0x01),
    transmission_priority(static_cast<Priority>((group_object_config >> 0) & 0x03))
{
}

Group_Object_Config & Group_Object_Config::operator=(const uint8_t & group_object_config)
{
    update_enable = (group_object_config >> 7) & 0x01;
    transmit_enable = (group_object_config >> 6) & 0x01;
    read_on_init = (group_object_config >> 5) & 0x01;
    write_enable = (group_object_config >> 4) & 0x01;
    read_enable = (group_object_config >> 3) & 0x01;
    communication_enable = (group_object_config >> 2) & 0x01;
    transmission_priority = static_cast<Priority>((group_object_config >> 0) & 0x03);

    return *this;
}

Group_Object_Config::operator uint8_t() const
{
    return
        (static_cast<uint8_t>(update_enable) << 7) |
        (static_cast<uint8_t>(transmit_enable) << 6) |
        (static_cast<uint8_t>(read_on_init) << 5) |
        (static_cast<uint8_t>(write_enable) << 4) |
        (static_cast<uint8_t>(read_enable) << 3) |
        (static_cast<uint8_t>(communication_enable) << 2) |
        (static_cast<uint8_t>(transmission_priority) << 0);
}

}
