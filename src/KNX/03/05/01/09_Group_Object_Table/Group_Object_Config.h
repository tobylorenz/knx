// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Priority.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Group Object Config/EEPROM Flags
 *
 * @ingroup KNX_03_04_01_03_02
 * @ingroup KNX_03_05_01_04_12_02_01_02
 * @ingroup KNX_03_05_01_04_12_03_01
 * @ingroup KNX_03_06_03_03_03_08_05
 */
class KNX_EXPORT Group_Object_Config
{
public:
    Group_Object_Config() = default;
    explicit Group_Object_Config(const uint8_t group_object_config);
    Group_Object_Config & operator=(const uint8_t & group_object_config);
    operator uint8_t() const;

    /**
     * (Read Response) Update Enable (bit 7)
     *
     * Update is used as a write if enable. (Responses from other
     * devices leading to updates of Datapoints in own device)
     *
     * specifies if the Group Object Value is updated if an
     * A_GroupValue_Read.res service primitive is received for that Group
     * Object.
     *
     * If enabled, the GO value is updated on arrival of a Value_Response after a
     * Value_Read was sent ("communication" and "transmit" shall be enabled).
     *
     * ETS: UpdateFlag
     *
     * - 0: disabled
     *      the value of the Group Object is not updated. The update-flag in the
     *      RAM-Flags-Table for that Group Object is not set.
     * - 1: enabled
     *      the value of the Group Object is updated by the value contained in the
     *      A_GroupValue_Read.res service primitive. The update-flag in the
     *      RAM-Flags-Table for that Group Object is set.
     */
    bool update_enable{true};

    /**
     * Transmit Enable (bit 6)
     *
     * Enables a read/write to bus
     *
     * specifies the behaviour of the Group Object server when the transmit
     * request flag is set (by the user application).
     *
     * If enabled, the GO is able to send a Value_Read or a Value_Write message
     * ("communication" shall also be enabled)
     *
     * ETS: TransmitFlag
     *
     * - 0: disabled
     *      the transmit-request flag is ignored. No A_GroupValue_Write.req or
     *      A_GroupValue_Read.req is generated.
     * - 1: enabled
     *      if the transmit request flag is set, it is evaluated and the appropriate
     *      Application Layer service primitive is generated, under the condition
     *      that the communication flag is set.
     */
    bool transmit_enable{true};

    union {
        /**
         * Segment Selector Type / Memory Segment / Memory Type (bit 5)
         *
         * device internal memory segment selector
         *
         * specifies the start address of the segment into which the data pointer
         * points. The value of the data pointer has to be incremented with the
         * value given below, in function of the segment selector, to obtain the
         * address of the group object value.
         * This segment selector allows locating the group object value in RAM or
         * in EEPROM.
         *
         * - 0: 00h (ROM)
         * - 1: 100h (EEPROM)
         */
        bool segment_selector_type;

        /**
         * Value Read on initialisation
         *
         * This shall specify whether or not the RAM-flags of the Group Object shall be set after reset of the
         * application and consequently an A_GroupValue_Read shall be issued to update this Group Object value
         *
         * As this feature has effect on busload especially after a reset of a complete installation this feature should
         * be used very carefully (e.g. it should not be set by default in an average application).
         *
         * ETS: ReadOnInitFlag
         *
         * - 0: disabled
         *      The Group Object value shall not be updated after reset.
         * - 1: enabled
         *      The Group Object value shall be updated after reset.
         */
        bool read_on_init{true};
    };

    /**
     * Write Enable (bit 4)
     *
     * Enables a write from bus
     *
     * specifies the behaviour of the Group Object server for this group object when an
     * A_GroupValue_Write.ind is received for this group object.
     *
     * If enabled, the GO is able to receive a Value_Write message and update its value accordingly
     * ("communication" shall also be enabled).
     *
     * ETS: WriteFlags
     *
     * - 0: disabled
     *      the A_GroupValue_Write.ind service is ignored. no update of the group
     *      object value.
     *
     * - 1: enabled
     *      the group object server updates the group object value with the value
     *      contained in the A_GroupValue_Write.ind service, under the condition
     *      that also the communication enable flag is set.
     */
    bool write_enable{true};

    /**
     * Read Enable (bit 3)
     *
     * Enables a read from bus
     *
     * specifies the behaviour of the Group Object server for this group object when an
     * A_GroupValue_Read.ind is received for this group object.
     *
     * If enabled, the GO value can be read by a Value_Read message. In order to ensure the
     * generation of a Value_Response, "communication" shall also be enabled.
     *
     * ETS: ReadFlag
     *
     * - 0: disabled
     *      the A_GroupValue_Read.ind service is ignored. no response is
     *      transmitted.
     * - 1: enabled
     *      the group object server answers to an A_GroupValue_Read.ind to this
     *      group object with an A_GroupValue_Read.res containing this group
     *      objects value, under the condition that also the communication enable
     *      flag is set.
     *      It is possible to read the value from another internal object.
     */
    bool read_enable{true};

    /**
     * Communication Enable (bit 2)
     *
     * Enables the communication to the bus
     *
     * enables or disables communication via this group object
     *
     * If enabled, the GO is able to send and receive messages.
     *
     * ETS: CommunicationFlag
     *
     * - 0: disabled
     *      messages for this group object are not handled
     * - 1: enabled
     *      messages for this group object are handled, in function of the other
     *      flags
     */
    bool communication_enable{true};

    /**
     * Transmission Priority (bit 1..0)
     *
     * Sets the transmission priority for a sending frame
     *
     * specifies the transmission-priority that is used for transmitting frames
     * from this group object.
     *
     * the priority of the Value Read or Write frame sent by the
     * group object.
     *
     * ETS: Priority
     */
    Priority transmission_priority{Priority::low};
};

using EEPROM_Flags = Group_Object_Config;

}
