// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Config.h>
#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Type.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Group Object Description/Descriptor
 *
 * @ingroup KNX_03_04_01_03_02
 * @ingroup KNX_03_05_01_04_12_02_01_02
 */
class KNX_EXPORT Group_Object_Descriptor
{
public:
    /** Data Pointer */
    std::shared_ptr<std::vector<uint8_t>> data{};

    /** Group Object Config */
    Group_Object_Config config;

    /** Group Object Type/Size */
    Group_Object_Type type;

    /**
     * datapoint type / value type
     *
     * ETS: DatapointType
     */
    std::shared_ptr<Datapoint_Type> datapoint_type;
};

using Group_Object_Description = Group_Object_Descriptor;

}
