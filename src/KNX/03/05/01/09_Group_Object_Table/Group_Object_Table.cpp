// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Group_Object_Table::Group_Object_Table() :
    System_Interface_Object(OT_GROUP_OBJECT_TABLE),
    load_state_machine(std::make_shared<Load_State_Machine>())
{
}

std::shared_ptr<Property> Group_Object_Table::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_GRP_OBJTABLE:
        return std::make_shared<Group_Object_Table_Property>();
    case PID_EXT_GRPOBJREFERENCE:
        return std::make_shared<Extended_Group_Object_Reference_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Group_Object_Table_Property> Group_Object_Table::group_object_table()
{
    std::shared_ptr<Property> property = property_by_id(Group_Object_Table::PID_GRP_OBJTABLE);
    if (!property) {
        property = std::make_shared<Group_Object_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Group_Object_Table_Property>(property);
}

std::shared_ptr<Extended_Group_Object_Reference_Property> Group_Object_Table::extended_group_object_reference()
{
    std::shared_ptr<Property> property = property_by_id(Group_Object_Table::PID_EXT_GRPOBJREFERENCE);
    if (!property) {
        property = std::make_shared<Extended_Group_Object_Reference_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Extended_Group_Object_Reference_Property>(property);
}

Group_Object_Number Group_Object_Table::group_object_number(const ASAP_Group asap) const
{
    /* find location of ASAP */
    std::map<ASAP_Group, Group_Object_Descriptor>::const_iterator it = group_object_descriptors.find(asap);

    /* group object number is position */
    const Group_Object_Number group_object_number(std::distance(std::cbegin(group_object_descriptors), it));
    assert(group_object_number < group_object_descriptors.size());

    return group_object_number;
}

ASAP_Group Group_Object_Table::asap(const Group_Object_Number group_object_number) const
{
    assert(group_object_number < group_object_descriptors.size());

    /* find location */
    std::map<ASAP_Group, Group_Object_Descriptor>::const_iterator it = std::cbegin(group_object_descriptors);
    for (auto i = 0; i < group_object_number; ++i) {
        ++it;
    }
    return it->first;
}

}
