// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <map>

#include <KNX/03/03/07/ASAP.h>
#include <KNX/03/03/07/Group_Object_Number.h>
#include <KNX/03/05/01/09_Group_Object_Table/Extended_Group_Object_Reference_Property.h>
#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Communication_Flags.h>
#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Descriptor.h>
#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/03/05/01/Unknown_Property.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Group Object Table
 *
 * @ingroup KNX_03_05_01_04_12
 */
class KNX_EXPORT Group_Object_Table :
    public System_Interface_Object
{
public:
    explicit Group_Object_Table();

    enum : Property_Id {
        PID_GRP_OBJTABLE = 51,
        PID_EXT_GRPOBJREFERENCE = 52
    };

    /** Load State Machine */
    std::shared_ptr<Load_State_Machine> load_state_machine{};

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    /** RAM-Flags-Table Pointer */
    std::map<ASAP_Group, Group_Object_Communication_Flags> communication_flags{};

    /** Group Object Descriptor (EEPROM) */
    std::map<ASAP_Group, Group_Object_Descriptor> group_object_descriptors{};

    std::shared_ptr<Group_Object_Table_Property> group_object_table();
    std::shared_ptr<Extended_Group_Object_Reference_Property> extended_group_object_reference();

    /** get group object number by ASAP */
    Group_Object_Number group_object_number(const ASAP_Group asap) const;

    /** get ASAP by group object number */
    ASAP_Group asap(const Group_Object_Number group_object_number) const;
};

}
