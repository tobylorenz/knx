// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Group_Object_Table_Property::Group_Object_Table_Property() :
    Data_Property(Group_Object_Table::PID_GRP_OBJTABLE)
{
    description.property_datatype = PDT_GENERIC_06; // @note not specified
}

void Group_Object_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    ++first;
    configuration = *first++;
    dpt = (*first++ << 8) | (*first++);
    dpst = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Group_Object_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(0);
    data.push_back(configuration);

    data.push_back(dpt >> 8);
    data.push_back(dpt & 0xff);

    data.push_back(dpst >> 8);
    data.push_back(dpst & 0xff);

    return data;
}

std::string Group_Object_Table_Property::text() const
{
    std::ostringstream oss;

    oss << "Configuration: "
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(configuration)
        << ", Datatype: "
        << std::dec << static_cast<uint16_t>(dpt)
        << "."
        << std::setfill('0') << std::setw(3) << std::dec << static_cast<uint16_t>(dpst);

    return oss.str();
}

}
