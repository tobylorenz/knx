// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Config.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Group Object Table Property
 *
 * @ingroup KNX_03_05_01_04_12_04_02_04
 */
class Group_Object_Table_Property :
    public Data_Property
{
public:
    Group_Object_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Configuration */
    Group_Object_Config configuration{};

    /** Datatype (High part of DPT) */
    uint16_t dpt{};

    /** Datatype (low part of DPT) */
    uint16_t dpst{};
};

}
