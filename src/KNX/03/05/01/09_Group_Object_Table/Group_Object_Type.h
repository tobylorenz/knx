// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Group Object Type
 *
 * @ingroup KNX_03_04_01_03_02
 * @ingroup KNX_03_05_01_04_12_02_01_02
 * @ingroup KNX_03_05_01_04_12_05_02_04_01_04
 *
 * ETS: ObjectSize
 */
enum class Group_Object_Type : uint8_t {
    Unsigned_Integer_1 = 0, // 1 bit; ETS: 1 Bit
    Unsigned_Integer_2 = 1, // 2 bit; ETS: 2 Bit
    Unsigned_Integer_3 = 2, // 3 bit; ETS: 3 Bit
    Unsigned_Integer_4 = 3, // 4 bit; ETS: 4 Bit
    Unsigned_Integer_5 = 4, // 5 bit; ETS: 5 Bit
    Unsigned_Integer_6 = 5, // 6 bit; ETS: 6 Bit
    Unsigned_Integer_7 = 6, // 7 bit; ETS: 7 Bit
    Octet_1 = 7, // 1 octet; ETS: 1 Byte
    Octet_2 = 8, // 2 octets; ETS: 2 Bytes
    Octet_3 = 9, // 3 octets; ETS: 3 Bytes
    Octet_4 = 10, // 4 octets; ETS: 4 Bytes
    Octet_6 = 11, // 6 octets; ETS: 6 Bytes
    Octet_8 = 12, // 8 octets; ETS: 8 Bytes
    Octet_10 = 13, // 10 octets; ETS: 10 Bytes
    Octet_14 = 14, // 14 octets; ETS: 14 Bytes
    Octet_5 = 15, // 5 octets; ETS: 5 Bytes
    Octet_7 = 16, // 7 octets; ETS: 7 Bytes
    Octet_9 = 17, // 9 octets; ETS: 9 Bytes
    Octet_11 = 18, // 11 octets; ETS: 11 Bytes
    Octet_12 = 19, // 12 octets; ETS: 12 Bytes
    Octet_13 = 20, // 13 octets; ETS: 13 Bytes
    Octet_15 = 21, // 15 octets; ETS: 15 Bytes
    Octet_16 = 22, // 16 octets; ETS: 16 Bytes
    Octet_17 = 23, // 17 octets; ETS: 17 Bytes
    // ...
    Octet_248 = 254, // 248 octets
    Octet_252 = 255 // 252 octets

                // ETS: 18 Bytes .. 50 Bytes
                // ETS: LegacyVarData
};

}
