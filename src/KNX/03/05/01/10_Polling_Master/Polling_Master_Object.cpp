// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/10_Polling_Master/Polling_Master_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

Polling_Master_Object::Polling_Master_Object() :
    System_Interface_Object(OT_POLLING_MASTER)
{
}

std::shared_ptr<Property> Polling_Master_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_POLLING_STATE:
        return std::make_shared<Polling_State_Property>();
    case PID_POLLING_SLAVE_ADDR:
        return std::make_shared<Polling_Slave_Address_Property>();
    case PID_POLL_CYCLE:
        return std::make_shared<Polling_Cycle_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Polling_State_Property> Polling_Master_Object::polling_state()
{
    std::shared_ptr<Property> property = property_by_id(Polling_Master_Object::PID_POLLING_STATE);
    if (!property) {
        property = std::make_shared<Polling_State_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Polling_State_Property>(property);
}

std::shared_ptr<Polling_Slave_Address_Property> Polling_Master_Object::polling_slave_address()
{
    std::shared_ptr<Property> property = property_by_id(Polling_Master_Object::PID_POLLING_SLAVE_ADDR);
    if (!property) {
        property = std::make_shared<Polling_Slave_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Polling_Slave_Address_Property>(property);
}

std::shared_ptr<Polling_Cycle_Property> Polling_Master_Object::polling_cycle()
{
    std::shared_ptr<Property> property = property_by_id(Polling_Master_Object::PID_POLL_CYCLE);
    if (!property) {
        property = std::make_shared<Polling_Cycle_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Polling_Cycle_Property>(property);
}

}
