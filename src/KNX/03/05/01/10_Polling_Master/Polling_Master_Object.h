// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/10_Polling_Master/Polling_Cycle_Property.h>
#include <KNX/03/05/01/10_Polling_Master/Polling_Slave_Address_Property.h>
#include <KNX/03/05/01/10_Polling_Master/Polling_State_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Polling_Master Object (Object Type 10)
 */
class KNX_EXPORT Polling_Master_Object :
    public System_Interface_Object
{
public:
    explicit Polling_Master_Object();

    /**
     * Polling Master Interface Object (Object Type 10)
     *
     * @ingroup KNX_03_07_03_03_03_02
     */
    enum : Property_Id {
        /** Polling State */
        PID_POLLING_STATE = 51,

        /** Polling Slave Address */
        PID_POLLING_SLAVE_ADDR = 52,

        /** Polling Cycle */
        PID_POLL_CYCLE = 53,
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<Polling_State_Property> polling_state();
    std::shared_ptr<Polling_Slave_Address_Property> polling_slave_address();
    std::shared_ptr<Polling_Cycle_Property> polling_cycle();
};

}
