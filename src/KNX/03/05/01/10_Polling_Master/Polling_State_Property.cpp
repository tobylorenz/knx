// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/10_Polling_Master/Polling_State_Property.h>

#include <KNX/03/05/01/10_Polling_Master/Polling_Master_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Polling_State_Property::Polling_State_Property() :
    Data_Property(Polling_Master_Object::PID_POLLING_STATE)
{
    description.property_datatype = PDT_UNSIGNED_CHAR; // @todo datatype?
}

void Polling_State_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Polling_State_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Polling_State_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Polling_State_Property::toData

    return data;
}

std::string Polling_State_Property::text() const
{
    // @todo Polling_State_Property::text
    return "";
}

}
