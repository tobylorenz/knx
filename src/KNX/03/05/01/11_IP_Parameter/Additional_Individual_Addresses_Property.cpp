// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Additional_Individual_Addresses_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Additional_Individual_Addresses_Property::Additional_Individual_Addresses_Property() :
    Data_Property(IP_Parameter_Object::PID_ADDITIONAL_INDIVIDUAL_ADDRESSES)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Additional_Individual_Addresses_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    while (first != last) {
        Individual_Address individual_address;
        individual_address.fromData(first, first + 2);
        first += 2;
        additional_individual_addresses.push_back(individual_address);
    }

    assert(first == last);
}

std::vector<uint8_t> Additional_Individual_Addresses_Property::toData() const
{
    std::vector<uint8_t> data;

    for (const Individual_Address & individual_address : additional_individual_addresses) {
        data.push_back(individual_address >> 8);
        data.push_back(individual_address & 0xff);
    }

    return data;
}

std::string Additional_Individual_Addresses_Property::text() const
{
    std::ostringstream oss;

    for (const auto & individual_address : additional_individual_addresses) {
        oss << ", " << individual_address.text();
    }

    return oss.str().substr(2);
}

}
