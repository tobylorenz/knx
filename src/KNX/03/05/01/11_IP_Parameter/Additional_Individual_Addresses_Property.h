// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Additional Individual Addresses
 *
 * @ingroup KNX_03_08_03_02_05_04
 */
class Additional_Individual_Addresses_Property :
    public Data_Property
{
public:
    Additional_Individual_Addresses_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Individual Address */
    std::vector<Individual_Address> additional_individual_addresses{};
};

}
