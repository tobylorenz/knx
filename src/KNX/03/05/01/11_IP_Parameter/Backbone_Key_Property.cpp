// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Backbone_Key_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Backbone_Key_Property::Backbone_Key_Property() :
    Data_Property(IP_Parameter_Object::PID_BACKBONE_KEY)
{
    description.property_datatype = PDT_GENERIC_16;
}

void Backbone_Key_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 16);

    // @todo Backbone_Key_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Backbone_Key_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Backbone_Key_Property::toData

    return data;
}

std::string Backbone_Key_Property::text() const
{
    // @todo Backbone_Key_Property::text
    return "";
}

}
