// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Current IP Assignment Method
 *
 * @ingroup KNX_03_08_03_02_05_05
 */
class Current_IP_Assignment_Method_Property :
    public Data_Property
{
public:
    Current_IP_Assignment_Method_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** manually */
    bool manually{false};

    /** BootP */
    bool BootP{false};

    /** DHCP */
    bool DHCP{false};

    /** AutoIP */
    bool AutoIP{false};
};

}
