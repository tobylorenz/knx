// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Current_Subnet_Mask_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Current_Subnet_Mask_Property::Current_Subnet_Mask_Property() :
    Data_Property(IP_Parameter_Object::PID_CURRENT_SUBNET_MASK)
{
    description.property_datatype = PDT_UNSIGNED_LONG;
}

void Current_Subnet_Mask_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    std::copy(first, last, std::begin(current_subnet_mask));

    assert(first == last);
}

std::vector<uint8_t> Current_Subnet_Mask_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(current_subnet_mask), std::cend(current_subnet_mask));

    return data;
}

std::string Current_Subnet_Mask_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(current_subnet_mask[0])
        << "."
        << std::dec << static_cast<uint16_t>(current_subnet_mask[1])
        << "."
        << std::dec << static_cast<uint16_t>(current_subnet_mask[2])
        << "."
        << std::dec << static_cast<uint16_t>(current_subnet_mask[3]);

    return oss.str();
}

}
