// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Current Subnet Mask
 *
 * @ingroup KNX_03_08_03_02_05_09
 */
class Current_Subnet_Mask_Property :
    public Data_Property
{
public:
    Current_Subnet_Mask_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Current Subnet Mask */
    std::array<uint8_t, 4> current_subnet_mask{};
};

}
