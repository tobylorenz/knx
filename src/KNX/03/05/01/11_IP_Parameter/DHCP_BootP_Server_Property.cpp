// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/DHCP_BootP_Server_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

DHCP_BootP_Server_Property::DHCP_BootP_Server_Property() :
    Data_Property(IP_Parameter_Object::PID_DHCP_BOOTP_SERVER)
{
    description.property_datatype = PDT_UNSIGNED_LONG;
}

void DHCP_BootP_Server_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    std::copy(first, last, std::begin(dhcp_bootp_server));

    assert(first == last);
}

std::vector<uint8_t> DHCP_BootP_Server_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(dhcp_bootp_server), std::cend(dhcp_bootp_server));

    return data;
}

std::string DHCP_BootP_Server_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(dhcp_bootp_server[0])
        << "."
        << std::dec << static_cast<uint16_t>(dhcp_bootp_server[1])
        << "."
        << std::dec << static_cast<uint16_t>(dhcp_bootp_server[2])
        << "."
        << std::dec << static_cast<uint16_t>(dhcp_bootp_server[3]);

    return oss.str();
}

}
