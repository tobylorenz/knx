// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DHCP/BootP Server
 *
 * @ingroup KNX_03_08_03_02_05_14
 */
class DHCP_BootP_Server_Property :
    public Data_Property
{
public:
    DHCP_BootP_Server_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** DHCP/BootP Server */
    std::array<uint8_t, 4> dhcp_bootp_server{};
};

}
