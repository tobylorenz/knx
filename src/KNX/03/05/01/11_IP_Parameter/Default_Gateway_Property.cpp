// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Default_Gateway_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Default_Gateway_Property::Default_Gateway_Property() :
    Data_Property(IP_Parameter_Object::PID_DEFAULT_GATEWAY)
{
    description.property_datatype = PDT_UNSIGNED_LONG;
}

void Default_Gateway_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    std::copy(first, last, std::begin(default_gateway));

    assert(first == last);
}

std::vector<uint8_t> Default_Gateway_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(default_gateway), std::cend(default_gateway));

    return data;
}

std::string Default_Gateway_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(default_gateway[0])
        << "."
        << std::dec << static_cast<uint16_t>(default_gateway[1])
        << "."
        << std::dec << static_cast<uint16_t>(default_gateway[2])
        << "."
        << std::dec << static_cast<uint16_t>(default_gateway[3]);

    return oss.str();
}

}
