// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Device_Authentication_Code_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Device_Authentication_Code_Property::Device_Authentication_Code_Property() :
    Data_Property(IP_Parameter_Object::PID_DEVICE_AUTHENTICATION_CODE)
{
    description.property_datatype = PDT_GENERIC_16;
}

void Device_Authentication_Code_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 16);

    // @todo Device_Authentication_Code_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Device_Authentication_Code_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Device_Authentication_Code_Property::toData

    return data;
}

std::string Device_Authentication_Code_Property::text() const
{
    // @todo Device_Authentication_Code_Property::text
    return "";
}

}
