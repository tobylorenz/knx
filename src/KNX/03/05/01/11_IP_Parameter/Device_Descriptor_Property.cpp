// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Device_Descriptor_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Device_Descriptor_Property::Device_Descriptor_Property() :
    Property()
{
    description.property_id = IP_Parameter_Object::PID_DEVICE_DESCRIPTOR;
    description.property_datatype = PDT_UNSIGNED_CHAR; // @type datatype?
}

void Device_Descriptor_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    // @todo Device_Descriptor_Property::fromData

    assert(first == last);
}

std::vector<uint8_t> Device_Descriptor_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Device_Descriptor_Property::toData

    return data;
}

std::string Device_Descriptor_Property::text() const
{
    // @todo Device_Descriptor_Property::text
    return "";
}

}
