// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Friendly_Name_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Friendly_Name_Property::Friendly_Name_Property() :
    Data_Property(IP_Parameter_Object::PID_FRIENDLY_NAME)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
    description.max_nr_of_elem = 30;
    nr_of_elem = 30;
}

void Friendly_Name_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 30);

    std::copy(first, last, std::begin(friendly_name));

    assert(first == last);
}

std::vector<uint8_t> Friendly_Name_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(friendly_name), std::cend(friendly_name));

    return data;
}

std::string Friendly_Name_Property::text() const
{
    std::string str;

    str.assign(std::cbegin(friendly_name), std::cend(friendly_name));

    return str;
}

}
