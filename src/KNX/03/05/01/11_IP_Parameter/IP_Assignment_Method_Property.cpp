// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/IP_Assignment_Method_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

IP_Assignment_Method_Property::IP_Assignment_Method_Property() :
    Data_Property(IP_Parameter_Object::PID_IP_ASSIGNMENT_METHOD)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void IP_Assignment_Method_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    manually = (*first >> 0) & 0x01;
    BootP = (*first >> 1) & 0x01;
    DHCP = (*first >> 2) & 0x01;
    AutoIP = (*first >> 3) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> IP_Assignment_Method_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (manually << 0) |
        (BootP << 1) |
        (DHCP << 2) |
        (AutoIP << 3));

    return data;
}

std::string IP_Assignment_Method_Property::text() const
{
    std::ostringstream oss;

    oss << (manually ? ", manually" : "")
        << (BootP ? ", BootP" : "")
        << (DHCP ? ", DHCP" : "")
        << (AutoIP ? ", AutoIP" : "");

    return oss.str().substr(2);
}

}
