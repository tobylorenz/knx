// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/IP_Capabilities_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

IP_Capabilities_Property::IP_Capabilities_Property() :
    Data_Property(IP_Parameter_Object::PID_IP_CAPABILITIES)
{
    description.property_datatype = PDT_BITSET8;
}

void IP_Capabilities_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    BootP = (*first >> 0) & 0x01;
    DHCP = (*first >> 1) & 0x01;
    AutoIP = (*first >> 2) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> IP_Capabilities_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (BootP << 0) |
        (DHCP << 1) |
        (AutoIP << 2));

    return data;
}

std::string IP_Capabilities_Property::text() const
{
    std::ostringstream oss;

    oss << (BootP ? ", BootP" : "")
        << (DHCP ? ", DHCP" : "")
        << (AutoIP ? ", AutoIP" : "");

    return oss.str().substr(2);
}

}
