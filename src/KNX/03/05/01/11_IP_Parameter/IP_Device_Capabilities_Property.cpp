// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/IP_Device_Capabilities_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

IP_Device_Capabilities_Property::IP_Device_Capabilities_Property() :
    Data_Property(IP_Parameter_Object::PID_KNXNETIP_DEVICE_CAPABILITIES)
{
    description.property_datatype = PDT_BITSET16;
}

void IP_Device_Capabilities_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    ++first;

    device_management = (*first >> 0) & 0x01;
    tunnelling = (*first >> 1) & 0x01;
    routing = (*first >> 2) & 0x01;
    remote_logging = (*first >> 3) & 0x01;
    remote_configuration_and_diagnosis = (*first >> 4) & 0x01;
    object_server = (*first >> 5) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> IP_Device_Capabilities_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(0);

    data.push_back(
        (device_management << 0) |
        (tunnelling << 1) |
        (routing << 2) |
        (remote_logging << 3) |
        (remote_configuration_and_diagnosis << 4) |
        (object_server << 5));

    return data;
}

std::string IP_Device_Capabilities_Property::text() const
{
    std::ostringstream oss;

    oss << (device_management ? ", Device Management" : "")
        << (tunnelling ? ", Tunnelling" : "")
        << (routing ? ", Routing" : "")
        << (remote_logging ? ", Remote Logging" : "")
        << (remote_configuration_and_diagnosis ? ", Remote Configuration and Diagnosis" : "")
        << (object_server ? ", Object Server" : "");

    return oss.str().substr(2);
}

}
