// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/IP_Device_State_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

IP_Device_State_Property::IP_Device_State_Property() :
    Data_Property(IP_Parameter_Object::PID_KNXNETIP_DEVICE_STATE)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void IP_Device_State_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    knx_fault = (*first >> 0) & 0x01;
    ip_fault = (*first >> 0) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> IP_Device_State_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (knx_fault << 0) |
        (ip_fault << 1));

    return data;
}

std::string IP_Device_State_Property::text() const
{
    std::ostringstream oss;

    oss << (knx_fault ? ", KNX Fault" : "")
        << (ip_fault ? ", IP Fault" : "");

    return oss.str().substr(2);
}

}
