// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * IP Device State
 *
 * @ingroup KNX_03_08_03_02_05_20
 */
class IP_Device_State_Property :
    public Data_Property
{
public:
    IP_Device_State_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** KNX Fault */
    bool knx_fault{false};

    /** IP Fault */
    bool ip_fault{false};
};

}
