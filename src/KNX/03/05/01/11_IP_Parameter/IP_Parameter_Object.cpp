// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

IP_Parameter_Object::IP_Parameter_Object() :
    System_Interface_Object(OT_KNXIP_PARAMETER)
{
}

std::shared_ptr<Property> IP_Parameter_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_PROJECT_INSTALLATION_ID:
        return std::make_shared<Project_Installation_Identification_Property>();
    case PID_KNX_INDIVIDUAL_ADDRESS:
        return std::make_shared<Individual_Address_Property>();
    case PID_ADDITIONAL_INDIVIDUAL_ADDRESSES:
        return std::make_shared<Additional_Individual_Addresses_Property>();
    case PID_CURRENT_IP_ASSIGNMENT_METHOD:
        return std::make_shared<Current_IP_Assignment_Method_Property>();
    case PID_IP_ASSIGNMENT_METHOD:
        return std::make_shared<IP_Assignment_Method_Property>();
    case PID_IP_CAPABILITIES:
        return std::make_shared<IP_Capabilities_Property>();
    case PID_CURRENT_IP_ADDRESS:
        return std::make_shared<Current_IP_Address_Property>();
    case PID_CURRENT_SUBNET_MASK:
        return std::make_shared<Current_Subnet_Mask_Property>();
    case PID_CURRENT_DEFAULT_GATEWAY:
        return std::make_shared<Current_Default_Gateway_Property>();
    case PID_IP_ADDRESS:
        return std::make_shared<IP_Address_Property>();
    case PID_SUBNET_MASK:
        return std::make_shared<Subnet_Mask_Property>();
    case PID_DEFAULT_GATEWAY:
        return std::make_shared<Default_Gateway_Property>();
    case PID_DHCP_BOOTP_SERVER:
        return std::make_shared<DHCP_BootP_Server_Property>();
    case PID_MAC_ADDRESS:
        return std::make_shared<MAC_Address_Property>();
    case PID_SYSTEM_SETUP_MULTICAST_ADDRESS:
        return std::make_shared<System_Setup_Multicast_Address_Property>();
    case PID_ROUTING_MULTICAST_ADDRESS:
        return std::make_shared<Routing_Multicast_Address_Property>();
    case PID_TTL:
        return std::make_shared<TTL_Property>();
    case PID_KNXNETIP_DEVICE_CAPABILITIES:
        return std::make_shared<IP_Device_Capabilities_Property>();
    case PID_KNXNETIP_DEVICE_STATE:
        return std::make_shared<IP_Device_State_Property>();
    case PID_KNXNETIP_ROUTING_CAPABILITIES:
        return std::make_shared<IP_Routing_Capabilities_Property>();
    case PID_PRIORITY_FIFO_ENABLED:
        return std::make_shared<Priority_FIFO_Enabled_Property>();
    case PID_QUEUE_OVERFLOW_TO_IP:
        return std::make_shared<Queue_Overflow_To_IP_Property>();
    case PID_QUEUE_OVERFLOW_TO_KNX:
        return std::make_shared<Queue_Overflow_To_KNX_Property>();
    case PID_MSG_TRANSMIT_TO_IP:
        return std::make_shared<Message_Transmit_To_IP_Property>();
    case PID_MSG_TRANSMIT_TO_KNX:
        return std::make_shared<Message_Transmit_To_KNX_Property>();
    case PID_FRIENDLY_NAME:
        return std::make_shared<Friendly_Name_Property>();
    //    case PID_DEVICE_DESCRIPTOR:
    //        return std::make_shared<Device_Descriptor_Property>();
    case PID_ROUTING_BUSY_WAIT_TIME:
        return std::make_shared<Routing_Busy_Wait_Time_Property>();
    case PID_TUNNELLING_ADDRESSES:
        return std::make_shared<Tunnelling_Addresses_Property>();
    case PID_BACKBONE_KEY:
        return std::make_shared<Backbone_Key_Property>();
    case PID_DEVICE_AUTHENTICATION_CODE:
        return std::make_shared<Device_Authentication_Code_Property>();
    case PID_PASSWORD_HASHES:
        return std::make_shared<Password_Hashes_Property>();
    case PID_SECURED_SERVICE_FAMILIES:
        return std::make_shared<Secured_Service_Families_Property>();
    case PID_MULTICAST_LATENCY_TOLERANCE:
        return std::make_shared<Multicast_Latency_Tolerance_Property>();
    case PID_SYNC_LATENCY_FRACTION:
        return std::make_shared<Sync_Latency_Fraction_Property>();
    case PID_TUNNELLING_USERS:
        return std::make_shared<Tunnelling_Users_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Project_Installation_Identification_Property> IP_Parameter_Object::project_installation_identification()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_PROJECT_INSTALLATION_ID);
    if (!property) {
        property = std::make_shared<Project_Installation_Identification_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Project_Installation_Identification_Property>(property);
}

std::shared_ptr<Individual_Address_Property> IP_Parameter_Object::individual_address()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_KNX_INDIVIDUAL_ADDRESS);
    if (!property) {
        property = std::make_shared<Individual_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Individual_Address_Property>(property);
}

std::shared_ptr<Additional_Individual_Addresses_Property> IP_Parameter_Object::additional_individual_addresses()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_ADDITIONAL_INDIVIDUAL_ADDRESSES);
    if (!property) {
        property = std::make_shared<Additional_Individual_Addresses_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Additional_Individual_Addresses_Property>(property);
}

std::shared_ptr<Current_IP_Assignment_Method_Property> IP_Parameter_Object::current_ip_assignment_method()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_CURRENT_IP_ASSIGNMENT_METHOD);
    if (!property) {
        property = std::make_shared<Current_IP_Assignment_Method_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Current_IP_Assignment_Method_Property>(property);
}

std::shared_ptr<IP_Assignment_Method_Property> IP_Parameter_Object::ip_assignment_method()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_IP_ASSIGNMENT_METHOD);
    if (!property) {
        property = std::make_shared<IP_Assignment_Method_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<IP_Assignment_Method_Property>(property);
}

std::shared_ptr<IP_Capabilities_Property> IP_Parameter_Object::ip_capabilities()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_IP_CAPABILITIES);
    if (!property) {
        property = std::make_shared<IP_Capabilities_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<IP_Capabilities_Property>(property);
}

std::shared_ptr<Current_IP_Address_Property> IP_Parameter_Object::current_ip_address()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_CURRENT_IP_ADDRESS);
    if (!property) {
        property = std::make_shared<Current_IP_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Current_IP_Address_Property>(property);
}

std::shared_ptr<Current_Subnet_Mask_Property> IP_Parameter_Object::current_subnet_mask()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_CURRENT_SUBNET_MASK);
    if (!property) {
        property = std::make_shared<Current_Subnet_Mask_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Current_Subnet_Mask_Property>(property);
}

std::shared_ptr<Current_Default_Gateway_Property> IP_Parameter_Object::current_default_gateway()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_CURRENT_DEFAULT_GATEWAY);
    if (!property) {
        property = std::make_shared<Current_Default_Gateway_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Current_Default_Gateway_Property>(property);
}

std::shared_ptr<IP_Address_Property> IP_Parameter_Object::ip_address()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_IP_ADDRESS);
    if (!property) {
        property = std::make_shared<IP_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<IP_Address_Property>(property);
}

std::shared_ptr<Subnet_Mask_Property> IP_Parameter_Object::subnet_mask()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_SUBNET_MASK);
    if (!property) {
        property = std::make_shared<Subnet_Mask_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Subnet_Mask_Property>(property);
}

std::shared_ptr<Default_Gateway_Property> IP_Parameter_Object::default_gateway()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_DEFAULT_GATEWAY);
    if (!property) {
        property = std::make_shared<Default_Gateway_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Default_Gateway_Property>(property);
}

std::shared_ptr<DHCP_BootP_Server_Property> IP_Parameter_Object::dhcp_bootp_server()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_DHCP_BOOTP_SERVER);
    if (!property) {
        property = std::make_shared<DHCP_BootP_Server_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<DHCP_BootP_Server_Property>(property);
}

std::shared_ptr<MAC_Address_Property> IP_Parameter_Object::mac_address()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_MAC_ADDRESS);
    if (!property) {
        property = std::make_shared<MAC_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<MAC_Address_Property>(property);
}

std::shared_ptr<System_Setup_Multicast_Address_Property> IP_Parameter_Object::system_setup_multicast_address()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_SYSTEM_SETUP_MULTICAST_ADDRESS);
    if (!property) {
        property = std::make_shared<System_Setup_Multicast_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<System_Setup_Multicast_Address_Property>(property);
}

std::shared_ptr<Routing_Multicast_Address_Property> IP_Parameter_Object::routing_multicast_address()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_ROUTING_MULTICAST_ADDRESS);
    if (!property) {
        property = std::make_shared<Routing_Multicast_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Routing_Multicast_Address_Property>(property);
}

std::shared_ptr<TTL_Property> IP_Parameter_Object::ttl()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_TTL);
    if (!property) {
        property = std::make_shared<TTL_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<TTL_Property>(property);
}

std::shared_ptr<IP_Device_Capabilities_Property> IP_Parameter_Object::ip_device_capabilities()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_KNXNETIP_DEVICE_CAPABILITIES);
    if (!property) {
        property = std::make_shared<IP_Device_Capabilities_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<IP_Device_Capabilities_Property>(property);
}

std::shared_ptr<IP_Device_State_Property> IP_Parameter_Object::ip_device_state()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_KNXNETIP_DEVICE_STATE);
    if (!property) {
        property = std::make_shared<IP_Device_State_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<IP_Device_State_Property>(property);
}

std::shared_ptr<IP_Routing_Capabilities_Property> IP_Parameter_Object::ip_routing_capabilities()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_KNXNETIP_ROUTING_CAPABILITIES);
    if (!property) {
        property = std::make_shared<IP_Routing_Capabilities_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<IP_Routing_Capabilities_Property>(property);
}

std::shared_ptr<Priority_FIFO_Enabled_Property> IP_Parameter_Object::priority_fifo_enabled()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_PRIORITY_FIFO_ENABLED);
    if (!property) {
        property = std::make_shared<Priority_FIFO_Enabled_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Priority_FIFO_Enabled_Property>(property);
}

std::shared_ptr<Queue_Overflow_To_IP_Property> IP_Parameter_Object::queue_overflow_to_ip()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_QUEUE_OVERFLOW_TO_IP);
    if (!property) {
        property = std::make_shared<Queue_Overflow_To_IP_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Queue_Overflow_To_IP_Property>(property);
}

std::shared_ptr<Queue_Overflow_To_KNX_Property> IP_Parameter_Object::queue_overflow_to_knx()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_QUEUE_OVERFLOW_TO_KNX);
    if (!property) {
        property = std::make_shared<Queue_Overflow_To_KNX_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Queue_Overflow_To_KNX_Property>(property);
}

std::shared_ptr<Message_Transmit_To_IP_Property> IP_Parameter_Object::message_transmit_to_ip()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_MSG_TRANSMIT_TO_IP);
    if (!property) {
        property = std::make_shared<Message_Transmit_To_IP_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Message_Transmit_To_IP_Property>(property);
}

std::shared_ptr<Message_Transmit_To_KNX_Property> IP_Parameter_Object::message_transmit_to_knx()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_MSG_TRANSMIT_TO_KNX);
    if (!property) {
        property = std::make_shared<Message_Transmit_To_KNX_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Message_Transmit_To_KNX_Property>(property);
}

std::shared_ptr<Friendly_Name_Property> IP_Parameter_Object::friendly_name()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_FRIENDLY_NAME);
    if (!property) {
        property = std::make_shared<Friendly_Name_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Friendly_Name_Property>(property);
}

//std::shared_ptr<Device_Descriptor_Property> IP_Parameter_Object::device_descriptor() {
//    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_DEVICE_DESCRIPTOR);
//    if (!property) {
//        property = std::make_shared<Device_Descriptor_Property>();
//        const Property_Index property_index = properties.size();
//        properties[property_index] = property;
//    }
//    return std::dynamic_pointer_cast<Device_Descriptor_Property>(property);
//}

std::shared_ptr<Routing_Busy_Wait_Time_Property> IP_Parameter_Object::routing_busy_wait_time()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_ROUTING_BUSY_WAIT_TIME);
    if (!property) {
        property = std::make_shared<Routing_Busy_Wait_Time_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Routing_Busy_Wait_Time_Property>(property);
}

std::shared_ptr<Tunnelling_Addresses_Property> IP_Parameter_Object::tunnelling_addresses()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_TUNNELLING_ADDRESSES);
    if (!property) {
        property = std::make_shared<Tunnelling_Addresses_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Tunnelling_Addresses_Property>(property);
}

std::shared_ptr<Backbone_Key_Property> IP_Parameter_Object::backbone_key()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_BACKBONE_KEY);
    if (!property) {
        property = std::make_shared<Backbone_Key_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Backbone_Key_Property>(property);
}

std::shared_ptr<Device_Authentication_Code_Property> IP_Parameter_Object::device_authentication_code()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_DEVICE_AUTHENTICATION_CODE);
    if (!property) {
        property = std::make_shared<Device_Authentication_Code_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Device_Authentication_Code_Property>(property);
}

std::shared_ptr<Password_Hashes_Property> IP_Parameter_Object::password_hashes()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_PASSWORD_HASHES);
    if (!property) {
        property = std::make_shared<Password_Hashes_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Password_Hashes_Property>(property);
}

std::shared_ptr<Secured_Service_Families_Property> IP_Parameter_Object::secured_service_families()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_SECURED_SERVICE_FAMILIES);
    if (!property) {
        property = std::make_shared<Secured_Service_Families_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Secured_Service_Families_Property>(property);
}

std::shared_ptr<Multicast_Latency_Tolerance_Property> IP_Parameter_Object::multicast_latency_tolerance()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_MULTICAST_LATENCY_TOLERANCE);
    if (!property) {
        property = std::make_shared<Multicast_Latency_Tolerance_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Multicast_Latency_Tolerance_Property>(property);
}

std::shared_ptr<Sync_Latency_Fraction_Property> IP_Parameter_Object::sync_latency_fraction()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_SYNC_LATENCY_FRACTION);
    if (!property) {
        property = std::make_shared<Sync_Latency_Fraction_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Sync_Latency_Fraction_Property>(property);
}

std::shared_ptr<Tunnelling_Users_Property> IP_Parameter_Object::tunnelling_users()
{
    std::shared_ptr<Property> property = property_by_id(IP_Parameter_Object::PID_TUNNELLING_USERS);
    if (!property) {
        property = std::make_shared<Tunnelling_Users_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Tunnelling_Users_Property>(property);
}

}
