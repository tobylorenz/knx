// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/11_IP_Parameter/Additional_Individual_Addresses_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Backbone_Key_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Current_Default_Gateway_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Current_IP_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Current_IP_Assignment_Method_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Current_Subnet_Mask_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/DHCP_BootP_Server_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Default_Gateway_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Device_Authentication_Code_Property.h>
// #include <KNX/03/05/01/11_IP_Parameter/Device_Descriptor_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Friendly_Name_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Assignment_Method_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Capabilities_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Device_Capabilities_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Device_State_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Routing_Capabilities_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Individual_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/MAC_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Message_Transmit_To_IP_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Message_Transmit_To_KNX_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Multicast_Latency_Tolerance_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Password_Hashes_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Priority_FIFO_Enabled_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Project_Installation_Identification_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Queue_Overflow_To_IP_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Queue_Overflow_To_KNX_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Routing_Busy_Wait_Time_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Routing_Multicast_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Secured_Service_Families_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Subnet_Mask_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Sync_Latency_Fraction_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/System_Setup_Multicast_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/TTL_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Tunnelling_Addresses_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Tunnelling_Users_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * KNXnet/IP Parameter Object (Object Type 11)
 *
 * @ingroup KNX_03_05_01_04_07
 * @ingroup KNX_03_08_03_02_05
 */
class KNX_EXPORT IP_Parameter_Object :
    public System_Interface_Object
{
public:
    explicit IP_Parameter_Object();

    /**
     * KNXnet/IP Parameter Object (Object Type 11)
     *
     * @ingroup KNX_03_05_01_04_07
     * @ingroup KNX_03_08_03_02_05
     * @ingroup KNX_AN185_02_03_02_01
     * @ingroup KNX_AN185_02_07
     */
    enum : Property_Id {
        /** Project Installation Identification */
        PID_PROJECT_INSTALLATION_ID = 51,

        /** KNX Individual Address */
        PID_KNX_INDIVIDUAL_ADDRESS = 52,

        /** Additional Individual Addresses */
        PID_ADDITIONAL_INDIVIDUAL_ADDRESSES = 53,

        PID_CURRENT_IP_ASSIGNMENT_METHOD = 54,

        PID_IP_ASSIGNMENT_METHOD = 55,

        PID_IP_CAPABILITIES = 56,

        PID_CURRENT_IP_ADDRESS = 57,

        PID_CURRENT_SUBNET_MASK = 58,

        PID_CURRENT_DEFAULT_GATEWAY = 59,

        PID_IP_ADDRESS = 60,

        PID_SUBNET_MASK = 61,

        PID_DEFAULT_GATEWAY = 62,

        PID_DHCP_BOOTP_SERVER = 63,

        PID_MAC_ADDRESS = 64,

        PID_SYSTEM_SETUP_MULTICAST_ADDRESS = 65,

        PID_ROUTING_MULTICAST_ADDRESS = 66,

        PID_TTL = 67,

        PID_KNXNETIP_DEVICE_CAPABILITIES = 68,

        PID_KNXNETIP_DEVICE_STATE = 69,

        PID_KNXNETIP_ROUTING_CAPABILITIES = 70,

        PID_PRIORITY_FIFO_ENABLED = 71,

        PID_QUEUE_OVERFLOW_TO_IP = 72,

        PID_QUEUE_OVERFLOW_TO_KNX = 73,

        PID_MSG_TRANSMIT_TO_IP = 74,

        PID_MSG_TRANSMIT_TO_KNX = 75,

        PID_FRIENDLY_NAME = 76,

        PID_DEVICE_DESCRIPTOR = 77, // @tode Device_Object::PID_DEVICE_DESCRIPTOR

        PID_ROUTING_BUSY_WAIT_TIME = 78,

        PID_TUNNELLING_ADDRESSES = 79,

        PID_BACKBONE_KEY = 91,

        PID_DEVICE_AUTHENTICATION_CODE = 92,

        PID_PASSWORD_HASHES = 93,

        PID_SECURED_SERVICE_FAMILIES = 94,

        PID_MULTICAST_LATENCY_TOLERANCE = 95,

        PID_SYNC_LATENCY_FRACTION = 96,

        PID_TUNNELLING_USERS = 97
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<Project_Installation_Identification_Property> project_installation_identification(); // @see Device_Information_DIB::project_installation_identifier
    std::shared_ptr<Individual_Address_Property> individual_address(); // @see Device_Information_DIB::individual_address and Addresses_DIB::individual_address
    std::shared_ptr<Additional_Individual_Addresses_Property> additional_individual_addresses(); // @see Addresses_DIB::additional_individual_addresses
    std::shared_ptr<Current_IP_Assignment_Method_Property> current_ip_assignment_method(); // @see IP_Current_Config_DIB::current_ip_assignment_method
    std::shared_ptr<IP_Assignment_Method_Property> ip_assignment_method(); // @see IP_Config_DIB::IP_Assignment_Method
    std::shared_ptr<IP_Capabilities_Property> ip_capabilities(); // @see IP_Config_DIB::ip_capabilities
    std::shared_ptr<Current_IP_Address_Property> current_ip_address(); // @see IP_Current_Config_DIB::current_ip_address
    std::shared_ptr<Current_Subnet_Mask_Property> current_subnet_mask(); // @see IP_Current_Config_DIB::current_subnet_mask
    std::shared_ptr<Current_Default_Gateway_Property> current_default_gateway(); // @see IP_Current_Config_DIB::current_default_gateway
    std::shared_ptr<IP_Address_Property> ip_address(); // @see IP_Config_DIB::ip_address
    std::shared_ptr<Subnet_Mask_Property> subnet_mask(); // @see IP_Config_DIB::subnet_mask
    std::shared_ptr<Default_Gateway_Property> default_gateway(); // @see IP_Config_DIB::default_gateway
    std::shared_ptr<DHCP_BootP_Server_Property> dhcp_bootp_server(); // @see IP_Current_Config_DIB::dhcp_server
    std::shared_ptr<MAC_Address_Property> mac_address(); // @see Device_Information_DIB::device_mac_address
    std::shared_ptr<System_Setup_Multicast_Address_Property> system_setup_multicast_address();
    std::shared_ptr<Routing_Multicast_Address_Property> routing_multicast_address(); // @see Device_Information_DIB::device_routing_multicast_address
    std::shared_ptr<TTL_Property> ttl();
    std::shared_ptr<IP_Device_Capabilities_Property> ip_device_capabilities(); // @see Supported_Service_Families_DIB::service_families
    std::shared_ptr<IP_Device_State_Property> ip_device_state();
    std::shared_ptr<IP_Routing_Capabilities_Property> ip_routing_capabilities();
    std::shared_ptr<Priority_FIFO_Enabled_Property> priority_fifo_enabled();
    std::shared_ptr<Queue_Overflow_To_IP_Property> queue_overflow_to_ip(); // @see Device_Statistics::queue_overflow_to_ip
    std::shared_ptr<Queue_Overflow_To_KNX_Property> queue_overflow_to_knx(); // @see Device_Statistics::queue_overflow_to_knx
    std::shared_ptr<Message_Transmit_To_IP_Property> message_transmit_to_ip(); // @see Device_Statistics::telegrams_transmitted_to_IP
    std::shared_ptr<Message_Transmit_To_KNX_Property> message_transmit_to_knx(); // @see Device_Statistics::telegrams_transmitted_to_KNX
    std::shared_ptr<Friendly_Name_Property> friendly_name(); // @see Device_Information_DIB::device_friendly_name
    // @todo std::shared_ptr<Device_Descriptor_Property> device_descriptor();
    std::shared_ptr<Routing_Busy_Wait_Time_Property> routing_busy_wait_time();
    std::shared_ptr<Tunnelling_Addresses_Property> tunnelling_addresses();
    std::shared_ptr<Backbone_Key_Property> backbone_key();
    std::shared_ptr<Device_Authentication_Code_Property> device_authentication_code();
    std::shared_ptr<Password_Hashes_Property> password_hashes();
    std::shared_ptr<Secured_Service_Families_Property> secured_service_families();
    std::shared_ptr<Multicast_Latency_Tolerance_Property> multicast_latency_tolerance();
    std::shared_ptr<Sync_Latency_Fraction_Property> sync_latency_fraction();
    std::shared_ptr<Tunnelling_Users_Property> tunnelling_users();
};

}
