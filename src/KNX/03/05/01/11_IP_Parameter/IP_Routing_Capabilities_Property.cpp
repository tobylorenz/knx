// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/IP_Routing_Capabilities_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

IP_Routing_Capabilities_Property::IP_Routing_Capabilities_Property() :
    Data_Property(IP_Parameter_Object::PID_KNXNETIP_ROUTING_CAPABILITIES)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void IP_Routing_Capabilities_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    queue_overflow_error_count = (*first >> 0) & 0x01;
    transmitted_telegram_info_count = (*first >> 1) & 0x01;
    priority_fifo = (*first >> 2) & 0x01;
    multiple_knx_installations = (*first >> 3) & 0x01;
    group_address_mapping = (*first >> 4) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> IP_Routing_Capabilities_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (queue_overflow_error_count << 0) |
        (transmitted_telegram_info_count << 1) |
        (priority_fifo << 2) |
        (multiple_knx_installations << 3) |
        (group_address_mapping << 4));

    return data;
}

std::string IP_Routing_Capabilities_Property::text() const
{
    std::ostringstream oss;

    oss << (queue_overflow_error_count ? ", Statistics, Queue overflow error count, implemented" : "")
        << (transmitted_telegram_info_count ? ", Statistics, Transmitted telegram info count, implemented" : "")
        << (priority_fifo ? ", Priority/FIFO implemented" : "")
        << (multiple_knx_installations ? ", Multiple KNX installations supported" : "")
        << (group_address_mapping ? ", Group Address mapping supported" : "");

    return oss.str().substr(2);
}

}
