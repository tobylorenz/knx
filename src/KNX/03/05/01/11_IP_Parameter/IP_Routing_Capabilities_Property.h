// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * IP Routing Capabilities
 *
 * @ingroup KNX_03_08_03_02_05_21
 */
class IP_Routing_Capabilities_Property :
    public Data_Property
{
public:
    IP_Routing_Capabilities_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Statistics, Queue overflow error count, implemented */
    bool queue_overflow_error_count{};

    /** Statistics, Transmitted telegram info count, implemented */
    bool transmitted_telegram_info_count{};

    /** Priority/FIFO implemented */
    bool priority_fifo{};

    /** Multiple KNX installations supported */
    bool multiple_knx_installations{};

    /** Group Address mapping supported */
    bool group_address_mapping{};
};

}
