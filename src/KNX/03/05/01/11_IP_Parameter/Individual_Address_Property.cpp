// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Individual_Address_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Individual_Address_Property::Individual_Address_Property() :
    Data_Property(IP_Parameter_Object::PID_KNX_INDIVIDUAL_ADDRESS)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Individual_Address_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    individual_address.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> Individual_Address_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(individual_address >> 8);
    data.push_back(individual_address & 0xff);

    return data;
}

std::string Individual_Address_Property::text() const
{
    std::ostringstream oss;

    oss << ", " << individual_address.text();

    return oss.str().substr(2);
}

}
