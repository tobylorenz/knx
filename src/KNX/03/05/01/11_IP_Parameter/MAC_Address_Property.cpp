// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/MAC_Address_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

MAC_Address_Property::MAC_Address_Property() :
    Data_Property(IP_Parameter_Object::PID_MAC_ADDRESS)
{
    description.property_datatype = PDT_GENERIC_06;
}

void MAC_Address_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    std::copy(first, last, std::begin(mac_address));

    assert(first == last);
}

std::vector<uint8_t> MAC_Address_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(mac_address), std::cend(mac_address));

    return data;
}

std::string MAC_Address_Property::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(mac_address[0])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(mac_address[1])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(mac_address[2])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(mac_address[3])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(mac_address[4])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(mac_address[5]);

    return oss.str();
}

}
