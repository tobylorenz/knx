// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Message_Transmit_To_IP_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Message_Transmit_To_IP_Property::Message_Transmit_To_IP_Property() :
    Data_Property(IP_Parameter_Object::PID_MSG_TRANSMIT_TO_IP)
{
    description.property_datatype = PDT_UNSIGNED_LONG;
}

void Message_Transmit_To_IP_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    telegrams_transmitted_to_ip = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Message_Transmit_To_IP_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((telegrams_transmitted_to_ip >> 24) & 0xff);
    data.push_back((telegrams_transmitted_to_ip >> 16) & 0xff);
    data.push_back((telegrams_transmitted_to_ip >> 8) & 0xff);
    data.push_back((telegrams_transmitted_to_ip >> 0) & 0xff);

    return data;
}

std::string Message_Transmit_To_IP_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << telegrams_transmitted_to_ip;

    return oss.str();
}

}
