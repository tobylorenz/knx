// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Multicast_Latency_Tolerance_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Multicast_Latency_Tolerance_Property::Multicast_Latency_Tolerance_Property() :
    Data_Property(IP_Parameter_Object::PID_MULTICAST_LATENCY_TOLERANCE)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Multicast_Latency_Tolerance_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 2);

    // @todo Multicast_Latency_Tolerance_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Multicast_Latency_Tolerance_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Multicast_Latency_Tolerance_Property::toData

    return data;
}

std::string Multicast_Latency_Tolerance_Property::text() const
{
    // @todo Multicast_Latency_Tolerance_Property::text
    return "";
}

}
