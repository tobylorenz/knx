// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Password_Hashes_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Password_Hashes_Property::Password_Hashes_Property() :
    Data_Property(IP_Parameter_Object::PID_PASSWORD_HASHES)
{
    description.property_datatype = PDT_GENERIC_16;
}

void Password_Hashes_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Password_Hashes_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Password_Hashes_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Password_Hashes_Property::toData

    return data;
}

std::string Password_Hashes_Property::text() const
{
    // @todo Password_Hashes_Property::text
    return "";
}

}
