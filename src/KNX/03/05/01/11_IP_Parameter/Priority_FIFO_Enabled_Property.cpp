// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Priority_FIFO_Enabled_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Priority_FIFO_Enabled_Property::Priority_FIFO_Enabled_Property() :
    Data_Property(IP_Parameter_Object::PID_PRIORITY_FIFO_ENABLED)
{
    description.property_datatype = PDT_BINARY_INFORMATION;
}

void Priority_FIFO_Enabled_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    priority_fifo_enabled = *first++ & 0x01;

    assert(first == last);
}

std::vector<uint8_t> Priority_FIFO_Enabled_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(priority_fifo_enabled << 0);

    return data;
}

std::string Priority_FIFO_Enabled_Property::text() const
{
    return priority_fifo_enabled ? "enabled" : "disabled";
}

}
