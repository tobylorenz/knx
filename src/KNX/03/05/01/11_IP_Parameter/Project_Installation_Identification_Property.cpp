// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Project_Installation_Identification_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Project_Installation_Identification_Property::Project_Installation_Identification_Property() :
    Data_Property(IP_Parameter_Object::PID_PROJECT_INSTALLATION_ID)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Project_Installation_Identification_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    project_number = (*first++ << 4) | (*first >> 4);
    installation_number = (*first & 0x0f);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Project_Installation_Identification_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(project_number >> 4);
    data.push_back(
        ((project_number & 0x0f) << 4) |
        installation_number);

    return data;
}

std::string Project_Installation_Identification_Property::text() const
{
    std::ostringstream oss;

    oss << "Project Number: "
        << std::dec << project_number
        << ", Installation Number: "
        << std::dec << static_cast<uint16_t>(installation_number);

    return oss.str();
}

}
