// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Project Installation Identification
 *
 * @ingroup KNX_03_08_03_02_05_02
 */
class Project_Installation_Identification_Property :
    public Data_Property
{
public:
    Project_Installation_Identification_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** project number */
    uint12_t project_number{};

    /** installation number */
    uint4_t installation_number{};
};

}
