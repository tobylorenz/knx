// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Queue_Overflow_To_IP_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Queue_Overflow_To_IP_Property::Queue_Overflow_To_IP_Property() :
    Data_Property(IP_Parameter_Object::PID_QUEUE_OVERFLOW_TO_IP)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Queue_Overflow_To_IP_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    queue_overflow_to_ip = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Queue_Overflow_To_IP_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(queue_overflow_to_ip >> 8);
    data.push_back(queue_overflow_to_ip & 0xff);

    return data;
}

std::string Queue_Overflow_To_IP_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << queue_overflow_to_ip;

    return oss.str();
}

}
