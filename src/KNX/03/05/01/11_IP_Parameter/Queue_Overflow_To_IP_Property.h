// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Queue Overflow to IP
 *
 * @ingroup KNX_03_08_03_02_05_23
 */
class Queue_Overflow_To_IP_Property :
    public Data_Property
{
public:
    Queue_Overflow_To_IP_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Queue Overflow to IP */
    uint16_t queue_overflow_to_ip{};
};

}
