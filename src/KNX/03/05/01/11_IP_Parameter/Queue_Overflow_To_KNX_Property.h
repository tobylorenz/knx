// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Queue Overflow to KNX
 *
 * @ingroup KNX_03_08_03_02_05_24
 */
class Queue_Overflow_To_KNX_Property :
    public Data_Property
{
public:
    Queue_Overflow_To_KNX_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Queue Overflow to KNX */
    uint16_t queue_overflow_to_knx{};
};

}
