// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Routing_Busy_Wait_Time_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Routing_Busy_Wait_Time_Property::Routing_Busy_Wait_Time_Property() :
    Data_Property(IP_Parameter_Object::PID_ROUTING_BUSY_WAIT_TIME)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Routing_Busy_Wait_Time_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    routing_busy_wait_time = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Routing_Busy_Wait_Time_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(routing_busy_wait_time >> 8);
    data.push_back(routing_busy_wait_time & 0xff);

    return data;
}

std::string Routing_Busy_Wait_Time_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << routing_busy_wait_time << " ms";

    return oss.str();
}

}
