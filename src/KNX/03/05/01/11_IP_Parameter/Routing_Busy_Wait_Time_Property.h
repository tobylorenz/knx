// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Routing Busy Wait Time
 *
 * @ingroup KNX_03_08_03_02_05_28
 */
class Routing_Busy_Wait_Time_Property :
    public Data_Property
{
public:
    Routing_Busy_Wait_Time_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Routing Busy Wait Time */
    uint16_t routing_busy_wait_time{100}; // ms
};

}
