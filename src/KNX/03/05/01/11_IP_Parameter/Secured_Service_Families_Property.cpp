// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Secured_Service_Families_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Secured_Service_Families_Property::Secured_Service_Families_Property() :
    Function_Property(IP_Parameter_Object::PID_SECURED_SERVICE_FAMILIES)
{
}

}
