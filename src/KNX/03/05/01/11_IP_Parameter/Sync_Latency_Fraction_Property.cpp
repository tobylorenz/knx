// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Sync_Latency_Fraction_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Sync_Latency_Fraction_Property::Sync_Latency_Fraction_Property() :
    Data_Property(IP_Parameter_Object::PID_SYNC_LATENCY_FRACTION)
{
    description.property_datatype = PDT_SCALING;
}

void Sync_Latency_Fraction_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Sync_Latency_Fraction_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Sync_Latency_Fraction_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Sync_Latency_Fraction_Property::toData

    return data;
}

std::string Sync_Latency_Fraction_Property::text() const
{
    // @todo Sync_Latency_Fraction_Property::text
    return "";
}

}
