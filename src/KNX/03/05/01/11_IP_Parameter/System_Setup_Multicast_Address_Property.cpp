// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/System_Setup_Multicast_Address_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

System_Setup_Multicast_Address_Property::System_Setup_Multicast_Address_Property() :
    Data_Property(IP_Parameter_Object::PID_SYSTEM_SETUP_MULTICAST_ADDRESS)
{
    description.property_datatype = PDT_UNSIGNED_LONG;
}

void System_Setup_Multicast_Address_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    std::copy(first, last, std::begin(system_setup_multicast_address));

    assert(first == last);
}

std::vector<uint8_t> System_Setup_Multicast_Address_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(system_setup_multicast_address), std::cend(system_setup_multicast_address));

    return data;
}

std::string System_Setup_Multicast_Address_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(system_setup_multicast_address[0])
        << "."
        << std::dec << static_cast<uint16_t>(system_setup_multicast_address[1])
        << "."
        << std::dec << static_cast<uint16_t>(system_setup_multicast_address[2])
        << "."
        << std::dec << static_cast<uint16_t>(system_setup_multicast_address[3]);

    return oss.str();
}

}
