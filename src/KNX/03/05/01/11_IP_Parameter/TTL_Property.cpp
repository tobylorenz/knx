// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/TTL_Property.h>

#include <sstream>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

TTL_Property::TTL_Property() :
    Data_Property(IP_Parameter_Object::PID_TTL)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void TTL_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    ttl = *first++;

    assert(first == last);
}

std::vector<uint8_t> TTL_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(ttl);

    return data;
}

std::string TTL_Property::text() const
{
    std::stringstream oss;

    oss << std::dec << static_cast<uint16_t>(ttl);

    return oss.str();
}

}
