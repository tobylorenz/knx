// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/11_IP_Parameter/Tunnelling_Addresses_Property.h>

#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Tunnelling_Addresses_Property::Tunnelling_Addresses_Property() :
    Data_Property(IP_Parameter_Object::PID_TUNNELLING_ADDRESSES)
{
    description.property_datatype = PDT_UNSIGNED_LONG; // @todo datatype?
}

void Tunnelling_Addresses_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Tunnelling_Addresses_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Tunnelling_Addresses_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Tunnelling_Addresses_Property::toData

    return data;
}

std::string Tunnelling_Addresses_Property::text() const
{
    // @todo Tunnelling_Addresses_Property::text
    return "";
}

}
