// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/13_File_Server/File_Command_Property.h>

#include <KNX/03/05/01/13_File_Server/File_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

File_Command_Property::File_Command_Property() :
    Function_Property(File_Server_Object::PID_FILE_COMMAND)
{
}

std::string get_Media_Type(const File_Command_Property::Media_Type media_type, const File_Command_Property::Media_Subtype media_subtype)
{
    switch (media_type) {
    case 0x00:
        /* text */
        switch (media_subtype) {
        case 0x00:
            return "text/plain";
        case 0x01:
            return "text/html";
        case 0x02:
            return "text/short-html-200";
        case 0x03:
            return "text/short-html-320";
        case 0x04:
            return "text/short-html-480";
        case 0x05:
            return "text/short-html-640";
        case 0x06:
            return "text/short-html-800";
        case 0x07:
            return "text/short-html-1024";
        default:
            return "text/reserved";
        }
        break;
    case 0x01:
        /* image */
        switch (media_subtype) {
        case 0x00:
            return "image/jpeg";
        case 0x01:
            return "image/gif";
        case 0x02:
            return "image/png";
        case 0x03:
            return "image/tiff";
        case 0x04:
            return "image/g3fax";
        default:
            return "image/reserved";
        }
        break;
    case 0x02:
        /* audio */
        switch (media_subtype) {
        case 0x00:
            return "audio/basic";
        default:
            return "audio/reserved";
        }
        break;
    case 0x03:
        /* video */
        switch (media_subtype) {
        case 0x00:
            return "video/mpeg";
        default:
            return "video/reserved";
        }
        break;
    case 0x04:
        /* application */
        switch (media_subtype) {
        case 0x00:
            return "application/octet-stream";
        case 0x01:
            return "application/postscript";
        case 0x02:
            return "application/pdf";
        case 0x03:
            return "application/zip";
        default:
            return "application/reserved";
        }
        break;
    }
    return "reserved";
}

}
