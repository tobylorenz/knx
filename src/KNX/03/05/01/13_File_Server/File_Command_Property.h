// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/File_Handle.h>
#include <KNX/03/04/01/Function_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Property File Command
 *
 * @ingroup KNX_03_05_01_04_08_03
 */
class File_Command_Property :
    public Function_Property
{
public:
    File_Command_Property();

    /**
     * Return Code
     *
     * @ingroup KNX_03_05_01_04_08_03_02
     */
    enum class Return_Code : uint8_t {
        /* Command independent Return Codes */

        /** Command successful */
        Command_successful = 0x00,

        // Reserved = 0x01 .. 0x0f,

        /** Invalid command */
        Invalid_command = 0x10,

        // Reserved = 0x11 .. 0x1f,

        /* Command dependent Return Codes */

        /** Object busy */
        Object_busy = 0x20,

        /** Command not possible */
        Command_not_possible = 0x21,

        /** Not found */
        Not_found = 0x22,

        /** Write protected */
        Write_protected = 0x23,

        /** Access denied */
        Access_denied = 0x24,

        /** No more space available */
        No_more_space_available = 0x25,

        /** Directory not empty */
        Directory_not_empty = 0x26,

        // Reserved = 0x27 .. 0xff,
    };

    /**
     * Command
     *
     * @ingroup KNX_03_05_01_04_08_03_03
     */
    enum class Command : uint8_t {
        // Reserved = 0x00,

        /** Get File Handle */
        Get_File_Handle = 0x01,

        // Reserved = 0x02 .. 0x10,

        /** Retrieve File */
        Retrieve_File = 0x11,

        /** Store File */
        Store_File = 0x12,

        /** List Directory */
        List_Directory = 0x13,

        /** Rename From */
        Rename_From = 0x14,

        /** Rename To */
        Rename_To = 0x15,

        /** Delete */
        Delete = 0x16,

        /** Remove Directory */
        Remove_Directory = 0x17,

        /** Make Directory */
        Make_Directory = 0x18,

        // Reserved = 0x19 .. 0x1c,

        /** Get File Size */
        Get_File_Size = 0x1d,

        /** Get Empty Disk Space */
        Get_Empty_Disk_Space = 0x1e,

        /** Abort */
        Abort = 0x1f,

        // Reserved = 0x20,

        /** Get File */
        Get_File = 0x21,

        /** Post File */
        Post_File = 0x22,

        // Reserved = 0x23 .. 0xFF,
    };

    /** File Handle */
    //using File_Handle = File_Handle;

    /** File Size */
    using File_Size = uint32_t;

    /** Disk Space */
    using Disk_Space = uint32_t;

    /** Media Type */
    using Media_Type = uint8_t;

    /** Media Subtype */
    using Media_Subtype = uint8_t;
};

/**
 * get Content-Types from Media-Type and -Subtype
 *
 * @ingroup KNX_03_05_01_04_08_03_03_05
 *
 * @param media_type Media-Type
 * @param media_subtype Media-Subtype
 * @return Content-Type
 */
std::string get_Media_Type(const File_Command_Property::Media_Type media_type, const File_Command_Property::Media_Subtype media_subtype);

}
