// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/13_File_Server/File_Path_Property.h>

#include <KNX/03/05/01/13_File_Server/File_Server_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

File_Path_Property::File_Path_Property() :
    Data_Property(File_Server_Object::PID_FILE_PATH)
{
    description.property_datatype = PDT_UTF8; // alt. PDT_UNSIGNED_CHAR
}

void File_Path_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 1);

    length = *first++;

    file_path.assign(first, last);
}

std::vector<uint8_t> File_Path_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(length);
    data.insert(std::cend(data), std::begin(file_path), std::end(file_path));

    return data;
}

std::string File_Path_Property::text() const
{
    return file_path;
}

}
