// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Property File Path
 *
 * @ingroup KNX_03_05_01_04_08_04
 */
class File_Path_Property :
    public Data_Property
{
public:
    File_Path_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Length of File Path */
    uint8_t length{};

    /** File Path */
    std::string file_path{};
};

}
