// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/13_File_Server/File_Server_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

File_Server_Object::File_Server_Object() :
    System_Interface_Object(OT_FILE_SERVER)
{
}

std::shared_ptr<Property> File_Server_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_MAX_APDU_LENGTH_OUT:
        return std::make_shared<Max_APDU_Length_Out_Property>();
    case PID_FILE_COMMAND:
        return std::make_shared<File_Command_Property>();
    case PID_FILE_PATH:
        return std::make_shared<File_Path_Property>();
    //    case PID_MAX_APDU_LENGTH:
    //        return std::make_shared<Max_APDU_Length_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Max_APDU_Length_Out_Property> File_Server_Object::max_apdu_length_out()
{
    std::shared_ptr<Property> property = property_by_id(File_Server_Object::PID_MAX_APDU_LENGTH);
    if (!property) {
        property = std::make_shared<Max_APDU_Length_Out_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Max_APDU_Length_Out_Property>(property);
}

std::shared_ptr<File_Command_Property> File_Server_Object::file_command()
{
    std::shared_ptr<Property> property = property_by_id(File_Server_Object::PID_FILE_COMMAND);
    if (!property) {
        property = std::make_shared<File_Command_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<File_Command_Property>(property);
}

std::shared_ptr<File_Path_Property> File_Server_Object::file_path()
{
    std::shared_ptr<Property> property = property_by_id(File_Server_Object::PID_FILE_PATH);
    if (!property) {
        property = std::make_shared<File_Path_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<File_Path_Property>(property);
}

//std::shared_ptr<Max_APDU_Length_Property> File_Server_Object::max_apdu_length() {
//    std::shared_ptr<Property> property = property_by_id(File_Server_Object::PID_MAX_APDU_LENGTH);
//    if (!property) {
//        property = std::make_shared<Max_APDU_Length_Property>();
//        const Property_Index property_index = properties.size();
//        properties[property_index] = property;
//    }
//    return std::dynamic_pointer_cast<Max_APDU_Length_Property>(property);
//}

}
