// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/13_File_Server/File_Command_Property.h>
#include <KNX/03/05/01/13_File_Server/File_Path_Property.h>
#include <KNX/03/05/01/13_File_Server/Max_APDU_Length_Out_Property.h>
//#include <KNX/03/05/01/13_File_Server/Max_APDU_Length_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * File Server Object (Object Type 13)
 *
 * @ingroup KNX_03_05_01_04_13
 */
class KNX_EXPORT File_Server_Object :
    public System_Interface_Object
{
public:
    explicit File_Server_Object();

    /**
     * File Server Object (Object Type 13)
     *
     * @ingroup KNX_03_05_01_04_08
     */
    enum : Property_Id {
        /** Max. APDU Length Out = 51 */
        PID_MAX_APDU_LENGTH_OUT = 51,

        /** File Command */
        PID_FILE_COMMAND = 52,

        /** File Path */
        PID_FILE_PATH = 53,

        /** Max. APDU-Length */
        PID_MAX_APDU_LENGTH = 56, // @todo Device_Object::PID_MAX_APDU_LENGTH
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<Max_APDU_Length_Out_Property> max_apdu_length_out();
    std::shared_ptr<File_Command_Property> file_command();
    std::shared_ptr<File_Path_Property> file_path();
    //    std::shared_ptr<Max_APDU_Length_Property> max_apdu_length();
};

}
