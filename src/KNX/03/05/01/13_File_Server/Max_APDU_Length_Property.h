// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Property Max. APDU-Length
 *
 * @ingroup KNX_03_05_01_04_08_05
 */
class Max_APDU_Length_Property :
    public Data_Property
{
public:
    Max_APDU_Length_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Max. APDU Length */
    uint16_t max_apdu_length{55};
};

}
