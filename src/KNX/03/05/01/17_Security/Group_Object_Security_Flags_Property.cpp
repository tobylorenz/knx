// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Group_Object_Security_Flags_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Group_Object_Security_Flags_Property::Group_Object_Security_Flags_Property() :
    Data_Property(Security_Object::PID_GO_SECURITY_FLAGS)
{
    description.property_datatype = PDT_GENERIC_01;
}

void Group_Object_Security_Flags_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    flags.assign(first, last);
}

std::vector<uint8_t> Group_Object_Security_Flags_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(flags), std::cend(flags));

    return data;
}

std::string Group_Object_Security_Flags_Property::text() const
{
    // @todo Group_Object_Security_Flags_Property::text
    return "";
}

}
