// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Group Object Security Flags
 *
 * @ingroup KNX_AN158_02_03_02_16
 */
class Group_Object_Security_Flags_Property :
    public Data_Property
{
public:
    Group_Object_Security_Flags_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Group Object Security Flags */
    std::vector<uint8_t> flags;
};

}
