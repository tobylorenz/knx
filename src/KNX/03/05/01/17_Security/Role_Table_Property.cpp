// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Role_Table_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Role_Table_Property::Role_Table_Property() :
    Data_Property(Security_Object::PID_ROLE_TABLE)
{
    description.property_datatype = PDT_UNSIGNED_CHAR; // @todo datatype?
}

void Role_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    while (first != last) {
        const Link_Index link_index = (*first++ << 8) | (*first++);
        const Role role = *first++;
        role_table[link_index] = role;
    }

    assert(first == last);
}

std::vector<uint8_t> Role_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    for (const std::pair<Link_Index, Role> entry : role_table) {
        const Link_Index & link_index = entry.first;
        const Role & role = entry.second;
        data.push_back(link_index >> 8);
        data.push_back(link_index & 0xff);
        data.push_back(role);
    }

    return data;
}

std::string Role_Table_Property::text() const
{
    // @todo Role_Table_Property::text
    return "";
}

}
