// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <map>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Role Table
 *
 * @ingroup KNX_AN158_02_03_02_15
 */
class Role_Table_Property :
    public Data_Property
{
public:
    Role_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Link Index */
    using Link_Index = uint16_t;

    /** Role */
    using Role = uint8_t;

    /** Role Table */
    std::map<Link_Index, Role> role_table;
};

}
