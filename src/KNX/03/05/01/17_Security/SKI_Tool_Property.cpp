// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/SKI_Tool_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

SKI_Tool_Property::SKI_Tool_Property() :
    Data_Property(Security_Object::PID_SKI_TOOL)
{
    description.property_datatype = PDT_GENERIC_16;
}

void SKI_Tool_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 16);

    std::copy(first, first + 16, std::begin(key));
    first += 16;

    assert(first == last);
}

std::vector<uint8_t> SKI_Tool_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(key), std::cend(key));

    return data;
}

std::string SKI_Tool_Property::text() const
{
    // @todo SKI_Tool_Property::text
    return "";
}

}
