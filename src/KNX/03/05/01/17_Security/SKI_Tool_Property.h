// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Security Tool Key
 *
 * @ingroup KNX_AN158_02_03_02_10
 */
class SKI_Tool_Property :
    public Data_Property
{
public:
    SKI_Tool_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Security Tool Key */
    std::array<uint8_t, 16> key;
};

}
