// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Security_Failures_Log_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Security_Failures_Log_Property::Security_Failures_Log_Property() :
    Function_Property(Security_Object::PID_SECURITY_FAILURES_LOG)
{
}

}
