// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Security_Individual_Address_Table_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Security_Individual_Address_Table_Property::Security_Individual_Address_Table_Property() :
    Data_Property(Security_Object::PID_SECURITY_INDIVIDUAL_ADDRESS_TABLE)
{
    description.property_datatype = PDT_GENERIC_08;
}

void Security_Individual_Address_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    while (first != last) {
        Individual_Address individual_address;
        individual_address.fromData(first, first + 2);
        first += 2;
        const Security_Sequence_Number last_valid_seqnr =
            (static_cast<uint64_t>(*first++) << 40) |
            (static_cast<uint64_t>(*first++) << 32) |
            (static_cast<uint64_t>(*first++) << 24) |
            (static_cast<uint64_t>(*first++) << 16) |
            (static_cast<uint64_t>(*first++) << 8) |
            (static_cast<uint64_t>(*first++) << 0);
        table[individual_address] = last_valid_seqnr;
    }

    assert(first == last);
}

std::vector<uint8_t> Security_Individual_Address_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    for (const std::pair<Individual_Address, Security_Sequence_Number> & entry : table) {
        const Individual_Address & individual_address = entry.first;
        const Security_Sequence_Number & last_valid_seqnr = entry.second;
        data.push_back(individual_address >> 8);
        data.push_back(individual_address & 0xff);
        data.push_back((last_valid_seqnr >> 40) & 0xff);
        data.push_back((last_valid_seqnr >> 32) & 0xff);
        data.push_back((last_valid_seqnr >> 24) & 0xff);
        data.push_back((last_valid_seqnr >> 16) & 0xff);
        data.push_back((last_valid_seqnr >> 8) & 0xff);
        data.push_back((last_valid_seqnr >> 0) & 0xff);
    }

    return data;
}

std::string Security_Individual_Address_Table_Property::text() const
{
    // @todo Security_Individual_Address_Table_Property::text
    return "";
}

}
