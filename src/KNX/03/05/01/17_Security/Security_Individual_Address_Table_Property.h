// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <map>

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/05/01/17_Security/Security_Sequence_Number.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Security Individual Address Table
 *
 * @ingroup KNX_AN158_02_03_08
 */
class Security_Individual_Address_Table_Property :
    public Data_Property
{
public:
    Security_Individual_Address_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Security Individual Address Table */
    std::map<Individual_Address, Security_Sequence_Number> table;
};

}
