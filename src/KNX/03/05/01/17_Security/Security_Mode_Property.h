// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Function_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Security Mode
 *
 * @ingroup KNX_AN158_02_03_02_05
 */
class Security_Mode_Property :
    public Function_Property
{
public:
    Security_Mode_Property();
};

}
