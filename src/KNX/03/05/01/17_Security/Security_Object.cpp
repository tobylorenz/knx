// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Security_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

Security_Object::Security_Object() :
    System_Interface_Object(OT_SECURITY)
{
}

std::shared_ptr<Property> Security_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_SECURITY_MODE:
        return std::make_shared<Security_Mode_Property>();
    case PID_P2P_KEY_TABLE:
        return std::make_shared<P2P_Key_Table_Property>();
    case PID_GRP_KEY_TABLE:
        return std::make_shared<Group_Key_Table_Property>();
    case PID_SECURITY_INDIVIDUAL_ADDRESS_TABLE:
        return std::make_shared<Security_Individual_Address_Table_Property>();
    case PID_SECURITY_FAILURES_LOG:
        return std::make_shared<Security_Failures_Log_Property>();
    case PID_SKI_TOOL:
        return std::make_shared<SKI_Tool_Property>();
    case PID_SECURITY_REPORT:
        return std::make_shared<Security_Report_Property>();
    case PID_SECURITY_REPORT_CONTROL:
        return std::make_shared<Security_Report_Control_Property>();
    case PID_SEQUENCE_NUMBER_SENDING:
        return std::make_shared<Sequence_Number_Sending_Property>();
    case PID_ZONE_KEYS_TABLE:
        return std::make_shared<Zone_Keys_Table_Property>();
    case PID_GO_SECURITY_FLAGS:
        return std::make_shared<Group_Object_Security_Flags_Property>();
    case PID_ROLE_TABLE:
        return std::make_shared<Role_Table_Property>();
    case PID_TOOL_SEQUENCE_NUMBER_SENDING:
        return std::make_shared<Tool_Sequence_Number_Sending_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Security_Mode_Property> Security_Object::security_mode()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_SECURITY_MODE);
    if (!property) {
        property = std::make_shared<Security_Mode_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Security_Mode_Property>(property);
}

std::shared_ptr<P2P_Key_Table_Property> Security_Object::p2p_key_table()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_P2P_KEY_TABLE);
    if (!property) {
        property = std::make_shared<P2P_Key_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<P2P_Key_Table_Property>(property);
}

std::shared_ptr<Group_Key_Table_Property> Security_Object::group_key_table()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_GRP_KEY_TABLE);
    if (!property) {
        property = std::make_shared<Group_Key_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Group_Key_Table_Property>(property);
}

std::shared_ptr<Security_Individual_Address_Table_Property> Security_Object::security_individual_address_table()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_SECURITY_INDIVIDUAL_ADDRESS_TABLE);
    if (!property) {
        property = std::make_shared<Security_Individual_Address_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Security_Individual_Address_Table_Property>(property);
}

std::shared_ptr<Security_Failures_Log_Property> Security_Object::security_failures_log()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_SECURITY_FAILURES_LOG);
    if (!property) {
        property = std::make_shared<Security_Failures_Log_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Security_Failures_Log_Property>(property);
}

std::shared_ptr<SKI_Tool_Property> Security_Object::ski_tool()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_SKI_TOOL);
    if (!property) {
        property = std::make_shared<SKI_Tool_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<SKI_Tool_Property>(property);
}

std::shared_ptr<Security_Report_Property> Security_Object::security_report()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_SECURITY_REPORT);
    if (!property) {
        property = std::make_shared<Security_Report_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Security_Report_Property>(property);
}

std::shared_ptr<Security_Report_Control_Property> Security_Object::security_report_control()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_SECURITY_REPORT_CONTROL);
    if (!property) {
        property = std::make_shared<Security_Report_Control_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Security_Report_Control_Property>(property);
}

std::shared_ptr<Sequence_Number_Sending_Property> Security_Object::sequence_number_sending()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_SEQUENCE_NUMBER_SENDING);
    if (!property) {
        property = std::make_shared<Sequence_Number_Sending_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Sequence_Number_Sending_Property>(property);
}

std::shared_ptr<Zone_Keys_Table_Property> Security_Object::zone_keys_table()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_ZONE_KEYS_TABLE);
    if (!property) {
        property = std::make_shared<Zone_Keys_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Zone_Keys_Table_Property>(property);
}

std::shared_ptr<Group_Object_Security_Flags_Property> Security_Object::group_object_security_flags()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_GO_SECURITY_FLAGS);
    if (!property) {
        property = std::make_shared<Group_Object_Security_Flags_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Group_Object_Security_Flags_Property>(property);
}

std::shared_ptr<Role_Table_Property> Security_Object::role_table()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_ROLE_TABLE);
    if (!property) {
        property = std::make_shared<Role_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Role_Table_Property>(property);
}

std::shared_ptr<Tool_Sequence_Number_Sending_Property> Security_Object::tool_sequence_number_sending()
{
    std::shared_ptr<Property> property = property_by_id(Security_Object::PID_TOOL_SEQUENCE_NUMBER_SENDING);
    if (!property) {
        property = std::make_shared<Tool_Sequence_Number_Sending_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Tool_Sequence_Number_Sending_Property>(property);
}

}
