// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/03/05/01/17_Security/Group_Key_Table_Property.h>
#include <KNX/03/05/01/17_Security/Group_Object_Security_Flags_Property.h>
#include <KNX/03/05/01/17_Security/P2P_Key_Table_Property.h>
#include <KNX/03/05/01/17_Security/Role_Table_Property.h>
#include <KNX/03/05/01/17_Security/Security_Failures_Log_Property.h>
#include <KNX/03/05/01/17_Security/Security_Individual_Address_Table_Property.h>
#include <KNX/03/05/01/17_Security/Security_Mode_Property.h>
#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/05/01/17_Security/Security_Report_Control_Property.h>
#include <KNX/03/05/01/17_Security/Security_Report_Property.h>
#include <KNX/03/05/01/17_Security/Sequence_Number_Sending_Property.h>
#include <KNX/03/05/01/17_Security/SKI_Tool_Property.h>
#include <KNX/03/05/01/17_Security/Tool_Sequence_Number_Sending_Property.h>
#include <KNX/03/05/01/17_Security/Zone_Keys_Table_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Security Object (Object Type 17)
 *
 * @ingroup KNX_AN158_02_03_02
 */
class KNX_EXPORT Security_Object :
    public System_Interface_Object
{
public:
    explicit Security_Object();

    enum : Property_Id {
        PID_SECURITY_MODE = 51,
        PID_P2P_KEY_TABLE = 52,
        PID_GRP_KEY_TABLE = 53,
        PID_SECURITY_INDIVIDUAL_ADDRESS_TABLE = 54,
        PID_SECURITY_FAILURES_LOG = 55,
        PID_SKI_TOOL = 56,
        PID_SECURITY_REPORT = 57,
        PID_SECURITY_REPORT_CONTROL = 58,
        PID_SEQUENCE_NUMBER_SENDING = 59,
        PID_ZONE_KEYS_TABLE = 60,
        PID_GO_SECURITY_FLAGS = 61,
        PID_ROLE_TABLE = 62,

        PID_TOOL_SEQUENCE_NUMBER_SENDING = 250
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<Security_Mode_Property> security_mode();
    std::shared_ptr<P2P_Key_Table_Property> p2p_key_table();
    std::shared_ptr<Group_Key_Table_Property> group_key_table();
    std::shared_ptr<Security_Individual_Address_Table_Property> security_individual_address_table();
    std::shared_ptr<Security_Failures_Log_Property> security_failures_log();
    std::shared_ptr<SKI_Tool_Property> ski_tool();
    std::shared_ptr<Security_Report_Property> security_report();
    std::shared_ptr<Security_Report_Control_Property> security_report_control();
    std::shared_ptr<Sequence_Number_Sending_Property> sequence_number_sending();
    std::shared_ptr<Zone_Keys_Table_Property> zone_keys_table();
    std::shared_ptr<Group_Object_Security_Flags_Property> group_object_security_flags();
    std::shared_ptr<Role_Table_Property> role_table();
    std::shared_ptr<Tool_Sequence_Number_Sending_Property> tool_sequence_number_sending();
};

}
