// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Security_Report_Control_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Security_Report_Control_Property::Security_Report_Control_Property() :
    Data_Property(Security_Object::PID_SECURITY_REPORT_CONTROL)
{
    description.property_datatype = PDT_BINARY_INFORMATION;
}

void Security_Report_Control_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    control.fromData(first, first + 1);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Security_Report_Control_Property::toData() const
{
    return control.toData();
}

std::string Security_Report_Control_Property::text() const
{
    return control.text();
}

}
