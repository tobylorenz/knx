// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/07/02/1/DPT_1_003.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Security Report Control
 *
 * @ingroup KNX_AN158_02_03_02_12
 */
class Security_Report_Control_Property :
    public Data_Property
{
public:
    Security_Report_Control_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Security Report Control */
    DPT_Enable control;
};

}
