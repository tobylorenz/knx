// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Security_Report_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Security_Report_Property::Security_Report_Property() :
    Data_Property(Security_Object::PID_SECURITY_REPORT)
{
    description.property_datatype = PDT_BITSET8;
}

void Security_Report_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    report.fromData(first, first + 1);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Security_Report_Property::toData() const
{
    return report.toData();
}

std::string Security_Report_Property::text() const
{
    return report.text();
}

}
