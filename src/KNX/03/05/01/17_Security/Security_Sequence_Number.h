// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/** Security Sequence Number */
using Security_Sequence_Number = uint48_t;

}
