// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Sequence_Number_Sending_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Sequence_Number_Sending_Property::Sequence_Number_Sending_Property() :
    Data_Property(Security_Object::PID_SEQUENCE_NUMBER_SENDING)
{
    description.property_datatype = PDT_GENERIC_06;
}

void Sequence_Number_Sending_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    number =
        (static_cast<uint64_t>(*first++) << 40) |
        (static_cast<uint64_t>(*first++) << 32) |
        (static_cast<uint64_t>(*first++) << 24) |
        (static_cast<uint64_t>(*first++) << 16) |
        (static_cast<uint64_t>(*first++) << 8) |
        (static_cast<uint64_t>(*first++) << 0);

    assert(first == last);
}

std::vector<uint8_t> Sequence_Number_Sending_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((number >> 40) & 0xff);
    data.push_back((number >> 32) & 0xff);
    data.push_back((number >> 24) & 0xff);
    data.push_back((number >> 16) & 0xff);
    data.push_back((number >> 8) & 0xff);
    data.push_back((number >> 0) & 0xff);

    return data;
}

std::string Sequence_Number_Sending_Property::text() const
{
    // @todo Sequence_Number_Sending_Property::text
    return "";
}

}
