// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/05/01/17_Security/Security_Sequence_Number.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Sequence Number Sending
 *
 * @ingroup KNX_AN158_02_03_02_14
 */
class Sequence_Number_Sending_Property :
    public Data_Property
{
public:
    Sequence_Number_Sending_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Sequence Number Sending */
    Security_Sequence_Number number;
};

}
