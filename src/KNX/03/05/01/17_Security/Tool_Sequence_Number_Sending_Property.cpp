// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Tool_Sequence_Number_Sending_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Tool_Sequence_Number_Sending_Property::Tool_Sequence_Number_Sending_Property() :
    Data_Property(Security_Object::PID_TOOL_SEQUENCE_NUMBER_SENDING)
{
    description.property_datatype = PDT_UNSIGNED_CHAR; // @todo datatype?
}

void Tool_Sequence_Number_Sending_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Tool_Sequence_Number_Sending_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Tool_Sequence_Number_Sending_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Tool_Sequence_Number_Sending_Property::toData

    return data;
}

std::string Tool_Sequence_Number_Sending_Property::text() const
{
    // @todo Tool_Sequence_Number_Sending_Property::text
    return "";
}

}
