// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/17_Security/Zone_Keys_Table_Property.h>

#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Zone_Keys_Table_Property::Zone_Keys_Table_Property() :
    Data_Property(Security_Object::PID_ZONE_KEYS_TABLE)
{
    description.property_datatype = PDT_GENERIC_19;
}

void Zone_Keys_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 19);

    // @todo Zone_Keys_Table_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Zone_Keys_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Zone_Keys_Table_Property::toData

    return data;
}

std::string Zone_Keys_Table_Property::text() const
{
    // @todo Zone_Keys_Table_Property::text
    return "";
}

}
