// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/Fast_Ack_Activate_Property.h>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Fast_Ack_Activate_Property::Fast_Ack_Activate_Property() :
    Data_Property(RF_Medium_Object::PID_FAST_ACK_ACTIVATE)
{
    description.property_datatype = PDT_BINARY_INFORMATION;
}

void Fast_Ack_Activate_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    activation = *first++;

    assert(first == last);
}

std::vector<uint8_t> Fast_Ack_Activate_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(activation);

    return data;
}

std::string Fast_Ack_Activate_Property::text() const
{
    return activation ? "activate" : "deactivate";
}

}
