// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/Fast_Ack_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Fast_Ack_Property::Fast_Ack_Property() :
    Data_Property(RF_Medium_Object::PID_FAST_ACK)
{
    description.property_datatype = PDT_GENERIC_02;
}

void Fast_Ack_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    enable_disable = *first++;
    fast_ack_info_octet = *first++;

    assert(first == last);
}

std::vector<uint8_t> Fast_Ack_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(enable_disable);
    data.push_back(fast_ack_info_octet);

    return data;
}

std::string Fast_Ack_Property::text() const
{
    std::ostringstream oss;

    oss << "Enable/Disable: ";
    switch (enable_disable) {
    case 0:
        oss << "Disable";
        break;
    case 1:
        oss << "Enable";
        break;
    default:
        oss << "Reserved";
        break;
    }

    oss << ", Fast Ack Info Octet: "
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(fast_ack_info_octet);

    return oss.str();
}

}
