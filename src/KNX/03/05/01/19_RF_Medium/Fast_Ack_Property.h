// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Fast Ack
 *
 * @ingroup KNX_03_05_01_04_09_11
 */
class Fast_Ack_Property :
    public Data_Property
{
public:
    Fast_Ack_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Enable/Disable */
    uint8_t enable_disable{};

    /** Fast Ack Info Octet */
    uint8_t fast_ack_info_octet{};
};

}
