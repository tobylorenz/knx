// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_BiDir_Timeout_Property.h>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_BiDir_Timeout_Property::RF_BiDir_Timeout_Property() :
    Function_Property(RF_Medium_Object::PID_RF_BIDIR_TIMEOUT),
    Network_Parameter_Property()
{
}

}
