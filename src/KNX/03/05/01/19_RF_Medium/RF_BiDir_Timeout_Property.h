// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Function_Property.h>
#include <KNX/03/04/01/Network_Parameter_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Bidirectional Mode Time-out
 *
 * @ingroup KNX_AN160_02_03_02_08
 */
class RF_BiDir_Timeout_Property :
    public Function_Property,
    public Network_Parameter_Property
{
public:
    RF_BiDir_Timeout_Property();
};

}
