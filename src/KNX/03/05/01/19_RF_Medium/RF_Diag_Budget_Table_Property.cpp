// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_Diag_Budget_Table_Property.h>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Diag_Budget_Table_Property::RF_Diag_Budget_Table_Property() :
    Data_Property(RF_Medium_Object::PID_RF_DIAG_BUDGET_TABLE)
{
    description.property_datatype = PDT_GENERIC_04;
}

void RF_Diag_Budget_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    while (first != last) {
        Budget_Entry budget_entry{};

        budget_entry.nr_of_telegrams = *first++;
        budget_entry.min = *first++;
        budget_entry.max = *first++;
        budget_entry.average = *first++;

        budget_table.push_back(budget_entry);
    }

    assert(first == last);
}

std::vector<uint8_t> RF_Diag_Budget_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    for (const auto & budget_entry : budget_table) {
        data.push_back(budget_entry.nr_of_telegrams);
        data.push_back(budget_entry.min);
        data.push_back(budget_entry.max);
        data.push_back(budget_entry.average);
    }

    return data;
}

std::string RF_Diag_Budget_Table_Property::text() const
{
    // @todo RF_Diag_Budget_Table_Property::text
    return "";
}

}
