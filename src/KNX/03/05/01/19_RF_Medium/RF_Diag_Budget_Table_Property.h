// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Diagnose Budget Table
 *
 * @ingroup KNX_AN160_02_03_02_10
 */
class RF_Diag_Budget_Table_Property :
    public Data_Property
{
public:
    RF_Diag_Budget_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    struct Budget_Entry {
        /** Nr of Telegrams */
        uint8_t nr_of_telegrams{};

        /** Min (dBm) */
        uint8_t min{};

        /** Max (dBm) */
        uint8_t max{};

        /** Average (dBm) */
        uint8_t average{};
    };

    std::vector<Budget_Entry> budget_table{};
};

}
