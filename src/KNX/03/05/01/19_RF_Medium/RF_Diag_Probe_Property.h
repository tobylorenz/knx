// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Function_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Diagnose Probe
 *
 * @ingroup KNX_AN160_02_03_02_11
 */
class RF_Diag_Probe_Property :
    public Function_Property
{
public:
    RF_Diag_Probe_Property();
};

}
