// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_Diag_Source_Address_Filter_Table_Property.h>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Diag_Source_Address_Filter_Table_Property::RF_Diag_Source_Address_Filter_Table_Property() :
    Data_Property(RF_Medium_Object::PID_RF_DIAG_SA_FILTER_TABLE)
{
    description.property_datatype = PDT_GENERIC_03;
}

void RF_Diag_Source_Address_Filter_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    while (first != last) {
        Filter_Entry filter_entry{};

        filter_entry.repeat_count_value = *first & 0x07;
        filter_entry.check_repeat_count = (*first >> 3) & 0x01;
        ++first;

        filter_entry.individual_address.fromData(first, first + 2);
        first += 2;

        filter_table.push_back(filter_entry);
    }

    assert(first == last);
}

std::vector<uint8_t> RF_Diag_Source_Address_Filter_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    for (const auto & filter_entry : filter_table) {
        data.push_back(
            (filter_entry.repeat_count_value << 0) |
            (filter_entry.check_repeat_count << 3));
        data.push_back(filter_entry.individual_address >> 8);
        data.push_back(filter_entry.individual_address & 0xff);
    }

    return data;
}

std::string RF_Diag_Source_Address_Filter_Table_Property::text() const
{
    // @todo RF_Diag_Source_Address_Filter_Table_Property::text
    return "";
}

}
