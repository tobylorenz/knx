// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Diagnose Source Address Filter Table
 *
 * @ingroup KNX_AN160_02_03_02_09
 */
class RF_Diag_Source_Address_Filter_Table_Property :
    public Data_Property
{
public:
    RF_Diag_Source_Address_Filter_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    struct Filter_Entry {
        /** Repeat count value */
        uint3_t repeat_count_value{};

        /** Check repeat count */
        bool check_repeat_count{false};

        /** Individual Address */
        Individual_Address individual_address{};
    };

    std::vector<Filter_Entry> filter_table{};
};

}
