// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_Filtering_Mode_Selection_Property.h>

#include <sstream>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Filtering_Mode_Selection_Property::RF_Filtering_Mode_Selection_Property() :
    Data_Property(RF_Medium_Object::PID_RF_FILTERING_MODE_SELECT)
{
    description.property_datatype = PDT_ENUM8; // alt. PDT_UNSIGNED_CHAR
}

void RF_Filtering_Mode_Selection_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    rf_filtering_mode_selection = *first++;

    assert(first == last);
}

std::vector<uint8_t> RF_Filtering_Mode_Selection_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(rf_filtering_mode_selection);

    return data;
}

std::string RF_Filtering_Mode_Selection_Property::text() const
{
    std::ostringstream oss;

    switch (rf_filtering_mode_selection) {
    case 0:
        oss << "no filtering";
        break;
    case 1:
        oss << "filtering by Domain Address";
        break;
    case 2:
        oss << "filtering by KNX Serial Number table";
        break;
    case 3:
        oss << "filtering by Domain Address and by KNX Serial number table";
        break;
    }

    return oss.str();
}

}
