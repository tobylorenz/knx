// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

RF_Medium_Object::RF_Medium_Object() :
    System_Interface_Object(OT_RF_MEDIUM)
{
}

std::shared_ptr<Property> RF_Medium_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    case PID_RF_MULTI_TYPE:
        return std::make_shared<RF_Multi_Type_Property>();
    case PID_RF_MULTI_PHYSICAL_FEATURES:
        return std::make_shared<RF_Multi_Physical_Features_Property>();
    case PID_RF_MULTI_CALL_CHANNEL:
        return std::make_shared<RF_Multi_Call_Channel_Property>();
    case PID_RF_MULTI_OBJECT_LINK:
        return std::make_shared<RF_Multi_Object_Link_Property>();
    case PID_RF_MULTI_EXT_GA_REPEATED:
        return std::make_shared<RF_Multi_Extended_Group_Address_Property>();
    //    case PID_RF_DOMAIN_ADDRESS:
    //        return std::make_shared<RF_Domain_Address_Property>();
    case PID_RF_RETRANSMITTER:
        return std::make_shared<RF_Retransmitter_Property>();
    case PID_RF_FILTERING_MODE_SUPPORT:
        return std::make_shared<Supported_RF_Filtering_Modes_Property>();
    case PID_RF_FILTERING_MODE_SELECT:
        return std::make_shared<RF_Filtering_Mode_Selection_Property>();
    case PID_RF_BIDIR_TIMEOUT:
        return std::make_shared<RF_BiDir_Timeout_Property>();
    case PID_RF_DIAG_SA_FILTER_TABLE:
        return std::make_shared<RF_Diag_Source_Address_Filter_Table_Property>();
    case PID_RF_DIAG_BUDGET_TABLE:
        return std::make_shared<RF_Diag_Budget_Table_Property>();
    case PID_RF_DIAG_PROBE:
        return std::make_shared<RF_Diag_Probe_Property>();
    case PID_TRANSMISSION_MODE:
        return std::make_shared<Transmission_Mode_Property>();
    case PID_RECEPTION_MODE:
        return std::make_shared<Reception_Mode_Property>();
    case PID_TEST_SIGNAL:
        return std::make_shared<Test_Signal_Property>();
    case PID_FAST_ACK:
        return std::make_shared<Fast_Ack_Property>();
    case PID_FAST_ACK_ACTIVATE:
        return std::make_shared<Fast_Ack_Activate_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<RF_Multi_Type_Property> RF_Medium_Object::rf_multi_type()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_MULTI_TYPE);
    if (!property) {
        property = std::make_shared<RF_Multi_Type_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Multi_Type_Property>(property);
}

std::shared_ptr<RF_Multi_Physical_Features_Property> RF_Medium_Object::rf_multi_physical_features()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_MULTI_PHYSICAL_FEATURES);
    if (!property) {
        property = std::make_shared<RF_Multi_Physical_Features_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Multi_Physical_Features_Property>(property);
}

std::shared_ptr<RF_Multi_Call_Channel_Property> RF_Medium_Object::rf_multi_call_channel()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_MULTI_CALL_CHANNEL);
    if (!property) {
        property = std::make_shared<RF_Multi_Call_Channel_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Multi_Call_Channel_Property>(property);
}

std::shared_ptr<RF_Multi_Object_Link_Property> RF_Medium_Object::rf_multi_object_link()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_MULTI_OBJECT_LINK);
    if (!property) {
        property = std::make_shared<RF_Multi_Object_Link_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Multi_Object_Link_Property>(property);
}

std::shared_ptr<RF_Multi_Extended_Group_Address_Property> RF_Medium_Object::rf_multi_extended_group_address_repeated()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_MULTI_EXT_GA_REPEATED);
    if (!property) {
        property = std::make_shared<RF_Multi_Extended_Group_Address_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Multi_Extended_Group_Address_Property>(property);
}

//std::shared_ptr<RF_Domain_Address_Property> RF_Medium_Object::rf_domain_address() {
//    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_DOMAIN_ADDRESS);
//    if (!property) {
//        property = std::make_shared<RF_Domain_Address_Property>();
//        const Property_Index property_index = properties.size();
//        properties[property_index] = property;
//    }
//    return std::dynamic_pointer_cast<RF_Domain_Address_Property>(property);
//}

std::shared_ptr<RF_Retransmitter_Property> RF_Medium_Object::rf_retransmitter()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_RETRANSMITTER);
    if (!property) {
        property = std::make_shared<RF_Retransmitter_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Retransmitter_Property>(property);
}

std::shared_ptr<Supported_RF_Filtering_Modes_Property> RF_Medium_Object::supported_rf_filtering_modes()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_FILTERING_MODE_SUPPORT);
    if (!property) {
        property = std::make_shared<Supported_RF_Filtering_Modes_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Supported_RF_Filtering_Modes_Property>(property);
}

std::shared_ptr<RF_Filtering_Mode_Selection_Property> RF_Medium_Object::rf_filtering_mode_select()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_FILTERING_MODE_SELECT);
    if (!property) {
        property = std::make_shared<RF_Filtering_Mode_Selection_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Filtering_Mode_Selection_Property>(property);
}

std::shared_ptr<RF_BiDir_Timeout_Property> RF_Medium_Object::rf_bidir_timeout()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_BIDIR_TIMEOUT);
    if (!property) {
        property = std::make_shared<RF_BiDir_Timeout_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_BiDir_Timeout_Property>(property);
}

std::shared_ptr<RF_Diag_Source_Address_Filter_Table_Property> RF_Medium_Object::rf_diag_source_address_filter_table()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_DIAG_SA_FILTER_TABLE);
    if (!property) {
        property = std::make_shared<RF_Diag_Source_Address_Filter_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Diag_Source_Address_Filter_Table_Property>(property);
}

std::shared_ptr<RF_Diag_Budget_Table_Property> RF_Medium_Object::rf_diag_quality_table()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_DIAG_BUDGET_TABLE);
    if (!property) {
        property = std::make_shared<RF_Diag_Budget_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Diag_Budget_Table_Property>(property);
}

std::shared_ptr<RF_Diag_Probe_Property> RF_Medium_Object::rf_diag_probe()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RF_DIAG_PROBE);
    if (!property) {
        property = std::make_shared<RF_Diag_Probe_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<RF_Diag_Probe_Property>(property);
}

std::shared_ptr<Transmission_Mode_Property> RF_Medium_Object::transmission_mode()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_TRANSMISSION_MODE);
    if (!property) {
        property = std::make_shared<Transmission_Mode_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Transmission_Mode_Property>(property);
}

std::shared_ptr<Reception_Mode_Property> RF_Medium_Object::reception_mode()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_RECEPTION_MODE);
    if (!property) {
        property = std::make_shared<Reception_Mode_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Reception_Mode_Property>(property);
}

std::shared_ptr<Test_Signal_Property> RF_Medium_Object::test_signal()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_TEST_SIGNAL);
    if (!property) {
        property = std::make_shared<Test_Signal_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Test_Signal_Property>(property);
}

std::shared_ptr<Fast_Ack_Property> RF_Medium_Object::fast_ack()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_FAST_ACK);
    if (!property) {
        property = std::make_shared<Fast_Ack_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Fast_Ack_Property>(property);
}

std::shared_ptr<Fast_Ack_Activate_Property> RF_Medium_Object::fast_ack_activate()
{
    std::shared_ptr<Property> property = property_by_id(RF_Medium_Object::PID_FAST_ACK_ACTIVATE);
    if (!property) {
        property = std::make_shared<Fast_Ack_Activate_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Fast_Ack_Activate_Property>(property);
}

}
