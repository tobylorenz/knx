// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/19_RF_Medium/Fast_Ack_Activate_Property.h>
#include <KNX/03/05/01/19_RF_Medium/Fast_Ack_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_BiDir_Timeout_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Diag_Probe_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Diag_Budget_Table_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Diag_Source_Address_Filter_Table_Property.h>
//#include <KNX/03/05/01/19_RF_Medium/RF_Domain_Address_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Filtering_Mode_Selection_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Multi_Call_Channel_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Multi_Extended_Group_Address_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Multi_Object_Link_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Multi_Physical_Features_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Multi_Type_Property.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Retransmitter_Property.h>
#include <KNX/03/05/01/19_RF_Medium/Reception_Mode_Property.h>
#include <KNX/03/05/01/19_RF_Medium/Supported_RF_Filtering_Modes_Property.h>
#include <KNX/03/05/01/19_RF_Medium/Test_Signal_Property.h>
#include <KNX/03/05/01/19_RF_Medium/Transmission_Mode_Property.h>
#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Medium Object (Object Type 19)
 *
 * @ingroup KNX_03_05_01_04_09
 * @ingroup KNX_AN160_02_03_02
 */
class KNX_EXPORT RF_Medium_Object :
    public System_Interface_Object
{
public:
    explicit RF_Medium_Object();

    /**
     * RF Medium Object for cEMI (Object Type 19)
     *
     * @ingroup KNX_03_05_01_04_09
     * @ingroup KNX_AN160_02_03_02
     */
    enum : Property_Id {
        /** RF Multi Type */
        PID_RF_MULTI_TYPE = 51,

        /** RF Multi Physical Features */
        PID_RF_MULTI_PHYSICAL_FEATURES = 52,

        /** RF Multi Call Channel */
        PID_RF_MULTI_CALL_CHANNEL = 53,

        /** RF Multi Object Link */
        PID_RF_MULTI_OBJECT_LINK = 54,

        /** RD Multi Extended Group Address Repeated */
        PID_RF_MULTI_EXT_GA_REPEATED = 55,

        /** RF Domain Address */
        PID_RF_DOMAIN_ADDRESS = 56, // @todo Domain_Object::PID_RF_DOMAIN_ADDRESS

        /** RF Retransmitter Flag */
        PID_RF_RETRANSMITTER = 57,

        /** RF filtering on KNX Serial Number or RF DoA */
        PID_RF_FILTERING_MODE_SUPPORT = 58,

        /** RF Filtering Mode Selection */
        PID_RF_FILTERING_MODE_SELECT = 59,

        /** Time-out for bidirectional communication */
        PID_RF_BIDIR_TIMEOUT = 60,

        /** Filter Table for Source Addresses for diagnostics of Link Budget */
        PID_RF_DIAG_SA_FILTER_TABLE = 61,

        /** Result of Link Budget diagnostics */
        PID_RF_DIAG_BUDGET_TABLE = 62,

        /** Trigger and response for Link Budget diagnostic help telegrams */
        PID_RF_DIAG_PROBE = 63,

        /** Transmission Mode */
        PID_TRANSMISSION_MODE = 70,

        /** Reception Mode */
        PID_RECEPTION_MODE = 71,

        /** Test Signal */
        PID_TEST_SIGNAL = 72,

        /** Fast Ack */
        PID_FAST_ACK = 73,

        /** Fast Ack Activate */
        PID_FAST_ACK_ACTIVATE = 74
    };

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;

    std::shared_ptr<RF_Multi_Type_Property> rf_multi_type();
    std::shared_ptr<RF_Multi_Physical_Features_Property> rf_multi_physical_features();
    std::shared_ptr<RF_Multi_Call_Channel_Property> rf_multi_call_channel();
    std::shared_ptr<RF_Multi_Object_Link_Property> rf_multi_object_link();
    std::shared_ptr<RF_Multi_Extended_Group_Address_Property> rf_multi_extended_group_address_repeated();
    //    std::shared_ptr<RF_Domain_Address_Property> rf_domain_address();
    std::shared_ptr<RF_Retransmitter_Property> rf_retransmitter();
    std::shared_ptr<Supported_RF_Filtering_Modes_Property> supported_rf_filtering_modes();
    std::shared_ptr<RF_Filtering_Mode_Selection_Property> rf_filtering_mode_select();
    std::shared_ptr<RF_BiDir_Timeout_Property> rf_bidir_timeout();
    std::shared_ptr<RF_Diag_Source_Address_Filter_Table_Property> rf_diag_source_address_filter_table();
    std::shared_ptr<RF_Diag_Budget_Table_Property> rf_diag_quality_table();
    std::shared_ptr<RF_Diag_Probe_Property> rf_diag_probe();
    std::shared_ptr<Transmission_Mode_Property> transmission_mode();
    std::shared_ptr<Reception_Mode_Property> reception_mode();
    std::shared_ptr<Test_Signal_Property> test_signal();
    std::shared_ptr<Fast_Ack_Property> fast_ack();
    std::shared_ptr<Fast_Ack_Activate_Property> fast_ack_activate();
};

}
