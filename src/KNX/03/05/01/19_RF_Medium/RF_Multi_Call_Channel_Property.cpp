// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_Multi_Call_Channel_Property.h>

#include <sstream>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Multi_Call_Channel_Property::RF_Multi_Call_Channel_Property() :
    Data_Property(RF_Medium_Object::PID_RF_MULTI_CALL_CHANNEL)
{
    description.property_datatype = PDT_GENERIC_01;
}

void RF_Multi_Call_Channel_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    slow_frequency_call_channel = (*first >> 0) & 0x03;
    fast_frequency_call_channel = (*first >> 2) & 0x03;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> RF_Multi_Call_Channel_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (slow_frequency_call_channel << 0) |
        (fast_frequency_call_channel << 2));

    return data;
}

std::string RF_Multi_Call_Channel_Property::text() const
{
    std::ostringstream oss;

    oss << "Slow Frequency Call Channel: ";
    switch (slow_frequency_call_channel) {
    case 0:
        oss << "S1";
        break;
    case 1:
        oss << "S2";
        break;
    case 2:
    case 3:
        oss << "reserved";
        break;
    }

    oss << ", Fast Frequency Call Channel: ";
    switch (fast_frequency_call_channel) {
    case 0:
        oss << "F1";
        break;
    case 1:
        oss << "F2";
        break;
    case 2:
        oss << "F3";
        break;
    case 3:
        oss << "reserved";
        break;
    }

    return oss.str();
}

}
