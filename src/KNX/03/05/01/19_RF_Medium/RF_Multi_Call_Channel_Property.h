// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Multi Call Channel
 *
 * @ingroup KNX_03_05_01_04_09_05
 */
class RF_Multi_Call_Channel_Property :
    public Data_Property
{
public:
    RF_Multi_Call_Channel_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Slow Frequency Call Channel */
    uint2_t slow_frequency_call_channel{};

    /** Fast Frequency Call Channel */
    uint2_t fast_frequency_call_channel{};
};

}
