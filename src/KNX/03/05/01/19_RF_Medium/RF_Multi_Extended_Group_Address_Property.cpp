// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_Multi_Extended_Group_Address_Property.h>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Multi_Extended_Group_Address_Property::RF_Multi_Extended_Group_Address_Property() :
    Function_Property(RF_Medium_Object::PID_RF_MULTI_EXT_GA_REPEATED)
{
}

}
