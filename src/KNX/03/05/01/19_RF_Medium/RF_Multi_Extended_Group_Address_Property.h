// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Function_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Multi Extended Group Address Repeated
 *
 * @ingroup KNX_03_05_01_04_09_07
 */
class RF_Multi_Extended_Group_Address_Property :
    public Function_Property
{
public:
    RF_Multi_Extended_Group_Address_Property();
};

}
