// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_Multi_Physical_Features_Property.h>

#include <sstream>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Multi_Physical_Features_Property::RF_Multi_Physical_Features_Property() :
    Data_Property(RF_Medium_Object::PID_RF_MULTI_PHYSICAL_FEATURES)
{
    description.property_datatype = PDT_BITSET8;
}

void RF_Multi_Physical_Features_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    scan_fx = (*first >> 0) & 0x01;
    scan_sx = (*first >> 1) & 0x01;
    tx_fx = (*first >> 2) & 0x01;
    tx_sx = (*first >> 3) & 0x01;
    phys_ack = (*first >> 4) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> RF_Multi_Physical_Features_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (scan_fx << 0) |
        (scan_sx << 1) |
        (tx_fx << 2) |
        (tx_sx << 3) |
        (phys_ack << 4));

    return data;
}

std::string RF_Multi_Physical_Features_Property::text() const
{
    std::ostringstream oss;

    oss << "Scan Fx: " << (scan_fx ? "1" : "0")
        << ", Scan Sx: " << (scan_sx ? "1" : "0")
        << ", TxFx: " << (tx_fx ? "1" : "0")
        << ", TxSx: " << (tx_sx ? "1" : "0")
        << ", PhysAck: " << (phys_ack ? "1" : "0");

    return oss.str();
}

}
