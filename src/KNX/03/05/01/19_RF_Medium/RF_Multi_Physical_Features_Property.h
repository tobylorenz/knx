// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Multi Physical Features
 *
 * @ingroup KNX_03_05_01_04_09_04
 */
class RF_Multi_Physical_Features_Property :
    public Data_Property
{
public:
    RF_Multi_Physical_Features_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Scan Fx */
    bool scan_fx{};

    /** Scan Sx */
    bool scan_sx{};

    /** TxFx */
    bool tx_fx{};

    /** TxSx */
    bool tx_sx{};

    /** PhysAck */
    bool phys_ack{};
};

}
