// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_Multi_Type_Property.h>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Multi_Type_Property::RF_Multi_Type_Property() :
    Data_Property(RF_Medium_Object::PID_RF_MULTI_TYPE)
{
    description.property_datatype = PDT_BITSET8; // PDT_GENERIC_01
}

void RF_Multi_Type_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    rf_multi_type = *first++;

    assert(first == last);
}

std::vector<uint8_t> RF_Multi_Type_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(rf_multi_type);

    return data;
}

std::string RF_Multi_Type_Property::text() const
{
    return rf_multi_type ? "KNX RF Multi type" : "KNX RF Ready type";
}

}
