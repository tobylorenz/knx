// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Multi Type
 *
 * @ingroup KNX_03_05_01_04_09_03
 * @ingroup KNX_AN160_02_03_02_03
 */
class RF_Multi_Type_Property :
    public Data_Property
{
public:
    RF_Multi_Type_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** RF-Multi Type */
    bool rf_multi_type{};
};

}
