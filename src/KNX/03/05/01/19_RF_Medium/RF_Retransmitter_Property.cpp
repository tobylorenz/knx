// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/RF_Retransmitter_Property.h>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

RF_Retransmitter_Property::RF_Retransmitter_Property() :
    Data_Property(RF_Medium_Object::PID_RF_RETRANSMITTER)
{
    description.property_datatype = PDT_BINARY_INFORMATION;
}

void RF_Retransmitter_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    rf_retransmitter_flag = *first & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> RF_Retransmitter_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(rf_retransmitter_flag);

    return data;
}

std::string RF_Retransmitter_Property::text() const
{
    return rf_retransmitter_flag ? "Enable" : "Disable";
}

}
