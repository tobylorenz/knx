// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Retransmitter Flag
 *
 * @ingroup KNX_AN160_02_03_02_05
 */
class RF_Retransmitter_Property :
    public Data_Property
{
public:
    RF_Retransmitter_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** RF Retransmitter Flag */
    bool rf_retransmitter_flag{false};
};

}
