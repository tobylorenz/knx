// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/Test_Signal_Property.h>

#include <sstream>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Test_Signal_Property::Test_Signal_Property() :
    Data_Property(RF_Medium_Object::PID_TEST_SIGNAL)
{
    description.property_datatype = PDT_GENERIC_03; // @note Spec says PDT_GENERIC_02, but shows 3 octets
}

void Test_Signal_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 3);

    signal_frequency = *first++;
    signal_form = *first++;
    signal_duration = *first++;

    assert(first == last);
}

std::vector<uint8_t> Test_Signal_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(signal_frequency);
    data.push_back(signal_form);
    data.push_back(signal_duration);

    return data;
}

std::string Test_Signal_Property::text() const
{
    std::ostringstream oss;

    oss << "Signal frequency: ";
    switch (signal_frequency) {
    case 0x00:
        oss << "stop the current carrier transmission and resumes reception";
        break;
    case 0x01:
        oss << "F1 Ready frequency (RF1.R)";
        break;
    case 0x02:
        oss << "F1 frequency (RF1.M)";
        break;
    case 0x03:
        oss << "F2 frequency (RF1.M)";
        break;
    case 0x04:
        oss << "F3 frequency (RF1.M)";
        break;
    case 0x05:
        oss << "S1 frequency (RF1.M)";
        break;
    case 0x06:
        oss << "S2 frequency (RF1.M)";
        break;
    case 0x0A:
        oss << "F1 Ready frequency (RF2.M)";
        break;
    case 0x0B:
        oss << "F1 frequency (RF2.M)";
        break;
    case 0x0C:
        oss << "F2 frequency (RF2.M)";
        break;
    case 0x0D:
        oss << "F3 frequency (RF2.M)";
        break;
    case 0x0E:
        oss << "S1 frequency (RF2.M)";
        break;
    case 0x0F:
        oss << "S2 frequency (RF2.M)";
        break;
    case 0x13:
        oss << "F1 frequency (RF5.M)";
        break;
    case 0x14:
        oss << "F2 frequency (RF5.M)";
        break;
    case 0x15:
        oss << "F3 frequency (RF5.M)";
        break;
    case 0x16:
        oss << "S1 frequency (RF5.M)";
        break;
    case 0x17:
        oss << "S2 frequency (RF5.M)";
        break;
    default:
        oss << "Reserved for future use of the KNX system";
        break;
    }

    oss << ", Signal form: ";
    switch (signal_form) {
    case 0:
        oss << "carrier on fc - fdev";
        break;
    case 1:
        oss << "carrier on fc + fdev";
        break;
    case 2:
        oss << "0/1 chips modulated signal";
        break;
    default:
        oss << "Reserved for future use of the KNX system";
        break;
    }

    oss << ", Signal duration: " << std::dec << static_cast<uint16_t>(signal_duration) << " s";

    return oss.str();
}

}
