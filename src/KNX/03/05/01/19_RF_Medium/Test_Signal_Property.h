// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Test Signal
 *
 * @ingroup KNX_03_05_01_04_09_10
 */
class Test_Signal_Property :
    public Data_Property
{
public:
    Test_Signal_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Signal frequency */
    uint8_t signal_frequency{};

    /** Signal form */
    uint8_t signal_form{};

    /** Signal duration */
    uint8_t signal_duration{};
};

}
