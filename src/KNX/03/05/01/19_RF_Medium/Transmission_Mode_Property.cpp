// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/19_RF_Medium/Transmission_Mode_Property.h>

#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Transmission_Mode_Property::Transmission_Mode_Property() :
    Data_Property(RF_Medium_Object::PID_TRANSMISSION_MODE)
{
    description.property_datatype = PDT_ENUM8;
}

void Transmission_Mode_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    transmission_mode = *first++;

    assert(first == last);
}

std::vector<uint8_t> Transmission_Mode_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(transmission_mode);

    return data;
}

std::string Transmission_Mode_Property::text() const
{
    switch (transmission_mode) {
    case 0x01:
        return "Transmission on the single F1 frequency (RF1.R)";
    case 0x02:
        return "Transmission on the single F1 frequency (RF1.M)";
    case 0x03:
        return "Transmission on the single F2 frequency (RF1.M)";
    case 0x04:
        return "Transmission on the single F3 frequency (RF1.M)";
    case 0x05:
        return "Transmission on the single S1 frequency (RF1.M)";
    case 0x06:
        return "Transmission on the single S2 frequency (RF1.M)";
    case 0x07:
        return "Transmission on the fast Fx frequencies (RF1.M)";
    case 0x08:
        return "Transmission on the slow Sx frequencies (RF1.M)";
    case 0x0A:
        return "Transmission on the single F1 frequency (RF2.R)";
    case 0x0B:
        return "Transmission on the single F1 frequency (RF2.M)";
    case 0x0C:
        return "Transmission on the single F2 frequency (RF2.M)";
    case 0x0D:
        return "Transmission on the single F3 frequency (RF2.M)";
    case 0x0E:
        return "Transmission on the single S1 frequency (RF2.M)";
    case 0x0F:
        return "Transmission on the single S2 frequency (RF2.M)";
    case 0x10:
        return "Transmission on the fast Fx frequencies (RF2.M)";
    case 0x11:
        return "Transmission on the slow Sx frequencies (RF2.M)";
    case 0x13:
        return "Transmission on the single F1 frequency (RF5.M)";
    case 0x14:
        return "Transmission on the single F2 frequency (RF5.M)";
    case 0x15:
        return "Transmission on the single F3 frequency (RF5.M)";
    case 0x16:
        return "Transmission on the single S1 frequency (RF5.M)";
    case 0x17:
        return "Transmission on the single S2 frequency (RF5.M)";
    case 0x18:
        return "Transmission on the fast Fx frequencies (RF5.M)";
    case 0x19:
        return "Transmission on the slow Sx frequencies (RF5.M)";
    default:
        break;
    }

    return "Reserved for future use of the KNX system";
}

}
