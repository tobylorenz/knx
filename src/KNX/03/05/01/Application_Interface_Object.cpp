// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Application_Interface_Object.h>

namespace KNX {

Application_Interface_Object::Application_Interface_Object(const Object_Type object_type) :
    Interface_Object(object_type)
{
}

}
