// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Application Interface Object
 *
 * @ingroup KNX_03_04_01_04_08
 */
class KNX_EXPORT Application_Interface_Object : public Interface_Object
{
public:
    explicit Application_Interface_Object(const Object_Type object_type);
};

}
