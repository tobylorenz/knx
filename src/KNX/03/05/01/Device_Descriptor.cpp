// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Device_Descriptor.h>

#include <KNX/03/05/01/Device_Descriptor_Type_0.h>
#include <KNX/03/05/01/Device_Descriptor_Type_2.h>

namespace KNX {

KNX_EXPORT std::shared_ptr<Device_Descriptor> make_Device_Descriptor(const Descriptor_Type descriptor_type, std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    std::shared_ptr<Device_Descriptor> device_descriptor;
    switch (descriptor_type) {
    case 0:
        device_descriptor = std::make_shared<Device_Descriptor_Type_0>();
        device_descriptor->fromData(first, last);
        break;
    case 2:
        device_descriptor = std::make_shared<Device_Descriptor_Type_2>();
        device_descriptor->fromData(first, last);
        break;
    }
    return device_descriptor;
}

}
