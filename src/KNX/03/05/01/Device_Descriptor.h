// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/03/07/Descriptor_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Device Descriptor
 *
 * @ingroup KNX_03_05_01_04_01
 */
class KNX_EXPORT Device_Descriptor
{
public:
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) = 0;
    virtual std::vector<uint8_t> toData() const = 0;
    virtual std::string text() const = 0;
};

/**
 * make Device_Descriptor
 *
 * @param[in] descriptor_type Device Descriptor Type
 * @param[in] first octet
 * @param[in] last octet
 * @return Device_Descriptor
 */
KNX_EXPORT std::shared_ptr<Device_Descriptor> make_Device_Descriptor(const Descriptor_Type descriptor_type, std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
