// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Device_Descriptor_Type_0.h>

#include <iomanip>
#include <sstream>

namespace KNX {

/* Mask_Type */

Mask_Type::Mask_Type(const uint8_t mask_type) :
    medium_type(static_cast<Medium_Type>((mask_type >> 4) & 0x0f)),
    firmware_type((mask_type >> 0) & 0x0f)
{
}

Mask_Type & Mask_Type::operator=(const uint8_t & mask_type)
{
    medium_type = static_cast<Medium_Type>((mask_type >> 4) & 0x0f);
    firmware_type = (mask_type >> 0) & 0x0f;

    return *this;
}

Mask_Type::operator uint8_t() const
{
    return
        (static_cast<uint8_t>(medium_type) << 4) |
        (static_cast<uint8_t>(firmware_type) << 0);
}

std::string Mask_Type::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex
        << static_cast<uint16_t>(operator uint8_t());

    return oss.str();
}

/* Firmware_Version */

Firmware_Version::Firmware_Version(const uint8_t firmware_version) :
    version((firmware_version >> 4) & 0x0f),
    subcode((firmware_version >> 0) & 0x0f)
{
}

Firmware_Version & Firmware_Version::operator=(const uint8_t & firmware_version)
{
    version = (firmware_version >> 4) & 0x0f;
    subcode = (firmware_version >> 0) & 0x0f;

    return *this;
}

Firmware_Version::operator uint8_t() const
{
    return
        (static_cast<uint8_t>(version) << 4) |
        (static_cast<uint8_t>(subcode) << 0);
}

std::string Firmware_Version::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex
        << static_cast<uint16_t>(operator uint8_t());

    return oss.str();
}

/* Device_Descriptor_Type_0 */

Device_Descriptor_Type_0::Device_Descriptor_Type_0(const uint16_t device_descriptor_type_0) :
    mask_type((device_descriptor_type_0 >> 8) & 0xff),
    firmware_version((device_descriptor_type_0 >> 0) & 0xff)
{
}

Device_Descriptor_Type_0 & Device_Descriptor_Type_0::operator=(const uint16_t & device_descriptor_type_0)
{
    mask_type = (device_descriptor_type_0 >> 8) & 0xff;
    firmware_version = (device_descriptor_type_0 >> 0) & 0xff;

    return *this;
}

Device_Descriptor_Type_0::operator uint16_t() const
{
    return
        (static_cast<uint16_t>(mask_type) << 8) |
        (static_cast<uint16_t>(firmware_version) << 0);
}

void Device_Descriptor_Type_0::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    mask_type = *first++;
    firmware_version = *first++;

    assert(first == last);
}

std::vector<uint8_t> Device_Descriptor_Type_0::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(mask_type);
    data.push_back(firmware_version);

    return data;
}

std::string Device_Descriptor_Type_0::text() const
{
    std::ostringstream oss;

    oss << mask_type.text() << firmware_version.text();

    return oss.str();
}

}
