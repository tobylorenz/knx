// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/Device_Descriptor.h>
#include <KNX/types.h>

namespace KNX {

/** Mask Type */
class KNX_EXPORT Mask_Type
{
public:
    Mask_Type() = default;
    explicit Mask_Type(const uint8_t mask_type);
    Mask_Type & operator=(const uint8_t & mask_type);
    operator uint8_t() const;

    virtual std::string text() const;

    /** Medium Type */
    enum class Medium_Type : uint8_t { // actually uint4_t
        /** Twisted Pair */
        TP1 = 0,

        /** PowerLine */
        PL110 = 1,

        RF = 2,

        TP0 = 3,

        PL132 = 4,

        /** Internet Protocol */
        IP = 5,
    };

    /** Medium Type */
    Medium_Type medium_type{Medium_Type::TP1};

    /** Firmware Type */
    uint4_t firmware_type{};
};

/** Firmware Version */
class KNX_EXPORT Firmware_Version
{
public:
    Firmware_Version() = default;
    explicit Firmware_Version(const uint8_t firmware_version);
    Firmware_Version & operator=(const uint8_t & firmware_version);
    operator uint8_t() const;
    virtual std::string text() const;

    /** Version */
    uint4_t version{};

    /** Subcode */
    uint4_t subcode{};
};

/**
 * Device Descriptor Type 0 (mask version)
 *
 * @ingroup KNX_03_05_01_04_01_02
 */
class KNX_EXPORT Device_Descriptor_Type_0 :
    public Device_Descriptor
{
public:
    Device_Descriptor_Type_0() = default;
    explicit Device_Descriptor_Type_0(const uint16_t device_descriptor_type_0);
    Device_Descriptor_Type_0 & operator=(const uint16_t & device_descriptor_type_0);
    operator uint16_t() const;

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Mask Type */
    Mask_Type mask_type{};

    /** Firmware Version */
    Firmware_Version firmware_version{};
};

using Mask_Version = Device_Descriptor_Type_0;

}
