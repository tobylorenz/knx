// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Device_Descriptor_Type_2.h>

#include <cassert>
#include <iomanip>
#include <sstream>

namespace KNX {

void Device_Descriptor_Type_2::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 14);

    application_manufacturer = (*first++ << 8) | *first++;

    device_type = (*first << 8) | *first++;

    version = *first++;
    miscellaneous = *first >> 6;
    lt_base = (*first++) & 0x7f;

    channel_info_1 = (*first++ << 8) | *first++;

    channel_info_2 = (*first++ << 8) | *first++;

    channel_info_3 = (*first++ << 8) | *first++;

    channel_info_4 = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> Device_Descriptor_Type_2::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(application_manufacturer >> 8);
    data.push_back(application_manufacturer & 0xff);

    data.push_back(device_type >> 8);
    data.push_back(device_type & 0xff);

    data.push_back(version);

    data.push_back((miscellaneous >> 6) | (lt_base & 0x7f));

    data.push_back(channel_info_1 >> 8);
    data.push_back(channel_info_1 & 0xff);

    data.push_back(channel_info_2 >> 8);
    data.push_back(channel_info_2 & 0xff);

    data.push_back(channel_info_3 >> 8);
    data.push_back(channel_info_3 & 0xff);

    data.push_back(channel_info_4 >> 8);
    data.push_back(channel_info_4 & 0xff);

    return data;
}

std::string Device_Descriptor_Type_2::text() const
{
    std::ostringstream oss;

    oss << "Application Manufacturer: " << std::setfill('0') << std::setw(4) << std::hex << application_manufacturer
        << ", Device Type: " << std::setfill('0') << std::setw(4) << std::hex << device_type
        << ", Version: " << std::setfill('0') << std::setw(2) << std::hex << version
        << ", Misc: " << std::setfill('0') << std::setw(1) << std::hex << miscellaneous
        << ", LT Base: " << std::setfill('0') << std::setw(2) << std::hex << lt_base
        << ", Ch Info 1: " << std::setfill('0') << std::setw(2) << std::hex << channel_info_1
        << ", Ch Info 2: " << std::setfill('0') << std::setw(2) << std::hex << channel_info_2
        << ", Ch Info 3: " << std::setfill('0') << std::setw(2) << std::hex << channel_info_3
        << ", Ch Info 4: " << std::setfill('0') << std::setw(2) << std::hex << channel_info_4;

    return oss.str();
}

}
