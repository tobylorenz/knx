// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <string>

#include <KNX/03/05/01/Device_Descriptor.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Device Descriptor Type 2
 *
 * @ingroup KNX_03_05_01_04_01_03
 */
class KNX_EXPORT Device_Descriptor_Type_2 :
    public Device_Descriptor
{
public:
    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Application Manufacturer */
    uint16_t application_manufacturer{};

    /** Device Type */
    uint16_t device_type{};

    /** Version */
    uint8_t version{};

    /** Miscellaneous */
    uint2_t miscellaneous{};

    /** LT_Base */
    uint6_t lt_base{};

    /** Channel Info 1 */
    uint16_t channel_info_1{};

    /** Channel Info 2 */
    uint16_t channel_info_2{};

    /** Channel Info 3 */
    uint16_t channel_info_3{};

    /** Channel Info 4 */
    uint16_t channel_info_4{};
};

}
