// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/System_Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Easy Parameter Block Table (PaBT)
 *
 * @ingroup KNX_03_05_01_04_13
 */
class KNX_EXPORT Easy_Parameter_Block_Table
{
public:
};

}
