// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Interface_Object.h>

#include <KNX/03/05/01/Interface_Object.h>
#include <KNX/03/05/01/Unknown_Property.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Interface_Object::Interface_Object(const Object_Type object_type) :
    object_type(object_type)
{
}

std::shared_ptr<Property> Interface_Object::make_property(const Property_Id property_id) const
{
    assert(property_id <= 50);

    switch (property_id) {
    case PID_OBJECT_TYPE:
        return std::make_shared<Interface_Object_Type_Property>(object_type);
    case PID_OBJECT_NAME:
        return std::make_shared<Interface_Object_Name_Property>();
    case PID_SEMAPHOR:
        return std::make_shared<Semaphor_Property>();
    case PID_GROUP_OBJECT_REFERENCE:
        return std::make_shared<Group_Object_Reference_Property>();
    case PID_LOAD_STATE_CONTROL:
        return std::make_shared<Load_Control_Property>();
    case PID_RUN_STATE_CONTROL:
        return std::make_shared<Run_Control_Property>();
    case PID_TABLE_REFERENCE:
        return std::make_shared<Table_Reference_Property>();
    case PID_SERVICE_CONTROL:
        return std::make_shared<Service_Control_Property>();
    case PID_FIRMWARE_REVISION:
        return std::make_shared<Firmware_Revision_Property>();
    case PID_SERVICES_SUPPORTED:
        return std::make_shared<Services_Supported_Property>();
    case PID_SERIAL_NUMBER:
        return std::make_shared<Serial_Number_Property>();
    case PID_MANUFACTURER_ID:
        return std::make_shared<Manufacturer_Identifier_Property>();
    case PID_PROGRAM_VERSION:
        return std::make_shared<Application_Version_Property>();
    case PID_DEVICE_CONTROL:
        return std::make_shared<Device_Control_Property>();
    case PID_ORDER_INFO:
        return std::make_shared<Order_Info_Property>();
    case PID_PEI_TYPE:
        return std::make_shared<PEI_Type_Property>();
    case PID_PORT_CONFIGURATION:
        return std::make_shared<Port_Configuration_Property>();
    case PID_POLL_GROUP_SETTINGS:
        return std::make_shared<Polling_Group_Settings_Property>();
    case PID_MANUFACTURER_DATA:
        return std::make_shared<Manufacturer_Data_Property>();
    case PID_ENABLE:
        return std::make_shared<Enable_Property>();
    case PID_DESCRIPTION:
        return std::make_shared<Description_Property>();
    case PID_FILE:
        return std::make_shared<File_Property>();
    case PID_TABLE:
        return std::make_shared<Table_Property>();
    case PID_ENROL:
        return std::make_shared<Interface_Object_Link_Property>();
    case PID_VERSION:
        return std::make_shared<Version_Property>();
    case PID_GROUP_OBJECT_LINK:
        return std::make_shared<Group_Address_Assignment_Property>();
    case PID_MCB_TABLE:
        return std::make_shared<Memory_Control_Table_Property>();
    case PID_ERROR_CODE:
        return std::make_shared<Error_Code_Property>();
    case PID_OBJECT_INDEX:
        return std::make_shared<Object_Index_Property>();
    case PID_DOWNLOAD_COUNTER:
        return std::make_shared<Download_Counter_Property>();
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

std::shared_ptr<Property> Interface_Object::property_by_id(const Property_Id property_id)
{
    std::shared_ptr<Property> property;
    for (const auto & it : properties) {
        if (it.second->description.property_id == property_id) {
            property = it.second;
            break;
        }
    }
    return property;
}

std::shared_ptr<Property> Interface_Object::property_by_index(const Property_Index property_index)
{
    std::shared_ptr<Property> property;
    if (properties.count(property_index) > 0) {
        property = properties[property_index];
    }
    return property;
}

Property_Index Interface_Object::property_index(std::shared_ptr<Property> property) const
{
    for (const auto & it : properties) {
        if (it.second == property) {
            return it.first;
        }
    }
    return properties.size();
}

std::shared_ptr<Interface_Object_Type_Property> Interface_Object::interface_object_type()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_OBJECT_TYPE);
    if (!property) {
        property = std::make_shared<Interface_Object_Type_Property>(object_type);
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Interface_Object_Type_Property>(property);
}

std::shared_ptr<Interface_Object_Name_Property> Interface_Object::interface_object_name()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_OBJECT_NAME);
    if (!property) {
        property = std::make_shared<Interface_Object_Name_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Interface_Object_Name_Property>(property);
}

std::shared_ptr<Semaphor_Property> Interface_Object::semaphor()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_SEMAPHOR);
    if (!property) {
        property = std::make_shared<Semaphor_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Semaphor_Property>(property);
}

std::shared_ptr<Group_Object_Reference_Property> Interface_Object::group_object_reference()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_GROUP_OBJECT_REFERENCE);
    if (!property) {
        property = std::make_shared<Group_Object_Reference_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Group_Object_Reference_Property>(property);
}

std::shared_ptr<Load_Control_Property> Interface_Object::load_control()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_LOAD_STATE_CONTROL);
    if (!property) {
        property = std::make_shared<Load_Control_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Load_Control_Property>(property);
}

std::shared_ptr<Run_Control_Property> Interface_Object::run_control()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_RUN_STATE_CONTROL);
    if (!property) {
        property = std::make_shared<Run_Control_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Run_Control_Property>(property);
}

std::shared_ptr<Table_Reference_Property> Interface_Object::table_reference()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_TABLE_REFERENCE);
    if (!property) {
        property = std::make_shared<Table_Reference_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Table_Reference_Property>(property);
}

std::shared_ptr<Service_Control_Property> Interface_Object::service_control()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_SERVICE_CONTROL);
    if (!property) {
        property = std::make_shared<Service_Control_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Service_Control_Property>(property);
}

std::shared_ptr<Firmware_Revision_Property> Interface_Object::firmware_revision()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_FIRMWARE_REVISION);
    if (!property) {
        property = std::make_shared<Firmware_Revision_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Firmware_Revision_Property>(property);
}

std::shared_ptr<Services_Supported_Property> Interface_Object::services_supported()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_SERVICES_SUPPORTED);
    if (!property) {
        property = std::make_shared<Services_Supported_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Services_Supported_Property>(property);
}

std::shared_ptr<Serial_Number_Property> Interface_Object::serial_number()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_SERIAL_NUMBER);
    if (!property) {
        property = std::make_shared<Serial_Number_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Serial_Number_Property>(property);
}

std::shared_ptr<Manufacturer_Identifier_Property> Interface_Object::manufacturer_identifier()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_MANUFACTURER_ID);
    if (!property) {
        property = std::make_shared<Manufacturer_Identifier_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Manufacturer_Identifier_Property>(property);
}

std::shared_ptr<Application_Version_Property> Interface_Object::application_version()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_PROGRAM_VERSION);
    if (!property) {
        property = std::make_shared<Application_Version_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Application_Version_Property>(property);
}

std::shared_ptr<Device_Control_Property> Interface_Object::device_control()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_DEVICE_CONTROL);
    if (!property) {
        property = std::make_shared<Device_Control_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Device_Control_Property>(property);
}

std::shared_ptr<Order_Info_Property> Interface_Object::order_info()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_ORDER_INFO);
    if (!property) {
        property = std::make_shared<Order_Info_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Order_Info_Property>(property);
}

std::shared_ptr<PEI_Type_Property> Interface_Object::pei_type()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_PEI_TYPE);
    if (!property) {
        property = std::make_shared<PEI_Type_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<PEI_Type_Property>(property);
}

std::shared_ptr<Port_Configuration_Property> Interface_Object::port_configuration()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_PORT_CONFIGURATION);
    if (!property) {
        property = std::make_shared<Port_Configuration_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Port_Configuration_Property>(property);
}

std::shared_ptr<Polling_Group_Settings_Property> Interface_Object::polling_group_settings()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_POLL_GROUP_SETTINGS);
    if (!property) {
        property = std::make_shared<Polling_Group_Settings_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Polling_Group_Settings_Property>(property);
}

std::shared_ptr<Manufacturer_Data_Property> Interface_Object::manufacturer_data()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_MANUFACTURER_DATA);
    if (!property) {
        property = std::make_shared<Manufacturer_Data_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Manufacturer_Data_Property>(property);
}

std::shared_ptr<Enable_Property> Interface_Object::enable()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_ENABLE);
    if (!property) {
        property = std::make_shared<Enable_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Enable_Property>(property);
}

std::shared_ptr<Description_Property> Interface_Object::description()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_DESCRIPTION);
    if (!property) {
        property = std::make_shared<Description_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Description_Property>(property);
}

std::shared_ptr<File_Property> Interface_Object::file()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_FILE);
    if (!property) {
        property = std::make_shared<File_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<File_Property>(property);
}

std::shared_ptr<Table_Property> Interface_Object::table()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_TABLE);
    if (!property) {
        property = std::make_shared<Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Table_Property>(property);
}

std::shared_ptr<Interface_Object_Link_Property> Interface_Object::interface_object_link()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_ENROL);
    if (!property) {
        property = std::make_shared<Interface_Object_Link_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Interface_Object_Link_Property>(property);
}

std::shared_ptr<Version_Property> Interface_Object::version()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_VERSION);
    if (!property) {
        property = std::make_shared<Version_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Version_Property>(property);
}

std::shared_ptr<Group_Address_Assignment_Property> Interface_Object::group_address_assignment()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_GROUP_OBJECT_LINK);
    if (!property) {
        property = std::make_shared<Group_Address_Assignment_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Group_Address_Assignment_Property>(property);
}

std::shared_ptr<Memory_Control_Table_Property> Interface_Object::memory_control_table()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_MCB_TABLE);
    if (!property) {
        property = std::make_shared<Memory_Control_Table_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Memory_Control_Table_Property>(property);
}

std::shared_ptr<Error_Code_Property> Interface_Object::error_code()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_ERROR_CODE);
    if (!property) {
        property = std::make_shared<Error_Code_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Error_Code_Property>(property);
}

std::shared_ptr<Object_Index_Property> Interface_Object::object_index()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_OBJECT_INDEX);
    if (!property) {
        property = std::make_shared<Object_Index_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Object_Index_Property>(property);
}

std::shared_ptr<Download_Counter_Property> Interface_Object::download_counter()
{
    std::shared_ptr<Property> property = property_by_id(Interface_Object::PID_DOWNLOAD_COUNTER);
    if (!property) {
        property = std::make_shared<Download_Counter_Property>();
        const Property_Index property_index = properties.size();
        properties[property_index] = property;
    }
    return std::dynamic_pointer_cast<Download_Counter_Property>(property);
}

}
