// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <map>
#include <memory>

#include <KNX/03/03/02/Group_Address.h>
#include <KNX/03/03/07/Max_Nr_Of_Elem.h>
#include <KNX/03/03/07/Object_Type.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/05/01/generic/Application_Version_Property.h>
#include <KNX/03/05/01/generic/Description_Property.h>
#include <KNX/03/05/01/generic/Device_Control_Property.h>
#include <KNX/03/05/01/generic/Download_Counter_Property.h>
#include <KNX/03/05/01/generic/Enable_Property.h>
#include <KNX/03/05/01/generic/Error_Code_Property.h>
#include <KNX/03/05/01/generic/File_Property.h>
#include <KNX/03/05/01/generic/Firmware_Revision_Property.h>
#include <KNX/03/05/01/generic/Group_Address_Assignment_Property.h>
#include <KNX/03/05/01/generic/Group_Object_Reference_Property.h>
#include <KNX/03/05/01/generic/Interface_Object_Link_Property.h>
#include <KNX/03/05/01/generic/Interface_Object_Name_Property.h>
#include <KNX/03/05/01/generic/Interface_Object_Type_Property.h>
#include <KNX/03/05/01/generic/Load_Control_Property.h>
#include <KNX/03/05/01/generic/Manufacturer_Data_Property.h>
#include <KNX/03/05/01/generic/Manufacturer_Identifier_Property.h>
#include <KNX/03/05/01/generic/Memory_Control_Table_Property.h>
#include <KNX/03/05/01/generic/Object_Index_Property.h>
#include <KNX/03/05/01/generic/Order_Info_Property.h>
#include <KNX/03/05/01/generic/PEI_Type_Property.h>
#include <KNX/03/05/01/generic/Polling_Group_Settings_Property.h>
#include <KNX/03/05/01/generic/Port_Configuration_Property.h>
#include <KNX/03/05/01/generic/Run_Control_Property.h>
#include <KNX/03/05/01/generic/Semaphor_Property.h>
#include <KNX/03/05/01/generic/Serial_Number_Property.h>
#include <KNX/03/05/01/generic/Service_Control_Property.h>
#include <KNX/03/05/01/generic/Services_Supported_Property.h>
#include <KNX/03/05/01/generic/Table_Property.h>
#include <KNX/03/05/01/generic/Table_Reference_Property.h>
#include <KNX/03/05/01/generic/Version_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Interface Object
 *
 * @ingroup KNX_03_04_01_04_03
 * @ingroup KNX_03_05_01_04_02
 */
class KNX_EXPORT Interface_Object
{
public:
    explicit Interface_Object(const Object_Type object_type);
    virtual ~Interface_Object() = default;

    /**
     * Interface Object Type independent standard Properties
     *
     * @ingroup KNX_03_05_01_04_02
     * @ingroup KNX_03_07_03_03_02
     */
    enum : Property_Id {
        /** Interface Object Type */
        PID_OBJECT_TYPE = 1,

        /** Interface Object Name */
        PID_OBJECT_NAME = 2,

        /** Semaphor */
        PID_SEMAPHOR = 3,

        /** Group Object Reference */
        PID_GROUP_OBJECT_REFERENCE = 4,

        /** Load Control */
        PID_LOAD_STATE_CONTROL = 5,

        /** Run Control */
        PID_RUN_STATE_CONTROL = 6,

        /** Table Reference */
        PID_TABLE_REFERENCE = 7,

        /** Service Control */
        PID_SERVICE_CONTROL = 8,

        /** Firmware Revision */
        PID_FIRMWARE_REVISION = 9,

        /** Supported Services */
        PID_SERVICES_SUPPORTED = 10,

        /** KNX Serial Number */
        PID_SERIAL_NUMBER = 11,

        /** Manufacturer Identifier */
        PID_MANUFACTURER_ID = 12,

        /** Application Version */
        PID_PROGRAM_VERSION = 13, // formerly named PID_APPLICATION_VERSION

        /** Device Control */
        PID_DEVICE_CONTROL = 14,

        /** Order Info */
        PID_ORDER_INFO = 15,

        /** PEI Type */
        PID_PEI_TYPE = 16,

        /** PortADDR */
        PID_PORT_CONFIGURATION = 17,

        /** Polling Group Settings */
        PID_POLL_GROUP_SETTINGS = 18,

        /** Manufacturer Data */
        PID_MANUFACTURER_DATA = 19,

        PID_ENABLE = 20,

        /** Description */
        PID_DESCRIPTION = 21,

        PID_FILE = 22,

        /** Table */
        PID_TABLE = 23,

        /** Interface Object Link */
        PID_ENROL = 24,

        /** Version */
        PID_VERSION = 25,

        /** Group Address Assignment */
        PID_GROUP_OBJECT_LINK = 26,

        /** Memory Control Table */
        PID_MCB_TABLE = 27,

        /** Error Code */
        PID_ERROR_CODE = 28,

        /** Object Index */
        PID_OBJECT_INDEX = 29,

        /** Download Counter */
        PID_DOWNLOAD_COUNTER = 30,
    };

    const Object_Type object_type{};

    /** Properties */
    std::map<Property_Index, std::shared_ptr<Property>> properties{};

    virtual std::shared_ptr<Property> make_property(const Property_Id property_id) const;
    std::shared_ptr<Property> property_by_id(const Property_Id property_id);
    std::shared_ptr<Property> property_by_index(const Property_Index property_index);
    Property_Index property_index(std::shared_ptr<Property> property) const;

    std::shared_ptr<Interface_Object_Type_Property> interface_object_type();
    std::shared_ptr<Interface_Object_Name_Property> interface_object_name();
    std::shared_ptr<Semaphor_Property> semaphor();
    std::shared_ptr<Group_Object_Reference_Property> group_object_reference();
    std::shared_ptr<Load_Control_Property> load_control();
    std::shared_ptr<Run_Control_Property> run_control();
    std::shared_ptr<Table_Reference_Property> table_reference();
    std::shared_ptr<Service_Control_Property> service_control();
    std::shared_ptr<Firmware_Revision_Property> firmware_revision();
    std::shared_ptr<Services_Supported_Property> services_supported();
    std::shared_ptr<Serial_Number_Property> serial_number();
    std::shared_ptr<Manufacturer_Identifier_Property> manufacturer_identifier();
    std::shared_ptr<Application_Version_Property> application_version();
    std::shared_ptr<Device_Control_Property> device_control();
    std::shared_ptr<Order_Info_Property> order_info();
    std::shared_ptr<PEI_Type_Property> pei_type();
    std::shared_ptr<Port_Configuration_Property> port_configuration();
    std::shared_ptr<Polling_Group_Settings_Property> polling_group_settings();
    std::shared_ptr<Manufacturer_Data_Property> manufacturer_data();
    std::shared_ptr<Enable_Property> enable();
    std::shared_ptr<Description_Property> description();
    std::shared_ptr<File_Property> file();
    std::shared_ptr<Table_Property> table();
    std::shared_ptr<Interface_Object_Link_Property> interface_object_link();
    std::shared_ptr<Version_Property> version();
    std::shared_ptr<Group_Address_Assignment_Property> group_address_assignment();
    std::shared_ptr<Memory_Control_Table_Property> memory_control_table();
    std::shared_ptr<Error_Code_Property> error_code();
    std::shared_ptr<Object_Index_Property> object_index();
    std::shared_ptr<Download_Counter_Property> download_counter();
};

}
