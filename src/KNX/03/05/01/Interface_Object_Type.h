// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Object_Type.h>

namespace KNX {

/**
 * (Interface) Object Type
 *
 * @ingroup KNX_03_05_03_02_09_02_02_01
 */
enum : Object_Type {
    /** Device Object */
    OT_DEVICE = 0,

    /** Addresstable Object */
    OT_ADDRESS_TABLE = 1, // OIDX_ADDRSS_TABLE

    /** Associationtable Object */
    OT_ASSOCIATION_TABLE = 2, // OIDX_ASSOCIATION_TABLE

    /** Applicationprogram Object */
    OT_APPLICATION_PROGRAM = 3, // OIDX_APPLICATION_PROGRAM

    /** Interfaceprogram Object */
    OT_INTERFACE_PROGRAM = 4, // OIDX_PEI_PROGRAM

    /** KNX-Object Associationtable Object */
    OT_EIBOBJECT_ASSOCIATION_TABLE = 5,

    /** Router Object */
    OT_ROUTER = 6,

    /** LTE Address Routing Table Object */
    OT_LTE_ADDRESS_ROUTING_TABLE = 7,

    /** cEMI Server Object */
    OT_CEMI_SERVER = 8,

    /** Group Object Table Object */
    OT_GROUP_OBJECT_TABLE = 9,

    /** Polling Master */
    OT_POLLING_MASTER = 10,

    /** KNXnet/IP Parameter Object */
    OT_KNXIP_PARAMETER = 11,

    // 12

    /** File Server Object */
    OT_FILE_SERVER = 13,

    // 14 .. 16

    /** Security Object */
    OT_SECURITY = 17,

    // 18

    /** RF Medium Object */
    OT_RF_MEDIUM = 19,

    // 20 .. 408

    // @todo 0x190: Lightning

    /** Indoor Brightness Sensor */
    OT_INDOOR_BRIGHTNESS_SENSOR = 409, // = 0x199

    /** Indoor Luminance Sensor */
    OT_INDOOR_LUMINANCE_SENSOR = 410, // = 0x19a

    // 411 .. 416

    /** Light Switching Actuator Basic */
    OT_LIGHT_SWITCHING_ACTUATOR_BASIC = 417, // = 0x1a1

    /** Dimming Actuator Basic */
    OT_DIMMING_ACTUATOR_BASIC = 418, // = 0x1a2

    // 419

    /** Dimming Sensor Basic */
    OT_DIMMING_SENSOR_BASIC = 420, // = 0x1a4

    /** Switching Sensor Basic */
    OT_SWITCHING_SENSOR_BASIC = 421, // = 0x1a5

    // 422 .. 799

    // @todo 0x258: Sensors and actuators
    // @todo 0x320: Shutters and blinds

    /** Sunblind Actuator Basic */
    OT_SUNBLIND_ACUTATOR_BASIC = 800, // = 0x320

    /** Sunblind Sensor Basic */
    OT_SUNBLIND_SENSOR_BASIC = 801, // 0x321

    // @todo 0x3e8: Functions of common interest, such as date and time
    // @todo 0x44d: Metering the consumption of energy, water, gas, and so on
    // @todo 0x4b0: The OpenTherm gateway, which connects KNX to the OT Bus used for heating systems, such as gas condensing boilers
    // @todo 0x4e2: Reserved
    // @todo 0xdac: White goods
    // @todo 0xed8: Reserved
    // @todo 0xfa0: White goods

    // @todo 0xc351: First object
    // @todo 0xffff: Last object
};

}
