// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Interface_Objects.h>

#include <algorithm>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Interface_Object.h>

namespace KNX {

std::shared_ptr<Interface_Object> Interface_Objects::make_object(const Object_Type object_type) const
{
    switch (object_type) {
    case OT_DEVICE:
        return std::make_shared<Device_Object>();
    case OT_ADDRESS_TABLE:
        return std::make_shared<Group_Address_Table>();
    case OT_ASSOCIATION_TABLE:
        return std::make_shared<Group_Object_Association_Table>();
    case OT_APPLICATION_PROGRAM:
        return std::make_shared<Application_Program>();
    case OT_INTERFACE_PROGRAM:
        return std::make_shared<PEI_Program>();
    //    case OT_EIBOBJECT_ASSOCIATION_TABLE:
    //        return std::make_shared<>();
    case OT_ROUTER:
        return std::make_shared<Router_Object>();
    case OT_LTE_ADDRESS_ROUTING_TABLE:
        return std::make_shared<LTE_Address_Routing_Table_Object>();
    case OT_CEMI_SERVER:
        return std::make_shared<CEMI_Server_Object>();
    case OT_GROUP_OBJECT_TABLE:
        return std::make_shared<Group_Object_Table>();
    case OT_POLLING_MASTER:
        return std::make_shared<Polling_Master_Object>();
    case OT_KNXIP_PARAMETER:
        return std::make_shared<IP_Parameter_Object>();
    case OT_FILE_SERVER:
        return std::make_shared<File_Server_Object>();
    case OT_SECURITY:
        return std::make_shared<Security_Object>();
    case OT_RF_MEDIUM:
        return std::make_shared<RF_Medium_Object>();
    default:
        break;
    }

    return std::make_shared<Unknown_Interface_Object>(object_type);
}

std::shared_ptr<Interface_Object> Interface_Objects::object_by_type(const Object_Type object_type)
{
    std::shared_ptr<Interface_Object> object;
    for (const auto & it : objects) {
        if (it.second->object_type == object_type) {
            object = it.second;
            break;
        }
    }
    return object;
}

std::shared_ptr<Interface_Object> Interface_Objects::object_by_index(const Object_Index object_index)
{
    return objects[object_index];
}

std::shared_ptr<Interface_Object> Interface_Objects::object_by_type_instance(const Object_Type object_type, const Object_Instance object_instance)
{
    std::shared_ptr<Interface_Object> object;
    if (object_instance == 0) {
        return object;
    }

    Object_Instance oi = 0;
    for (const auto & it : objects) {
        if (it.second->object_type == object_type) {
            oi++;
            if (oi == object_instance) {
                object = it.second;
                break;
            }
        }
    }
    return object;
}

void Interface_Objects::sort_by_object_type()
{
    /* copy interface object into a vector */
    std::vector<std::shared_ptr<Interface_Object>> object_vector;
    for (const auto & object : objects) {
        object_vector.push_back(object.second);
    }

    /* sort the vector according to interface object type */
    std::sort(std::begin(object_vector), std::end(object_vector), [=](const std::shared_ptr<Interface_Object> & a, const std::shared_ptr<Interface_Object> & b) -> bool {
        return a->object_type < b->object_type;
    });

    /* copy vector back into map */
    objects.clear();
    for (Object_Index object_index = 0; object_index < object_vector.size(); ++object_index) {
        objects[object_index] = object_vector[object_index];
    }
}

std::shared_ptr<Device_Object> Interface_Objects::device()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_DEVICE);
    if (!object) {
        object = std::make_shared<Device_Object>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<Device_Object>(object);
}

std::shared_ptr<Group_Address_Table> Interface_Objects::group_address_table()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_ADDRESS_TABLE);
    if (!object) {
        object = std::make_shared<Group_Address_Table>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<Group_Address_Table>(object);
}

std::shared_ptr<Group_Object_Association_Table> Interface_Objects::group_object_association_table()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_ASSOCIATION_TABLE);
    if (!object) {
        object = std::make_shared<Group_Object_Association_Table>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<Group_Object_Association_Table>(object);
}

std::shared_ptr<Application_Program> Interface_Objects::application_program()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_APPLICATION_PROGRAM);
    if (!object) {
        object = std::make_shared<Application_Program>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<Application_Program>(object);
}

std::shared_ptr<PEI_Program> Interface_Objects::pei_program()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_INTERFACE_PROGRAM);
    if (!object) {
        object = std::make_shared<PEI_Program>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<PEI_Program>(object);
}

std::shared_ptr<Router_Object> Interface_Objects::router()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_ROUTER);
    if (!object) {
        object = std::make_shared<Router_Object>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<Router_Object>(object);
}

std::shared_ptr<LTE_Address_Routing_Table_Object> Interface_Objects::lte_address_routing_table()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_LTE_ADDRESS_ROUTING_TABLE);
    if (!object) {
        object = std::make_shared<LTE_Address_Routing_Table_Object>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<LTE_Address_Routing_Table_Object>(object);
}

std::shared_ptr<CEMI_Server_Object> Interface_Objects::cemi_server()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_CEMI_SERVER);
    if (!object) {
        object = std::make_shared<CEMI_Server_Object>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<CEMI_Server_Object>(object);
}

std::shared_ptr<Group_Object_Table> Interface_Objects::group_object_table()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_GROUP_OBJECT_TABLE);
    if (!object) {
        object = std::make_shared<Group_Object_Table>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<Group_Object_Table>(object);
}

std::shared_ptr<Polling_Master_Object> Interface_Objects::polling_master()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_POLLING_MASTER);
    if (!object) {
        object = std::make_shared<Polling_Master_Object>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<Polling_Master_Object>(object);
}

std::shared_ptr<IP_Parameter_Object> Interface_Objects::ip_parameter()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_KNXIP_PARAMETER);
    if (!object) {
        object = std::make_shared<IP_Parameter_Object>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<IP_Parameter_Object>(object);
}

std::shared_ptr<File_Server_Object> Interface_Objects::file_server()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_FILE_SERVER);
    if (!object) {
        object = std::make_shared<File_Server_Object>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<File_Server_Object>(object);
}

std::shared_ptr<Security_Object> Interface_Objects::security()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_SECURITY);
    if (!object) {
        object = std::make_shared<Security_Object>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<Security_Object>(object);
}

std::shared_ptr<RF_Medium_Object> Interface_Objects::rf_medium()
{
    std::shared_ptr<Interface_Object> object = object_by_type(OT_RF_MEDIUM);
    if (!object) {
        object = std::make_shared<RF_Medium_Object>();
        const Object_Index object_index = objects.size();
        objects[object_index] = object;
    }
    return std::dynamic_pointer_cast<RF_Medium_Object>(object);
}

}
