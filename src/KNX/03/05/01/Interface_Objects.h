// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <map>

#include <KNX/03/03/07/Object_Index.h>
#include <KNX/03/05/01/Interface_Object.h>
#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>
#include <KNX/03/05/01/02_Group_Object_Association_Table/Group_Object_Association_Table.h>
#include <KNX/03/05/01/03_Application_Program/Application_Program.h>
#include <KNX/03/05/01/04_PEI_Program/PEI_Program.h>
#include <KNX/03/05/01/06_Router/Router_Object.h>
#include <KNX/03/05/01/07_LTE_Address_Routing_Table/LTE_Address_Routing_Table_Object.h>
#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table.h>
#include <KNX/03/05/01/10_Polling_Master/Polling_Master_Object.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>
#include <KNX/03/05/01/13_File_Server/File_Server_Object.h>
#include <KNX/03/05/01/17_Security/Security_Object.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>

namespace KNX {

class KNX_EXPORT Interface_Objects
{
public:
    virtual ~Interface_Objects() = default;

    /** interface objects */
    std::map<Object_Index, std::shared_ptr<Interface_Object>> objects{};

    /** make object of object_type */
    std::shared_ptr<Interface_Object> make_object(const Object_Type object_type) const;

    /** get object by object_type */
    std::shared_ptr<Interface_Object> object_by_type(const Object_Type object_type);

    /** get object by object_index */
    std::shared_ptr<Interface_Object> object_by_index(const Object_Index object_index);

    /** get object by object_type and object_instance */
    std::shared_ptr<Interface_Object> object_by_type_instance(const Object_Type object_type, const Object_Instance object_instance);

    /** sort objects by object_type */
    void sort_by_object_type();

    /** 0 Device Object */
    std::shared_ptr<Device_Object> device();

    /** 1 Group Address Table */
    std::shared_ptr<Group_Address_Table> group_address_table();

    /** 2 Group Object Association Table */
    std::shared_ptr<Group_Object_Association_Table> group_object_association_table();

    /** 3 Application Program */
    std::shared_ptr<Application_Program> application_program();

    /** 4 PEI Program */
    std::shared_ptr<PEI_Program> pei_program();

    // 5 ?

    /** 6 Router */
    std::shared_ptr<Router_Object> router();

    /** 7 LTE Address Routing Table */
    std::shared_ptr<LTE_Address_Routing_Table_Object> lte_address_routing_table();

    /** 8 cEMI Server */
    std::shared_ptr<CEMI_Server_Object> cemi_server();

    /** 9 Group Object Table */
    std::shared_ptr<Group_Object_Table> group_object_table();

    /** 10 Polling Master */
    std::shared_ptr<Polling_Master_Object> polling_master();

    /** 11 IP Parameter */
    std::shared_ptr<IP_Parameter_Object> ip_parameter();

    // 12 ?

    /** 13 File Server */
    std::shared_ptr<File_Server_Object> file_server();

    // 14 ?

    // 15 ?

    // 16 ?

    /** 17 Security */
    std::shared_ptr<Security_Object> security();

    // 18 ?

    /** 19 RF Medium */
    std::shared_ptr<RF_Medium_Object> rf_medium();
};

}
