// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Load_State_Machine.h>

namespace KNX {

void Load_State_Machine::handle_event(const Event event, const std::vector<uint8_t> /*additional_info*/)
{
    const State old_state = state;

    switch (state) {
    case State::Unloaded:
        switch (event) {
        case Event::No_Operation:
            state = State::Unloaded;
            break;
        case Event::Start_Loading:
            state = State::Loading;
            break;
        case Event::Load_Completed:
            state = State::Unloaded; // recommended
            // new_state = State::Error; // optional
            break;
        case Event::Additional_Load_Controls:
            state = State::Unloaded; // recommended
            // new_state = State::Error; // optional
            break;
        case Event::Unload:
            state = State::Unloaded;
            break;
        }
        break;
    case State::Loaded:
        switch (event) {
        case Event::No_Operation:
            state = State::Loaded;
            break;
        case Event::Start_Loading:
            state = State::Loading;
            break;
        case Event::Load_Completed:
            state = State::Loaded; // recommended
            // new_state = State::Error; // optional
            break;
        case Event::Additional_Load_Controls:
            state = State::Error;
            break;
        case Event::Unload:
            state = State::Unloading; // intermediate state
            state = State::Unloaded; // mandatory
            break;
        }
        break;
    case State::Loading:
        switch (event) {
        case Event::No_Operation:
            state = State::Loading;
            break;
        case Event::Start_Loading:
            state = State::Loading;
            break;
        case Event::Load_Completed:
            state = State::LoadCompleting; // intermediate state
            state = State::Loaded; // mandatory
            break;
        case Event::Additional_Load_Controls:
            state = State::Loading;
            break;
        case Event::Unload:
            state = State::Unloading; // intermediate state
            state = State::Unloaded; // mandatory
            break;
        }
        break;
    case State::Error:
        switch (event) {
        case Event::No_Operation:
            state = State::Error;
            break;
        case Event::Start_Loading:
            state = State::Error;
            break;
        case Event::Load_Completed:
            state = State::Error;
            break;
        case Event::Additional_Load_Controls:
            state = State::Error;
            break;
        case Event::Unload:
            state = State::Unloading; // intermediate state
            state = State::Unloaded; // mandatory
            break;
        }
        break;
    case State::Unloading:
        switch (event) {
        case Event::No_Operation:
            state = State::Unloading;
            break;
        case Event::Start_Loading:
            state = State::Error;
            break;
        case Event::Load_Completed:
            state = State::Error;
            break;
        case Event::Additional_Load_Controls:
            state = State::Error;
            break;
        case Event::Unload:
            state = State::Unloading; // intermediate state
            state = State::Unloaded; // mandatory
            break;
        }
        break;
    case State::LoadCompleting:
        switch (event) {
        case Event::No_Operation:
            state = State::LoadCompleting;
            break;
        case Event::Start_Loading:
            // new_state = State::Error; // recommended
            // optional
            state = State::LoadCompleting; // intermediate state
            state = State::Loaded; // mandatory
            break;
        case Event::Load_Completed:
            // new_state = State::Error; // recommended
            // optional
            state = State::LoadCompleting; // intermediate state
            state = State::Loaded; // mandatory
            break;
        case Event::Additional_Load_Controls:
            // new_state = State::Error; // recommended
            // optional
            state = State::LoadCompleting; // intermediate state
            state = State::Loaded; // mandatory
            break;
        case Event::Unload:
            // recommended
            state = State::Unloading; // intermediate state
            state = State::Unloaded; // mandatory
            // optional
            // new_state = State::LoadCompleting; // intermediate state
            // new_state = State::Loaded; // mandatory
            break;
        }
        break;
    }

    if (state_updated) {
        state_updated(old_state, state);
    }
}

void Load_State_Machine::device_restart()
{
    const State old_load_state = state;
    State new_load_state = state;

    switch (old_load_state) {
    case State::Unloaded:
        new_load_state = State::Unloaded;
        break;
    case State::Loaded:
        new_load_state = State::Loaded; // Error in case of error detection at start-up
        break;
    case State::Loading:
        new_load_state = State::Loading; // recommended
        // new_state = State::Error; // optional
        break;
    case State::Error:
        new_load_state = State::Error;
        break;
    case State::Unloading:
        new_load_state = State::Unloaded;
        break;
    case State::LoadCompleting:
        // new_state = State::Unloaded; // recommended
        // optional
        new_load_state = State::LoadCompleting; // intermediate state
        new_load_state = State::Loaded; // mandatory
        break;
    }

    state = new_load_state;
    if (state_updated) {
        state_updated(old_load_state, new_load_state);
    }
}

}
