// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>
#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Load State Machine
 *
 * @ingroup KNX_03_05_01_04_17
 */
class KNX_EXPORT Load_State_Machine
{
public:
    virtual ~Load_State_Machine() = default;

    /** Load States */
    enum class State : uint8_t {
        /** Unloaded */
        Unloaded = 0,

        /** Loaded */
        Loaded = 1,

        /** Loading */
        Loading = 2,

        /** Error */
        Error = 3,

        /** Unloading */
        Unloading = 4, // optional

        /** LoadCompleting */
        LoadCompleting = 5
    };

    /** @copydoc Load_States */
    State state{};

    /** Load Events */
    enum class Event : uint8_t {
        /** No Opeation */
        No_Operation = 0x00,

        /** Start Loading */
        Start_Loading = 0x01,

        /** Load Completed */
        Load_Completed = 0x02, // optional

        /** Additional Load Controls */
        Additional_Load_Controls = 0x03,

        /** Unload */
        Unload = 0x04
    };

    /** Handle Event */
    void handle_event(const Event event, const std::vector<uint8_t> additional_info);

    /** Device Restart */
    void device_restart();

    /** state updated and changed when old_state!=new_state */
    std::function<void(const State old_state, const State new_state)> state_updated{};
};

}
