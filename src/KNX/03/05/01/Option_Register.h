// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>

namespace KNX {

/**
 * OptionReg
 *
 * @ingroup KNX_03_05_01_04_19
 */
class KNX_EXPORT Option_Register
{
public:
};

}
