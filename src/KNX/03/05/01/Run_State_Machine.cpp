// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Run_State_Machine.h>

namespace KNX {

void Run_State_Machine::handle_event(const Event event, const std::vector<uint8_t> /*additional_info*/)
{
    const State old_state = state;

    switch (state) {
    case State::Halted:
        switch (event) {
        case Event::NOP:
            state = State::Halted;
            break;
        case Event::Restart:
            if (executable_part_loaded()) {
                state = State::Halted; // intermediate state
                state = State::Ready; // intermediate state
                state = State::Running; // mandatory
            } else {
                state = State::Halted;
            }
            break;
        case Event::Stop:
            state = State::Terminated;
            break;
        }
        break;
    case State::Running:
        switch (event) {
        case Event::NOP:
            state = State::Running;
            break;
        case Event::Restart:
            if (executable_part_loaded()) {
                state = State::Halted; // intermediate state
                state = State::Ready; // intermediate state
                state = State::Running; // mandatory
            } else {
                state = State::Halted;
            }
            break;
        case Event::Stop:
            state = State::Terminated; // recommended
            // new_state = State::Halted; // optional (BCU 2 goes to Terminated and masks 0300h and 2300h to Halted.)
            break;
        }
        break;
    case State::Ready:
        switch (event) {
        case Event::NOP:
            state = State::Ready;
            break;
        case Event::Restart:
            if (executable_part_loaded()) {
                state = State::Halted; // intermediate state
                state = State::Ready; // intermediate state
                state = State::Running; // mandatory
            } else {
                state = State::Halted;
            }
            break;
        case Event::Stop:
            state = State::Terminated;
            break;
        }
        break;
    case State::Terminated:
        switch (event) {
        case Event::NOP:
            state = State::Terminated;
            break;
        case Event::Restart:
            if (executable_part_loaded()) {
                state = State::Halted; // intermediate state
                state = State::Ready; // intermediate state
                state = State::Running; // mandatory
            } else {
                state = State::Halted;
            }
            break;
        case Event::Stop:
            state = State::Terminated;
            break;
        }
        break;
    case State::Starting:
        switch (event) {
        case Event::NOP:
            state = State::Starting; // intermediate state
            state = State::Halted; // mandatory
            break;
        case Event::Restart:
            if (executable_part_loaded()) {
                state = State::Starting; // intermediate state
                state = State::Halted; // mandatory
            } else {
                state = State::Starting; // intermediate state
                state = State::Halted; // mandatory
            }
            break;
        case Event::Stop:
            state = State::Starting; // intermediate state
            state = State::Halted; // mandatory
            break;
        }
        break;
    case State::Shutting_Down:
        switch (event) {
        case Event::NOP:
            state = State::Starting; // intermediate state
            state = State::Halted; // mandatory
            break;
        case Event::Restart:
            if (executable_part_loaded()) {
                state = State::Starting; // intermediate state
                state = State::Halted; // mandatory
            } else {
                state = State::Starting; // intermediate state
                state = State::Halted; // mandatory
            }
            break;
        case Event::Stop:
            state = State::Starting; // intermediate state
            state = State::Halted; // mandatory
            break;
        }
        break;
    }

    if (state_updated) {
        state_updated(old_state, state);
    }
}

void Run_State_Machine::device_restart()
{
    const State old_state = state;

    switch (state) {
    case State::Halted:
        if (executable_part_loaded()) {
            state = State::Halted; // intermediate state
            state = State::Ready; // intermediate state
            state = State::Running; // mandatory
        } else {
            state = State::Halted;
        }
        break;
    case State::Running:
        if (executable_part_loaded()) {
            state = State::Halted; // intermediate state
            state = State::Ready; // intermediate state
            state = State::Running; // mandatory
        } else {
            // not applicable
        }
        break;
    case State::Ready:
        if (executable_part_loaded()) {
            state = State::Halted; // intermediate state
            state = State::Ready; // intermediate state
            state = State::Running; // mandatory
        } else {
            // not applicable
        }
        break;
    case State::Terminated:
        if (executable_part_loaded()) {
            state = State::Halted; // intermediate state
            state = State::Ready; // intermediate state
            state = State::Running; // mandatory
        } else {
            state = State::Halted;
        }
        break;
    case State::Starting:
        if (executable_part_loaded()) {
            state = State::Starting; // intermediate state
            state = State::Halted; // mandatory
        } else {
            state = State::Starting; // intermediate state
            state = State::Halted; // mandatory
        }
        break;
    case State::Shutting_Down:
        if (executable_part_loaded()) {
            state = State::Shutting_Down; // intermediate state
            state = State::Halted; // mandatory
        } else {
            state = State::Shutting_Down; // intermediate state
            state = State::Halted; // mandatory
        }
        break;
    }

    if (state_updated) {
        state_updated(old_state, state);
    }
}

bool Run_State_Machine::executable_part_loaded() const
{
    if (std::shared_ptr<Load_State_Machine> load_state_machine_lock = load_state_machine.lock()) {
        return load_state_machine_lock->state == Load_State_Machine::State::Loaded;
    }

    return false;
}

void Run_State_Machine::load_state_updated(const Load_State_Machine::State /*old_load_state*/, const Load_State_Machine::State new_load_state)
{
    const State old_run_state = state;
    State new_run_state = state;

    switch (new_load_state) {
    case Load_State_Machine::State::Unloaded:
        switch (old_run_state) {
        case State::Halted:
            new_run_state = State::Halted;
            break;
        case State::Running:
            new_run_state = State::Shutting_Down; // intermediate state
            new_run_state = State::Halted; // mandatory
            break;
        case State::Ready:
            new_run_state = State::Halted;
            break;
        case State::Terminated:
            new_run_state = State::Halted;
            break;
        case State::Starting:
            new_run_state = State::Starting; // intermediate state
            new_run_state = State::Halted; // mandatory
            break;
        case State::Shutting_Down:
            new_run_state = State::Shutting_Down; // intermediate state
            new_run_state = State::Halted; // mandatory
            break;
        }
        break;
    case Load_State_Machine::State::Loaded:
        break;
    case Load_State_Machine::State::Loading:
        break;
    case Load_State_Machine::State::Error:
        break;
    case Load_State_Machine::State::Unloading:
        break;
    case Load_State_Machine::State::LoadCompleting:
        break;
    }

    state = new_run_state;
    if (state_updated) {
        state_updated(old_run_state, new_run_state);
    }
}

}
