// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/05/01/Load_State_Machine.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Run State Machine
 *
 * @ingroup KNX_03_05_01_04_18
 */
class KNX_EXPORT Run_State_Machine
{
public:
    virtual ~Run_State_Machine() = default;

    /** Run States */
    enum class State : uint8_t {
        /** Halted */
        Halted = 0,

        /** Running */
        Running = 1,

        /** Ready */
        Ready = 2,

        /** Terminated */
        Terminated = 3,

        /** Starting */
        Starting = 4,

        /** Shutting down */
        Shutting_Down = 5
    };

    /** @copydoc Run_States */
    State state{};

    /** Run State Events */
    enum class Event : uint8_t {
        /** NOP */
        NOP = 0,

        /** Restart */
        Restart = 1,

        /** Stop */
        Stop = 2
    };

    /** Load State Machine */
    std::weak_ptr<Load_State_Machine> load_state_machine{};

    /** Handle Event */
    void handle_event(const Event event, const std::vector<uint8_t> additional_info);

    /** Device Restart */
    void device_restart();

    /** state updated and changed when old_state!=new_state */
    std::function<void(const State old_state, const State new_state)> state_updated;

    /** Load state updated. Handles unload. */
    void load_state_updated(const Load_State_Machine::State old_load_state, const Load_State_Machine::State new_load_state);

protected:
    /** executable part loaded */
    bool executable_part_loaded() const;
};

}
