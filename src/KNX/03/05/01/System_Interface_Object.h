// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * System Interface Object
 *
 * Examples:
 * - Device Object
 * - Group Address Table Object
 * - Group Object Association Table Object
 * - Application Program Object
 *
 * @ingroup KNX_03_04_01_04_07
 */
class KNX_EXPORT System_Interface_Object : public Interface_Object
{
public:
    explicit System_Interface_Object(const Object_Type object_type);
};

}
