// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Unknown_Interface_Object.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Unknown_Property.h>

namespace KNX {

Unknown_Interface_Object::Unknown_Interface_Object(const Object_Type object_type_) :
    Interface_Object(object_type_)
{
}

std::shared_ptr<Property> Unknown_Interface_Object::make_property(const Property_Id property_id) const
{
    if (property_id <= 50) {
        return Interface_Object::make_property(property_id);
    }

    switch (property_id) {
    default:
        break;
    }

    return std::make_shared<Unknown_Property>(property_id);
}

}
