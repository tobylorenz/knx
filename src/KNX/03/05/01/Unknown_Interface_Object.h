// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

class KNX_EXPORT Unknown_Interface_Object :
    public Interface_Object
{
public:
    explicit Unknown_Interface_Object(const Object_Type object_type_);

    std::shared_ptr<Property> make_property(const Property_Id property_id) const override;
};

}
