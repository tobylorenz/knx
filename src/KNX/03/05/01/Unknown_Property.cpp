// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/Unknown_Property.h>

#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Unknown_Property::Unknown_Property(const Property_Id property_id) :
    Data_Property(property_id)
{
}

void Unknown_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    data.assign(first, last);
}

std::vector<uint8_t> Unknown_Property::toData() const
{
    return data;
}

std::string Unknown_Property::text() const
{
    return "";
}

}
