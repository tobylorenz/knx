#!/bin/sh

# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

NAME=$1
DIR=01_Group_Address_Table
OBJECT=Group_Address_Table

# h
cat > ${DIR}/${NAME}.h << EOF
#pragma once

#include <KNX/03/04/01/Property.h>
#include <KNX/knx_export.h>

class ${NAME} : public Property {
public:
    ${NAME}();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
};
EOF

# cpp
cat > ${DIR}/${NAME}.cpp << EOF
#include <KNX/03/05/01/${DIR}/${NAME}.h>

#include <KNX/03/05/01/${DIR}/${OBJECT}.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

${NAME}::${NAME}() :
    Property()
{
    description.property_id = ${OBJECT}::PID_bla;
    description.property_datatype = PDT_bla;
}

void ${NAME}::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) {
    assert(std::distance(first, last) == 1);

    // @todo ${NAME}::fromData

    assert(first == last);
}

std::vector<uint8_t> ${NAME}::toData() const {
    std::vector<uint8_t> data;

    // @todo ${NAME}::toData

    return data;
}
EOF
