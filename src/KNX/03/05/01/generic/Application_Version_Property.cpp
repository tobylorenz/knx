// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Application_Version_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Application_Version_Property::Application_Version_Property() :
    Data_Property(Interface_Object::PID_PROGRAM_VERSION)
{
    description.property_datatype = PDT_GENERIC_05;
}

void Application_Version_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    /* @todo For Test 8/3/7 1.6.6.5, we have to reject an invalid program version */
    std::array<uint8_t, 5> invalid_program_version{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }; // invalid
    std::array<uint8_t, 5> new_program_version{};
    std::copy(first, last, std::begin(new_program_version));
    if (new_program_version == invalid_program_version) {
        return;
    }

    manufacturer = (*first++ << 8) | (*first++);

    application_number = (*first++ << 8) | (*first++);

    application_version = *first++;

    assert(first == last);
}

std::vector<uint8_t> Application_Version_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(manufacturer >> 8);
    data.push_back(manufacturer & 0xff);

    data.push_back(application_number >> 8);
    data.push_back(application_number & 0xff);

    data.push_back(application_version);

    return data;
}

std::string Application_Version_Property::text() const
{
    std::ostringstream oss;

    oss << "Manufacturer: " << std::setfill('0') << std::setw(4) << std::hex << manufacturer
        << ", Application Number: " << std::setfill('0') << std::setw(4) << std::hex << application_number
        << ", Application Version: V" << std::dec << static_cast<uint16_t>(application_version >> 4) << "." << static_cast<uint16_t>(application_version & 0x0f);

    return oss.str();
}

}
