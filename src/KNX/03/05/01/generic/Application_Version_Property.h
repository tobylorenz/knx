// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Program Version
 *
 * @ingroup KNX_03_05_01_04_02_13
 *
 * Example: 00 83 00 56 11
 * decodes to
 *   M-0083: Manufacturer is "MDT technologies"
 *   A-0056-11-A826: ApplicationProgram is "Bus power supply with diagnosis function"
 *   11 is "V1.1"

 * Example: 00 9c 50 00 10
 * decodes to
 *   M-009C: Manufacturer is "Rademacher Geräte-Elektronic GmbH"
 *   A-5000-10-B112-O00C9: ApplicationProgram is "RolloTube"
 *   10 is "V1.0"
 */
class Application_Version_Property :
    public Data_Property
{
public:
    Application_Version_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Manufacturer */
    uint16_t manufacturer{};

    /** Application Number */
    uint16_t application_number{};

    /** Application Version */
    uint8_t application_version{};
};

}
