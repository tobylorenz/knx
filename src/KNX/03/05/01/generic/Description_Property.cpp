// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Description_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Description_Property::Description_Property() :
    Data_Property(Interface_Object::PID_DESCRIPTION)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Description_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    description_.assign(first, last);
}

std::vector<uint8_t> Description_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(description_), std::cend(description_));

    return data;
}

std::string Description_Property::text() const
{
    return description_;
}

}
