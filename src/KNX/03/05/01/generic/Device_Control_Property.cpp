// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Device_Control_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Device_Control_Property::Device_Control_Property() :
    Data_Property(Interface_Object::PID_DEVICE_CONTROL)
{
    description.write_enable = true;
    description.property_datatype = PDT_BITSET8; // alt. PDT_GENERIC_01
}

void Device_Control_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    device_control.fromData(first, first + 1);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Device_Control_Property::toData() const
{
    return device_control.toData();
}

std::string Device_Control_Property::text() const
{
    return device_control.text();
}

}
