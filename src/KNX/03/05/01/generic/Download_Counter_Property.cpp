// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Download_Counter_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Download_Counter_Property::Download_Counter_Property() :
    Data_Property(Interface_Object::PID_DOWNLOAD_COUNTER)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Download_Counter_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    download_counter = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Download_Counter_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(download_counter >> 8);
    data.push_back(download_counter & 0xff);

    return data;
}

std::string Download_Counter_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << download_counter;

    return oss.str();
}

}
