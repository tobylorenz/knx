// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Error_Code_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Error_Code_Property::Error_Code_Property() :
    Data_Property(Interface_Object::PID_ERROR_CODE)
{
    description.property_datatype = PDT_ENUM8; // alt. PDT_UNSIGNED_CHAR
}

void Error_Code_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    error_code.fromData(first, last);

    assert(first == last);
}

std::vector<uint8_t> Error_Code_Property::toData() const
{
    return error_code.toData();
}

std::string Error_Code_Property::text() const
{
    return error_code.text();
}

}
