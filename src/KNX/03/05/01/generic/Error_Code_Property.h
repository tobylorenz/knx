// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/07/02/20/DPT_20_011.h> /* DPT_ErrorClass_System */
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Error code
 *
 * @ingroup KNX_03_05_01_04_02_28
 * @ingroup KNX_03_05_01_04_10_07_02_07
 * @ingroup KNX_03_05_01_04_11_04_02_07
 * @ingroup KNX_03_05_01_04_12_05_02_06
 * @ingroup KNX_03_05_01_04_14_03_03_08
 */
class Error_Code_Property :
    public Data_Property
{
public:
    Error_Code_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Error code */
    DPT_ErrorClass_System error_code{};
};

}
