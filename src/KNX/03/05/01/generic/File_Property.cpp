// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/File_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

File_Property::File_Property() :
    Data_Property(Interface_Object::PID_FILE)
{
    description.property_datatype = PDT_GENERIC_01; // @todo datatype?
}

void File_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo File_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> File_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo File_Property::toData

    return data;
}

std::string File_Property::text() const
{
    // @todo File_Property::text
    return "";
}

}
