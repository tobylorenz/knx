// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Firmware_Revision_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Firmware_Revision_Property::Firmware_Revision_Property() :
    Data_Property(Interface_Object::PID_FIRMWARE_REVISION)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Firmware_Revision_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    firmware_revision = *first++;

    assert(first == last);
}

std::vector<uint8_t> Firmware_Revision_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(firmware_revision);

    return data;
}

std::string Firmware_Revision_Property::text() const
{
    std::ostringstream oss;

    oss << static_cast<uint16_t>(firmware_revision);

    return oss.str();
}

}
