// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Group_Object_Reference_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Group_Object_Reference_Property::Group_Object_Reference_Property() :
    Data_Property(Interface_Object::PID_GROUP_OBJECT_REFERENCE)
{
    description.property_datatype = PDT_GENERIC_01; // @todo datatype?
}

void Group_Object_Reference_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Group_Object_Reference_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Group_Object_Reference_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Group_Object_Reference_Property::toData

    return data;
}

std::string Group_Object_Reference_Property::text() const
{
    // @todo Group_Object_Reference_Property::text
    return "";
}

}
