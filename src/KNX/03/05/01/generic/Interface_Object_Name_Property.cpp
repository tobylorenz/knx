// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Interface_Object_Name_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Interface_Object_Name_Property::Interface_Object_Name_Property() :
    Data_Property(Interface_Object::PID_OBJECT_NAME)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Interface_Object_Name_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    object_name.assign(first, last);
}

std::vector<uint8_t> Interface_Object_Name_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(object_name), std::cend(object_name));

    return data;
}

std::string Interface_Object_Name_Property::text() const
{
    return object_name;
}

}
