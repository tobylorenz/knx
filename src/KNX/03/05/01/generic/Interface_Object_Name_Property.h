// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <string>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Interface Object Name
 *
 * @ingroup KNX_03_05_01_04_02_02
 * @ingroup KNX_03_05_01_04_10_06_02_02
 * @ingroup KNX_03_05_01_04_11_04_02_02
 * @ingroup KNX_03_05_01_04_12_04_02_02
 * @ingroup KNX_03_05_01_04_14_02_02_02
 */
class Interface_Object_Name_Property :
    public Data_Property
{
public:
    Interface_Object_Name_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Interface Object Name */
    std::string object_name{};
};

}
