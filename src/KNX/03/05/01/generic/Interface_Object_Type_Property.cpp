// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Interface_Object_Type_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Interface_Object_Type_Property::Interface_Object_Type_Property(const Object_Type object_type) :
    Data_Property(Interface_Object::PID_OBJECT_TYPE),
    object_type(object_type)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Interface_Object_Type_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    object_type = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Interface_Object_Type_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(object_type >> 8);
    data.push_back(object_type & 0xff);

    return data;
}

std::string Interface_Object_Type_Property::text() const
{
    switch (object_type) {
    case OT_DEVICE:
        return "Device Object";
    case OT_ADDRESS_TABLE:
        return "Addresstable Object";
    case OT_ASSOCIATION_TABLE:
        return "Associationtable Object";
    case OT_APPLICATION_PROGRAM:
        return "Applicationprogram Object";
    case OT_INTERFACE_PROGRAM:
        return "Interfaceprogram Object";
    case OT_EIBOBJECT_ASSOCIATION_TABLE:
        return "KNX-Object Associationtable Object";
    case OT_ROUTER:
        return "Router Object";
    case OT_LTE_ADDRESS_ROUTING_TABLE:
        return "LTE Address Routing Table Object";
    case OT_CEMI_SERVER:
        return "cEMI Server Object";
    case OT_GROUP_OBJECT_TABLE:
        return "Group Object Table Object";
    case OT_POLLING_MASTER:
        return "Polling Master";
    case OT_KNXIP_PARAMETER:
        return "KNXnet/IP Parameter Object";
    case OT_FILE_SERVER:
        return "File Server Object";
    case OT_SECURITY:
        return "Security Object";
    case OT_RF_MEDIUM:
        return "RF Medium Object";
    case OT_INDOOR_BRIGHTNESS_SENSOR:
        return "Indoor Brightness Sensor";
    case OT_INDOOR_LUMINANCE_SENSOR:
        return "Indoor Luminance Sensor";
    case OT_LIGHT_SWITCHING_ACTUATOR_BASIC:
        return "Light Switching Actuator Basic";
    case OT_DIMMING_ACTUATOR_BASIC:
        return "Dimming Actuator Basic";
    case OT_DIMMING_SENSOR_BASIC:
        return "Dimming Sensor Basic";
    case OT_SWITCHING_SENSOR_BASIC:
        return "Switching Sensor Basic";
    case OT_SUNBLIND_ACUTATOR_BASIC:
        return "Sunblind Actuator Basic";
    case OT_SUNBLIND_SENSOR_BASIC:
        return "Sunblind Sensor Basic";
    }

    return "";
}

}
