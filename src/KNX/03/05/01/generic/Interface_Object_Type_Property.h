// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Object_Type.h>
#include <KNX/03/04/01/Data_Property.h>
//#include <KNX/03/07/02/7/DPT_7_010.h> /* DPT_PropDataType */
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Interface Object Type
 *
 * @ingroup KNX_03_05_01_04_02_01
 * @ingroup KNX_03_05_01_04_10_06_02_01
 * @ingroup KNX_03_05_01_04_10_07_02_01
 * @ingroup KNX_03_05_01_04_11_04_02_01
 * @ingroup KNX_03_05_01_04_12_04_02_01
 * @ingroup KNX_03_05_01_04_12_05_02_01
 * @ingroup KNX_03_05_01_04_14_02_02_01
 * @ingroup KNX_03_05_01_04_14_03_03_02
 */
class Interface_Object_Type_Property :
    public Data_Property
{
public:
    explicit Interface_Object_Type_Property(const Object_Type object_type);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Object Type */
    Object_Type object_type{}; // @note DPT_PropDataType according to spec
};

}
