// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Load_Control_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Load_Control_Property::Load_Control_Property() :
    Data_Property(Interface_Object::PID_LOAD_STATE_CONTROL)
{
    description.write_enable = true;
    description.property_datatype = PDT_CONTROL;
}

Load_Control_Property::Load_Control_Property(std::shared_ptr<Load_State_Machine> load_state_machine) :
    Data_Property(Interface_Object::PID_LOAD_STATE_CONTROL),
    load_state_machine(load_state_machine)
{
    description.write_enable = true;
    description.property_datatype = PDT_CONTROL;
}

void Load_Control_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const auto length = std::distance(first, last);

    if (length == 1) {
        // OpenETS stores received states via fromData.
        state = Load_State_Machine::State(*first++);
        assert(first == last);
        return;
    }

    if (length == 10) {
        event = static_cast<Load_State_Machine::Event>(*first++);
        additional_info.assign(first, first + 9);
        first += 9;

        assert(first == last);

        if (load_state_machine) {
            load_state_machine->handle_event(event, additional_info);
        }

        return;
    }

    assert(false);
}

std::vector<uint8_t> Load_Control_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(load_state_machine ? static_cast<uint8_t>(load_state_machine->state) : 0);

    return data;
}

std::string Load_Control_Property::text() const
{
    const Load_State_Machine::State state = load_state_machine ? load_state_machine->state : this->state;

    switch (state) {
    case Load_State_Machine::State::Unloaded:
        return "Unloaded";
    case Load_State_Machine::State::Loaded:
        return "Loaded";
    case Load_State_Machine::State::Loading:
        return "Loading";
    case Load_State_Machine::State::Error:
        return "Error";
    case Load_State_Machine::State::Unloading:
        return "Unloading";
    case Load_State_Machine::State::LoadCompleting:
        return "LoadCompleting";
    }

    return "";
}

}
