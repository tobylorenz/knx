// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/05/01/Load_State_Machine.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Load Control
 *
 * @ingroup KNX_03_05_01_04_02_05
 * @ingroup KNX_03_05_01_04_04_02
 * @ingroup KNX_03_05_01_04_05_02
 * @ingroup KNX_03_05_01_04_10_06_02_03
 * @ingroup KNX_03_05_01_04_10_07_02_02
 * @ingroup KNX_03_05_01_04_11_04_02_03
 * @ingroup KNX_03_05_01_04_12_04_02_03
 * @ingroup KNX_03_05_01_04_12_05_02_02
 * @ingroup KNX_03_05_01_04_14_02_02_03
 * @ingroup KNX_03_05_01_04_14_03_03_03
 */
class Load_Control_Property :
    public Data_Property
{
public:
    Load_Control_Property();
    explicit Load_Control_Property(std::shared_ptr<Load_State_Machine> load_state_machine);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override; // returns state

    /** Received Event */
    Load_State_Machine::Event event{};

    /** Received Additional Info */
    std::vector<uint8_t> additional_info{};

    /** Transmitted State */
    Load_State_Machine::State state{};

    /** Load State Machine */
    std::shared_ptr<Load_State_Machine> load_state_machine{};
};

}
