// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Manufacturer_Data_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Manufacturer_Data_Property::Manufacturer_Data_Property() :
    Data_Property(Interface_Object::PID_MANUFACTURER_DATA)
{
    description.property_datatype = PDT_VARIABLE_LENGTH; // or PDT_GENERIC_04
}

void Manufacturer_Data_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    manufacturer_data.assign(first, last);
}

std::vector<uint8_t> Manufacturer_Data_Property::toData() const
{
    return manufacturer_data;
}

std::string Manufacturer_Data_Property::text() const
{
    std::ostringstream oss;

    for (auto i : manufacturer_data) {
        oss << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(i);
    }

    return oss.str();
}

}
