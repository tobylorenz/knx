// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Manufacturer Data
 *
 * @ingroup KNX_03_05_01_04_02_19
 */
class Manufacturer_Data_Property :
    public Data_Property
{
public:
    Manufacturer_Data_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Manufacturer Data */
    std::vector<uint8_t> manufacturer_data{};
};

}
