// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Manufacturer_Identifier_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Manufacturer_Identifier_Property::Manufacturer_Identifier_Property() :
    Data_Property(Interface_Object::PID_MANUFACTURER_ID)
{
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Manufacturer_Identifier_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    manufacturer_id = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Manufacturer_Identifier_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(manufacturer_id >> 8);
    data.push_back(manufacturer_id & 0xff);

    return data;
}

std::string Manufacturer_Identifier_Property::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(4) << std::hex << manufacturer_id;

    return oss.str();
}

}
