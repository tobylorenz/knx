// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Memory_Control_Table_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Memory_Control_Table_Property::Memory_Control_Table_Property() :
    Data_Property(Interface_Object::PID_MCB_TABLE)
{
    description.property_datatype = PDT_GENERIC_08;
}

void Memory_Control_Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    while (first != last) {
        Memory_Control_Block mcb;

        mcb.segment_size_1 =
            (*first++ << 24) |
            (*first++ << 16) |
            (*first++ << 8) |
            (*first++ << 0);

        mcb.crc_control_byte = *first++;

        mcb.read_access_1 = *first >> 4;
        mcb.write_access_1 = *first & 0x0f;
        ++first;

        mcb.crc = (*first++ << 8) | (*first++);

        mcb_table.push_back(mcb);
    }

    assert(first == last);
}

std::vector<uint8_t> Memory_Control_Table_Property::toData() const
{
    std::vector<uint8_t> data;

    for (auto mcb : mcb_table) {
        data.push_back((mcb.segment_size_1 >> 24) & 0xff);
        data.push_back((mcb.segment_size_1 >> 16) & 0xff);
        data.push_back((mcb.segment_size_1 >> 8) & 0xff);
        data.push_back((mcb.segment_size_1 >> 0) & 0xff);
        data.push_back(mcb.crc_control_byte);
        data.push_back((mcb.read_access_1 << 4) | mcb.write_access_1);
        data.push_back(mcb.crc >> 8);
        data.push_back(mcb.crc & 0xff);
    }

    return data;
}

std::string Memory_Control_Table_Property::text() const
{
    std::ostringstream oss;

    // @todo Memory_Control_Table_Property::text()

    return oss.str();
}

}
