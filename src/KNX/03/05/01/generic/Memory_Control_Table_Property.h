// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Memory Control Table
 *
 * @ingroup KNX_03_05_01_04_02_27
 * @ingroup KNX_03_05_01_04_10_07_02_06
 * @ingroup KNX_03_05_01_04_11_04_02_06
 * @ingroup KNX_03_05_01_04_12_05_02_05
 * @ingroup KNX_03_05_01_04_14_03_03_07
 */
class Memory_Control_Table_Property :
    public Data_Property
{
public:
    Memory_Control_Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Memory Control Block (MCB) */
    struct Memory_Control_Block {
        /** Segment Size 1 */
        uint32_t segment_size_1{};

        /** CRC Control Byte */
        uint8_t crc_control_byte{};

        /** Read Access 1 */
        uint4_t read_access_1{};

        /** Write Access 1 */
        uint4_t write_access_1{};

        /** CRC */
        uint16_t crc{};
    };

    /** Memory Control Block Table */
    std::vector<Memory_Control_Block> mcb_table{};
};

}
