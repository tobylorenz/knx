// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Object_Index_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Object_Index_Property::Object_Index_Property() :
    Data_Property(Interface_Object::PID_OBJECT_INDEX)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Object_Index_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    object_index = *first++;

    assert(first == last);
}

std::vector<uint8_t> Object_Index_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(object_index);

    return data;
}

std::string Object_Index_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(object_index);

    return oss.str();
}

}
