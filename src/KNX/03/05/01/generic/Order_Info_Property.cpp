// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Order_Info_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Order_Info_Property::Order_Info_Property() :
    Data_Property(Interface_Object::PID_ORDER_INFO)
{
    description.property_datatype = PDT_GENERIC_10;
}

void Order_Info_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 10);

    std::copy(first, last, std::begin(order_info));
    first += 10;

    assert(first == last);
}

std::vector<uint8_t> Order_Info_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(order_info), std::cend(order_info));

    return data;
}

std::string Order_Info_Property::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[0])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[1])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[2])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[3])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[4])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[5])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[6])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[7])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[8])
        << ":"
        << std::setfill('0') << std::setw(2) << std::hex << static_cast<uint16_t>(order_info[9]);

    return oss.str();
}

}
