// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/PEI_Type_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

PEI_Type_Property::PEI_Type_Property() :
    Data_Property(Interface_Object::PID_PEI_TYPE)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void PEI_Type_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    pei_type = *first++;

    assert(first == last);
}

std::vector<uint8_t> PEI_Type_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(pei_type);

    return data;
}

std::string PEI_Type_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(pei_type);

    return oss.str();
}

}
