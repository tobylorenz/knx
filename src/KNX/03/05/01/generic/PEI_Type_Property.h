// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PEI Type
 *
 * @ingroup KNX_03_05_01_04_02_16
 * @ingroup KNX_03_05_01_04_14_03_03_05
 */
class PEI_Type_Property :
    public Data_Property
{
public:
    PEI_Type_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** PEI Type */
    uint8_t pei_type{};
};

}
