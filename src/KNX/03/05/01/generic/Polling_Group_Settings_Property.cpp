// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Polling_Group_Settings_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Polling_Group_Settings_Property::Polling_Group_Settings_Property() :
    Data_Property(Interface_Object::PID_POLL_GROUP_SETTINGS)
{
    description.property_datatype = PDT_POLL_GROUP_SETTINGS;
}

void Polling_Group_Settings_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 3);

    polling_group_address.fromData(first, first + 2);
    first += 2;

    disable = (*first >> 7);
    polling_slot_number = (*first & 0x0f);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Polling_Group_Settings_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(polling_group_address >> 8);
    data.push_back(polling_group_address & 0xff);
    data.push_back((disable << 7) | polling_slot_number);

    return data;
}

std::string Polling_Group_Settings_Property::text() const
{
    std::ostringstream oss;

    oss << "Group Address: " << std::setfill('0') << std::setw(2) << std::hex << polling_group_address // @todo group address naming scheme?
        << ", Disable: " << (disable ? "true" : "enable")
        << ", Slot Nr.: " << std::dec << static_cast<uint16_t>(polling_slot_number);

    return oss.str();
}

}
