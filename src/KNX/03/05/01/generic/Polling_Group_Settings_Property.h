// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Group_Address.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Pollgroup Settings
 *
 * @ingroup KNX_03_05_01_04_02_18
 */
class Polling_Group_Settings_Property :
    public Data_Property
{
public:
    Polling_Group_Settings_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Polling Group Address */
    Group_Address polling_group_address{};

    /** Disable */
    bool disable{false};

    /** Polling Slot Number */
    uint4_t polling_slot_number{};
};

}
