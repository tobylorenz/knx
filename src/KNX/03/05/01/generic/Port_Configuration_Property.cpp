// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Port_Configuration_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Port_Configuration_Property::Port_Configuration_Property() :
    Data_Property(Interface_Object::PID_PORT_CONFIGURATION)
{
    description.property_datatype = PDT_UNSIGNED_CHAR;
}

void Port_Configuration_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    port_configuration = *first++;

    assert(first == last);
}

std::vector<uint8_t> Port_Configuration_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(port_configuration);

    return data;
}

std::string Port_Configuration_Property::text() const
{
    std::ostringstream oss;

    oss << std::dec << static_cast<uint16_t>(port_configuration);

    return oss.str();
}

}
