// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Run_Control_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Run_Control_Property::Run_Control_Property() :
    Data_Property(Interface_Object::PID_RUN_STATE_CONTROL)
{
    description.write_enable = true;
    description.property_datatype = PDT_CONTROL;
}

Run_Control_Property::Run_Control_Property(std::shared_ptr<Run_State_Machine> run_state_machine) :
    Data_Property(Interface_Object::PID_RUN_STATE_CONTROL),
    run_state_machine(run_state_machine)
{
    description.write_enable = true;
    description.property_datatype = PDT_CONTROL;
}


void Run_Control_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const auto length = std::distance(first, last);

    if (length == 1) {
        // OpenETS stores received states via fromData.
        state = Run_State_Machine::State(*first++);
        assert(first == last);
        return;
    }

    if (length == 10) {
        event = static_cast<Run_State_Machine::Event>(*first++);
        additional_info.assign(first, first + 9);
        first += 9;

        assert(first == last);

        if (run_state_machine) {
            run_state_machine->handle_event(event, additional_info);
        }

        return;
    }

    assert(false);
}

std::vector<uint8_t> Run_Control_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(run_state_machine ? static_cast<uint8_t>(run_state_machine->state) : 0);

    return data;
}

std::string Run_Control_Property::text() const
{
    const Run_State_Machine::State state = run_state_machine ? run_state_machine->state : this->state;

    switch (state) {
    case Run_State_Machine::State::Halted:
        return "Halted";
    case Run_State_Machine::State::Running:
        return "Running";
    case Run_State_Machine::State::Ready:
        return "Ready";
    case Run_State_Machine::State::Terminated:
        return "Terminated";
    case Run_State_Machine::State::Starting:
        return "Starting";
    case Run_State_Machine::State::Shutting_Down:
        return "Shutting down";
    }

    return "";
}

}
