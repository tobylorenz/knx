// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/05/01/Run_State_Machine.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Run Control
 *
 * @ingroup KNX_03_05_01_04_02_06
 * @ingroup KNX_03_05_01_04_14_03_03_04
 */
class Run_Control_Property :
    public Data_Property
{
public:
    Run_Control_Property();
    explicit Run_Control_Property(std::shared_ptr<Run_State_Machine> run_state_machine);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override; // returns state

    /** Received Event */
    Run_State_Machine::Event event{};

    /** Received Additional Info */
    std::vector<uint8_t> additional_info{};

    /** Transmitted State */
    Run_State_Machine::State state{};

    /** Run State Machine */
    std::shared_ptr<Run_State_Machine> run_state_machine{};
};

}
