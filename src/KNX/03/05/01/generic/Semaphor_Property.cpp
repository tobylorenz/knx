// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Semaphor_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Semaphor_Property::Semaphor_Property() :
    Data_Property(Interface_Object::PID_SEMAPHOR)
{
    description.property_datatype = PDT_GENERIC_01; // @todo datatype?
}

void Semaphor_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    //assert(std::distance(first, last) == 1);

    // @todo Semaphor_Property::fromData

    //assert(first == last);
}

std::vector<uint8_t> Semaphor_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Semaphor_Property::toData

    return data;
}

std::string Semaphor_Property::text() const
{
    // @todo Semaphor_Property::text
    return "";
}

}
