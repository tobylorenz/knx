// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Serial_Number_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Serial_Number_Property::Serial_Number_Property() :
    Data_Property(Interface_Object::PID_SERIAL_NUMBER),
    Network_Parameter_Property()
{
    description.property_datatype = PDT_GENERIC_06;
}

void Serial_Number_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    std::copy(first, last, std::begin(serial_number.serial_number));
    first += 6;

    assert(first == last);
}

std::vector<uint8_t> Serial_Number_Property::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(serial_number.serial_number), std::cend(serial_number.serial_number));

    return data;
}

std::string Serial_Number_Property::text() const
{
    return serial_number.text();
}

}
