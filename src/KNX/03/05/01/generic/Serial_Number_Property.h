// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Serial_Number.h>
#include <KNX/03/04/01/Data_Property.h>
#include <KNX/03/04/01/Network_Parameter_Property.h>
#include <KNX/03/07/02/221/DPT_221_001.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Serial Number
 *
 * @ingroup KNX_03_05_01_04_02_11
 */
class Serial_Number_Property :
    public Data_Property,
    public Network_Parameter_Property
{
public:
    Serial_Number_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Serial Number */
    Serial_Number serial_number{}; // @todo DPT_SerNum
};

}
