// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Service_Control_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Service_Control_Property::Service_Control_Property() :
    Data_Property(Interface_Object::PID_SERVICE_CONTROL)
{
    description.write_enable = true;
    description.property_datatype = PDT_UNSIGNED_INT;
}

void Service_Control_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    service_control = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> Service_Control_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(service_control >> 8);
    data.push_back(service_control & 0xff);

    return data;
}

std::string Service_Control_Property::text() const
{
    std::ostringstream oss;

    oss << "User Stopped_ServiceInfo Enable: "
        << ((service_control & (1 << 0)) ? "enable" : "disable")
        << ", Received own Individual Address Service Info enable: "
        << ((service_control & (1 << 1)) ? "enable" : "disable")
        << ", Individual Address Write Enable: "
        << ((service_control & (1 << 2)) ? "enable" : "disable")
        << ", Application Interface Layer Services on EMI Disable: "
        << ((service_control & (1 << 8)) ? "disable" : "enable")
        << ", Link Layer Services on EMI Disable: "
        << ((service_control & (1 << 9)) ? "disable" : "enable")
        << ", Network Layer Services on EMI Disable: "
        << ((service_control & (1 << 10)) ? "disable" : "enable")
        << ", Transport Layer Group Services on EMI Disable: "
        << ((service_control & (1 << 11)) ? "disable" : "enable")
        << ", Switch Service-Services on EMI Disable: "
        << ((service_control & (1 << 12)) ? "disable" : "enable")
        << ", Transport Layer Connection Oriented Services on EMI Disable: "
        << ((service_control & (1 << 13)) ? "disable" : "enable")
        << ", Application Layer Services on EMI Disable: "
        << ((service_control & (1 << 14)) ? "disable" : "enable")
        << ", Management Services on EMI Disable: "
        << ((service_control & (1 << 15)) ? "disable" : "enable");

    return oss.str();
}

}
