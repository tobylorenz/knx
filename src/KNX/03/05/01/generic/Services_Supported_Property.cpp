// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Services_Supported_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Services_Supported_Property::Services_Supported_Property() :
    Data_Property(Interface_Object::PID_SERVICES_SUPPORTED)
{
    description.property_datatype = PDT_VARIABLE_LENGTH; // @note not specified
}

void Services_Supported_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    // @todo Services_Supported_Property::fromData
}

std::vector<uint8_t> Services_Supported_Property::toData() const
{
    std::vector<uint8_t> data;

    // @todo Services_Supported_Property::toData

    return data;
}

std::string Services_Supported_Property::text() const
{
    // @todo Services_Supported_Property::text
    return "";
}

}
