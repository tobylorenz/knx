// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Table_Property.h>

#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Table_Property::Table_Property() :
    Data_Property(Interface_Object::PID_TABLE)
{
    description.property_datatype = PDT_VARIABLE_LENGTH;
}

void Table_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    table.assign(first, last);
}

std::vector<uint8_t> Table_Property::toData() const
{
    return table;
}

std::string Table_Property::text() const
{
    // @todo Table_Property::text() depends on Interface Object
    return "";
}

}
