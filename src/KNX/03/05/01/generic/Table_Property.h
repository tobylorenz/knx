// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/04/01/Data_Property.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Table
 *
 * @ingroup KNX_03_05_01_04_02_23
 * @ingroup KNX_03_05_01_04_10_06_02_04
 * @ingroup KNX_03_05_01_04_10_06_02_04
 * @ingroup KNX_03_05_01_04_10_07_02_04
 * @ingroup KNX_03_05_01_04_11_04_02_05
 * @ingroup KNX_03_05_01_04_12_05_02_04
 */
class Table_Property :
    public Data_Property
{
public:
    Table_Property();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Table */
    std::vector<uint8_t> table{};
};

}
