// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Table_Reference_Property.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Table_Reference_Property::Table_Reference_Property() :
    Data_Property(Interface_Object::PID_TABLE_REFERENCE)
{
    description.property_datatype = PDT_UNSIGNED_LONG;
}

void Table_Reference_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    table_reference =
        (*first++ << 24) |
        (*first++ << 16) |
        (*first++ << 8) |
        (*first++);

    assert(first == last);
}

std::vector<uint8_t> Table_Reference_Property::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((table_reference >> 24) & 0xff);
    data.push_back((table_reference >> 16) & 0xff);
    data.push_back((table_reference >> 8) & 0xff);
    data.push_back(table_reference & 0xff);

    return data;
}

std::string Table_Reference_Property::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(8) << std::hex
        << table_reference;

    return oss.str();
}

}
