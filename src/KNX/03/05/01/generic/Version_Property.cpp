// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/generic/Version_Property.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

Version_Property::Version_Property() :
    Data_Property(Interface_Object::PID_VERSION)
{
    description.property_datatype = PDT_VERSION; // alt. PDT_GENERIC_02. tests: PDT_UNSIGNED INT
}

void Version_Property::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    version.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> Version_Property::toData() const
{
    return version.toData();
}

std::string Version_Property::text() const
{
    return version.text();
}

}
