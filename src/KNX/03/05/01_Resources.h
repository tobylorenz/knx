// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 3 Network Resources */

/* 3.2 Domain Addresses */
#include <KNX/03/03/07/Domain_Address_6.h>

/* 4 Device Resources */

/* 4.1 Device Descriptors */
#include <KNX/03/05/01/Device_Descriptor.h>

/* 4.1.2 Device Descriptor Type 0 (mask version) */
#include <KNX/03/05/01/Device_Descriptor_Type_0.h>

/* 4.1.3 Device Descriptor Type 2 */
#include <KNX/03/05/01/Device_Descriptor_Type_2.h>

/* 4.2 Interface Object Type independent Properties */
#include <KNX/03/05/01/Interface_Object.h>

/* 4.3 Device Object (Object Type 0) */
#include <KNX/03/05/01/00_Device/Device_Object.h>

/* 4.4 Router Object (Object Type 6) */
#include <KNX/03/05/01/06_Router/Router_Object.h>

/* 4.5 LTE Address Routing Table Object (Object Type 7) */
#include <KNX/03/05/01/07_LTE_Address_Routing_Table/LTE_Address_Routing_Table_Object.h>

/* 4.6 cEMI Server Object (Object Type 8) */
#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>

/* 4.7 KNXnet/IP Parameter Object (Object Type 11) */
#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>

/* 4.8 File Server Object (Object Type 13) */
#include <KNX/03/05/01/13_File_Server/File_Server_Object.h>

/* 4.9 RF Medium Object (Object Type 19) */
#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>

/* 4.10 Group Address Table (GrAT) */
#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>

/* 4.11 Group Object Association Table (GrOAT) */
#include <KNX/03/05/01/02_Group_Object_Association_Table/Group_Object_Association_Table.h>

/* 4.12 Group Object Table */
#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table.h>

/* 4.13 Easy Parameter Block Table (PaBT) */
#include <KNX/03/05/01/Easy_Parameter_Block_Table.h>

/* 4.14 Application Program */
#include <KNX/03/05/01/03_Application_Program/Application_Program.h>

/* 4.15 PEI Program */
#include <KNX/03/05/01/04_PEI_Program/PEI_Program.h>

/* 4.16 KNX Serial Number */
#include <KNX/03/03/07/Serial_Number.h>

/* 4.17 Load State Machine */
#include <KNX/03/05/01/Load_State_Machine.h>

/* 4.18 Run State Machine */
#include <KNX/03/05/01/Run_State_Machine.h>

/* 4.19 OptionReg */
#include <KNX/03/05/01/Option_Register.h>

/* 4.20 Programming Mode (prog_mode) */
#include <KNX/03/05/01/00_Device/Programming_Mode_Property.h>

/* 4.21 Group telegram rate limitation */
#include <KNX/03/05/01/00_Device/Group_Telegram_Rate_Limitation_Time_Base_Property.h>
#include <KNX/03/05/01/00_Device/Group_Telegram_Rate_Limitation_Number_Of_Telegrams_Property.h>
