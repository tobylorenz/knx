// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/05/02/Device_Management.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

void Device_Management::DMP_DeviceDescriptor_InfoReport_req()
{
    if (DMP_DeviceDescriptor_InfoReport_data.state == 0) {
        DMP_DeviceDescriptor_InfoReport_data.state = 1;
    }
}

void Device_Management::DM_Disconnect_req(const DM_Disconnect_Flags flags)
{
    // @todo DM_Disconnect_req
}

void Device_Management::DMP_Disconnect_RCo_req(const Individual_Address destination_address)
{
    if (DMP_Disconnect_RCo_data.state == 0) {
        DMP_Disconnect_RCo_data.state = 1;
        DMP_Disconnect_RCo_data.destination_address = destination_address;
        assert(A_Disconnect_req);
        A_Disconnect_req(Priority::low, {destination_address, true});
    }
}

void Device_Management::DMP_Disconnect_LEmi1_req()
{
    // @todo DMP_Disconnect_LEmi1_req
}

void Device_Management::DM_Identify_req()
{
    // @todo DM_Identify_req
}

void Device_Management::DMP_Identify_R_req(const ASAP_Individual asap)
{
    if (DMP_Identify_R_data.state == 0) {
        DMP_Identify_R_data.state = 1;
        DMP_Identify_R_data.asap = asap;
        assert(A_DeviceDescriptor_Read_req);
        A_DeviceDescriptor_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 0);
    }
}

void Device_Management::DMP_Identify_RCo2_req(const Individual_Address server_IA)
{
    if (DMP_Identify_RCo2_data.state == 0) {
        DMP_Identify_RCo2_data.state = 1;
        DMP_Identify_RCo2_data.server_IA = server_IA;
        assert(A_PropertyValue_Read_req);
        const ASAP_Individual asap{server_IA, true};
        A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 0, Device_Object::PID_MANUFACTURER_ID, 1, 1);
    }
}

void Device_Management::DM_Authorize_req(const DM_Authorize_Flags flags, const Key key)
{
    // @todo DM_Authorize_req
}

void Device_Management::DMP_Authorize_RCo_req(const Individual_Address individual_address, const Key key)
{
    if (DMP_Authorize_RCo_data.state == 0) {
        DMP_Authorize_RCo_data.state = 1;
        DMP_Authorize_RCo_data.individual_address = individual_address;
        DMP_Authorize_RCo_data.key = key;
        assert(A_Authorize_Request_req);
        const ASAP_Connected asap{individual_address, true};
        A_Authorize_Request_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, key);
        DMP_Authorize_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
    }
}

void Device_Management::DMP_Authorize2_RCo_req(const Individual_Address individual_address, const Key client_key)
{
    if (DMP_Authorize2_RCo_data.state == 0) {
        DMP_Authorize2_RCo_data.state = 1;
        DMP_Authorize2_RCo_data.individual_address = individual_address;
        DMP_Authorize2_RCo_data.client_key = client_key;
        assert(A_Authorize_Request_req);
        const ASAP_Connected asap{individual_address, true};
        A_Authorize_Request_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, {0xFF, 0xFF, 0xFF, 0xFF});
        DMP_Authorize2_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
    }
}

void Device_Management::DM_SetKey_req(const DM_SetKey_Flags flags, const Key key, const Level level)
{
    // @todo DM_SetKey_req
}

void Device_Management::DMP_SetKey_RCo_req(const Individual_Address individual_address, const Key key, const Level level)
{
    if (DMP_SetKey_RCo_data.state == 0) {
        DMP_SetKey_RCo_data.state = 1;
        DMP_SetKey_RCo_data.individual_address = individual_address;
        DMP_SetKey_RCo_data.key = key;
        DMP_SetKey_RCo_data.level = level;
        assert(A_Key_Write_req);
        const ASAP_Connected asap{individual_address, true};
        A_Key_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, level, key);
        DMP_SetKey_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
    }
}

void Device_Management::DM_Restart_req(const DM_Restart_Flags flags)
{
    // @todo DM_Restart_req
}

void Device_Management::DMP_Restart_RCl_req(const Individual_Address individual_address, const Restart_Type mpp_RestartType, const Restart_Erase_Code mpp_EraseCode, const Restart_Channel_Number mpp_ChannelNumber)
{
    if (DMP_Restart_RCl_data.state == 0) {
        DMP_Restart_RCl_data.state = 1;
        DMP_Restart_RCl_data.individual_address = individual_address;
        DMP_Restart_RCl_data.mpp_RestartType = mpp_RestartType;
        DMP_Restart_RCl_data.mpp_EraseCode = mpp_EraseCode;
        DMP_Restart_RCl_data.mpp_ChannelNumber = mpp_ChannelNumber;
        assert(A_Restart_req);
        const ASAP_Individual asap{individual_address, false};
        A_Restart_req(Ack_Request::dont_care, mpp_ChannelNumber, mpp_EraseCode, Priority::low, Network_Layer_Parameter, mpp_RestartType, asap);
        DMP_Restart_RCl_timeout_timer_restart(std::chrono::milliseconds(3000));
    }
}

void Device_Management::DMP_Restart_RCo_req(const Individual_Address individual_address, const Restart_Type mpp_RestartType, const Restart_Erase_Code mpp_EraseCode, const Restart_Channel_Number mpp_ChannelNumber)
{
    if (DMP_Restart_RCo_data.state == 0) {
        DMP_Restart_RCo_data.state = 1;
        DMP_Restart_RCo_data.individual_address = individual_address;
        DMP_Restart_RCo_data.mpp_RestartType = mpp_RestartType;
        DMP_Restart_RCo_data.mpp_EraseCode = mpp_EraseCode;
        DMP_Restart_RCo_data.mpp_ChannelNumber = mpp_ChannelNumber;
        assert(A_Restart_req);
        const ASAP_Individual asap{individual_address, true};
        A_Restart_req(Ack_Request::dont_care, mpp_ChannelNumber, mpp_EraseCode, Priority::low, Network_Layer_Parameter, mpp_RestartType, asap);
        DMP_Restart_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
    }
}

void Device_Management::DMP_Restart_LEmi1_req()
{
    assert(PC_Set_Value_req);
    Programming_Mode_Property programming_mode_property;
    programming_mode_property.reset = true;
    PC_Set_Value_req(1, 0x60, programming_mode_property.toData());
    if (DMP_Restart_LEmi1_Lcon) {
        DMP_Restart_LEmi1_Lcon(Status::ok);
    }
}

void Device_Management::DM_Delay_req(const DM_Delay_Flags flags, const std::chrono::milliseconds delay_time)
{
    DMP_Delay_req(delay_time);
}

void Device_Management::DMP_Delay_req(const std::chrono::milliseconds delay_time)
{
    if (DMP_Delay_data.state == 0) {
        DMP_Delay_data.state = 1;
        DMP_Delay_data.delay_time = delay_time;
        DMP_Delay_timeout_timer_restart(delay_time);
    }
}

void Device_Management::DM_IndividualAddressRead_req()
{
    DMP_IndividualAddressRead_LEmi1_req();
}

void Device_Management::DMP_IndividualAddressRead_LEmi1_req()
{
    if (DMP_IndividualAddressRead_LEmi1_data.state == 0) {
        DMP_IndividualAddressRead_LEmi1_data.state = 1;
        assert(PC_Get_Value_req);
        PC_Get_Value_req(2, 0x0117);
    }
}

void Device_Management::DM_IndividualAddressWrite_req(const Individual_Address individual_address)
{
    DMP_IndividualAddressWrite_LEmi1_req(individual_address);
}

void Device_Management::DMP_IndividualAddressWrite_LEmi1_req(const Individual_Address IAnew)
{
    if (DMP_IndividualAddressWrite_LEmi1_data.state == 0) {
        DMP_IndividualAddressWrite_LEmi1_data.state = 1;
        DMP_IndividualAddressWrite_LEmi1_data.IAnew = IAnew;
        assert(PC_Set_Value_req);
        std::vector<uint8_t> data;
        data.push_back(IAnew >> 8);
        data.push_back(IAnew & 0xff);
        PC_Set_Value_req(2, 0x0117, data);
        assert(PC_Get_Value_req);
        PC_Get_Value_req(2, 0x0117);
    }
}

void Device_Management::DM_DomainAddress_Read_req()
{
    DMP_DomainAddressRead_LEmi1_req();
}

void Device_Management::DMP_DomainAddressRead_LEmi1_req()
{
    if (DMP_DomainAddressRead_LEmi1_data.state == 0) {
        DMP_DomainAddressRead_LEmi1_data.state = 1;
        assert(PC_Get_Value_req);
        PC_Get_Value_req(2, 0x102);
    }
}

void Device_Management::DM_DomainAddressWrite_req(const Domain_Address_2 domain_address_new)
{
    DMP_DomainAddressWrite_LEmi1_req(domain_address_new);
}

void Device_Management::DMP_DomainAddressWrite_LEmi1_req(const Domain_Address_2 domain_address_new)
{
    if (DMP_DomainAddressWrite_LEmi1_data.state == 0) {
        DMP_DomainAddressWrite_LEmi1_data.state = 1;
        DMP_DomainAddressWrite_LEmi1_data.domain_address_new = domain_address_new;
        assert(PC_Set_Value_req);
        PC_Set_Value_req(2, 0x0102, domain_address_new.toData());
        assert(PC_Get_Value_req);
        PC_Get_Value_req(2, 0x102);
    }
}

void Device_Management::DM_ProgMode_Switch_req(const DM_ProgMode_Switch_Flags flags, const uint8_t mode)
{
    // @todo DM_ProgMode_Switch_req
}

void Device_Management::DMP_ProgModeSwitch_RCo_req(const Individual_Address individual_address, const uint8_t mode)
{
    if (DMP_ProgModeSwitch_RCo_data.state == 0) {
        DMP_ProgModeSwitch_RCo_data.state = 1;
        DMP_ProgModeSwitch_RCo_data.individual_address = individual_address;
        DMP_ProgModeSwitch_RCo_data.mode = mode;
        assert(A_Memory_Read_req);
        const ASAP_Connected asap{individual_address, true};
        A_Memory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 1, 0x60);
    }
}

void Device_Management::DMP_ProgModeSwitch_LEmi1_req(const uint8_t mode)
{
    if (DMP_ProgModeSwitch_LEmi1_data.state == 0) {
        DMP_ProgModeSwitch_LEmi1_data.state = 1;
        DMP_ProgModeSwitch_LEmi1_data.mode = mode;
        assert(PC_Get_Value_req);
        PC_Get_Value_req(1, 0x60);
    }
}

void Device_Management::DM_PeiTypeVerify_req(const DM_PeiTypeVerify_Flags flags, const uint8_t dataBlockStartAddress, const uint8_t data)
{
    // location_of_data_in_management_control
    // @todo DM_PeiTypeVerify_req
}

void Device_Management::DMP_PeiTypeVerify_RCo_ADC_req(const Individual_Address individual_address)
{
    if (DMP_PeiTypeVerify_RCo_ADC_data.state == 0) {
        DMP_PeiTypeVerify_RCo_ADC_data.state = 1;
        DMP_PeiTypeVerify_RCo_ADC_data.individual_address = individual_address;
        assert(A_ADC_Read_req);
        const ASAP_Connected asap{individual_address, true};
        A_ADC_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 4, 1);
    }
}

void Device_Management::DMP_PeiTypeVerify_R_IO_req(const ASAP_Individual asap)
{
    if (DMP_PeiTypeVerify_R_IO_data.state == 0) {
        DMP_PeiTypeVerify_R_IO_data.state = 1;
        DMP_PeiTypeVerify_R_IO_data.asap = asap;
        assert(A_PropertyDescription_Read_req);
        A_PropertyDescription_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 0x00, Device_Object::PID_PEI_TYPE, 0);
    }
}

void Device_Management::DM_PeiTypeRead_req(const DM_PeiTypeRead_Flags flags, const uint8_t dataBlockStartAddress)
{
    // location_of_data_in_management_control
    // @todo DM_PeiTypeRead_req
}

void Device_Management::DMP_PeiTypeRead_RCo_ADC_req(const Individual_Address individual_address)
{
    if (DMP_PeiTypeRead_RCo_ADC_data.state == 0) {
        DMP_PeiTypeRead_RCo_ADC_data.state = 1;
        DMP_PeiTypeRead_RCo_ADC_data.individual_address = individual_address;
        assert(A_ADC_Read_req);
        const ASAP_Connected asap{individual_address, true};
        A_ADC_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 4, 1);
    }
}

void Device_Management::DMP_PeiTypeRead_R_IO_req(const ASAP_Individual asap)
{
    if (DMP_PeiTypeRead_R_IO_data.state == 0) {
        DMP_PeiTypeRead_R_IO_data.state = 1;
        DMP_PeiTypeRead_R_IO_data.asap = asap;
        assert(A_PropertyDescription_Read_req);
        A_PropertyDescription_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 0x00, Device_Object::PID_PEI_TYPE, 0);
    }
}

void Device_Management::DM_MemWrite_req(const DM_MemWrite_Flags flags, const uint8_t dataBlockStartAddress, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
    // location_of_data_in_management_control
    // verify_enabled
    // @todo DM_MemWrite_req
}

void Device_Management::DMP_MemWrite_RCo_req(const Individual_Address individual_address, const bool verify, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_MemWrite_RCo_data.state == 0) {
        DMP_MemWrite_RCo_data.individual_address = individual_address;
        DMP_MemWrite_RCo_data.verify = verify;
        DMP_MemWrite_RCo_data.deviceStartAddress = deviceStartAddress;
        DMP_MemWrite_RCo_data.deviceEndAddress = deviceEndAddress;
        DMP_MemWrite_RCo_data.data = data;
        DMP_MemWrite_RCo_data.offset = 0;
        if (DMP_MemWrite_RCo_data.offset >= data.size()) {
            DMP_MemWrite_RCo_data.state = 0;
            if (DMP_MemWrite_RCo_Lcon) {
                DMP_MemWrite_RCo_Lcon(individual_address, verify, deviceStartAddress, deviceEndAddress, data, Status::ok);
            }
        } else {
            DMP_MemWrite_RCo_data.state = 1;
            assert(A_Memory_Write_req);
            const ASAP_Connected asap{individual_address, true};
            Memory_Number number = DMP_MemWrite_RCo_data.data.size() - DMP_MemWrite_RCo_data.offset;
            // reduce to connection limit
            if (number > 12) { // @todo size is medium dependent
                number = 12;
            }
            Memory_Data data;
            data.assign(DMP_MemWrite_RCo_data.data.cbegin() + DMP_MemWrite_RCo_data.offset, DMP_MemWrite_RCo_data.data.cbegin() + DMP_MemWrite_RCo_data.offset + number);
            A_Memory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, number, DMP_MemWrite_RCo_data.deviceStartAddress + DMP_MemWrite_RCo_data.offset, data);
        }
    }
}

void Device_Management::DMP_MemWrite_RCoV_req(const Individual_Address individual_address, const bool verify, const bool device_control_unknown, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_MemWrite_RCoV_data.state == 0) {
        DMP_MemWrite_RCoV_data.individual_address = individual_address;
        DMP_MemWrite_RCoV_data.verify = verify;
        DMP_MemWrite_RCoV_data.deviceStartAddress = deviceStartAddress;
        DMP_MemWrite_RCoV_data.deviceEndAddress = deviceEndAddress;
        DMP_MemWrite_RCoV_data.data = data;
        DMP_MemWrite_RCoV_data.offset = 0;
        if (device_control_unknown) {
            DMP_MemWrite_RCoV_data.state = 1;
            assert(A_PropertyDescription_Read_req);
            const ASAP_Individual asap{individual_address, true};
            A_PropertyDescription_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, OT_DEVICE, Device_Object::PID_DEVICE_CONTROL, 0);
        } else {
            if (DMP_MemWrite_RCoV_data.offset >= data.size()) {
                DMP_MemWrite_RCoV_data.state = 0;
                if (DMP_MemWrite_RCoV_Lcon) {
                    DMP_MemWrite_RCoV_Lcon(individual_address, verify, device_control_unknown, deviceStartAddress, deviceEndAddress, data, Status::ok);
                }
            } else {
                DMP_MemWrite_RCoV_data.state = 3;
                assert(A_PropertyValue_Read_req);
                const ASAP_Individual asap{individual_address, true};
                A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, OT_DEVICE, Device_Object::PID_DEVICE_CONTROL, 1, 1);
            }
        }
    }
}

void Device_Management::DMP_MemWrite_LEmi1_req(const bool verify, const Memory_Address DmpStartAddr, const Memory_Address DmpEndAddr, const Memory_Data DmpData)
{
    if (DMP_MemWrite_LEmi1_data.state == 0) {
        DMP_MemWrite_LEmi1_data.verify = verify;
        DMP_MemWrite_LEmi1_data.DmpStartAddr = DmpStartAddr;
        DMP_MemWrite_LEmi1_data.DmpEndAddr = DmpEndAddr;
        DMP_MemWrite_LEmi1_data.DmpData = DmpData;
        DMP_MemWrite_LEmi1_data.offset = 0;
        if (DMP_MemWrite_LEmi1_data.offset >= DmpData.size()) {
            DMP_MemWrite_LEmi1_data.state = 0;
            if (DMP_MemWrite_LEmi1_Lcon) {
                DMP_MemWrite_LEmi1_Lcon(verify, DmpStartAddr, DmpEndAddr, DmpData, Status::ok);
            }
        } else {
            assert(PC_Set_Value_req);
            Memory_Number length = DMP_MemWrite_LEmi1_data.DmpData.size() - DMP_MemWrite_LEmi1_data.offset;
            // reduce to 12
            if (length > 12) {
                length = 12;
            }
            Memory_Data data;
            data.assign(DMP_MemWrite_LEmi1_data.DmpData.cbegin() + DMP_MemWrite_LEmi1_data.offset, DMP_MemWrite_LEmi1_data.DmpData.cbegin() + DMP_MemWrite_LEmi1_data.offset + length);
            if (DMP_MemWrite_LEmi1_data.verify) {
                DMP_MemWrite_LEmi1_data.state = 1;
                PC_Set_Value_req(length, DMP_MemWrite_LEmi1_data.DmpStartAddr + DMP_MemWrite_LEmi1_data.offset, data);
                if (PC_Get_Value_req) {
                    PC_Get_Value_req(length, DMP_MemWrite_LEmi1_data.DmpStartAddr + DMP_MemWrite_LEmi1_data.offset);
                }
            } else {
                DMP_MemWrite_LEmi1_data.state = 2;
                PC_Set_Value_req(length, DMP_MemWrite_LEmi1_data.DmpStartAddr + DMP_MemWrite_LEmi1_data.offset, data);
                DMP_MemWrite_LEmi1_timeout_timer_restart(std::chrono::milliseconds(3000));
            }
        }
    }
}

void Device_Management::DM_MemVerify_req(const DM_MemVerify_Flags flags, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
    // location_of_data_in_management_control
    // @todo DM_MemVerify_req
}

void Device_Management::DMP_MemVerify_RCo_req(const Individual_Address individual_address, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_MemVerify_RCo_data.state == 0) {
        DMP_MemVerify_RCo_data.individual_address = individual_address;
        DMP_MemVerify_RCo_data.deviceStartAddress = deviceStartAddress;
        DMP_MemVerify_RCo_data.deviceEndAddress = deviceEndAddress;
        DMP_MemVerify_RCo_data.data = data;
        DMP_MemVerify_RCo_data.offset = 0;
        if (DMP_MemVerify_RCo_data.offset >= DMP_MemVerify_RCo_data.data.size()) {
            DMP_MemVerify_RCo_data.state = 0;
            if (DMP_MemVerify_RCo_Lcon) {
                DMP_MemVerify_RCo_Lcon(individual_address, deviceStartAddress, deviceEndAddress, data, Status::ok);
            }
        } else {
            DMP_MemVerify_RCo_data.state = 1;
            assert(A_Memory_Read_req);
            const ASAP_Connected asap{individual_address, true};
            Memory_Number number = DMP_MemVerify_RCo_data.data.size() - DMP_MemVerify_RCo_data.offset;
            // reduce to connection limit
            if (number > 12) { // @todo size is medium dependent
                number = 12;
            }
            A_Memory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, number, DMP_MemVerify_RCo_data.deviceStartAddress + DMP_MemVerify_RCo_data.offset);
        }
    }
}

void Device_Management::DMP_MemVerify_LEmi1_req(const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_MemVerify_LEmi1_data.state == 0) {
        DMP_MemVerify_LEmi1_data.deviceStartAddress = deviceStartAddress;
        DMP_MemVerify_LEmi1_data.deviceEndAddress = deviceEndAddress;
        DMP_MemVerify_LEmi1_data.data = data;
        DMP_MemVerify_LEmi1_data.offset = 0;
        if (DMP_MemVerify_LEmi1_data.offset >= DMP_MemVerify_LEmi1_data.data.size()) {
            DMP_MemVerify_LEmi1_data.state = 0;
            if (DMP_MemVerify_LEmi1_Lcon) {
                DMP_MemVerify_LEmi1_Lcon(deviceStartAddress, deviceEndAddress, data, Status::ok);
            }
        } else {
            DMP_MemVerify_LEmi1_data.state = 1;
            assert(PC_Get_Value_req);
            Memory_Number number = DMP_MemVerify_LEmi1_data.data.size() - DMP_MemVerify_LEmi1_data.offset;
            // reduce to 12
            if (number > 12) {
                number = 12;
            }
            PC_Get_Value_req(number, DMP_MemVerify_LEmi1_data.deviceStartAddress + DMP_MemVerify_LEmi1_data.offset);
        }
    }
}

void Device_Management::DM_MemRead_req(const DM_MemRead_Flags flags, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress)
{
    // location_of_data_in_management_control
    // @todo DM_MemRead_req
}

void Device_Management::DMP_MemRead_RCo_req(const Individual_Address individual_address, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_MemRead_RCo_data.state == 0) {
        DMP_MemRead_RCo_data.individual_address = individual_address;
        DMP_MemRead_RCo_data.deviceStartAddress = deviceStartAddress;
        DMP_MemRead_RCo_data.deviceEndAddress = deviceEndAddress;
        DMP_MemRead_RCo_data.data = data;
        DMP_MemRead_RCo_data.offset = 0;
        if (DMP_MemRead_RCo_data.offset >= DMP_MemRead_RCo_data.data.size()) {
            DMP_MemRead_RCo_data.state = 0;
            if (DMP_MemRead_RCo_Lcon) {
                DMP_MemRead_RCo_Lcon(individual_address, deviceStartAddress, deviceEndAddress, data, Status::ok);
            }
        } else {
            DMP_MemRead_RCo_data.state = 1;
            assert(A_Memory_Read_req);
            const ASAP_Connected asap{individual_address, true};
            Memory_Number number = DMP_MemRead_RCo_data.data.size() - DMP_MemRead_RCo_data.offset;
            // reduce to connection limit
            if (number > 12) { // @todo size is medium dependent
                number = 12;
            }
            A_Memory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, number, DMP_MemRead_RCo_data.deviceStartAddress + DMP_MemRead_RCo_data.offset);
        }
    }
}

void Device_Management::DMP_MemRead_LEmi1_req(const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_MemRead_LEmi1_data.state == 0) {
        DMP_MemRead_LEmi1_data.deviceStartAddress = deviceStartAddress;
        DMP_MemRead_LEmi1_data.deviceEndAddress = deviceEndAddress;
        DMP_MemRead_LEmi1_data.data = data;
        DMP_MemRead_LEmi1_data.offset = 0;
        if (DMP_MemRead_LEmi1_data.offset >= DMP_MemRead_LEmi1_data.data.size()) {
            DMP_MemRead_LEmi1_data.state = 0;
            if (DMP_MemVerify_LEmi1_Lcon) {
                DMP_MemVerify_LEmi1_Lcon(deviceStartAddress, deviceEndAddress, data, Status::ok);
            }
        } else {
            DMP_MemRead_LEmi1_data.state = 1;
            assert(PC_Get_Value_req);
            Memory_Number number = DMP_MemRead_LEmi1_data.data.size() - DMP_MemRead_LEmi1_data.offset;
            // reduce to 12
            if (number > 12) {
                number = 12;
            }
            PC_Get_Value_req(number, DMP_MemRead_LEmi1_data.deviceStartAddress + DMP_MemRead_LEmi1_data.offset);
        }
    }
}

void Device_Management::DM_UserMemWrite_req(const DM_UserMemWrite_Flags flags, const uint8_t dataBlockStartAddress, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data)
{
    // location_of_data_in_management_control
    // verify_enabled
    // @todo DM_UserMemWrite_req
}

void Device_Management::DMP_UserMemWrite_RCo_req(const Individual_Address individual_address, const bool verify, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_UserMemWrite_RCo_data.state == 0) {
        DMP_UserMemWrite_RCo_data.individual_address = individual_address;
        DMP_UserMemWrite_RCo_data.verify = verify;
        DMP_UserMemWrite_RCo_data.deviceStartAddress = deviceStartAddress;
        DMP_UserMemWrite_RCo_data.deviceEndAddress = deviceEndAddress;
        DMP_UserMemWrite_RCo_data.data = data;
        DMP_UserMemWrite_RCo_data.offset = 0;
        if (DMP_UserMemWrite_RCo_data.offset >= data.size()) {
            DMP_UserMemWrite_RCo_data.state = 0;
            if (DMP_UserMemWrite_RCo_Lcon) {
                DMP_UserMemWrite_RCo_Lcon(individual_address, verify, deviceStartAddress, deviceEndAddress, data, Status::ok);
            }
        } else {
            DMP_UserMemWrite_RCo_data.state = 1;
            assert(A_UserMemory_Write_req);
            const ASAP_Connected asap{individual_address, true};
            UserMemory_Number number = DMP_UserMemWrite_RCo_data.data.size() - DMP_UserMemWrite_RCo_data.offset;
            // reduce to connection limit
            if (number > 11) { // @todo size is medium dependent
                number = 11;
            }
            Memory_Data data;
            data.assign(DMP_UserMemWrite_RCo_data.data.cbegin() + DMP_UserMemWrite_RCo_data.offset, DMP_UserMemWrite_RCo_data.data.cbegin() + DMP_UserMemWrite_RCo_data.offset + number);
            A_UserMemory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, number, DMP_UserMemWrite_RCo_data.deviceStartAddress + DMP_UserMemWrite_RCo_data.offset, data);
        }
    }
}

void Device_Management::DMP_UserMemWrite_RCoV_req(const Individual_Address individual_address, const bool device_control_unknown, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_UserMemWrite_RCoV_data.state == 0) {
        DMP_UserMemWrite_RCoV_data.individual_address = individual_address;
        DMP_UserMemWrite_RCoV_data.device_control_unknown = device_control_unknown;
        DMP_UserMemWrite_RCoV_data.deviceStartAddress = deviceStartAddress;
        DMP_UserMemWrite_RCoV_data.deviceEndAddress = deviceEndAddress;
        DMP_UserMemWrite_RCoV_data.data = data;
        // @todo DMP_UserMemWrite_RCoV_req
    }
}

void Device_Management::DM_UserMemVerify_req(const DM_UserMemVerify_Flags flags, const uint8_t dataBlockStartAddress, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data)
{
    // location_of_data_in_management_control
    // @todo DM_UserMemVerify_req
}

void Device_Management::DMP_UserMemVerify_RCo_req(const Individual_Address individual_address, const bool device_control_unknown, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_UserMemVerify_RCo_data.state == 0) {
        DMP_UserMemVerify_RCo_data.individual_address = individual_address;
        DMP_UserMemVerify_RCo_data.device_control_unknown = device_control_unknown;
        DMP_UserMemVerify_RCo_data.deviceStartAddress = deviceStartAddress;
        DMP_UserMemVerify_RCo_data.deviceEndAddress = deviceEndAddress;
        DMP_UserMemVerify_RCo_data.data = data;
        // @todo DMP_UserMemVerify_RCo_req
    }
}

void Device_Management::DM_UserMemRead_req(const DM_UserMemRead_Flags flags, const uint8_t dataBlockStartAddress, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress)
{
    // location_of_data_in_management_control
    // @todo DM_UserMemRead_req
}

void Device_Management::DMP_UserMemRead_RCo_req(const Individual_Address individual_address, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data)
{
    if (DMP_UserMemRead_RCo_data.state == 0) {
        DMP_UserMemRead_RCo_data.individual_address = individual_address;
        DMP_UserMemRead_RCo_data.deviceStartAddress = deviceStartAddress;
        DMP_UserMemRead_RCo_data.deviceEndAddress = deviceEndAddress;
        DMP_UserMemRead_RCo_data.data = data;
        // @todo DMP_UserMemRead_RCo_req
    }
}

void Device_Management::DM_InterfaceObjectWrite_req(const DM_InterfaceObjectWrite_Flags flags, const uint8_t dataBlockStartAddress, const Object_Type object_type, const Object_Index object_index, const Property_Id PID, const PropertyValue_Start_Index start_index, const PropertyValue_Nr_Of_Elem noElements, const Property_Value data)
{
    // location_of_data_in_management_control
    // verify_enabled
    // address_via_object_index
    // @todo DM_InterfaceObjectWrite_req
}

void Device_Management::DMP_InterfaceObjectWrite_R_req()
{
    if (DMP_InterfaceObjectWrite_R_data.state == 0) {
        DMP_InterfaceObjectWrite_R_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        // @todo DMP_InterfaceObjectWrite_R_req
    }
}

void Device_Management::DMP_ReducedInterfaceObjectWrite_R_req()
{
    if (DMP_ReducedInterfaceObjectWrite_R_data.state == 0) {
        DMP_ReducedInterfaceObjectWrite_R_data.state = 1;
        // @todo DMP_ReducedInterfaceObjectWrite_R_req
    }
}

void Device_Management::DM_InterfaceObjectVerify_req(const DM_InterfaceObjectVerify_Flags flags, const uint8_t dataBlockStartAddress, const Object_Type object_type, const Object_Index object_index, const Property_Id PID, const PropertyValue_Start_Index start_index, const PropertyValue_Nr_Of_Elem noElements, const Property_Value data)
{
    // location_of_data_in_management_control
    // reserved
    // address_via_object_index
    // @todo DM_InterfaceObjectVerify_req
}

void Device_Management::DMP_InterfaceObjectVerify_R_req()
{
    if (DMP_InterfaceObjectVerify_R_data.state == 0) {
        DMP_InterfaceObjectVerify_R_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        // @todo DMP_InterfaceObjectVerify_R_req
    }
}

void Device_Management::DM_InterfaceObjectRead_req(const DM_InterfaceObjectRead_Flags flags, const uint8_t dataBlockStartAddress, const Object_Type object_type, const Object_Index object_index, const Property_Id PID, const PropertyValue_Start_Index start_index, const PropertyValue_Nr_Of_Elem noElements)
{
    // location_of_data_in_management_control
    // reserved
    // address_via_object_index
    // @todo DM_InterfaceObjectRead_req
}

void Device_Management::DMP_InterfaceObjectRead_R_req()
{
    if (DMP_InterfaceObjectRead_R_data.state == 0) {
        DMP_InterfaceObjectRead_R_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        // @todo DMP_InterfaceObjectRead_R_req
    }
}

void Device_Management::DMP_ReducedInterfaceObjectRead_R_req()
{
    if (DMP_ReducedInterfaceObjectRead_R_data.state == 0) {
        DMP_ReducedInterfaceObjectRead_R_data.state = 1;
        // @todo DMP_ReducedInterfaceObjectRead_R_req
    }
}

void Device_Management::DM_InterfaceObjectScan_req(const DM_InterfaceObjectScan_Flags flags, const uint8_t dataBlockStartAddress, const uint8_t object_index, const uint8_t data)
{
    // location_of_data_in_management_control
    // reserved
    // scan_all_properties
    // scan_all_interface_objects
    // @todo DM_InterfaceObjectScan_req
}

void Device_Management::DMP_InterfaceObjectScan_R_req()
{
    if (DMP_InterfaceObjectScan_R_data.state == 0) {
        DMP_InterfaceObjectScan_R_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        // @todo DMP_InterfaceObjectScan_R_req
    }
}

void Device_Management::DMP_ReducedInterfaceObjectScan_R_req()
{
    if (DMP_ReducedInterfaceObjectScan_R_data.state == 0) {
        DMP_ReducedInterfaceObjectScan_R_data.state = 1;
        // @todo DMP_ReducedInterfaceObjectScan_R_req
    }
}

void Device_Management::DM_InterfaceObjectInfoReport_req()
{
    // @todo DM_InterfaceObjectInfoReport_req
}

void Device_Management::DMP_InterfaceObject_InfoReport_RCl_req(const ASAP_Individual mpp_ASAP, const Comm_Mode mpp_comm_mode, const Hop_Count_Type mpp_hop_count_type, const Object_Type mpp_object_type, const Property_Id mpp_PID, const Priority mpp_priority, const Parameter_Test_Info mpp_test_info, const Parameter_Test_Result mpp_test_result)
{
    if (DMP_InterfaceObject_InfoReport_RCl_data.state == 0) {
        DMP_InterfaceObject_InfoReport_RCl_data.state = 1;
        //assert(A_NetworkParameter_InfoReport_req);
        // @todo DMP_InterfaceObject_InfoReport_RCl_req
    }
}

void Device_Management::DMP_FunctionProperty_Write_R_req()
{
    if (DMP_FunctionProperty_Write_R_data.state == 0) {
        DMP_FunctionProperty_Write_R_data.state = 1;
        //assert(A_FunctionPropertyCommand_req);
        // @todo DMP_FunctionProperty_Write_R_req
    }
}

void Device_Management::DM_LoadStateMachineWrite_req(const DM_LoadStateMachineWrite_Flags flags, const Object_Type stateMachineType,  const Object_Index stateMachineNr, const uint8_t event, const uint8_t eventData)
{
    // location_of_data_in_management_control
    // verify_resulting_state_enabled
    // @todo DM_LoadStateMachineWrite_req
}

void Device_Management::DMP_LoadStateMachineWrite_RCo_Mem_req()
{
    if (DMP_LoadStateMachineWrite_RCo_Mem_data.state == 0) {
        DMP_LoadStateMachineWrite_RCo_Mem_data.state = 1;
        assert(A_Memory_Write_req);
        // @todo DMP_LoadStateMachineWrite_RCo_Mem_req
    }
}

void Device_Management::DMP_LoadStateMachineWrite_RCo_IO_req()
{
    if (DMP_LoadStateMachineWrite_RCo_IO_data.state == 0) {
        DMP_LoadStateMachineWrite_RCo_IO_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        // @todo DMP_LoadStateMachineWrite_RCo_IO_req
    }
}

void Device_Management::DMP_DownloadLoadablePart_RCo_IO_req()
{
    if (DMP_DownloadLoadablePart_RCo_IO_data.state == 0) {
        DMP_DownloadLoadablePart_RCo_IO_data.state = 1;
        assert(A_PropertyValue_Write_req);
        // @todo DMP_DownloadLoadablePart_RCo_IO_req
    }
}

void Device_Management::DM_LoadStateMachineVerify_req(const DM_LoadStateMachineVerify_Flags flags, const Object_Type stateMachineType, const Object_Index stateMachineNr, const uint8_t state)
{
    // location_of_data_in_management_control
    // @todo DM_LoadStateMachineVerify_req
}

void Device_Management::DMP_LoadStateMachineVerify_RCo_Mem_req()
{
    if (DMP_LoadStateMachineVerify_RCo_Mem_data.state == 0) {
        DMP_LoadStateMachineVerify_RCo_Mem_data.state = 1;
        assert(A_Memory_Read_req);
        // @todo DMP_LoadStateMachineVerify_RCo_Mem_req
    }
}

void Device_Management::DMP_LoadStateMachineVerify_R_IO_req()
{
    if (DMP_LoadStateMachineVerify_R_IO_data.state == 0) {
        DMP_LoadStateMachineVerify_R_IO_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        assert(A_PropertyValue_Read_req);
        // @todo DMP_LoadStateMachineVerify_R_IO_req
    }
}

void Device_Management::DM_LoadStateMachineRead_req(const Individual_Address destination_address, const DM_LoadStateMachineRead_Flags flags, const Object_Type state_machine_type, const Object_Index state_machine_nr)
{
    if (flags.location_of_data_in_data_block) {
        DMP_LoadStateMachineRead_RCo_Mem_req(destination_address, state_machine_type);
    } else {
        DMP_LoadStateMachineRead_R_IO_req(destination_address, state_machine_nr);
    }
}

void Device_Management::DMP_LoadStateMachineRead_RCo_Mem_req(const Individual_Address destination_address, const Object_Type state_machine_type)
{
    if (DMP_LoadStateMachineRead_RCo_Mem_data.state == 0) {
        DMP_LoadStateMachineRead_RCo_Mem_data.state = 1;
        DMP_LoadStateMachineRead_RCo_Mem_data.destination_address = destination_address;
        DMP_LoadStateMachineRead_RCo_Mem_data.state_machine_type = state_machine_type;

        // @todo only valid for mask version 070n (BIM M112)

        switch (state_machine_type) {
        case OT_ADDRESS_TABLE:
            // address table
            DMP_LoadStateMachineRead_RCo_Mem_data.memory_address = 0xB6EA;
            break;
        case OT_ASSOCIATION_TABLE:
            // association table
            DMP_LoadStateMachineRead_RCo_Mem_data.memory_address = 0xB6EB;
            break;
        case OT_APPLICATION_PROGRAM:
            // application program
            DMP_LoadStateMachineRead_RCo_Mem_data.memory_address = 0xB6EC;
            break;
        case OT_INTERFACE_PROGRAM:
            // PEI program
            DMP_LoadStateMachineRead_RCo_Mem_data.memory_address = 0xB6ED;
            break;
        default:
            assert(false);
        }

        assert(A_Memory_Read_req);
        const ASAP_Connected asap{destination_address, true};
        A_Memory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 1, DMP_LoadStateMachineRead_RCo_Mem_data.memory_address);
    }
}

void Device_Management::DMP_LoadStateMachineRead_R_IO_req(const Individual_Address destination_address, const Object_Index state_machine_nr)
{
    if (DMP_LoadStateMachineRead_R_IO_data.state == 0) {
        DMP_LoadStateMachineRead_R_IO_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        // @todo DMP_LoadStateMachineRead_R_IO_req
    }
}

void Device_Management::DM_RunStateMachineWrite_req(const DM_RunStateMachineWrite_Flags flags, const Object_Type stateMachineType, const Object_Index stateMachineNr, const uint8_t event)
{
    // location_of_data_in_management_control
    // verify_resulting_state_enabled
    // @todo DM_RunStateMachineWrite_req
}

void Device_Management::DMP_RunStateMachineWrite_RCo_Mem_req()
{
    if (DMP_RunStateMachineWrite_RCo_Mem_data.state == 0) {
        DMP_RunStateMachineWrite_RCo_Mem_data.state = 1;
        assert(A_Memory_Write_req);
        // @todo DMP_RunStateMachineWrite_RCo_Mem_req
    }
}

void Device_Management::DMP_RunStateMachineWrite_R_IO_req()
{
    if (DMP_RunStateMachineWrite_R_IO_data.state == 0) {
        DMP_RunStateMachineWrite_R_IO_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        // @todo DMP_RunStateMachineWrite_R_IO_req
    }
}

void Device_Management::DM_RunStateMachineVerify_req(const DM_RunStateMachineVerify_Flags flags, const Object_Type stateMachineType, const Object_Index stateMachineNr, const uint8_t state)
{
    // location_of_data_in_data_block
    // @todo DM_RunStateMachineVerify_req
}

void Device_Management::DMP_RunStateMachineVerify_RCo_Mem_req()
{
    if (DMP_RunStateMachineVerify_RCo_Mem_data.state == 0) {
        DMP_RunStateMachineVerify_RCo_Mem_data.state = 1;
        assert(A_Memory_Read_req);
        // @todo DMP_RunStateMachineVerify_RCo_Mem_req
    }
}

void Device_Management::DMP_RunStateMachineVerify_R_IO_req()
{
    if (DMP_RunStateMachineVerify_R_IO_data.state == 0) {
        DMP_RunStateMachineVerify_R_IO_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        // @todo DMP_RunStateMachineVerify_R_IO_req
    }
}

void Device_Management::DM_RunStateMachineRead_req(const DM_RunStateMachineRead_Flags flags, const uint8_t dataBlockStartAddress, const Object_Type stateMachineType, const Object_Index stateMachineNr)
{
    // location_of_data_in_data_block
    // @todo DM_RunStateMachineRead_req
}

void Device_Management::DMP_RunStateMachineRead_RCo_Mem_req()
{
    if (DMP_RunStateMachineRead_RCo_Mem_data.state == 0) {
        DMP_RunStateMachineRead_RCo_Mem_data.state = 1;
        assert(A_Memory_Read_req);
        // @todo DMP_RunStateMachineRead_RCo_Mem_req
    }
}

void Device_Management::DMP_RunStateMachineRead_R_IO_req()
{
    if (DMP_RunStateMachineRead_R_IO_data.state == 0) {
        DMP_RunStateMachineRead_R_IO_data.state = 1;
        assert(A_PropertyDescription_Read_req);
        // @todo DMP_RunStateMachineRead_R_IO_req
    }
}

void Device_Management::DM_GroupObjectLink_Read_req()
{
    // @todo DM_GroupObjectLink_Read_req
}

void Device_Management::DM_GroupObjectLink_Write_req()
{
    // @todo DM_GroupObjectLink_Write_req
}

void Device_Management::DMP_GroupObjectLink_Read_RCl_req(const GO GO)
{
    if (DMP_GroupObjectLink_Read_RCl_data.state == 0) {
        DMP_GroupObjectLink_Read_RCl_data.state = 1;
        assert(A_Link_Read_req);
        // @todo DMP_GroupObjectLink_Read_RCl_req
    }
}

void Device_Management::DMP_GroupObjectLink_Write_RCl_req(const GO GO)
{
    if (DMP_GroupObjectLink_Write_RCl_data.state == 0) {
        DMP_GroupObjectLink_Write_RCl_data.state = 1;
        assert(A_Link_Write_req);
        // @todo DMP_GroupObjectLink_Write_RCl_req
    }
}

void Device_Management::DM_LCSlaceMemWrite_req(const DM_LCSlaceMemWrite_Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data)
{
    // location_of_data_in_data_block
    // verify_enabled
    // @todo DM_LCSlaceMemWrite_req
}

void Device_Management::DMP_LCSlaveMemWrite_RCo_req()
{
    if (DMP_LCSlaveMemWrite_RCo_data.state == 0) {
        DMP_LCSlaveMemWrite_RCo_data.state = 1;
        assert(A_Write_Router_Memory_req);
        // @todo DMP_LCSlaveMemWrite_RCo_req
    }
}

void Device_Management::DM_LCSlaveMemVerify_req(const DM_LCSlaveMemVerify_Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data)
{
    // location_of_data_in_data_block
    // @todo DM_LCSlaveMemVerify_req
}

void Device_Management::DMP_LCSlaveMemVerify_RCo_req()
{
    if (DMP_LCSlaveMemVerify_RCo_data.state == 0) {
        DMP_LCSlaveMemVerify_RCo_data.state = 1;
        assert(A_Read_Router_Memory_req);
        // @todo DMP_LCSlaveMemVerify_RCo_req
    }
}

void Device_Management::DM_LCSlaveMemRead_req(const DM_LCSlaveMemRead_Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data)
{
    // location_of_data_in_data_block
    // @todo DM_LCSlaveMemRead_req
}

void Device_Management::DMP_LCSlaveMemRead_RCo_req()
{
    if (DMP_LCSlaveMemRead_RCo_data.state == 0) {
        DMP_LCSlaveMemRead_RCo_data.state = 1;
        assert(A_Read_Router_Memory_req);
        // @todo DMP_LCSlaveMemRead_RCo_req
    }
}

void Device_Management::DM_LCSlaveMemWrite_req(const DM_LCSlaveMemWrite_Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data)
{
    // location_of_data_in_data_block
    // verify_enabled
    // @todo DM_LCSlaveMemWrite_req
}

void Device_Management::DMP_LCExtMemWrite_RCo_req()
{
    if (DMP_LCExtMemWrite_RCo_data.state == 0) {
        DMP_LCExtMemWrite_RCo_data.state = 1;
        assert(A_Write_Routing_Table_req);
        // @todo DMP_LCExtMemWrite_RCo_req
    }
}

void Device_Management::DM_LCExtMemVerify_req(const DM_LCExtMemVerify_Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data)
{
    // location_of_data_in_data_block
    // @todo DM_LCExtMemVerify_req
}

void Device_Management::DMP_LCExtMemVerify_RCo_req()
{
    if (DMP_LCExtMemVerify_RCo_data.state == 0) {
        DMP_LCExtMemVerify_RCo_data.state = 1;
        assert(A_Read_Routing_Table_req);
        // @todo DMP_LCExtMemVerify_RCo_req
    }
}

void Device_Management::DM_LCExtMemRead_req(const DM_LCExtMemRead_Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data)
{
    // location_of_data_in_data_block
    // @todo DM_LCExtMemRead_req
}

void Device_Management::DMP_LCExtMemRead_RCo_req()
{
    if (DMP_LCExtMemRead_RCo_data.state == 0) {
        DMP_LCExtMemRead_RCo_data.state = 1;
        assert(A_Read_Routing_Table_req);
        // @todo DMP_LCExtMemRead_RCo_req
    }
}

void Device_Management::DM_LCExtMemOpen_req()
{
    // @todo DM_LCExtMemOpen_req
}

void Device_Management::DMP_LCExtMemOpen_RCo_req()
{
    if (DMP_LCExtMemOpen_RCo_data.state == 0) {
        DMP_LCExtMemOpen_RCo_data.state = 1;
        assert(A_Open_Routing_Table_req);
        // @todo DMP_LCExtMemOpen_RCo_req
    }
}

void Device_Management::DM_LCRouteTableStateWrite_req(const DM_LCRouteTableStateWrite_Flags flags, const uint8_t routeTableState)
{
    // location_of_data_in_data_block
    // verify_enabled
    // @todo DM_LCRouteTableStateWrite_req
}

void Device_Management::DMP_LCRouteTableStateWrite_RCo_req()
{
    if (DMP_LCRouteTableStateWrite_RCo_data.state == 0) {
        DMP_LCRouteTableStateWrite_RCo_data.state = 1;
        // assert(A_Write_Router_Status_req);
        // @todo DMP_LCRouteTableStateWrite_RCo_req
    }
}

void Device_Management::DM_LCRouteTableStateVerify_req(const DM_LCRouteTableStateVerify_Flags flags, const uint8_t routeTableState)
{
    // location_of_data_in_data_block
    // @todo DM_LCRouteTableStateVerify_req
}

void Device_Management::DMP_LCRouteTableStateVerify_RCo_req()
{
    if (DMP_LCRouteTableStateVerify_RCo_data.state == 0) {
        DMP_LCRouteTableStateVerify_RCo_data.state = 1;
        // assert(A_Read_Router_Status_req);
        // @todo DMP_LCRouteTableStateVerify_RCo_req
    }
}

void Device_Management::DM_LCRouteTableStateRead_req(const DM_LCRouteTableStateRead_Flags flags, const uint8_t routeTableState)
{
    // location_of_data_in_data_block
    // @todo DM_LCRouteTableStateRead_req
}

void Device_Management::DMP_LCRouteTableStateRead_RCo_req()
{
    if (DMP_LCRouteTableStateRead_RCo_data.state == 0) {
        DMP_LCRouteTableStateRead_RCo_data.state = 1;
        // assert(A_Read_Router_Status_req);
        // @todo DMP_LCRouteTableStateRead_RCo_req
    }
}

/* EMI User Layer */

void Device_Management::PC_Get_Value_con(const uint8_t length, const uint16_t address, const std::vector<uint8_t> data)
{
    if ((DMP_Connect_LEmi1_data.state == 1) && (length == 2) && (address == 0x004E)) {
        DMP_Connect_LEmi1_data.state = 0;
        DMP_Connect_LEmi1_data.DD0.fromData(std::cbegin(data), std::cend(data));
        if (DMP_Connect_LEmi1_Lcon) {
            DMP_Connect_LEmi1_Lcon(DMP_Connect_LEmi1_data.DD0, Status::ok);
        }
    }

    if ((DMP_IndividualAddressRead_LEmi1_data.state == 1) && (length == 2) && (address == 0x0117)) {
        DMP_IndividualAddressRead_LEmi1_data.state = 0;
        DMP_IndividualAddressRead_LEmi1_data.individual_address.fromData(std::cbegin(data), std::cend(data));
        if (DMP_IndividualAddressRead_LEmi1_Lcon) {
            DMP_IndividualAddressRead_LEmi1_Lcon(DMP_IndividualAddressRead_LEmi1_data.individual_address, Status::ok);
        }
    }

    if ((DMP_IndividualAddressWrite_LEmi1_data.state == 1) && (length == 2) && (address == 0x0117)) {
        DMP_IndividualAddressWrite_LEmi1_data.state = 0;
        DMP_IndividualAddressWrite_LEmi1_data.IAresult.fromData(std::cbegin(data), std::cend(data));
        if (DMP_IndividualAddressWrite_LEmi1_Lcon) {
            if (DMP_IndividualAddressWrite_LEmi1_data.IAnew == DMP_IndividualAddressWrite_LEmi1_data.IAresult) {
                DMP_IndividualAddressWrite_LEmi1_Lcon(DMP_IndividualAddressWrite_LEmi1_data.IAnew, DMP_IndividualAddressWrite_LEmi1_data.IAresult, Status::ok);
            } else {
                DMP_IndividualAddressWrite_LEmi1_Lcon(DMP_IndividualAddressWrite_LEmi1_data.IAnew, DMP_IndividualAddressWrite_LEmi1_data.IAresult, Status::not_ok);
            }
        }
    }

    if ((DMP_DomainAddressRead_LEmi1_data.state == 1) && (length == 2) && (address == 0x0102)) {
        DMP_DomainAddressRead_LEmi1_data.state = 0;
        DMP_DomainAddressRead_LEmi1_data.domain_address.fromData(std::cbegin(data), std::cend(data));
        if (DMP_DomainAddressRead_LEmi1_Lcon) {
            DMP_DomainAddressRead_LEmi1_Lcon(DMP_DomainAddressRead_LEmi1_data.domain_address, Status::ok);
        }
    }

    if ((DMP_DomainAddressWrite_LEmi1_data.state == 1) && (length == 2) && (address == 0x0102)) {
        DMP_DomainAddressWrite_LEmi1_data.state = 0;
        DMP_DomainAddressWrite_LEmi1_data.domain_address_result.fromData(std::cbegin(data), std::cend(data));
        if (DMP_DomainAddressWrite_LEmi1_Lcon) {
            if (DMP_DomainAddressWrite_LEmi1_data.domain_address_new == DMP_DomainAddressWrite_LEmi1_data.domain_address_result) {
                DMP_DomainAddressWrite_LEmi1_Lcon(DMP_DomainAddressWrite_LEmi1_data.domain_address_new, Status::ok);
            } else {
                DMP_DomainAddressWrite_LEmi1_Lcon(DMP_DomainAddressWrite_LEmi1_data.domain_address_new, Status::not_ok);
            }
        }
    }

    if ((DMP_ProgModeSwitch_LEmi1_data.state == 1) && (length == 1) && (address == 0x60)) {
        DMP_ProgModeSwitch_LEmi1_data.state = 0;
        Programming_Mode_Property programming_mode_property;
        programming_mode_property.fromData(std::cbegin(data), std::cend(data));
        if (programming_mode_property.prog_mode != DMP_ProgModeSwitch_LEmi1_data.mode) {
            assert(PC_Set_Value_req);
            programming_mode_property.prog_mode = DMP_ProgModeSwitch_LEmi1_data.mode;
            PC_Set_Value_req(1, 0x60, programming_mode_property.toData());
        }
        if (DMP_ProgModeSwitch_LEmi1_Lcon) {
            DMP_ProgModeSwitch_LEmi1_Lcon(DMP_ProgModeSwitch_LEmi1_data.mode, Status::ok);
        }
    }

    if (DMP_MemWrite_LEmi1_data.state == 1) {
        Memory_Number expected_length = DMP_MemWrite_LEmi1_data.DmpData.size() - DMP_MemWrite_LEmi1_data.offset;
        // reduce to 12
        if (expected_length > 12) {
            expected_length = 12;
        }
        if ((length == expected_length) && (address == DMP_MemWrite_LEmi1_data.DmpStartAddr + DMP_MemWrite_LEmi1_data.offset)) {
            Memory_Data expected_data;
            expected_data.assign(DMP_MemWrite_LEmi1_data.DmpData.cbegin() + DMP_MemWrite_LEmi1_data.offset, DMP_MemWrite_LEmi1_data.DmpData.cbegin() + DMP_MemWrite_LEmi1_data.offset + length);
            if (data == expected_data) {
                DMP_MemWrite_LEmi1_data.offset += length;
                if (DMP_MemWrite_LEmi1_data.offset >= DMP_MemWrite_LEmi1_data.DmpData.size()) {
                    if (DMP_MemWrite_LEmi1_Lcon) {
                        DMP_MemWrite_LEmi1_data.state = 0;
                        DMP_MemWrite_LEmi1_Lcon(DMP_MemWrite_LEmi1_data.verify, DMP_MemWrite_LEmi1_data.DmpStartAddr, DMP_MemWrite_LEmi1_data.DmpEndAddr, DMP_MemWrite_LEmi1_data.DmpData, Status::ok);
                    }
                } else {
                    assert(PC_Set_Value_req);
                    Memory_Number new_length = DMP_MemWrite_LEmi1_data.DmpData.size() - DMP_MemWrite_LEmi1_data.offset;
                    // reduce to 12
                    if (new_length > 12) {
                        new_length = 12;
                    }
                    Memory_Data new_data;
                    new_data.assign(DMP_MemWrite_LEmi1_data.DmpData.cbegin() + DMP_MemWrite_LEmi1_data.offset, DMP_MemWrite_LEmi1_data.DmpData.cbegin() + DMP_MemWrite_LEmi1_data.offset + length);
                    if (DMP_MemWrite_LEmi1_data.verify) {
                        PC_Set_Value_req(new_length, DMP_MemWrite_LEmi1_data.DmpStartAddr + DMP_MemWrite_LEmi1_data.offset, new_data);
                        DMP_MemWrite_LEmi1_data.state = 1;
                        if (PC_Get_Value_req) {
                            PC_Get_Value_req(new_length, DMP_MemWrite_LEmi1_data.DmpStartAddr + DMP_MemWrite_LEmi1_data.offset);
                        }
                    } else {
                        DMP_MemWrite_LEmi1_data.state = 2;
                        PC_Set_Value_req(new_length, DMP_MemWrite_LEmi1_data.DmpStartAddr + DMP_MemWrite_LEmi1_data.offset, new_data);
                        DMP_MemWrite_LEmi1_timeout_timer_restart(std::chrono::milliseconds(3000));
                    }
                }
            } else {
                if (DMP_MemWrite_LEmi1_Lcon) {
                    DMP_MemWrite_LEmi1_data.state = 0;
                    DMP_MemWrite_LEmi1_Lcon(DMP_MemWrite_LEmi1_data.verify, DMP_MemWrite_LEmi1_data.DmpStartAddr, DMP_MemWrite_LEmi1_data.DmpEndAddr, DMP_MemWrite_LEmi1_data.DmpData, Status::not_ok);
                }
            }
        }
    }

    if (DMP_MemVerify_LEmi1_data.state == 1) {
        Memory_Number expected_length = DMP_MemVerify_LEmi1_data.data.size() - DMP_MemVerify_LEmi1_data.offset;
        // reduce to 12
        if (expected_length > 12) {
            expected_length = 12;
        }
        if ((length == expected_length) && (address == DMP_MemVerify_LEmi1_data.deviceStartAddress + DMP_MemVerify_LEmi1_data.offset)) {
            Memory_Data expected_data;
            expected_data.assign(DMP_MemVerify_LEmi1_data.data.cbegin() + DMP_MemVerify_LEmi1_data.offset, DMP_MemVerify_LEmi1_data.data.cbegin() + DMP_MemVerify_LEmi1_data.offset + length);
            if (data == expected_data) {
                DMP_MemVerify_LEmi1_data.offset += length;
                if (DMP_MemVerify_LEmi1_data.offset >= DMP_MemVerify_LEmi1_data.data.size()) {
                    if (DMP_MemVerify_LEmi1_Lcon) {
                        DMP_MemVerify_LEmi1_data.state = 0;
                        DMP_MemVerify_LEmi1_Lcon(DMP_MemVerify_LEmi1_data.deviceStartAddress, DMP_MemVerify_LEmi1_data.deviceEndAddress, DMP_MemVerify_LEmi1_data.data, Status::ok);
                    }
                } else {
                    Memory_Number new_length = DMP_MemVerify_LEmi1_data.data.size() - DMP_MemVerify_LEmi1_data.offset;
                    // reduce to 12
                    if (new_length > 12) {
                        new_length = 12;
                    }
                    DMP_MemVerify_LEmi1_data.state = 1;
                    if (PC_Get_Value_req) {
                        PC_Get_Value_req(new_length, DMP_MemVerify_LEmi1_data.deviceStartAddress + DMP_MemVerify_LEmi1_data.offset);
                    }
                }
            } else {
                if (DMP_MemVerify_LEmi1_Lcon) {
                    DMP_MemVerify_LEmi1_data.state = 0;
                    DMP_MemVerify_LEmi1_Lcon(DMP_MemVerify_LEmi1_data.deviceStartAddress, DMP_MemVerify_LEmi1_data.deviceEndAddress, DMP_MemVerify_LEmi1_data.data, Status::not_ok);
                }
            }
        }
    }

    if (DMP_MemRead_LEmi1_data.state == 1) {
        Memory_Number expected_length = DMP_MemRead_LEmi1_data.data.size() - DMP_MemRead_LEmi1_data.offset;
        // reduce to 12
        if (expected_length > 12) {
            expected_length = 12;
        }
        if ((length == expected_length) && (address == DMP_MemRead_LEmi1_data.deviceStartAddress + DMP_MemRead_LEmi1_data.offset)) {
            std::copy(data.cbegin(), data.cend(), DMP_MemRead_LEmi1_data.data.begin() + DMP_MemRead_LEmi1_data.offset);
            DMP_MemRead_LEmi1_data.offset += length;
            if (DMP_MemRead_LEmi1_data.offset >= DMP_MemRead_LEmi1_data.data.size()) {
                if (DMP_MemRead_LEmi1_Lcon) {
                    DMP_MemRead_LEmi1_data.state = 0;
                    DMP_MemRead_LEmi1_Lcon(DMP_MemRead_LEmi1_data.deviceStartAddress, DMP_MemRead_LEmi1_data.deviceEndAddress, DMP_MemRead_LEmi1_data.data, Status::ok);
                }
            } else {
                Memory_Number new_length = DMP_MemRead_LEmi1_data.data.size() - DMP_MemRead_LEmi1_data.offset;
                // reduce to 12
                if (new_length > 12) {
                    new_length = 12;
                }
                DMP_MemRead_LEmi1_data.state = 1;
                if (PC_Get_Value_req) {
                    PC_Get_Value_req(new_length, DMP_MemRead_LEmi1_data.deviceStartAddress + DMP_MemRead_LEmi1_data.offset);
                }
            }
        }
    }
}

/* Application Layer */

void Device_Management::A_Connect_Lcon(const ASAP_Connected asap, const Status a_status)
{
    if ((DMP_Connect_RCo_data.state == 1) && (asap.address == DMP_Connect_RCo_data.IA_target)) {
        if (a_status == Status::ok) {
            DMP_Connect_RCo_data.state = 2;
            assert(A_DeviceDescriptor_Read_req);
            A_DeviceDescriptor_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 0);
        } else {
            DMP_Connect_RCo_data.state = 0;
            if (DMP_Connect_RCo_Lcon) {
                DMP_Connect_RCo_Lcon(DMP_Connect_RCo_data.IA_target, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_Disconnect_Lcon(const ASAP_Connected asap, const Status a_status)
{
    if ((DMP_Disconnect_RCo_data.state == 1) && (DMP_Disconnect_RCo_data.destination_address == asap.address)) {
        if (a_status == Status::ok) {
            DMP_Disconnect_RCo_data.state = 0;
            if (DMP_Disconnect_RCo_Lcon) {
                DMP_Disconnect_RCo_Lcon(asap.address, Status::ok);
            }
        } else {
            DMP_Disconnect_RCo_data.state = 0;
            if (DMP_Disconnect_RCo_Lcon) {
                DMP_Disconnect_RCo_Lcon(asap.address, Status::not_ok);
            }
        }
    }

    if (DMP_Restart_RCo_data.state == 2) {
        if (a_status == Status::ok) {
            DMP_Restart_RCo_data.state = 0;
            if (DMP_Restart_RCo_Acon) {
                DMP_Restart_RCo_Acon(DMP_Restart_RCo_data.individual_address, DMP_Restart_RCo_data.mpp_RestartType, DMP_Restart_RCo_data.mpp_EraseCode, DMP_Restart_RCo_data.mpp_ChannelNumber, DMP_Restart_RCo_data.mpp_ErrorCode, DMP_Restart_RCo_data.mpp_ProcessTime);
            }
        } else {
            DMP_Restart_RCo_data.state = 0;
            if (DMP_Restart_RCo_Lcon) {
                DMP_Restart_RCo_Lcon(DMP_Restart_RCo_data.individual_address, DMP_Restart_RCo_data.mpp_RestartType, DMP_Restart_RCo_data.mpp_EraseCode, DMP_Restart_RCo_data.mpp_ChannelNumber, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_Disconnect_ind(const ASAP_Connected asap)
{
    if (((DMP_Connect_RCo_data.state == 1) || (DMP_Connect_RCo_data.state == 2) || (DMP_Connect_RCo_data.state == 3)) && (asap.address == DMP_Connect_RCo_data.IA_target)) {
        DMP_Connect_RCo_data.state = 0;
        if (DMP_Connect_RCo_Lcon) {
            DMP_Connect_RCo_Lcon(DMP_Connect_RCo_data.IA_target, Status::not_ok);
        }
    }

    if ((DMP_Restart_RCo_data.state == 1) || (DMP_Restart_RCo_data.state == 2)) {
        DMP_Restart_RCo_data.state = 0;
        if (DMP_Restart_RCo_Acon) {
            DMP_Restart_RCo_Acon(DMP_Restart_RCo_data.individual_address, DMP_Restart_RCo_data.mpp_RestartType, DMP_Restart_RCo_data.mpp_EraseCode, DMP_Restart_RCo_data.mpp_ChannelNumber, DMP_Restart_RCo_data.mpp_ErrorCode, DMP_Restart_RCo_data.mpp_ProcessTime);
        }
    }

    //    for (auto & state: device.DMP_LoadStateMachineRead_Rco_Mem_states) {
    //        if (state.second == Device::DMP_LoadStateMachineRead_Rco_Mem_State::Running) {
    //            state.second = Device::DMP_LoadStateMachineRead_Rco_Mem_State::Idle;
    //            if (DMP_LoadStateMachineRead_RCo_Mem_Acon) {
    //                DMP_LoadStateMachineRead_RCo_Mem_Acon(asap.address, state.first, Status::not_ok);
    //            }
    //        }
    //    }
}

/* Services (System Broadcast) */

void Device_Management::A_DeviceDescriptor_InfoReport_Lcon(const Ack_Request /*ack_request*/, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Status a_status)
{
    if (DMP_DeviceDescriptor_InfoReport_data.state == 1) {
        if (a_status == Status::ok) {
            DMP_DeviceDescriptor_InfoReport_data.state = 0;
            if (DMP_DeviceDescriptor_InfoReport_Acon) {
                DMP_DeviceDescriptor_InfoReport_Acon(descriptor_type, device_descriptor);
            }
        } else {
            DMP_DeviceDescriptor_InfoReport_data.state = 0;
            if (DMP_DeviceDescriptor_InfoReport_Lcon) {
                DMP_DeviceDescriptor_InfoReport_Lcon(Status::not_ok);
            }
        }
    }
}

/* Services (Individual) */

void Device_Management::A_DeviceDescriptor_Read_Lcon(const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Status a_status)
{
    if ((DMP_Connect_RCo_data.state == 2) && (asap.address == DMP_Connect_RCo_data.IA_target) && (descriptor_type == 0)) {
        if (a_status == Status::ok) {
            DMP_Connect_RCo_data.state = 3;
            DMP_Connect_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_Connect_RCo_data.state = 0;
            if (DMP_Connect_RCo_Lcon) {
                DMP_Connect_RCo_Lcon(DMP_Connect_RCo_data.IA_target, Status::not_ok);
            }
        }
    }

    if ((DMP_Connect_RCl_data.state == 1) && (asap.address == DMP_Connect_RCl_data.IA_target) && (descriptor_type == 0)) {
        if (a_status == Status::ok) {
            DMP_Connect_RCl_data.state = 2;
            DMP_Connect_RCl_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_Connect_RCl_data.state = 0;
            if (DMP_Connect_RCl_Lcon) {
                DMP_Connect_RCl_Lcon(DMP_Connect_RCl_data.IA_target, Status::not_ok);
            }
        }
    }

    if ((DMP_Identify_R_data.state == 1) && (asap == DMP_Identify_R_data.asap) && (descriptor_type == 0)) {
        if (a_status == Status::ok) {
            DMP_Identify_R_data.state = 2;
            DMP_Identify_R_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_Identify_R_data.state = 0;
            if (DMP_Identify_R_Lcon) {
                DMP_Identify_R_Lcon(DMP_Identify_R_data.asap, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_DeviceDescriptor_Read_Acon(const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor)
{
    if ((DMP_Connect_RCo_data.state == 3) && (asap.address == DMP_Connect_RCo_data.IA_target) && (descriptor_type == 0)) {
        DMP_Connect_RCo_data.state = 0;
        if (DMP_Connect_RCo_Acon) {
            DMP_Connect_RCo_Acon(asap.address);
        }
    }

    if ((DMP_Connect_RCl_data.state == 2) && (asap.address == DMP_Connect_RCl_data.IA_target) && (descriptor_type == 0)) {
        DMP_Connect_RCl_data.state = 0;
        if (DMP_Connect_RCl_Acon) {
            DMP_Connect_RCl_Acon(asap.address);
        }
    }

    if ((DMP_Identify_R_data.state == 2) && (asap == DMP_Identify_R_data.asap) && (descriptor_type == 0)) {
        DMP_Identify_R_data.state = 3;
        DMP_Identify_R_data.device_descriptor_0.fromData(std::cbegin(device_descriptor), std::cend(device_descriptor));
        assert(A_PropertyValue_Read_req);
        A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 0x00, 0x48, 0x01, 0x01);
    }
}

void Device_Management::A_Restart_Acon(const Restart_Error_Code error_code, const Priority /*priority*/, const Restart_Process_Time process_time, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap)
{
    if ((DMP_Restart_RCl_data.state == 1) && (asap.address == DMP_Restart_RCl_data.individual_address) && (!asap.connected)) {
        DMP_Restart_RCl_data.state = 0;
        DMP_Restart_RCl_data.mpp_ErrorCode = error_code;
        DMP_Restart_RCl_data.mpp_ProcessTime = process_time;
        if (DMP_Restart_RCl_Acon) {
            DMP_Restart_RCl_Acon(DMP_Restart_RCl_data.individual_address, DMP_Restart_RCl_data.mpp_RestartType, DMP_Restart_RCl_data.mpp_EraseCode, DMP_Restart_RCl_data.mpp_ChannelNumber, error_code, process_time);
        }
    }

    if ((DMP_Restart_RCo_data.state == 1) && (asap.address == DMP_Restart_RCo_data.individual_address) && (asap.connected)) {
        DMP_Restart_RCo_data.state = 2;
        DMP_Restart_RCo_data.mpp_ErrorCode = error_code;
        DMP_Restart_RCo_data.mpp_ProcessTime = process_time;
        assert(A_Disconnect_req);
        A_Disconnect_req(Priority::low, asap);
    }
}

void Device_Management::A_PropertyValue_Read_Lcon(const Ack_Request /*ack_request*/, const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Status a_status)
{
    if ((DMP_Identify_R_data.state == 3) && (asap == DMP_Identify_R_data.asap) && (object_index == 0) && (property_id == 0x48) && (nr_of_elem == 0x01) && (start_index == 0x01)) {
        if (a_status == Status::ok) {
            DMP_Identify_R_data.state = 4;
            DMP_Identify_R_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_Identify_R_data.state = 0;
            if (DMP_Identify_R_Lcon) {
                DMP_Identify_R_Lcon(DMP_Identify_R_data.asap, Status::not_ok);
            }
        }
    }

    if ((DMP_Identify_RCo2_data.state == 1) && (asap.address == DMP_Identify_RCo2_data.server_IA) && (object_index == 0) && (property_id == Device_Object::PID_MANUFACTURER_ID) && (nr_of_elem == 1) && (start_index == 1)) {
        if (a_status == Status::ok) {
            DMP_Identify_RCo2_data.state = 2;
            DMP_Identify_RCo2_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_Identify_RCo2_data.state = 0;
            if (DMP_Identify_RCo2_Lcon) {
                DMP_Identify_RCo2_Lcon(DMP_Identify_RCo2_data.server_IA, Status::not_ok);
            }
        }
    } else if ((DMP_Identify_RCo2_data.state == 3) && (asap.address == DMP_Identify_RCo2_data.server_IA) && (object_index == 0) && (property_id == Device_Object::PID_HARDWARE_TYPE) && (nr_of_elem == 1) && (start_index == 1)) {
        if (a_status == Status::ok) {
            DMP_Identify_RCo2_data.state = 4;
            DMP_Identify_RCo2_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_Identify_RCo2_data.state = 0;
            if (DMP_Identify_RCo2_Lcon) {
                DMP_Identify_RCo2_Lcon(DMP_Identify_RCo2_data.server_IA, Status::not_ok);
            }
        }
    }

    if ((DMP_PeiTypeVerify_R_IO_data.state == 3) && (asap == DMP_PeiTypeVerify_R_IO_data.asap) && (object_index == 0) && (property_id == Device_Object::PID_PEI_TYPE) && (nr_of_elem == 1) && (start_index == 1)) {
        if (a_status == Status::ok) {
            DMP_PeiTypeVerify_R_IO_data.state = 4;
            DMP_PeiTypeVerify_R_IO_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_PeiTypeVerify_R_IO_data.state = 0;
            if (DMP_PeiTypeVerify_R_IO_Lcon) {
                DMP_PeiTypeVerify_R_IO_Lcon(DMP_PeiTypeVerify_R_IO_data.asap, Status::not_ok);
            }
        }
    }

    if ((DMP_PeiTypeRead_R_IO_data.state == 3) && (asap == DMP_PeiTypeRead_R_IO_data.asap) && (object_index == 0) && (property_id == Device_Object::PID_PEI_TYPE) && (nr_of_elem == 1) && (start_index == 1)) {
        if (a_status == Status::ok) {
            DMP_PeiTypeRead_R_IO_data.state = 4;
            DMP_PeiTypeRead_R_IO_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_PeiTypeRead_R_IO_data.state = 0;
            if (DMP_PeiTypeRead_R_IO_Lcon) {
                DMP_PeiTypeRead_R_IO_Lcon(DMP_PeiTypeRead_R_IO_data.asap, Status::not_ok);
            }
        }
    }

    if ((DMP_MemWrite_RCoV_data.state == 3) && (asap.address == DMP_MemWrite_RCoV_data.individual_address) && (object_index == 0) && (property_id == Device_Object::PID_DEVICE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
        if (a_status == Status::ok) {
            DMP_MemWrite_RCoV_data.state = 4;
            DMP_MemWrite_RCoV_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_MemWrite_RCoV_data.state = 0;
            if (DMP_MemWrite_RCoV_Lcon) {
                DMP_MemWrite_RCoV_Lcon(DMP_MemWrite_RCoV_data.individual_address, DMP_MemWrite_RCoV_data.verify, DMP_MemWrite_RCoV_data.device_control_unknown, DMP_MemWrite_RCoV_data.deviceStartAddress, DMP_MemWrite_RCoV_data.deviceEndAddress, DMP_MemWrite_RCoV_data.data, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_PropertyValue_Read_Acon(const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data)
{
    if ((DMP_Identify_R_data.state == 4) && (asap == DMP_Identify_R_data.asap) && (object_index == 0) && (property_id == 0x48) && (nr_of_elem == 0x01) && (start_index == 0x01)) {
        DMP_Identify_R_data.state = 0;
        DMP_Identify_R_data.mgt_descriptor_1 = data;
        if (DMP_Identify_R_Acon) {
            DMP_Identify_R_Acon(DMP_Identify_R_data.asap, DMP_Identify_R_data.device_descriptor_0, DMP_Identify_R_data.mgt_descriptor_1);
        }
    }

    if ((DMP_Identify_RCo2_data.state == 2) && (asap.address == DMP_Identify_RCo2_data.server_IA) && (object_index == 0) && (property_id == Device_Object::PID_MANUFACTURER_ID) && (nr_of_elem == 1) && (start_index == 1)) {
        DMP_Identify_RCo2_data.state = 3;
        std::vector<uint8_t>::const_iterator first = std::cbegin(data);
        std::vector<uint8_t>::const_iterator last = std::cbegin(data);
        assert(std::distance(first, last) == 2);
        DMP_Identify_RCo2_data.manufacturer_id = (*first++ << 8) | (*first++);
        assert(first == last);
        assert(A_PropertyValue_Read_req);
        A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 0, Device_Object::PID_HARDWARE_TYPE, 1, 1);
    } else if ((DMP_Identify_RCo2_data.state == 4) && (asap.address == DMP_Identify_RCo2_data.server_IA) && (object_index == 0) && (property_id == Device_Object::PID_HARDWARE_TYPE) && (nr_of_elem == 1) && (start_index == 1)) {
        DMP_Identify_RCo2_data.state = 0;
        assert(data.size() == 6);
        std::copy(std::cbegin(data), std::cend(data), std::begin(DMP_Identify_RCo2_data.hardware_type));
        if (DMP_Identify_RCo2_Acon) {
            DMP_Identify_RCo2_Acon(DMP_Identify_RCo2_data.server_IA, DMP_Identify_RCo2_data.manufacturer_id, DMP_Identify_RCo2_data.hardware_type);
        }
    }

    if ((DMP_PeiTypeVerify_R_IO_data.state == 4) && (asap == DMP_PeiTypeVerify_R_IO_data.asap) && (object_index == 0) && (property_id == Device_Object::PID_PEI_TYPE) && (nr_of_elem == 1) && (start_index == 1)) {
        DMP_PeiTypeVerify_R_IO_data.state = 0;
        assert(data.size() == 1);
        DMP_PeiTypeVerify_R_IO_data.pei_type = data[0];
        if (DMP_PeiTypeVerify_R_IO_Acon) {
            DMP_PeiTypeVerify_R_IO_Acon(asap, DMP_PeiTypeVerify_R_IO_data.pei_type);
        }
    }

    if ((DMP_PeiTypeRead_R_IO_data.state == 4) && (asap == DMP_PeiTypeRead_R_IO_data.asap) && (object_index == 0) && (property_id == Device_Object::PID_PEI_TYPE) && (nr_of_elem == 1) && (start_index == 1)) {
        DMP_PeiTypeRead_R_IO_data.state = 0;
        assert(data.size() == 1);
        DMP_PeiTypeRead_R_IO_data.pei_type = data[0];
        if (DMP_PeiTypeRead_R_IO_Acon) {
            DMP_PeiTypeRead_R_IO_Acon(asap, DMP_PeiTypeRead_R_IO_data.pei_type);
        }
    }

    if ((DMP_MemWrite_RCoV_data.state == 4) && (asap.address == DMP_MemWrite_RCoV_data.individual_address) && (object_index == 0) && (property_id == Device_Object::PID_DEVICE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
        assert(data.size() == 1);
        DMP_MemWrite_RCoV_data.device_control.attributes = data[0];
        if (DMP_MemWrite_RCoV_data.device_control.attributes[2] == 0) {
            DMP_MemWrite_RCoV_data.state = 5;
            assert(A_PropertyValue_Write_req);
            Property_Value new_data{data};
            new_data[0] |= (1 << 2);
            A_PropertyValue_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 0, Device_Object::PID_DEVICE_CONTROL, 1, 1, new_data);
        } else {
            if (DMP_MemWrite_RCoV_data.offset >= DMP_MemWrite_RCoV_data.data.size()) {
                DMP_MemWrite_RCoV_data.state = 0;
                if (DMP_MemWrite_RCoV_Acon) {
                    DMP_MemWrite_RCoV_Acon(DMP_MemWrite_RCoV_data.individual_address, DMP_MemWrite_RCoV_data.verify, DMP_MemWrite_RCoV_data.device_control_unknown, DMP_MemWrite_RCoV_data.deviceStartAddress, DMP_MemWrite_RCoV_data.deviceEndAddress, DMP_MemWrite_RCoV_data.data);
                }
            } else {
                DMP_MemWrite_RCoV_data.state = 7;
                assert(A_Memory_Write_req);
                Memory_Number number = DMP_MemWrite_RCoV_data.data.size() - DMP_MemWrite_RCoV_data.offset;
                // reduce to connection limit
                if (number > 12) { // @todo size is medium dependent
                    number = 12;
                }
                Memory_Data data;
                data.assign(DMP_MemWrite_RCoV_data.data.cbegin() + DMP_MemWrite_RCoV_data.offset, DMP_MemWrite_RCoV_data.data.cbegin() + DMP_MemWrite_RCoV_data.offset + number);
                A_Memory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, number, DMP_MemWrite_RCoV_data.deviceStartAddress + DMP_MemWrite_RCoV_data.offset, data);
            }
        }
    } else if ((DMP_MemWrite_RCoV_data.state == 6) && (asap.address == DMP_MemWrite_RCoV_data.individual_address) && (object_index == 0) && (property_id == Device_Object::PID_DEVICE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
        if (DMP_MemWrite_RCoV_data.offset >= DMP_MemWrite_RCoV_data.data.size()) {
            DMP_MemWrite_RCoV_data.state = 0;
            if (DMP_MemWrite_RCoV_Acon) {
                DMP_MemWrite_RCoV_Acon(DMP_MemWrite_RCoV_data.individual_address, DMP_MemWrite_RCoV_data.verify, DMP_MemWrite_RCoV_data.device_control_unknown, DMP_MemWrite_RCoV_data.deviceStartAddress, DMP_MemWrite_RCoV_data.deviceEndAddress, DMP_MemWrite_RCoV_data.data);
            }
        } else {
            DMP_MemWrite_RCoV_data.state = 7;
            assert(A_Memory_Write_req);
            Memory_Number number = DMP_MemWrite_RCoV_data.data.size() - DMP_MemWrite_RCoV_data.offset;
            // reduce to connection limit
            if (number > 12) { // @todo size is medium dependent
                number = 12;
            }
            Memory_Data data;
            data.assign(DMP_MemWrite_RCoV_data.data.cbegin() + DMP_MemWrite_RCoV_data.offset, DMP_MemWrite_RCoV_data.data.cbegin() + DMP_MemWrite_RCoV_data.offset + number);
            A_Memory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, number, DMP_MemWrite_RCoV_data.deviceStartAddress + DMP_MemWrite_RCoV_data.offset, data);
        }
    }
}

void Device_Management::A_PropertyValue_Write_Lcon(const Ack_Request /*ack_request*/, const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data, const Status a_status)
{
    if ((DMP_MemWrite_RCoV_data.state == 5) && (asap.address == DMP_MemWrite_RCoV_data.individual_address) && (object_index == 0) && (property_id == Device_Object::PID_DEVICE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
        if (a_status == Status::ok) {
            DMP_MemWrite_RCoV_data.state = 6;
            DMP_MemWrite_RCoV_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_MemWrite_RCoV_data.state = 0;
            if (DMP_MemWrite_RCoV_Lcon) {
                DMP_MemWrite_RCoV_Lcon(DMP_MemWrite_RCoV_data.individual_address, DMP_MemWrite_RCoV_data.verify, DMP_MemWrite_RCoV_data.device_control_unknown, DMP_MemWrite_RCoV_data.deviceStartAddress, DMP_MemWrite_RCoV_data.deviceEndAddress, DMP_MemWrite_RCoV_data.data, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_PropertyDescription_Read_Lcon(const Ack_Request /*ack_request*/, const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const Status a_status)
{
    if ((DMP_PeiTypeVerify_R_IO_data.state == 1) && (asap == DMP_PeiTypeVerify_R_IO_data.asap) && (object_index == 0) && (property_id == Device_Object::PID_PEI_TYPE) && (property_index == 0)) {
        if (a_status == Status::ok) {
            DMP_PeiTypeVerify_R_IO_data.state = 2;
            DMP_PeiTypeVerify_R_IO_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_PeiTypeVerify_R_IO_data.state = 0;
            if (DMP_PeiTypeVerify_R_IO_Lcon) {
                DMP_PeiTypeVerify_R_IO_Lcon(DMP_PeiTypeVerify_R_IO_data.asap, Status::not_ok);
            }
        }
    }

    if ((DMP_PeiTypeRead_R_IO_data.state == 1) && (asap == DMP_PeiTypeRead_R_IO_data.asap) && (object_index == 0) && (property_id == Device_Object::PID_PEI_TYPE) && (property_index == 0)) {
        if (a_status == Status::ok) {
            DMP_PeiTypeRead_R_IO_data.state = 2;
            DMP_PeiTypeRead_R_IO_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_PeiTypeRead_R_IO_data.state = 0;
            if (DMP_PeiTypeRead_R_IO_Lcon) {
                DMP_PeiTypeRead_R_IO_Lcon(DMP_PeiTypeRead_R_IO_data.asap, Status::not_ok);
            }
        }
    }

    if ((DMP_MemWrite_RCoV_data.state == 1) && (asap.address == DMP_MemWrite_RCoV_data.individual_address) && (object_index == 0) && (property_id == Device_Object::PID_DEVICE_CONTROL) && (property_index == 0)) {
        if (a_status == Status::ok) {
            DMP_MemWrite_RCoV_data.state = 2;
            DMP_MemWrite_RCoV_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_MemWrite_RCoV_data.state = 0;
            if (DMP_MemWrite_RCoV_Lcon) {
                DMP_MemWrite_RCoV_Lcon(DMP_MemWrite_RCoV_data.individual_address, DMP_MemWrite_RCoV_data.verify, DMP_MemWrite_RCoV_data.device_control_unknown, DMP_MemWrite_RCoV_data.deviceStartAddress, DMP_MemWrite_RCoV_data.deviceEndAddress, DMP_MemWrite_RCoV_data.data, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_PropertyDescription_Read_Acon(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const bool write_enable, const Property_Type type, const Max_Nr_Of_Elem max_nr_of_elem, const Access access)
{
    if ((DMP_PeiTypeVerify_R_IO_data.state == 2) && (asap == DMP_PeiTypeVerify_R_IO_data.asap) && (object_index == 0) && (property_id == Device_Object::PID_PEI_TYPE) && (property_index == 0) && (type == PDT_UNSIGNED_CHAR)) {
        DMP_PeiTypeVerify_R_IO_data.state = 3;
        assert(A_PropertyValue_Read_req);
        A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, OT_DEVICE, Device_Object::PID_PEI_TYPE, 0x01, 0x01);
    }

    if ((DMP_PeiTypeRead_R_IO_data.state == 2) && (asap == DMP_PeiTypeRead_R_IO_data.asap) && (object_index == 0) && (property_id == Device_Object::PID_PEI_TYPE) && (property_index == 0) && (type == PDT_UNSIGNED_CHAR)) {
        DMP_PeiTypeRead_R_IO_data.state = 3;
        assert(A_PropertyValue_Read_req);
        A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, OT_DEVICE, Device_Object::PID_PEI_TYPE, 0x01, 0x01);
    }

    if ((DMP_MemWrite_RCoV_data.state == 2) && (asap.address == DMP_MemWrite_RCoV_data.individual_address) && (object_index == 0) && (property_id == Device_Object::PID_DEVICE_CONTROL) && (property_index == 0)) {
        DMP_MemWrite_RCoV_data.state = 3;
        assert(A_PropertyValue_Read_req);
        A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, OT_DEVICE, Device_Object::PID_DEVICE_CONTROL, 0x01, 0x01);
    }
}

/* Services (Connected) */

void Device_Management::A_ADC_Read_Lcon(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count, const Status a_status)
{
    if ((DMP_PeiTypeVerify_RCo_ADC_data.state == 1) && (asap.address == DMP_PeiTypeVerify_RCo_ADC_data.individual_address) && (asap.connected) && (channel_nr == 4) && (read_count == 1)) {
        if (a_status == Status::ok) {
            DMP_PeiTypeVerify_RCo_ADC_data.state = 2;
            DMP_PeiTypeVerify_RCo_ADC_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_PeiTypeVerify_RCo_ADC_data.state = 0;
            if (DMP_PeiTypeVerify_RCo_ADC_Lcon) {
                DMP_PeiTypeVerify_RCo_ADC_Lcon(DMP_PeiTypeVerify_RCo_ADC_data.individual_address, Status::not_ok);
            }
        }
    }

    if ((DMP_PeiTypeRead_RCo_ADC_data.state == 1) && (asap.address == DMP_PeiTypeRead_RCo_ADC_data.individual_address) && (asap.connected) && (channel_nr == 4) && (read_count == 1)) {
        if (a_status == Status::ok) {
            DMP_PeiTypeRead_RCo_ADC_data.state = 2;
            DMP_PeiTypeRead_RCo_ADC_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_PeiTypeRead_RCo_ADC_data.state = 0;
            if (DMP_PeiTypeRead_RCo_ADC_Lcon) {
                DMP_PeiTypeRead_RCo_ADC_Lcon(DMP_PeiTypeRead_RCo_ADC_data.individual_address, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_ADC_Read_Acon(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count, const ADC_Sum sum)
{
    if ((DMP_PeiTypeVerify_RCo_ADC_data.state == 2) && (asap.address == DMP_PeiTypeVerify_RCo_ADC_data.individual_address) && (asap.connected) && (channel_nr == 4) && (read_count == 1)) {
        DMP_PeiTypeVerify_RCo_ADC_data.state = 0;
        DMP_PeiTypeVerify_RCo_ADC_data.adc_value = sum / read_count;
        DMP_PeiTypeVerify_RCo_ADC_data.pei_type = (10 * DMP_PeiTypeVerify_RCo_ADC_data.adc_value + 60) / 128;
        if (DMP_PeiTypeVerify_RCo_ADC_Acon) {
            DMP_PeiTypeVerify_RCo_ADC_Acon(DMP_PeiTypeVerify_RCo_ADC_data.individual_address, DMP_PeiTypeVerify_RCo_ADC_data.pei_type);
        }
    }

    if ((DMP_PeiTypeRead_RCo_ADC_data.state == 2) && (asap.address == DMP_PeiTypeRead_RCo_ADC_data.individual_address) && (asap.connected) && (channel_nr == 4) && (read_count == 1)) {
        DMP_PeiTypeRead_RCo_ADC_data.state = 0;
        DMP_PeiTypeRead_RCo_ADC_data.adc_value = sum / read_count;
        DMP_PeiTypeRead_RCo_ADC_data.pei_type = (10 * DMP_PeiTypeRead_RCo_ADC_data.adc_value + 60) / 128;
        if (DMP_PeiTypeRead_RCo_ADC_Acon) {
            DMP_PeiTypeRead_RCo_ADC_Acon(DMP_PeiTypeRead_RCo_ADC_data.individual_address, DMP_PeiTypeRead_RCo_ADC_data.pei_type);
        }
    }
}

void Device_Management::A_Memory_Read_Lcon(const Ack_Request /*ack_request*/, const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Status a_status)
{
    if ((DMP_ProgModeSwitch_RCo_data.state == 1) && (asap.address == DMP_ProgModeSwitch_RCo_data.individual_address) && (asap.connected) && (number == 1) && (memory_address == 0x60)) {
        if (a_status == Status::ok) {
            DMP_ProgModeSwitch_RCo_data.state = 2;
            DMP_ProgModeSwitch_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_ProgModeSwitch_RCo_data.state = 0;
            if (DMP_ProgModeSwitch_RCo_Lcon) {
                DMP_ProgModeSwitch_RCo_Lcon(DMP_ProgModeSwitch_RCo_data.individual_address, DMP_ProgModeSwitch_RCo_data.mode, Status::not_ok);
            }
        }
    }

    if ((DMP_MemWrite_RCo_data.state == 2) && (asap.address == DMP_MemWrite_RCo_data.individual_address) && (memory_address == DMP_MemWrite_RCo_data.deviceStartAddress + DMP_MemWrite_RCo_data.offset)) {
        if (a_status == Status::ok) {
            DMP_MemWrite_RCo_data.state = 3;
            DMP_MemWrite_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_MemWrite_RCo_data.state = 0;
            if (DMP_MemWrite_RCo_Lcon) {
                DMP_MemWrite_RCo_Lcon(DMP_MemWrite_RCo_data.individual_address, DMP_MemWrite_RCo_data.verify, DMP_MemWrite_RCo_data.deviceStartAddress, DMP_MemWrite_RCo_data.deviceEndAddress, DMP_MemWrite_RCo_data.data, Status::not_ok);
            }
        }
    }

    if ((DMP_MemVerify_RCo_data.state == 1) && (asap.address == DMP_MemVerify_RCo_data.individual_address) && (memory_address == DMP_MemVerify_RCo_data.deviceStartAddress + DMP_MemVerify_RCo_data.offset)) {
        if (a_status == Status::ok) {
            DMP_MemVerify_RCo_data.state = 2;
            DMP_MemVerify_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_MemVerify_RCo_data.state = 0;
            if (DMP_MemVerify_RCo_Lcon) {
                DMP_MemVerify_RCo_Lcon(DMP_MemVerify_RCo_data.individual_address, DMP_MemVerify_RCo_data.deviceStartAddress, DMP_MemVerify_RCo_data.deviceEndAddress, DMP_MemVerify_RCo_data.data, Status::not_ok);
            }
        }
    }

    if ((DMP_MemRead_RCo_data.state == 1) && (asap.address == DMP_MemRead_RCo_data.individual_address) && (memory_address == DMP_MemRead_RCo_data.deviceStartAddress + DMP_MemRead_RCo_data.offset)) {
        if (a_status == Status::ok) {
            DMP_MemRead_RCo_data.state = 2;
            DMP_MemRead_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_MemRead_RCo_data.state = 0;
            if (DMP_MemRead_RCo_Lcon) {
                DMP_MemRead_RCo_Lcon(DMP_MemRead_RCo_data.individual_address, DMP_MemRead_RCo_data.deviceStartAddress, DMP_MemRead_RCo_data.deviceEndAddress, DMP_MemRead_RCo_data.data, Status::not_ok);
            }
        }
    }

    if ((DMP_LoadStateMachineRead_RCo_Mem_data.state == 1) && (asap.address == DMP_LoadStateMachineRead_RCo_Mem_data.destination_address) && (memory_address == DMP_LoadStateMachineRead_RCo_Mem_data.memory_address)) {
        if (a_status == Status::ok) {
            DMP_LoadStateMachineRead_RCo_Mem_data.state = 2;
            DMP_LoadStateMachineRead_RCo_Mem_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_LoadStateMachineRead_RCo_Mem_data.state = 0;
            if (DMP_LoadStateMachineRead_RCo_Mem_Lcon) {
                DMP_LoadStateMachineRead_RCo_Mem_Lcon(asap.address, DMP_LoadStateMachineRead_RCo_Mem_data.state_machine_type, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_Memory_Read_Acon(const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data)
{
    if ((DMP_ProgModeSwitch_RCo_data.state == 2) && (asap.address == DMP_ProgModeSwitch_RCo_data.individual_address) && (asap.connected) && (number == 1) && (memory_address == 0x60)) {
        Programming_Mode_Property programming_mode_property;
        programming_mode_property.fromData(std::cbegin(data), std::cend(data));
        if (programming_mode_property.prog_mode != DMP_ProgModeSwitch_RCo_data.mode) {
            DMP_ProgModeSwitch_RCo_data.state = 3;
            assert(A_Memory_Write_req);
            programming_mode_property.prog_mode = DMP_ProgModeSwitch_RCo_data.mode;
            A_Memory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 1, 0x60, programming_mode_property.toData());
        } else {
            DMP_ProgModeSwitch_RCo_data.state = 0;
            if (DMP_ProgModeSwitch_RCo_Acon) {
                DMP_ProgModeSwitch_RCo_Acon(DMP_ProgModeSwitch_RCo_data.individual_address, DMP_ProgModeSwitch_RCo_data.mode);
            }
        }
    }

    if ((DMP_MemWrite_RCo_data.state == 3) && (asap.address == DMP_MemWrite_RCo_data.individual_address) && (memory_address == DMP_MemWrite_RCo_data.deviceStartAddress + DMP_MemWrite_RCo_data.offset)) {
        Memory_Data expected_data;
        expected_data.assign(DMP_MemWrite_RCo_data.data.cbegin() + DMP_MemWrite_RCo_data.offset, DMP_MemWrite_RCo_data.data.cbegin() + DMP_MemWrite_RCo_data.offset + number);
        if (expected_data != data) {
            DMP_MemWrite_RCo_data.state = 0;
            if (DMP_MemWrite_RCo_Lcon) {
                DMP_MemWrite_RCo_Lcon(DMP_MemWrite_RCo_data.individual_address, DMP_MemWrite_RCo_data.verify, DMP_MemWrite_RCo_data.deviceStartAddress, DMP_MemWrite_RCo_data.deviceEndAddress, DMP_MemWrite_RCo_data.data, Status::not_ok);
            }
        } else {
            DMP_MemWrite_RCo_data.offset += number;
            if (DMP_MemWrite_RCo_data.offset >= data.size()) {
                DMP_MemWrite_RCo_data.state = 0;
                if (DMP_MemWrite_RCo_Acon) {
                    DMP_MemWrite_RCo_Acon(DMP_MemWrite_RCo_data.individual_address, DMP_MemWrite_RCo_data.verify, DMP_MemWrite_RCo_data.deviceStartAddress, DMP_MemWrite_RCo_data.deviceEndAddress, DMP_MemWrite_RCo_data.data);
                }
            } else {
                DMP_MemWrite_RCo_data.state = 1;
                assert(A_Memory_Write_req);
                Memory_Number next_number = DMP_MemWrite_RCo_data.data.size() - DMP_MemWrite_RCo_data.offset;
                // reduce to connection limit
                if (next_number > 12) { // @todo size is medium dependent
                    next_number = 12;
                }
                Memory_Data next_data;
                next_data.assign(DMP_MemWrite_RCo_data.data.cbegin() + DMP_MemWrite_RCo_data.offset, DMP_MemWrite_RCo_data.data.cbegin() + DMP_MemWrite_RCo_data.offset + next_number);
                A_Memory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, next_number, DMP_MemWrite_RCo_data.deviceStartAddress + DMP_MemWrite_RCo_data.offset, data);
            }
        }
    }

    if ((DMP_MemVerify_RCo_data.state == 2) && (asap.address == DMP_MemVerify_RCo_data.individual_address) && (memory_address == DMP_MemVerify_RCo_data.deviceStartAddress + DMP_MemVerify_RCo_data.offset)) {
        Memory_Data expected_data;
        expected_data.assign(DMP_MemVerify_RCo_data.data.cbegin() + DMP_MemVerify_RCo_data.offset, DMP_MemVerify_RCo_data.data.cbegin() + DMP_MemVerify_RCo_data.offset + number);
        if (expected_data != data) {
            DMP_MemVerify_RCo_data.state = 0;
            if (DMP_MemVerify_RCo_Lcon) {
                DMP_MemVerify_RCo_Lcon(DMP_MemVerify_RCo_data.individual_address, DMP_MemVerify_RCo_data.deviceStartAddress, DMP_MemVerify_RCo_data.deviceEndAddress, DMP_MemVerify_RCo_data.data, Status::not_ok);
            }
        } else {
            DMP_MemVerify_RCo_data.offset += number;
            if (DMP_MemVerify_RCo_data.offset >= data.size()) {
                DMP_MemVerify_RCo_data.state = 0;
                if (DMP_MemVerify_RCo_Acon) {
                    DMP_MemVerify_RCo_Acon(DMP_MemVerify_RCo_data.individual_address, DMP_MemVerify_RCo_data.deviceStartAddress, DMP_MemVerify_RCo_data.deviceEndAddress, DMP_MemVerify_RCo_data.data);
                }
            } else {
                DMP_MemVerify_RCo_data.state = 1;
                assert(A_Memory_Read_req);
                Memory_Number next_number = DMP_MemVerify_RCo_data.data.size() - DMP_MemVerify_RCo_data.offset;
                // reduce to connection limit
                if (next_number > 12) { // @todo size is medium dependent
                    next_number = 12;
                }
                A_Memory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, next_number, DMP_MemVerify_RCo_data.deviceStartAddress + DMP_MemVerify_RCo_data.offset);
            }
        }
    }

    if ((DMP_MemRead_RCo_data.state == 2) && (asap.address == DMP_MemRead_RCo_data.individual_address) && (memory_address == DMP_MemRead_RCo_data.deviceStartAddress + DMP_MemRead_RCo_data.offset)) {
        Memory_Number expected_number = DMP_MemRead_RCo_data.data.size() - DMP_MemRead_RCo_data.offset;
        // reduce to connection limit
        if (expected_number > 12) { // @todo size is medium dependent
            expected_number = 12;
        }
        if ((number == expected_number) && (memory_address == DMP_MemRead_RCo_data.deviceStartAddress + DMP_MemRead_RCo_data.offset)) {
            std::copy(data.cbegin(), data.cend(), DMP_MemRead_RCo_data.data.begin() + DMP_MemRead_RCo_data.offset);
            DMP_MemRead_RCo_data.offset += number;
            if (DMP_MemRead_RCo_data.offset >= DMP_MemRead_RCo_data.data.size()) {
                if (DMP_MemRead_RCo_Acon) {
                    DMP_MemRead_RCo_data.state = 0;
                    DMP_MemRead_RCo_Acon(DMP_MemRead_RCo_data.individual_address, DMP_MemRead_RCo_data.deviceStartAddress, DMP_MemRead_RCo_data.deviceEndAddress, DMP_MemRead_RCo_data.data);
                }
            } else {
                DMP_MemRead_RCo_data.state = 1;
                assert(A_Memory_Read_req);
                Memory_Number next_number = DMP_MemRead_RCo_data.data.size() - DMP_MemRead_RCo_data.offset;
                // reduce to connection limit
                if (next_number > 12) { // @todo size is medium dependent
                    next_number = 12;
                }
                A_Memory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, next_number, DMP_MemRead_RCo_data.deviceStartAddress + DMP_MemRead_RCo_data.offset);
            }
        }
    }

    if ((DMP_LoadStateMachineRead_RCo_Mem_data.state == 2) && (asap.address == DMP_LoadStateMachineRead_RCo_Mem_data.destination_address) && (memory_address == DMP_LoadStateMachineRead_RCo_Mem_data.memory_address)) {
        DMP_LoadStateMachineRead_RCo_Mem_data.state = 0;
        DMP_LoadStateMachineRead_RCo_Mem_data.loadstate = data;
        if (DMP_LoadStateMachineRead_RCo_Mem_Acon) {
            DMP_LoadStateMachineRead_RCo_Mem_Acon(asap.address, DMP_LoadStateMachineRead_RCo_Mem_data.state_machine_type, data);
        }
    }
}

void Device_Management::A_Memory_Write_Lcon(const Ack_Request /*ack_request*/, const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data, const Status a_status)
{
    if ((DMP_ProgModeSwitch_RCo_data.state == 3) && (asap.address == DMP_ProgModeSwitch_RCo_data.individual_address) && (asap.connected) && (number == 1) && (memory_address == 0x60)) {
        Programming_Mode_Property programming_mode_property;
        programming_mode_property.fromData(std::cbegin(data), std::cend(data));
        if (programming_mode_property.prog_mode == DMP_ProgModeSwitch_RCo_data.mode) {
            if (a_status == Status::ok) {
                DMP_ProgModeSwitch_RCo_data.state = 4;
                DMP_ProgModeSwitch_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
            } else {
                DMP_ProgModeSwitch_RCo_data.state = 0;
                if (DMP_ProgModeSwitch_RCo_Lcon) {
                    DMP_ProgModeSwitch_RCo_Lcon(DMP_ProgModeSwitch_RCo_data.individual_address, DMP_ProgModeSwitch_RCo_data.mode, Status::not_ok);
                }
            }
        }
    }

    if ((DMP_MemWrite_RCo_data.state == 1) && (asap.address == DMP_MemWrite_RCo_data.individual_address) && (asap.connected)) {
        if (a_status == Status::ok) {
            if (DMP_MemWrite_RCo_data.verify) {
                DMP_MemWrite_RCo_data.state = 2;
                assert(A_Memory_Read_req);
                A_Memory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, number, memory_address);
            } else {
                DMP_MemWrite_RCo_data.offset += number;
                if (DMP_MemWrite_RCo_data.offset >= data.size()) {
                    DMP_MemWrite_RCo_data.state = 0;
                    if (DMP_MemWrite_RCo_Acon) {
                        DMP_MemWrite_RCo_Acon(DMP_MemWrite_RCo_data.individual_address, DMP_MemWrite_RCo_data.verify, DMP_MemWrite_RCo_data.deviceStartAddress, DMP_MemWrite_RCo_data.deviceEndAddress, DMP_MemWrite_RCo_data.data);
                    }
                } else {
                    DMP_MemWrite_RCo_data.state = 4;
                    DMP_MemWrite_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
                }
            }
        } else {
            DMP_MemWrite_RCo_data.state = 0;
            if (DMP_MemWrite_RCo_Lcon) {
                DMP_MemWrite_RCo_Lcon(DMP_MemWrite_RCo_data.individual_address, DMP_MemWrite_RCo_data.verify, DMP_MemWrite_RCo_data.deviceStartAddress, DMP_MemWrite_RCo_data.deviceEndAddress, DMP_MemWrite_RCo_data.data, Status::not_ok);
            }
        }
    }

    if ((DMP_MemWrite_RCoV_data.state == 7) && (asap.address == DMP_MemWrite_RCoV_data.individual_address) && (asap.connected)) {
        if (a_status == Status::ok) {
            DMP_MemWrite_RCoV_data.state = 8;
            DMP_MemWrite_RCoV_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_MemWrite_RCoV_data.state = 0;
            if (DMP_MemWrite_RCoV_Lcon) {
                DMP_MemWrite_RCoV_Lcon(DMP_MemWrite_RCoV_data.individual_address, DMP_MemWrite_RCoV_data.verify, DMP_MemWrite_RCoV_data.device_control_unknown, DMP_MemWrite_RCoV_data.deviceStartAddress, DMP_MemWrite_RCoV_data.deviceEndAddress, DMP_MemWrite_RCoV_data.data, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_Memory_Write_Acon(const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data)
{
    if ((DMP_ProgModeSwitch_RCo_data.state == 4) && (asap.address == DMP_ProgModeSwitch_RCo_data.individual_address) && (asap.connected) && (number == 1) && (memory_address == 0x60)) {
        Programming_Mode_Property programming_mode_property;
        programming_mode_property.fromData(std::cbegin(data), std::cend(data));
        if (programming_mode_property.prog_mode == DMP_ProgModeSwitch_RCo_data.mode) {
            DMP_ProgModeSwitch_RCo_data.state = 0;
            if (DMP_ProgModeSwitch_RCo_Acon) {
                DMP_ProgModeSwitch_RCo_Acon(DMP_ProgModeSwitch_RCo_data.individual_address, DMP_ProgModeSwitch_RCo_data.mode);
            }
        } else {
            DMP_ProgModeSwitch_RCo_data.state = 0;
            if (DMP_ProgModeSwitch_RCo_Lcon) {
                DMP_ProgModeSwitch_RCo_Lcon(DMP_ProgModeSwitch_RCo_data.individual_address, DMP_ProgModeSwitch_RCo_data.mode, Status::not_ok);
            }
        }
    }

    if ((DMP_MemWrite_RCoV_data.state == 8) && (asap.address == DMP_MemWrite_RCoV_data.individual_address) && (asap.connected)) {
        DMP_MemWrite_RCoV_data.offset += number;
        if (DMP_MemWrite_RCoV_data.offset >= data.size()) {
            DMP_MemWrite_RCoV_data.state = 0;
            if (DMP_MemWrite_RCoV_Acon) {
                DMP_MemWrite_RCoV_Acon(DMP_MemWrite_RCoV_data.individual_address, DMP_MemWrite_RCoV_data.verify, DMP_MemWrite_RCoV_data.device_control_unknown, DMP_MemWrite_RCoV_data.deviceStartAddress, DMP_MemWrite_RCoV_data.deviceEndAddress, DMP_MemWrite_RCoV_data.data);
            }
        } else {
            DMP_MemWrite_RCoV_data.state = 7;
            assert(A_Memory_Write_req);
            Memory_Number next_number = DMP_MemWrite_RCoV_data.data.size() - DMP_MemWrite_RCoV_data.offset;
            // reduce to connection limit
            if (next_number > 12) { // @todo size is medium dependent
                next_number = 12;
            }
            Memory_Data next_data;
            next_data.assign(DMP_MemWrite_RCoV_data.data.cbegin() + DMP_MemWrite_RCoV_data.offset, DMP_MemWrite_RCoV_data.data.cbegin() + DMP_MemWrite_RCoV_data.offset + next_number);
            A_Memory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, next_number, DMP_MemWrite_RCoV_data.deviceStartAddress + DMP_MemWrite_RCoV_data.offset, next_data);
        }
    }
}

void Device_Management::A_UserMemory_Read_Lcon(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Status a_status)
{
}

void Device_Management::A_UserMemory_Read_Acon(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data)
{
}

void Device_Management::A_UserMemory_Write_Lcon(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data, const Status a_status)
{
}

void Device_Management::A_UserMemory_Write_Acon(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data)
{
}

void Device_Management::A_Authorize_Request_Acon(const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Level level)
{
    if ((DMP_Authorize_RCo_data.state == 1) && (asap.address == DMP_Authorize_RCo_data.individual_address) && (asap.connected)) {
        DMP_Authorize_RCo_data.state = 0;
        DMP_Authorize_RCo_data.level = level;
        if (DMP_Authorize_RCo_Acon) {
            DMP_Authorize_RCo_Acon(DMP_Authorize_RCo_data.individual_address, DMP_Authorize_RCo_data.key, level);
        }
    }

    if ((DMP_Authorize2_RCo_data.state == 1) && (asap.address == DMP_Authorize2_RCo_data.individual_address) && (asap.connected)) {
        DMP_Authorize2_RCo_data.state = 2;
        DMP_Authorize2_RCo_data.free_level = level;
        A_Authorize_Request_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, DMP_Authorize2_RCo_data.client_key);
        DMP_Authorize2_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
    } else if ((DMP_Authorize2_RCo_data.state == 2) && (asap.address == DMP_Authorize2_RCo_data.individual_address) && (asap.connected)) {
        DMP_Authorize2_RCo_data.client_level = level;
        if (DMP_Authorize2_RCo_data.client_level > DMP_Authorize2_RCo_data.free_level) {
            DMP_Authorize2_RCo_data.state = 3;
            A_Authorize_Request_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, {0xFF, 0xFF, 0xFF, 0xFF});
            DMP_Authorize2_RCo_timeout_timer_restart(std::chrono::milliseconds(3000));
        } else {
            DMP_Authorize2_RCo_data.state = 0;
            if (DMP_Authorize2_RCo_Acon) {
                DMP_Authorize2_RCo_Acon(DMP_Authorize2_RCo_data.individual_address, DMP_Authorize2_RCo_data.free_level, DMP_Authorize2_RCo_data.client_key, DMP_Authorize2_RCo_data.client_level);
            }
        }
    } else if ((DMP_Authorize2_RCo_data.state == 3) && (asap.address == DMP_Authorize2_RCo_data.individual_address) && (asap.connected)) {
        DMP_Authorize2_RCo_data.state = 0;
        if (DMP_Authorize2_RCo_Acon) {
            DMP_Authorize2_RCo_Acon(DMP_Authorize2_RCo_data.individual_address, DMP_Authorize2_RCo_data.free_level, DMP_Authorize2_RCo_data.client_key, DMP_Authorize2_RCo_data.client_level);
        }
    }
}

void Device_Management::A_Key_Write_Acon(const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Level level)
{
    if ((DMP_SetKey_RCo_data.state == 1) && (asap.address == DMP_SetKey_RCo_data.individual_address) && (asap.connected)) {
        DMP_SetKey_RCo_data.state = 0;
        if (DMP_SetKey_RCo_data.level == level) {
            if (DMP_SetKey_RCo_Acon) {
                DMP_SetKey_RCo_Acon(asap.address, DMP_SetKey_RCo_data.key, DMP_SetKey_RCo_data.level);
            }
        } else {
            if (DMP_SetKey_RCo_Lcon) {
                DMP_SetKey_RCo_Lcon(asap.address, DMP_SetKey_RCo_data.key, DMP_SetKey_RCo_data.level, Status::not_ok);
            }
        }
    }
}

void Device_Management::A_Read_Routing_Table_Acon(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)
{
}

void Device_Management::A_Write_Routing_Table_Acon(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)
{
}

void Device_Management::A_Read_Router_Memory_Acon(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)
{
}

void Device_Management::A_Write_Router_Memory_Acon(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data)
{
}

/* DMP_Connect_RCo */

void Device_Management::DMP_Connect_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_Connect_RCo_data.timeout_timer);

    DMP_Connect_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_Connect_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_Connect_RCo_data.state == 3) {
        DMP_Connect_RCo_data.state = 0;
        if (DMP_Connect_RCo_Lcon) {
            DMP_Connect_RCo_Lcon(DMP_Connect_RCo_data.IA_target, Status::not_ok);
        }
    }
}

/* DMP_Connect_RCl */

void Device_Management::DMP_Connect_RCl_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_Connect_RCl_data.timeout_timer);

    DMP_Connect_RCl_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_Connect_RCl_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_Connect_RCl_data.state == 2) {
        DMP_Connect_RCl_data.state = 0;
        if (DMP_Connect_RCl_Lcon) {
            DMP_Connect_RCl_Lcon(DMP_Connect_RCl_data.IA_target, Status::not_ok);
        }
    }
}

/* DMP_Connect_LEmi1 */
/* DMP_Connect_LcEMI */
/* DMP_Connect_R_KNXnetIPDeviceManagement */
/* DMP_DeviceDescriptor_InfoReport */
/* DMP_Disconnect_RCo */
/* DMP_Disconnect_LEmi1 */

/* DMP_Identify_R */

void Device_Management::DMP_Identify_R_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_Identify_R_data.timeout_timer);

    DMP_Identify_R_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_Identify_R_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_Identify_R_data.state == 2) {
        DMP_Identify_R_data.state = 0;
        if (DMP_Identify_R_Lcon) {
            DMP_Identify_R_Lcon(DMP_Identify_R_data.asap, Status::not_ok);
        }
    } else if (DMP_Identify_R_data.state == 4) {
        DMP_Identify_R_data.state = 0;
        if (DMP_Identify_R_Lcon) {
            DMP_Identify_R_Lcon(DMP_Identify_R_data.asap, Status::not_ok);
        }
    }
}

/* DMP_Identify_RCo2 */

void Device_Management::DMP_Identify_RCo2_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_Identify_RCo2_data.timeout_timer);

    DMP_Identify_RCo2_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_Identify_RCo2_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_Identify_RCo2_data.state == 2) {
        DMP_Identify_RCo2_data.state = 0;
        if (DMP_Identify_RCo2_Lcon) {
            DMP_Identify_RCo2_Lcon(DMP_Identify_RCo2_data.server_IA, Status::not_ok);
        }
    } else if (DMP_Identify_RCo2_data.state == 4) {
        DMP_Identify_RCo2_data.state = 0;
        if (DMP_Identify_RCo2_Lcon) {
            DMP_Identify_RCo2_Lcon(DMP_Identify_RCo2_data.server_IA, Status::not_ok);
        }
    }
}

/* DMP_Authorize_Rco */

void Device_Management::DMP_Authorize_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_Authorize_RCo_data.timeout_timer);

    DMP_Authorize_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_Authorize_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_Authorize_RCo_data.state == 1) {
        DMP_Authorize_RCo_data.state = 0;
        if (DMP_Authorize_RCo_Lcon) {
            DMP_Authorize_RCo_Lcon(DMP_Authorize_RCo_data.individual_address, DMP_Authorize_RCo_data.key, Status::not_ok);
        }
    }
}

/* DMP_Authorize2_Rco */

void Device_Management::DMP_Authorize2_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_Authorize2_RCo_data.timeout_timer);

    DMP_Authorize2_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_Authorize2_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if ((DMP_Authorize_RCo_data.state == 1) || (DMP_Authorize_RCo_data.state == 2) || (DMP_Authorize_RCo_data.state == 3)) {
        DMP_Authorize_RCo_data.state = 0;
        if (DMP_Authorize_RCo_Lcon) {
            DMP_Authorize_RCo_Lcon(DMP_Authorize_RCo_data.individual_address, DMP_Authorize_RCo_data.key, Status::not_ok);
        }
    }
}

/* DMP_SetKey_RCo */

void Device_Management::DMP_SetKey_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_SetKey_RCo_data.timeout_timer);

    DMP_SetKey_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_SetKey_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_SetKey_RCo_data.state == 1) {
        DMP_SetKey_RCo_data.state = 0;
        if (DMP_SetKey_RCo_Lcon) {
            DMP_SetKey_RCo_Lcon(DMP_SetKey_RCo_data.individual_address, DMP_SetKey_RCo_data.key, DMP_SetKey_RCo_data.level, Status::not_ok);
        }
    }
}


/* DMP_Restart_RCl */

void Device_Management::DMP_Restart_RCl_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_Restart_RCl_data.timeout_timer);

    DMP_Restart_RCl_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_Restart_RCl_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_Restart_RCl_data.state == 1) {
        DMP_Restart_RCl_data.state = 0;
        if (DMP_Restart_RCl_Lcon) {
            DMP_Restart_RCl_Lcon(DMP_Restart_RCl_data.individual_address, DMP_Restart_RCl_data.mpp_RestartType, DMP_Restart_RCl_data.mpp_EraseCode, DMP_Restart_RCl_data.mpp_ChannelNumber, Status::not_ok);
        }
    }
}

/* DMP_Restart_RCo */

void Device_Management::DMP_Restart_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_Restart_RCo_data.timeout_timer);

    DMP_Restart_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_Restart_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_Restart_RCo_data.state == 1) {
        DMP_Restart_RCo_data.state = 0;
        if (DMP_Restart_RCo_Lcon) {
            DMP_Restart_RCo_Lcon(DMP_Restart_RCo_data.individual_address, DMP_Restart_RCo_data.mpp_RestartType, DMP_Restart_RCo_data.mpp_EraseCode, DMP_Restart_RCo_data.mpp_ChannelNumber, Status::not_ok);
        }
    }
}

/* DMP_Restart_LEmi1 */

/* DMP_Delay */

void Device_Management::DMP_Delay_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_Delay_data.timeout_timer);

    DMP_Delay_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_Delay_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_Delay_data.state == 1) {
        DMP_Delay_data.state = 0;
        if (DMP_Delay_Lcon) {
            DMP_Delay_Lcon(DMP_Delay_data.delay_time, Status::ok);
        }
    }
}

/* DMP_IndividualAddressRead_LEmi1 */
/* DMP_IndividualAddressWrite_LEmi1 */
/* DMP_DomainAddressRead_LEmi1 */
/* DMP_DomainAddressWrite_LEmi1 */

/* DMP_ProgModeSwitch_RCo */

void Device_Management::DMP_ProgModeSwitch_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_ProgModeSwitch_RCo_data.timeout_timer);

    DMP_ProgModeSwitch_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_ProgModeSwitch_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_ProgModeSwitch_RCo_data.state == 2) {
        DMP_ProgModeSwitch_RCo_data.state = 0;
        if (DMP_ProgModeSwitch_RCo_Lcon) {
            DMP_ProgModeSwitch_RCo_Lcon(DMP_ProgModeSwitch_RCo_data.individual_address, DMP_ProgModeSwitch_RCo_data.mode, Status::not_ok);
        }
    } else if (DMP_ProgModeSwitch_RCo_data.state == 4) {
        DMP_ProgModeSwitch_RCo_data.state = 0;
        if (DMP_ProgModeSwitch_RCo_Lcon) {
            DMP_ProgModeSwitch_RCo_Lcon(DMP_ProgModeSwitch_RCo_data.individual_address, DMP_ProgModeSwitch_RCo_data.mode, Status::not_ok);
        }
    }
}

/* DMP_ProgModeSwitch_LEmi1 */

/* DMP_PeiTypeVerify_RCo_ADC */

void Device_Management::DMP_PeiTypeVerify_RCo_ADC_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_PeiTypeVerify_RCo_ADC_data.timeout_timer);

    DMP_PeiTypeVerify_RCo_ADC_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_PeiTypeVerify_RCo_ADC_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_PeiTypeVerify_RCo_ADC_data.state == 2) {
        DMP_PeiTypeVerify_RCo_ADC_data.state = 0;
        if (DMP_PeiTypeVerify_RCo_ADC_Lcon) {
            DMP_PeiTypeVerify_RCo_ADC_Lcon(DMP_PeiTypeVerify_RCo_ADC_data.individual_address, Status::not_ok);
        }
    }
}

/* DMP_PeiTypeVerify_R_IO */

void Device_Management::DMP_PeiTypeVerify_R_IO_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_PeiTypeVerify_R_IO_data.timeout_timer);

    DMP_PeiTypeVerify_R_IO_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_PeiTypeVerify_R_IO_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if ((DMP_PeiTypeVerify_R_IO_data.state == 2) || (DMP_PeiTypeVerify_R_IO_data.state == 4)) {
        DMP_PeiTypeVerify_R_IO_data.state = 0;
        if (DMP_PeiTypeVerify_R_IO_Lcon) {
            DMP_PeiTypeVerify_R_IO_Lcon(DMP_PeiTypeVerify_R_IO_data.asap, Status::not_ok);
        }
    }
}

/* DMP_PeiTypeRead_RCo_ADC_data */

void Device_Management::DMP_PeiTypeRead_RCo_ADC_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_PeiTypeRead_RCo_ADC_data.timeout_timer);

    DMP_PeiTypeRead_RCo_ADC_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_PeiTypeRead_RCo_ADC_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_PeiTypeRead_RCo_ADC_data.state == 2) {
        DMP_PeiTypeRead_RCo_ADC_data.state = 0;
        if (DMP_PeiTypeRead_RCo_ADC_Lcon) {
            DMP_PeiTypeRead_RCo_ADC_Lcon(DMP_PeiTypeRead_RCo_ADC_data.individual_address, Status::not_ok);
        }
    }
}

/* DMP_PeiTypeRead_R_IO */

void Device_Management::DMP_PeiTypeRead_R_IO_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_PeiTypeRead_R_IO_data.timeout_timer);

    DMP_PeiTypeRead_R_IO_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_PeiTypeRead_R_IO_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if ((DMP_PeiTypeRead_R_IO_data.state == 2) || (DMP_PeiTypeRead_R_IO_data.state == 4)) {
        DMP_PeiTypeRead_R_IO_data.state = 0;
        if (DMP_PeiTypeRead_R_IO_Lcon) {
            DMP_PeiTypeRead_R_IO_Lcon(DMP_PeiTypeRead_R_IO_data.asap, Status::not_ok);
        }
    }
}

/* DMP_MemWrite_RCo */

void Device_Management::DMP_MemWrite_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_MemWrite_RCo_data.timeout_timer);

    DMP_MemWrite_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_MemWrite_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_MemWrite_RCo_data.state == 3) {
        DMP_MemWrite_RCo_data.state = 0;
        if (DMP_MemWrite_RCo_Lcon) {
            DMP_MemWrite_RCo_Lcon(DMP_MemWrite_RCo_data.individual_address, DMP_MemWrite_RCo_data.verify, DMP_MemWrite_RCo_data.deviceStartAddress, DMP_MemWrite_RCo_data.deviceEndAddress, DMP_MemWrite_RCo_data.data, Status::not_ok);
        }
    } else if (DMP_MemWrite_RCo_data.state == 4) {
        DMP_MemWrite_RCo_data.state = 1;
        assert(A_Memory_Write_req);
        const ASAP_Connected asap{DMP_MemWrite_RCo_data.individual_address, true};
        Memory_Number number = DMP_MemWrite_RCo_data.data.size() - DMP_MemWrite_RCo_data.offset;
        // reduce to connection limit
        if (number > 12) { // @todo size is medium dependent
            number = 12;
        }
        Memory_Data data;
        data.assign(DMP_MemWrite_RCo_data.data.cbegin() + DMP_MemWrite_RCo_data.offset, DMP_MemWrite_RCo_data.data.cbegin() + DMP_MemWrite_RCo_data.offset + number);
        A_Memory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, number, DMP_MemWrite_RCo_data.deviceStartAddress + DMP_MemWrite_RCo_data.offset, data);
    }
}

/* DMP_MemWrite_RCoV */

void Device_Management::DMP_MemWrite_RCoV_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_MemWrite_RCoV_data.timeout_timer);

    DMP_MemWrite_RCoV_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_MemWrite_RCoV_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if ((DMP_MemWrite_RCoV_data.state == 2) || (DMP_MemWrite_RCoV_data.state == 4) || (DMP_MemWrite_RCoV_data.state == 6) || (DMP_MemWrite_RCoV_data.state == 8)) {
        DMP_MemWrite_RCoV_data.state = 0;
        if (DMP_MemWrite_RCoV_Lcon) {
            DMP_MemWrite_RCoV_Lcon(DMP_MemWrite_RCoV_data.individual_address, DMP_MemWrite_RCoV_data.verify, DMP_MemWrite_RCoV_data.device_control_unknown, DMP_MemWrite_RCoV_data.deviceStartAddress, DMP_MemWrite_RCoV_data.deviceEndAddress, DMP_MemWrite_RCoV_data.data, Status::not_ok);
        }
    }
}

/* DMP_MemWrite_LEmi1 */

void Device_Management::DMP_MemWrite_LEmi1_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_MemWrite_LEmi1_data.timeout_timer);

    DMP_MemWrite_LEmi1_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_MemWrite_LEmi1_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_MemWrite_LEmi1_data.state == 2) {
        Memory_Number expected_length = DMP_MemWrite_LEmi1_data.DmpData.size() - DMP_MemWrite_LEmi1_data.offset;
        // reduce to 12
        if (expected_length > 12) {
            expected_length = 12;
        }
        DMP_MemWrite_LEmi1_data.offset += expected_length;
        if (DMP_MemWrite_LEmi1_data.offset >= DMP_MemWrite_LEmi1_data.DmpData.size()) {
            if (DMP_MemWrite_LEmi1_Lcon) {
                DMP_MemWrite_LEmi1_data.state = 0;
                DMP_MemWrite_LEmi1_Lcon(DMP_MemWrite_LEmi1_data.verify, DMP_MemWrite_LEmi1_data.DmpStartAddr, DMP_MemWrite_LEmi1_data.DmpEndAddr, DMP_MemWrite_LEmi1_data.DmpData, Status::not_ok);
            }
        } else {
            DMP_MemWrite_LEmi1_data.state = 2;
            assert(PC_Set_Value_req);
            Memory_Number new_length = DMP_MemWrite_LEmi1_data.DmpData.size() - DMP_MemWrite_LEmi1_data.offset;
            // reduce to 12
            if (new_length > 12) {
                new_length = 12;
            }
            Memory_Data new_data;
            new_data.assign(DMP_MemWrite_LEmi1_data.DmpData.cbegin() + DMP_MemWrite_LEmi1_data.offset, DMP_MemWrite_LEmi1_data.DmpData.cbegin() + DMP_MemWrite_LEmi1_data.offset + new_length);
            PC_Set_Value_req(new_length, DMP_MemWrite_LEmi1_data.DmpStartAddr + DMP_MemWrite_LEmi1_data.offset, new_data);
            DMP_MemWrite_LEmi1_timeout_timer_restart(std::chrono::milliseconds(3000));
        }
    }
}

/* DMP_MemVerify_RCo */

void Device_Management::DMP_MemVerify_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_MemVerify_RCo_data.timeout_timer);

    DMP_MemVerify_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_MemVerify_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_MemVerify_RCo_data.state == 2) {
        DMP_MemVerify_RCo_data.state = 0;
        if (DMP_MemVerify_RCo_Lcon) {
            DMP_MemVerify_RCo_Lcon(DMP_MemVerify_RCo_data.individual_address, DMP_MemVerify_RCo_data.deviceStartAddress, DMP_MemVerify_RCo_data.deviceEndAddress, DMP_MemVerify_RCo_data.data, Status::not_ok);
        }
    }
}

/* DMP_MemVerify_LEmi1 */

/* DMP_MemRead_RCo */

void Device_Management::DMP_MemRead_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_MemRead_RCo_data.timeout_timer);

    DMP_MemRead_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_MemRead_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_MemRead_RCo_data.state == 2) {
        DMP_MemRead_RCo_data.state = 0;
        if (DMP_MemRead_RCo_Lcon) {
            DMP_MemRead_RCo_Lcon(DMP_MemRead_RCo_data.individual_address, DMP_MemRead_RCo_data.deviceStartAddress, DMP_MemRead_RCo_data.deviceEndAddress, DMP_MemRead_RCo_data.data, Status::not_ok);
        }
    }
}

/* DMP_MemRead_LEmi1 */

/* DMP_UserMemWrite_RCo */

void Device_Management::DMP_UserMemWrite_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_UserMemWrite_RCo_data.timeout_timer);

    DMP_UserMemWrite_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_UserMemWrite_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_UserMemWrite_RCoV */

void Device_Management::DMP_UserMemWrite_RCoV_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_UserMemWrite_RCoV_data.timeout_timer);

    DMP_UserMemWrite_RCoV_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_UserMemWrite_RCoV_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_UserMemVerify_RCo */

void Device_Management::DMP_UserMemVerify_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_UserMemVerify_RCo_data.timeout_timer);

    DMP_UserMemVerify_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_UserMemVerify_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_UserMemRead_RCo */

void Device_Management::DMP_UserMemRead_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_UserMemRead_RCo_data.timeout_timer);

    DMP_UserMemRead_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_UserMemRead_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_InterfaceObjectWrite_R */

void Device_Management::DMP_InterfaceObjectWrite_R_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_InterfaceObjectWrite_R_data.timeout_timer);

    DMP_InterfaceObjectWrite_R_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_InterfaceObjectWrite_R_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_ReducedInterfaceObjectWrite_R */

void Device_Management::DMP_ReducedInterfaceObjectWrite_R_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_ReducedInterfaceObjectWrite_R_data.timeout_timer);

    DMP_ReducedInterfaceObjectWrite_R_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_ReducedInterfaceObjectWrite_R_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_InterfaceObjectVerify_R */

void Device_Management::DMP_InterfaceObjectVerify_R_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_InterfaceObjectVerify_R_data.timeout_timer);

    DMP_InterfaceObjectVerify_R_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_InterfaceObjectVerify_R_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_InterfaceObjectRead_R */

void Device_Management::DMP_InterfaceObjectRead_R_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_InterfaceObjectRead_R_data.timeout_timer);

    DMP_InterfaceObjectRead_R_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_InterfaceObjectRead_R_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_ReducedInterfaceObjectRead_R */

void Device_Management::DMP_ReducedInterfaceObjectRead_R_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_ReducedInterfaceObjectRead_R_data.timeout_timer);

    DMP_ReducedInterfaceObjectRead_R_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_ReducedInterfaceObjectRead_R_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_InterfaceObjectScan_R */

void Device_Management::DMP_InterfaceObjectScan_R_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_InterfaceObjectScan_R_data.timeout_timer);

    DMP_InterfaceObjectScan_R_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_InterfaceObjectScan_R_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_ReducedInterfaceObjectScan_R */

void Device_Management::DMP_ReducedInterfaceObjectScan_R_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_ReducedInterfaceObjectScan_R_data.timeout_timer);

    DMP_ReducedInterfaceObjectScan_R_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_ReducedInterfaceObjectScan_R_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_InterfaceObject_InfoReport */

/* DMP_FunctionProperty_Write_R */

void Device_Management::DMP_FunctionProperty_Write_R_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_FunctionProperty_Write_R_data.timeout_timer);

    DMP_FunctionProperty_Write_R_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_FunctionProperty_Write_R_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LoadStateMachineWrite_RCo_Mem */

void Device_Management::DMP_LoadStateMachineWrite_RCo_Mem_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LoadStateMachineWrite_RCo_Mem_data.timeout_timer);

    DMP_LoadStateMachineWrite_RCo_Mem_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LoadStateMachineWrite_RCo_Mem_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LoadStateMachineWrite_RCo_IO */

void Device_Management::DMP_LoadStateMachineWrite_RCo_IO_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LoadStateMachineWrite_RCo_IO_data.timeout_timer);

    DMP_LoadStateMachineWrite_RCo_IO_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LoadStateMachineWrite_RCo_IO_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_DownloadLoadablePart_RCo_IO */

void Device_Management::DMP_DownloadLoadablePart_RCo_IO_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_DownloadLoadablePart_RCo_IO_data.timeout_timer);

    DMP_DownloadLoadablePart_RCo_IO_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_DownloadLoadablePart_RCo_IO_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LoadStateMachineVerify_RCo_Mem */

void Device_Management::DMP_LoadStateMachineVerify_RCo_Mem_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LoadStateMachineVerify_RCo_Mem_data.timeout_timer);

    DMP_LoadStateMachineVerify_RCo_Mem_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LoadStateMachineVerify_RCo_Mem_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LoadStateMachineVerify_R_IO */

void Device_Management::DMP_LoadStateMachineVerify_R_IO_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LoadStateMachineVerify_R_IO_data.timeout_timer);

    DMP_LoadStateMachineVerify_R_IO_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LoadStateMachineVerify_R_IO_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LoadStateMachineRead_RCo_Mem */

void Device_Management::DMP_LoadStateMachineRead_RCo_Mem_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LoadStateMachineRead_RCo_Mem_data.timeout_timer);

    DMP_LoadStateMachineRead_RCo_Mem_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LoadStateMachineRead_RCo_Mem_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

    if (DMP_LoadStateMachineRead_RCo_Mem_data.state == 2) {
        DMP_LoadStateMachineRead_RCo_Mem_data.state = 0;
        if (DMP_LoadStateMachineRead_RCo_Mem_Lcon) {
            DMP_LoadStateMachineRead_RCo_Mem_Lcon(DMP_LoadStateMachineRead_RCo_Mem_data.destination_address, DMP_LoadStateMachineRead_RCo_Mem_data.state_machine_type, Status::not_ok);
        }
    }
}

/* DMP_LoadStateMachineRead_R_IO */

void Device_Management::DMP_LoadStateMachineRead_R_IO_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LoadStateMachineRead_R_IO_data.timeout_timer);

    DMP_LoadStateMachineRead_R_IO_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LoadStateMachineRead_R_IO_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_RunStateMachineWrite_RCo_Mem */

void Device_Management::DMP_RunStateMachineWrite_RCo_Mem_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_RunStateMachineWrite_RCo_Mem_data.timeout_timer);

    DMP_RunStateMachineWrite_RCo_Mem_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_RunStateMachineWrite_RCo_Mem_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_RunStateMachineWrite_R_IO */

void Device_Management::DMP_RunStateMachineWrite_R_IO_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_RunStateMachineWrite_R_IO_data.timeout_timer);

    DMP_RunStateMachineWrite_R_IO_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_RunStateMachineWrite_R_IO_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_RunStateMachineVerify_RCo_Mem */

void Device_Management::DMP_RunStateMachineVerify_RCo_Mem_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_RunStateMachineVerify_RCo_Mem_data.timeout_timer);

    DMP_RunStateMachineVerify_RCo_Mem_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_RunStateMachineVerify_RCo_Mem_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_RunStateMachineVerify_R_IO */

void Device_Management::DMP_RunStateMachineVerify_R_IO_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_RunStateMachineVerify_R_IO_data.timeout_timer);

    DMP_RunStateMachineVerify_R_IO_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_RunStateMachineVerify_R_IO_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_RunStateMachineRead_RCo_Mem */

void Device_Management::DMP_RunStateMachineRead_RCo_Mem_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_RunStateMachineRead_RCo_Mem_data.timeout_timer);

    DMP_RunStateMachineRead_RCo_Mem_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_RunStateMachineRead_RCo_Mem_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_RunStateMachineRead_R_IO */

void Device_Management::DMP_RunStateMachineRead_R_IO_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_RunStateMachineRead_R_IO_data.timeout_timer);

    DMP_RunStateMachineRead_R_IO_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_RunStateMachineRead_R_IO_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_GroupObjectLink_Read_RCl */

void Device_Management::DMP_GroupObjectLink_Read_RCl_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_GroupObjectLink_Read_RCl_data.timeout_timer);

    DMP_GroupObjectLink_Read_RCl_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_GroupObjectLink_Read_RCl_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_GroupObjectLink_Write_RCl */

void Device_Management::DMP_GroupObjectLink_Write_RCl_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_GroupObjectLink_Write_RCl_data.timeout_timer);

    DMP_GroupObjectLink_Write_RCl_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_GroupObjectLink_Write_RCl_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCSlaveMemWrite_RCo */

void Device_Management::DMP_LCSlaveMemWrite_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCSlaveMemWrite_RCo_data.timeout_timer);

    DMP_LCSlaveMemWrite_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCSlaveMemWrite_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCSlaveMemVerify_RCo */

void Device_Management::DMP_LCSlaveMemVerify_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCSlaveMemVerify_RCo_data.timeout_timer);

    DMP_LCSlaveMemVerify_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCSlaveMemVerify_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCSlaveMemRead_RCo */

void Device_Management::DMP_LCSlaveMemRead_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCSlaveMemRead_RCo_data.timeout_timer);

    DMP_LCSlaveMemRead_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCSlaveMemRead_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCExtMemWrite_RCo */

void Device_Management::DMP_LCExtMemWrite_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCExtMemWrite_RCo_data.timeout_timer);

    DMP_LCExtMemWrite_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCExtMemWrite_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCExtMemVerify_RCo */

void Device_Management::DMP_LCExtMemVerify_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCExtMemVerify_RCo_data.timeout_timer);

    DMP_LCExtMemVerify_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCExtMemVerify_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCExtMemRead_RCo */

void Device_Management::DMP_LCExtMemRead_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCExtMemRead_RCo_data.timeout_timer);

    DMP_LCExtMemRead_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCExtMemRead_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCExtMemOpen_RCo */

void Device_Management::DMP_LCExtMemOpen_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCExtMemOpen_RCo_data.timeout_timer);

    DMP_LCExtMemOpen_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCExtMemOpen_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCRouteTableStateWrite_RCo */

void Device_Management::DMP_LCRouteTableStateWrite_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCRouteTableStateWrite_RCo_data.timeout_timer);

    DMP_LCRouteTableStateWrite_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCRouteTableStateWrite_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCRouteTableStateVerify_RCo */

void Device_Management::DMP_LCRouteTableStateVerify_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCRouteTableStateVerify_RCo_data.timeout_timer);

    DMP_LCRouteTableStateVerify_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCRouteTableStateVerify_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

/* DMP_LCRouteTableStateRead_RCo */

void Device_Management::DMP_LCRouteTableStateRead_RCo_timeout_timer_restart(const std::chrono::milliseconds timeout)
{
    assert(DMP_LCRouteTableStateRead_RCo_data.timeout_timer);

    DMP_LCRouteTableStateRead_RCo_data.timeout_timer->expires_after(timeout);
}

void Device_Management::DMP_LCRouteTableStateRead_RCo_timeout_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // timer expired
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        return;
    }

}

}
