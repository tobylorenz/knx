// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 3 Device Management Procedures */

/* 3.2 DM_Connect */
#include <KNX/03/05/02/Device_Management/DM_Connect.h>

/* 3.3 DM_Disconnect */
#include <KNX/03/05/02/Device_Management/DM_Disconnect.h>

/* 3.4 DM_Identify */
#include <KNX/03/05/02/Device_Management/DM_Identify.h>

/* 3.5 DM_Authorize */
#include <KNX/03/05/02/Device_Management/DM_Authorize.h>

/* 3.6 DM_SetKey */
#include <KNX/03/05/02/Device_Management/DM_SetKey.h>

/* 3.7 DM_Restart */
#include <KNX/03/05/02/Device_Management/DM_Restart.h>

/* 3.8 DM_Delay */
#include <KNX/03/05/02/Device_Management/DM_Delay.h>

/* 3.9 DM_IndividualAddressRead */
#include <KNX/03/05/02/Device_Management/DM_IndividualAddressRead.h>

/* 3.10 DM_IndividualAddressWrite */
#include <KNX/03/05/02/Device_Management/DM_IndividualAddressWrite.h>

/* 3.11 DM_DomainAddress_Read */
#include <KNX/03/05/02/Device_Management/DM_DomainAddressRead.h>

/* 3.12 DM_DomainAddressWrite */
#include <KNX/03/05/02/Device_Management/DM_DomainAddressWrite.h>

/* 3.13 DM_ProgMode_Switch */
#include <KNX/03/05/02/Device_Management/DM_ProgModeSwitch.h>

/* 3.14 DM_PeiTypeVerify */
#include <KNX/03/05/02/Device_Management/DM_PeiTypeVerify.h>

/* 3.15 DM_PeiTypeRead */
#include <KNX/03/05/02/Device_Management/DM_PeiTypeRead.h>

/* 3.16 DM_MemWrite */
#include <KNX/03/05/02/Device_Management/DM_MemWrite.h>

/* 3.17 DM_MemVerify */
#include <KNX/03/05/02/Device_Management/DM_MemVerify.h>

/* 3.18 DM_MemRead */
#include <KNX/03/05/02/Device_Management/DM_MemRead.h>

/* 3.19 DM_UserMemWrite */
#include <KNX/03/05/02/Device_Management/DM_UserMemWrite.h>

/* 3.20 DM_UserMemVerify */
#include <KNX/03/05/02/Device_Management/DM_UserMemVerify.h>

/* 3.21 DM_UserMemRead */
#include <KNX/03/05/02/Device_Management/DM_UserMemRead.h>

/* 3.22 DM_InterfaceObjectWrite */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectWrite.h>

/* 3.23 DM_InterfaceObjectVerify */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectVerify.h>

/* 3.24 DM_InterfaceObjectRead */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectRead.h>

/* 3.25 DM_InterfaceObjectScan */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectScan.h>

/* 3.26 DM_InterfaceObjectInfoReport */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectInfoReport.h>

/* 3.27 DM_FunctionProperty_Write_R */
#include <KNX/03/05/02/Device_Management/DM_FunctionProperty_Write.h>

/* 3.28 DM_LoadStateMachineWrite */
#include <KNX/03/05/02/Device_Management/DM_LoadStateMachineWrite.h>

/* 3.29 DM_LoadStateMachineVerify */
#include <KNX/03/05/02/Device_Management/DM_LoadStateMachineVerify.h>

/* 3.30 DM_LoadStateMachineRead */
#include <KNX/03/05/02/Device_Management/DM_LoadStateMachineRead.h>

/* 3.31 DM_RunStateMachineWrite */
#include <KNX/03/05/02/Device_Management/DM_RunStateMachineWrite.h>

/* 3.32 DM_RunStateMachineVerify */
#include <KNX/03/05/02/Device_Management/DM_RunStateMachineVerify.h>

/* 3.33 DM_RunStateMachineRead */
#include <KNX/03/05/02/Device_Management/DM_RunStateMachineRead.h>

/* 3.34 Procedures with Link Services */
#include <KNX/03/05/02/Device_Management/DM_GroupObjectLink_Read.h>
#include <KNX/03/05/02/Device_Management/DM_GroupObjectLink_Write.h>

/* 3.35 DM_LCSlaveMemWrite */
#include <KNX/03/05/02/Device_Management/DM_LCSlaveMemWrite.h>

/* 3.36 DM_LCSlaveMemVerify */
#include <KNX/03/05/02/Device_Management/DM_LCSlaveMemVerify.h>

/* 3.37 DM_LCSlaveMemRead */
#include <KNX/03/05/02/Device_Management/DM_LCSlaveMemRead.h>

/* 3.38 DM_LCExtMemWrite */
#include <KNX/03/05/02/Device_Management/DM_LCExtMemWrite.h>

/* 3.39 DM_LCExtMemVerify */
#include <KNX/03/05/02/Device_Management/DM_LCExtMemVerify.h>

/* 3.40 DM_LCExtMemRead */
#include <KNX/03/05/02/Device_Management/DM_LCExtMemRead.h>

/* 3.41 DM_LCExtMemOpen */
#include <KNX/03/05/02/Device_Management/DM_LCExtMemOpen.h>

/* 3.42 DM_LCRouteTableStateWrite */
#include <KNX/03/05/02/Device_Management/DM_LCRouteTableStateWrite.h>

/* 3.43 DM_LCRouteTableStateVerify */
#include <KNX/03/05/02/Device_Management/DM_LCRouteTableStateVerify.h>

/* 3.44 DM_LCRouteTableStateRead */
#include <KNX/03/05/02/Device_Management/DM_LCRouteTableStateRead.h>
