// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Authorize.h>

namespace KNX {

DM_Authorize::DM_Authorize(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    dmp_Authorize_RCo(io_context, application_layer, parameters),
    dmp_Authorize2_RCo(io_context, application_layer, parameters)
{
}

DM_Authorize::~DM_Authorize()
{
}

void DM_Authorize::req(const Flags flags, const Key key)
{
}

}
