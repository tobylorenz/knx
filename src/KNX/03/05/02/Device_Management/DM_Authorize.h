// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

/* 3.5.1 DMP_Authorize_RCo */
#include <KNX/03/05/02/Device_Management/DM_Authorize/DMP_Authorize_RCo.h>

/* 3.5.2 DMP_Authorize2_RCo */
#include <KNX/03/05/02/Device_Management/DM_Authorize/DMP_Authorize2_RCo.h>

namespace KNX {

/**
 * DM_Authorize
 *
 * @ingroup KNX_03_05_02_03_05
 */
class KNX_EXPORT DM_Authorize :
    public std::enable_shared_from_this<DM_Authorize>
{
public:
    explicit DM_Authorize(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DM_Authorize();

    struct Flags {
    };

    void req(const Flags flags, const Key key);
    std::function<void(Status dm_status)> con;

private:
    DMP_Authorize_RCo dmp_Authorize_RCo;
    DMP_Authorize2_RCo dmp_Authorize2_RCo;
};

}
