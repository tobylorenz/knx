// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_Authorize2_RCo
 *
 * @ingroup KNX_03_05_02_03_05_02
 */
class KNX_EXPORT DMP_Authorize2_RCo :
    public std::enable_shared_from_this<DMP_Authorize2_RCo>
{
public:
    explicit DMP_Authorize2_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_Authorize2_RCo();

    void req(const Key client_key);
    std::function<void(const Level free_level, const Level client_level, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Authorize.req
        S02, ///< waiting A_Authorize.Acon
        S03, ///< sending A_Authorize.req
        S04, ///< waiting A_Authorize.Acon
        S05, ///< sending A_Authorize.req
        S06, ///< waiting A_Authorize.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Authorize_req();
    void A02_A_Authorize_Acon();
    void A03_A_Authorize_req();
    void A04_A_Authorize_Acon();
    void A05_A_Authorize_req();
    void A06_A_Authorize_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
