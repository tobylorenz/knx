// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Connect.h>

namespace KNX {

DM_Connect::DM_Connect(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    dmp_Connect_RCo(io_context, application_layer, parameters),
    dmp_Connect_RCl(io_context, application_layer, parameters)
{
}

DM_Connect::~DM_Connect()
{
}

void DM_Connect::req(const Flags flags, const Individual_Address destination_address)
{
    if (flags.connectionless_communication) {
        dmp_Connect_RCl.req(destination_address);
        dmp_Connect_RCl.con = [this](const Status dm_status){
            con(dm_status);
        };
    } else {
        dmp_Connect_RCo.req(destination_address);
        dmp_Connect_RCo.con = [this](const Status dm_status){
            con(dm_status);
        };
    }
}

}
