// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

/* 3.2.1 DMP_Connect_RCo */
#include <KNX/03/05/02/Device_Management/DM_Connect/DMP_Connect_RCo.h>

/* 3.2.2 DMP_Connect_RCl */
#include <KNX/03/05/02/Device_Management/DM_Connect/DMP_Connect_RCl.h>

/* 3.2.3 DMP_Connect_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_Connect/DMP_Connect_LEmi1.h>

/* 3.2.5 DMP_Connect_LcEMI */
#include <KNX/03/05/02/Device_Management/DM_Connect/DMP_Connect_LcEMI.h>

/* 3.2.6 DMP_Connect_R_KNXnetIPDeviceManagement */
#include <KNX/03/05/02/Device_Management/DM_Connect/DMP_Connect_R_IP.h>

/* 3.2.7 DMP_DeviceDescriptor_InfoReport */
#include <KNX/03/05/02/Device_Management/DM_Connect/DMP_DeviceDescriptor_InfoReport.h>

namespace KNX {

/**
 * DM_Connect
 *
 * @ingroup KNX_03_05_02_03_02
 */
class KNX_EXPORT DM_Connect :
    public std::enable_shared_from_this<DM_Connect>
{
public:
    explicit DM_Connect(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DM_Connect();

    struct Flags {
        bool connectionless_communication : 1; // bit 0
    };

    void req(const Flags flags, const Individual_Address destination_address);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    DMP_Connect_RCo dmp_Connect_RCo;
    DMP_Connect_RCl dmp_Connect_RCl;
};

}
