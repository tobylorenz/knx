// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Connect/DMP_Connect_RCl.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_Connect_RCl::DMP_Connect_RCl(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_Connect_RCl::~DMP_Connect_RCl()
{
}

void DMP_Connect_RCl::req(const Individual_Address IA_target)
{
    if (state == State::S00) {
        state = State::S01;
        parameters.communication_mode = DM_Parameters::Communication_Mode::RCl;
        parameters.destination_address = IA_target;
        A01_A_DeviceDescriptor_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_Connect_RCl::A01_A_DeviceDescriptor_Read_req()
{
    assert(state == State::S01);

    application_layer.A_DeviceDescriptor_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), 0, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_DeviceDescriptor_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_Connect_RCl::A02_A_DeviceDescriptor_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_DeviceDescriptor_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor) -> void {
        if ((asap == parameters.asap()) && (descriptor_type == 0)) {
            state = State::S00;
            parameters.device_descriptor_0.fromData(std::cbegin(device_descriptor), std::cend(device_descriptor));
            con(Status::ok);
        }
    });
}
void DMP_Connect_RCl::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

}
