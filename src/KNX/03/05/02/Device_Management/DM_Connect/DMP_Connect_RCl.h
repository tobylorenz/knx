// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_Connect_RCl
 *
 * @ingroup KNX_03_05_02_03_02_02
 */
class KNX_EXPORT DMP_Connect_RCl :
    public std::enable_shared_from_this<DMP_Connect_RCl>
{
public:
    explicit DMP_Connect_RCl(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_Connect_RCl();

    void req(const Individual_Address IA_target);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_DeviceDescriptor_Read.req
        S02, ///< waiting A_DeviceDescriptor_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_DeviceDescriptor_Read_req();
    void A02_A_DeviceDescriptor_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
