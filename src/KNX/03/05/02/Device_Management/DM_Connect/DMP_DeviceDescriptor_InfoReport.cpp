// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Connect/DMP_DeviceDescriptor_InfoReport.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_DeviceDescriptor_InfoReport::DMP_DeviceDescriptor_InfoReport(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_DeviceDescriptor_InfoReport::~DMP_DeviceDescriptor_InfoReport()
{
}

void DMP_DeviceDescriptor_InfoReport::req(const Descriptor_Type DM_DDType, const Device_Descriptor_Data DM_DD)
{
    // @todo     assert((parameters.communication_mode == DM_Parameters::Communication_Mode::RCo) || (parameters.communication_mode == DM_Parameters::Communication_Mode::RCl));

    if (state == State::S00) {
        state = State::S01;
        parameters.device_descriptor_type = DM_DDType;
        switch(DM_DDType) {
        case 0:
            parameters.device_descriptor_0.fromData(std::cbegin(DM_DD), std::cend(DM_DD));
            break;
        case 2:
            parameters.device_descriptor_2.fromData(std::cbegin(DM_DD), std::cend(DM_DD));
            break;
        }
        A01_A_DeviceDescriptor_InfoReport_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_DeviceDescriptor_InfoReport::A01_A_DeviceDescriptor_InfoReport_req()
{
    assert(state == State::S01);

    Device_Descriptor_Data descriptor_data;
    switch(parameters.device_descriptor_type) {
    case 0:
        descriptor_data = parameters.device_descriptor_0.toData();
        break;
    case 2:
        descriptor_data = parameters.device_descriptor_2.toData();
        break;
    }
    application_layer.A_DeviceDescriptor_InfoReport_req(Ack_Request::dont_care, parameters.device_descriptor_type, descriptor_data, Network_Layer_Parameter, Priority::low, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S00;
            con(Status::ok);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_DeviceDescriptor_InfoReport::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

}
