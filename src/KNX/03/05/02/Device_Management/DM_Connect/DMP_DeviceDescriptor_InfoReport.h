// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_DeviceDescriptor_InfoReport
 *
 * @ingroup KNX_03_05_02_03_02_07
 */
class KNX_EXPORT DMP_DeviceDescriptor_InfoReport :
    public std::enable_shared_from_this<DMP_DeviceDescriptor_InfoReport>
{
public:
    explicit DMP_DeviceDescriptor_InfoReport(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_DeviceDescriptor_InfoReport();

    void req(const Descriptor_Type DM_DDType, const Device_Descriptor_Data DM_DD);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_DeviceDescriptor_InfoReport.req
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_DeviceDescriptor_InfoReport_req();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
