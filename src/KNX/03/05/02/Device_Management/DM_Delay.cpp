// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Delay.h>

namespace KNX {

DM_Delay::DM_Delay(asio::io_context & io_context) :
    dmp_Delay(io_context)
{
}

DM_Delay::~DM_Delay()
{
}

void DM_Delay::req(const Flags /*flags*/, const std::chrono::milliseconds delay_time)
{
    dmp_Delay.req(delay_time);
    dmp_Delay.con = [this](const Status dm_status){
        con(dm_status);
    };
}

}
