// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <chrono>

#include <asio/io_context.hpp>

#include <KNX/knx_export.h>

/* 3.8.2 DMP_Delay */
#include <KNX/03/05/02/Device_Management/DM_Delay/DMP_Delay.h>

namespace KNX {

/**
 * DM_Delay
 *
 * @ingroup KNX_03_05_02_03_08
 */
class KNX_EXPORT DM_Delay :
    public std::enable_shared_from_this<DM_Delay>
{
public:
    explicit DM_Delay(asio::io_context & io_context);
    virtual ~DM_Delay();

    struct Flags {
    };

    void req(const Flags flags, const std::chrono::milliseconds delay_time);
    std::function<void(Status dm_status)> con;

private:
    DMP_Delay dmp_Delay;
};

}
