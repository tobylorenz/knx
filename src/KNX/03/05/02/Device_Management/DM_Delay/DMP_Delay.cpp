// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Delay/DMP_Delay.h>

namespace KNX {

DMP_Delay::DMP_Delay(asio::io_context & io_context) :
    io_context(io_context),
    timeout_timer(io_context)
{
}

DMP_Delay::~DMP_Delay()
{
}

void DMP_Delay::req(const std::chrono::milliseconds delay_time)
{
    if (state == State::S00) {
        state = State::S01;
        restart_timer(delay_time);
    }
}

void DMP_Delay::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(Status::ok);
            break;
        }
    });
}

}
