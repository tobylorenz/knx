// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_Delay
 *
 * @ingroup KNX_03_05_02_03_08_01
 */
class KNX_EXPORT DMP_Delay :
    public std::enable_shared_from_this<DMP_Delay>
{
public:
    explicit DMP_Delay(asio::io_context & io_context);
    virtual ~DMP_Delay();

    void req(const std::chrono::milliseconds delay_time);
    std::function<void(Status dm_status)> con;

private:
    asio::io_context & io_context;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< delaying
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void restart_timer(const std::chrono::milliseconds duration);
};

}
