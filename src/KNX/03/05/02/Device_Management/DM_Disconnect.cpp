// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Disconnect.h>

namespace KNX {

DM_Disconnect::DM_Disconnect(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    dmp_Disconnect_RCo(io_context, application_layer, parameters)
{
}

DM_Disconnect::~DM_Disconnect()
{
}

void DM_Disconnect::req(const Flags flags)
{
    //dmp_disconnect_rco.req();
    dmp_Disconnect_RCo.con = [this](const Status dm_status){
        con(dm_status);
    };
}

}
