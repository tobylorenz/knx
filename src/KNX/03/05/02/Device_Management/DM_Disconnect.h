// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

/* 3.3.2 DMP_Disconnect_RCo */
#include <KNX/03/05/02/Device_Management/DM_Disconnect/DMP_Disconnect_RCo.h>

/* 3.3.4 DMP_Disconnect_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_Disconnect/DMP_Disconnect_LEmi1.h>

namespace KNX {

/**
 * DM_Disconnect
 *
 * @ingroup KNX_03_05_02_03_03
 */
class KNX_EXPORT DM_Disconnect :
    public std::enable_shared_from_this<DM_Disconnect>
{
public:
    explicit DM_Disconnect(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DM_Disconnect();

    struct Flags {
    };

    void req(const Flags flags);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    DMP_Disconnect_RCo dmp_Disconnect_RCo;
};

}
