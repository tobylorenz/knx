# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/DMP_Disconnect_LEmi1.h
        ${CMAKE_CURRENT_SOURCE_DIR}/DMP_Disconnect_RCo.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/DMP_Disconnect_LEmi1.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/DMP_Disconnect_RCo.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/DMP_Disconnect_LEmi1.h
        ${CMAKE_CURRENT_SOURCE_DIR}/DMP_Disconnect_RCo.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/05/02/Device_Management/DM_Disconnect)
