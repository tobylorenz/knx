// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Disconnect/DMP_Disconnect_LEmi1.h>

namespace KNX {

DMP_Disconnect_LEmi1::DMP_Disconnect_LEmi1(asio::io_context & io_context, EMI_User_Layer & user_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    user_layer(user_layer)
{
}

DMP_Disconnect_LEmi1::~DMP_Disconnect_LEmi1()
{
}

void DMP_Disconnect_LEmi1::req()
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::LEmi1);

    if (state == State::S00) {
        con(Status::ok);
    }
}

}
