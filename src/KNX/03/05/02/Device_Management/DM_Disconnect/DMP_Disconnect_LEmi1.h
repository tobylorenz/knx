// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/EMI_User_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_Disconnect_LEmi1
 *
 * @ingroup KNX_03_05_02_03_03_04
 */
class KNX_EXPORT DMP_Disconnect_LEmi1 :
    public std::enable_shared_from_this<DMP_Disconnect_LEmi1>
{
public:
    explicit DMP_Disconnect_LEmi1(asio::io_context & io_context, EMI_User_Layer & user_layer, DM_Parameters & parameters);
    virtual ~DMP_Disconnect_LEmi1();

    void req();
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    asio::io_context & io_context;
    EMI_User_Layer & user_layer;

    /** state */
    enum class State {
        S00, ///< idle
    };
    State state { State::S00 };
};

}
