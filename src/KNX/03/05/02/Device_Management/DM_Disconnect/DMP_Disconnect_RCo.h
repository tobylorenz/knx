// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_Disconnect_RCo
 *
 * @ingroup KNX_03_05_02_03_03_02
 */
class KNX_EXPORT DMP_Disconnect_RCo :
    public std::enable_shared_from_this<DMP_Disconnect_RCo>
{
public:
    explicit DMP_Disconnect_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_Disconnect_RCo();

    void req();
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Disconnect.req
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Disconnect_req();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
