// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_DomainAddressRead.h>

namespace KNX {

DM_DomainAddressRead::DM_DomainAddressRead(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_DomainAddressRead::~DM_DomainAddressRead()
{
}

void DM_DomainAddressRead::req()
{
}

}
