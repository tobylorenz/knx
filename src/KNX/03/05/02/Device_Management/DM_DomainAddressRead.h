// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.11.2 DMP_DomainAddressRead_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_DomainAddressRead/DMP_DomainAddressRead_LEmi1.h>

namespace KNX {

/**
 * DM_DomainAddress_Read
 *
 * @ingroup KNX_03_05_02_03_11
 */
class KNX_EXPORT DM_DomainAddressRead :
     public std::enable_shared_from_this<DM_DomainAddressRead>
{
public:
     explicit DM_DomainAddressRead(asio::io_context & io_context, Application_Layer & application_layer);
     virtual ~DM_DomainAddressRead();

     void req();
     std::function<void(Status dm_status)> con;
};

}
