// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_DomainAddressWrite.h>

namespace KNX {

DM_DomainAddressWrite::DM_DomainAddressWrite(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_DomainAddressWrite::~DM_DomainAddressWrite()
{
}

void DM_DomainAddressWrite::req(const Domain_Address_2 domain_address)
{
}

}
