// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.12.2 DMP_DomainAddressWrite_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_DomainAddressWrite/DMP_DomainAddressWrite_LEmi1.h>

namespace KNX {

/**
 * DM_DomainAddressWrite
 *
 * @ingroup KNX_03_05_02_03_12
 */
class KNX_EXPORT DM_DomainAddressWrite :
     public std::enable_shared_from_this<DM_DomainAddressWrite>
{
public:
     explicit DM_DomainAddressWrite(asio::io_context & io_context, Application_Layer & application_layer);
     virtual ~DM_DomainAddressWrite();

     void req(const Domain_Address_2 domain_address);
     std::function<void(Status dm_status)> con;
};

}
