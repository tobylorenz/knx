// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_FunctionProperty_Write.h>

namespace KNX {

DM_FunctionProperty_Write::DM_FunctionProperty_Write(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_FunctionProperty_Write::~DM_FunctionProperty_Write()
{
}

void DM_FunctionProperty_Write::req()
{
}

}
