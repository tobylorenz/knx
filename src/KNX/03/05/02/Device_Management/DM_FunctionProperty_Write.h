// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

#include <KNX/03/05/02/Device_Management/DM_FunctionProperty_Write/DMP_FunctionProperty_Write_R.h>

namespace KNX {

/**
 * DM_FunctionProperty_Write_R
 *
 * @ingroup KNX_03_05_02_03_27
 */
class KNX_EXPORT DM_FunctionProperty_Write :
    public std::enable_shared_from_this<DM_FunctionProperty_Write>
{
public:
    explicit DM_FunctionProperty_Write(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_FunctionProperty_Write();

    void req();
    std::function<void(Status dm_status)> con;
};

}
    






