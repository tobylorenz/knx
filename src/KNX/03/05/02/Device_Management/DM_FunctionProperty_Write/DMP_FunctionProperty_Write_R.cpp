// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_FunctionProperty_Write/DMP_FunctionProperty_Write_R.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_FunctionProperty_Write_R::DMP_FunctionProperty_Write_R(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_FunctionProperty_Write_R::~DMP_FunctionProperty_Write_R()
{
}

void DMP_FunctionProperty_Write_R::req(const Object_Index OI, const Property_Id PID, const Property_Value command)
{
    assert((parameters.communication_mode == DM_Parameters::Communication_Mode::RCo) || (parameters.communication_mode == DM_Parameters::Communication_Mode::RCl));

    if (state == State::S00) {
        state = State::S01;
        this->OI = OI;
        this->PID = PID;
        this->command = command;
        this->error = 0;
        A01_A_FunctionPropertyCommand_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_FunctionProperty_Write_R::A01_A_FunctionPropertyCommand_req()
{
    assert(state == State::S01);

    Comm_Mode comm_mode{};
    switch(parameters.communication_mode) {
    case DM_Parameters::Communication_Mode::RCl:
        comm_mode = Comm_Mode::Individual;
        break;
    case DM_Parameters::Communication_Mode::RCo:
        comm_mode = Comm_Mode::Connected;
        break;
    default:
        assert(false);
    }

    application_layer.A_FunctionPropertyCommand_req(Ack_Request::dont_care, parameters.asap(), comm_mode, command, Network_Layer_Parameter, OI, Priority::low, PID, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_FunctionPropertyCommand_Acon();
            restart_timer(Acon_timeout);
        case Status::not_ok:
            state = State::S00;
            con(error, Status::not_ok);
        }
    });
}

void DMP_FunctionProperty_Write_R::A02_A_FunctionPropertyCommand_Acon()
{
    assert(state == State::S02);

    application_layer.A_FunctionPropertyCommand_Acon([this](const ASAP_Individual asap, const Comm_Mode /*comm_mode*/, const Property_Value data, const Hop_Count_Type /*hop_count_type*/, const Object_Index object_index, const Priority /*priority*/, const Property_Id property_id, const std::optional<Property_Return_Code> return_code) -> void {
        if ((asap == parameters.asap()) && (data == command) && (object_index == OI) && (property_id == PID)) {
            state = State::S00;
            error = return_code.value();
            con(error, Status::ok);
        }
        if (state == State::S02) {
            A02_A_FunctionPropertyCommand_Acon();
        }
    });
}

void DMP_FunctionProperty_Write_R::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            // con(Status::not_ok);
            break;
        }
    });
}

}
