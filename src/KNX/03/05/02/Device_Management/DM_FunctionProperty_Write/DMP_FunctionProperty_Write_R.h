// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_FunctionProperty_Write_R
 *
 * @ingroup KNX_03_05_02_03_27
 */
class KNX_EXPORT DMP_FunctionProperty_Write_R :
    public std::enable_shared_from_this<DMP_FunctionProperty_Write_R>
{
public:
    explicit DMP_FunctionProperty_Write_R(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_FunctionProperty_Write_R();

    void req(const Object_Index OI, const Property_Id PID, const Property_Value command);
    std::function<void(const Property_Return_Code error, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    Object_Index OI{};
    Property_Id PID{};
    Property_Value command{};
    Property_Return_Code error{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_FunctionPropertyCommand.req
        S02, ///< waiting A_FunctionPropertyCommand.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_FunctionPropertyCommand_req();
    void A02_A_FunctionPropertyCommand_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
