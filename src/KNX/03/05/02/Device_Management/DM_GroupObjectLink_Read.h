// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.34.2 DMP_GroupObjectLink_Read_RCl */
#include <KNX/03/05/02/Device_Management/DM_GroupObjectLink_Read/DMP_GroupObjectLink_Read_RCl.h>

namespace KNX {

/**
 * DM_GroupObjectLink_Read
 * 
 * @ingroup KNX_03_05_02_03_34
 */
class KNX_EXPORT DM_GroupObjectLink_Read :
    public std::enable_shared_from_this<DM_GroupObjectLink_Read>
{
public:
    explicit DM_GroupObjectLink_Read(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_GroupObjectLink_Read();

    void req();
    std::function<void(Status dm_status)> con;
};

}
