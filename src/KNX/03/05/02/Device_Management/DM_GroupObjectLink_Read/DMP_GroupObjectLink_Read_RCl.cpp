// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_GroupObjectLink_Read/DMP_GroupObjectLink_Read_RCl.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_GroupObjectLink_Read_RCl::DMP_GroupObjectLink_Read_RCl(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_GroupObjectLink_Read_RCl::~DMP_GroupObjectLink_Read_RCl()
{
}

void DMP_GroupObjectLink_Read_RCl::req(const Group_Object_Number group_object_number)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCl);

    if (state == State::S00) {
        state = State::S01;
        this->group_object_number = group_object_number;
        sendingindex = 0;
        GAList.clear();
        start_index = 1;
        A01_A_Link_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_GroupObjectLink_Read_RCl::A01_A_Link_Read_req()
{
    assert(state == State::S01);

    application_layer.A_Link_Read_req(parameters.asap(), group_object_number, Priority::low, start_index, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_Link_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(sendingindex, GAList, Status::not_ok);
            break;
        }
    });
}

void DMP_GroupObjectLink_Read_RCl::A02_A_Link_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_Link_Read_Acon([this](const ASAP_Individual asap, const std::vector<Group_Address> group_address_list, const Group_Object_Number group_object_number, const Priority /*priority*/, const Link_Sending_Address sending_address, const Link_Start_Index start_index) -> void {
        if ((asap == parameters.asap()) && (group_object_number == this->group_object_number)) {
            if (start_index == 0) {
                // negative response
                con(sendingindex, GAList, Status::not_ok);
            } else
            if (start_index == this->start_index) {
                // positive response
                GAList.insert(std::end(GAList), std::cbegin(group_address_list), std::cend(group_address_list));
                const uint4_t size = GAList.size();
                this->start_index = this->start_index + size;
                if (size == 6) {
                    state = State::S01;
                    A01_A_Link_Read_req();
                    restart_timer(Lcon_timeout);
                } else
                if (size < 6) {
                    state = State::S00;
                    con(sendingindex, GAList, Status::ok);
                } else { // size > 6
                    // should not happen
                    con(sendingindex, GAList, Status::not_ok);
                }
            } else { // start_index != this->start_index
                // should not happen
                con(sendingindex, GAList, Status::not_ok);
            }
        }
        if (state == State::S02) {
            A02_A_Link_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_GroupObjectLink_Read_RCl::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(sendingindex, GAList, Status::not_ok);
            break;
        }
    });
}

}
