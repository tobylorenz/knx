// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_GroupObjectLink_Write.h>

namespace KNX {

DM_GroupObjectLink_Write::DM_GroupObjectLink_Write(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_GroupObjectLink_Write::~DM_GroupObjectLink_Write()
{
}

void DM_GroupObjectLink_Write::req()
{
}

}
