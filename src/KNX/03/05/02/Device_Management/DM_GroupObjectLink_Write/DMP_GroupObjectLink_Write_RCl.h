// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_GroupObjectLink_Write_RCl
 *
 * @ingroup KNX_03_05_02_03_34_03
 */
class KNX_EXPORT DMP_GroupObjectLink_Write_RCl :
    public std::enable_shared_from_this<DMP_GroupObjectLink_Write_RCl>
{
public:
    explicit DMP_GroupObjectLink_Write_RCl(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_GroupObjectLink_Write_RCl();

    void req(const Group_Object_Number group_object_number, const Link_Write_Flags::D action, const Link_Write_Flags::S sending, const Group_Address value);
    std::function<void(Link_Sending_Address sendingindex, std::vector<Group_Address> GAList, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    Group_Object_Number group_object_number{}; // in
    Link_Write_Flags flags{}; // in
    Group_Address value{}; // in
    Link_Sending_Address sendingindex{}; // out
    std::vector<Group_Address> GAList{}; // out

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Link_Write.req
        S02, ///< waiting A_Link_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Link_Write_req();
    void A02_A_Link_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
