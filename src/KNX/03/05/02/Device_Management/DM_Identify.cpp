// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Identify.h>

namespace KNX {

DM_Identify::DM_Identify(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    dmp_Identify_R(io_context, application_layer, parameters),
    dmp_Identify_RCo2(io_context, application_layer, parameters)
{
}

DM_Identify::~DM_Identify()
{
}

void DM_Identify::req()
{
}

}
