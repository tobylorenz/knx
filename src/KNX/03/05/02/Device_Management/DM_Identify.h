// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

/* 3.4.2 DMP_Identify_R */
#include <KNX/03/05/02/Device_Management/DM_Identify/DMP_Identify_R.h>

/* 3.4.3 DMP_Identify_RCo2 */
#include <KNX/03/05/02/Device_Management/DM_Identify/DMP_Identify_RCo2.h>

namespace KNX {

/**
 * DM_Identify
 * 
 * @ingroup KNX_03_05_02_03_04
 */
class KNX_EXPORT DM_Identify :
    public std::enable_shared_from_this<DM_Identify>
{
public:
    explicit DM_Identify(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DM_Identify();

    void req();
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    DMP_Identify_R dmp_Identify_R;
    DMP_Identify_RCo2 dmp_Identify_RCo2;
};

}
