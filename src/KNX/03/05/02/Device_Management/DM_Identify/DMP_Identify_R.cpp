// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Identify/DMP_Identify_R.h>

#include <KNX/03/05/01/00_Device/Device_Object.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_Identify_R::DMP_Identify_R(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_Identify_R::~DMP_Identify_R()
{
}

void DMP_Identify_R::req()
{
    assert((parameters.communication_mode == DM_Parameters::Communication_Mode::RCo) || (parameters.communication_mode == DM_Parameters::Communication_Mode::RCl));

    if (state == State::S00) {
        state = State::S01;
        A01_A_DeviceDescriptor_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_Identify_R::A01_A_DeviceDescriptor_Read_req()
{
    assert(state == State::S01);

    application_layer.A_DeviceDescriptor_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), 0, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S01;
            A02_A_DeviceDescriptor_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(parameters.device_descriptor_0, parameters.management_descriptor_1.toData(), Status::not_ok);
            break;
        }
    });
}

void DMP_Identify_R::A02_A_DeviceDescriptor_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_DeviceDescriptor_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor) -> void {
        if ((asap == parameters.asap()) && (descriptor_type == 0)) {
            parameters.device_descriptor_0.fromData(std::cbegin(device_descriptor), std::cend(device_descriptor));
            if (parameters.device_descriptor_0 == 0x0300) {
                state = State::S03;
                A03_A_PropertyValue_Read_req();
            } else {
                state = State::S00;
                con(parameters.device_descriptor_0, parameters.management_descriptor_1.toData(), Status::ok);
            }
        }
    });
}

void DMP_Identify_R::A03_A_PropertyValue_Read_req()
{
    assert(state == State::S03);

    application_layer.A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), 0, Device_Object::PID_MGT_DESCRIPTOR_01, 1, 1, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S04;
            A04_A_PropertyValue_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(parameters.device_descriptor_0, parameters.management_descriptor_1.toData(), Status::not_ok);
            break;
        }
    });
}

void DMP_Identify_R::A04_A_PropertyValue_Read_Acon()
{
    assert(state == State::S04);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == parameters.asap()) && (object_index == 0) && (property_id == Device_Object::PID_MGT_DESCRIPTOR_01) && (nr_of_elem == 1) && (start_index == 1)) {
            state = State::S00;
            parameters.management_descriptor_1.fromData(std::cbegin(data), std::cend(data));
            con(parameters.device_descriptor_0, parameters.management_descriptor_1.toData(), Status::ok);
        }
        if (state == State::S04) {
            A04_A_PropertyValue_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_Identify_R::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            // con(Status::not_ok);
            break;
        }
    });
}

}
