// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_Identify_RCo2
 *
 * @ingroup KNX_03_05_02_03_04_03
 */
class KNX_EXPORT DMP_Identify_RCo2 :
    public std::enable_shared_from_this<DMP_Identify_RCo2>
{
public:
    explicit DMP_Identify_RCo2(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_Identify_RCo2();

    void req();
    std::function<void(const Device_Descriptor_Type_0 device_descriptor_type_0, const Manufacturer_Identifier_Property manufacturer_id, const Hardware_Type_Property hardware_type, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_PropertyValue_Read.req
        S02, ///< sending A_PropertyValue_Read.Acon
        S03, ///< sending A_PropertyValue_Read.req
        S04, ///< sending A_PropertyValue_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_PropertyValue_Read_req();
    void A02_A_PropertyValue_Read_Acon();
    void A03_A_PropertyValue_Read_req();
    void A04_A_PropertyValue_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
