// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_IndividualAddressRead.h>

namespace KNX {

DM_IndividualAddressRead::DM_IndividualAddressRead(asio::io_context & io_context, EMI_User_Layer & user_layer, DM_Parameters & parameters) :
    parameters(parameters),
    dmp_IndividualAddressRead_LEmi1(io_context, user_layer, parameters)
{
}

DM_IndividualAddressRead::~DM_IndividualAddressRead()
{
}

void DM_IndividualAddressRead::req()
{
}

}
