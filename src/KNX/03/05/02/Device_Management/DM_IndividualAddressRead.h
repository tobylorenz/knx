// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/02/Device_Management/DM_Parameters.h>

#include <KNX/03/06/03/EMI/EMI_User_Layer.h>
#include <KNX/knx_export.h>

/* 3.9.2 DMP_IndividualAddressRead_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_IndividualAddressRead/DMP_IndividualAddressRead_LEmi1.h>

namespace KNX {

/**
 * DM_IndividualAddressRead
 * 
 * @ingroup KNX_03_05_02_03_09
 */
class KNX_EXPORT DM_IndividualAddressRead :
    public std::enable_shared_from_this<DM_IndividualAddressRead>
{
public:
    explicit DM_IndividualAddressRead(asio::io_context & io_context, EMI_User_Layer & user_layer, DM_Parameters & parameters);
    virtual ~DM_IndividualAddressRead();

    void req();
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    DMP_IndividualAddressRead_LEmi1 dmp_IndividualAddressRead_LEmi1;
};

}
