// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_IndividualAddressWrite.h>

namespace KNX {

DM_IndividualAddressWrite::DM_IndividualAddressWrite(asio::io_context & io_context, EMI_User_Layer & user_layer, DM_Parameters & parameters) :
    parameters(parameters),
    dmp_IndividualAddressWrite_LEmi1(io_context, user_layer, parameters)
{
}

DM_IndividualAddressWrite::~DM_IndividualAddressWrite()
{
}

void DM_IndividualAddressWrite::req(const Individual_Address individual_address)
{
}

}
