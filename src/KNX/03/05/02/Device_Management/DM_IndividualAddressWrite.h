// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/03/06/03/EMI/EMI_User_Layer.h>
#include <KNX/knx_export.h>

/* 3.10.2 DMP_IndividualAddressWrite_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_IndividualAddressWrite/DMP_IndividualAddressWrite_LEmi1.h>

namespace KNX {

/**
 * DM_IndividualAddressWrite
 *
 * @ingroup KNX_03_05_02_03_10
 */
class KNX_EXPORT DM_IndividualAddressWrite :
    public std::enable_shared_from_this<DM_IndividualAddressWrite>
{
public:
    explicit DM_IndividualAddressWrite(asio::io_context & io_context, EMI_User_Layer & user_layer, DM_Parameters & parameters);
    virtual ~DM_IndividualAddressWrite();

    void req(const Individual_Address individual_address);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    DMP_IndividualAddressWrite_LEmi1 dmp_IndividualAddressWrite_LEmi1;
};

}
