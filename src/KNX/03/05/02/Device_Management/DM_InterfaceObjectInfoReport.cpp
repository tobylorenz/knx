// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectInfoReport.h>

namespace KNX {

DM_InterfaceObjectInfoReport::DM_InterfaceObjectInfoReport(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_InterfaceObjectInfoReport::~DM_InterfaceObjectInfoReport()
{
}

void DM_InterfaceObjectInfoReport::req()
{
}

}
