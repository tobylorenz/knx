// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* DMP_InterfaceObjectInfoReport_RCl */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectInfoReport/DMP_InterfaceObjectInfoReport_RCl.h>

namespace KNX {

/**
 * DM_InterfaceObjectInfoReport
 *
 * @ingroup KNX_03_05_02_03_26
 */
class KNX_EXPORT DM_InterfaceObjectInfoReport :
    public std::enable_shared_from_this<DM_InterfaceObjectInfoReport>
{
public:
    explicit DM_InterfaceObjectInfoReport(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_InterfaceObjectInfoReport();

    void req();
    std::function<void(Status dm_status)> con;
};

}
