// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectInfoReport/DMP_InterfaceObjectInfoReport_RCl.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_InterfaceObjectInfoReport_RCl::DMP_InterfaceObjectInfoReport_RCl(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_InterfaceObjectInfoReport_RCl::~DMP_InterfaceObjectInfoReport_RCl()
{
}

void DMP_InterfaceObjectInfoReport_RCl::req(const ASAP_Individual mpp_ASAP, const Comm_Mode mpp_comm_mode, const Hop_Count_Type mpp_hop_count_type, const Object_Type mpp_object_type, const Property_Id mpp_PID, const Priority mpp_priority, const Parameter_Test_Info mpp_test_info, const Parameter_Test_Result mpp_test_result)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCl);

    if (state == State::S00) {
        state = State::S01;
        this->mpp_ASAP = mpp_ASAP;
        this->mpp_comm_mode = mpp_comm_mode;
        this->mpp_hop_count_type = mpp_hop_count_type;
        this->mpp_object_type = mpp_object_type;
        this->mpp_PID = mpp_PID;
        this->mpp_priority = mpp_priority;
        this->mpp_test_info = mpp_test_info;
        this->mpp_test_result = mpp_test_result;
        A01_A_Networkparameter_InfoReport_ind();
        restart_timer(ind_timeout);
    }
}

void DMP_InterfaceObjectInfoReport_RCl::A01_A_Networkparameter_InfoReport_ind()
{
    assert(state == State::S01);

    const Parameter_Type parameter_type{mpp_object_type, mpp_PID};
    application_layer.A_NetworkParameter_InfoReport_req(mpp_ASAP, mpp_comm_mode, mpp_hop_count_type, parameter_type, mpp_priority, mpp_test_info, mpp_test_result, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S00;
            con(Status::ok);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_InterfaceObjectInfoReport_RCl::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            // con(Status::not_ok);
            break;
        }
    });
}

}
