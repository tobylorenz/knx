// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_InterfaceObjectInfoReport_RCl
 *
 * @ingroup KNX_03_05_02_03_26_01
 */
class KNX_EXPORT DMP_InterfaceObjectInfoReport_RCl :
    public std::enable_shared_from_this<DMP_InterfaceObjectInfoReport_RCl>
{
public:
    explicit DMP_InterfaceObjectInfoReport_RCl(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_InterfaceObjectInfoReport_RCl();

    void req(const ASAP_Individual mpp_ASAP, const Comm_Mode mpp_comm_mode, const Hop_Count_Type mpp_hop_count_type, const Object_Type mpp_object_type, const Property_Id mpp_PID, const Priority mpp_priority, const Parameter_Test_Info mpp_test_info, const Parameter_Test_Result mpp_test_result);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    ASAP_Individual mpp_ASAP{};
    Comm_Mode mpp_comm_mode{};
    Hop_Count_Type mpp_hop_count_type{};
    Object_Type mpp_object_type{};
    Property_Id mpp_PID{};
    Priority mpp_priority{};
    Parameter_Test_Info mpp_test_info{};
    Parameter_Test_Result mpp_test_result{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_NetworkParameter_InfoReport.req
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Networkparameter_InfoReport_ind();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
