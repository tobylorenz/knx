// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectRead.h>

namespace KNX {

DM_InterfaceObjectRead::DM_InterfaceObjectRead(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_InterfaceObjectRead::~DM_InterfaceObjectRead()
{
}

void DM_InterfaceObjectRead::req(const Flags flags, const uint8_t dataBlockStartAddress, const Object_Type object_type, const Object_Index object_index, const Property_Id PID, const PropertyValue_Start_Index start_index, const PropertyValue_Nr_Of_Elem noElements)
{
}

}
