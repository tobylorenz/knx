// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.24.2 DMP_InterfaceObjectRead_R */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectRead/DMP_InterfaceObjectRead_R.h>

/* 3.24.3 DMP_ReducedInterfaceObjectRead_R */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectRead/DMP_ReducedInterfaceObjectRead_R.h>

namespace KNX {

/**
 * DM_InterfaceObjectRead
 *
 * @ingroup KNX_03_05_02_03_24
 */
class KNX_EXPORT DM_InterfaceObjectRead :
    public std::enable_shared_from_this<DM_InterfaceObjectRead>
{
public:
    explicit DM_InterfaceObjectRead(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_InterfaceObjectRead();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
        bool reserved : 1; // bit 1
        bool address_via_object_index : 1; // bit 2
    };

    void req(const Flags flags, const uint8_t dataBlockStartAddress, const Object_Type object_type, const Object_Index object_index, const Property_Id PID, const PropertyValue_Start_Index start_index, const PropertyValue_Nr_Of_Elem noElements);
    std::function<void(Status dm_status)> con;
};

}
