// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectRead/DMP_InterfaceObjectRead_R.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_InterfaceObjectRead_R::DMP_InterfaceObjectRead_R(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_InterfaceObjectRead_R::~DMP_InterfaceObjectRead_R()
{
}

void DMP_InterfaceObjectRead_R::req(const bool property_known, const Object_Type object_type, const Object_Index object_index, const Property_Id PID, const PropertyValue_Start_Index start_index, const PropertyValue_Nr_Of_Elem noElements)
{
    assert((parameters.communication_mode == DM_Parameters::Communication_Mode::RCo) || (parameters.communication_mode == DM_Parameters::Communication_Mode::RCl));

    if (state == State::S00) {
        this->property_known = property_known;
        this->object_type = object_type;
        this->object_index = object_index;
        this->PID = PID;
        this->start_index = start_index;
        this->noElements = noElements;
        if (!property_known) {
            state = State::S01;
            A01_A_PropertyDescription_Read_req();
            restart_timer(Lcon_timeout);
        } else {
            state = State::S03;
            A03_A_PropertyValue_Read_req();
            restart_timer(Lcon_timeout);
        }
    }
}

void DMP_InterfaceObjectRead_R::A01_A_PropertyDescription_Read_req()
{
    assert(state == State::S01);

    application_layer.A_PropertyDescription_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), object_index, PID, 0, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_PropertyDescription_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(data, Status::not_ok);
            break;
        }
    });
}

void DMP_InterfaceObjectRead_R::A02_A_PropertyDescription_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_PropertyDescription_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const bool write_enable, const Property_Type type, const Max_Nr_Of_Elem max_nr_of_elem, const Access access) -> void {
        if ((asap == parameters.asap()) && (object_index == this->object_index) && (property_id == PID)) {
            state = State::S03;
            A03_A_PropertyValue_Read_req();
            restart_timer(Lcon_timeout);
        }
        if (state == State::S02) {
            A02_A_PropertyDescription_Read_Acon();
            // do not restart timer
        }
    });
}

void DMP_InterfaceObjectRead_R::A03_A_PropertyValue_Read_req()
{
    assert(state == State::S03);

    application_layer.A_PropertyValue_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), object_index, PID, noElements, start_index, data, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S04;
            A04_A_PropertyValue_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(data, Status::not_ok);
            break;
        }
    });
}

void DMP_InterfaceObjectRead_R::A04_A_PropertyValue_Read_Acon()
{
    assert(state == State::S04);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == parameters.asap()) && (object_index == this->object_index) && (property_id == PID) && (nr_of_elem == noElements) && (start_index == this->start_index)) {
            state = State::S00;
            this->data = data;
            con(data, Status::ok);
        }
        if (state == State::S04) {
            A04_A_PropertyValue_Read_Acon();
            // do not restart timer
        }
    });
}

void DMP_InterfaceObjectRead_R::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            // con(Status::not_ok);
            break;
        }
    });
}

}
