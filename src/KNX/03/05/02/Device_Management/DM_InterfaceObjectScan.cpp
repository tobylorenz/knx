// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectScan.h>

namespace KNX {

DM_InterfaceObjectScan::DM_InterfaceObjectScan(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_InterfaceObjectScan::~DM_InterfaceObjectScan()
{
}

void DM_InterfaceObjectScan::req(const Flags flags, const uint8_t dataBlockStartAddress, const Object_Index object_index, const uint8_t data)
{
}

}
