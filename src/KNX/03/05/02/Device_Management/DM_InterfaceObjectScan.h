// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.25.2 DMP_InterfaceObjectScan_R */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectScan/DMP_InterfaceObjectScan_R.h>

/* 3.25.3 DMP_ReducedInterfaceObjectScan_R */
#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectScan/DMP_ReducedInterfaceObjectScan_R.h>
    
namespace KNX {  

/**
 * DM_InterfaceObjectScan
 *
 * @ingroup KNX_03_05_02_03_25
 */
class KNX_EXPORT DM_InterfaceObjectScan :
    public std::enable_shared_from_this<DM_InterfaceObjectScan>
{
public:
    explicit DM_InterfaceObjectScan(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_InterfaceObjectScan();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
        bool reserved : 1; // bit 1
        bool scan_all_properties : 1; // bit 2
        bool scan_all_interface_objects : 1; // bit 3
    };

    void req(const Flags flags, const uint8_t dataBlockStartAddress, const Object_Index object_index, const uint8_t data);
    std::function<void(Status dm_status)> con;
};

}
