// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_InterfaceObjectScan_R
 *
 * @ingroup KNX_03_05_02_03_25_02
 */
class KNX_EXPORT DMP_InterfaceObjectScan_R :
    public std::enable_shared_from_this<DMP_InterfaceObjectScan_R>
{
public:
    explicit DMP_InterfaceObjectScan_R(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_InterfaceObjectScan_R();

    void req(const Object_Index object_index = 0, const bool interface_object_scan = true, const bool property_scan = true);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    Object_Index object_index{};
    bool interface_object_scan{};
    bool property_scan{};

    /* Service Variables */
    Property_Index property_index{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_PropertyDescription_Read.req
        S02, ///< waiting A_PropertyDescription_Read.Acon
        S03, ///< sending A_PropertyValue_Read.req
        S04, ///< waiting A_PropertyValue_Read.Acon
        S05, ///< sending A_PropertyDescription_Read.req
        S06, ///< waiting A_PropertyDescription_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_PropertyDescription_Read_req();
    void A02_A_PropertyDescription_Read_Acon();
    void A03_A_PropertyValue_Read_req();
    void A04_A_PropertyValue_Read_Acon();
    void A05_A_PropertyDescription_Read_req();
    void A06_A_PropertyDescription_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
