// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_InterfaceObjectScan/DMP_ReducedInterfaceObjectScan_R.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_ReducedInterfaceObjectScan_R::DMP_ReducedInterfaceObjectScan_R(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_ReducedInterfaceObjectScan_R::~DMP_ReducedInterfaceObjectScan_R()
{
}

void DMP_ReducedInterfaceObjectScan_R::req(const Object_Index object_index, const bool interface_object_scan)
{
    assert((parameters.communication_mode == DM_Parameters::Communication_Mode::RCo) || (parameters.communication_mode == DM_Parameters::Communication_Mode::RCl));

    if (state == State::S00) {
        this->object_index = object_index;
        this->interface_object_scan = interface_object_scan;
        if (interface_object_scan) {
            state = State::S01;
            A01_A_PropertyValue_Read_req();
            restart_timer(Lcon_timeout);
        } else {
            state = State::S00;
            con(Status::ok);
        }
    }
}

void DMP_ReducedInterfaceObjectScan_R::A01_A_PropertyValue_Read_req()
{
    assert(state == State::S01);

    application_layer.A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), this->object_index, 1, 1, 1, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_PropertyValue_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_ReducedInterfaceObjectScan_R::A02_A_PropertyValue_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == parameters.asap()) && (object_index == this->object_index) && (property_id == 1) && (start_index == 1) && (nr_of_elem == 1)) {
            if (property_id != 0) {
                state = State::S01;
                this->object_index++;
                A01_A_PropertyValue_Read_req();
                restart_timer(Lcon_timeout);
            } else {
                state = State::S00;
                con(Status::ok);
            }
        }
        if (state == State::S02) {
            A02_A_PropertyValue_Read_Acon();
            // do not restart timer
        }
    });
}

void DMP_ReducedInterfaceObjectScan_R::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            // con(Status::not_ok);
            break;
        }
    });
}

}
