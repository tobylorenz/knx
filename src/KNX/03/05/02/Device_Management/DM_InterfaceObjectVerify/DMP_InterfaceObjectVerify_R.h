// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_InterfaceObjectVerify_R
 *
 * @ingroup KNX_03_05_02_03_23_02
 */
class KNX_EXPORT DMP_InterfaceObjectVerify_R :
    public std::enable_shared_from_this<DMP_InterfaceObjectVerify_R>
{
public:
    explicit DMP_InterfaceObjectVerify_R(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_InterfaceObjectVerify_R();

    void req(const bool property_known, const Object_Type object_type, const Object_Index object_index, const Property_Id PID, const PropertyValue_Start_Index start_index, const PropertyValue_Nr_Of_Elem noElements, const Property_Value data);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    bool property_known{};
    Object_Type object_type{};
    Object_Index object_index{};
    Property_Id PID{};
    PropertyValue_Start_Index start_index{};
    PropertyValue_Nr_Of_Elem noElements{};
    Property_Value data{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_PropertyDescription_Read.req
        S02, ///< waiting A_PropertyDescription_Read.Acon
        S03, ///< sending A_PropertyValue_Read.req
        S04, ///< waiting A_PropertyValue_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_PropertyDescription_Read_req();
    void A02_A_PropertyDescription_Read_Acon();
    void A03_A_PropertyValue_Read_req();
    void A04_A_PropertyValue_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
