// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LCExtMemOpen.h>

namespace KNX {

DM_LCExtMemOpen::DM_LCExtMemOpen(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_LCExtMemOpen::~DM_LCExtMemOpen()
{
}

void DM_LCExtMemOpen::req()
{
}

}
