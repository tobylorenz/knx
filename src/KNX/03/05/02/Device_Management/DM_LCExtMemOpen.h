// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.41.2 DMP_LCExtMemOpen_RCo */
#include <KNX/03/05/02/Device_Management/DM_LCExtMemOpen/DMP_LCExtMemOpen_RCo.h>

namespace KNX {

/**
 * DM_LCExtMemOpen
 *
 * @ingroup KNX_03_05_02_03_41
 */
class KNX_EXPORT DM_LCExtMemOpen :
    public std::enable_shared_from_this<DM_LCExtMemOpen>
{
public:
    explicit DM_LCExtMemOpen(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_LCExtMemOpen();

    void req();
    std::function<void(Status dm_status)> con;
};

}
