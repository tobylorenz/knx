// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LCExtMemOpen/DMP_LCExtMemOpen_RCo.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_LCExtMemOpen_RCo::DMP_LCExtMemOpen_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer)
{
}

DMP_LCExtMemOpen_RCo::~DMP_LCExtMemOpen_RCo()
{
}

void DMP_LCExtMemOpen_RCo::req()
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCo);

    if (state == State::S00) {
        state = State::S01;
        A01_A_Open_Routing_Table_req();
    }
}

void DMP_LCExtMemOpen_RCo::A01_A_Open_Routing_Table_req()
{
    assert(state == State::S01);

    application_layer.A_Open_Routing_Table_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S00;
            con(Status::ok);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

}
