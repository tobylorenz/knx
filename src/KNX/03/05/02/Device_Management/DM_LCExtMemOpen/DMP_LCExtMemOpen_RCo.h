// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_LCExtMemOpen_RCo
 *
 * @ingroup KNX_03_05_02_03_41_02
 */
class KNX_EXPORT DMP_LCExtMemOpen_RCo :
    public std::enable_shared_from_this<DMP_LCExtMemOpen_RCo>
{
public:
    explicit DMP_LCExtMemOpen_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_LCExtMemOpen_RCo();

    void req();
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Open_Routing_Table.req
    };
    State state { State::S00 };

    /* actions */
    void A01_A_Open_Routing_Table_req();
};

}
