// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.40.2 DMP_LCExtMemRead_RCo */
#include <KNX/03/05/02/Device_Management/DM_LCExtMemRead/DMP_LCExtMemRead_RCo.h>

namespace KNX {

/**
 * DM_LCExtMemRead
 *
 * @ingroup KNX_03_05_02_03_40
 */
class KNX_EXPORT DM_LCExtMemRead :
    public std::enable_shared_from_this<DM_LCExtMemRead>
{
public:
    explicit DM_LCExtMemRead(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_LCExtMemRead();

    struct Flags {
        bool location_of_data_in_data_block : 1; // bit 0
    };

    void req(const Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data);
    std::function<void(Status dm_status)> con;
};

}
