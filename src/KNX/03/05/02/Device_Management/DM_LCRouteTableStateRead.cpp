// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LCRouteTableStateRead.h>

namespace KNX {

DM_LCRouteTableStateRead::DM_LCRouteTableStateRead(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_LCRouteTableStateRead::~DM_LCRouteTableStateRead()
{
}

void DM_LCRouteTableStateRead::req(const Flags flags, const uint8_t routeTableState)
{
}

}
