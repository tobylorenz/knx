// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.44.2 DMP_LCRouteTableStateRead_RCo */
#include <KNX/03/05/02/Device_Management/DM_LCRouteTableStateRead/DMP_LCRouteTableStateRead_RCo.h>

namespace KNX {

/**
 * DM_LCRouteTableStateRead
 *
 * @ingroup KNX_03_05_02_03_44
 */
class KNX_EXPORT DM_LCRouteTableStateRead :
    public std::enable_shared_from_this<DM_LCRouteTableStateRead>
{
public:
    explicit DM_LCRouteTableStateRead(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_LCRouteTableStateRead();

    struct Flags {
        bool location_of_data_in_data_block : 1; // bit 0
    };

    void req(const Flags flags, const uint8_t routeTableState);
    std::function<void(Status dm_status)> con;
};

}
