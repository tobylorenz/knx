// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LCRouteTableStateVerify.h>

namespace KNX {

DM_LCRouteTableStateVerify::DM_LCRouteTableStateVerify(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_LCRouteTableStateVerify::~DM_LCRouteTableStateVerify()
{
}

void DM_LCRouteTableStateVerify::req(const Flags flags, const uint8_t routeTableState)
{
}

}
