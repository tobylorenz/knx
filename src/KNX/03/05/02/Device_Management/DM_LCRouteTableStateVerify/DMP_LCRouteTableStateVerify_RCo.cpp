// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LCRouteTableStateVerify/DMP_LCRouteTableStateVerify_RCo.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_LCRouteTableStateVerify_RCo::DMP_LCRouteTableStateVerify_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_LCRouteTableStateVerify_RCo::~DMP_LCRouteTableStateVerify_RCo()
{
}

void DMP_LCRouteTableStateVerify_RCo::req(const uint8_t route_table_state)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCo);

    if (state == State::S00) {
        state = State::S01;
        parameters.route_table_state = route_table_state;
        A01_A_Read_Router_Status_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_LCRouteTableStateVerify_RCo::A01_A_Read_Router_Status_req()
{
    assert(state == State::S01);

    application_layer.A_Read_Router_Status_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_Read_Router_Status_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(parameters.route_table_state, Status::not_ok);
            break;
        }
    });
}

void DMP_LCRouteTableStateVerify_RCo::A02_A_Read_Router_Status_Acon()
{
    assert(state == State::S02);

    application_layer.A_Read_Router_Status_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t route_table_state) -> void {
        if (asap == parameters.asap()) {
            if (route_table_state == parameters.route_table_state) {
                state = State::S00;
                con(parameters.route_table_state, Status::ok);
            } else {
                state = State::S00;
                parameters.route_table_state = route_table_state;
                con(parameters.route_table_state, Status::not_ok);
            }
        }
        if (state == State::S02) {
            A02_A_Read_Router_Status_Acon();
            // do not restart timer
        }
    });
}

void DMP_LCRouteTableStateVerify_RCo::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(parameters.route_table_state, Status::not_ok);
            break;
        }
    });
}

}
