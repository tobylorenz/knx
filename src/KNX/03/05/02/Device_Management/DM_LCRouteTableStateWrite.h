// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.42.2 DMP_LCRouteTableStateWrite_RCo */
#include <KNX/03/05/02/Device_Management/DM_LCRouteTableStateWrite/DMP_LCRouteTableStateWrite_RCo.h>

namespace KNX {

/**
 * DM_LCRouteTableStateWrite
 *
 * @ingroup KNX_03_05_02_03_42
 */
class KNX_EXPORT DM_LCRouteTableStateWrite :
    public std::enable_shared_from_this<DM_LCRouteTableStateWrite>
{
public:
    explicit DM_LCRouteTableStateWrite(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_LCRouteTableStateWrite();

    struct Flags {
        bool location_of_data_in_data_block : 1; // bit 0
        bool verify_enabled : 1; // bit 1
    };
    
    void req(const Flags flags, const uint8_t routeTableState);
    std::function<void(Status dm_status)> con;
};

}
