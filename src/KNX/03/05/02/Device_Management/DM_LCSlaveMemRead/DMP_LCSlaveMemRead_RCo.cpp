// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LCSlaveMemRead/DMP_LCSlaveMemRead_RCo.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_LCSlaveMemRead_RCo::DMP_LCSlaveMemRead_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_LCSlaveMemRead_RCo::~DMP_LCSlaveMemRead_RCo()
{
}

void DMP_LCSlaveMemRead_RCo::req(const uint16_t deviceStartAddress, const uint16_t deviceEndAddress)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCo);

    if (state == State::S00) {
        this->deviceStartAddress = deviceStartAddress;
        this->deviceEndAddress = deviceEndAddress;
        data.clear();
        current_address = this->deviceStartAddress;
        current_count = (deviceEndAddress - current_address);
        if (current_count > 11) {
            current_count = 11;
        }
        if (current_count > 0) {
            state = State::S01;
            A01_A_Read_Router_Memory_req();
            restart_timer(Lcon_timeout);
        } else {
            con(data, Status::ok);
        }
    }
}

void DMP_LCSlaveMemRead_RCo::A01_A_Read_Router_Memory_req()
{
    assert(state == State::S01);

    application_layer.A_Read_Router_Memory_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), current_count, current_address, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_Read_Router_Memory_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(data, Status::not_ok);
            break;
        }
    });
}

void DMP_LCSlaveMemRead_RCo::A02_A_Read_Router_Memory_Acon()
{
    assert(state == State::S02);

    application_layer.A_Read_Router_Memory_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data) -> void {
        if ((asap == parameters.asap()) && (count == current_count) && (address == current_address)) {
            this->data.insert(std::end(this->data), std::cbegin(data), std::cend(data));
            current_address = current_address + current_count;
            current_count = (deviceEndAddress - current_address);
            if (current_count > 11) {
                current_count = 11;
            }
            if (current_count > 0) {
                state = State::S01;
                A01_A_Read_Router_Memory_req();
                restart_timer(Lcon_timeout);
            } else {
                state = State::S00;
                con(data, Status::ok);
            }
        }
        if (state == State::S02) {
            A02_A_Read_Router_Memory_Acon();
            // do not restart timer
        }
    });
}

void DMP_LCSlaveMemRead_RCo::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(data, Status::not_ok);
            break;
        }
    });
}

}
