// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_LCSlaveMemRead_RCo
 *
 * @ingroup KNX_03_05_02_03_37_02
 */
class KNX_EXPORT DMP_LCSlaveMemRead_RCo :
    public std::enable_shared_from_this<DMP_LCSlaveMemRead_RCo>
{
public:
    explicit DMP_LCSlaveMemRead_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_LCSlaveMemRead_RCo();

    void req(const uint16_t deviceStartAddress, const uint16_t deviceEndAddress);
    std::function<void(std::vector<uint8_t> data, const Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    uint16_t deviceStartAddress{}; // in
    uint16_t deviceEndAddress{}; // in
    std::vector<uint8_t> data{}; // out

    /** Variables */
    uint8_t current_count{};
    uint16_t current_address{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Read_Router_Memory.req
        S02, ///< waiting A_Read_Router_Memory.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Read_Router_Memory_req();
    void A02_A_Read_Router_Memory_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
