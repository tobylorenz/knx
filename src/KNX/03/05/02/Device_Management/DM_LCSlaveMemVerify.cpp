// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LCSlaveMemVerify.h>

namespace KNX {

DM_LCSlaveMemVerify::DM_LCSlaveMemVerify(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_LCSlaveMemVerify::~DM_LCSlaveMemVerify()
{
}

void DM_LCSlaveMemVerify::req(const Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data)
{
}

}
