// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LCSlaveMemWrite.h>

namespace KNX {

DM_LCSlaveMemWrite::DM_LCSlaveMemWrite(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_LCSlaveMemWrite::~DM_LCSlaveMemWrite()
{
}

void DM_LCSlaveMemWrite::req(const Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data)
{
}

}
