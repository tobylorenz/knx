// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.35.2 DMP_LCSlaveMemWrite_RCo */
#include <KNX/03/05/02/Device_Management/DM_LCSlaveMemWrite/DMP_LCSlaveMemWrite_RCo.h>

namespace KNX {

/**
 * DM_LCSlaveMemWrite
 *
 * @ingroup KNX_03_05_02_03_35
 */
class KNX_EXPORT DM_LCSlaveMemWrite :
    public std::enable_shared_from_this<DM_LCSlaveMemWrite>
{
public:
    explicit DM_LCSlaveMemWrite(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_LCSlaveMemWrite();

    struct Flags {
        bool location_of_data_in_data_block : 1; // bit 0
        bool verify_enabled : 1; // bit 1
    };

    void req(const Flags flags, const uint8_t dataBlockStartAddress, const uint8_t deviceStartAddress, const uint8_t deviceEndAddress, const uint8_t data);
    std::function<void(Status dm_status)> con;
};

}
