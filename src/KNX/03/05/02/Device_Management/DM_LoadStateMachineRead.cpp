// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LoadStateMachineRead.h>

namespace KNX {

DM_LoadStateMachineRead::DM_LoadStateMachineRead(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_LoadStateMachineRead::~DM_LoadStateMachineRead()
{
}

void DM_LoadStateMachineRead::req(const Individual_Address destination_address, const Flags flags, const Object_Type state_machine_type, const Object_Index state_machine_nr)
{
}

}
