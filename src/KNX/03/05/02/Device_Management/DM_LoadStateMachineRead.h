// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.30.2 DMP_LoadStateMachineRead_RCo_Mem */
#include <KNX/03/05/02/Device_Management/DM_LoadStateMachineRead/DMP_LoadStateMachineRead_RCo_Mem.h>

/* 3.30.3 DMP_LoadStateMachineRead_R_IO */
#include <KNX/03/05/02/Device_Management/DM_LoadStateMachineRead/DMP_LoadStateMachineRead_R_IO.h>

namespace KNX {

/**
 * DM_LoadStateMachineRead
 *
 * @ingroup KNX_03_05_02_03_30
 */
class KNX_EXPORT DM_LoadStateMachineRead :
    public std::enable_shared_from_this<DM_LoadStateMachineRead>
{
public:
    explicit DM_LoadStateMachineRead(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_LoadStateMachineRead();

    struct Flags {
        bool location_of_data_in_data_block : 1; // bit 0
    };
    
    void req(const Individual_Address destination_address, const Flags flags, const Object_Type state_machine_type, const Object_Index state_machine_nr);
    std::function<void(Status dm_status)> con;
};

}
