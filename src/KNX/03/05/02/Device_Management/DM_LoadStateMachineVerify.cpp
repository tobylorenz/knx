// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LoadStateMachineVerify.h>

namespace KNX {

DM_LoadStateMachineVerify::DM_LoadStateMachineVerify(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_LoadStateMachineVerify::~DM_LoadStateMachineVerify()
{
}

void DM_LoadStateMachineVerify::req(const Flags flags, const Object_Type stateMachineType, const Object_Index stateMachineNr, const uint8_t state)
{
}

}
