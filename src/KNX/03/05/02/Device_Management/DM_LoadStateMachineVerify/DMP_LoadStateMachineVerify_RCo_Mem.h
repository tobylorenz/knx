// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/01/Load_State_Machine.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DM_LoadStateMachineVerify_RCo_Mem
 *
 * @ingroup KNX_03_05_02_03_29_02
 */
class KNX_EXPORT DMP_LoadStateMachineVerify_RCo_Mem :
    public std::enable_shared_from_this<DMP_LoadStateMachineVerify_RCo_Mem>
{
public:
    explicit DMP_LoadStateMachineVerify_RCo_Mem(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_LoadStateMachineVerify_RCo_Mem();

    void req(const Object_Type stateMachineType, const Object_Index stateMachineNr, const Load_State_Machine::State loadstate);
    std::function<void(Load_State_Machine::State loadstate, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    Object_Type stateMachineType{};
    Object_Index stateMachineNr{};
    Load_State_Machine::State loadstate{}; // out

    /* Service Data */
    Memory_Address address{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Memory_Read.req
        S02, ///< waiting A_Memory_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Memory_Read_req();
    void A02_A_Memory_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
