// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LoadStateMachineWrite.h>

namespace KNX {

DM_LoadStateMachineWrite::DM_LoadStateMachineWrite(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_LoadStateMachineWrite::~DM_LoadStateMachineWrite()
{
}

void DM_LoadStateMachineWrite::req(const Flags flags, const Object_Type stateMachineType,  const Object_Index stateMachineNr, const uint8_t event, const uint8_t eventData)
{
}

}
