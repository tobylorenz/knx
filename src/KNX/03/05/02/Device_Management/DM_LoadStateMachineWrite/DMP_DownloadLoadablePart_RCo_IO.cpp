// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_LoadStateMachineWrite/DMP_DownloadLoadablePart_RCo_IO.h>

#include <KNX/03/05/01/Load_State_Machine.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_DownloadLoadablePart_RCo_IO::DMP_DownloadLoadablePart_RCo_IO(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_DownloadLoadablePart_RCo_IO::~DMP_DownloadLoadablePart_RCo_IO()
{
}

void DMP_DownloadLoadablePart_RCo_IO::req(const Object_Index object_index, const std::vector<Property_Value> additional_load_controls)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCo);

    if (state == State::S00) {
        state = State::S01;
        this->object_index = object_index;
        this->additional_load_controls = additional_load_controls;
        additional_load_control = std::cbegin(this->additional_load_controls);
        A01_A_PropertyValue_Write_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_DownloadLoadablePart_RCo_IO::A01_A_PropertyValue_Write_req()
{
    assert(state == State::S01);

    const Property_Value data{0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; // Unload
    application_layer.A_PropertyValue_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), object_index, Interface_Object::PID_LOAD_STATE_CONTROL, 1, 1, data, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S03;
            A03_A_PropertyValue_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_DownloadLoadablePart_RCo_IO::A02_A_PropertyValue_Read_req()
{
    assert(state == State::S02);

    application_layer.A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), object_index, Interface_Object::PID_LOAD_STATE_CONTROL, 1, 1, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S03;
            A03_A_PropertyValue_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_DownloadLoadablePart_RCo_IO::A03_A_PropertyValue_Read_Acon()
{
    assert(state == State::S03);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == parameters.asap()) && (object_index == this->object_index) && (property_id == Interface_Object::PID_LOAD_STATE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
            const Load_State_Machine::State loadstate = static_cast<Load_State_Machine::State>(data[0]);
            switch(loadstate) {
            case Load_State_Machine::State::Unloaded:
                state = State::S04;
                A04_A_PropertyValue_Write_req();
                restart_timer(Lcon_timeout);
                break;
            case Load_State_Machine::State::Unloading:
                state = State::S03;
                A02_A_PropertyValue_Read_req();
                restart_timer(Lcon_timeout);
            default:
                state = State::S00;
                con(Status::not_ok);
            }
        }
        if (state == State::S03) {
            A03_A_PropertyValue_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_DownloadLoadablePart_RCo_IO::A04_A_PropertyValue_Write_req()
{
    assert(state == State::S04);

    const Property_Value data{0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; // Load
    application_layer.A_PropertyValue_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), object_index, Interface_Object::PID_LOAD_STATE_CONTROL, 1, 1, data, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S05;
            A05_A_PropertyValue_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_DownloadLoadablePart_RCo_IO::A05_A_PropertyValue_Read_Acon()
{
    assert(state == State::S05);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == parameters.asap()) && (object_index == this->object_index) && (property_id == Interface_Object::PID_LOAD_STATE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
            const Load_State_Machine::Event loadevent = static_cast<Load_State_Machine::Event>(data[0]);
            switch(loadevent) {
            case Load_State_Machine::Event::Load_Completed:
                if (additional_load_control != std::cend(additional_load_controls)) {
                    state = State::S06;
                    A06_A_PropertyValue_Write_req();
                    restart_timer(Lcon_timeout);
                } else {
                    state = State::S08;
                    A06_A_PropertyValue_Write_req();
                    restart_timer(Lcon_timeout);
                }
                break;
            default:
                state = State::S00;
                con(Status::not_ok);
            }
        }
        if (state == State::S05) {
            A05_A_PropertyValue_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_DownloadLoadablePart_RCo_IO::A06_A_PropertyValue_Write_req()
{
    assert(state == State::S06);

    application_layer.A_PropertyValue_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), object_index, Interface_Object::PID_LOAD_STATE_CONTROL, 1, 1, *additional_load_control, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S07;
            A07_A_PropertyValue_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_DownloadLoadablePart_RCo_IO::A07_A_PropertyValue_Read_Acon()
{
    assert(state == State::S07);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == parameters.asap()) && (object_index == this->object_index) && (property_id == Interface_Object::PID_LOAD_STATE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
            const Load_State_Machine::Event loadevent = static_cast<Load_State_Machine::Event>(data[0]);
            switch(loadevent) {
            case Load_State_Machine::Event::Load_Completed:
                additional_load_control++;
                if (additional_load_control != std::cend(additional_load_controls)) {
                    state = State::S06;
                    A06_A_PropertyValue_Write_req();
                    restart_timer(Lcon_timeout);
                } else {
                    state = State::S08;
                    A06_A_PropertyValue_Write_req();
                    restart_timer(Lcon_timeout);
                }
                break;
            default:
                state = State::S00;
                con(Status::not_ok);
            }
        }
        if (state == State::S07) {
            A07_A_PropertyValue_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_DownloadLoadablePart_RCo_IO::A08_A_PropertyValue_Write_req()
{
    assert(state == State::S08);

    const Property_Value data{0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}; // LoadComplete
    application_layer.A_PropertyValue_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), object_index, Interface_Object::PID_LOAD_STATE_CONTROL, 1, 1, data, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S09;
            A09_A_PropertyValue_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_DownloadLoadablePart_RCo_IO::A09_A_PropertyValue_Read_Acon()
{
    assert(state == State::S09);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == parameters.asap()) && (object_index == this->object_index) && (property_id == Interface_Object::PID_LOAD_STATE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
            const Load_State_Machine::State loadstate = static_cast<Load_State_Machine::State>(data[0]);
            switch(loadstate) {
            case Load_State_Machine::State::Loaded:
                state = State::S00;
                con(Status::ok);
                break;
            default:
                state = State::S00;
                con(Status::not_ok);
            }
        }
        if (state == State::S09) {
            A09_A_PropertyValue_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_DownloadLoadablePart_RCo_IO::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

}
