// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_DownloadLoadablePart_RCo_IO
 *
 * @ingroup KNX_03_05_02_03_28_04
 */
class KNX_EXPORT DMP_DownloadLoadablePart_RCo_IO :
    public std::enable_shared_from_this<DMP_DownloadLoadablePart_RCo_IO>
{
public:
    explicit DMP_DownloadLoadablePart_RCo_IO(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_DownloadLoadablePart_RCo_IO();

    void req(const Object_Index object_index, const std::vector<Property_Value> additional_load_controls);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    Object_Index object_index{};
    std::vector<Property_Value> additional_load_controls{};

    /** Service Data */
    std::vector<Property_Value>::const_iterator additional_load_control;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_PropertyValue_Write.req
        S02, ///< sending A_PropertyValue_Read.req
        S03, ///< waiting A_PropertyValue_Read.Acon
        S04, ///< sending A_PropertyValue_Write.req
        S05, ///< waiting A_PropertyValue_Read.Acon
        S06, ///< sending A_PropertyValue_Write.req
        S07, ///< waiting A_PropertyValue_Read.Acon
        S08, ///< sending A_PropertyValue_Write.req
        S09, ///< waiting A_PropertyValue_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_PropertyValue_Write_req();
    void A02_A_PropertyValue_Read_req();
    void A03_A_PropertyValue_Read_Acon();
    void A04_A_PropertyValue_Write_req();
    void A05_A_PropertyValue_Read_Acon();
    void A06_A_PropertyValue_Write_req();
    void A07_A_PropertyValue_Read_Acon();
    void A08_A_PropertyValue_Write_req();
    void A09_A_PropertyValue_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
