// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_MemRead.h>

namespace KNX {

DM_MemRead::DM_MemRead(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_MemRead::~DM_MemRead()
{
}

void DM_MemRead::req(const Flags flags, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress)
{
}

}
