// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.18.2 DMP_MemRead_RCo */
#include <KNX/03/05/02/Device_Management/DM_MemRead/DMP_MemRead_RCo.h>

/* 3.18.3 DMP_MemRead_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_MemRead/DMP_MemRead_LEmi1.h>

namespace KNX {

/**
 * DM_MemRead
 *
 * @ingroup KNX_03_05_02_03_18
 */
class KNX_EXPORT DM_MemRead :
    public std::enable_shared_from_this<DM_MemRead>
{
public:
    explicit DM_MemRead(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_MemRead();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
    };

    void req(const Flags flags, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress);
    std::function<void(Status dm_status)> con;
};

}
