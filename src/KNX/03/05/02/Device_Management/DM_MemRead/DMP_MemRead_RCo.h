// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_MemRead_RCo
 *
 * @ingroup KNX_03_05_02_03_18_02
 */
class KNX_EXPORT DMP_MemRead_RCo :
    public std::enable_shared_from_this<DMP_MemRead_RCo>
{
public:
    explicit DMP_MemRead_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_MemRead_RCo();

    void req(const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress);
    std::function<void(const Memory_Data data, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    Memory_Address deviceStartAddress{};
    Memory_Address deviceEndAddress{};
    Memory_Data data{};

    /* Service parameters */
    Memory_Number serviceDBLen{};
    Memory_Address serviceDBAddr{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Memory_Read.req
        S02, ///< waiting A_Memory_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Memory_Read_req();
    void A02_A_Memory_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
