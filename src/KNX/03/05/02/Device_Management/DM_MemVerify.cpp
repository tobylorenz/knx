// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_MemVerify.h>

namespace KNX {

DM_MemVerify::DM_MemVerify(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_MemVerify::~DM_MemVerify()
{
}

void DM_MemVerify::req(const Flags flags, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
}

}
