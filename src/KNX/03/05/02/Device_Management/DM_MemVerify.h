// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.17.2 DMP_MemVerify_RCo */
#include <KNX/03/05/02/Device_Management/DM_MemVerify/DMP_MemVerify_RCo.h>

/* 3.17.3 DMP_MemVerify_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_MemVerify/DMP_MemVerify_LEmi1.h>

namespace KNX {

/**
 * DM_MemVerify
 *
 * @ingroup KNX_03_05_02_03_17
 */
class KNX_EXPORT DM_MemVerify :
    public std::enable_shared_from_this<DM_MemVerify>
{
public:
    explicit DM_MemVerify(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_MemVerify();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
    };

    void req(const Flags flags, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data);
    std::function<void(Status dm_status)> con;
};

}
