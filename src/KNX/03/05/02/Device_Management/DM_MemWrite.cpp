// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_MemWrite.h>

namespace KNX {

DM_MemWrite::DM_MemWrite(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_MemWrite::~DM_MemWrite()
{
}

void DM_MemWrite::req(const Flags flags, const uint8_t dataBlockStartAddress, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
}

}
