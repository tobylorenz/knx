// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.16.2 DMP_MemWrite_RCo */
#include <KNX/03/05/02/Device_Management/DM_MemWrite/DMP_MemWrite_RCo.h>

/* 3.16.3 DMP_MemWrite_RCoV */
#include <KNX/03/05/02/Device_Management/DM_MemWrite/DMP_MemWrite_RCoV.h>

/* 3.16.4 DMP_MemWrite_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_MemWrite/DMP_MemWrite_LEmi1.h>

namespace KNX {

/**
 * DM_MemWrite
 *
 * @ingroup KNX_03_05_02_03_16
 */
class KNX_EXPORT DM_MemWrite :
    public std::enable_shared_from_this<DM_MemWrite>
{
public:
    explicit DM_MemWrite(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_MemWrite();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
        bool verify_enabled : 1; // bit 1
    };

    void req(const Flags flags, const uint8_t dataBlockStartAddress, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data);
    std::function<void(Status dm_status)> con;
};

}
