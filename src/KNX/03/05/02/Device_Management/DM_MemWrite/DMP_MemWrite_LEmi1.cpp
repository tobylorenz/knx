// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_MemWrite/DMP_MemWrite_LEmi1.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_MemWrite_LEmi1::DMP_MemWrite_LEmi1(asio::io_context & io_context, EMI_User_Layer & user_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    user_layer(user_layer),
    timeout_timer(io_context)
{
}

DMP_MemWrite_LEmi1::~DMP_MemWrite_LEmi1()
{
}

void DMP_MemWrite_LEmi1::req(const bool verify, const Memory_Address DmpStartAddr, const Memory_Address DmpEndAddr, const Memory_Data DmpData)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::LEmi1);

    if (state == State::S00) {
        state = State::S01;
        this->verify = verify;
        this->DmpStartAddr = DmpStartAddr;
        this->DmpEndAddr = DmpEndAddr;
        this->DmpData = DmpData;
        SrvDBAddr = DmpStartAddr;
        SrvDBLen = std::min(12, DmpEndAddr - SrvDBAddr);
        if (SrvDBLen > 0) {
            SrvDataIn.assign(std::cbegin(DmpData) + (SrvDBAddr - DmpStartAddr), std::cend(DmpData) + (SrvDBAddr - DmpStartAddr) + SrvDBLen);
            A01_PC_Set_Value_req();
            restart_timer(Lcon_timeout);
        } else {
            state = State::S00;
            con(Status::ok);
        }
    }
}

void DMP_MemWrite_LEmi1::A01_PC_Set_Value_req()
{
    assert(state == State::S01);

    user_layer.PC_Set_Value_req(SrvDBLen, SrvDBAddr, SrvDataIn);

    if (verify) {
        state = State::S02;
        A02_PC_Get_Value_req();
        restart_timer(Lcon_timeout);
    } else {
        SrvDBAddr += SrvDBLen;
        SrvDBLen = std::min(12, DmpEndAddr - SrvDBAddr);
        if (SrvDBLen > 0) {
            SrvDataIn.assign(std::cbegin(DmpData) + (SrvDBAddr - DmpStartAddr), std::cend(DmpData) + (SrvDBAddr - DmpStartAddr) + SrvDBLen);
            A01_PC_Set_Value_req();
            restart_timer(Lcon_timeout);
        } else {
            state = State::S00;
            con(Status::ok);
        }
    }
}

void DMP_MemWrite_LEmi1::A02_PC_Get_Value_req()
{
    assert(state == State::S02);

    user_layer.PC_Get_Value_req(SrvDBLen, SrvDBAddr, [this](const std::vector<uint8_t> data, const Status u_status) -> void {
        switch(u_status) {
        case Status::ok:
            state = State::S01;
            SrvDataOut = data;
            if (SrvDataIn == SrvDataOut) {
                SrvDBAddr += SrvDBLen;
                SrvDBLen = std::min(12, DmpEndAddr - SrvDBAddr);
                if (SrvDBLen > 0) {
                    SrvDataIn.assign(std::cbegin(DmpData) + (SrvDBAddr - DmpStartAddr), std::cend(DmpData) + (SrvDBAddr - DmpStartAddr) + SrvDBLen);
                    A01_PC_Set_Value_req();
                    restart_timer(Lcon_timeout);
                } else {
                    state = State::S00;
                    con(Status::ok);
                }
            } else {
                state = State::S00;
                con(Status::not_ok);
            }
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_MemWrite_LEmi1::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

}
