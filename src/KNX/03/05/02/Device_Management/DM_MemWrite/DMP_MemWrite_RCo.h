// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_MemWrite_RCo
 *
 * @ingroup KNX_03_05_02_03_16_02
 */
class KNX_EXPORT DMP_MemWrite_RCo :
    public std::enable_shared_from_this<DMP_MemWrite_RCo>
{
public:
    explicit DMP_MemWrite_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_MemWrite_RCo();

    void req(const bool verify, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    bool verify{};
    Memory_Address deviceStartAddress{};
    Memory_Address deviceEndAddress{};
    Memory_Data data{};

    /* Service parameters */
    Memory_Data serviceDataIn{};
    Memory_Data serviceDataOut{};
    Memory_Number serviceDBLen{};
    Memory_Address serviceDBAddr{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Memory_Write.req
        S02, ///< sending A_Memory_Read.req
        S03, ///< waiting A_Memory_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Memory_Write_req();
    void A02_A_Memory_Read_req();
    void A03_A_Memory_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
