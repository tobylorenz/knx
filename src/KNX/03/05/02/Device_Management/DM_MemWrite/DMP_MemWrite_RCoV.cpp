// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_MemWrite/DMP_MemWrite_RCoV.h>

#include <KNX/03/05/01/Interface_Object_Type.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_MemWrite_RCoV::DMP_MemWrite_RCoV(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_MemWrite_RCoV::~DMP_MemWrite_RCoV()
{
}

void DMP_MemWrite_RCoV::req(const bool verify_mode_active, const bool device_control_known, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCoV);

    if (state == State::S00) {
        this->verify_mode_active = verify_mode_active;
        this->device_control_known = device_control_known;
        this->deviceStartAddress = deviceStartAddress;
        this->deviceEndAddress = deviceEndAddress;
        this->data = data;
        if (!verify_mode_active) {
            if (!device_control_known) {
                state = State::S01;
                A01_A_PropertyDescription_Read_req();
                restart_timer(Lcon_timeout);
            } else {
                state = State::S03;
                A03_A_PropertyValue_Read_req();
                restart_timer(Lcon_timeout);
            }
        } else {
            serviceDBAddr = deviceStartAddress;
            serviceDBLen = std::min(12, deviceEndAddress - serviceDBAddr);
            if (serviceDBLen > 0) {
                state = State::S07;
                serviceDataIn.assign(std::cbegin(data) + (serviceDBAddr - deviceStartAddress), std::cend(data) + (serviceDBAddr - deviceStartAddress) + serviceDBLen);
                A07_A_Memory_Write_req();
                restart_timer(Lcon_timeout);
            } else {
                state = State::S00;
                con(Status::ok);
            }
        }
    }
}

void DMP_MemWrite_RCoV::A01_A_PropertyDescription_Read_req()
{
    assert(state == State::S01);

    application_layer.A_PropertyDescription_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), OT_DEVICE, Interface_Object::PID_DEVICE_CONTROL, 0, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_PropertyDescription_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_MemWrite_RCoV::A02_A_PropertyDescription_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_PropertyDescription_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const bool write_enable, const Property_Type type, const Max_Nr_Of_Elem max_nr_of_elem, const Access access) -> void {
        if ((asap == parameters.asap()) && (object_index == OT_DEVICE) && (property_id == Interface_Object::PID_DEVICE_CONTROL)){
            state = State::S03;
            A03_A_PropertyValue_Read_req();
            restart_timer(Lcon_timeout);
        }
        if (state == State::S02) {
            A02_A_PropertyDescription_Read_Acon();
            // do not restart timer
        }
    });
}

void DMP_MemWrite_RCoV::A03_A_PropertyValue_Read_req()
{
    assert(state == State::S03);

    application_layer.A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), OT_DEVICE, Interface_Object::PID_DEVICE_CONTROL, 1, 1, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S04;
            A04_A_PropertyValue_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_MemWrite_RCoV::A04_A_PropertyValue_Read_Acon()
{
    assert(state == State::S04);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == parameters.asap()) && (object_index == OT_DEVICE) && (property_id == Interface_Object::PID_DEVICE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
            parameters.device_control.fromData(std::cbegin(data), std::cend(data));
            if (parameters.device_control.device_control.verify_mode_on()) {
                state = State::S05;
                parameters.device_control.device_control.set_verify_mode_on(true);
                A05_A_PropertyValue_Write_req();
                restart_timer(Lcon_timeout);
            } else {
                serviceDBAddr = deviceStartAddress;
                serviceDBLen = std::min(12, deviceEndAddress - serviceDBAddr);
                if (serviceDBLen > 0) {
                    state = State::S07;
                    serviceDataIn.assign(std::cbegin(data) + (serviceDBAddr - deviceStartAddress), std::cend(data) + (serviceDBAddr - deviceStartAddress) + serviceDBLen);
                    A07_A_Memory_Write_req();
                    restart_timer(Lcon_timeout);
                } else {
                    state = State::S00;
                    con(Status::ok);
                }
            }
        }
        if (state == State::S04) {
            A04_A_PropertyValue_Read_Acon();
            // do not restart timer
        }
    });
}

void DMP_MemWrite_RCoV::A05_A_PropertyValue_Write_req()
{
    assert(state == State::S05);

    application_layer.A_PropertyValue_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), OT_DEVICE, Interface_Object::PID_DEVICE_CONTROL, 1, 1, parameters.device_control.toData(), [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S06;
            A06_A_PropertyValue_Write_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_MemWrite_RCoV::A06_A_PropertyValue_Write_Acon()
{
    assert(state == State::S06);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == parameters.asap()) && (object_index == OT_DEVICE) && (property_id == Interface_Object::PID_DEVICE_CONTROL) && (nr_of_elem == 1) && (start_index == 1)) {
            parameters.device_control.fromData(std::cbegin(data), std::cend(data));
            if (parameters.device_control.device_control.verify_mode_on()) {
                state = State::S07;
                A07_A_Memory_Write_req();
                restart_timer(Lcon_timeout);
            } else {
                state = State::S00;
                con(Status::not_ok);
            }
        }
        if (state == State::S06) {
            A04_A_PropertyValue_Read_Acon();
            // do not restart timer
        }
    });
}

void DMP_MemWrite_RCoV::A07_A_Memory_Write_req()
{
    assert(state == State::S07);

    application_layer.A_Memory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), serviceDBLen, serviceDBAddr, serviceDataIn, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S08;
            A08_A_Memory_Write_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_MemWrite_RCoV::A08_A_Memory_Write_Acon()
{
    assert(state == State::S08);

    application_layer.A_Memory_Write_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data) -> void {
        if ((asap == parameters.asap()) && (number == serviceDBLen) && (memory_address == serviceDBAddr)) {
            serviceDataOut = data;
            if (serviceDataIn == serviceDataOut) {
                serviceDBAddr += serviceDBLen;
                serviceDBLen = std::min(12, deviceEndAddress - serviceDBAddr);
                if (serviceDBLen > 0) {
                    state = State::S08;
                    serviceDataIn.assign(std::cbegin(data) + (serviceDBAddr - deviceStartAddress), std::cend(data) + (serviceDBAddr - deviceStartAddress) + serviceDBLen);
                    A08_A_Memory_Write_Acon();
                    restart_timer(Lcon_timeout);
                } else {
                    state = State::S00;
                    con(Status::ok);
                }
            } else {
                state = State::S00;
                con(Status::not_ok);
            }
        }
        if (state == State::S08) {
            A08_A_Memory_Write_Acon();
            // do not restart timer
        }
    });
}

void DMP_MemWrite_RCoV::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            // con(Status::not_ok);
            break;
        }
    });
}

}
