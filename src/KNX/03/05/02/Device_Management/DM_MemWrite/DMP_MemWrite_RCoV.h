// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_MemWrite_RCoV
 *
 * @ingroup KNX_03_05_02_03_16_03
 */
class KNX_EXPORT DMP_MemWrite_RCoV :
    public std::enable_shared_from_this<DMP_MemWrite_RCoV>
{
public:
    explicit DMP_MemWrite_RCoV(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_MemWrite_RCoV();

    void req(const bool verify_mode_active, const bool device_control_known, const Memory_Address deviceStartAddress, const Memory_Address deviceEndAddress, const Memory_Data data);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    bool verify_mode_active{};
    bool device_control_known{};
    Memory_Address deviceStartAddress{};
    Memory_Address deviceEndAddress{};
    Memory_Data data{};

    /* Service parameters */
    Memory_Data serviceDataIn{};
    Memory_Data serviceDataOut{};
    Memory_Number serviceDBLen{};
    Memory_Address serviceDBAddr{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_PropertyDescription_Read.req
        S02, ///< waiting A_PropertyDescription_Read.Acon
        S03, ///< sending A_PropertyValue_Read.req
        S04, ///< waiting A_PropertyValue_Read.Acon
        S05, ///< sending A_PropertyValue_Write.req
        S06, ///< waiting A_PropertyValue_Write.Acon
        S07, ///< sending A_Memory_Write.req
        S08, ///< waiting A_Memory_Write.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_PropertyDescription_Read_req();
    void A02_A_PropertyDescription_Read_Acon();
    void A03_A_PropertyValue_Read_req();
    void A04_A_PropertyValue_Read_Acon();
    void A05_A_PropertyValue_Write_req();
    void A06_A_PropertyValue_Write_Acon();
    void A07_A_Memory_Write_req();
    void A08_A_Memory_Write_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
