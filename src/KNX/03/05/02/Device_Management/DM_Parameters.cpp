// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Parameters.h>

namespace KNX {

ASAP_Individual DM_Parameters::asap() const
{
    ASAP_Individual asap;
    asap.address = destination_address;
    switch(communication_mode) {
    case Communication_Mode::RCo:
        asap.connected = false;
        break;
    case Communication_Mode::RCl:
        asap.connected = true;
        break;
    default:
        assert(false);
    }

    return asap;
}

}
