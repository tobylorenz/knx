// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/07/ASAP.h>
#include <KNX/03/03/07/Domain_Address_2.h>
#include <KNX/03/03/07/Key.h>
#include <KNX/03/03/07/Level.h>
#include <KNX/03/03/07/Restart_Channel_Number.h>
#include <KNX/03/03/07/Restart_Erase_Code.h>
#include <KNX/03/03/07/Restart_Error_Code.h>
#include <KNX/03/03/07/Restart_Process_Time.h>
#include <KNX/03/03/07/Restart_Type.h>
#include <KNX/03/05/01/00_Device/Hardware_Type_Property.h>
#include <KNX/03/05/01/00_Device/Management_Descriptor_1_Property.h>
#include <KNX/03/05/01/00_Device/Programming_Mode_Property.h>
#include <KNX/03/05/01/Device_Descriptor_Type_0.h>
#include <KNX/03/05/01/Device_Descriptor_Type_2.h>
#include <KNX/03/05/01/generic/Device_Control_Property.h>
#include <KNX/03/05/01/generic/Manufacturer_Identifier_Property.h>
#include <KNX/03/05/01/generic/PEI_Type_Property.h>
#include <KNX/03/05/01/generic/Serial_Number_Property.h>
#include <KNX/Memory.h>

namespace KNX {

/** Device Management Parameters */
struct DM_Parameters
{
    /**
     * Communication Mode
     *
     * @ingroup KNX_03_05_02_01_02
     */
    enum class Communication_Mode {
        /** remote - connection-oriented */
        RCo,

        /** remote - connection-oriented with verify mode */
        RCoV,

        /** remote - connectionless */
        RCl,

        /** local - EMI 1 */
        LEmi1,

        /** local - EMI 2 */
        LEmi2,

        /** local - cEMI */
        LcEMI,

        // the following are not in the table, but often used as well

        /** KNXnet/IP */
        R_IP
    };
    // flags
    // bit 0: 0=connection oriented communication, 1=connectionless communication

    /** @copydoc Communication_Mode */
    Communication_Mode communication_mode{};

    /** Individual Address of target */
    Individual_Address destination_address{}; // IA_target, IAresult

    /** ASAP */
    ASAP_Individual asap() const;

#if 0
    /** Device Descriptor */
    QMap<KNX::Descriptor_Type, std::vector<uint8_t>> device_descriptors{};
#else
    /** Device Descriptor Type (0 or 2) */
    Descriptor_Type device_descriptor_type{0}; // DM_DDType

    /** Device Descriptor 0 (Mask Type, Firmware Version) */
    Device_Descriptor_Type_0 device_descriptor_0{}; // DD0

    /** Device Descriptor 2 */
    Device_Descriptor_Type_2 device_descriptor_2{};
#endif

#if 0
    /** Interface Objects */
    KNX::Interface_Objects interface_objects{};
#else
    /** OT_DEVICE / PID_MGT_DESCRIPTOR_01 */
    Management_Descriptor_1_Property management_descriptor_1{}; // mgt_descriptor_1

    /** OT_DEVICE / PID_MANUFACTURER_ID */
    Manufacturer_Identifier_Property manufacturer_identifier{}; // manufacturer_id

    /** Serial Number */
    Serial_Number_Property serial_number{};

    /** OT_DEVICE / PID_HARDWARE_TYPE */
    Hardware_Type_Property hardware_type{};
#endif

    /** Client Access Key */
    Key key{};

    /** Client Access Level */
    Level level{};

    /** Free Access Level (using Free Access Key 0xffffffff) */
    Level free_level{};

    /** Domain Address */
    Domain_Address_2 domain_address{}; // domain_address_result

    /** Programming Mode */
    Programming_Mode_Property programming_mode{};

    /** PEI Type */
    PEI_Type_Property pei_type{};

    /** Device Control */
    Device_Control_Property device_control{};

    // Memory
    // @todo Memory memory{};

    // User Memory
    // @todo Memory user_memory{};

    // Interface Objects
    // @todo interface objects

    // Load State Machine
    // @todo Load State

    // Run State Machine
    // @todo Run State

    // Group Object Link
    // @todo Group Object Links

    // LC Slave Mem (Router Memory)
    // @todo Memory router_memory{};

    // LC Ext Mem (Routing Table)
    // @todo Memory routing_table{};

    // LC Route Table State (Router Status)
    uint8_t route_table_state{};
};

}
