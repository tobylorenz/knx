// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_PeiTypeRead.h>

namespace KNX {

DM_PeiTypeRead::DM_PeiTypeRead(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_PeiTypeRead::~DM_PeiTypeRead()
{
}

void DM_PeiTypeRead::req(const Flags flags, const uint8_t dataBlockStartAddress)
{
}

}
