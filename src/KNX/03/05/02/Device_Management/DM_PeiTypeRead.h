// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.15.2 DMP_PeiTypeRead_RCo_ADC */
#include <KNX/03/05/02/Device_Management/DM_PeiTypeRead/DMP_PeiTypeRead_RCo_ADC.h>

/* 3.15.3 DMP_PeiTypeRead_R_IO */
#include <KNX/03/05/02/Device_Management/DM_PeiTypeRead/DMP_PeiTypeRead_R_IO.h>

namespace KNX {

/**
 * DM_PeiTypeRead
 *
 * @ingroup KNX_03_05_02_03_15
 */
class KNX_EXPORT DM_PeiTypeRead :
    public std::enable_shared_from_this<DM_PeiTypeRead>
{
public:
    explicit DM_PeiTypeRead(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_PeiTypeRead();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
    };

    void req(const Flags flags, const uint8_t dataBlockStartAddress);
    std::function<void(Status dm_status)> con;
};

}
