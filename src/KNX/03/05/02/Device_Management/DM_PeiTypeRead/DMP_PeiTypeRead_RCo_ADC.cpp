// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_PeiTypeRead/DMP_PeiTypeRead_RCo_ADC.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_PeiTypeRead_RCo_ADC::DMP_PeiTypeRead_RCo_ADC(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_PeiTypeRead_RCo_ADC::~DMP_PeiTypeRead_RCo_ADC()
{
}

void DMP_PeiTypeRead_RCo_ADC::req()
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCo);

    if (state == State::S00) {
        state = State::S01;
    }
}

void DMP_PeiTypeRead_RCo_ADC::A01_A_ADC_Read_req()
{
    assert(state == State::S01);

    application_layer.A_ADC_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), 4, 1, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_ADC_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(parameters.pei_type.pei_type, Status::not_ok);
            break;
        }
    });
}

void DMP_PeiTypeRead_RCo_ADC::A02_A_ADC_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_ADC_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count, const ADC_Sum sum) -> void {
        if ((asap == parameters.asap()) && (channel_nr == 4) && (read_count == 1)) {
            state = State::S00;
            parameters.pei_type.pei_type = (10 * sum + 60) / 128;
            con(parameters.pei_type.pei_type, Status::ok);
        }
        if (state == State::S02) {
            A02_A_ADC_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_PeiTypeRead_RCo_ADC::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(parameters.pei_type.pei_type, Status::not_ok);
            break;
        }
    });
}

}
