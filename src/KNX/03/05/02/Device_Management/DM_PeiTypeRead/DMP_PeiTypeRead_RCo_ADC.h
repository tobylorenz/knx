// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_PeiTypeRead_RCo_ADC
 *
 * @ingroup KNX_03_05_02_03_15_02
 */
class KNX_EXPORT DMP_PeiTypeRead_RCo_ADC :
    public std::enable_shared_from_this<DMP_PeiTypeRead_RCo_ADC>
{
public:
    explicit DMP_PeiTypeRead_RCo_ADC(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_PeiTypeRead_RCo_ADC();

    void req();
    std::function<void(const uint8_t pei_type, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_ADC_Read.req
        S02, ///< waiting A_ADC_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_ADC_Read_req();
    void A02_A_ADC_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
