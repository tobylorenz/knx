// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_PeiTypeVerify.h>

namespace KNX {

DM_PeiTypeVerify::DM_PeiTypeVerify(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_PeiTypeVerify::~DM_PeiTypeVerify()
{
}

void DM_PeiTypeVerify::req(const Flags flags, const uint8_t dataBlockStartAddress, const uint8_t data)
{
}

}
