// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.14.2 DMP_PeiTypeVerify_RCo_ADC */
#include <KNX/03/05/02/Device_Management/DM_PeiTypeVerify/DMP_PeiTypeVerify_RCo_ADC.h>

/* 3.14.3 DMP_PeiTypeVerify_R_IO */
#include <KNX/03/05/02/Device_Management/DM_PeiTypeVerify/DMP_PeiTypeVerify_R_IO.h>

namespace KNX {
    
/**
 * DM_PeiTypeVerify
 *
 * @ingroup KNX_03_05_02_03_14
 */
class KNX_EXPORT DM_PeiTypeVerify :
    public std::enable_shared_from_this<DM_PeiTypeVerify>
{
public:
    explicit DM_PeiTypeVerify(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_PeiTypeVerify();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
    };
    
    void req(const Flags flags, const uint8_t dataBlockStartAddress, const uint8_t data);
    std::function<void(Status dm_status)> con;
};

}
