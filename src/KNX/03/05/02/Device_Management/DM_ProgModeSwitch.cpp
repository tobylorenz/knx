// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_ProgModeSwitch.h>

namespace KNX {

DM_ProgModeSwitch::DM_ProgModeSwitch(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_ProgModeSwitch::~DM_ProgModeSwitch()
{
}

void DM_ProgModeSwitch::req(const Flags flags, const uint8_t mode)
{
}

}
