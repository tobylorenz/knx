// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.13.2 DMP_ProgModeSwitch_RCo */
#include <KNX/03/05/02/Device_Management/DM_ProgModeSwitch/DMP_ProgModeSwitch_RCo.h>

/* 3.13.3 DMP_ProgModeSwitch_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_ProgModeSwitch/DMP_ProgModeSwitch_LEmi1.h>

namespace KNX {

/**
 * DM_ProgMode_Switch
 *
 * @ingroup KNX_03_05_02_03_13
 */
class KNX_EXPORT DM_ProgModeSwitch :
    public std::enable_shared_from_this<DM_ProgModeSwitch>
{
public:
    explicit DM_ProgModeSwitch(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_ProgModeSwitch();

    struct Flags {
    };
    
    void req(const Flags flags, const uint8_t mode);
    std::function<void(Status dm_status)> con;
};

}
