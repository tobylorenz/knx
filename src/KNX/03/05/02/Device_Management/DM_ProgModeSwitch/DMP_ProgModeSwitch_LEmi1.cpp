// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_ProgModeSwitch/DMP_ProgModeSwitch_LEmi1.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_ProgModeSwitch_LEmi1::DMP_ProgModeSwitch_LEmi1(asio::io_context & io_context, EMI_User_Layer & user_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    user_layer(user_layer),
    timeout_timer(io_context)
{
}

DMP_ProgModeSwitch_LEmi1::~DMP_ProgModeSwitch_LEmi1()
{
}

void DMP_ProgModeSwitch_LEmi1::req(const bool prog_mode_new)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::LEmi1);

    if (state == State::S00) {
        state = State::S01;
        this->prog_mode_new = prog_mode_new;
        A01_PC_Get_Value_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_ProgModeSwitch_LEmi1::A01_PC_Get_Value_req()
{
    assert(state == State::S01);

    user_layer.PC_Get_Value_req(1, 0x60, [this](const std::vector<uint8_t> data, const Status u_status) -> void {
        switch(u_status) {
        case Status::ok:
            state = State::S02;
            parameters.programming_mode.fromData(std::cbegin(data), std::cend(data));
            parameters.programming_mode.prog_mode = prog_mode_new;
            parameters.programming_mode.p_parity = parameters.programming_mode.parity_calculated();
            A02_PEI_Memory_Write_req();
            restart_timer(Lcon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_ProgModeSwitch_LEmi1::A02_PEI_Memory_Write_req()
{
    assert(state == State::S02);

    user_layer.PC_Set_Value_req(1, 0x60, parameters.programming_mode.toData());

    state = State::S00;
    con(Status::ok);
}

void DMP_ProgModeSwitch_LEmi1::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

}
