// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_ProgModeSwitch/DMP_ProgModeSwitch_RCo.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_ProgModeSwitch_RCo::DMP_ProgModeSwitch_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_ProgModeSwitch_RCo::~DMP_ProgModeSwitch_RCo()
{
}

void DMP_ProgModeSwitch_RCo::req(const bool prog_mode_new)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCo);

    if (state == State::S00) {
        state = State::S01;
        this->prog_mode_new = prog_mode_new;
        A01_A_Memory_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_ProgModeSwitch_RCo::A01_A_Memory_Read_req()
{
    assert(state == State::S01);

    application_layer.A_Memory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), 1, 0x60, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_Memory_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_ProgModeSwitch_RCo::A02_A_Memory_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_Memory_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data) -> void {
        if ((asap == parameters.asap()) && (number == 1) && (memory_address == 0x60)) {
            state = State::S03;
            parameters.programming_mode.fromData(std::cbegin(data), std::cend(data));
            parameters.programming_mode.prog_mode = prog_mode_new;
            parameters.programming_mode.p_parity = parameters.programming_mode.parity_calculated();
            A03_A_Memory_Write_req();
            restart_timer(Lcon_timeout);
        }
        if (state == State::S02) {
            A02_A_Memory_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_ProgModeSwitch_RCo::A03_A_Memory_Write_req()
{
    assert(state == State::S03);

    application_layer.A_Memory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), 1, 0x60, parameters.programming_mode.toData(), [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S00;
            con(Status::ok);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_ProgModeSwitch_RCo::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

}
