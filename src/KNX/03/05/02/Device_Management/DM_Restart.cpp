// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Restart.h>

namespace KNX {

DM_Restart::DM_Restart(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_Restart::~DM_Restart()
{
}

void DM_Restart::req(const struct Flags flags)
{
}

}
