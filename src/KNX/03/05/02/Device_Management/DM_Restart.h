// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.7.2 DMP_Restart_RCl */
#include <KNX/03/05/02/Device_Management/DM_Restart/DMP_Restart_RCl.h>

/* 3.7.3 DMP_Restart_RCo */
#include <KNX/03/05/02/Device_Management/DM_Restart/DMP_Restart_RCo.h>

/* 3.7.4 DMP_Restart_LEmi1 */
#include <KNX/03/05/02/Device_Management/DM_Restart/DMP_Restart_LEmi1.h>

namespace KNX {

/**
 * DM_Restart
 *
 * @ingroup KNX_03_05_02_03_07
 */
class KNX_EXPORT DM_Restart :
    public std::enable_shared_from_this<DM_Restart>
{
public:
    explicit DM_Restart(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_Restart();

    struct Flags {
    };
    
    void req(const struct Flags flags);
    std::function<void(Status dm_status)> con;
};

}
