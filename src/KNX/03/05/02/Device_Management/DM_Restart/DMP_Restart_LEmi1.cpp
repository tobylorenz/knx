// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Restart/DMP_Restart_LEmi1.h>

namespace KNX {

DMP_Restart_LEmi1::DMP_Restart_LEmi1(asio::io_context & io_context, EMI_User_Layer & user_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    user_layer(user_layer),
    timeout_timer(io_context)
{
}

DMP_Restart_LEmi1::~DMP_Restart_LEmi1()
{
}

void DMP_Restart_LEmi1::req()
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::LEmi1);

    if (state == State::S00) {
        state = State::S01;
    }
}

void DMP_Restart_LEmi1::A01_PC_Set_Value_req()
{
    assert(state == State::S01);

    user_layer.PC_Set_Value_req(1, 0x0060, {0xC0});

    state = State::S00;
    con(Status::ok);
}

}
