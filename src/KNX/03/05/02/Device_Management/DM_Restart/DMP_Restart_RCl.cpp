// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_Restart/DMP_Restart_RCl.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_Restart_RCl::DMP_Restart_RCl(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_Restart_RCl::~DMP_Restart_RCl()
{
}

void DMP_Restart_RCl::req(const Restart_Type mpp_RestartType, const Restart_Erase_Code mpp_EraseCode, const Restart_Channel_Number mpp_ChannelNumber)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCl);

    if (state == State::S00) {
        state = State::S01;
        restart_type = mpp_RestartType;
        restart_erase_code = mpp_EraseCode;
        restart_channel_number = mpp_ChannelNumber;
        A01_A_Restart_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_Restart_RCl::A01_A_Restart_req()
{
    assert(state == State::S01);

    application_layer.A_Restart_req(Ack_Request::dont_care, restart_channel_number, restart_erase_code, Priority::low,Network_Layer_Parameter, restart_type, parameters.asap(), [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_Restart_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(restart_error_code, restart_type, Status::not_ok);
            break;
        }
    });
}

void DMP_Restart_RCl::A02_A_Restart_Acon()
{
    assert(state == State::S02);

    application_layer.A_Restart_Acon([this](const Restart_Error_Code error_code, const Priority /*priority*/, const Restart_Process_Time restart_process_time, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap) -> void {
        if (asap == parameters.asap()) {
            state = State::S00;
            restart_error_code = error_code;
            restart_type = restart_process_time;
            con(restart_error_code, restart_type, Status::ok);
        }
        if (state == State::S02) {
            A02_A_Restart_Acon();
            // do not reset timer
        }
    });
}

void DMP_Restart_RCl::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(restart_error_code, restart_type, Status::ok);
            break;
        }
    });
}

}
