// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DM_Restart_RCl
 *
 * @ingroup KNX_03_05_02_03_07_02
 */
class KNX_EXPORT DMP_Restart_RCl :
    public std::enable_shared_from_this<DMP_Restart_RCl>
{
public:
    explicit DMP_Restart_RCl(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_Restart_RCl();

    void req(const Restart_Type mpp_RestartType, const Restart_Erase_Code mpp_EraseCode, const Restart_Channel_Number mpp_ChannelNumber);
    std::function<void(const Restart_Error_Code mpp_ErrorCode, const Restart_Process_Time mpp_ProcessTime, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

    /** Restart Type */
    Restart_Type restart_type{};

    /** Restart Erase Code */
    Restart_Erase_Code restart_erase_code{};

    /** Restart Channel Number */
    Restart_Channel_Number restart_channel_number{};

    /** Restart Error Code */
    Restart_Error_Code restart_error_code{};

    /** Restart Process Time */
    Restart_Process_Time restart_process_time{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Restart.req
        S02, ///< waiting A_Restart.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Restart_req();
    void A02_A_Restart_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
