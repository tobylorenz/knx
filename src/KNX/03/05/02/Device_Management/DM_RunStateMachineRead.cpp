// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_RunStateMachineRead.h>

namespace KNX {

DM_RunStateMachineRead::DM_RunStateMachineRead(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_RunStateMachineRead::~DM_RunStateMachineRead()
{
}

void DM_RunStateMachineRead::req(const Flags flags, const uint8_t dataBlockStartAddress, const Object_Type stateMachineType, const Object_Index stateMachineNr)
{
}

}
