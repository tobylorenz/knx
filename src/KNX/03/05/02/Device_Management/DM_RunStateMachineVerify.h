// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.32.2 DMP_RunStateMachineVerify_RCo_Mem */
#include <KNX/03/05/02/Device_Management/DM_RunStateMachineVerify/DMP_RunStateMachineVerify_RCo_Mem.h>

/* 3.32.3 DMP_RunStateMachineVerify_R_IO */
#include <KNX/03/05/02/Device_Management/DM_RunStateMachineVerify/DMP_RunStateMachineVerify_R_IO.h>

namespace KNX {

/**
 * DM_RunStateMachineVerify
 *
 * @ingroup KNX_03_05_02_03_32
 */
class KNX_EXPORT DM_RunStateMachineVerify :
    public std::enable_shared_from_this<DM_RunStateMachineVerify>
{
public:
    explicit DM_RunStateMachineVerify(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_RunStateMachineVerify();

    struct Flags {
        bool location_of_data_in_data_block : 1; // bit 0
    };

    void req(const Flags flags, const Object_Type stateMachineType, const Object_Index stateMachineNr, const uint8_t state);
    std::function<void(Status dm_status)> con;
};

}
