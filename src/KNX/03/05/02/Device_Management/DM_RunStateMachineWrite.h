// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.31.2 DMP_RunStateMachineWrite_RCo_Mem */
#include <KNX/03/05/02/Device_Management/DM_RunStateMachineWrite/DMP_RunStateMachineWrite_RCo_Mem.h>

/* 3.31.3 DMP_RunStateMachineWrite_R_IO */
#include <KNX/03/05/02/Device_Management/DM_RunStateMachineWrite/DMP_RunStateMachineWrite_R_IO.h>

namespace KNX {

/**
 * DM_RunStateMachineWrite
 *
 * @ingroup KNX_03_05_02_03_31
 */
class KNX_EXPORT DM_RunStateMachineWrite :
    public std::enable_shared_from_this<DM_RunStateMachineWrite>
{
public:
    explicit DM_RunStateMachineWrite(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_RunStateMachineWrite();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
        bool verify_resulting_state_enabled : 1; // bit 1
    };
    
    void req(const Flags flags, const Object_Type stateMachineType, const Object_Index stateMachineNr, const uint8_t event);
    std::function<void(Status dm_status)> con;
};

}
