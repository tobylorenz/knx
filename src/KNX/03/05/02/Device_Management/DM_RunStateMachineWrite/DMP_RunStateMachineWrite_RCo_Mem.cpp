// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_RunStateMachineWrite/DMP_RunStateMachineWrite_RCo_Mem.h>

#include <KNX/03/05/01/Interface_Object_Type.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_RunStateMachineWrite_RCo_Mem::DMP_RunStateMachineWrite_RCo_Mem(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_RunStateMachineWrite_RCo_Mem::~DMP_RunStateMachineWrite_RCo_Mem()
{
}

void DMP_RunStateMachineWrite_RCo_Mem::req(const Object_Type stateMachineType, const Object_Index stateMachineNr, const Memory_Data eventData)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCo);
    assert((parameters.device_descriptor_0.mask_type & 0xfff0) == 0x0700); // BIM M112
    assert(eventData.size() == 10);

    if (state == State::S00) {
        state = State::S01;
        this->stateMachineType = stateMachineType;
        this->stateMachineNr = stateMachineNr;
        this->eventData = eventData;
        switch(stateMachineType) {
        case OT_ADDRESS_TABLE:
            address = 0xB6EA;
            break;
        case OT_ASSOCIATION_TABLE:
            address = 0xB6EB;
            break;
        case OT_APPLICATION_PROGRAM:
            address = 0xB6EC;
            break;
        case OT_INTERFACE_PROGRAM:
            address = 0xB6ED;
            break;
        default:
            assert(false);
        }
        A01_A_Memory_Write_req();
        restart_timer(Lcon_timeout);
    }
}

void DMP_RunStateMachineWrite_RCo_Mem::A01_A_Memory_Write_req()
{
    assert(state == State::S01);

    application_layer.A_Memory_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data) -> void {
        if ((asap == parameters.asap()) && (number == 10) && (memory_address == address)) {
            runstate = static_cast<Run_State_Machine::State>(data[0]);
            con(runstate, Status::ok);
        }
        if (state == State::S03) {
            A03_A_Memory_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_RunStateMachineWrite_RCo_Mem::A02_A_Memory_Read_req()
{
    assert(state == State::S02);

    application_layer.A_Memory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), 10, address, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S03;
            A03_A_Memory_Read_Acon();
            restart_timer(Lcon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(runstate, Status::not_ok);
            break;
        }
    });
}

void DMP_RunStateMachineWrite_RCo_Mem::A03_A_Memory_Read_Acon()
{
    assert(state == State::S03);

    application_layer.A_Memory_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data) -> void {
        if ((asap == parameters.asap()) && (number == 10) && (memory_address == address)) {
            runstate = static_cast<Run_State_Machine::State>(data[0]);
            con(runstate, Status::ok);
        }
        if (state == State::S03) {
            A03_A_Memory_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_RunStateMachineWrite_RCo_Mem::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(runstate, Status::not_ok);
            break;
        }
    });
}

}
