// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/01/Run_State_Machine.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_RunStateMachineWrite_RCo_Mem
 *
 * @ingroup KNX_03_05_02_03_31_02
 */
class KNX_EXPORT DMP_RunStateMachineWrite_RCo_Mem :
    public std::enable_shared_from_this<DMP_RunStateMachineWrite_RCo_Mem>
{
public:
    explicit DMP_RunStateMachineWrite_RCo_Mem(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_RunStateMachineWrite_RCo_Mem();

    void req(const Object_Type stateMachineType, const Object_Index stateMachineNr, const Memory_Data eventData);
    std::function<void(Run_State_Machine::State runstate, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    Object_Type stateMachineType{};
    Object_Index stateMachineNr{};
    Memory_Data eventData{}; // 10 octets including event L1/2/3/4 as first octet
    Run_State_Machine::State runstate{}; // out

    /* Service Data */
    Memory_Address address{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Memory_Write.req
        S02, ///< sending A_Memory_Read.req
        S03, ///< waiting A_Memory_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Memory_Write_req();
    void A02_A_Memory_Read_req();
    void A03_A_Memory_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
