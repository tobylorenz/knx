// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/01/Run_State_Machine.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_RunStateMachineWrite_R_IO
 *
 * @ingroup KNX_03_05_02_03_31_03
 */
class KNX_EXPORT DMP_RunStateMachineWrite_R_IO :
    public std::enable_shared_from_this<DMP_RunStateMachineWrite_R_IO>
{
public:
    explicit DMP_RunStateMachineWrite_R_IO(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_RunStateMachineWrite_R_IO();

    void req(const bool management_control_known, const Object_Index object_index, const Memory_Data data);
    std::function<void(Run_State_Machine::State runstate, Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    bool management_control_known{};
    Object_Index object_index{};
    Memory_Data data{};
    Run_State_Machine::State runstate{}; // out

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_PropertyDescription_Read.req
        S02, ///< waiting A_PropertyDescription_Read.Acon
        S03, ///< sending A_PropertyValue_Write.req
        S04, ///< waiting A_PropertyValue_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_PropertyDescription_Read_req();
    void A02_A_PropertyDescription_Read_Acon();
    void A03_A_PropertyValue_Write_req();
    void A04_A_PropertyValue_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
