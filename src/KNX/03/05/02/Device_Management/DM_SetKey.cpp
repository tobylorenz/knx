// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_SetKey.h>

namespace KNX {

DM_SetKey::DM_SetKey(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_SetKey::~DM_SetKey()
{
}

void DM_SetKey::req(const Flags flags, const Key key, const Level level)
{
}

}
