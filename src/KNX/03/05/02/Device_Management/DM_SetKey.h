// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.6.1 DMP_SetKey_RCo */
#include <KNX/03/05/02/Device_Management/DM_SetKey/DMP_SetKey_RCo.h>

namespace KNX {

/**
 * DM_SetKey
 *
 * @ingroup KNX_03_05_02_03_06
 */
class KNX_EXPORT DM_SetKey :
    public std::enable_shared_from_this<DM_SetKey>
{
public:
    explicit DM_SetKey(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_SetKey();

    struct Flags {
    };
    
    void req(const Flags flags, const Key key, const Level level);
    std::function<void(Status dm_status)> con;
};

}
