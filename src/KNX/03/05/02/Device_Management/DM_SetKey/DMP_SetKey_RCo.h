// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DM_SetKey_RCo
 *
 * @ingroup KNX_03_05_02_03_06_01
 */
class KNX_EXPORT DMP_SetKey_RCo :
    public std::enable_shared_from_this<DMP_SetKey_RCo>
{
public:
    explicit DMP_SetKey_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_SetKey_RCo();

    void req(const Key key, const Level level);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Key_Write.req
        S02, ///< waiting A_Key_Write.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Key_Write_req();
    void A02_A_Key_Write_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
