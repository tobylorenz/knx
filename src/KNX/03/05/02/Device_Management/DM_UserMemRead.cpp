// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_UserMemRead.h>

namespace KNX {

DM_UserMemRead::DM_UserMemRead(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_UserMemRead::~DM_UserMemRead()
{
}

void DM_UserMemRead::req(const Flags flags, const uint8_t dataBlockStartAddress, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress)
{
}

}
