// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.21.2 DMP_UserMemRead_RCo */
#include <KNX/03/05/02/Device_Management/DM_UserMemRead/DMP_UserMemRead_RCo.h>

namespace KNX {

/**
 * DM_UserMemRead
 *
 * @ingroup KNX_03_05_02_03_21
 */
class KNX_EXPORT DM_UserMemRead :
    public std::enable_shared_from_this<DM_UserMemRead>
{
public:
    explicit DM_UserMemRead(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_UserMemRead();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
    };

    void req(const Flags flags, const uint8_t dataBlockStartAddress, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress);
    std::function<void(Status dm_status)> con;
};

}
