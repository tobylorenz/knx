// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_UserMemRead/DMP_UserMemRead_RCo.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_UserMemRead_RCo::DMP_UserMemRead_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_UserMemRead_RCo::~DMP_UserMemRead_RCo()
{
}

void DMP_UserMemRead_RCo::req(const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCo);

    if (state == State::S00) {
        serviceDBAddr = deviceStartAddress;
        serviceDBLen = deviceEndAddress - serviceDBAddr;
        if (serviceDBLen > 12) {
            serviceDBLen = 12;
        }
        if (serviceDBLen > 0) {
            state = State::S01;
            A01_A_UserMemory_Read_req();
            restart_timer(Lcon_timeout);
        } else {
            state = State::S00;
            con(data, Status::ok);
        }
    }
}

void DMP_UserMemRead_RCo::A01_A_UserMemory_Read_req()
{
    assert(state == State::S01);

    application_layer.A_UserMemory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), serviceDBLen, serviceDBAddr, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_UserMemory_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(data, Status::not_ok);
            break;
        }
    });
}

void DMP_UserMemRead_RCo::A02_A_UserMemory_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_Memory_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data) -> void {
        if ((asap == parameters.asap()) && (number == serviceDBLen) && (memory_address == serviceDBAddr)) {
            // @todo save this in DM_Parameters
            std::copy(std::cbegin(data), std::cend(data), std::begin(this->data) + (serviceDBAddr - deviceStartAddress));
            serviceDBAddr = serviceDBAddr + serviceDBLen;
            serviceDBLen = deviceEndAddress - serviceDBAddr;
            if (serviceDBLen > 12) {
                serviceDBLen = 12;
            }
            if (serviceDBLen > 0) {
                state = State::S01;
                A01_A_UserMemory_Read_req();
                restart_timer(Lcon_timeout);
            } else {
                state = State::S00;
                con(data, Status::ok);
            }
        }
        if (state == State::S02) {
            A02_A_UserMemory_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_UserMemRead_RCo::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            // con(Status::not_ok);
            break;
        }
    });
}

}
