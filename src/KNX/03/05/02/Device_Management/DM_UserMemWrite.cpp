// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_UserMemWrite.h>

namespace KNX {

DM_UserMemWrite::DM_UserMemWrite(asio::io_context & io_context, Application_Layer & application_layer)
{
}

DM_UserMemWrite::~DM_UserMemWrite()
{
}

void DM_UserMemWrite::req(const Flags flags, const uint8_t dataBlockStartAddress, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data)
{
}

}
