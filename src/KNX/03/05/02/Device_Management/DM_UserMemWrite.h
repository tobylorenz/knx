// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

/* 3.19.2 DMP_UserMemWrite_RCo */
#include <KNX/03/05/02/Device_Management/DM_UserMemWrite/DMP_UserMemWrite_RCo.h>

/* 3.19.3 DMP_UserMemWrite_RCoV */
#include <KNX/03/05/02/Device_Management/DM_UserMemWrite/DMP_UserMemWrite_RCoV.h>

namespace KNX {

/**
 * DM_UserMemWrite
 *
 * @ingroup KNX_03_05_02_03_19
 */
class KNX_EXPORT DM_UserMemWrite :
    public std::enable_shared_from_this<DM_UserMemWrite>
{
public:
    explicit DM_UserMemWrite(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DM_UserMemWrite();

    struct Flags {
        bool location_of_data_in_management_control : 1; // bit 0
        bool verify_enabled : 1; // bit 1
    };

    void req(const Flags flags, const uint8_t dataBlockStartAddress, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data);
    std::function<void(Status dm_status)> con;
};

}
