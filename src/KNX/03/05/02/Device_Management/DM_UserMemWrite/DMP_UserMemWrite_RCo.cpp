// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Device_Management/DM_UserMemWrite/DMP_UserMemWrite_RCo.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_UserMemWrite_RCo::DMP_UserMemWrite_RCo(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters) :
    parameters(parameters),
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_UserMemWrite_RCo::~DMP_UserMemWrite_RCo()
{
}

void DMP_UserMemWrite_RCo::req(const bool verify, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data)
{
    assert(parameters.communication_mode == DM_Parameters::Communication_Mode::RCo);

    if (state == State::S00) {
        this->verify = verify;
        this->deviceStartAddress = deviceStartAddress;
        this->deviceEndAddress = deviceEndAddress;
        this->data = data;
        serviceDBAddr = deviceStartAddress;
        serviceDBLen = deviceEndAddress - serviceDBAddr;
        if (serviceDBLen > 12) {
            serviceDBLen = 12;
        }
        if (serviceDBLen > 0) {
            state = State::S01;
            serviceDataIn.assign(std::cbegin(data) + (serviceDBAddr - deviceStartAddress), std::cend(data) + (serviceDBAddr - deviceStartAddress) + serviceDBLen);
            A01_A_UserMemory_Write_req();
            restart_timer(Lcon_timeout);
        } else {
            state = State::S00;
            con(Status::ok);
        }
    }
}

void DMP_UserMemWrite_RCo::A01_A_UserMemory_Write_req()
{
    assert(state == State::S01);

    application_layer.A_UserMemory_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), serviceDBLen, serviceDBAddr, serviceDataIn, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            if (verify) {
                state = State::S02;
                A02_A_UserMemory_Read_req();
                restart_timer(Lcon_timeout);
            } else {
                serviceDBAddr = serviceDBAddr + serviceDBLen;
                serviceDBLen = deviceEndAddress - serviceDBAddr;
                if (serviceDBLen > 12) {
                    serviceDBLen = 12;
                }
                if (serviceDBLen > 0) {
                    state = State::S01;
                    serviceDataIn.assign(std::cbegin(data) + (serviceDBAddr - deviceStartAddress), std::cend(data) + (serviceDBAddr - deviceStartAddress) + serviceDBLen);
                    A01_A_UserMemory_Write_req();
                    restart_timer(Lcon_timeout);
                } else {
                    state = State::S00;
                    con(Status::ok);
                }
            }
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_UserMemWrite_RCo::A02_A_UserMemory_Read_req()
{
    assert(state == State::S02);

    application_layer.A_UserMemory_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, parameters.asap(), serviceDBLen, serviceDBAddr, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S03;
            A03_A_UserMemory_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void DMP_UserMemWrite_RCo::A03_A_UserMemory_Read_Acon()
{
    assert(state == State::S03);

    application_layer.A_UserMemory_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data) -> void {
        if ((asap == parameters.asap()) && (number == serviceDBLen) && (memory_address == serviceDBAddr)) {
            serviceDataOut = data;
            // @todo save this in DM_Parameters
            if (serviceDataIn == serviceDataOut) {
                serviceDBAddr = serviceDBAddr+ serviceDBLen;
                serviceDBLen = deviceEndAddress - serviceDBAddr;
                if (serviceDBLen > 12) {
                    serviceDBLen = 12;
                }
                if (serviceDBLen > 0) {
                    state = State::S01;
                    serviceDataIn.assign(std::cbegin(data) + (serviceDBAddr - deviceStartAddress), std::cend(data) + (serviceDBAddr - deviceStartAddress) + serviceDBLen);
                    A01_A_UserMemory_Write_req();
                    restart_timer(Lcon_timeout);
                } else {
                    state = State::S00;
                    con(Status::ok);
                }
            } else {
                state = State::S00;
                con(Status::not_ok);
            }
        }
        if (state == State::S03) {
            A03_A_UserMemory_Read_Acon();
            // do not reset timer
        }
    });
}

void DMP_UserMemWrite_RCo::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            // con(Status::not_ok);
            break;
        }
    });
}

}
