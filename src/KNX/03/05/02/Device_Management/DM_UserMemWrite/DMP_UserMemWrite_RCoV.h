// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/02/Device_Management/DM_Parameters.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_UserMemWrite_RCoV
 *
 * @ingroup KNX_03_05_02_03_19_03
 */
class KNX_EXPORT DMP_UserMemWrite_RCoV :
    public std::enable_shared_from_this<DMP_UserMemWrite_RCoV>
{
public:
    explicit DMP_UserMemWrite_RCoV(asio::io_context & io_context, Application_Layer & application_layer, DM_Parameters & parameters);
    virtual ~DMP_UserMemWrite_RCoV();

    void req(const bool verify_mode_active, const bool device_control_known, const UserMemory_Address deviceStartAddress, const UserMemory_Address deviceEndAddress, const Memory_Data data);
    std::function<void(Status dm_status)> con;

    /** Parameters */
    DM_Parameters & parameters;
    bool verify_mode_active{};
    bool device_control_known{};
    UserMemory_Address deviceStartAddress{};
    UserMemory_Address deviceEndAddress{};
    Memory_Data data{};

    /* Service parameters */
    Memory_Data serviceDataIn{};
    Memory_Data serviceDataOut{};
    UserMemory_Number serviceDBLen{};
    UserMemory_Address serviceDBAddr{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_PropertyDescription_Read.req
        S02, ///< waiting A_PropertyDescription_Read.Acon
        S03, ///< sending A_PropertyValue_Read.req
        S04, ///< waiting A_PropertyValue_Read.Acon
        S05, ///< sending A_PropertyValue_Write.req
        S06, ///< waiting A_PropertyValue_Write.Acon
        S07, ///< sending A_UserMemory_Write.req
        S08, ///< waiting A_UserMemory_Write.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_PropertyDescription_Read_req();
    void A02_A_PropertyDescription_Read_Acon();
    void A03_A_PropertyValue_Read_req();
    void A04_A_PropertyValue_Read_Acon();
    void A05_A_PropertyValue_Write_req();
    void A06_A_PropertyValue_Write_Acon();
    void A07_A_UserMemory_Write_req();
    void A08_A_UserMemory_Write_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
