
void File_Transfer::A_FunctionPropertyState_Read_Acon_initiator(){
    application_layer.A_FunctionPropertyState_Read_Acon([this](const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const std::optional<Property_Return_Code> return_code) -> void {
        if (!return_code.has_value()) {
            return;
        }

        std::vector<uint8_t>::const_iterator first = std::cbegin(data);
        std::vector<uint8_t>::const_iterator last = std::cend(data);

        const File_Command_Property::Command command = static_cast<File_Command_Property::Command>(*first++);
        switch (command) {
        case File_Command_Property::Command::Get_File_Handle:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
                assert(std::distance(first, last) == 1);
                const File_Handle file_Handle = *first++;
                // @todo Get_File_Handle Command_successful
            }
            break;
            case File_Command_Property::Return_Code::Object_busy: {
                assert(std::distance(first, last) == 1);
                const Object_Index next_free_object = *first++;
                // @todo Get_File_Handle Object_Busy
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Retrieve_File:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            case File_Command_Property::Return_Code::Access_denied: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Store_File:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            case File_Command_Property::Return_Code::Write_protected: {
            }
            break;
            case File_Command_Property::Return_Code::Access_denied: {
            }
            break;
            case File_Command_Property::Return_Code::No_more_space_available: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::List_Directory:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Rename_From:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            case File_Command_Property::Return_Code::Write_protected: {
            }
            break;
            case File_Command_Property::Return_Code::Access_denied: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Rename_To:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Command_not_possible: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Delete:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            case File_Command_Property::Return_Code::Write_protected: {
            }
            break;
            case File_Command_Property::Return_Code::Access_denied: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Remove_Directory:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            case File_Command_Property::Return_Code::Write_protected: {
            }
            break;
            case File_Command_Property::Return_Code::Access_denied: {
            }
            break;
            case File_Command_Property::Return_Code::Directory_not_empty: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Make_Directory:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            case File_Command_Property::Return_Code::Write_protected: {
            }
            break;
            case File_Command_Property::Return_Code::Access_denied: {
            }
            break;
            case File_Command_Property::Return_Code::Directory_not_empty: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Get_File_Size:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
                assert(std::distance(first, last) == 4);
                const File_Command_Property::File_Size file_size =
                    (*first++ << 24) |
                    (*first++ << 16) |
                    (*first++ << 8) |
                    (*first++ << 0);
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            case File_Command_Property::Return_Code::Access_denied: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Get_Empty_Disk_Space:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
                assert(std::distance(first, last) == 4);
                const File_Command_Property::Disk_Space disk_space =
                    (*first++ << 24) |
                    (*first++ << 16) |
                    (*first++ << 8) |
                    (*first++ << 0);
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Abort:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Command_not_possible: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Get_File:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
                assert(std::distance(first, last) == 2);
                const File_Command_Property::Media_Type media_type = *first++;
                const File_Command_Property::Media_Subtype mdeia_subtype = *first++;
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            case File_Command_Property::Return_Code::Access_denied: {
            }
            break;
            default:
                assert(false);
            }
            break;
        case File_Command_Property::Command::Post_File:
            switch (static_cast<File_Command_Property::Return_Code>(return_code.value())) {
            case File_Command_Property::Return_Code::Command_successful: {
            }
            break;
            case File_Command_Property::Return_Code::Invalid_command: {
            }
            break;
            case File_Command_Property::Return_Code::Not_found: {
            }
            break;
            case File_Command_Property::Return_Code::Write_protected: {
            }
            break;
            case File_Command_Property::Return_Code::Access_denied: {
            }
            break;
            case File_Command_Property::Return_Code::No_more_space_available: {
            }
            break;
            default:
                assert(false);
            }
            break;
        default:
            assert(false);
        }

        A_FunctionPropertyState_Read_Acon_initiator();
    });
}

}
