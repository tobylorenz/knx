// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 5 File Transfer Procedures */

/* 5.2 FTP_RetrieveFile */
#include <KNX/03/05/02/File_Transfer/FTP_RetrieveFile.h>

/* 5.3 FTP_StoreFile */
#include <KNX/03/05/02/File_Transfer/FTP_StoreFile.h>

/* 5.4 FTP_ListDirectory */
#include <KNX/03/05/02/File_Transfer/FTP_ListDirectory.h>

/* 5.5 FTP_Rename (consisting of Rename From and Rename To) */
#include <KNX/03/05/02/File_Transfer/FTP_Rename.h>

/* 5.6 FTP_Delete */
#include <KNX/03/05/02/File_Transfer/FTP_Delete.h>

/* 5.7 FTP_RemoveDirectory */
#include <KNX/03/05/02/File_Transfer/FTP_RemoveDirectory.h>

/* 5.8 FTP_MakeDirectory */
#include <KNX/03/05/02/File_Transfer/FTP_MakeDirectory.h>

/* 5.9 FTP_FileSize */
#include <KNX/03/05/02/File_Transfer/FTP_FileSize.h>

/* 5.10 FTP_EmptyDiskSpace */
#include <KNX/03/05/02/File_Transfer/FTP_EmptyDiskSpace.h>

/* 5.11 FTP_Abort */
#include <KNX/03/05/02/File_Transfer/FTP_Abort.h>

/* 5.12 hTTP_GteFile */
#include <KNX/03/05/02/File_Transfer/hTTP_GetFile.h>

/* 5.13 hTTP_PostFile */
#include <KNX/03/05/02/File_Transfer/hTTP_PostFile.h>
