// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/File_Transfer/FTP_Abort.h>

namespace KNX {

FTP_Abort::FTP_Abort(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

FTP_Abort::~FTP_Abort()
{
}

void FTP_Abort::req(const Individual_Address server_ia, const Object_Index file_server_oi)
{
    if (state == State::S00) {
        state = State::S01;
        this->server_ia = server_ia;
        this->file_server_oi = file_server_oi;
        A01_A_FunctionPropertyCommand();
    }
}

void FTP_Abort::A01_A_FunctionPropertyCommand()
{
    assert(state == State::S01);

    state = State::S02;
    A02_A_FunctionPropertyState_Response();
}

void FTP_Abort::A02_A_FunctionPropertyState_Response()
{
    assert(state == State::S02);

    state = State::S00;
    con(Status::ok);
}

void FTP_Abort::restart_timer(std::chrono::milliseconds duration)
{

}

}
