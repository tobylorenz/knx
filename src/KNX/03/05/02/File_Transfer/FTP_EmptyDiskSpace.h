// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * FTP_EmptyDiskSpace
 *
 * @ingroup KNX_03_05_02_05_10
 */
class KNX_EXPORT FTP_EmptyDiskSpace :
    public std::enable_shared_from_this<FTP_EmptyDiskSpace>
{
public:
    explicit FTP_EmptyDiskSpace(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~FTP_EmptyDiskSpace();

    void req(const Individual_Address server_ia, const Object_Index file_server_oi);
    std::function<void(const File_Handle file_handle, const Status ftp_status)> con;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_FunctionPropertyCommand.req
        S02, ///< waiting A_FunctionPropertyState.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_FunctionPropertyCommand_req();
    void A02_A_FunctionPropertyState_Acon();
    void restart_timer(std::chrono::milliseconds duration);
};

}
