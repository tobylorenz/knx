// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/File_Transfer/FTP_RetrieveFile.h>

namespace KNX {

FTP_RetrieveFile::FTP_RetrieveFile(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

FTP_RetrieveFile::~FTP_RetrieveFile()
{
}

void FTP_RetrieveFile::req(const Individual_Address server_ia, const Object_Index file_server_oi, const std::string file_path)
{
    // Property_Value data;
    // data.push_back(static_cast<uint8_t>(File_Command_Property::Command::Get_File_Handle));
    // application_layer.A_FunctionPropertyCommand_req(Ack_Request::dont_care, {server_ia, false}, Comm_Mode::Individual, data, Network_Layer_Parameter, file_server_oi, Priority::low, File_Server_Object::PID_FILE_COMMAND, [](const Status a_status){
    // });

    // A_FunctionPropertyCommand
    // A_FunctionPropertyState_Response
    // A_PropertyValue_Write
    // A_PropertyValue_Response
    // A_FileStream_InfoReport
}

}
