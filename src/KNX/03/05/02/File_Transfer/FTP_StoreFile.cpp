// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/File_Transfer/FTP_StoreFile.h>

namespace KNX {

FTP_StoreFile::FTP_StoreFile(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

FTP_StoreFile::~FTP_StoreFile()
{
}

void FTP_StoreFile::req(const Individual_Address server_ia, const Object_Index file_server_oi, const std::string file_path)
{
    // A_FunctionPropertyCommand
    // A_FunctionPropertyState_Response
    // A_PropertyValue_Write
    // A_PropertyValue_Response
    // A_FileStream_InfoReport
}

}
