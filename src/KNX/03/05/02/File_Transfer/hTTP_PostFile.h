// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * hTTP_PostFile
 *
 * @note identical to FTP_StoreFile
 *
 * @ingroup KNX_03_05_02_05_13
 */
class KNX_EXPORT hTTP_PostFile :
    public std::enable_shared_from_this<hTTP_PostFile>
{
public:
    explicit hTTP_PostFile(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~hTTP_PostFile();

    void req(const Individual_Address server_ia, const Object_Index file_server_oi, const std::string file_path);
    std::function<void(const File_Handle file_handle)> con;

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void restart_timer(std::chrono::milliseconds duration);
};

}
