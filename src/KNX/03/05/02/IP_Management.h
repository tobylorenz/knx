// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 4 KNXnet/IP Management Procedures */

/* 4.1 DMP_KNXnet/IP_Connect */
#include <KNX/03/05/02/IP_Management/DMP_IP_Connect.h>

/* 4.2 DMP_InterfaceObjectWrite_IP */
#include <KNX/03/05/02/IP_Management/DMP_InterfaceObjectRead_IP.h>

/* 4.3 DMP_InterfaceObjectRead_IP */
#include <KNX/03/05/02/IP_Management/DMP_InterfaceObjectWrite_IP.h>
