// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/IP_Management/DMP_IP_Connect.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

DMP_IP_Connect::DMP_IP_Connect(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_IP_Connect::~DMP_IP_Connect()
{
}

void DMP_IP_Connect::req(const IP_Host_Protocol_Address_Information dmp_HPAIControlEndpoint, const IP_Host_Protocol_Address_Information dmp_HPAIDataEndpoint, const Connection_Request_Information dmp_CRI)
{
    if (state == State::S00) {
        state = State::S01;
        this->dmp_HPAIControlEndpoint = dmp_HPAIControlEndpoint;
        this->dmp_HPAIDataEndpoint = dmp_HPAIDataEndpoint;
        this->dmp_CRI = dmp_CRI;
        dmp_CommChannelID = 0;
        dmp_Status = 0;
        dmp_HPAIServerDataEndpoint = Host_Protocol_Address_Information();
        dmp_CRD = Connection_Response_Data_Block();
        do_CONNECT_REQUEST();
        do_timer(Lcon_timeout);
    }
}

void DMP_IP_Connect::do_CONNECT_REQUEST()
{
    assert(state == State::S01);

    IP_Host_Protocol_Address_Information remote_control_endpoint;
    ip_communication_channel = std::make_shared<IP_Communication_Channel>(io_context, remote_control_endpoint);
    ip_communication_channel->connect_req();
    ip_communication_channel->on_connect_socket = [this, &dmp_HPAIControlEndpoint, &dmp_HPAIDataEndpoint, &dmp_CRI]() -> void {
        //dmp_HPAIControlEndpoint = ip_communication_channel->local_control_endpoint();
        //dmp_HPAIDataEndpoint = local_data_endpoint();
        ip_communication_channel->connect_req(dmp_CRI.connection_type_code);

    };

    state = State::S02;
    do_CONNECT_RESPONSE();
}

void DMP_IP_Connect::do_CONNECT_RESPONSE()
{
    assert(state == State::S02);

    ip_communication_channel->connect_Acon = [&dmp_CommChannelID, &dmp_Status, &dmp_HPAIServerDataEndpoint, &dmp_CRD](const Connect_Response_Frame connect_response_frame) -> void {
        dmp_CommChannelID = connect_response_frame.communication_channel_id;
        dmp_Status = connect_response_frame.status;
        dmp_HPAIServerDataEndpoint = *connect_response_frame.data_endpoint;
        dmp_CRD = *connect_response_frame.connection_response_data_block;
    };

    state = State::S00;
}

void DMP_IP_Connect::do_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        default:
            state = State::S00;
            con(Status::not_ok, dmp_CommChannelID, dmp_Status, dmp_HPAIServerDataEndpoint, dmp_CRD);
            break;
        }
    });
}

}
