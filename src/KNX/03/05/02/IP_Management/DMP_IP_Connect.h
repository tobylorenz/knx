// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/08/01/Error_Codes.h>
#include <KNX/03/08/02/Communication_Channel_Id.h>
#include <KNX/03/08/02/Connection_Request_Information.h>
#include <KNX/03/08/02/Connection_Response_Data_Block.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_KNXnet/IP_Connect
 *
 * @ingroup KNX_03_05_02_04_01
 */
class KNX_EXPORT DMP_IP_Connect :
    public std::enable_shared_from_this<DMP_IP_Connect>
{
public:
    explicit DMP_IP_Connect(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DMP_IP_Connect();

    void req(const IP_Host_Protocol_Address_Information dmp_HPAIControlEndpoint, const IP_Host_Protocol_Address_Information dmp_HPAIDataEndpoint, const Connection_Request_Information dmp_CRI);
    std::function<void(const Communication_Channel_Id dmp_CommChannelID, const Status dmp_Status, const IP_Host_Protocol_Address_Information dmp_HPAIServerDataEndpoint, const Connection_Response_Data_Block dmp_CRD, const Status dm_status)> con;

    /* Parameter(s) */
    const IP_Host_Protocol_Address_Information dmp_HPAIControlEndpoint; // in
    const IP_Host_Protocol_Address_Information dmp_HPAIDataEndpoint; // in
    const Connection_Request_Information dmp_CRI; // in
    const Communication_Channel_Id dmp_CommChannelID; // out
    const Status dmp_Status; // out
    const IP_Host_Protocol_Address_Information dmp_HPAIServerDataEndpoint; // out
    const Connection_Response_Data_Block dmp_CRD; // out

    /* Variable(s) */

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    enum class State {
        S00, ///< idle
        S01, ///< sending CONNECT_REQUEST
        S02, ///< waiting CONNECT_RESPONSE
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    void do_CONNECT_REQUEST();
    void do_CONNECT_RESPONSE();
    void do_timer();
};

}
