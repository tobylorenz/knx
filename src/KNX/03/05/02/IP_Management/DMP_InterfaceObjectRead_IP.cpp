// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/IP_Management/DMP_InterfaceObjectRead_IP.h>

namespace KNX {

DMP_InterfaceObjectRead_IP::DMP_InterfaceObjectRead_IP(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_InterfaceObjectRead_IP::~DMP_InterfaceObjectRead_IP()
{
}

void DMP_InterfaceObjectRead_IP::req(
    const Object_Type mpp_Obj_Type,
    const Object_Instance mpp_Obj_Inst,
    const Property_Id mpp_Prop_ID,
    const Property_Value mpp_Prop_Value,
    const PropertyValue_Nr_Of_Elem mpp_Prop_CurrentNr)
{
    // -> DEVICE_CONFIGURATION_REQUEST
    {
        std::shared_ptr<CEMI_M_PropRead_req> cemi_frame = std::make_shared<CEMI_M_PropRead_req>();
        cemi_frame->interface_object_type = mpp_Obj_Type;
        cemi_frame->object_instance = mpp_Obj_Inst;
        cemi_frame->property_id = mpp_Prop_ID;
        cemi_frame->number_of_elements = 1;
        cemi_frame->start_index = 0;
        ip_communication_channel->device_configuration_req(cemi_frame->toData());
    }

    // <- DEVICE_CONFIGURATION_ACK
    //    ip_communication_channel->device_configuration_ack_ind = [](const Device_Configuration_Ack_Frame device_configuration_ack_frame){
    //    };

    // <- DEVICE_CONFIGURATION_REQUEST
    //    ip_communication_channel->device_configuration_ind = [&mpp_Prop_Value, &mpp_Prop_CurrentNr](const Device_Configuration_Request_Frame device_configuration_request_frame){
    //        CEMI_M_PropRead_con cemi_frame;
    //        cemi_frame.fromData(device_configuration_request_frame.cemi_frame_data.cbegin(), device_configuration_request_frame.cemi_frame_data.cend());
    //        mpp_Prop_Value = cemi_frame.data;
    //        mpp_Prop_CurrentNr = cemi_frame.number_of_elements;
    //    };

    // -> DEVICE_CONFIGURATION_ACK
    //    ip_communication_channel->device_configuration_ack_req();

    // -> DEVICE_CONFIGURATION_REQUEST
    {
        std::shared_ptr<CEMI_M_PropRead_req> cemi_frame = std::make_shared<CEMI_M_PropRead_req>();
        cemi_frame->interface_object_type = mpp_Obj_Type;
        cemi_frame->object_instance = mpp_Obj_Inst;
        cemi_frame->property_id = mpp_Prop_ID;
        cemi_frame->number_of_elements = mpp_Obj_NoE;
        cemi_frame->start_index = mpp_Obj_SIx;
        ip_communication_channel->device_configuration_req(cemi_frame->toData());
    }

    // <- DEVICE_CONFIGURATION_ACK
    //    ip_communication_channel->device_configuration_ack_ind = [](const Device_Configuration_Ack_Frame device_configuration_ack_frame){
    //    };

    // <- DEVICE_CONFIGURATION_REQUEST
    //    ip_communication_channel->device_configuration_ind = [&mpp_Prop_Value, &mpp_Prop_CurrentNr](const Device_Configuration_Request_Frame device_configuration_request_frame){
    //        CEMI_M_PropRead_con cemi_frame;
    //        cemi_frame.fromData(device_configuration_request_frame.cemi_frame_data.cbegin(), device_configuration_request_frame.cemi_frame_data.cend());
    //        mpp_Prop_Value = cemi_frame.data;
    //        mpp_Prop_CurrentNr = cemi_frame.number_of_elements;
    //    };

    // -> DEVICE_CONFIGURATION_ACK
    //    ip_communication_channel->device_configuration_ack_req();
}

void DMP_InterfaceObjectRead_IP::do_DEVICE_CONFIGURATION_REQUEST()
{

}

}
