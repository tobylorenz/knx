// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * DMP_InterfaceObjectRead_IP
 *
 * @ingroup KNX_03_05_02_04_03
 */
class KNX_EXPORT DMP_InterfaceObjectRead_IP :
    public std::enable_shared_from_this<DMP_InterfaceObjectRead_IP>
{
public:
    explicit DMP_InterfaceObjectRead_IP(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~DMP_InterfaceObjectRead_IP();

    void req(
        const Object_Type mpp_Obj_Type,
        const Object_Instance mpp_Obj_Inst,
        const Property_Id mpp_Prop_ID,
        const Property_Value mpp_Prop_Value,
        const PropertyValue_Nr_Of_Elem mpp_Prop_CurrentNr);

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    enum class State {
        S00, ///< idle
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    void do_DEVICE_CONFIGURATION_REQUEST();
};

}
