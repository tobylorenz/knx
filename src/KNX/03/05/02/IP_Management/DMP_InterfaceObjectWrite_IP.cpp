// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/IP_Management/DMP_InterfaceObjectWrite_IP.h>

namespace KNX {

DMP_InterfaceObjectWrite_IP::DMP_InterfaceObjectWrite_IP(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

DMP_InterfaceObjectWrite_IP::~DMP_InterfaceObjectWrite_IP()
{
}

void DMP_InterfaceObjectWrite_IP::req(
    const Object_Type mpp_Obj_Type,
    const Object_Instance mpp_Obj_Inst,
    const Property_Id mpp_Prop_ID,
    const Property_Value mpp_Prop_Value,
    const PropertyValue_Start_Index mpp_Prop_StartIndex,
    const PropertyValue_Nr_Of_Elem mpp_Prop_NrOfElem,
    const Error_Code mpp_ErrorCode)
{
    // -> DEVICE_CONFIGURATION_REQUEST
    std::shared_ptr<CEMI_M_PropWrite_req> cemi_frame = std::make_shared<CEMI_M_PropWrite_req>();
    cemi_frame->interface_object_type = mpp_Obj_Type;
    cemi_frame->object_instance = mpp_Obj_Inst;
    cemi_frame->property_id = mpp_Prop_ID;
    cemi_frame->number_of_elements = mpp_Obj_NoE; // or mpp_Prop_NrOfElem?
    cemi_frame->start_index = mpp_Obj_SIx; // or mpp_Prop_StartIndex?
    cemi_frame->data = mpp_Prop_Value;
    ip_communication_channel->device_configuration_req(cemi_frame->toData());

    // <- DEVICE_CONFIGURATION_ACK
    //    ip_communication_channel->device_configuration_ack_ind = [&mpp_ErrorCode](const Device_Configuration_Ack_Frame device_configuration_ack_frame){
    //        mpp_ErrorCode = device_configuration_ack_frame.status;
    //    };

    // <- DEVICE_CONFIGURATION_REQUEST
    //    ip_communication_channel->device_configuration_ind = [&mpp_ErrorCode](const Device_Configuration_Request_Frame device_configuration_request_frame){
    //        CEMI_M_PropWrite_con cemi_frame;
    //        cemi_frame.fromData(device_configuration_request_frame.cemi_frame_data.cbegin(), device_configuration_request_frame.cemi_frame_data.cend());
    //        mpp_ErrorCode = cemi_frame.error_code;
    //    };

    // -> DEVICE_CONFIGURATION_ACK
    //ip_communication_channel->device_configuration_ack_req();
}

void DMP_InterfaceObjectWrite_IP::do_DEVICE_CONFIGURATION_REQUEST()
{

}

}
