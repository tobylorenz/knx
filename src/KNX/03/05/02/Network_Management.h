// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 2 Network Management Procedures */

/* 2.2 NM_IndividualAddress_Read */
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Read.h>

/* 2.3 NM_IndividualAddress_Write */
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Write.h>

/* 2.4 NM_IndividualAddress_SerialNumber_Read */
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Read.h>

/* 2.5 NM_IndividualAddress_SerialNumber_Write */
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Write.h>

/* 2.6 NM_IndividualAddress_SerialNumber_Write2 */
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Write2.h>

/* 2.7 NM_DomainAddress_Read */
#include <KNX/03/05/02/Network_Management/NM_DomainAddress_Read.h>

/* 2.8 NM_DomainAndIndividualAddress_Read */
#include <KNX/03/05/02/Network_Management/NM_DomainAndIndividualAddress_Read.h>

/* 2.9 NM_DomainAndIndividualAddress_Write */
#include <KNX/03/05/02/Network_Management/NM_DomainAndIndividualAddress_Write.h>

/* 2.10 NM_DomainAndIndividualAddress_Write2 */
#include <KNX/03/05/02/Network_Management/NM_DomainAndIndividualAddress_Write2.h>

/* 2.11 NM_DomainAndIndividualAddress_Write3 */
#include <KNX/03/05/02/Network_Management/NM_DomainAndIndividualAddress_Write3.h>

/* 2.12 NM_DomainAddress_Scan2 */
#include <KNX/03/05/02/Network_Management/NM_DomainAddress_Scan2.h>

/* 2.13 NM_Router_Scan */
#include <KNX/03/05/02/Network_Management/NM_Router_Scan.h>

/* 2.14 NM_SubnetworkDevices_Scan */
#include <KNX/03/05/02/Network_Management/NM_SubnetworkDevices_Scan.h>

/* 2.15 NM_IndividualAddress_Reset */
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Reset.h>

/* 2.16 NM_IndividualAddress_Check */
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Check.h>

/* 2.17 NM_SystemNetworkParameter_Read_R */
#include <KNX/03/05/02/Network_Management/NM_SystemNetworkParameter_Read_R.h>

/* 2.17 NM_Read_SerialNumber_by_ProgrammingMode */
#include <KNX/03/05/02/Network_Management/NM_Read_SerialNumber_By_ProgrammingMode.h>

/* 2.17 NM_Read_SerialNumber_By_ExFactoryState */
#include <KNX/03/05/02/Network_Management/NM_Read_SerialNumber_By_ExFactoryState.h>

/* 2.17 NM_Read_SerialNumber_By_PowerReset */
#include <KNX/03/05/02/Network_Management/NM_Read_SerialNumber_By_PowerReset.h>

/* 2.17 Manufacturer specific use of A_SystemNetworkParameter_Read */
#include <KNX/03/05/02/Network_Management/NM_Read_SerialNumber_Manufacturer.h>

/* 2.18 NM_SystemNetworkParameter_Write_R */
#include <KNX/03/05/02/Network_Management/NM_SystemNetworkParameter_Write_R.h>

/* 2.19.1 NM_NetworkParameter_Write_R */
#include <KNX/03/05/02/Network_Management/NM_NetworkParameter_Write_R.h>

/* 2.19.2 NM_IndividualAddress_Check_LocalSubNetwork */
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Check_LocalSubNetwork.h>

/* 2.19.3 NM_IndividualAddress_SerialNumber_Report */
#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Report.h>

/* 2.20.1 NM_NetworkParameter_Read_R */
#include <KNX/03/05/02/Network_Management/NM_NetworkParameter_Read_R.h>

/* 2.20.2 NM_GroupAddress_Scan */
#include <KNX/03/05/02/Network_Management/NM_GroupAddress_Scan.h>

/* 2.20.3 NM_ObjectIndex_Read */
#include <KNX/03/05/02/Network_Management/NM_ObjectIndex_Read.h>

/* 2.21 NM_SerialNumberDefaultIA_Scan */
#include <KNX/03/05/02/Network_Management/NM_SerialNumberDefaultIA_Scan.h>
