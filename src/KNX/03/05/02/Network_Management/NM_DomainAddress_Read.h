// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_DomainAddress_Read
 *
 * @ingroup KNX_03_05_02_02_07
 */
class KNX_EXPORT NM_DomainAddress_Read :
    public std::enable_shared_from_this<NM_DomainAddress_Read>
{
public:
    explicit NM_DomainAddress_Read(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_DomainAddress_Read();

    void req();
    std::function<void(const std::vector<Individual_Address> individual_addresses, const std::vector<Domain_Address> domain_addresses, const Status nm_status)> con;

    /* Parameter(s) */
    std::vector<Individual_Address> individual_addresses{}; // out
    std::vector<Domain_Address> domain_addresses{}; // out
    
private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_DomainAddress_Read.req
        S02, ///< waiting A_DomainAddress_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_DomainAddress_Read_req();
    void A02_A_DomainAddress_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}

