// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_DomainAddress_Scan2.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_DomainAddress_Scan2::NM_DomainAddress_Scan2(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_DomainAddress_Scan2::~NM_DomainAddress_Scan2()
{
}



void NM_DomainAddress_Scan2::req(const Domain_Address_6 mpp_DoA_start, const Domain_Address_6 mpp_DoA_end)
{
    if (state == State::S00) {
        state = State::S01;
        this->mpp_DoA_start = mpp_DoA_start;
        this->mpp_DoA_end = mpp_DoA_end;
        mpp_KNX_SN.clear();
        mpp_IA.clear();
        mpp_DoA_response.clear();
        A01_A_DomainAddressSelective_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_DomainAddress_Scan2::A01_A_DomainAddressSelective_Read_req()
{
    assert(state == State::S01);

    ASDU asdu{};
    asdu.push_back(1); // type
    const std::vector<uint8_t> mpp_DoA_start_data = mpp_DoA_start.toData();
    asdu.insert(std::cend(asdu), std::cbegin(mpp_DoA_start_data), std::cend(mpp_DoA_start_data)); // domain_address_start
    const std::vector<uint8_t> mpp_DoA_end_data = mpp_DoA_end.toData();
    asdu.insert(std::cend(asdu), std::cbegin(mpp_DoA_end_data), std::cend(mpp_DoA_end_data)); // domain_address_end
    asdu.push_back(0); // reserved
    application_layer.A_DomainAddressSelective_Read_req(Priority::system, Network_Layer_Parameter, asdu, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_DomainAddress_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(mpp_KNX_SN, mpp_IA, mpp_DoA_response, Status::not_ok);
            break;
        }
    });
}

void NM_DomainAddress_Scan2::A02_A_DomainAddress_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_DomainAddress_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Domain_Address domain_address) -> void {
        mpp_IA.push_back(asap.address);
        mpp_DoA_response.push_back(domain_address);
        if (state == State::S02) {
            A02_A_DomainAddress_Read_Acon();
            // do not reset timer
        }
    });
}

void NM_DomainAddress_Scan2::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S02:
            state = State::S00;
            con(mpp_KNX_SN, mpp_IA, mpp_DoA_response, Status::ok);
            break;
        default:
            state = State::S00;
            con(mpp_KNX_SN, mpp_IA, mpp_DoA_response, Status::not_ok);
            break;
        }
    });
}

}
