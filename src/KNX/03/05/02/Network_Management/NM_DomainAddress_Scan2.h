// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_DomainAddress_Scan2
 *
 * @ingroup KNX_03_05_02_02_12
 */
class KNX_EXPORT NM_DomainAddress_Scan2 :
    public std::enable_shared_from_this<NM_DomainAddress_Scan2>
{
public:
    explicit NM_DomainAddress_Scan2(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_DomainAddress_Scan2();

    void req(const Domain_Address_6 mpp_DoA_start, const Domain_Address_6 mpp_DoA_end);
    std::function<void(const std::vector<Serial_Number> mpp_KNX_SN, const std::vector<Individual_Address> mpp_IA, const std::vector<Domain_Address> mpp_DoA_response, const Status nm_status)> con;

    /* Parameter(s) */
    Domain_Address_6 mpp_DoA_start{}; // in
    Domain_Address_6 mpp_DoA_end{}; // in
    std::vector<Serial_Number> mpp_KNX_SN{}; // out  // @note Serial Number is not received in any response of the sequence
    std::vector<Individual_Address> mpp_IA{}; // out
    std::vector<Domain_Address> mpp_DoA_response{}; // out

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_DomainAddressSelective_Read.req
        S02, ///< waiting A_DomainAddress_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_DomainAddressSelective_Read_req();
    void A02_A_DomainAddress_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
