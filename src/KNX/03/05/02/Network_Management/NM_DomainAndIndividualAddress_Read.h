// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {


/**
 * NM_DomainAndIndividualAddress_Read
 *
 * @ingroup KNX_03_05_02_02_08
 */
class KNX_EXPORT NM_DomainAndIndividualAddress_Read :
    public std::enable_shared_from_this<NM_DomainAndIndividualAddress_Read>
{
public:
    explicit NM_DomainAndIndividualAddress_Read(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_DomainAndIndividualAddress_Read();

    void req();
    std::function<void(const std::vector<Individual_Address> individual_addresses, const std::vector<Domain_Address> domain_addresses, const Status nm_status)> con;

    /* Parameter(s) */
    std::vector<Individual_Address> individual_addresses{}; // out
    std::vector<Domain_Address> domain_addresses{}; // out

    /* Variable(s) */
    // Individual_Address IA_n{};
    // Domain_Address_2 DoA_n{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_DomainAddress_Read.req
        S02, ///< waiting A_DomainAddress_Read.Acon
        S03, ///< sending A_IndividualAddress_Read.req
        S04, ///< waiting A_IndividualAddress_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_DomainAddress_Read_req();
    void A02_A_DomainAddress_Read_Acon();
    void A03_A_IndividualAddress_Read_req();
    void A04_A_IndividualAddress_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
