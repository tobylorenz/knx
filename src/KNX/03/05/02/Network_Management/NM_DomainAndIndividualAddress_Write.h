// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_DomainAndIndividualAddress_Write
 *
 * @ingroup KNX_03_05_02_02_09
 */
class KNX_EXPORT NM_DomainAndIndividualAddress_Write :
    public std::enable_shared_from_this<NM_DomainAndIndividualAddress_Write>
{
public:
    explicit NM_DomainAndIndividualAddress_Write(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_DomainAndIndividualAddress_Write();

    void req(const Domain_Address DoA_new, const Individual_Address IA_new);
    std::function<void(const Status nm_status)> con;

    /* Parameter(s) */
    Domain_Address DoA_new{}; // in
    Individual_Address IA_new{}; // in

    /* Variable(s) */
    std::vector<Individual_Address> IA_current{};
    std::vector<Domain_Address> DoA_current{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;
    
    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Connect.req
        S02, ///< waiting A_Disconnect.ind
        S03, ///< sending A_DeviceDescriptor_Read.req
        S04, ///< waiting A_DeviceDescriptor_Read.Acon
        S05, ///< sending A_Disconnect.req
        S06, ///< sending A_DomainAddress_Read.req
        S07, ///< waiting A_DomainAddress_Read.Acon
        S08, ///< sending A_DomainAddress_Write.req
        S09, ///< sending A_IndividualAddress_Write.req
        S10, ///< sending A_Connect.req
        S11, ///< sending A_DeviceDescriptor_Read.req
        S12, ///< waiting A_DeviceDescriptor_Read.Acon
        S13, ///< sending A_Restart.req
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Connect_req();
    void A02_A_Disconnect_ind();
    void A03_A_DeviceDescriptor_Read_req();
    void A04_A_DeviceDescriptor_Read_Acon();
    void A05_A_Disconnect_req();
    void A06_A_DomainAddress_Read_req();
    void A07_A_DomainAddress_Read_Acon();
    void A08_A_DomainAddress_Write_req();
    void A09_A_IndividualAddress_Write_req();
    void A10_A_Connect_req();
    void A11_A_DeviceDescriptor_Read_req();
    void A12_A_DeviceDescriptor_Read_Acon();
    void A13_A_Restart_req();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
