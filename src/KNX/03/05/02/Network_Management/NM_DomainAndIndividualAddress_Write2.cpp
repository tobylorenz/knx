// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_DomainAndIndividualAddress_Write2.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_DomainAndIndividualAddress_Write2::NM_DomainAndIndividualAddress_Write2(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_DomainAndIndividualAddress_Write2::~NM_DomainAndIndividualAddress_Write2()
{
}

void NM_DomainAndIndividualAddress_Write2::req(const Domain_Address NmpDoANew, const Individual_Address NmpIANew)
{
    if (state == State::S00) {
        state = State::S01;
        this->NmpDoANew = NmpDoANew;
        this->NmpIANew = NmpIANew;
        NmpIACurrent.clear();
        A01_A_IndividualAddress_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_DomainAndIndividualAddress_Write2::A01_A_IndividualAddress_Read_req()
{
    assert(state == State::S01);

    application_layer.A_DomainAddress_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_IndividualAddress_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(*(std::cbegin(NmpIACurrent)), Status::not_ok);
            break;
        }
    });
}

void NM_DomainAndIndividualAddress_Write2::A02_A_IndividualAddress_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_IndividualAddress_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const Individual_Address individual_address) -> void {
        this->NmpIACurrent.insert(individual_address);
        if (state == State::S02) {
            A02_A_IndividualAddress_Read_Acon();
            // do not reset timer
        }
    });
}

void NM_DomainAndIndividualAddress_Write2::A03_A_DomainAddress_Write_req()
{
    assert(state == State::S03);

    application_layer.A_DomainAddress_Write_req(Ack_Request::dont_care, Priority::normal, Network_Layer_Parameter, NmpDoANew, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            if (NmpIANew != *(std::cbegin(NmpIACurrent))) {
                state = State::S04;
                A04_A_IndividualAddress_Write_req();
                restart_timer(Lcon_timeout);
            } else {
                state = State::S05;
                A05_A_DeviceDescriptor_Read_req();
                restart_timer(Lcon_timeout);
            }
            break;
        case Status::not_ok:
            state = State::S00;
            con(*(std::cbegin(NmpIACurrent)), Status::not_ok);
            break;
        }
    });
}

void NM_DomainAndIndividualAddress_Write2::A04_A_IndividualAddress_Write_req()
{
    assert(state == State::S04);

    application_layer.A_IndividualAddress_Write_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, NmpIANew, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S05;
            A05_A_DeviceDescriptor_Read_req();
            restart_timer(Lcon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(*(std::cbegin(NmpIACurrent)), Status::not_ok);
            break;
        }
    });
}

void NM_DomainAndIndividualAddress_Write2::A05_A_DeviceDescriptor_Read_req()
{
    assert(state == State::S05);

    const ASAP_Connected asap{NmpIANew, true};
    const Descriptor_Type descriptor_type{0};
    application_layer.A_DeviceDescriptor_Read_req(Ack_Request::dont_care, Priority::system, Network_Layer_Parameter, asap, descriptor_type, [this](Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S06;
            A06_A_DeviceDescriptor_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(*(std::cbegin(NmpIACurrent)), Status::not_ok);
            break;
        }
    });
}

void NM_DomainAndIndividualAddress_Write2::A06_A_DeviceDescriptor_Read_Acon()
{
    assert(state == State::S06);

    application_layer.A_DeviceDescriptor_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor) -> void {
        if ((asap.address == NmpIANew) && (descriptor_type == 0)) {
            state = State::S07;
            A07_A_Restart_req();
            restart_timer(Lcon_timeout);
        }
    });
}

void NM_DomainAndIndividualAddress_Write2::A07_A_Restart_req()
{
    assert(state == State::S07);

    const Restart_Channel_Number channel_number{};
    const Restart_Erase_Code erase_code{};
    const Restart_Type restart_type{};
    const ASAP_Individual asap{NmpIANew, false};
    application_layer.A_Restart_req(Ack_Request::dont_care, channel_number, erase_code, Priority::system, Network_Layer_Parameter, restart_type, asap, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S00;
            con(*(std::cbegin(NmpIACurrent)), Status::ok);
            break;
        case Status::not_ok:
            state = State::S00;
            con(*(std::cbegin(NmpIACurrent)), Status::not_ok);
            break;
        }
    });
}

void NM_DomainAndIndividualAddress_Write2::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S02:
            switch(NmpIACurrent.size()) {
            case 0:
                // no device in programming mode
                state = State::S00;
                con(*(std::cbegin(NmpIACurrent)), Status::not_ok);
                break;
            case 1:
                // one device in programming mode
                state = State::S03;
                A03_A_DomainAddress_Write_req();
                restart_timer(Lcon_timeout);
                break;
            default:
                // multiple devices in programming mode
                state = State::S00;
                con(*(std::cbegin(NmpIACurrent)), Status::not_ok);
                break;
            }
            break;
        default:
            state = State::S00;
            con(*(std::cbegin(NmpIACurrent)), Status::not_ok);
            break;
        }
    });
}

}
