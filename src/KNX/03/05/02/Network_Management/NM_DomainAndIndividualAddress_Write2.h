// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <set>

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {
    
/**
 * NM_DomainAndIndividualAddress_Write2
 *
 * @ingroup KNX_03_05_02_02_10
 */
class KNX_EXPORT NM_DomainAndIndividualAddress_Write2 :
    public std::enable_shared_from_this<NM_DomainAndIndividualAddress_Write2>
{
public:
    explicit NM_DomainAndIndividualAddress_Write2(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_DomainAndIndividualAddress_Write2();

    void req(const Domain_Address NmpDoANew, const Individual_Address NmpIANew);
    std::function<void(const Individual_Address NmpIACurrent, const Status nm_status)> con;

    /* Parameter(s) */
    Domain_Address NmpDoANew{}; // in
    Individual_Address NmpIANew{}; // in
    std::set<Individual_Address> NmpIACurrent{}; // out

    /* Variable(s) */
    
private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_IndividualAddress_Read.req
        S02, ///< waiting A_IndividualAddress_Read.Acon
        S03, ///< sending A_DomainAddress_Write.req
        S04, ///< sending A_IndividualAddress_Write.req
        S05, ///< sending A_DeviceDescriptor_Read.req
        S06, ///< waiting A_DeviceDescriptor_Read.Acon
        S07, ///< sending A_Restart.req
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_IndividualAddress_Read_req();
    void A02_A_IndividualAddress_Read_Acon();
    void A03_A_DomainAddress_Write_req();
    void A04_A_IndividualAddress_Write_req();
    void A05_A_DeviceDescriptor_Read_req();
    void A06_A_DeviceDescriptor_Read_Acon();
    void A07_A_Restart_req();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
