// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_DomainAndIndividualAddress_Write3.h>

namespace KNX {

NM_DomainAndIndividualAddress_Write3::NM_DomainAndIndividualAddress_Write3(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_DomainAndIndividualAddress_Write3::~NM_DomainAndIndividualAddress_Write3()
{
}

void NM_DomainAndIndividualAddress_Write3::req()
{
    if (state == State::S00) {
        // always answer with failure
        state = State::S00;
        con(Status::not_ok);
    }
}

}
