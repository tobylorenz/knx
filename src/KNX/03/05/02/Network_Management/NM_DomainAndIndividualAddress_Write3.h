// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_DomainAndIndividualAddress_Write3
 *
 * @note is not yet specified
 *
 * @ingroup KNX_03_05_02_02_11
 */
class KNX_EXPORT NM_DomainAndIndividualAddress_Write3 :
    public std::enable_shared_from_this<NM_DomainAndIndividualAddress_Write3>
{
public:
    explicit NM_DomainAndIndividualAddress_Write3(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_DomainAndIndividualAddress_Write3();

    void req();
    std::function<void(const Status nm_status)> con;

    /* Parameter(s)*/

    /* Variable(s) */

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    //void A01_A_xyz();
    //void restart_timer(const std::chrono::milliseconds duration);
};

}
