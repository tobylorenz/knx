// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_GroupAddress_Scan.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_GroupAddress_Scan::NM_GroupAddress_Scan(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_GroupAddress_Scan::~NM_GroupAddress_Scan()
{
}

void NM_GroupAddress_Scan::req(const Group_Address GA_checked, const uint8_t range_checked)
{
    if (state == State::S00) {
        state = State::S01;
        this->GA_checked = GA_checked;
        this->range_checked = range_checked;
        test_result.clear();
        A01_A_NetworkParameter_Read_req();
    }
}

void NM_GroupAddress_Scan::A01_A_NetworkParameter_Read_req()
{
    assert(state == State::S01);

    const ASAP_Individual asap{KNX::Individual_Address(0x0000), false};
    parameter_type = {OT_ADDRESS_TABLE, Group_Address_Table::PID_TABLE};
    test_info.clear();
    test_info.push_back(range_checked);
    test_info.push_back(GA_checked >> 8);
    test_info.push_back(GA_checked & 0xff);
    application_layer.A_NetworkParameter_Read_req(asap, Comm_Mode::Broadcast, Network_Layer_Parameter, parameter_type, Priority::low, test_info, [this](const Status a_status) -> void {
        switch(a_status) {
            case Status::ok:
                state = State::S02;
                A02_A_NetworkParameter_Read_Acon();
                restart_timer(Acon_timeout);
                break;
            case Status::not_ok:
                state = State::S00;
                con(test_result, Status::not_ok);
                break;
            }
    });
}

void NM_GroupAddress_Scan::A02_A_NetworkParameter_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_NetworkParameter_Read_Acon([this](const ASAP_Individual /*asap*/, const Hop_Count_Type /*hop_count_type*/, const Individual_Address /*individual_address*/, const Parameter_Type parameter_type, const Priority /*priority*/, const Parameter_Test_Info_Result test_info_result) -> void {
        if (parameter_type == this->parameter_type) {
            const Parameter_Test_Info test_info(std::cbegin(test_info_result), std::cbegin(test_info_result) + this->test_info.size());
            if (test_info == this->test_info) {
                const Parameter_Test_Result one_test_result(std::cbegin(test_info_result) + this->test_info.size(), std::cend(test_info_result));
                test_result.push_back(one_test_result);
            }
        }
        if (state == State::S02) {
            A02_A_NetworkParameter_Read_Acon();
            restart_timer(Acon_timeout);
        }
    });
}

void NM_GroupAddress_Scan::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S02:
            state = State::S00;
            con(test_result, Status::ok);
            break;
        default:
            state = State::S00;
            con(test_result, Status::not_ok);
            break;
        }
    });
}

}
