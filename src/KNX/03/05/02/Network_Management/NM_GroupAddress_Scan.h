// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_GroupAddress_Scan
 *
 * @ingroup KNX_03_05_02_02_20_02
 */
class KNX_EXPORT NM_GroupAddress_Scan :
    public std::enable_shared_from_this<NM_GroupAddress_Scan>
{
public:
    explicit NM_GroupAddress_Scan(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_GroupAddress_Scan();

    void req(const Group_Address GA_checked, const uint8_t range_checked);
    std::function<void(const std::vector<Parameter_Test_Result> test_result, const Status nm_status)> con;

    /* Parameter(s) */
    Group_Address GA_checked{}; // in
    uint8_t range_checked{}; // in
    std::vector<Parameter_Test_Result> test_result{}; // out

    /* Variable(s) */
    Parameter_Type parameter_type{};
    Parameter_Test_Info test_info{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_NetworkParameter_Read.req
        S02, ///< waiting A_NetworkParameter_Read.Acon
    };
    State state { State::S00 };
    
    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_NetworkParameter_Read_req();
    void A02_A_NetworkParameter_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
