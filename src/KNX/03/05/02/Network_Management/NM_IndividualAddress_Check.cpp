// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Check.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_IndividualAddress_Check::NM_IndividualAddress_Check(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_IndividualAddress_Check::~NM_IndividualAddress_Check()
{
}

void NM_IndividualAddress_Check::req(const Individual_Address IA_test)
{
    if (state == State::S00) {
        state = State::S01;
        this->IA_test = IA_test;
        result = Result::NotOccupied;
        DDType = Descriptor_Type();
        DDx.clear();
        A01_A_Connect_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_IndividualAddress_Check::A01_A_Connect_req()
{
    assert(state == State::S01);

    const ASAP_Connected asap{IA_test, true};
    application_layer.A_Connect_req(asap, Priority::system, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_Disconnect_ind();
            restart_timer(ind_timeout);
            break;
        case Status::not_ok:
            // IA_test occupied
            state = State::S00;
            result = Result::Occupied;
            con(result, DDType, DDx, Status::ok);
            break;
        }
    });
}

void NM_IndividualAddress_Check::A02_A_Disconnect_ind()
{
    assert(state == State::S02);

    application_layer.A_Disconnect_ind([this](const ASAP_Connected asap) -> void {
        if (asap.address == IA_test) {
            // IA_test is occupied and does not support Transport Layer connections
            state = State::S00;
            result = Result::Occupied;
            con(result, DDType, DDx, Status::ok);
        }
    });
}

void NM_IndividualAddress_Check::A03_A_DeviceDescriptor_Read_req()
{
    assert(state == State::S03);

    const ASAP_Connected asap{IA_test, true};
    const Descriptor_Type descriptor_type{0};
    application_layer.A_DeviceDescriptor_Read_req(Ack_Request::dont_care, Priority::system, Network_Layer_Parameter, asap, descriptor_type, [this](Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S04;
            A04_A_DeviceDescriptor_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(result, DDType, DDx, Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_Check::A04_A_DeviceDescriptor_Read_Acon()
{
    assert(state == State::S04);

    application_layer.A_DeviceDescriptor_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor) -> void {
        if ((asap.address == IA_test) && (descriptor_type == 0)) {
            // IA_test is occupied
            state = State::S05;
            result = Result::Occupied;
            DDType = descriptor_type;
            DDx = device_descriptor;
            A05_A_Disconnect_req();
            restart_timer(Lcon_timeout);
        }
    });
}

void NM_IndividualAddress_Check::A05_A_Disconnect_req()
{
    assert(state == State::S05);

    const ASAP_Connected asap{IA_test, true};
    application_layer.A_Disconnect_req(Priority::system, asap, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S00;
            con(result, DDType, DDx, Status::ok);
            break;
        case Status::not_ok:
            state = State::S00;
            con(result, DDType, DDx, Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_Check::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S02:
            state = State::S03;
            A03_A_DeviceDescriptor_Read_req();
            restart_timer(Lcon_timeout);
            break;
        case State::S04:
            state = State::S05;
            A05_A_Disconnect_req();
            restart_timer(Lcon_timeout);
            break;
        default:
            state = State::S00;
            con(result, DDType, DDx, Status::not_ok);
            break;
        }
    });
}

}
