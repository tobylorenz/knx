// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {
    
/**
 * NM_IndividualAddress_Check
 *
 * also been named NM_IndividualAddress_Scan
 *
 * @ingroup KNX_03_05_02_02_16
 */
class KNX_EXPORT NM_IndividualAddress_Check :
    public std::enable_shared_from_this<NM_IndividualAddress_Check>
{
public:
    enum class Result {
        Occupied, ///< occupied
        NotOccupied ///< not occupied
    };

    explicit NM_IndividualAddress_Check(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_IndividualAddress_Check();

    void req(const Individual_Address IA_test);
    std::function<void(const Result result, const Descriptor_Type DDType, const Device_Descriptor_Data DDx, const Status nm_status)> con;

    /* Parameter(s) */
    Individual_Address IA_test{}; // in
    Result result{}; // out
    Descriptor_Type DDType{}; // out
    Device_Descriptor_Data DDx{}; // out

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Connect.req
        S02, ///< receiving A_Disconnect.ind
        S03, ///< sending A_DeviceDescriptor_Read.req
        S04, ///< waiting A_DeviceDescriptor_Read.Acon
        S05, ///< sending A_Disconnect.req
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Connect_req();
    void A02_A_Disconnect_ind();
    void A03_A_DeviceDescriptor_Read_req();
    void A04_A_DeviceDescriptor_Read_Acon();
    void A05_A_Disconnect_req();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
