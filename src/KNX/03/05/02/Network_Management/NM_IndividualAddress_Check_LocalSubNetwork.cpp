// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Check_LocalSubNetwork.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/00_Device/Device_Object.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_IndividualAddress_Check_LocalSubNetwork::NM_IndividualAddress_Check_LocalSubNetwork(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_IndividualAddress_Check_LocalSubNetwork::~NM_IndividualAddress_Check_LocalSubNetwork()
{
}


void NM_IndividualAddress_Check_LocalSubNetwork::req(const Individual_Address PPPP)
{
    if (state == State::S00) {
        state = State::S01;
        this->PPPP = PPPP;
        result = Result::NotOccupied;
        A01_A_NetworkParameter_Write_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_IndividualAddress_Check_LocalSubNetwork::A01_A_NetworkParameter_Write_req()
{
    assert(state == State::S01);

    const ASAP_Individual asap{PPPP, false};
    const Parameter_Type parameter_type{OT_DEVICE, Device_Object::PID_ADDR_CHECK};
    const Parameter_Value value{0x00};
    application_layer.A_NetworkParameter_Write_req(asap, Comm_Mode::Individual, Hop_Count_Type(0), parameter_type, Priority::low, value, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            // IA PPPP is Occupied
            state = State::S00;
            result = Result::Occupied;
            con(result, Status::ok);
            break;
        case Status::not_ok:
            // IA PPPP is Not Occupied
            state = State::S00;
            result = Result::NotOccupied;
            con(result, Status::ok);
            break;
        }
    });
};

void NM_IndividualAddress_Check_LocalSubNetwork::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(result, Status::not_ok);
            break;
        }
    });
}

}
