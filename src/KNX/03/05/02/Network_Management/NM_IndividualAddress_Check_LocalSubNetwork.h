// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_IndividualAddress_Check_LocalSubNetwork
 *
 * @ingroup KNX_03_05_02_02_19_02
 */
class KNX_EXPORT NM_IndividualAddress_Check_LocalSubNetwork :
    public std::enable_shared_from_this<NM_IndividualAddress_Check_LocalSubNetwork>
{
public:
    enum class Result {
        Occupied, ///< occupied
        NotOccupied ///< not occupied
    };

    explicit NM_IndividualAddress_Check_LocalSubNetwork(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_IndividualAddress_Check_LocalSubNetwork();

    void req(const Individual_Address PPPP);
    std::function<void(const Result result, const Status nm_status)> con;

    /* Parameter(s) */
    Individual_Address PPPP{}; // in
    Result result{}; // out

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_NetworkParameter_Write.req
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_NetworkParameter_Write_req();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
