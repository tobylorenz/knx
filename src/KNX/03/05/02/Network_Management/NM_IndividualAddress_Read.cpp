// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Read.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon
    
NM_IndividualAddress_Read::NM_IndividualAddress_Read(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_IndividualAddress_Read::~NM_IndividualAddress_Read()
{
}

void NM_IndividualAddress_Read::req()
{
    if (state == State::S00) {
        state = State::S01;
        individual_addresses.clear();
        A01_A_IndividualAddress_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_IndividualAddress_Read::A01_A_IndividualAddress_Read_req()
{
    assert(state == State::S01);

    application_layer.A_IndividualAddress_Read_req(Ack_Request::dont_care, Priority::normal, Network_Layer_Parameter, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_IndividualAddress_Read_Acon();
            restart_timer(std::chrono::milliseconds(3000)); // spec: "time-out: 3 s"
            break;
        case Status::not_ok:
            state = State::S00;
            con(individual_addresses, Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_Read::A02_A_IndividualAddress_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_IndividualAddress_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const Individual_Address individual_address) -> void {
        this->individual_addresses.insert(individual_address);
        if (state == State::S02) {
            A02_A_IndividualAddress_Read_Acon();
            // do not reset timer
        }
    });
}

void NM_IndividualAddress_Read::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S02:
            state = State::S00;
            con(individual_addresses, Status::ok);
            break;
        default:
            state = State::S00;
            con(individual_addresses, Status::not_ok);
            break;
        }
    });
}

}
