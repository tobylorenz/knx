// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_IndividualAddress_Read
 *
 * @ingroup KNX_03_05_02_02_02
 */
class KNX_EXPORT NM_IndividualAddress_Read :
    public std::enable_shared_from_this<NM_IndividualAddress_Read>
{
public:
    explicit NM_IndividualAddress_Read(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_IndividualAddress_Read();

    void req();
    std::function<void(std::set<Individual_Address> individual_addresses, Status nm_status)> con;

    /* Parameter(s) */
    std::set<Individual_Address> individual_addresses{}; // out

    /* Service parameter(s) */
    // none

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    enum class State {
        S00, ///< idle
        S01, ///< sending A_IndividualAddress_Read.req
        S02, ///< waiting A_IndividualAddress_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_IndividualAddress_Read_req();
    void A02_A_IndividualAddress_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
