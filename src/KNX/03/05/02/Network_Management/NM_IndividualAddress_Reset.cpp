// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_Reset.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_IndividualAddress_Reset::NM_IndividualAddress_Reset(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_IndividualAddress_Reset::~NM_IndividualAddress_Reset()
{
}

void NM_IndividualAddress_Reset::req()
{
    if (state == State::S00) {
        state = State::S01;
        A01_A_IndividualAddress_Write_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_IndividualAddress_Reset::A01_A_IndividualAddress_Write_req()
{
    assert(state == State::S01);

    application_layer.A_IndividualAddress_Write_req(Ack_Request::dont_care, Priority::system, Network_Layer_Parameter, Individual_Address(0xFFFF), [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_Connect_req();
            restart_timer(Lcon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_Reset::A02_A_Connect_req()
{
    assert(state == State::S02);

    const ASAP_Connected asap{Individual_Address(0xFFFF), true};
    application_layer.A_Connect_req(asap, Priority::system, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S03;
            A03_A_Restart_req();
            restart_timer(Lcon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_Reset::A03_A_Restart_req()
{
    assert(state == State::S03);

    const Restart_Channel_Number channel_number{};
    const Restart_Erase_Code erase_code{};
    const Restart_Type restart_type{};
    const ASAP_Individual asap{Individual_Address(0xFFFF), false};
    application_layer.A_Restart_req(Ack_Request::dont_care, channel_number, erase_code, Priority::system, Network_Layer_Parameter, restart_type, asap, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S04;
            A04_A_Disconnect_req();
            restart_timer(Lcon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_Reset::A04_A_Disconnect_req()
{
    assert(state == State::S04);

    const ASAP_Connected asap{Individual_Address(0xFFFF), true};
    application_layer.A_Disconnect_req(Priority::system, asap, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S05;
            A05_A_IndividualAddress_Read_req();
            restart_timer(Lcon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_Reset::A05_A_IndividualAddress_Read_req()
{
    assert(state == State::S05);

    application_layer.A_IndividualAddress_Read_req(Ack_Request::dont_care, Priority::system, Network_Layer_Parameter, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S06;
            A06_A_IndividualAddress_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_Reset::A06_A_IndividualAddress_Read_Acon()
{
    assert(state == State::S06);

    application_layer.A_IndividualAddress_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const Individual_Address /*individual_address*/) -> void {
        state = State::S01;
        A01_A_IndividualAddress_Write_req();
    });
}

void NM_IndividualAddress_Reset::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancels pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S06:
            state = State::S00;
            con(Status::ok);
            break;
        default:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

}
