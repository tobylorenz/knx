// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {
    
/**
 * NM_IndividualAddress_Reset
 *
 * @ingroup KNX_03_05_02_02_15
 */
class KNX_EXPORT NM_IndividualAddress_Reset :
    public std::enable_shared_from_this<NM_IndividualAddress_Reset>
{
public:
    explicit NM_IndividualAddress_Reset(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_IndividualAddress_Reset();

    void req();
    std::function<void(const Status nm_status)> con;

    /* Parameter(s) */

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_IndividualAddress_Write.req
        S02, ///< sending A_Connect.req
        S03, ///< sending A_Restart.req
        S04, ///< sending A_Disconnect.req
        S05, ///< sending A_IndividualAddress_Read.req
        S06, ///< waiting A_IndividualAddress_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_IndividualAddress_Write_req();
    void A02_A_Connect_req();
    void A03_A_Restart_req();
    void A04_A_Disconnect_req();
    void A05_A_IndividualAddress_Read_req();
    void A06_A_IndividualAddress_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
