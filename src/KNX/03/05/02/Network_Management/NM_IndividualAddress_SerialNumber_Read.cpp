// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Read.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_IndividualAddress_SerialNumber_Read::NM_IndividualAddress_SerialNumber_Read(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_IndividualAddress_SerialNumber_Read::~NM_IndividualAddress_SerialNumber_Read()
{
}

void NM_IndividualAddress_SerialNumber_Read::req(const Serial_Number SN_Device)
{
    if (state == State::S00) {
        state = State::S01;
        this->SN_Device = SN_Device;
        DoA_current = 0;
        IA_current = 0;
        A01_A_IndividualAddressSerialNumber_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_IndividualAddress_SerialNumber_Read::A01_A_IndividualAddressSerialNumber_Read_req()
{
    assert(state == State::S01);

    application_layer.A_IndividualAddressSerialNumber_Read_req(Ack_Request::dont_care, Priority::normal, Network_Layer_Parameter, SN_Device, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_IndividualAddressSerialNumber_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(DoA_current, IA_current, Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_SerialNumber_Read::A02_A_IndividualAddressSerialNumber_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_IndividualAddressSerialNumber_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const Serial_Number serial_number, const Individual_Address individual_address, const Domain_Address_2 domain_address) -> void {
        if (SN_Device == serial_number) {
            state = State::S00;
            IA_current = individual_address;
            DoA_current = domain_address;
            con(DoA_current, IA_current, Status::ok);
        }
    });
}

void NM_IndividualAddress_SerialNumber_Read::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(DoA_current, IA_current, Status::not_ok);
            break;
        }
    });
}

}
