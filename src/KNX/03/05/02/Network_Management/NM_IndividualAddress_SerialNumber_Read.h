// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_IndividualAddress_SerialNumber_Read
 *
 * @ingroup KNX_03_05_02_02_04
 */
class KNX_EXPORT NM_IndividualAddress_SerialNumber_Read :
    public std::enable_shared_from_this<NM_IndividualAddress_SerialNumber_Read>
{
public:
    explicit NM_IndividualAddress_SerialNumber_Read(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_IndividualAddress_SerialNumber_Read();

    void req(const Serial_Number SN_Device);
    std::function<void(Domain_Address_2 DoA_current, Individual_Address IA_current, Status nm_status)> con;

    /* Parameter(s) */
    Serial_Number SN_Device{}; // in
    Domain_Address_2 DoA_current{}; // out
    Individual_Address IA_current{}; // out

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_IndividualAddressSerialNumber_Read.req
        S02, ///< waiting A_IndividualAddressSerialNumber_Read.Acon
    };
    State state { State::S00 };
    
    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_IndividualAddressSerialNumber_Read_req();
    void A02_A_IndividualAddressSerialNumber_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
