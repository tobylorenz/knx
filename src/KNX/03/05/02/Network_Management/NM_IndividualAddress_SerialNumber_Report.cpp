// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Report.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/00_Device/Device_Object.h>

namespace KNX {

NM_IndividualAddress_SerialNumber_Report::NM_IndividualAddress_SerialNumber_Report(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer)
{
}

NM_IndividualAddress_SerialNumber_Report::~NM_IndividualAddress_SerialNumber_Report()
{
}

void NM_IndividualAddress_SerialNumber_Report::start()
{
    if (state == State::S00) {
        state = State::S01;
        Device_SN = Serial_Number();
        A01_A_NetworkParameter_Write_ind();
    }
}

void NM_IndividualAddress_SerialNumber_Report::stop()
{
    state = State::S00;
}

void NM_IndividualAddress_SerialNumber_Report::A01_A_NetworkParameter_Write_ind()
{
    assert(state == State::S01);

    application_layer.A_NetworkParameter_Write_ind([this](const ASAP_Individual /*asap*/, const Parameter_Type parameter_type, const Priority /*priority*/, const Parameter_Value value) -> void {
        if ((parameter_type.object_type == OT_DEVICE) && (parameter_type.pid == Device_Object::PID_ADDR_REPORT)) {
            std::copy(std::cbegin(value), std::cbegin(value) + 6, std::begin(Device_SN.serial_number));
            ind(Device_SN);
        }
        if (state == State::S01) {
            A01_A_NetworkParameter_Write_ind();
        }
    });
}

}
