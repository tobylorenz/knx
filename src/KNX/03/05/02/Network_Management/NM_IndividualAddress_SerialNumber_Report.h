// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {
    
/**
 * NM_IndividualAddress_SerialNumber_Report
 *
 * @ingroup KNX_03_05_02_02_19_03
 */
class KNX_EXPORT NM_IndividualAddress_SerialNumber_Report :
    public std::enable_shared_from_this<NM_IndividualAddress_SerialNumber_Report>
{
public:
    explicit NM_IndividualAddress_SerialNumber_Report(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_IndividualAddress_SerialNumber_Report();

    void start();
    void stop();
    std::function<void(const Serial_Number Device_SN)> ind;

    /* Parameter(s) */
    Serial_Number Device_SN{}; // out

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    enum class State {
        S00, ///< idle
        S01, ///< waiting A_NetworkParameter_Write.ind
    };
    State state { State::S00 };

    /* actions */
    void A01_A_NetworkParameter_Write_ind();
};

}

