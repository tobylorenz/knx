// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_IndividualAddress_SerialNumber_Write
 *
 * @ingroup KNX_03_05_02_02_05
 */
class KNX_EXPORT NM_IndividualAddress_SerialNumber_Write :
    public std::enable_shared_from_this<NM_IndividualAddress_SerialNumber_Write>
{
public:
    explicit NM_IndividualAddress_SerialNumber_Write(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_IndividualAddress_SerialNumber_Write();

    void req(const Serial_Number SN_Device, const Individual_Address IA_new);
    std::function<void(const Domain_Address_2 DoA_Device, const Status nm_status)> con;

    /* Parameter(s) */
    Serial_Number SN_Device{}; // in
    Individual_Address IA_new{}; // in
    Domain_Address_2 DoA_Device{}; // out

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_IndividualAddressSerialNumber_Write.req
        S02, ///< sending A_IndividualAddressSerialNumber_Read.req
        S03, ///< waiting A_IndividualAddressSerialNumber_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_IndividualAddressSerialNumber_Write_req();
    void A02_A_IndividualAddressSerialNumber_Read_req();
    void A03_A_IndividualAddressSerialNumber_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
