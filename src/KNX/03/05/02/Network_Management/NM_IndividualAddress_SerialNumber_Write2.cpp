// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_IndividualAddress_SerialNumber_Write2.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_IndividualAddress_SerialNumber_Write2::NM_IndividualAddress_SerialNumber_Write2(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_IndividualAddress_SerialNumber_Write2::~NM_IndividualAddress_SerialNumber_Write2()
{
}

void NM_IndividualAddress_SerialNumber_Write2::req(const Serial_Number serial_number, const Individual_Address new_address)
{
    if (state == State::S00) {
        state = State::S01;
        this->serial_number = serial_number;
        this->new_address = new_address;
        device_descriptor = Device_Descriptor_Type_2();
        A01_A_IndividualAddressSerialNumber_Write_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_IndividualAddress_SerialNumber_Write2::A01_A_IndividualAddressSerialNumber_Write_req()
{
    assert(state == State::S01);

    application_layer.A_IndividualAddressSerialNumber_Write_req(Ack_Request::dont_care, Priority::system, Network_Layer_Parameter, serial_number, new_address, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_IndividualAddressSerialNumber_Read_req();
            restart_timer(Lcon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(device_descriptor, Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_SerialNumber_Write2::A02_A_IndividualAddressSerialNumber_Read_req()
{
    assert(state == State::S02);

    application_layer.A_IndividualAddressSerialNumber_Read_req(Ack_Request::dont_care, Priority::system, Network_Layer_Parameter, serial_number, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S03;
            A03_A_IndividualAddressSerialNumber_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(device_descriptor, Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_SerialNumber_Write2::A03_A_IndividualAddressSerialNumber_Read_Acon()
{
    assert(state == State::S03);

    application_layer.A_IndividualAddressSerialNumber_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const Serial_Number serial_number, const Individual_Address individual_address, const Domain_Address_2 /*domain_address*/) -> void {
        if (serial_number == this->serial_number) {
            if (individual_address == new_address) {
                state = State::S04;
                A04_A_DeviceDescriptor_Read_req();
                restart_timer(Lcon_timeout);
            } else {
                // Different answer received
                state = State::S00;
                con(device_descriptor, Status::not_ok);
            }
        }
    });
}

void NM_IndividualAddress_SerialNumber_Write2::A04_A_DeviceDescriptor_Read_req()
{
    assert(state == State::S04);

    const ASAP_Individual asap{new_address, false};
    const Descriptor_Type descriptor_type{2};
    application_layer.A_DeviceDescriptor_Read_req(Ack_Request::dont_care, Priority::system, Network_Layer_Parameter, asap, descriptor_type, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S05;
            A05_A_DeviceDescriptor_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(device_descriptor, Status::not_ok);
            break;
        }
    });
}

void NM_IndividualAddress_SerialNumber_Write2::A05_A_DeviceDescriptor_Read_Acon()
{
    assert(state == State::S05);

    application_layer.A_DeviceDescriptor_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Descriptor_Type descriptor_type, const Device_Descriptor_Data device_descriptor_data) -> void {
        if ((asap.address == new_address) && (descriptor_type == 2)) {
            state = State::S00;
            device_descriptor.fromData(std::cbegin(device_descriptor_data), std::cend(device_descriptor_data));
            con(device_descriptor, Status::ok);
        }
    });
}

void NM_IndividualAddress_SerialNumber_Write2::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(device_descriptor, Status::not_ok);
            break;
        }
    });
}

}
