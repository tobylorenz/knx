// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/Device_Descriptor_Type_2.h>
#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_IndividualAddress_SerialNumber_Write2
 *
 * @ingroup KNX_03_05_02_02_06
 */
class KNX_EXPORT NM_IndividualAddress_SerialNumber_Write2 :
    public std::enable_shared_from_this<NM_IndividualAddress_SerialNumber_Write2>
{
public:
    explicit NM_IndividualAddress_SerialNumber_Write2(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_IndividualAddress_SerialNumber_Write2();

    void req(const Serial_Number serial_number, const Individual_Address new_address);
    std::function<void(const Device_Descriptor_Type_2 device_descriptor, const Status nm_status)> con;

    /* Parameter(s) */
    Serial_Number serial_number{}; // in
    Individual_Address new_address{}; // in
    Device_Descriptor_Type_2 device_descriptor{}; // out

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_IndividualAddressSerialNumber_Write.req
        S02, ///< sending A_IndividualAddressSerialNumber_Read.req
        S03, ///< waiting A_IndividualAddressSerialNumber_Read.Acon
        S04, ///< sending A_DeviceDescriptor_Read.req
        S05, ///< waiting A_DeviceDescriptor_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_IndividualAddressSerialNumber_Write_req();
    void A02_A_IndividualAddressSerialNumber_Read_req();
    void A03_A_IndividualAddressSerialNumber_Read_Acon();
    void A04_A_DeviceDescriptor_Read_req();
    void A05_A_DeviceDescriptor_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
