// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {
    
/**
 * NM_NetworkParameter_Read_R
 *
 * @ingroup KNX_03_05_02_02_20_01
 */
class KNX_EXPORT NM_NetworkParameter_Read_R :
    public std::enable_shared_from_this<NM_NetworkParameter_Read_R>
{
public:
    explicit NM_NetworkParameter_Read_R(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_NetworkParameter_Read_R();

    void req(const ASAP_Individual asap, const Hop_Count_Type hop_count_type_req, const Object_Type object_type, const Property_Id PID, const Parameter_Test_Info test_info, const Comm_Mode comm_mode_req, const Comm_Mode comm_mode_res, const Hop_Count_Type hop_count_type_res);
    std::function<void(const std::vector<Parameter_Test_Result> result_data, const Status nm_status)> con;

    /* Parameter(s) */
    ASAP_Individual asap{}; // in
    Hop_Count_Type hop_count_type_req{}; // in
    Object_Type object_type{}; // in
    Property_Id PID{}; // in
    Parameter_Test_Info test_info{}; // in
    Comm_Mode comm_mode_req{}; // in
    Comm_Mode comm_mode_res{}; // in
    Hop_Count_Type hop_count_type_res{}; // in
    std::vector<Parameter_Test_Result> result_data{}; // out

    /* Service parameter(s) */
    // Parameter_Test_Info test_info{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_NetworkParameter_Read.req
        S02, ///< waiting A_NetworkParameter_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_NetworkParameter_Read_req();
    void A02_A_NetworkParameter_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
