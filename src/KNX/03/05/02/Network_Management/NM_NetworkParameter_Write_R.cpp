// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_NetworkParameter_Write_R.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_NetworkParameter_Write_R::NM_NetworkParameter_Write_R(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_NetworkParameter_Write_R::~NM_NetworkParameter_Write_R()
{
}

void NM_NetworkParameter_Write_R::req(const ASAP_Individual asap, const Comm_Mode comm_mode, const Hop_Count_Type hop_count_type_req, const Object_Type object_type, const Property_Id PID, Priority priority, const Parameter_Value value)
{
    if (state == State::S00) {
        state = State::S01;
        this->asap = asap;
        this->comm_mode = comm_mode;
        this->hop_count_type_req = hop_count_type_req;
        this->object_type = object_type;
        this->PID = PID;
        this->priority = priority;
        this->value = value;
        A01_A_NetworkParameter_Write();
        restart_timer(Lcon_timeout);
    }
}

void NM_NetworkParameter_Write_R::A01_A_NetworkParameter_Write()
{
    assert(state == State::S01);

    const Parameter_Type parameter_type{object_type, PID};
    application_layer.A_NetworkParameter_Write_req(asap, comm_mode, hop_count_type_req, parameter_type, priority, value, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S00;
            con(Status::ok);
            break;
        case Status::not_ok:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
};

void NM_NetworkParameter_Write_R::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        default:
            state = State::S00;
            con(Status::not_ok);
            break;
        }
    });
}

}
