// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_ObjectIndex_Read
 *
 * @ingroup KNX_03_05_02_02_20_03
 */
class KNX_EXPORT NM_ObjectIndex_Read :
public std::enable_shared_from_this<NM_ObjectIndex_Read>
{
public:
    explicit NM_ObjectIndex_Read(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_ObjectIndex_Read();

    void req(const ASAP_Individual asap, const Comm_Mode comm_mode_req, const Object_Type object_type, const Property_Id PID, const Parameter_Test_Info test_info, const Comm_Mode comm_mode_res);
    std::function<void(const std::vector<Parameter_Test_Result> test_result, const Status nm_status)> con;

    /* Parameter(s) */
    ASAP_Individual asap{}; // in
    Comm_Mode comm_mode_req{}; // in
    // comm_mode_req = Comm_Mode::Individual
    Object_Type object_type{}; // in
    Property_Id PID{}; // in
    // PID = Interface_Object::PID_OBJECT_INDEX
    Parameter_Test_Info test_info{}; // in
    // test_info[0] = start_instance_of_object_type
    // test_info[1] = number_of_instances
    std::vector<Parameter_Test_Result> test_result{}; // out
    Comm_Mode comm_mode_res{}; // in
    // comm_mode_res = Comm_Mode::Individual
 
    /* Variable(s) */
    Parameter_Type parameter_type{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_NetworkParameter_Read.req
        S02, ///< waiting A_NetworkParameter_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_NetworkParameter_Read_req();
    void A02_A_NetworkParameter_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}

