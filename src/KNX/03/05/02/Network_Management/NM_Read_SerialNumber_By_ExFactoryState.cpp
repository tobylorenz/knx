// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_Read_SerialNumber_By_ExFactoryState.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_Read_SerialNumber_By_ExFactoryState::NM_Read_SerialNumber_By_ExFactoryState(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_Read_SerialNumber_By_ExFactoryState::~NM_Read_SerialNumber_By_ExFactoryState()
{
}

void NM_Read_SerialNumber_By_ExFactoryState::req(const uint8_t wait_time)
{
    if (state == State::S00) {
        state = State::S01;
        test_info[1] = wait_time;
        mpp_KNX_Serial_Number.clear();
        A01_A_SystemNetworkParameter_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_Read_SerialNumber_By_ExFactoryState::A01_A_SystemNetworkParameter_Read_req()
{
    assert(state == State::S01);

    application_layer.A_SystemNetworkParameter_Read_req(Network_Layer_Parameter, parameter_type, Priority::low, test_info, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_SystemNetworkParameter_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(mpp_KNX_Serial_Number, Status::not_ok);
            break;
        }
    });
    restart_timer(Lcon_timeout);
}

void NM_Read_SerialNumber_By_ExFactoryState::A02_A_SystemNetworkParameter_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_SystemNetworkParameter_Read_Acon([this](const ASAP_Individual /*asap*/, const Hop_Count_Type /*hop_count_type*/, const System_Parameter_Type parameter_type, const Priority /*priority*/, const Parameter_Test_Info_Result test_info_result) -> void {
        if (parameter_type == this->parameter_type) {
            const Parameter_Test_Info test_info(std::cbegin(test_info_result), std::cbegin(test_info_result) + this->test_info.size());
            if (test_info == this->test_info) {
                const Parameter_Test_Result test_result(std::cbegin(test_info_result) + this->test_info.size(), std::cend(test_info_result));
                Serial_Number serial_number;
                std::copy(std::cbegin(test_result), std::cbegin(test_result) + 6, std::end(serial_number.serial_number));
                mpp_KNX_Serial_Number.push_back(serial_number);
            }
        }
        if (state == State::S02) {
            A02_A_SystemNetworkParameter_Read_Acon();
            restart_timer(Acon_timeout);
        }
    });
}

void NM_Read_SerialNumber_By_ExFactoryState::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S02:
            state = State::S00;
            con(mpp_KNX_Serial_Number, Status::ok);
            break;
        default:
            state = State::S00;
            con(mpp_KNX_Serial_Number, Status::not_ok);
            break;
        }
    });
}

}
