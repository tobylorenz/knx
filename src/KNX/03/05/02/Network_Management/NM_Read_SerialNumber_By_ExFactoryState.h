// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/05/01/Interface_Object.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_Read_SerialNumber_By_ExFactoryState
 *
 * @ingroup KNX_03_05_02_02_17
 */
class KNX_EXPORT NM_Read_SerialNumber_By_ExFactoryState :
    public std::enable_shared_from_this<NM_Read_SerialNumber_By_ExFactoryState>
{
public:
    explicit NM_Read_SerialNumber_By_ExFactoryState(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_Read_SerialNumber_By_ExFactoryState();

    void req(const uint8_t wait_time);
    std::function<void(const std::vector<Serial_Number> mpp_KNX_Serial_Number, const Status nm_status)> con;

    /* Parameter(s) */
    uint8_t wait_time{}; // 0 s to 255 s // @todo own type
    std::vector<Serial_Number> mpp_KNX_Serial_Number{}; // out

    /* Variable(s) */
    const System_Parameter_Type parameter_type{OT_DEVICE, Interface_Object::PID_SERIAL_NUMBER};
    Parameter_Test_Info test_info{0x02, 0};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_SystemNetworkParameter_Read.req
        S02, ///< waiting A_SystemNetworkParameter_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_SystemNetworkParameter_Read_req();
    void A02_A_SystemNetworkParameter_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
