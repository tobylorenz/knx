// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_Router_Scan.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_Router_Scan::NM_Router_Scan(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_Router_Scan::~NM_Router_Scan()
{
}

void NM_Router_Scan::req()
{
    if (state == State::S00) {
        state = State::S01;
        individual_addresses.clear();
        SNA_Current = 0;
        A01_A_Connect_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_Router_Scan::A01_A_Connect_req()
{
    assert(state == State::S01);

    Individual_Address individual_address;
    individual_address.set_subnetwork_address(SNA_Current);
    individual_address.set_device_address(0x00);
    const ASAP_Connected asap{individual_address, true};
    application_layer.A_Connect_req(asap, Priority::low, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_Disconnect_ind();
            restart_timer(std::chrono::milliseconds(100)); // spec: "delay for 0,1 s"
            break;
        case Status::not_ok:
            state = State::S00;
            con(individual_addresses, Status::not_ok);
            break;
        }
    });
}

void NM_Router_Scan::A02_A_Disconnect_ind()
{
    assert(state == State::S02);

    application_layer.A_Disconnect_ind([this](const ASAP_Connected asap) -> void {
        if ((asap.address.subnetwork_address() == SNA_Current) && (asap.address.device_address() == 0)) {
            individual_addresses.insert(asap.address);
        }
        if (state == State::S02) {
            A02_A_Disconnect_ind();
        }
    });
}

void NM_Router_Scan::A03_A_Disconnect_ind()
{
    assert(state == State::S03);

    application_layer.A_Disconnect_ind([this](const ASAP_Connected asap) -> void {
        if (asap.address.device_address() == 0) {
            individual_addresses.insert(asap.address);
        }
        if (state == State::S03) {
            A03_A_Disconnect_ind();
        }
    });
}

void NM_Router_Scan::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S02:
            if (SNA_Current == 255) {
                state = State::S03;
                A03_A_Disconnect_ind();
                restart_timer(std::chrono::milliseconds(6000)); // spec: "time-out: 6s after last send A_Connect"
            } else {
                state = State::S01;
                SNA_Current++;
                A01_A_Connect_req();
                restart_timer(Lcon_timeout);
            }
            break;
        case State::S03:
            state = State::S00;
            con(individual_addresses, Status::ok);
            break;
        default:
            state = State::S00;
            con(individual_addresses, Status::not_ok);
            break;
        }
    });
}

}
