// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {
/**
 * NM_Router_Scan
 *
 * @ingroup KNX_03_05_02_02_13
 */
class KNX_EXPORT NM_Router_Scan :
    public std::enable_shared_from_this<NM_Router_Scan>
{
public:
    explicit NM_Router_Scan(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_Router_Scan();

    void req();
    std::function<void(const std::set<Individual_Address> individual_addresses, const Status nm_status)> con;

    /* Parameter(s) */
    std::set<Individual_Address> individual_addresses{}; // out

    /* Variable(s) */
    Subnetwork_Address SNA_Current{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Connect.req
        S02, ///< waiting A_Disconnect.ind
        S03, ///< waiting A_Disconnect.ind
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Connect_req();
    void A02_A_Disconnect_ind();
    void A03_A_Disconnect_ind();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
