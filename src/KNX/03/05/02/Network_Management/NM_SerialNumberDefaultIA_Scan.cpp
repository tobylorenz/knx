// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_SerialNumberDefaultIA_Scan.h>

#include <KNX/03/05/01/Interface_Object.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_SerialNumberDefaultIA_Scan::NM_SerialNumberDefaultIA_Scan(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_SerialNumberDefaultIA_Scan::~NM_SerialNumberDefaultIA_Scan()
{
}

void NM_SerialNumberDefaultIA_Scan::req()
{
    if (state == State::S00) {
        state = State::S01;
        A01_A_PropertyValue_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_SerialNumberDefaultIA_Scan::A01_A_PropertyValue_Read_req()
{
    assert(state == State::S01);

    asap = {Individual_Address(0xff), false}; // @todo which SNA? Spec suggest to look into Device_Object::Subnetwork_Address_Property
    application_layer.A_PropertyValue_Read_req(Ack_Request::dont_care, Priority::low, Network_Layer_Parameter, asap, 0, Interface_Object::PID_SERIAL_NUMBER, 1, 1, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_PropertyValue_Read_Acon();
            restart_timer(std::chrono::milliseconds(7000)); // spec: "Time-out: 7 sec"
            break;
        case Status::not_ok:
            state = State::S00;
            con(serial_numbers, Status::not_ok);
            break;
        }
    });
}

void NM_SerialNumberDefaultIA_Scan::A02_A_PropertyValue_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_PropertyValue_Read_Acon([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void {
        if ((asap == this->asap) && (object_index == 0) && (property_id == Interface_Object::PID_SERIAL_NUMBER) && (nr_of_elem == 1) && (start_index == 1)) {
            Serial_Number serial_number;
            std::copy(std::cbegin(data), std::cbegin(data) + 6, std::end(serial_number.serial_number));
            serial_numbers.push_back(serial_number);
        }
        if (state == State::S02) {
            A02_A_PropertyValue_Read_Acon();
            // do not reset timer
        }
    });
}

void NM_SerialNumberDefaultIA_Scan::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S02:
            state = State::S00;
            con(serial_numbers, Status::ok);
            break;
        default:
            state = State::S00;
            con(serial_numbers, Status::not_ok);
            break;
        }
    });
}

}
