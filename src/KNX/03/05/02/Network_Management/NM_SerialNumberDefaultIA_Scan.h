// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_SerialNumberDefaultIA_Scan
 *
 * @ingroup KNX_03_05_02_02_21
 */
class KNX_EXPORT NM_SerialNumberDefaultIA_Scan :
    public std::enable_shared_from_this<NM_SerialNumberDefaultIA_Scan>
{
public:
    explicit NM_SerialNumberDefaultIA_Scan(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_SerialNumberDefaultIA_Scan();

    void req();
    std::function<void(std::vector<Serial_Number> serial_numbers, const Status nm_status)> con;

    /* Parameter(s) */
    std::vector<Serial_Number> serial_numbers{}; // out
    
    /* Variable(s) */
    ASAP_Individual asap{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_PropertyValue_Read.req
        S02, ///< waiting A_PropertyValue_Read.Acon
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_PropertyValue_Read_req();
    void A02_A_PropertyValue_Read_Acon();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
