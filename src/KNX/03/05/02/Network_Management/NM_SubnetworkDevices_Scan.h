// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_SubnetworkDevices_Scan
 *
 * @ingroup KNX_03_05_02_02_14
 */
class KNX_EXPORT NM_SubnetworkDevices_Scan :
    public std::enable_shared_from_this<NM_SubnetworkDevices_Scan>
{
public:
    explicit NM_SubnetworkDevices_Scan(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_SubnetworkDevices_Scan();

    void req(const Device_Address SNA);
    std::function<void(const std::set<Device_Address> DA, const Status nm_status)> con;

    /* Parameter(s) */
    Subnetwork_Address SNA{}; // in
    std::set<Device_Address> DA{}; // out

    /* Variable(s) */
    Device_Address DA_Current{};

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_Connect.req
        S02, ///< waiting A_Disconnect.ind
        S03, ///< waiting A_Disconnect.ind
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_Connect_req();
    void A02_A_Disconnect_ind();
    void A03_A_Disconnect_ind();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
