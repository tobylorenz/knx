// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/05/02/Network_Management/NM_SystemNetworkParameter_Read_R.h>

namespace KNX {

// times, unless spec has explicit numbers
static std::chrono::milliseconds Lcon_timeout{100}; // time to wait for Lcon, after req
static std::chrono::milliseconds ind_timeout{1000}; // time to wait for ind
static std::chrono::milliseconds Acon_timeout{1000}; // time to wait for Acon

NM_SystemNetworkParameter_Read_R::NM_SystemNetworkParameter_Read_R(asio::io_context & io_context, Application_Layer & application_layer) :
    io_context(io_context),
    application_layer(application_layer),
    timeout_timer(io_context)
{
}

NM_SystemNetworkParameter_Read_R::~NM_SystemNetworkParameter_Read_R()
{
}

void NM_SystemNetworkParameter_Read_R::req(const ASAP_Individual asap, const Hop_Count_Type hop_count_type_req, const Object_Type object_type, const Property_Id PID, const Parameter_Test_Info test_info, const Comm_Mode comm_mode_req, const Comm_Mode comm_mode_res, const Hop_Count_Type hop_count_type_res)
{
    if (state == State::S00) {
        state = State::S01;
        this->asap = asap;
        this->hop_count_type_req = hop_count_type_req;
        this->object_type = object_type;
        this->PID = PID;
        this->test_info = test_info;
        this->comm_mode_req = comm_mode_req;
        this->comm_mode_res = comm_mode_res;
        this->hop_count_type_res = hop_count_type_res;
        result_data.clear();
        A01_A_SystemNetworkParameter_Read_req();
        restart_timer(Lcon_timeout);
    }
}

void NM_SystemNetworkParameter_Read_R::A01_A_SystemNetworkParameter_Read_req()
{
    assert(state == State::S01);

    assert(asap.connected == false);
    switch(comm_mode_req) {
    case Comm_Mode::Broadcast:
        assert(asap.address == Individual_Address(0));
        break;
    case Comm_Mode::Individual:
        break;
    default:
        assert(false);
    }
    const System_Parameter_Type parameter_type{object_type, PID};
    application_layer.A_SystemNetworkParameter_Read_req(hop_count_type_req, parameter_type, Priority::low, test_info, [this](const Status a_status) -> void {
        switch(a_status) {
        case Status::ok:
            state = State::S02;
            A02_A_SystemNetworkParameter_Read_Acon();
            restart_timer(Acon_timeout);
            break;
        case Status::not_ok:
            state = State::S00;
            con(result_data, Status::not_ok);
            break;
        }
    });
    restart_timer(Lcon_timeout);
}

void NM_SystemNetworkParameter_Read_R::A02_A_SystemNetworkParameter_Read_Acon()
{
    assert(state == State::S02);

    application_layer.A_SystemNetworkParameter_Read_Acon([this](const ASAP_Individual asap, const Hop_Count_Type /*hop_count_type*/, const System_Parameter_Type parameter_type, const Priority /*priority*/, const Parameter_Test_Info_Result test_info_result) -> void {
        if ((asap == this->asap) && (parameter_type.object_type == this->object_type) && (parameter_type.pid == this->PID)) {
            const Parameter_Test_Info test_info(std::cbegin(test_info_result), std::cbegin(test_info_result) + this->test_info.size());
            if (test_info == this->test_info) {
                const Parameter_Test_Result test_result(std::cbegin(test_info_result) + this->test_info.size(), std::cend(test_info_result));
                result_data.push_back(test_result);
            }
        }
        if (state == State::S02) {
            A02_A_SystemNetworkParameter_Read_Acon();
            restart_timer(Acon_timeout);
        }
    });
}

void NM_SystemNetworkParameter_Read_R::restart_timer(const std::chrono::milliseconds duration)
{
    timeout_timer.expires_after(duration); // cancel pending timers
    timeout_timer.async_wait([this](const std::error_code & error) -> void {
        switch (error.value()) {
        case 0: // timer expired
            break;
        case asio::error::operation_aborted: // timer cancelled
        default:
            return;
        }

        switch(state) {
        case State::S00:
            break;
        case State::S02:
            state = State::S00;
            con(result_data, Status::ok);
            break;
        default:
            state = State::S00;
            con(result_data, Status::not_ok);
            break;
        }
    });
}

}
