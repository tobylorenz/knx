// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * NM_SystemNetworkParameter_Write_R
 *
 * @note This is not specified. The method signatures are assumptions.
 *
 * @ingroup KNX_03_05_02_02_18
 */
class KNX_EXPORT NM_SystemNetworkParameter_Write_R :
    public std::enable_shared_from_this<NM_SystemNetworkParameter_Write_R>
{
public:
    explicit NM_SystemNetworkParameter_Write_R(asio::io_context & io_context, Application_Layer & application_layer);
    virtual ~NM_SystemNetworkParameter_Write_R();

    void req(const ASAP_Individual asap, const Comm_Mode comm_mode, const Hop_Count_Type hop_count_type_req, const Object_Type object_type, const System_Property_Id PID, Priority priority, const Parameter_Value value);
    std::function<void(const Status nm_status)> con;

    /* Parameter(s) */
    ASAP_Individual asap{}; // in
    Comm_Mode comm_mode{}; // in
    Hop_Count_Type hop_count_type_req{}; // in
    Object_Type object_type{}; // in
    System_Property_Id PID{}; // in
    Priority priority{}; // in
    Parameter_Value value{}; // in

private:
    asio::io_context & io_context;
    Application_Layer & application_layer;

    /** state */
    enum class State {
        S00, ///< idle
        S01, ///< sending A_NetworkParameter_Write.req
    };
    State state { State::S00 };

    asio::steady_timer timeout_timer;

    /* actions */
    void A01_A_SystemNetworkParameter_Write_req();
    void restart_timer(const std::chrono::milliseconds duration);
};

}
