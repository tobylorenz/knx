// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <chrono>
#include <map>

#include <KNX/03/08/01/Medium_Code.h>

namespace KNX {

/**
 * medium dependent factor
 *
 * @ingroup KNX_03_05_02_01_03
 */
static const std::map<Medium_Code, std::chrono::milliseconds> T_media {
    {Medium_Code::TP1, std::chrono::milliseconds(20)},
    {Medium_Code::PL110, std::chrono::milliseconds(390)},
};

}
