// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 Introduction */

/* 1.3 Timing aspects of Management Procedures */
#include <KNX/03/05/02/Timing.h>

/* 2 Network Management Procedures */
#include <KNX/03/05/02/Network_Management.h>

/* 3 Device Management Procedures */
#include <KNX/03/05/02/Device_Management.h>

/* 4 KNXnet/IP Management Procedures */
#include <KNX/03/05/02/IP_Management.h>

/* 5 File Transfer Procedures */
#include <KNX/03/05/02/File_Transfer.h>
