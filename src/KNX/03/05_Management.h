// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 Resources */
#include <KNX/03/05/01_Resources.h>

/* 2 Management Procedures */
#include <KNX/03/05/02_Management_Procedures.h>

/* 3 Configuration Procedures */
#include <KNX/03/05/03_Configuration_Procedures.h>
