// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

namespace KNX {

/**
 * PEI_types
 * - 0: no adaptor
 * - 1: illegal adaptor
 * - 2: 4 inputs, 1 output (LED)
 * - 3: reserved
 * - 4: 2 inputs, 2 outputs, 1 output (LED)
 * - 5: reserved
 * - 6: 3 inputs, 1 output, 1 output (LED)
 * - 7: reserved
 * - 8: 5 inputs
 * - 9: reserved
 * - 10: default: FT1.2 protocol
 * - 10: loadable serial protocol
 * - 11: reserved
 * - 12: serial synchronous interface, message protocol
 * - 13: reserved
 * - 14: serial synchronous interface, data block protocol
 * - 15: reserved
 * - 16: serial asynchronous interface, message protocol
 * - 17: programmable I/O
 * - 18: reserved
 * - 19: 4 outputs, 1 output (LED)
 * - 20: download
 */

}
