// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 3 EMI1 and EMI2 */

/* 3.3 Messages at the KNX protocol layer EMI */

/* 3.3.3 Busmonitor EMI */
#include <KNX/03/06/03/EMI/Busmonitor/L_Busmon/EMI_L_Busmon_ind.h>
#include <KNX/03/06/03/EMI/Busmonitor/L_Plain_Data/EMI_L_Plain_Data_req.h>
#include <KNX/03/06/03/EMI/Busmonitor/LM_Reset/EMI_LM_Reset_ind.h>

/* 3.3.4 Data Link Layer EMI */
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_req.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_con.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_ind.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data_con.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast_req.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast_con.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast_ind.h>

/* 3.3.5 Network Layer EMI */
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_ind.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Group/EMI_N_Data_Group_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Group/EMI_N_Data_Group_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Group/EMI_N_Data_Group_ind.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_ind.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Poll_Data/EMI_N_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Poll_Data/EMI_N_Poll_Data_con.h>

/* 3.3.6 Transport Layer EMI */
#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_SystemBroadcast/EMI_T_Data_SystemBroadcast_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_SystemBroadcast/EMI_T_Data_SystemBroadcast_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_SystemBroadcast/EMI_T_Data_SystemBroadcast_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Poll_Data/EMI_T_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Poll_Data/EMI_T_Poll_Data_con.h>

/* 3.3.7 Application Layer EMI */
#include <KNX/03/06/03/EMI/Application_Layer/M_Connect/EMI_M_Connect_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_Disconnect/EMI_M_Disconnect_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_req.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_req.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual_req.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data_con.h>

/* 3.3.8 User Layer (i.e. Default) EMI */
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read_req.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read_con.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Flags_Read/EMI_U_Flags_Read_req.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Flags_Read/EMI_U_Flags_Read_con.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Event/EMI_U_Event_ind.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Write/EMI_U_Value_Write_req.h>
#include <KNX/03/06/03/EMI/User_Layer/U_User_Data/EMI_U_User_Data.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_req.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_con.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_ind.h>

/* 3.3.9 Other default EMI services */
#include <KNX/03/06/03/EMI/Other/PC_Set_Value/EMI_PC_Set_Value_req.h>
#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value_req.h>
#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value_con.h>
#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify_req.h>
#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify_con.h>
#include <KNX/03/06/03/EMI/Other/PEI_Switch/EMI_PEI_Switch_req.h>
#include <KNX/03/06/03/EMI/Other/TM_Timer/EMI_TM_Timer_ind.h>
