# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group.h
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group_con.h
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group_ind.h
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group_req.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group_con.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group_ind.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group_req.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group.h
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group_con.h
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group_ind.h
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_A_Data_Group_req.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/06/03/EMI/Application_Layer/A_Data_Group)
