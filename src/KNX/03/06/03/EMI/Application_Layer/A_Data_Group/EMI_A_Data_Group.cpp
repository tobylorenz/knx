// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group.h>

#include <cassert>

#include <KNX/03/03/07/APDU.h>

namespace KNX {

EMI_A_Data_Group::EMI_A_Data_Group(const EMI_Message_Code message_code) :
    EMI_APDU(message_code)
{
}

void EMI_A_Data_Group::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    /* octet 1..6 */
    EMI_APDU::fromData(first, first + 6);
    first += 6;

    /* octet 7 */
    const uint8_t octet_7 = *first++;
    hop_count_type = (octet_7 >> 4) & 0x07;
    length = octet_7 & 0x0f;

    /* octet 8..N */
    tsdu = make_APDU(first, last);
}

std::vector<uint8_t> EMI_A_Data_Group::toData() const
{
    std::vector<uint8_t> data = EMI_APDU::toData();

    /* octet 7 */
    data.push_back(
        (1 << 7) |
        (static_cast<uint8_t>(hop_count_type) << 4) |
        (length << 0));

    /* octet 8..N */
    const std::vector<uint8_t> tsdu_data = tsdu->toData();
    data.insert(std::cend(data), std::cbegin(tsdu_data), std::cend(tsdu_data));

    return data;
}

}
