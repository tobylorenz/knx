// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/03/03/Hop_Count_Type.h>
#include <KNX/03/03/04/TSDU.h>
#include <KNX/03/06/03/EMI/Application_Layer/EMI_APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Data_Group message
 */
class KNX_EXPORT EMI_A_Data_Group :
    public EMI_APDU
{
public:
    explicit EMI_A_Data_Group(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** hop count type */
    Hop_Count_Type hop_count_type{Network_Layer_Parameter};

    /** length */
    uint8_t length{};

    /** APDU */
    std::shared_ptr<TSDU> tsdu{};
};

}
