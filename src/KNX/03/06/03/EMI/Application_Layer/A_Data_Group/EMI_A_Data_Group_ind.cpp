// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_ind.h>

namespace KNX {

EMI_A_Data_Group_ind::EMI_A_Data_Group_ind() :
    EMI_A_Data_Group(EMI_Message_Code::A_Data_Group_ind)
{
}

}
