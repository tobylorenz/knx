// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Data_Group.ind message
 *
 * @ingroup KNX_03_06_03_03_03_07_09
 */
class KNX_EXPORT EMI_A_Data_Group_ind :
    public EMI_A_Data_Group
{
public:
    EMI_A_Data_Group_ind();
};

}
