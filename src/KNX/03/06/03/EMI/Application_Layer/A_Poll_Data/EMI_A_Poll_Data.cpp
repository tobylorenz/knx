// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data.h>

#include <cassert>

namespace KNX {

EMI_A_Poll_Data::EMI_A_Poll_Data(const EMI_Message_Code message_code) :
    EMI_APDU(message_code)
{
    control.poll = Poll::Poll_Data;
}

void EMI_A_Poll_Data::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 7);

    /* octet 1..6 */
    EMI_APDU::fromData(first, first + 6);
    first += 6;

    /* octet 7 */
    nr_of_slots = *first++ & 0x0f;

    assert(first == last);
}

std::vector<uint8_t> EMI_A_Poll_Data::toData() const
{
    std::vector<uint8_t> data = EMI_APDU::toData();

    /* octet 7 */
    data.push_back(nr_of_slots & 0x0f);

    return data;
}

}
