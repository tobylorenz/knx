// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Group_Address.h>
#include <KNX/03/06/03/EMI/Application_Layer/EMI_APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Poll_Data message
 */
class KNX_EXPORT EMI_A_Poll_Data :
    public EMI_APDU
{
public:
    explicit EMI_A_Poll_Data(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Nr of Slots */
    uint8_t nr_of_slots{};
};

}
