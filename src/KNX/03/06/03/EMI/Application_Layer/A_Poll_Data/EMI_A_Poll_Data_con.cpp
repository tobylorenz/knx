// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data_con.h>

#include <cassert>

namespace KNX {

EMI_A_Poll_Data_con::EMI_A_Poll_Data_con() :
    EMI_A_Poll_Data(EMI_Message_Code::A_Poll_Data_con)
{
}

void EMI_A_Poll_Data_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    /* octet 1..7 */
    EMI_A_Poll_Data::fromData(first, first + 7);
    first += 7;

    /* octet 8 */
    poll_data.assign(first, last);
}

std::vector<uint8_t> EMI_A_Poll_Data_con::toData() const
{
    std::vector<uint8_t> data = EMI_A_Poll_Data::toData();

    /* octet 8 */
    data.insert(std::cend(data), std::cbegin(poll_data), std::cend(poll_data));

    return data;
}

}
