// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data_req.h>

namespace KNX {

EMI_A_Poll_Data_req::EMI_A_Poll_Data_req() :
    EMI_A_Poll_Data(EMI_Message_Code::A_Poll_Data_req)
{
}

}
