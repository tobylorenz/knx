// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * A_Poll_Data.req message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_07_13
 */
class KNX_EXPORT EMI_A_Poll_Data_req :
    public EMI_A_Poll_Data
{
public:
    EMI_A_Poll_Data_req();
};

}
