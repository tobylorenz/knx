// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Control.h>
#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * EMI APDU
 *
 * @ingroup KNX_03_06_03_03_03_07
 */
class KNX_EXPORT EMI_APDU :
    public EMI_Message
{
public:
    EMI_APDU(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Control */
    Control control{};

    /** Source Address */
    Individual_Address source_address{};

    /** Destination Address */
    Address destination_address{};
};

}
