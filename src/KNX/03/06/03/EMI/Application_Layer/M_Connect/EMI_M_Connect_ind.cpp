// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Application_Layer/M_Connect/EMI_M_Connect_ind.h>

#include <cassert>

namespace KNX {

EMI_M_Connect_ind::EMI_M_Connect_ind() :
    EMI_APDU(EMI_Message_Code::M_Connect_ind)
{
}

}
