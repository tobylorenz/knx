// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Application_Layer/EMI_APDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_Disconnect.ind message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_07_03
 */
class KNX_EXPORT EMI_M_Disconnect_ind :
    public EMI_APDU
{
public:
    EMI_M_Disconnect_ind();
};

}
