// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_con.h>

namespace KNX {

EMI_M_User_Data_Connected_con::EMI_M_User_Data_Connected_con() :
    EMI_M_User_Data_Connected(EMI_Message_Code::M_User_Data_Connected_con)
{
}

}
