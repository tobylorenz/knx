// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual.h>

#include <cassert>

#include <KNX/03/03/07/APDU.h>

namespace KNX {

EMI_M_User_Data_Individual::EMI_M_User_Data_Individual(const EMI_Message_Code message_code) :
    EMI_APDU(message_code)
{
}

void EMI_M_User_Data_Individual::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    /* octet 1..6 */
    EMI_APDU::fromData(first, first + 6);
    first += 6;

    /* octet 7 */
    hop_count_type = (*first >> 4) & 0x07;
    length = *first & 0x0f;
    ++first;

    /* octet 8..N */
    tsdu = make_APDU(first, last);
}

std::vector<uint8_t> EMI_M_User_Data_Individual::toData() const
{
    assert(tsdu);

    /* octet 1..6 */
    std::vector<uint8_t> data = EMI_APDU::toData();

    /* octet 7 */
    data.push_back(
        (hop_count_type << 4) |
        (length << 0));

    /* octet 8..N */
    const std::vector<uint8_t> tsdu_data = tsdu->toData();
    data.insert(std::cend(data), std::cbegin(tsdu_data), std::cend(tsdu_data));

    return data;
}

}
