// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_User_Data_Individual.ind message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_07_10
 */
class KNX_EXPORT EMI_M_User_Data_Individual_req :
    public EMI_M_User_Data_Individual
{
public:
    EMI_M_User_Data_Individual_req();
};

}
