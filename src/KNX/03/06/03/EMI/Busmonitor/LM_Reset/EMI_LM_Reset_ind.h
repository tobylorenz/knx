// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * LM_Reset.ind message (EMI1/EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_03_04
 */
class KNX_EXPORT EMI_LM_Reset_ind :
    public EMI_Message
{
};

}
