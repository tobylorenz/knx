// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Busmonitor/L_Busmon/EMI_L_Busmon_ind.h>

#include <cassert>

namespace KNX {

EMI_L_Busmon_ind::EMI_L_Busmon_ind() :
    EMI_Message(EMI_Message_Code::L_Busmon_ind)
{
}

void EMI_L_Busmon_ind::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 5);
    assert(std::distance(first, last) <= 27);

    /* octet 1 */
    EMI_Message::fromData(first, first + 1);
    ++first;

    /* octet 2 */
    const uint8_t status = *first++;
    frame_error = (status >> 7) & 0x01;
    bit_error = (status >> 6) & 0x01;
    parity_error = (status >> 5) & 0x01;
    overflow = (status >> 4) & 0x01;
    lost = (status >> 3) & 0x01;
    sequence_number = status & 0x07;

    /* octet 3..4 */
    time_stamp = (*first++ << 8) | *first++;

    /* octet 5..N */
    lpdu.assign(first, last - 1);
}

std::vector<uint8_t> EMI_L_Busmon_ind::toData() const
{
    /* octet 1 */
    std::vector<uint8_t> data = EMI_Message::toData();

    /* octet 2 */
    data.push_back(
        (frame_error << 7) |
        (bit_error << 6) |
        (parity_error << 5) |
        (overflow << 4) |
        (lost << 3) |
        (sequence_number & 0x07));

    /* octet 3..4 */
    data.push_back(time_stamp >> 8);
    data.push_back(time_stamp & 0xff);

    /* octet 5..N */
    data.insert(std::cend(data), std::cbegin(lpdu), std::cend(lpdu));

    return data;
}

}
