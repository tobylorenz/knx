// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Busmon.ind message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_03_02
 */
class KNX_EXPORT EMI_L_Busmon_ind :
    public EMI_Message
{
public:
    EMI_L_Busmon_ind();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** frame error */
    bool frame_error{false};

    /** bit error */
    bool bit_error{false};

    /** parity error */
    bool parity_error{false};

    /** overflow */
    bool overflow{false};

    /** lost */
    bool lost{false};

    /** sequence number */
    uint8_t sequence_number{};

    /** Time Stamp */
    uint16_t time_stamp{};

    /** LPDU (control..checksum) */
    std::vector<uint8_t> lpdu{};
};

}
