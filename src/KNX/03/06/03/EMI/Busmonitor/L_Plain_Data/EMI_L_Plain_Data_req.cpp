// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Busmonitor/L_Plain_Data/EMI_L_Plain_Data_req.h>

#include <cassert>

namespace KNX {

EMI_L_Plain_Data_req::EMI_L_Plain_Data_req() :
    EMI_Message(EMI_Message_Code::L_Plain_Data_req)
{
}

void EMI_L_Plain_Data_req::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 6);
    assert(std::distance(first, last) <= 34);

    EMI_Message::fromData(first, first + 1);
    ++first;

    ++first; // unused

    time =
        (*first++ << 24) |
        (*first++ << 16) |
        (*first++ << 8) |
        (*first++);

    data.assign(first, last);
}

std::vector<uint8_t> EMI_L_Plain_Data_req::toData() const
{
    std::vector<uint8_t> data = EMI_Message::toData();

    data.push_back(0x00); // unused

    data.push_back((time >> 24) & 0xff);
    data.push_back((time >> 16) & 0xff);
    data.push_back((time >> 8) & 0xff);
    data.push_back(time & 0xff);

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

}
