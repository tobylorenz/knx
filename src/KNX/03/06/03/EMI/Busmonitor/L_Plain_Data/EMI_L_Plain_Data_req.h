// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Plain_Data.req (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_03_03
 */
class KNX_EXPORT EMI_L_Plain_Data_req :
    public EMI_Message
{
public:
    EMI_L_Plain_Data_req();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Time */
    uint32_t time{};

    /** Data */
    std::vector<uint8_t> data{};
};

}
