// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/03/02/Address_Type.h>
#include <KNX/03/03/02/LSDU.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/EMI_LPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Data message
 */
class KNX_EXPORT EMI_L_Data :
    public EMI_LPDU
{
public:
    explicit EMI_L_Data(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Address Type (AT) */
    Address_Type address_type{};

    /** octet count (LG) */
    uint8_t length{};

    virtual uint8_t length_calculated() const;

    /** NPDU */
    std::shared_ptr<LSDU> lsdu{};
};

}
