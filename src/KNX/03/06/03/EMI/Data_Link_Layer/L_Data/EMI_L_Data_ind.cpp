// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_ind.h>

namespace KNX {

EMI_L_Data_ind::EMI_L_Data_ind() :
    EMI_L_Data(EMI_Message_Code::L_Data_ind)
{
}

}
