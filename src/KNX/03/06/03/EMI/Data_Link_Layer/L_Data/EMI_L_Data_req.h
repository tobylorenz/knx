// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Data.req message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_04_02
 */
class KNX_EXPORT EMI_L_Data_req :
    public EMI_L_Data
{
public:
    EMI_L_Data_req();
};

}
