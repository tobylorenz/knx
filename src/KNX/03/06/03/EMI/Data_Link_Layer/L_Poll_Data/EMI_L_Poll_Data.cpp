// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data.h>

#include <cassert>

namespace KNX {

EMI_L_Poll_Data::EMI_L_Poll_Data(const EMI_Message_Code message_code) :
    EMI_LPDU(message_code)
{
    control.poll = Poll::Poll_Data;
}

}
