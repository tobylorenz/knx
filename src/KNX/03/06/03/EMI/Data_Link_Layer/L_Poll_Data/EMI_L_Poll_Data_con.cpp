// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data_con.h>

#include <cassert>

namespace KNX {

EMI_L_Poll_Data_con::EMI_L_Poll_Data_con() :
    EMI_L_Poll_Data(EMI_Message_Code::L_Poll_Data_con)
{
}

void EMI_L_Poll_Data_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    EMI_L_Poll_Data::fromData(first, first + 6);
    first += 6;

    nr_of_slots = (*first++) & 0x0f;

    poll_data.assign(first, last);
}

std::vector<uint8_t> EMI_L_Poll_Data_con::toData() const
{
    std::vector<uint8_t> data = EMI_L_Poll_Data::toData();

    data.push_back(nr_of_slots_calculated());

    data.insert(std::cend(data), std::cbegin(poll_data), std::cend(poll_data));

    return data;
}

uint4_t EMI_L_Poll_Data_con::nr_of_slots_calculated() const
{
    return poll_data.size() & 0x0f;
}

}
