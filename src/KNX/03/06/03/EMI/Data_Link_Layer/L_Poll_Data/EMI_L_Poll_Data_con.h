// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Poll_Data.con message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_04_06
 */
class KNX_EXPORT EMI_L_Poll_Data_con :
    public EMI_L_Poll_Data
{
public:
    EMI_L_Poll_Data_con();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** nr of slots */
    uint4_t nr_of_slots{};

    /** calculate nr of slots */
    uint4_t nr_of_slots_calculated() const;

    /** poll data */
    std::vector<uint8_t> poll_data{};
};

}
