// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data_req.h>

namespace KNX {

EMI_L_Poll_Data_req::EMI_L_Poll_Data_req() :
    EMI_L_Poll_Data(EMI_Message_Code::L_Poll_Data_req)
{
}

void EMI_L_Poll_Data_req::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 7);

    EMI_L_Poll_Data::fromData(first, first + 6);
    first += 6;

    nr_of_slots = (*first++) & 0x0f;

    assert(first == last);
}

std::vector<uint8_t> EMI_L_Poll_Data_req::toData() const
{
    std::vector<uint8_t> data = EMI_L_Poll_Data::toData();

    data.push_back(nr_of_slots);

    return data;
}

}
