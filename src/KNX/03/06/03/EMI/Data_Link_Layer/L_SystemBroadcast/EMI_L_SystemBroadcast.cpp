// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast.h>

#include <cassert>

#include <KNX/03/03/03/N_Data_Individual/N_Data_Individual_PDU.h>
#include <KNX/03/03/03/N_Data_Group/N_Data_Group_PDU.h>
#include <KNX/03/03/03/N_Data_Broadcast/N_Data_Broadcast_PDU.h>

namespace KNX {

EMI_L_SystemBroadcast::EMI_L_SystemBroadcast(const EMI_Message_Code message_code) :
    EMI_LPDU(message_code)
{
    control.system_broadcast = System_Broadcast::System_Broadcast;
    address_type = Address_Type::Group;
}

void EMI_L_SystemBroadcast::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    EMI_LPDU::fromData(first, first + 6);
    first += 6;

    const uint8_t at_npci_lg = *first++;
    address_type = static_cast<Address_Type>(at_npci_lg >> 7);
    const Hop_Count hop_count = (at_npci_lg >> 4) & 0x07;
    length = at_npci_lg & 0x0f;

    switch (address_type) {
    case Address_Type::Individual:
        lsdu = std::make_shared<N_Data_Individual_PDU>();
        break;
    case Address_Type::Group:
        if (destination_address == 0) {
            lsdu = std::make_shared<N_Data_Broadcast_PDU>();
        } else {
            lsdu = std::make_shared<N_Data_Group_PDU>();
        }
        break;
    }
    lsdu->fromData(hop_count, first, last);
}


std::vector<uint8_t> EMI_L_SystemBroadcast::toData() const
{
    assert(lsdu);

    /* octet 1..6 */
    std::vector<uint8_t> data = EMI_LPDU::toData();

    /* octet 7 */
    data.push_back(
        (static_cast<uint8_t>(address_type) << 7) |
        (lsdu->hop_count << 4) |
        length_calculated());

    /* octet 8..N */
    const std::vector<uint8_t> lsdu_data = lsdu->toData();
    data.insert(std::cend(data), std::cbegin(lsdu_data), std::cend(lsdu_data));

    return data;
}

uint8_t EMI_L_SystemBroadcast::length_calculated() const
{
    return lsdu ? lsdu->length_calculated() : 0;
}

}
