// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_SystemBroadcast.ind message (EMI/IMI 1 / EMI/IMI 2)
 *
 * @ingroup KNX_03_06_03_03_03_04_09
 */
class KNX_EXPORT EMI_L_SystemBroadcast_ind :
    public EMI_L_SystemBroadcast
{
public:
    EMI_L_SystemBroadcast_ind();
};

}
