// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/** Destination Layer */
enum Destination_Layer : uint8_t {
    NONE = 0,
    LL = 1,
    NL = 2,
    TLG = 3,
    TLC = 4,
    TLL = 5,
    AL = 6,
    MAN = 7,
    PEI = 8,
    USR = 9,
    RES = 10
};

}
