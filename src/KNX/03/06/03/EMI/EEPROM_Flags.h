// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table.h>

namespace KNX {

/** EEPROM flags */
using EEPROM_Flags = Group_Object_Config;

}
