// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/EMI1_Message_Code.h>

namespace KNX {

EMI1_Message_Code to_EMI1_Message_Code(const EMI_Message_Code emi)
{
    switch (emi) {
    case EMI_Message_Code::UNDEFINED:
        return EMI1_Message_Code::UNDEFINED;

    //    case EMI_Message_Code::Ph_Data_req:
    //        return EMI1_Message_Code::Ph_Data_req;
    //    case EMI_Message_Code::Ph_Data_con:
    //        return EMI1_Message_Code::Ph_Data_con;
    //    case EMI_Message_Code::Ph_Data_ind:
    //        return EMI1_Message_Code::Ph_Data_ind;

    case EMI_Message_Code::L_Busmon_ind:
        return EMI1_Message_Code::L_Busmon_ind;

    case EMI_Message_Code::L_Data_req:
        return EMI1_Message_Code::L_Data_req;
    case EMI_Message_Code::L_Data_con:
        return EMI1_Message_Code::L_Data_con;
    case EMI_Message_Code::L_Data_ind:
        return EMI1_Message_Code::L_Data_ind;

    case EMI_Message_Code::L_SystemBroadcast_req:
        return EMI1_Message_Code::L_SystemBroadcast_req;
    case EMI_Message_Code::L_SystemBroadcast_con:
        return EMI1_Message_Code::L_SystemBroadcast_con;
    case EMI_Message_Code::L_SystemBroadcast_ind:
        return EMI1_Message_Code::L_SystemBroadcast_ind;

    //    case EMI_Message_Code::L_Plain_Data_req:
    //        return EMI1_Message_Code::L_Plain_Data_req;

    //    case EMI_Message_Code::L_Poll_Data_req:
    //        return EMI1_Message_Code::L_Poll_Data_req;
    //    case EMI_Message_Code::L_Poll_Data_con:
    //        return EMI1_Message_Code::L_Poll_Data_con;

    //    case EMI_Message_Code::L_Meter_ind:
    //        return EMI1_Message_Code::L_Meter_ind;

    //    case EMI_Message_Code::N_Data_Individual_req:
    //        return EMI1_Message_Code::N_Data_Individual_req;
    //    case EMI_Message_Code::N_Data_Individual_con:
    //        return EMI1_Message_Code::N_Data_Individual_con;
    //    case EMI_Message_Code::N_Data_Individual_ind:
    //        return EMI1_Message_Code::N_Data_Individual_ind;

    //    case EMI_Message_Code::N_Data_Group_req:
    //        return EMI1_Message_Code::N_Data_Group_req;
    //    case EMI_Message_Code::N_Data_Group_con:
    //        return EMI1_Message_Code::N_Data_Group_con;
    //    case EMI_Message_Code::N_Data_Group_ind:
    //        return EMI1_Message_Code::N_Data_Group_ind;

    //    case EMI_Message_Code::N_Data_Broadcast_req:
    //        return EMI1_Message_Code::N_Data_Broadcast_req;
    //    case EMI_Message_Code::N_Data_Broadcast_con:
    //        return EMI1_Message_Code::N_Data_Broadcast_con;
    //    case EMI_Message_Code::N_Data_Broadcast_ind:
    //        return EMI1_Message_Code::N_Data_Broadcast_ind;

    //    case EMI_Message_Code::N_Poll_Data_req:
    //        return EMI1_Message_Code::N_Poll_Data_req;
    //    case EMI_Message_Code::N_Poll_Data_con:
    //        return EMI1_Message_Code::N_Poll_Data_con;

    case EMI_Message_Code::T_Connect_req:
        return EMI1_Message_Code::T_Connect_req;
    //    case EMI_Message_Code::T_Connect_con:
    //        return EMI1_Message_Code::T_Connect_con;
    case EMI_Message_Code::T_Connect_ind:
        return EMI1_Message_Code::T_Connect_ind;

    case EMI_Message_Code::T_Disconnect_req:
        return EMI1_Message_Code::T_Disconnect_req;
    //    case EMI_Message_Code::T_Disconnect_con:
    //        return EMI1_Message_Code::T_Disconnect_con;
    case EMI_Message_Code::T_Disconnect_ind:
        return EMI1_Message_Code::T_Disconnect_ind;

    case EMI_Message_Code::T_Data_Connected_req:
        return EMI1_Message_Code::T_Data_Connected_req;
    //    case EMI_Message_Code::T_Data_Connected_con:
    //        return EMI1_Message_Code::T_Data_Connected_con;
    case EMI_Message_Code::T_Data_Connected_ind:
        return EMI1_Message_Code::T_Data_Connected_ind;

    case EMI_Message_Code::T_Data_Group_req:
        return EMI1_Message_Code::T_Data_Group_req;
    case EMI_Message_Code::T_Data_Group_con:
        return EMI1_Message_Code::T_Data_Group_con;
    case EMI_Message_Code::T_Data_Group_ind:
        return EMI1_Message_Code::T_Data_Group_ind;

    case EMI_Message_Code::T_Data_Broadcast_req:
        return EMI1_Message_Code::T_Data_Broadcast_req;
    //    case EMI_Message_Code::T_Data_Broadcast_con:
    //        return EMI1_Message_Code::T_Data_Broadcast_con;
    case EMI_Message_Code::T_Data_Broadcast_ind:
        return EMI1_Message_Code::T_Data_Broadcast_ind;

    case EMI_Message_Code::T_Data_SystemBroadcast_req:
        return EMI1_Message_Code::T_Data_SystemBroadcast_req;
    case EMI_Message_Code::T_Data_SystemBroadcast_con:
        return EMI1_Message_Code::T_Data_SystemBroadcast_con;
    case EMI_Message_Code::T_Data_SystemBroadcast_ind:
        return EMI1_Message_Code::T_Data_SystemBroadcast_ind;

    case EMI_Message_Code::T_Data_Individual_req:
        return EMI1_Message_Code::T_Data_Individual_req;
    case EMI_Message_Code::T_Data_Individual_con:
        return EMI1_Message_Code::T_Data_Individual_con;
    case EMI_Message_Code::T_Data_Individual_ind:
        return EMI1_Message_Code::T_Data_Individual_ind;

    //    case EMI_Message_Code::T_Poll_Data_req:
    //        return EMI1_Message_Code::T_Poll_Data_req;
    //    case EMI_Message_Code::T_Poll_Data_con:
    //        return EMI1_Message_Code::T_Poll_Data_con;

    //    case EMI_Message_Code::M_Connect_ind:
    //        return EMI1_Message_Code::M_Connect_ind;
    //    case EMI_Message_Code::M_Disconnect_ind:
    //        return EMI1_Message_Code::M_Disconnect_ind;

    case EMI_Message_Code::M_User_Data_Connected_req:
        return EMI1_Message_Code::M_User_Data_Connected_req;
    //    case EMI_Message_Code::M_User_Data_Connected_con:
    //        return EMI1_Message_Code::M_User_Data_Connected_con;
    case EMI_Message_Code::M_User_Data_Connected_ind:
        return EMI1_Message_Code::M_User_Data_Connected_ind;

    //    case EMI_Message_Code::A_Data_Group_req:
    //        return EMI1_Message_Code::A_Data_Group_req;
    //    case EMI_Message_Code::A_Data_Group_con:
    //        return EMI1_Message_Code::A_Data_Group_con;
    //    case EMI_Message_Code::A_Data_Group_ind:
    //        return EMI1_Message_Code::A_Data_Group_ind;

    //    case EMI_Message_Code::M_User_Data_Individual_req:
    //        return EMI1_Message_Code::M_User_Data_Individual_req;
    //    case EMI_Message_Code::M_User_Data_Individual_con:
    //        return EMI1_Message_Code::M_User_Data_Individual_con;
    //    case EMI_Message_Code::M_User_Data_Individual_ind:
    //        return EMI1_Message_Code::M_User_Data_Individual_ind;

    //    case EMI_Message_Code::A_Poll_Data_req:
    //        return EMI1_Message_Code::A_Poll_Data_req;
    //    case EMI_Message_Code::A_Poll_Data_con:
    //        return EMI1_Message_Code::A_Poll_Data_con;

    //    case EMI_Message_Code::M_InterfaceObj_Data_req:
    //        return EMI1_Message_Code::M_InterfaceObj_Data_req;
    //    case EMI_Message_Code::M_InterfaceObj_Data_con:
    //        return EMI1_Message_Code::M_InterfaceObj_Data_con;
    //    case EMI_Message_Code::M_InterfaceObj_Data_ind:
    //        return EMI1_Message_Code::M_InterfaceObj_Data_ind;

    case EMI_Message_Code::U_Value_Read_req:
        return EMI1_Message_Code::U_Value_Read_req;
    case EMI_Message_Code::U_Value_Read_con:
        return EMI1_Message_Code::U_Value_Read_con;

    case EMI_Message_Code::U_Flags_Read_req:
        return EMI1_Message_Code::U_Flags_Read_req;
    case EMI_Message_Code::U_Flags_Read_con:
        return EMI1_Message_Code::U_Flags_Read_con;

    case EMI_Message_Code::U_Event_ind:
        return EMI1_Message_Code::U_Event_ind;

    case EMI_Message_Code::U_Value_Write_req:
        return EMI1_Message_Code::U_Value_Write_req;

    //    case EMI_Message_Code::U_User_Data:
    //        return EMI1_Message_Code::U_User_Data;

    case EMI_Message_Code::PC_Set_Value_req:
        return EMI1_Message_Code::PC_Set_Value_req;

    case EMI_Message_Code::PC_Get_Value_req:
        return EMI1_Message_Code::PC_Get_Value_req;
    case EMI_Message_Code::PC_Get_Value_con:
        return EMI1_Message_Code::PC_Get_Value_con;

    //    case EMI_Message_Code::PEI_Identify_req:
    //        return EMI1_Message_Code::PEI_Identify_req;
    //    case EMI_Message_Code::PEI_Identify_con:
    //        return EMI1_Message_Code::PEI_Identify_con;

    //    case EMI_Message_Code::PEI_Switch_req:
    //        return EMI1_Message_Code::PEI_Switch_req;

    //    case EMI_Message_Code::TM_Timer_ind:
    //        return EMI1_Message_Code::TM_Timer_ind;

    default:
        assert(false);
    }

    return EMI1_Message_Code::UNDEFINED;
}

EMI_Message_Code from_EMI1_Message_Code(const EMI1_Message_Code emi1)
{
    switch (emi1) {
    case EMI1_Message_Code::UNDEFINED:
        return EMI_Message_Code::UNDEFINED;

    //    case EMI1_Message_Code::Ph_Data_req:
    //        return EMI_Message_Code::Ph_Data_req;
    //    case EMI1_Message_Code::Ph_Data_con:
    //        return EMI_Message_Code::Ph_Data_con;
    //    case EMI1_Message_Code::Ph_Data_ind:
    //        return EMI_Message_Code::Ph_Data_ind;

    case EMI1_Message_Code::L_Busmon_ind:
        return EMI_Message_Code::L_Busmon_ind;

    case EMI1_Message_Code::L_Data_req:
        return EMI_Message_Code::L_Data_req;
    case EMI1_Message_Code::L_Data_con:
        return EMI_Message_Code::L_Data_con;
    //    case EMI1_Message_Code::L_Data_ind:
    //        return EMI_Message_Code::L_Data_ind;

    case EMI1_Message_Code::L_SystemBroadcast_req:
        return EMI_Message_Code::L_SystemBroadcast_req;
    case EMI1_Message_Code::L_SystemBroadcast_con:
        return EMI_Message_Code::L_SystemBroadcast_con;
    case EMI1_Message_Code::L_SystemBroadcast_ind:
        return EMI_Message_Code::L_SystemBroadcast_ind;

    //    case EMI1_Message_Code::L_Plain_Data_req:
    //        return EMI_Message_Code::L_Plain_Data_req;

    //    case EMI1_Message_Code::L_Poll_Data_req:
    //        return EMI_Message_Code::L_Poll_Data_req;
    //    case EMI1_Message_Code::L_Poll_Data_con:
    //        return EMI_Message_Code::L_Poll_Data_con;

    //    case EMI1_Message_Code::L_Meter_ind:
    //        return EMI_Message_Code::L_Meter_ind;

    //    case EMI1_Message_Code::N_Data_Individual_req:
    //        return EMI_Message_Code::N_Data_Individual_req;
    //    case EMI1_Message_Code::N_Data_Individual_con:
    //        return EMI_Message_Code::N_Data_Individual_con;
    //    case EMI1_Message_Code::N_Data_Individual_ind:
    //        return EMI_Message_Code::N_Data_Individual_ind;

    //    case EMI1_Message_Code::N_Data_Group_req:
    //        return EMI_Message_Code::N_Data_Group_req;
    //    case EMI1_Message_Code::N_Data_Group_con:
    //        return EMI_Message_Code::N_Data_Group_con;
    //    case EMI1_Message_Code::N_Data_Group_ind:
    //        return EMI_Message_Code::N_Data_Group_ind;

    //    case EMI1_Message_Code::N_Data_Broadcast_req:
    //        return EMI_Message_Code::N_Data_Broadcast_req;
    //    case EMI1_Message_Code::N_Data_Broadcast_con:
    //        return EMI_Message_Code::N_Data_Broadcast_con;
    //    case EMI1_Message_Code::N_Data_Broadcast_ind:
    //        return EMI_Message_Code::N_Data_Broadcast_ind;

    //    case EMI1_Message_Code::N_Poll_Data_req:
    //        return EMI_Message_Code::N_Poll_Data_req;
    //    case EMI1_Message_Code::N_Poll_Data_con:
    //        return EMI_Message_Code::N_Poll_Data_con;

    case EMI1_Message_Code::T_Connect_req:
        return EMI_Message_Code::T_Connect_req;
    //    case EMI1_Message_Code::T_Connect_con:
    //        return EMI_Message_Code::T_Connect_con;
    case EMI1_Message_Code::T_Connect_ind:
        return EMI_Message_Code::T_Connect_ind;

    case EMI1_Message_Code::T_Disconnect_req:
        return EMI_Message_Code::T_Disconnect_req;
    //    case EMI1_Message_Code::T_Disconnect_con:
    //        return EMI_Message_Code::T_Disconnect_con;
    case EMI1_Message_Code::T_Disconnect_ind:
        return EMI_Message_Code::T_Disconnect_ind;

    case EMI1_Message_Code::T_Data_Connected_req:
        return EMI_Message_Code::T_Data_Connected_req;
    //    case EMI1_Message_Code::T_Data_Connected_con:
    //        return EMI_Message_Code::T_Data_Connected_con;
    //    case EMI1_Message_Code::T_Data_Connected_ind:
    //        return EMI_Message_Code::T_Data_Connected_ind;

    case EMI1_Message_Code::T_Data_Group_req:
        return EMI_Message_Code::T_Data_Group_req;
    //    case EMI1_Message_Code::T_Data_Group_con:
    //        return EMI_Message_Code::T_Data_Group_con;
    case EMI1_Message_Code::T_Data_Group_ind:
        return EMI_Message_Code::T_Data_Group_ind;

    case EMI1_Message_Code::T_Data_Broadcast_req:
        return EMI_Message_Code::T_Data_Broadcast_req;
    //    case EMI1_Message_Code::T_Data_Broadcast_con:
    //        return EMI_Message_Code::T_Data_Broadcast_con;
    case EMI1_Message_Code::T_Data_Broadcast_ind:
        return EMI_Message_Code::T_Data_Broadcast_ind;

    case EMI1_Message_Code::T_Data_SystemBroadcast_req:
        return EMI_Message_Code::T_Data_SystemBroadcast_req;
    //    case EMI1_Message_Code::T_Data_SystemBroadcast_con:
    //        return EMI_Message_Code::T_Data_SystemBroadcast_con;
    //    case EMI1_Message_Code::T_Data_SystemBroadcast_ind:
    //        return EMI_Message_Code::T_Data_SystemBroadcast_ind;

    case EMI1_Message_Code::T_Data_Individual_req:
        return EMI_Message_Code::T_Data_Individual_req;
    case EMI1_Message_Code::T_Data_Individual_con:
        return EMI_Message_Code::T_Data_Individual_con;
    case EMI1_Message_Code::T_Data_Individual_ind:
        return EMI_Message_Code::T_Data_Individual_ind;

    //    case EMI1_Message_Code::T_Poll_Data_req:
    //        return EMI_Message_Code::T_Poll_Data_req;
    //    case EMI1_Message_Code::T_Poll_Data_con:
    //        return EMI_Message_Code::T_Poll_Data_con;

    //    case EMI1_Message_Code::M_Connect_ind:
    //        return EMI_Message_Code::M_Connect_ind;
    //    case EMI1_Message_Code::M_Disconnect_ind:
    //        return EMI_Message_Code::M_Disconnect_ind;

    case EMI1_Message_Code::M_User_Data_Connected_req:
        return EMI_Message_Code::M_User_Data_Connected_req;
    //    case EMI1_Message_Code::M_User_Data_Connected_con:
    //        return EMI_Message_Code::M_User_Data_Connected_con;
    //    case EMI1_Message_Code::M_User_Data_Connected_ind:
    //        return EMI_Message_Code::M_User_Data_Connected_ind;

    //    case EMI1_Message_Code::A_Data_Group_req:
    //        return EMI_Message_Code::A_Data_Group_req;
    //    case EMI1_Message_Code::A_Data_Group_con:
    //        return EMI_Message_Code::A_Data_Group_con;
    //    case EMI1_Message_Code::A_Data_Group_ind:
    //        return EMI_Message_Code::A_Data_Group_ind;

    //    case EMI1_Message_Code::M_User_Data_Individual_req:
    //        return EMI_Message_Code::M_User_Data_Individual_req;
    //    case EMI1_Message_Code::M_User_Data_Individual_con:
    //        return EMI_Message_Code::M_User_Data_Individual_con;
    //    case EMI1_Message_Code::M_User_Data_Individual_ind:
    //        return EMI_Message_Code::M_User_Data_Individual_ind;

    //    case EMI1_Message_Code::A_Poll_Data_req:
    //        return EMI_Message_Code::A_Poll_Data_req;
    //    case EMI1_Message_Code::A_Poll_Data_con:
    //        return EMI_Message_Code::A_Poll_Data_con;

    //    case EMI1_Message_Code::M_InterfaceObj_Data_req:
    //        return EMI_Message_Code::M_InterfaceObj_Data_req;
    //    case EMI1_Message_Code::M_InterfaceObj_Data_con:
    //        return EMI_Message_Code::M_InterfaceObj_Data_con;
    //    case EMI1_Message_Code::M_InterfaceObj_Data_ind:
    //        return EMI_Message_Code::M_InterfaceObj_Data_ind;

    case EMI1_Message_Code::U_Value_Read_req:
        return EMI_Message_Code::U_Value_Read_req;
    case EMI1_Message_Code::U_Value_Read_con:
        return EMI_Message_Code::U_Value_Read_con;

    case EMI1_Message_Code::U_Flags_Read_req:
        return EMI_Message_Code::U_Flags_Read_req;
    case EMI1_Message_Code::U_Flags_Read_con:
        return EMI_Message_Code::U_Flags_Read_con;

    //    case EMI1_Message_Code::U_Event_ind:
    //        return EMI_Message_Code::U_Event_ind;

    case EMI1_Message_Code::U_Value_Write_req:
        return EMI_Message_Code::U_Value_Write_req;

    //    case EMI1_Message_Code::U_User_Data:
    //        return EMI_Message_Code::U_User_Data;

    case EMI1_Message_Code::PC_Set_Value_req:
        return EMI_Message_Code::PC_Set_Value_req;

    //    case EMI1_Message_Code::PC_Get_Value_req:
    //        return EMI_Message_Code::PC_Get_Value_req;
    case EMI1_Message_Code::PC_Get_Value_con:
        return EMI_Message_Code::PC_Get_Value_con;

        //    case EMI1_Message_Code::PEI_Identify_req:
        //        return EMI_Message_Code::PEI_Identify_req;
        //    case EMI1_Message_Code::PEI_Identify_con:
        //        return EMI_Message_Code::PEI_Identify_con;

        //    case EMI1_Message_Code::PEI_Switch_req:
        //        return EMI_Message_Code::PEI_Switch_req;

        //    case EMI1_Message_Code::TM_Timer_ind:
        //        return EMI_Message_Code::TM_Timer_ind;
    }

    return EMI_Message_Code::UNDEFINED;
}

}
