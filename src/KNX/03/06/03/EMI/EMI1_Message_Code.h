// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

#include <KNX/03/06/03/EMI/EMI_Message_Code.h>

namespace KNX {

/**
 * EMI1 Message Codes
 *
 * @ingroup KNX_03_06_03_02
 */
enum class EMI1_Message_Code : uint8_t {
    UNDEFINED = 0x00,

    L_Busmon_ind = 0x49, // L_Busmon.ind

    L_Data_req = 0x11, // L_Data.req
    L_Data_con = 0x4E, // L_Data.con
    L_Data_ind = 0x49, // L_Data.ind

    L_SystemBroadcast_req = 0x15, // L_SystemBroadcast.req
    L_SystemBroadcast_con = 0x4C, // L_SystemBroadcast.con
    L_SystemBroadcast_ind = 0x4D, // L_SystemBroadcast.ind

    T_Connect_req = 0x23, // T_Connect.req
    T_Connect_ind = 0x43, // T_Connect.ind

    T_Disconnect_req = 0x24, // T_Disconnect.req
    T_Disconnect_ind = 0x44, // T_Disconnect.ind

    T_Data_Connected_req = 0x21, // T_Data_Connected.req
    T_Data_Connected_ind = 0x49, // T_Data_Connected.ind

    T_Data_Group_req = 0x22, // T_Data_Group.req
    T_Data_Group_con = 0x4E, // T_Data_Group.con
    T_Data_Group_ind = 0x4A, // T_Data_Group.ind

    T_Data_Broadcast_req = 0x2B, // T_Data_Broadcast.req
    T_Data_Broadcast_ind = 0x48, // T_Data_Broadcast.ind

    T_Data_SystemBroadcast_req = 0x25, // T_Data_SystemBroadcast.req
    T_Data_SystemBroadcast_con = 0x4C, // T_Data_SystemBroadcast.con
    T_Data_SystemBroadcast_ind = 0x4D, // T_Data_SystemBroadcast.ind

    T_Data_Individual_req = 0x2A, // T_Data_Individual.req
    T_Data_Individual_con = 0x4F, // T_Data_Individual.con
    T_Data_Individual_ind = 0x42, // T_Data_Individual.ind

    M_User_Data_Connected_req = 0x31, // M_User_Data_Connected.req
    M_User_Data_Connected_ind = 0x49, // M_User_Data_Connected.ind

    U_Value_Read_req = 0x35, // U_Value_Read.req
    U_Value_Read_con = 0x45, // U_Value_Read.con

    U_Flags_Read_req = 0x37, // U_Flags_Read.req
    U_Flags_Read_con = 0x47, // U_Flags_Read.con

    U_Event_ind = 0x4D, // U_Event.ind

    U_Value_Write_req = 0x36, // U_Value_Write.req

    PC_Set_Value_req = 0x46, // PC_Set_Value.req

    PC_Get_Value_req = 0x4C, // PC_Get_Value.req
    PC_Get_Value_con = 0x4B, // PC_Get_Value.con
};

EMI1_Message_Code to_EMI1_Message_Code(const EMI_Message_Code emi);
EMI_Message_Code from_EMI1_Message_Code(const EMI1_Message_Code emi1);

}
