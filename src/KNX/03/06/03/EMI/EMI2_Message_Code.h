// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

#include <KNX/03/06/03/EMI/EMI_Message_Code.h>

namespace KNX {

/**
 * EMI2 Message Codes
 *
 * @ingroup KNX_03_06_03_02
 */
enum class EMI2_Message_Code : uint8_t {
    UNDEFINED = 0x00,

    Ph_Data_req = 0x01, // Ph_Data.req
    Ph_Data_con = 0x1E, // Ph_Data.con
    Ph_Data_ind = 0x19, // Ph_Data.ind

    L_Busmon_ind = 0x2B, // L_Busmon.ind

    L_Data_req = 0x11, // L_Data.req
    L_Data_con = 0x2E, // L_Data.con
    L_Data_ind = 0x29, // L_Data.ind

    L_SystemBroadcast_req = 0x17, // L_SystemBroadcast.req
    L_SystemBroadcast_con = 0x26, // L_SystemBroadcast.con
    L_SystemBroadcast_ind = 0x28, // L_SystemBroadcast.ind

    L_Plain_Data_req = 0x10, // L_Plain_Data.req

    L_Poll_Data_req = 0x13, // L_Poll_Data.req
    L_Poll_Data_con = 0x25, // L_Poll_Data.con

    L_Meter_ind = 0x24, // L_Meter.ind

    N_Data_Individual_req = 0x21, // N_Data_Individual.req
    N_Data_Individual_con = 0x4E, // N_Data_Individual.con
    N_Data_Individual_ind = 0x49, // N_Data_Individual.ind

    N_Data_Group_req = 0x22, // N_Data_Group.req
    N_Data_Group_con = 0x3E, // N_Data_Group.con
    N_Data_Group_ind = 0x3A, // N_Data_Group.ind

    N_Data_Broadcast_req = 0x2C, // N_Data_Broadcast.req
    N_Data_Broadcast_con = 0x4F, // N_Data_Broadcast.con
    N_Data_Broadcast_ind = 0x4D, // N_Data_Broadcast.ind

    N_Poll_Data_req = 0x23, // N_Poll_Data.req
    N_Poll_Data_con = 0x35, // N_Poll_Data.con

    T_Connect_req = 0x43, // T_Connect.req
    T_Connect_con = 0x86, // T_Connect.con
    T_Connect_ind = 0x85, // T_Connect.ind

    T_Disconnect_req = 0x44, // T_Disconnect.req
    T_Disconnect_con = 0x88, // T_Disconnect.con
    T_Disconnect_ind = 0x87, // T_Disconnect.ind

    T_Data_Connected_req = 0x41, // T_Data_Connected.req
    T_Data_Connected_con = 0x8E, // T_Data_Connected.con
    T_Data_Connected_ind = 0x89, // T_Data_Connected.ind

    T_Data_Group_req = 0x32, // T_Data_Group.req
    T_Data_Group_con = 0x7E, // T_Data_Group.con
    T_Data_Group_ind = 0x7A, // T_Data_Group.ind

    T_Data_Broadcast_req = 0x4C, // T_Data_Broadcast.req
    T_Data_Broadcast_con = 0x8F, // T_Data_Broadcast.con
    T_Data_Broadcast_ind = 0x8D, // T_Data_Broadcast.ind

    T_Data_Individual_req = 0x4A, // T_Data_Individual.req
    T_Data_Individual_con = 0x9C, // T_Data_Individual.con
    T_Data_Individual_ind = 0x94, // T_Data_Individual.ind

    T_Poll_Data_req = 0x33, // T_Poll_Data.req
    T_Poll_Data_con = 0x75, // T_Poll_Data.con

    M_Connect_ind = 0xD5, // M_Connect.ind
    M_Disconnect_ind = 0xD7, // M_Disconnect.ind

    M_User_Data_Connected_req = 0x82, // M_User_Data_Connected.req
    M_User_Data_Connected_con = 0xD1, // M_User_Data_Connected.con
    M_User_Data_Connected_ind = 0xD2, // M_User_Data_Connected.ind

    A_Data_Group_req = 0x72, // A_Data_Group.req
    A_Data_Group_con = 0xEE, // A_Data_Group.con
    A_Data_Group_ind = 0xEA, // A_Data_Group.ind

    M_User_Data_Individual_req = 0x81, // M_User_Data_Individual.req
    M_User_Data_Individual_con = 0xDE, // M_User_Data_Individual.con
    M_User_Data_Individual_ind = 0xD9, // M_User_Data_Individual.ind

    A_Poll_Data_req = 0x73, // A_Poll_Data.req
    A_Poll_Data_con = 0xE5, // A_Poll_Data.con

    M_InterfaceObj_Data_req = 0x9A, // M_InterfaceObj_Data.req
    M_InterfaceObj_Data_con = 0xDC, // M_InterfaceObj_Data.con
    M_InterfaceObj_Data_ind = 0xD4, // M_InterfaceObj_Data.ind

    U_Value_Read_req = 0x74, // U_Value_Read.req
    U_Value_Read_con = 0xE4, // U_Value_Read.con

    U_Flags_Read_req = 0x7C, // U_Flags_Read.req
    U_Flags_Read_con = 0xEC, // U_Flags_Read.con

    U_Event_ind = 0xE7, // U_Event.ind

    U_Value_Write_req = 0x71, // U_Value_Write.req

    U_User_Data_0 = 0xD0, // U_User_Data
    //    U_User_Data_1 = 0xD1, // U_User_Data
    //    U_User_Data_2 = 0xD2, // U_User_Data
    U_User_Data_3 = 0xD3, // U_User_Data
    //    U_User_Data_4 = 0xD4, // U_User_Data
    //    U_User_Data_5 = 0xD5, // U_User_Data
    U_User_Data_6 = 0xD6, // U_User_Data
    //    U_User_Data_7 = 0xD7, // U_User_Data
    U_User_Data_8 = 0xD8, // U_User_Data
    //    U_User_Data_9 = 0xD9, // U_User_Data
    U_User_Data_A = 0xDA, // U_User_Data
    U_User_Data_B = 0xDB, // U_User_Data
    //    U_User_Data_C = 0xDC, // U_User_Data
    U_User_Data_D = 0xDD, // U_User_Data
    //    U_User_Data_E = 0xDE, // U_User_Data
    U_User_Data_F = 0xDF, // U_User_Data

    PC_Set_Value_req = 0xA6, // PC_Set_Value.req

    PC_Get_Value_req = 0xAC, // PC_Get_Value.req
    PC_Get_Value_con = 0xAB, // PC_Get_Value.con

    PEI_Identify_req = 0xA7, // PEI_Identify.req
    PEI_Identify_con = 0xA8, // PEI_Identify.con

    PEI_Switch_req = 0xA9, // PEI_Switch.req

    TM_Timer_ind = 0xC1 // TM_Timer.ind
};

EMI2_Message_Code to_EMI2_Message_Code(const EMI_Message_Code emi);
EMI_Message_Code from_EMI2_Message_Code(const EMI2_Message_Code emi2);

}
