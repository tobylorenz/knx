// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/EMI_Message.h>

#include <cassert>

#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_req.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_Connect/EMI_M_Connect_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_Disconnect/EMI_M_Disconnect_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_req.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual_req.h>
#include <KNX/03/06/03/EMI/Busmonitor/L_Busmon/EMI_L_Busmon_ind.h>
#include <KNX/03/06/03/EMI/Busmonitor/LM_Reset/EMI_LM_Reset_ind.h>
#include <KNX/03/06/03/EMI/Busmonitor/L_Plain_Data/EMI_L_Plain_Data_req.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_con.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_ind.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_req.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data_con.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast_con.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast_ind.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_ind.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Group/EMI_N_Data_Group_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Group/EMI_N_Data_Group_ind.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Group/EMI_N_Data_Group_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_ind.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Poll_Data/EMI_N_Poll_Data_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Poll_Data/EMI_N_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value_con.h>
#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value_req.h>
#include <KNX/03/06/03/EMI/Other/PC_Set_Value/EMI_PC_Set_Value_req.h>
#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify_con.h>
#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify_req.h>
#include <KNX/03/06/03/EMI/Other/PEI_Switch/EMI_PEI_Switch_req.h>
#include <KNX/03/06/03/EMI/Other/TM_Timer/EMI_TM_Timer_ind.h>
#include <KNX/03/06/03/EMI/Physical_Layer/Ph_Data/EMI_Ph_Data_con.h>
#include <KNX/03/06/03/EMI/Physical_Layer/Ph_Data/EMI_Ph_Data_ind.h>
#include <KNX/03/06/03/EMI/Physical_Layer/Ph_Data/EMI_Ph_Data_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_SystemBroadcast/EMI_T_Data_SystemBroadcast_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_SystemBroadcast/EMI_T_Data_SystemBroadcast_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_SystemBroadcast/EMI_T_Data_SystemBroadcast_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Poll_Data/EMI_T_Poll_Data_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Poll_Data/EMI_T_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_con.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_ind.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_req.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Event/EMI_U_Event_ind.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Flags_Read/EMI_U_Flags_Read_con.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Flags_Read/EMI_U_Flags_Read_req.h>
#include <KNX/03/06/03/EMI/User_Layer/U_User_Data/EMI_U_User_Data.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read_con.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read_req.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Write/EMI_U_Value_Write_req.h>

namespace KNX {

EMI_Message::EMI_Message()
{
}

EMI_Message::EMI_Message(const EMI_Message_Code message_code) :
    message_code(message_code)
{
}

void EMI_Message::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 1);

    message_code = static_cast<EMI_Message_Code>(*first++);

    assert(first == last);
}

std::vector<uint8_t> EMI_Message::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(static_cast<uint8_t>(message_code));

    return data;
}

std::shared_ptr<EMI_Message> make_EMI_Message(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 1);

    const EMI_Message_Code message_code = static_cast<EMI_Message_Code>(*first);
    switch (message_code) {
    case EMI_Message_Code::UNDEFINED:
        break;
    case EMI_Message_Code::Ph_Data_req: {
        // @note Ph_Data.req mentioned, but not specified
    }
    break;
    case EMI_Message_Code::Ph_Data_con: {
        // @note Ph_Data.con mentioned, but not specified
    }
    break;
    case EMI_Message_Code::Ph_Data_ind: {
        // @note Ph_Data.ind mentioned, but not specified
    }
    break;
    case EMI_Message_Code::L_Busmon_ind: {
        std::shared_ptr<EMI_L_Busmon_ind> frame = std::make_shared<EMI_L_Busmon_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_Data_req: {
        std::shared_ptr<EMI_L_Data_req> frame = std::make_shared<EMI_L_Data_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_Data_con: {
        std::shared_ptr<EMI_L_Data_con> frame = std::make_shared<EMI_L_Data_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_Data_ind: {
        std::shared_ptr<EMI_L_Data_ind> frame = std::make_shared<EMI_L_Data_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_SystemBroadcast_req: {
        std::shared_ptr<EMI_L_SystemBroadcast_req> frame = std::make_shared<EMI_L_SystemBroadcast_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_SystemBroadcast_con: {
        std::shared_ptr<EMI_L_SystemBroadcast_con> frame = std::make_shared<EMI_L_SystemBroadcast_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_SystemBroadcast_ind: {
        std::shared_ptr<EMI_L_SystemBroadcast_ind> frame = std::make_shared<EMI_L_SystemBroadcast_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_Plain_Data_req: {
        std::shared_ptr<EMI_L_Plain_Data_req> frame = std::make_shared<EMI_L_Plain_Data_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_Poll_Data_req: {
        std::shared_ptr<EMI_L_Poll_Data_req> frame = std::make_shared<EMI_L_Poll_Data_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_Poll_Data_con: {
        std::shared_ptr<EMI_L_Poll_Data_con> frame = std::make_shared<EMI_L_Poll_Data_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::L_Meter_ind: {
        // @note L_Meter.ind mentioned, but not specified
    }
    break;
    case EMI_Message_Code::N_Data_Individual_req: {
        std::shared_ptr<EMI_N_Data_Individual_req> frame = std::make_shared<EMI_N_Data_Individual_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Data_Individual_con: {
        std::shared_ptr<EMI_N_Data_Individual_con> frame = std::make_shared<EMI_N_Data_Individual_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Data_Broadcast_ind: {
        std::shared_ptr<EMI_N_Data_Broadcast_ind> frame = std::make_shared<EMI_N_Data_Broadcast_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Data_Group_req: {
        std::shared_ptr<EMI_N_Data_Group_req> frame = std::make_shared<EMI_N_Data_Group_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Data_Group_con: {
        std::shared_ptr<EMI_N_Data_Group_con> frame = std::make_shared<EMI_N_Data_Group_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Data_Group_ind: {
        std::shared_ptr<EMI_N_Data_Group_ind> frame = std::make_shared<EMI_N_Data_Group_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Data_Broadcast_req: {
        std::shared_ptr<EMI_N_Data_Broadcast_req> frame = std::make_shared<EMI_N_Data_Broadcast_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Data_Broadcast_con: {
        std::shared_ptr<EMI_N_Data_Broadcast_con> frame = std::make_shared<EMI_N_Data_Broadcast_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Data_Individual_ind: {
        std::shared_ptr<EMI_N_Data_Individual_ind> frame = std::make_shared<EMI_N_Data_Individual_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Poll_Data_req: {
        std::shared_ptr<EMI_N_Poll_Data_req> frame = std::make_shared<EMI_N_Poll_Data_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::N_Poll_Data_con: {
        std::shared_ptr<EMI_N_Poll_Data_con> frame = std::make_shared<EMI_N_Poll_Data_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Connect_req: {
        std::shared_ptr<EMI_T_Connect_req> frame = std::make_shared<EMI_T_Connect_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Connect_con: {
        std::shared_ptr<EMI_T_Connect_con> frame = std::make_shared<EMI_T_Connect_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Connect_ind: {
        std::shared_ptr<EMI_T_Connect_ind> frame = std::make_shared<EMI_T_Connect_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Disconnect_req: {
        std::shared_ptr<EMI_T_Disconnect_req> frame = std::make_shared<EMI_T_Disconnect_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Disconnect_con: {
        std::shared_ptr<EMI_T_Disconnect_con> frame = std::make_shared<EMI_T_Disconnect_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Disconnect_ind: {
        std::shared_ptr<EMI_T_Disconnect_ind> frame = std::make_shared<EMI_T_Disconnect_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Connected_req: {
        std::shared_ptr<EMI_T_Data_Connected_req> frame = std::make_shared<EMI_T_Data_Connected_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Connected_con: {
        std::shared_ptr<EMI_T_Data_Connected_con> frame = std::make_shared<EMI_T_Data_Connected_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Connected_ind: {
        std::shared_ptr<EMI_T_Data_Connected_ind> frame = std::make_shared<EMI_T_Data_Connected_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Group_req: {
        std::shared_ptr<EMI_T_Data_Group_req> frame = std::make_shared<EMI_T_Data_Group_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Group_con: {
        std::shared_ptr<EMI_T_Data_Group_con> frame = std::make_shared<EMI_T_Data_Group_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Group_ind: {
        std::shared_ptr<EMI_T_Data_Group_ind> frame = std::make_shared<EMI_T_Data_Group_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Broadcast_req: {
        std::shared_ptr<EMI_T_Data_Broadcast_req> frame = std::make_shared<EMI_T_Data_Broadcast_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Broadcast_con: {
        std::shared_ptr<EMI_T_Data_Broadcast_con> frame = std::make_shared<EMI_T_Data_Broadcast_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Broadcast_ind: {
        std::shared_ptr<EMI_T_Data_Broadcast_ind> frame = std::make_shared<EMI_T_Data_Broadcast_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_SystemBroadcast_req: {
        std::shared_ptr<EMI_T_Data_SystemBroadcast_req> frame = std::make_shared<EMI_T_Data_SystemBroadcast_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_SystemBroadcast_con: {
        std::shared_ptr<EMI_T_Data_SystemBroadcast_con> frame = std::make_shared<EMI_T_Data_SystemBroadcast_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_SystemBroadcast_ind: {
        std::shared_ptr<EMI_T_Data_SystemBroadcast_ind> frame = std::make_shared<EMI_T_Data_SystemBroadcast_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Individual_req: {
        std::shared_ptr<EMI_T_Data_Individual_req> frame = std::make_shared<EMI_T_Data_Individual_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Individual_con: {
        std::shared_ptr<EMI_T_Data_Individual_con> frame = std::make_shared<EMI_T_Data_Individual_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Data_Individual_ind: {
        std::shared_ptr<EMI_T_Data_Individual_ind> frame = std::make_shared<EMI_T_Data_Individual_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Poll_Data_req: {
        std::shared_ptr<EMI_T_Poll_Data_req> frame = std::make_shared<EMI_T_Poll_Data_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::T_Poll_Data_con: {
        std::shared_ptr<EMI_T_Poll_Data_con> frame = std::make_shared<EMI_T_Poll_Data_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_Connect_ind: {
        std::shared_ptr<EMI_M_Connect_ind> frame = std::make_shared<EMI_M_Connect_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_Disconnect_ind: {
        std::shared_ptr<EMI_M_Disconnect_ind> frame = std::make_shared<EMI_M_Disconnect_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_User_Data_Connected_req: {
        std::shared_ptr<EMI_M_User_Data_Connected_req> frame = std::make_shared<EMI_M_User_Data_Connected_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_User_Data_Connected_con: {
        std::shared_ptr<EMI_M_User_Data_Connected_con> frame = std::make_shared<EMI_M_User_Data_Connected_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_User_Data_Connected_ind: {
        std::shared_ptr<EMI_M_User_Data_Connected_ind> frame = std::make_shared<EMI_M_User_Data_Connected_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::A_Data_Group_req: {
        std::shared_ptr<EMI_A_Data_Group_req> frame = std::make_shared<EMI_A_Data_Group_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::A_Data_Group_con: {
        std::shared_ptr<EMI_A_Data_Group_con> frame = std::make_shared<EMI_A_Data_Group_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::A_Data_Group_ind: {
        std::shared_ptr<EMI_A_Data_Group_ind> frame = std::make_shared<EMI_A_Data_Group_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_User_Data_Individual_req: {
        std::shared_ptr<EMI_M_User_Data_Individual_req> frame = std::make_shared<EMI_M_User_Data_Individual_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_User_Data_Individual_con: {
        std::shared_ptr<EMI_M_User_Data_Individual_con> frame = std::make_shared<EMI_M_User_Data_Individual_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_User_Data_Individual_ind: {
        std::shared_ptr<EMI_M_User_Data_Individual_ind> frame = std::make_shared<EMI_M_User_Data_Individual_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::A_Poll_Data_req: {
        std::shared_ptr<EMI_A_Poll_Data_req> frame = std::make_shared<EMI_A_Poll_Data_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::A_Poll_Data_con: {
        std::shared_ptr<EMI_A_Poll_Data_con> frame = std::make_shared<EMI_A_Poll_Data_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_InterfaceObj_Data_req: {
        std::shared_ptr<EMI_M_InterfaceObj_Data_req> frame = std::make_shared<EMI_M_InterfaceObj_Data_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_InterfaceObj_Data_con: {
        std::shared_ptr<EMI_M_InterfaceObj_Data_con> frame = std::make_shared<EMI_M_InterfaceObj_Data_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::M_InterfaceObj_Data_ind: {
        std::shared_ptr<EMI_M_InterfaceObj_Data_ind> frame = std::make_shared<EMI_M_InterfaceObj_Data_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::U_Value_Read_req: {
        std::shared_ptr<EMI_U_Value_Read_req> frame = std::make_shared<EMI_U_Value_Read_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::U_Value_Read_con: {
        std::shared_ptr<EMI_U_Value_Read_con> frame = std::make_shared<EMI_U_Value_Read_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::U_Flags_Read_req: {
        std::shared_ptr<EMI_U_Flags_Read_req> frame = std::make_shared<EMI_U_Flags_Read_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::U_Flags_Read_con: {
        std::shared_ptr<EMI_U_Flags_Read_con> frame = std::make_shared<EMI_U_Flags_Read_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::U_Event_ind: {
        std::shared_ptr<EMI_U_Event_ind> frame = std::make_shared<EMI_U_Event_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::U_Value_Write_req: {
        std::shared_ptr<EMI_U_Value_Write_req> frame = std::make_shared<EMI_U_Value_Write_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::U_User_Data_0:
    case EMI_Message_Code::U_User_Data_1:
    case EMI_Message_Code::U_User_Data_2:
    case EMI_Message_Code::U_User_Data_3:
    case EMI_Message_Code::U_User_Data_4:
    case EMI_Message_Code::U_User_Data_5:
    case EMI_Message_Code::U_User_Data_6:
    case EMI_Message_Code::U_User_Data_7:
    case EMI_Message_Code::U_User_Data_8:
    case EMI_Message_Code::U_User_Data_9:
    case EMI_Message_Code::U_User_Data_A:
    case EMI_Message_Code::U_User_Data_B:
    case EMI_Message_Code::U_User_Data_C:
    case EMI_Message_Code::U_User_Data_D:
    case EMI_Message_Code::U_User_Data_E:
    case EMI_Message_Code::U_User_Data_F: {
        std::shared_ptr<EMI_U_User_Data> frame = std::make_shared<EMI_U_User_Data>(message_code);
        frame->fromData(first, last);
        return frame;
    }
    case EMI_Message_Code::PC_Set_Value_req: {
        std::shared_ptr<EMI_PC_Set_Value_req> frame = std::make_shared<EMI_PC_Set_Value_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::PC_Get_Value_req: {
        std::shared_ptr<EMI_PC_Get_Value_req> frame = std::make_shared<EMI_PC_Get_Value_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::PC_Get_Value_con: {
        std::shared_ptr<EMI_PC_Get_Value_con> frame = std::make_shared<EMI_PC_Get_Value_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::PEI_Identify_req: {
        std::shared_ptr<EMI_PEI_Identify_req> frame = std::make_shared<EMI_PEI_Identify_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::PEI_Identify_con: {
        std::shared_ptr<EMI_PEI_Identify_con> frame = std::make_shared<EMI_PEI_Identify_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::PEI_Switch_req: {
        std::shared_ptr<EMI_PEI_Switch_req> frame = std::make_shared<EMI_PEI_Switch_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case EMI_Message_Code::TM_Timer_ind: {
        std::shared_ptr<EMI_TM_Timer_ind> frame = std::make_shared<EMI_TM_Timer_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    }

    std::shared_ptr<EMI_Message> frame = std::make_shared<EMI_Message>();
    frame->fromData(first, last);
    return frame;
}

}
