// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/06/03/EMI/EMI_Message_Code.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * EMI Message
 */
class KNX_EXPORT EMI_Message
{
public:
    EMI_Message();
    explicit EMI_Message(const EMI_Message_Code message_code);
    virtual ~EMI_Message() = default;
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /** Message Code */
    EMI_Message_Code message_code{EMI_Message_Code::UNDEFINED};
};

KNX_EXPORT std::shared_ptr<EMI_Message> make_EMI_Message(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
