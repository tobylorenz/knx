// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * EMI Message Codes
 *
 * @ingroup KNX_03_06_03_02
 */
enum class EMI_Message_Code : uint8_t {
    UNDEFINED = 0,

    Ph_Data_req, // Ph_Data.req
    Ph_Data_con, // Ph_Data.con
    Ph_Data_ind, // Ph_Data.ind

    L_Busmon_ind, // L_Busmon.ind

    L_Data_req, // L_Data.req
    L_Data_con, // L_Data.con
    L_Data_ind, // L_Data.ind

    L_SystemBroadcast_req, // L_SystemBroadcast.req
    L_SystemBroadcast_con, // L_SystemBroadcast.con
    L_SystemBroadcast_ind, // L_SystemBroadcast.ind

    L_Plain_Data_req, // L_Plain_Data.req

    L_Poll_Data_req, // L_Poll_Data.req
    L_Poll_Data_con, // L_Poll_Data.con

    L_Meter_ind, // L_Meter.ind

    N_Data_Individual_req, // N_Data_Individual.req
    N_Data_Individual_con, // N_Data_Individual.con
    N_Data_Individual_ind, // N_Data_Individual.ind

    N_Data_Group_req, // N_Data_Group.req
    N_Data_Group_con, // N_Data_Group.con
    N_Data_Group_ind, // N_Data_Group.ind

    N_Data_Broadcast_req, // N_Data_Broadcast.req
    N_Data_Broadcast_con, // N_Data_Broadcast.con
    N_Data_Broadcast_ind, // N_Data_Broadcast.ind

    N_Poll_Data_req, // N_Poll_Data.req
    N_Poll_Data_con, // N_Poll_Data.con

    T_Connect_req, // T_Connect.req
    T_Connect_con, // T_Connect.con
    T_Connect_ind, // T_Connect.ind

    T_Disconnect_req, // T_Disconnect.req
    T_Disconnect_con, // T_Disconnect.con
    T_Disconnect_ind, // T_Disconnect.ind

    T_Data_Connected_req, // T_Data_Connected.req
    T_Data_Connected_con, // T_Data_Connected.con
    T_Data_Connected_ind, // T_Data_Connected.ind

    T_Data_Group_req, // T_Data_Group.req
    T_Data_Group_con, // T_Data_Group.con
    T_Data_Group_ind, // T_Data_Group.ind

    T_Data_Broadcast_req, // T_Data_Broadcast.req
    T_Data_Broadcast_con, // T_Data_Broadcast.con
    T_Data_Broadcast_ind, // T_Data_Broadcast.ind

    T_Data_SystemBroadcast_req, // T_Data_SystemBroadcast.req
    T_Data_SystemBroadcast_con, // T_Data_SystemBroadcast.con
    T_Data_SystemBroadcast_ind, // T_Data_SystemBroadcast.ind

    T_Data_Individual_req, // T_Data_Individual.req
    T_Data_Individual_con, // T_Data_Individual.con
    T_Data_Individual_ind, // T_Data_Individual.ind

    T_Poll_Data_req, // T_Poll_Data.req
    T_Poll_Data_con, // T_Poll_Data.con

    M_Connect_ind, // M_Connect.ind
    M_Disconnect_ind, // M_Disconnect.ind

    M_User_Data_Connected_req, // M_User_Data_Connected.req
    M_User_Data_Connected_con, // M_User_Data_Connected.con
    M_User_Data_Connected_ind, // M_User_Data_Connected.ind

    A_Data_Group_req, // A_Data_Group.req
    A_Data_Group_con, // A_Data_Group.con
    A_Data_Group_ind, // A_Data_Group.ind

    M_User_Data_Individual_req, // M_User_Data_Individual.req
    M_User_Data_Individual_con, // M_User_Data_Individual.con
    M_User_Data_Individual_ind, // M_User_Data_Individual.ind

    A_Poll_Data_req, // A_Poll_Data.req
    A_Poll_Data_con, // A_Poll_Data.con

    M_InterfaceObj_Data_req, // M_InterfaceObj_Data.req
    M_InterfaceObj_Data_con, // M_InterfaceObj_Data.con
    M_InterfaceObj_Data_ind, // M_InterfaceObj_Data.ind

    U_Value_Read_req, // U_Value_Read.req
    U_Value_Read_con, // U_Value_Read.con

    U_Flags_Read_req, // U_Flags_Read.req
    U_Flags_Read_con, // U_Flags_Read.con

    U_Event_ind, // U_Event.ind

    U_Value_Write_req, // U_Value_Write.req

    U_User_Data_0, // U_User_Data
    U_User_Data_1, // U_User_Data
    U_User_Data_2, // U_User_Data
    U_User_Data_3, // U_User_Data
    U_User_Data_4, // U_User_Data
    U_User_Data_5, // U_User_Data
    U_User_Data_6, // U_User_Data
    U_User_Data_7, // U_User_Data
    U_User_Data_8, // U_User_Data
    U_User_Data_9, // U_User_Data
    U_User_Data_A, // U_User_Data
    U_User_Data_B, // U_User_Data
    U_User_Data_C, // U_User_Data
    U_User_Data_D, // U_User_Data
    U_User_Data_E, // U_User_Data
    U_User_Data_F, // U_User_Data

    PC_Set_Value_req, // PC_Set_Value.req

    PC_Get_Value_req, // PC_Get_Value.req
    PC_Get_Value_con, // PC_Get_Value.con

    PEI_Identify_req, // PEI_Identify.req
    PEI_Identify_con, // PEI_Identify.con

    PEI_Switch_req, // PEI_Switch.req

    TM_Timer_ind // TM_Timer.ind
};

}
