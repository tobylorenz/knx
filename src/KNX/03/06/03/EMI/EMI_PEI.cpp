// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/EMI_PEI.h>

#include <cassert>

#include <KNX/03/03/03/NPDU.h>
#include <KNX/03/03/07/APDU.h>
#include <KNX/03/03/07/A_DeviceDescriptor_Read/A_DeviceDescriptor_Read_PDU.h>
#include <KNX/03/03/07/A_DeviceDescriptor_Response/A_DeviceDescriptor_Response_PDU.h>
#include <KNX/03/03/07/A_FunctionPropertyCommand/A_FunctionPropertyCommand_PDU.h>
#include <KNX/03/03/07/A_FunctionPropertyState_Read/A_FunctionPropertyState_Read_PDU.h>
#include <KNX/03/03/07/A_FunctionPropertyState_Response/A_FunctionPropertyState_Response_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_Read/A_NetworkParameter_Read_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_Response/A_NetworkParameter_Response_PDU.h>
#include <KNX/03/03/07/A_NetworkParameter_Write/A_NetworkParameter_Write_PDU.h>
#include <KNX/03/03/07/A_PropertyDescription_Read/A_PropertyDescription_Read_PDU.h>
#include <KNX/03/03/07/A_PropertyDescription_Response/A_PropertyDescription_Response_PDU.h>
#include <KNX/03/03/07/A_PropertyValue_Read/A_PropertyValue_Read_PDU.h>
#include <KNX/03/03/07/A_PropertyValue_Response/A_PropertyValue_Response_PDU.h>
#include <KNX/03/03/07/A_PropertyValue_Write/A_PropertyValue_Write_PDU.h>
#include <KNX/03/03/07/A_SystemNetworkParameter_Read/A_SystemNetworkParameter_Read_PDU.h>
#include <KNX/03/03/07/A_SystemNetworkParameter_Response/A_SystemNetworkParameter_Response_PDU.h>
#include <KNX/03/03/07/A_SystemNetworkParameter_Write/A_SystemNetworkParameter_Write_PDU.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Data_Group/EMI_A_Data_Group_req.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/A_Poll_Data/EMI_A_Poll_Data_req.h>
//#include <KNX/03/06/03/EMI/Application_Layer/M_Connect/EMI_M_Connect_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_Connect/EMI_M_Connect_ind.h>
//#include <KNX/03/06/03/EMI/Application_Layer/M_Disconnect/EMI_M_Disconnect_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_Disconnect/EMI_M_Disconnect_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Connected/EMI_M_User_Data_Connected_req.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual_con.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual_ind.h>
#include <KNX/03/06/03/EMI/Application_Layer/M_User_Data_Individual/EMI_M_User_Data_Individual_req.h>
#include <KNX/03/06/03/EMI/Busmonitor/LM_Reset/EMI_LM_Reset_ind.h>
#include <KNX/03/06/03/EMI/Busmonitor/L_Busmon/EMI_L_Busmon_ind.h>
#include <KNX/03/06/03/EMI/Busmonitor/L_Plain_Data/EMI_L_Plain_Data_req.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_con.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_ind.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Data/EMI_L_Data_req.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data_con.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_Poll_Data/EMI_L_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast_con.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast_ind.h>
#include <KNX/03/06/03/EMI/Data_Link_Layer/L_SystemBroadcast/EMI_L_SystemBroadcast_req.h>
#include <KNX/03/06/03/EMI/EMI1_Message_Code.h>
#include <KNX/03/06/03/EMI/EMI2_Message_Code.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_ind.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Group/EMI_N_Data_Group_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Group/EMI_N_Data_Group_ind.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Group/EMI_N_Data_Group_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_ind.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_req.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Poll_Data/EMI_N_Poll_Data_con.h>
#include <KNX/03/06/03/EMI/Network_Layer/N_Poll_Data/EMI_N_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value_con.h>
#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value_req.h>
#include <KNX/03/06/03/EMI/Other/PC_Set_Value/EMI_PC_Set_Value_req.h>
#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify_con.h>
#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify_req.h>
#include <KNX/03/06/03/EMI/Other/PEI_Switch/EMI_PEI_Switch_req.h>
#include <KNX/03/06/03/EMI/Other/TM_Timer/EMI_TM_Timer_ind.h>
#include <KNX/03/06/03/EMI/Physical_Layer/Ph_Data/EMI_Ph_Data_con.h>
#include <KNX/03/06/03/EMI/Physical_Layer/Ph_Data/EMI_Ph_Data_ind.h>
#include <KNX/03/06/03/EMI/Physical_Layer/Ph_Data/EMI_Ph_Data_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_SystemBroadcast/EMI_T_Data_SystemBroadcast_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_SystemBroadcast/EMI_T_Data_SystemBroadcast_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_SystemBroadcast/EMI_T_Data_SystemBroadcast_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_ind.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_req.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Poll_Data/EMI_T_Poll_Data_con.h>
#include <KNX/03/06/03/EMI/Transport_Layer/T_Poll_Data/EMI_T_Poll_Data_req.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_con.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_ind.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_req.h>
#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_req.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Event/EMI_U_Event_ind.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Flags_Read/EMI_U_Flags_Read_con.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Flags_Read/EMI_U_Flags_Read_req.h>
#include <KNX/03/06/03/EMI/User_Layer/U_User_Data/EMI_U_User_Data.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read_con.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read_req.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Write/EMI_U_Value_Write_req.h>
#include <KNX/03/05/01/Interface_Object_Type.h>

namespace KNX {

EMI_PEI::EMI_PEI(asio::io_context & io_context) :
    io_context(io_context),
    interface_objects(std::make_shared<Interface_Objects>()),
    current_level(std::make_shared<Level>()),
    network_layer(),
    transport_layer(network_layer, io_context),
    application_layer(transport_layer, io_context),
    application_interface_layer(application_layer),
    user_layer(application_interface_layer, application_layer)
{
    // interface objects
    std::shared_ptr<Device_Object> device_object = interface_objects->device();
    (void) device_object->interface_object_type();
    (void) device_object->device_control();
    (void) device_object->routing_count();
    std::shared_ptr<Group_Address_Table> group_address_table = interface_objects->group_address_table();
    (void) group_address_table->interface_object_type();
    std::shared_ptr<Group_Object_Association_Table> group_object_association_table = interface_objects->group_object_association_table();
    (void) group_object_association_table->interface_object_type();
    std::shared_ptr<Group_Object_Table> group_object_table = interface_objects->group_object_table();
    (void) group_object_table->interface_object_type();

    // network layer
    network_layer.device_object = device_object;

    // transport layer
    transport_layer.group_address_table = group_address_table;

    // application layer
    application_layer.group_object_association_table = group_object_association_table;
    A_PropertyValue_Read_ind_initiator();
    A_PropertyValue_Read_Acon_initiator();

    // application interface layer
    application_interface_layer.group_object_server.group_object_association_table = group_object_association_table;
    application_interface_layer.group_object_server.group_object_table = group_object_table;
    application_interface_layer.interface_object_server.interface_objects = interface_objects;
    application_interface_layer.interface_object_server.current_level = current_level;
    // application_interface_layer.file_server.file_server_objects = file_server_object;
    // Group Object Server
    application_interface_layer.group_object_server.U_Value_Read_con = std::bind(&EMI_PEI::U_Value_Read_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    application_interface_layer.group_object_server.U_Flags_Read_con = std::bind(&EMI_PEI::U_Flags_Read_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    application_interface_layer.group_object_server.U_Event_ind = std::bind(&EMI_PEI::U_Event_ind, this, std::placeholders::_1);
    // Interface Object Server

    // user (application) layer
    user_layer.device_object = device_object;
    user_layer.group_address_table = group_address_table;
    user_layer.memory = &memory;

    // PEI
}

void EMI_PEI::connect(Data_Link_Layer & data_link_layer)
{
    // data link layer
    this->data_link_layer = &data_link_layer;
    data_link_layer.device_object = interface_objects->device();
    data_link_layer.group_address_table = interface_objects->group_address_table();
    data_link_layer.L_Busmon_ind = std::bind(&EMI_PEI::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);

    // network layer
    network_layer.connect(data_link_layer);
}

void EMI_PEI::emi_req(std::vector<uint8_t> data)
{
    assert(data.size() >= 1);

    switch (emi_version) {
    case EMI_Version::EMI:
        break;
    case EMI_Version::EMI1:
        data[0] = static_cast<uint8_t>(from_EMI1_Message_Code(static_cast<EMI1_Message_Code>(data[0])));
        break;
    case EMI_Version::EMI2:
        data[0] = static_cast<uint8_t>(from_EMI2_Message_Code(static_cast<EMI2_Message_Code>(data[0])));
        break;
    }

    std::shared_ptr<EMI_Message> pdu = make_EMI_Message(std::cbegin(data), std::cend(data));

    // ordered according to Table 1 - Overview EMI message codes
    switch (pdu->message_code) {
    case EMI_Message_Code::Ph_Data_req: {
        std::shared_ptr<EMI_Ph_Data_req> pdu2 = std::dynamic_pointer_cast<EMI_Ph_Data_req>(pdu);
        assert(pdu2);

        // @note Ph_Data.req mentioned, but not specified
    }
    break;
    case EMI_Message_Code::L_Data_req: {
        std::shared_ptr<EMI_L_Data_req> pdu2 = std::dynamic_pointer_cast<EMI_L_Data_req>(pdu);
        assert(pdu2);

        if (data_link_layer) {
            assert(interface_objects->group_address_table());
            Frame_Format frame_format;
            frame_format.frame_type_parameter = (pdu2->lsdu->length_calculated() <= 15) ? Frame_Type_Parameter::Standard : Frame_Type_Parameter::Extended;
            data_link_layer->L_Data_req(pdu2->control.acknowledge, pdu2->address_type, pdu2->destination_address, frame_format, pdu2->lsdu, pdu2->control.priority, interface_objects->group_address_table()->individual_address);
        }
    }
    break;
    case EMI_Message_Code::L_SystemBroadcast_req: {
        std::shared_ptr<EMI_L_SystemBroadcast_req> pdu2 = std::dynamic_pointer_cast<EMI_L_SystemBroadcast_req>(pdu);
        assert(pdu2);

        if (data_link_layer) {
            Frame_Format frame_format;
            frame_format.frame_type_parameter = (pdu2->lsdu->length_calculated() <= 15) ? Frame_Type_Parameter::Standard : Frame_Type_Parameter::Extended;
            data_link_layer->L_SystemBroadcast_req(pdu2->control.acknowledge, Group_Address(pdu2->destination_address), frame_format, pdu2->lsdu, pdu2->control.priority);
        }
    }
    break;
    case EMI_Message_Code::L_Plain_Data_req: {
        std::shared_ptr<EMI_L_Plain_Data_req> pdu2 = std::dynamic_pointer_cast<EMI_L_Plain_Data_req>(pdu);
        assert(pdu2);

        if (data_link_layer) {
            data_link_layer->L_Plain_Data_req(pdu2->time, pdu2->data);
        }
    }
    break;
    case EMI_Message_Code::L_Poll_Data_req: {
        std::shared_ptr<EMI_L_Poll_Data_req> pdu2 = std::dynamic_pointer_cast<EMI_L_Poll_Data_req>(pdu);
        assert(pdu2);

        if (data_link_layer) {
            data_link_layer->L_Poll_Data_req(Group_Address(pdu2->destination_address), pdu2->nr_of_slots);
        }
    }
    break;
    case EMI_Message_Code::N_Data_Individual_req: {
        std::shared_ptr<EMI_N_Data_Individual_req> pdu2 = std::dynamic_pointer_cast<EMI_N_Data_Individual_req>(pdu);
        assert(pdu2);

        network_layer.N_Data_Individual_req(pdu2->control.acknowledge, Individual_Address(pdu2->destination_address), pdu2->hop_count_type, pdu2->control.priority, pdu2->nsdu);
    }
    break;
    case EMI_Message_Code::N_Data_Group_req: {
        std::shared_ptr<EMI_N_Data_Group_req> pdu2 = std::dynamic_pointer_cast<EMI_N_Data_Group_req>(pdu);
        assert(pdu2);

        network_layer.N_Data_Group_req(pdu2->control.acknowledge, Group_Address(pdu2->destination_address), Extended_Frame_Format::Standard, pdu2->hop_count_type, pdu2->control.priority, pdu2->nsdu);
    }
    break;
    case EMI_Message_Code::N_Data_Broadcast_req: {
        std::shared_ptr<EMI_N_Data_Broadcast_req> pdu2 = std::dynamic_pointer_cast<EMI_N_Data_Broadcast_req>(pdu);
        assert(pdu2);

        network_layer.N_Data_Broadcast_req(pdu2->control.acknowledge, pdu2->hop_count_type, pdu2->control.priority, pdu2->nsdu);
    }
    break;
    case EMI_Message_Code::N_Poll_Data_req: {
        std::shared_ptr<EMI_N_Poll_Data_req> pdu2 = std::dynamic_pointer_cast<EMI_N_Poll_Data_req>(pdu);
        assert(pdu2);

        if (data_link_layer) {
            // @note There is no N_Poll_Data service, but L_Poll_Data is identical
            data_link_layer->L_Poll_Data_req(Group_Address(pdu2->destination_address), pdu2->nr_of_slots);
        }
    }
    break;
    case EMI_Message_Code::T_Connect_req: {
        std::shared_ptr<EMI_T_Connect_req> pdu2 = std::dynamic_pointer_cast<EMI_T_Connect_req>(pdu);
        assert(pdu2);

        transport_layer.T_Connect_req(Individual_Address(pdu2->destination_address), pdu2->control.priority);
    }
    break;
    case EMI_Message_Code::T_Disconnect_req: {
        std::shared_ptr<EMI_T_Disconnect_req> pdu2 = std::dynamic_pointer_cast<EMI_T_Disconnect_req>(pdu);
        assert(pdu2);

        const TSAP_Connected tsap = Individual_Address(pdu2->destination_address);
        transport_layer.T_Disconnect_req(pdu2->control.priority, tsap);
    }
    break;
    case EMI_Message_Code::T_Data_Connected_req: {
        std::shared_ptr<EMI_T_Data_Connected_req> pdu2 = std::dynamic_pointer_cast<EMI_T_Data_Connected_req>(pdu);
        assert(pdu2);

        transport_layer.T_Data_Connected_req(pdu2->control.priority, current_tsap_connected, pdu2->tsdu);

        current_T_Data_Connected.priority = pdu2->control.priority;
        current_T_Data_Connected.tsdu = pdu2->tsdu;
    }
    break;
    case EMI_Message_Code::T_Data_Group_req: {
        std::shared_ptr<EMI_T_Data_Group_req> pdu2 = std::dynamic_pointer_cast<EMI_T_Data_Group_req>(pdu);
        assert(pdu2);

        const TSAP_Group tsap = Group_Address(pdu2->destination_address);
        assert(interface_objects->group_address_table());
        if ((tsap >= 1) && (tsap <= transport_layer.group_address_table->group_addresses.size())) {
            transport_layer.T_Data_Group_req(pdu2->control.acknowledge, pdu2->hop_count_type, pdu2->control.priority, tsap, pdu2->tsdu);
        } else {
            transport_layer.T_Data_Group_con(pdu2->control.acknowledge, pdu2->hop_count_type, pdu2->control.priority, interface_objects->group_address_table()->individual_address, tsap, pdu2->tsdu, Status::not_ok);
        }
    }
    break;
    case EMI_Message_Code::T_Data_Broadcast_req: {
        std::shared_ptr<EMI_T_Data_Broadcast_req> pdu2 = std::dynamic_pointer_cast<EMI_T_Data_Broadcast_req>(pdu);
        assert(pdu2);

        transport_layer.T_Data_Broadcast_req(pdu2->control.acknowledge, pdu2->hop_count_type, pdu2->control.priority, pdu2->tsdu);
    }
    break;
    case EMI_Message_Code::T_Data_SystemBroadcast_req: {
        std::shared_ptr<EMI_T_Data_SystemBroadcast_req> pdu2 = std::dynamic_pointer_cast<EMI_T_Data_SystemBroadcast_req>(pdu);
        assert(pdu2);

        transport_layer.T_Data_SystemBroadcast_req(pdu2->control.acknowledge, pdu2->hop_count_type, pdu2->control.priority, pdu2->tsdu);
    }
    break;
    case EMI_Message_Code::T_Data_Individual_req: {
        std::shared_ptr<EMI_T_Data_Individual_req> pdu2 = std::dynamic_pointer_cast<EMI_T_Data_Individual_req>(pdu);
        assert(pdu2);

        const TSAP_Individual tsap = Individual_Address(pdu2->destination_address);
        transport_layer.T_Data_Individual_req(pdu2->control.acknowledge, pdu2->hop_count_type, pdu2->control.priority, tsap, pdu2->tsdu);
    }
    break;
    case EMI_Message_Code::T_Poll_Data_req: {
        std::shared_ptr<EMI_T_Poll_Data_req> pdu2 = std::dynamic_pointer_cast<EMI_T_Poll_Data_req>(pdu);
        assert(pdu2);

        if (data_link_layer) {
            // @note There is no T_Poll_Data service, but L_Poll_Data is identical
            data_link_layer->L_Poll_Data_req(Group_Address(pdu2->destination_address), pdu2->nr_of_slots);
        }
    }
    break;
    case EMI_Message_Code::M_User_Data_Connected_req: {
        std::shared_ptr<EMI_M_User_Data_Connected_req> pdu2 = std::dynamic_pointer_cast<EMI_M_User_Data_Connected_req>(pdu);
        assert(pdu2);

        const TSAP_Connected tsap = Individual_Address(pdu2->destination_address);
        transport_layer.T_Data_Connected_req(pdu2->control.priority, tsap, pdu2->tsdu);
    }
    break;
    case EMI_Message_Code::A_Data_Group_req: {
        std::shared_ptr<EMI_A_Data_Group_req> pdu2 = std::dynamic_pointer_cast<EMI_A_Data_Group_req>(pdu);
        assert(pdu2);

        transport_layer.T_Data_Group_req(pdu2->control.acknowledge, pdu2->hop_count_type, pdu2->control.priority, Group_Address(pdu2->destination_address), pdu2->tsdu);
    }
    break;
    case EMI_Message_Code::M_User_Data_Individual_req: {
        std::shared_ptr<EMI_M_User_Data_Individual_req> pdu2 = std::dynamic_pointer_cast<EMI_M_User_Data_Individual_req>(pdu);
        assert(pdu2);

        const TSAP_Individual tsap = Individual_Address(pdu2->destination_address);
        transport_layer.T_Data_Individual_req(pdu2->control.acknowledge, pdu2->hop_count_type, pdu2->control.priority, tsap, pdu2->tsdu);
    }
    break;
    case EMI_Message_Code::A_Poll_Data_req: {
        std::shared_ptr<EMI_A_Poll_Data_req> pdu2 = std::dynamic_pointer_cast<EMI_A_Poll_Data_req>(pdu);
        assert(pdu2);

        // @note There is no A_Poll_Data service, but L_Poll_Data is identical
        data_link_layer->L_Poll_Data_req(Group_Address(pdu2->destination_address), pdu2->nr_of_slots);
    }
    break;
    case EMI_Message_Code::M_InterfaceObj_Data_req: {
        std::shared_ptr<EMI_M_InterfaceObj_Data_req> pdu2 = std::dynamic_pointer_cast<EMI_M_InterfaceObj_Data_req>(pdu);
        assert(pdu2);

        local_M_InterfaceObj_Data_req(pdu2->control, pdu2->destination_address, pdu2->communication_type, pdu2->hop_count_type, pdu2->apdu);
    }
    break;
    case EMI_Message_Code::U_Value_Read_req: {
        std::shared_ptr<EMI_U_Value_Read_req> pdu2 = std::dynamic_pointer_cast<EMI_U_Value_Read_req>(pdu);
        assert(pdu2);

        application_interface_layer.group_object_server.U_Value_Read_req(pdu2->group_object_number);
    }
    break;
    case EMI_Message_Code::U_Flags_Read_req: {
        std::shared_ptr<EMI_U_Flags_Read_req> pdu2 = std::dynamic_pointer_cast<EMI_U_Flags_Read_req>(pdu);
        assert(pdu2);

        application_interface_layer.group_object_server.U_Flags_Read_req(pdu2->group_object_number);
    }
    break;
    case EMI_Message_Code::U_Value_Write_req: {
        std::shared_ptr<EMI_U_Value_Write_req> pdu2 = std::dynamic_pointer_cast<EMI_U_Value_Write_req>(pdu);
        assert(pdu2);

        application_interface_layer.group_object_server.U_Value_Write_req(pdu2->group_object_number, pdu2->write_mask, pdu2->ram_flags, pdu2->value);
    }
    break;
    case EMI_Message_Code::U_User_Data_0:
    case EMI_Message_Code::U_User_Data_1:
    case EMI_Message_Code::U_User_Data_2:
    case EMI_Message_Code::U_User_Data_3:
    case EMI_Message_Code::U_User_Data_4:
    case EMI_Message_Code::U_User_Data_5:
    case EMI_Message_Code::U_User_Data_6:
    case EMI_Message_Code::U_User_Data_7:
    case EMI_Message_Code::U_User_Data_8:
    case EMI_Message_Code::U_User_Data_9:
    case EMI_Message_Code::U_User_Data_A:
    case EMI_Message_Code::U_User_Data_B:
    case EMI_Message_Code::U_User_Data_C:
    case EMI_Message_Code::U_User_Data_D:
    case EMI_Message_Code::U_User_Data_E:
    case EMI_Message_Code::U_User_Data_F: {
        std::shared_ptr<EMI_U_User_Data> pdu2 = std::dynamic_pointer_cast<EMI_U_User_Data>(pdu);
        assert(pdu2);

        // @todo user_layer.U_User_Data_req(pdu2->data);
    }
    break;
    case EMI_Message_Code::PC_Set_Value_req: {
        std::shared_ptr<EMI_PC_Set_Value_req> pdu2 = std::dynamic_pointer_cast<EMI_PC_Set_Value_req>(pdu);
        assert(pdu2);

        user_layer.PC_Set_Value_req(pdu2->length, pdu2->address, pdu2->data);
    }
    break;
    case EMI_Message_Code::PC_Get_Value_req: {
        std::shared_ptr<EMI_PC_Get_Value_req> pdu2 = std::dynamic_pointer_cast<EMI_PC_Get_Value_req>(pdu);
        assert(pdu2);

        user_layer.PC_Get_Value_req(pdu2->length, pdu2->address, [this, pdu2](const std::vector<uint8_t> data, const Status u_status) -> void {
            PC_Get_Value_con(pdu2->length, pdu2->address, data, u_status);
        });
    }
    break;
    case EMI_Message_Code::PEI_Identify_req: {
        std::shared_ptr<EMI_PEI_Identify_req> pdu2 = std::dynamic_pointer_cast<EMI_PEI_Identify_req>(pdu);
        assert(pdu2);

        user_layer.PEI_Identify_req([this](const Individual_Address individual_address, const Serial_Number serial_number, const Status u_status) -> void {
            PEI_Identify_con(individual_address, serial_number, u_status);
        });
    }
    break;
    case EMI_Message_Code::PEI_Switch_req: {
        std::shared_ptr<EMI_PEI_Switch_req> pdu2 = std::dynamic_pointer_cast<EMI_PEI_Switch_req>(pdu);
        assert(pdu2);

        //user_layer.PEI_Switch_req(pdu2->system_status, pdu2->ll, pdu2->nl, pdu2->tlg, pdu2->tlc, pdu2->tll, pdu2->al, pdu2->man, pdu2->pei, pdu2->usr);
        local_PEI_Switch_req(pdu2->system_status, pdu2->ll, pdu2->nl, pdu2->tlg, pdu2->tlc, pdu2->tll, pdu2->al, pdu2->man, pdu2->pei, pdu2->usr);
    }
    break;
    default:
        // only EMI con/ind and cEMI messages should be left here
        assert(false); // other requests are not supported
        break;
    }
}

void EMI_PEI::L_Busmon_ind(const Status /*l_status*/, const uint16_t time_stamp, const std::vector<uint8_t> lpdu)
{
    EMI_L_Busmon_ind pdu;
    // @todo pdu.sequence_number = 0;
    pdu.time_stamp = time_stamp;
    pdu.lpdu = lpdu;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::LM_Reset_ind()
{
    EMI_LM_Reset_ind pdu;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::L_Data_con(const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status)
{
    EMI_L_Data_con pdu;
    pdu.address_type = address_type;
    pdu.destination_address = destination_address;
    pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.lsdu = lsdu;
    pdu.control.confirm = (l_status == Status::ok) ? Confirm::no_error : Confirm::error;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::L_Data_ind(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)
{
    EMI_L_Data_ind pdu;
    pdu.control.acknowledge = ack_request;
    pdu.address_type = address_type;
    pdu.destination_address = destination_address;
    pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
    pdu.lsdu = lsdu;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::L_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status)
{
    assert(interface_objects->group_address_table());

    EMI_L_Poll_Data_con pdu;
    pdu.destination_address = destination_address;
    pdu.poll_data = poll_data_sequence;
    pdu.control.confirm = (l_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    pdu.nr_of_slots = poll_data_sequence.size();
    emi_con_internal(pdu.toData());
}

void EMI_PEI::L_SystemBroadcast_con(const Group_Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status)
{
    EMI_L_SystemBroadcast_con pdu;
    pdu.destination_address = destination_address;
    pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.lsdu = lsdu;
    pdu.control.confirm = (l_status == Status::ok) ? Confirm::no_error : Confirm::error;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::L_SystemBroadcast_ind(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, Priority priority, const Individual_Address source_address)
{
    EMI_L_SystemBroadcast_ind pdu;
    pdu.control.acknowledge = ack_request;
    pdu.destination_address = destination_address;
    pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
    pdu.lsdu = lsdu;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::N_Data_Individual_con(const Ack_Request ack_request, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, Status n_status)
{
    assert(interface_objects->group_address_table());

    EMI_N_Data_Individual_con pdu;
    pdu.control.acknowledge = ack_request;
    pdu.destination_address = destination_address;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.nsdu = nsdu;
    pdu.control.confirm = (n_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::N_Data_Individual_ind(const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)
{
    EMI_N_Data_Individual_ind pdu;
    pdu.destination_address = destination_address;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.nsdu = nsdu;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::N_Data_Group_con(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format /*extended_frame_format*/, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status)
{
    assert(interface_objects->group_address_table());

    EMI_N_Data_Group_con pdu;
    pdu.control.acknowledge = ack_request;
    pdu.destination_address = destination_address;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.nsdu = nsdu;
    pdu.control.confirm = (n_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::N_Data_Group_ind(const Group_Address destination_address, const Extended_Frame_Format /*extended_frame_format*/, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)
{
    EMI_N_Data_Group_ind pdu;
    pdu.destination_address = destination_address;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.nsdu = nsdu;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::N_Data_Broadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status)
{
    assert(interface_objects->group_address_table());

    EMI_N_Data_Broadcast_con pdu;
    pdu.control.acknowledge = ack_request;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.nsdu = nsdu;
    pdu.control.confirm = (n_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::N_Data_Broadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu)
{
    EMI_N_Data_Broadcast_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.nsdu = nsdu;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::N_Data_SystemBroadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Status n_status)
{
    assert(interface_objects->group_address_table());

    EMI_N_Data_Broadcast_con pdu;
    pdu.control.acknowledge = ack_request;
    pdu.hop_count_type = hop_count_type;
    pdu.nsdu = nsdu;
    pdu.control.priority = priority;
    pdu.control.confirm = (n_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.control.system_broadcast = System_Broadcast::System_Broadcast;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::N_Data_SystemBroadcast_ind(const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Individual_Address source_address)
{
    EMI_N_Data_Broadcast_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.nsdu = nsdu;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.control.system_broadcast = System_Broadcast::System_Broadcast;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::N_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status)
{
    assert(interface_objects->group_address_table());

    EMI_N_Poll_Data_con pdu;
    pdu.destination_address = destination_address;
    pdu.poll_data = poll_data_sequence;
    pdu.control.confirm = (l_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    pdu.nr_of_slots = poll_data_sequence.size();
    emi_con_internal(pdu.toData());
}

void EMI_PEI::T_Data_Group_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    EMI_T_Data_Group_con pdu;
    pdu.control.acknowledge = ack_request;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.destination_address = tsap;
    pdu.tsdu = tsdu;
    pdu.control.confirm = (t_status == Status::ok) ? Confirm::no_error : Confirm::error;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::T_Data_Group_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu)
{
    EMI_T_Data_Group_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.destination_address = tsap;
    pdu.tsdu = tsdu;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::T_Data_Broadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    assert(interface_objects->group_address_table());

    EMI_T_Data_Broadcast_con pdu;
    pdu.control.acknowledge = ack_request;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.tsdu = tsdu;
    pdu.control.confirm = (t_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::T_Data_Broadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu)
{
    EMI_T_Data_Broadcast_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.tsdu = tsdu;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::T_Data_SystemBroadcast_con(const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    assert(interface_objects->group_address_table());

    EMI_T_Data_SystemBroadcast_con pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.tsdu = tsdu;
    pdu.control.confirm = (t_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::T_Data_SystemBroadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu)
{
    EMI_T_Data_SystemBroadcast_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.tsdu = tsdu;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::T_Data_Individual_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    assert(interface_objects->group_address_table());

    EMI_T_Data_Individual_con pdu;
    pdu.control.acknowledge = ack_request;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.destination_address = tsap;
    pdu.tsdu = tsdu;
    pdu.control.confirm = (t_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::T_Data_Individual_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu)
{
    assert(interface_objects->group_address_table());

    EMI_T_Data_Individual_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = tsap; // @todo src_addr = TSAP ?
    pdu.tsdu = tsdu;
    pdu.destination_address = interface_objects->group_address_table()->individual_address;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::T_Connect_con(const Individual_Address destination_address, const TSAP_Connected tsap, const Status t_status)
{
    assert(interface_objects->group_address_table());

    EMI_T_Connect_con pdu;
    pdu.source_address = destination_address;
    pdu.control.confirm = (t_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.destination_address = interface_objects->group_address_table()->individual_address;
    emi_con_internal(pdu.toData());

    current_tsap_connected = tsap;
}

void EMI_PEI::T_Connect_ind(const TSAP_Connected tsap)
{
    assert(interface_objects->group_address_table());

    EMI_T_Connect_ind pdu;
    pdu.source_address = tsap; // @todo src_addr = TSAP ?
    pdu.destination_address = interface_objects->group_address_table()->individual_address;
    emi_ind_internal(pdu.toData());

    current_tsap_connected = tsap;
}

void EMI_PEI::T_Disconnect_con(const Priority priority, const TSAP_Connected tsap, const Status t_status)
{
    assert(interface_objects->group_address_table());

    EMI_T_Disconnect_con pdu;
    pdu.control.priority = priority;
    pdu.destination_address = tsap; // @todo src_addr = TSAP ?
    pdu.control.confirm = (t_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.control = 0x00; // @note Test shows this.
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    emi_con_internal(pdu.toData());

    current_tsap_connected = 0;
}

void EMI_PEI::T_Disconnect_ind(const TSAP_Connected tsap)
{
    assert(interface_objects->group_address_table());

    EMI_T_Disconnect_ind pdu;
    pdu.destination_address = interface_objects->group_address_table()->individual_address;
    pdu.control = 0x00; // @note Test shows this.
    pdu.source_address = tsap; // @todo src_addr = TSAP ?
    emi_ind_internal(pdu.toData());

    current_tsap_connected = 0;
}

void EMI_PEI::T_Data_Connected_con(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)
{
    assert(interface_objects->group_address_table());

    EMI_T_Data_Connected_con pdu;
    pdu.destination_address = tsap;
    pdu.control.priority = priority;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    pdu.hop_count_type = interface_objects->router()->hop_count()->hop_count; // @note Test shows this.
    pdu.tsdu = tsdu;
    emi_con_internal(pdu.toData());

    current_T_Data_Connected.tsdu.reset();
}

void EMI_PEI::T_Data_Connected_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)
{
    assert(interface_objects->group_address_table());

    EMI_T_Data_Connected_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = tsap; // @todo src_addr = TSAP ?
    pdu.tsdu = tsdu;
    pdu.destination_address = interface_objects->group_address_table()->individual_address;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::T_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status)
{
    assert(interface_objects->group_address_table());

    EMI_T_Poll_Data_con pdu;
    pdu.destination_address = destination_address;
    pdu.poll_data = poll_data_sequence;
    pdu.control.confirm = (l_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    pdu.nr_of_slots = poll_data_sequence.size();
    emi_con_internal(pdu.toData());
}

void EMI_PEI::M_Connect_ind(const ASAP_Connected asap)
{
    assert(interface_objects->group_address_table());

    EMI_M_Connect_ind pdu;
    pdu.source_address = asap.address; // @todo src_addr = TSAP ?
    pdu.destination_address = interface_objects->group_address_table()->individual_address;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::M_Disconnect_ind(const ASAP_Connected asap)
{
    assert(interface_objects->group_address_table());

    EMI_M_Disconnect_ind pdu;
    pdu.source_address = asap.address; // @todo src_addr = TSAP ?
    pdu.destination_address = interface_objects->group_address_table()->individual_address;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::M_User_Data_Connected_con(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)
{
    assert(interface_objects->group_address_table());
    assert(tsdu);

    EMI_M_User_Data_Connected_con pdu;
    //    pdu.control.acknowledge = ack_request;
    //    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.destination_address = tsap;
    pdu.tsdu = tsdu;
    //    pdu.control.confirm = (t_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    pdu.length = tsdu->length_calculated();
    emi_con_internal(pdu.toData());
}

void EMI_PEI::M_User_Data_Connected_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu)
{
    assert(interface_objects->group_address_table());
    assert(tsdu);

    EMI_M_User_Data_Connected_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = tsap; // @todo src_addr = ASAP ?
    pdu.tsdu = tsdu;
    pdu.destination_address = interface_objects->group_address_table()->individual_address;
    pdu.length = tsdu->length_calculated();
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::A_Data_Group_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    assert(tsdu);

    EMI_A_Data_Group_con pdu;
    pdu.control.acknowledge = ack_request;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.destination_address = tsap;
    pdu.tsdu = tsdu;
    pdu.control.confirm = (t_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.length = tsdu->length_calculated();
    emi_con_internal(pdu.toData());
}

void EMI_PEI::A_Data_Group_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu)
{
    assert(tsdu);

    EMI_A_Data_Group_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = source_address;
    pdu.destination_address = tsap;
    pdu.tsdu = tsdu;
    pdu.length = tsdu->length_calculated();
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::M_User_Data_Individual_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu, const Status t_status)
{
    assert(interface_objects->group_address_table());
    assert(tsdu);

    EMI_M_User_Data_Individual_con pdu;
    pdu.control.acknowledge = ack_request;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.destination_address = tsap;
    pdu.tsdu = tsdu;
    pdu.control.confirm = (t_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    pdu.length = tsdu->length_calculated();
    emi_con_internal(pdu.toData());
}

void EMI_PEI::M_User_Data_Individual_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu)
{
    assert(interface_objects->group_address_table());
    assert(tsdu);

    EMI_M_User_Data_Individual_ind pdu;
    pdu.hop_count_type = hop_count_type;
    pdu.control.priority = priority;
    pdu.source_address = tsap;
    pdu.tsdu = tsdu;
    pdu.destination_address = interface_objects->group_address_table()->individual_address;
    pdu.length = tsdu->length_calculated();
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::A_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status)
{
    assert(interface_objects->group_address_table());

    EMI_A_Poll_Data_con pdu;
    pdu.destination_address = destination_address;
    pdu.poll_data = poll_data_sequence;
    pdu.control.confirm = (l_status == Status::ok) ? Confirm::no_error : Confirm::error;
    pdu.source_address = interface_objects->group_address_table()->individual_address;
    pdu.nr_of_slots = poll_data_sequence.size();
    emi_con_internal(pdu.toData());
}

void EMI_PEI::U_Value_Read_con(const Group_Object_Number group_object_number, const Group_Object_Communication_Flags ram_flags, const std::vector<uint8_t> value)
{
    EMI_U_Value_Read_con pdu;
    pdu.group_object_number = group_object_number;
    pdu.ram_flags = ram_flags;
    pdu.value = value;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::U_Flags_Read_con(const Group_Object_Number group_object_number, const Group_Object_Communication_Flags ram_flags, const Group_Object_Config eeprom_flags, const Group_Object_Type value_type)
{
    EMI_U_Flags_Read_con pdu;
    pdu.group_object_number = group_object_number;
    pdu.ram_flags = ram_flags;
    pdu.eeprom_flags = eeprom_flags;
    pdu.value_type = value_type;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::U_Event_ind(const Connection_Number cr_id)
{
    EMI_U_Event_ind pdu;
    pdu.cr_id = cr_id;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::U_User_Data_con(const std::vector<uint8_t> /*data*/)
{
    //    EMI_U_User_Data pdu(message_code);
    //    pdu.data = data;
    //    emi_con_internal(pdu.toData());
}

void EMI_PEI::U_User_Data_ind(const std::vector<uint8_t> /*data*/)
{
    //    EMI_U_User_Data pdu(message_code);
    //    pdu.data = data;
    //    emi_ind_internal(pdu.toData());
}

void EMI_PEI::M_InterfaceObj_Data_con(const Control control, const Address destination_address, const uint8_t communication_type, const Hop_Count_Type hop_count_type, const std::shared_ptr<APDU> apdu)
{
    assert(apdu);

    EMI_M_InterfaceObj_Data_con pdu;
    pdu.control = control;
    pdu.destination_address = destination_address;
    pdu.communication_type = communication_type;
    pdu.hop_count_type = hop_count_type;
    pdu.apdu = apdu;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::M_InterfaceObj_Data_ind(const Control control, const Address destination_address, const uint8_t communication_type, const Hop_Count_Type hop_count_type, const std::shared_ptr<APDU> apdu)
{
    assert(apdu);

    EMI_M_InterfaceObj_Data_ind pdu;
    pdu.control = control;
    pdu.destination_address = destination_address;
    pdu.communication_type = communication_type;
    pdu.hop_count_type = hop_count_type;
    pdu.apdu = apdu;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::PC_Get_Value_con(const uint8_t length, const uint16_t address, const std::vector<uint8_t> data, const Status u_status)
{
    EMI_PC_Get_Value_con pdu;
    pdu.length = length;
    pdu.address = address;
    pdu.data = data;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::PEI_Identify_con(const Individual_Address individual_address, const Serial_Number serial_number, const Status u_status)
{
    EMI_PEI_Identify_con pdu;
    pdu.individual_address = individual_address;
    pdu.serial_number = serial_number;
    emi_con_internal(pdu.toData());
}

void EMI_PEI::TM_Timer_ind(const uint8_t style, const uint8_t timer_parameter, const uint8_t timer_number)
{
    EMI_TM_Timer_ind pdu;
    pdu.style = style;
    pdu.timer_parameter = timer_parameter;
    pdu.timer_number = timer_number;
    emi_ind_internal(pdu.toData());
}

void EMI_PEI::local_PEI_Switch_req(const System_Status system_status, const Destination_Layer ll, const Destination_Layer nl, const Destination_Layer tlg, const Destination_Layer tlc, const Destination_Layer tll, const Destination_Layer al, const Destination_Layer man, const Destination_Layer pei, const Destination_Layer usr)
{
    assert(data_link_layer);

    /* normal routing  */
    connect(*data_link_layer); // incl. network layer
    data_link_layer->mode = Data_Link_Layer::Mode::Normal;

    /* System Status */
    if (system_status.dm) {
        // Device Management?
        // not used, shall be 0
    }
    if (system_status.ue) {
        // @todo User program enable
    }
    if (system_status.se) {
        data_link_layer->mode = Data_Link_Layer::Mode::Busmonitor;
    }
    if (system_status.ale) {
        // @todo Application Layer enable
    }
    if (system_status.tle) {
        // @todo Transport Layer enable
    }
    if (system_status.llm) {
        // @todo Link Layer enabled
    }
    if (system_status.prog) {
        // not used, shall be 0
    }

    /* Data Link Layer */
    switch (ll) {
    case Destination_Layer::LL: // 1
        // PL -> LL (default)
        break;
    case Destination_Layer::PEI:
    // PL -> PEI
    // Ph_Data.con
    // Ph_Data.ind
    default:
        assert(false); // other redirections are not supported
        break;
    }

    /* Network Layer */
    switch (nl) {
    case Destination_Layer::NL: // 2
        // LL -> NL (default)
        break;
    case Destination_Layer::PEI:
        // LL -> PEI
        data_link_layer->L_Data_con = std::bind(&EMI_PEI::L_Data_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
        data_link_layer->L_Data_ind = std::bind(&EMI_PEI::L_Data_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
        data_link_layer->L_SystemBroadcast_con = std::bind(&EMI_PEI::L_SystemBroadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
        data_link_layer->L_SystemBroadcast_ind = std::bind(&EMI_PEI::L_SystemBroadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
        data_link_layer->L_Poll_Data_con = std::bind(&EMI_PEI::L_Poll_Data_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        data_link_layer->L_Busmon_ind = std::bind(&EMI_PEI::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        // L_Service_Information_ind
        break;
    default:
        assert(false); // other redirections are not supported
        break;
    }

    /* Transport Layer (Groups) */
    switch (tlg) {
    case Destination_Layer::TLG: // 3
        // NL -> TLG (default)
        break;
    case Destination_Layer::PEI:
        // NL -> PEI
        network_layer.N_Data_Group_con = std::bind(&EMI_PEI::N_Data_Group_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
        network_layer.N_Data_Group_ind = std::bind(&EMI_PEI::N_Data_Group_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
        // @note There is no N_Poll_Data service, so use L_Poll_Data instead.
        data_link_layer->L_Poll_Data_con = std::bind(&EMI_PEI::N_Poll_Data_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        break;
    default:
        assert(false); // other redirections are not supported
        break;
    }

    /* Transport Layer (connection-oriented) */
    switch (tlc) {
    case Destination_Layer::TLC: // 4
        // NL -> TLC (default)
        break;
    case Destination_Layer::PEI:
        // NL -> PEI
        network_layer.N_Data_Individual_con = std::bind(&EMI_PEI::N_Data_Individual_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
        network_layer.N_Data_Individual_ind = std::bind(&EMI_PEI::N_Data_Individual_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
        network_layer.N_Data_Broadcast_con = std::bind(&EMI_PEI::N_Data_Broadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
        network_layer.N_Data_Broadcast_ind = std::bind(&EMI_PEI::N_Data_Broadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        network_layer.N_Data_SystemBroadcast_con = std::bind(&EMI_PEI::N_Data_SystemBroadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
        network_layer.N_Data_SystemBroadcast_ind = std::bind(&EMI_PEI::N_Data_SystemBroadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        break;
    default:
        assert(false); // other redirections are not supported
        break;
    }

    /* Transport Layer (local) */
    switch (tll) {
    case Destination_Layer::TLC:
        // MAN -> TLC
        // PEI -> TLC
        break;
    case Destination_Layer::TLL: // 5
        // MAN -> TLL (default)
        // PEI -> TLL (default)
        break;
    case Destination_Layer::PEI:
        // MAN -> PEI
        //application_layer.A_Connect_con = std::bind(&EMI_PEI::M_Connect_con, this, std::placeholders::_1, std::placeholders::_2);
        application_layer.A_Connect_ind(std::bind(&EMI_PEI::M_Connect_ind, this, std::placeholders::_1));
        //application_layer.A_Disconnect_con = std::bind(&EMI_PEI::M_Disconnect_con, this, std::placeholders::_1, std::placeholders::_2);
        application_layer.A_Disconnect_ind(std::bind(&EMI_PEI::M_Disconnect_ind, this, std::placeholders::_1));
        transport_layer.T_Data_Connected_con = std::bind(&EMI_PEI::M_User_Data_Connected_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        transport_layer.T_Data_Connected_ind = std::bind(&EMI_PEI::M_User_Data_Connected_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        transport_layer.T_Data_Individual_con = std::bind(&EMI_PEI::M_User_Data_Individual_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
        transport_layer.T_Data_Individual_ind = std::bind(&EMI_PEI::M_User_Data_Individual_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        // PEI -> PEI
        break;
    default:
        assert(false); // other redirections are not supported
        break;
    }

    /* Application Layer (Groups) */
    switch (al) {
    case Destination_Layer::AL: // 6
        // TLG -> AL(G) (default)
        break;
    case Destination_Layer::PEI:
        // TLG -> PEI
        transport_layer.T_Data_Group_con = std::bind(&EMI_PEI::T_Data_Group_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
        transport_layer.T_Data_Group_ind = std::bind(&EMI_PEI::T_Data_Group_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
        // @note There is no T_Poll_Data service, so use L_Poll_Data instead.
        data_link_layer->L_Poll_Data_con = std::bind(&EMI_PEI::T_Poll_Data_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        break;
    default:
        assert(false); // other redirections are not supported
        break;
    }

    /* Mananagement */
    switch (man) {
    case Destination_Layer::MAN: // 7
        // TLC -> MAN (default)
        break;
    case Destination_Layer::PEI:
        // TLC -> PEI
        transport_layer.T_Data_Broadcast_con = std::bind(&EMI_PEI::T_Data_Broadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
        transport_layer.T_Data_Broadcast_ind = std::bind(&EMI_PEI::T_Data_Broadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        transport_layer.T_Data_SystemBroadcast_con = std::bind(&EMI_PEI::T_Data_SystemBroadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        transport_layer.T_Data_SystemBroadcast_ind = std::bind(&EMI_PEI::T_Data_SystemBroadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        transport_layer.T_Data_Individual_con = std::bind(&EMI_PEI::T_Data_Individual_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
        transport_layer.T_Data_Individual_ind = std::bind(&EMI_PEI::T_Data_Individual_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        transport_layer.T_Connect_con = std::bind(&EMI_PEI::T_Connect_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        transport_layer.T_Connect_ind = std::bind(&EMI_PEI::T_Connect_ind, this, std::placeholders::_1);
        transport_layer.T_Disconnect_con = std::bind(&EMI_PEI::T_Disconnect_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        transport_layer.T_Disconnect_ind = std::bind(&EMI_PEI::T_Disconnect_ind, this, std::placeholders::_1);
        transport_layer.T_Data_Connected_con = std::bind(&EMI_PEI::T_Data_Connected_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        transport_layer.T_Data_Connected_ind = std::bind(&EMI_PEI::T_Data_Connected_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        break;
    default:
        assert(false); // other redirections are not supported
        break;
    }

    /* PEI */
    switch (pei) {
    case Destination_Layer::PEI: // 8
        // AIL -> PEI (default)
        // USR -> PEI (default)
        break;
    default:
        assert(false); // other redirections are not supported
        break;
    }

    /* User (Application) */
    switch (usr) {
    case Destination_Layer::NONE: // 0
        // AIL -> NONE
        // flags
        // MAN -> NONE
        break;
    case Destination_Layer::PEI:
        // AIL -> PEI
        transport_layer.T_Data_Group_con = std::bind(&EMI_PEI::A_Data_Group_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
        transport_layer.T_Data_Group_ind = std::bind(&EMI_PEI::A_Data_Group_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
        // @note There is no A_Poll_Data service, so use L_Poll_Data instead.
        data_link_layer->L_Poll_Data_con = std::bind(&EMI_PEI::A_Poll_Data_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        // flags
        // MAN -> PEI
        //application_layer.A_Connect_con = std::bind(&EMI_PEI::M_Connect_con, this, std::placeholders::_1, std::placeholders::_2);
        application_layer.A_Connect_ind(std::bind(&EMI_PEI::M_Connect_ind, this, std::placeholders::_1));
        //application_layer.A_Disconnect_con = std::bind(&EMI_PEI::M_Disconnect_con, this, std::placeholders::_1, std::placeholders::_2);
        application_layer.A_Disconnect_ind(std::bind(&EMI_PEI::M_Disconnect_ind, this, std::placeholders::_1));
        transport_layer.T_Data_Connected_con = std::bind(&EMI_PEI::M_User_Data_Connected_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
        transport_layer.T_Data_Connected_ind = std::bind(&EMI_PEI::M_User_Data_Connected_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        transport_layer.T_Data_Individual_con = std::bind(&EMI_PEI::M_User_Data_Individual_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
        transport_layer.T_Data_Individual_ind = std::bind(&EMI_PEI::M_User_Data_Individual_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
        break;
    case Destination_Layer::USR:
        // AIL -> USR (default)
        // MAN -> USR (default)
        break;
    default:
        assert(false); // other redirections are not supported
        break;
    }
}

void EMI_PEI::local_M_InterfaceObj_Data_req(const Control control, const Address destination_address, const uint8_t communication_type, const Hop_Count_Type hop_count_type, const std::shared_ptr<APDU> apdu)
{
    // 0: connection-oriented; do not use Destination Address
    // !0: connectionless; use Destination Address
    const Comm_Mode comm_mode{(communication_type == 0) ? Comm_Mode::Connected : Comm_Mode::Individual};
    const ASAP_Individual asap{Individual_Address(destination_address), (communication_type == 0)};

    switch (apdu->apci) {
    case APCI::A_SystemNetworkParameter_Read: {
        std::shared_ptr<A_SystemNetworkParameter_Read_PDU> apdu2 = std::dynamic_pointer_cast<A_SystemNetworkParameter_Read_PDU>(apdu);
        assert(apdu2);
        application_layer.A_SystemNetworkParameter_Read_req(hop_count_type, apdu2->parameter_type, control.priority, apdu2->test_info, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_SystemNetworkParameter_Response: {
        std::shared_ptr<A_SystemNetworkParameter_Response_PDU> apdu2 = std::dynamic_pointer_cast<A_SystemNetworkParameter_Response_PDU>(apdu);
        assert(apdu2);
        application_layer.A_SystemNetworkParameter_Read_res(hop_count_type, apdu2->parameter_type, control.priority, apdu2->test_info_result, {}, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_SystemNetworkParameter_Write: {
        std::shared_ptr<A_SystemNetworkParameter_Write_PDU> apdu2 = std::dynamic_pointer_cast<A_SystemNetworkParameter_Write_PDU>(apdu);
        assert(apdu2);
        application_layer.A_SystemNetworkParameter_Write_req(hop_count_type, apdu2->parameter_type, control.priority, apdu2->value, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_FunctionPropertyCommand: {
        std::shared_ptr<A_FunctionPropertyCommand_PDU> apdu2 = std::dynamic_pointer_cast<A_FunctionPropertyCommand_PDU>(apdu);
        assert(apdu2);
        application_layer.A_FunctionPropertyCommand_req(control.acknowledge, asap, comm_mode, apdu2->data, hop_count_type, apdu2->object_index, control.priority, apdu2->property_id, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_FunctionPropertyState_Read: {
        std::shared_ptr<A_FunctionPropertyState_Read_PDU> apdu2 = std::dynamic_pointer_cast<A_FunctionPropertyState_Read_PDU>(apdu);
        assert(apdu2);
        application_layer.A_FunctionPropertyState_Read_req(asap, control.acknowledge, comm_mode, apdu2->data, hop_count_type, apdu2->object_index, control.priority, apdu2->property_id, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_FunctionPropertyState_Response: {
        std::shared_ptr<A_FunctionPropertyState_Response_PDU> apdu2 = std::dynamic_pointer_cast<A_FunctionPropertyState_Response_PDU>(apdu);
        assert(apdu2);
        application_layer.A_FunctionPropertyState_Read_res(control.acknowledge, asap, comm_mode, apdu2->data, hop_count_type, apdu2->object_index, control.priority, apdu2->property_id, apdu2->return_code, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/) -> void {
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_DeviceDescriptor_Read: {
        std::shared_ptr<A_DeviceDescriptor_Read_PDU> apdu2 = std::dynamic_pointer_cast<A_DeviceDescriptor_Read_PDU>(apdu);
        assert(apdu2);
        application_layer.A_DeviceDescriptor_Read_req(control.acknowledge, control.priority, hop_count_type, asap, apdu2->descriptor_type, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_DeviceDescriptor_Response: { // = A_DeviceDescriptor_InfoReport
        std::shared_ptr<A_DeviceDescriptor_Response_PDU> apdu2 = std::dynamic_pointer_cast<A_DeviceDescriptor_Response_PDU>(apdu);
        assert(apdu2);
        application_layer.A_DeviceDescriptor_Read_res(control.acknowledge, control.priority, hop_count_type, asap, apdu2->descriptor_type, apdu2->device_descriptor, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/) -> void {
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_PropertyValue_Read: {
        std::shared_ptr<A_PropertyValue_Read_PDU> apdu2 = std::dynamic_pointer_cast<A_PropertyValue_Read_PDU>(apdu);
        assert(apdu2);
        application_layer.A_PropertyValue_Read_req(control.acknowledge, control.priority, hop_count_type, asap, apdu2->object_index, apdu2->property_id, apdu2->nr_of_elem, apdu2->start_index, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_PropertyValue_Response: {
        std::shared_ptr<A_PropertyValue_Response_PDU> apdu2 = std::dynamic_pointer_cast<A_PropertyValue_Response_PDU>(apdu);
        assert(apdu2);
        application_layer.A_PropertyValue_Read_res(control.acknowledge, control.priority, hop_count_type, asap, apdu2->object_index, apdu2->property_id, apdu2->nr_of_elem, apdu2->start_index, apdu2->data, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_PropertyValue_Write: {
        std::shared_ptr<A_PropertyValue_Write_PDU> apdu2 = std::dynamic_pointer_cast<A_PropertyValue_Write_PDU>(apdu);
        assert(apdu2);
        application_layer.A_PropertyValue_Write_req(control.acknowledge, control.priority, hop_count_type, asap, apdu2->object_index, apdu2->property_id, apdu2->nr_of_elem, apdu2->start_index, apdu2->data, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_PropertyDescription_Read: {
        std::shared_ptr<A_PropertyDescription_Read_PDU> apdu2 = std::dynamic_pointer_cast<A_PropertyDescription_Read_PDU>(apdu);
        assert(apdu2);
        application_layer.A_PropertyDescription_Read_req(control.acknowledge, control.priority, hop_count_type, asap, apdu2->object_index, apdu2->property_id, apdu2->property_index, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_PropertyDescription_Response: {
        std::shared_ptr<A_PropertyDescription_Response_PDU> apdu2 = std::dynamic_pointer_cast<A_PropertyDescription_Response_PDU>(apdu);
        assert(apdu2);
        application_layer.A_PropertyDescription_Read_res(control.acknowledge, control.priority, hop_count_type, asap, apdu2->object_index, apdu2->property_id, apdu2->property_index, apdu2->write_enable, apdu2->type, apdu2->max_nr_of_elem, apdu2->access, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/) -> void {
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_NetworkParameter_Read: {
        std::shared_ptr<A_NetworkParameter_Read_PDU> apdu2 = std::dynamic_pointer_cast<A_NetworkParameter_Read_PDU>(apdu);
        assert(apdu2);
        application_layer.A_NetworkParameter_Read_req(asap, comm_mode, hop_count_type, apdu2->parameter_type, control.priority, apdu2->test_info, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_NetworkParameter_Response: { // = A_NetworkParameter_InfoReport
        std::shared_ptr<A_NetworkParameter_Response_PDU> apdu2 = std::dynamic_pointer_cast<A_NetworkParameter_Response_PDU>(apdu);
        assert(apdu2);
        application_layer.A_NetworkParameter_Read_res(asap, comm_mode, hop_count_type, apdu2->parameter_type, control.priority, apdu2->test_info_result, {}, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    case APCI::A_NetworkParameter_Write: {
        std::shared_ptr<A_NetworkParameter_Write_PDU> apdu2 = std::dynamic_pointer_cast<A_NetworkParameter_Write_PDU>(apdu);
        assert(apdu2);
        application_layer.A_NetworkParameter_Write_req(asap, comm_mode, hop_count_type, apdu2->parameter_type, control.priority, apdu2->value, [this, control, asap, communication_type, hop_count_type, apdu](const Status /*a_status*/){
            M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);
        });
    }
    break;
    default:
        assert(false);
        break;
    }
}

void EMI_PEI::A_PropertyValue_Read_ind_initiator()
{
    application_layer.A_PropertyValue_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index) -> void
    {
        Control control;
        control.priority = priority;
        const uint8_t communication_type = asap.connected ? 0 : 1;
        std::shared_ptr<A_PropertyValue_Read_PDU> apdu = std::make_shared<A_PropertyValue_Read_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->nr_of_elem = nr_of_elem;
        apdu->start_index = start_index;
        M_InterfaceObj_Data_ind(control, asap.address, communication_type, hop_count_type, apdu);

        A_PropertyValue_Read_ind_initiator();
    });
}

void EMI_PEI::A_PropertyValue_Read_Acon_initiator()
{
application_layer.A_PropertyValue_Read_Acon([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) -> void
    {
        Control control;
        control.priority = priority;
        const uint8_t communication_type = asap.connected ? 0 : 1;
        std::shared_ptr<A_PropertyValue_Response_PDU> apdu = std::make_shared<A_PropertyValue_Response_PDU>();
        apdu->object_index = object_index;
        apdu->property_id = property_id;
        apdu->nr_of_elem = nr_of_elem;
        apdu->start_index = start_index;
        apdu->data = data;
        M_InterfaceObj_Data_con(control, asap.address, communication_type, hop_count_type, apdu);

        A_PropertyValue_Read_Acon_initiator();
    });
}

//void EMI_PEI::local_A_PropertyValue_Write_Lcon(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data, const Status a_status) {
//    // @todo A_PropertyValue_Write_Lcon
//}

//void EMI_PEI::local_A_PropertyValue_Write_ind(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value data) {
//    // @todo A_PropertyValue_Write_ind
//}

//void EMI_PEI::local_A_PropertyDescription_Read_Lcon(const Ack_Request ack_request, const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const Status a_status) {
//    // @todo A_PropertyDescription_Read_Lcon
//}

//void EMI_PEI::local_A_PropertyDescription_Read_ind(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index) {
//    // @todo A_PropertyDescription_Read_ind
//}

//void EMI_PEI::local_A_PropertyDescription_Read_Acon(const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Object_Index object_index, const Property_Id property_id, const Property_Index property_index, const bool write_enable, const Property_Type type, const Max_Nr_Of_Elem max_nr_of_elem, const Access access) {
//    // @todo A_PropertyDescription_Read_Acon
//}

//void EMI_PEI::local_A_FunctionPropertyCommand_Lcon(const Ack_Request ack_request, const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const Status a_status) {
//    // @todo A_FunctionPropertyCommand_Lcon
//}

//void EMI_PEI::local_A_FunctionPropertyCommand_ind(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id) {
//    // @todo A_FunctionPropertyCommand_ind
//}

//void EMI_PEI::local_A_FunctionPropertyCommand_Acon(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const Property_Return_Code return_code) {
//    // @todo A_FunctionPropertyCommand_Acon
//}

//void EMI_PEI::local_A_FunctionPropertyState_Read_Lcon(const Ack_Request ack_request, const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const Status a_status) {
//    // @todo A_FunctionPropertyState_Read_Lcon
//}

//void EMI_PEI::local_A_FunctionPropertyState_Read_ind(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id) {
//    // @todo A_FunctionPropertyState_Read_ind
//}

//void EMI_PEI::local_A_FunctionPropertyState_Read_Acon(const ASAP_Individual asap, const Comm_Mode comm_mode, const Property_Value data, const Hop_Count_Type hop_count_type, const Object_Index object_index, const Priority priority, const Property_Id property_id, const Property_Return_Code return_code) {
//    // @todo A_FunctionPropertyState_Read_Acon
//}

void EMI_PEI::emi_con_internal(std::vector<uint8_t> data)
{
    assert(data.size() >= 1);

    if (emi_con) {
        switch (emi_version) {
        case EMI_Version::EMI:
            break;
        case EMI_Version::EMI1:
            data[0] = static_cast<uint8_t>(to_EMI1_Message_Code(static_cast<EMI_Message_Code>(data[0])));
            break;
        case EMI_Version::EMI2:
            data[0] = static_cast<uint8_t>(to_EMI2_Message_Code(static_cast<EMI_Message_Code>(data[0])));
            break;
        }
        emi_con(data);
    }
}

void EMI_PEI::emi_ind_internal(std::vector<uint8_t> data)
{
    assert(data.size() >= 1);

    if (emi_ind) {
        switch (emi_version) {
        case EMI_Version::EMI:
            break;
        case EMI_Version::EMI1:
            data[0] = static_cast<uint8_t>(to_EMI1_Message_Code(static_cast<EMI_Message_Code>(data[0])));
            break;
        case EMI_Version::EMI2:
            data[0] = static_cast<uint8_t>(to_EMI2_Message_Code(static_cast<EMI_Message_Code>(data[0])));
            break;
        }
        emi_ind(data);
    }
}

}
