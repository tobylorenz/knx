// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>

#include <KNX/03/03/02/Data_Link_Layer.h>
#include <KNX/03/03/03/Network_Layer.h>
#include <KNX/03/03/04/Transport_Layer.h>
#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/04/01/Application_Interface_Layer.h>
#include <KNX/03/05/01/Interface_Objects.h>
#include <KNX/03/06/03/EMI/Connection_Number.h>
#include <KNX/03/06/03/EMI/Destination_Layer.h>
#include <KNX/03/06/03/EMI/EEPROM_Flags.h>
#include <KNX/03/06/03/EMI/EMI_User_Layer.h>
#include <KNX/03/06/03/EMI/RAM_Flags.h>
#include <KNX/03/06/03/EMI/System_Status.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Write/EMI_U_Value_Write_req.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * EMI PEI stack
 */
class KNX_EXPORT EMI_PEI
{
public:
    explicit EMI_PEI(asio::io_context & io_context);
    virtual ~EMI_PEI() = default;

    /** connect Network Layer to Data Link Layer */
    void connect(Data_Link_Layer & data_link_layer);

    /* command interface */
    virtual void emi_req(std::vector<uint8_t> data);
    std::function<void(const std::vector<uint8_t> data)> emi_con;
    std::function<void(const std::vector<uint8_t> data)> emi_ind;
    enum EMI_Version {
        /** internal EMI code */
        EMI,

        /** EMI1 */
        EMI1,

        /** EMI2/IMI2 */
        EMI2
    };
    EMI_Version emi_version{EMI_Version::EMI2};

    /** IO context */
    asio::io_context & io_context;

    /** resources */
    std::shared_ptr<Interface_Objects> interface_objects{};

    /** memory */
    std::map<Memory_Address, Memory_Data> memory{};

    /** current level */
    std::shared_ptr<Level> current_level{};

    /* communication layer */

    /** data link layer */
    Data_Link_Layer * data_link_layer{};

    /** network layer */
    Network_Layer network_layer;

    /** transport layer */
    Transport_Layer transport_layer;

    /** application layer */
    Application_Layer application_layer;

    /** application interface layer */
    Application_Interface_Layer application_interface_layer;

    /* management layer = application Layer, especially because T_Data_* message need to go there */

    /** user (application) layer */
    EMI_User_Layer user_layer;

    /* Other default EMI services */
    virtual void PC_Get_Value_con(const uint8_t length, const uint16_t address, const std::vector<uint8_t> data, const Status u_status);
    virtual void PEI_Identify_con(const Individual_Address individual_address, const Serial_Number serial_number, const Status u_status);
    virtual void TM_Timer_ind(const uint8_t style, const uint8_t timer_parameter, const uint8_t timer_number);

protected:
    /* redirect A_PropertyValue_Read_* to M_InterfaceObj_Data_* */
    virtual void A_PropertyValue_Read_ind_initiator();
    virtual void A_PropertyValue_Read_Acon_initiator();

    /* 3.3.3 Busmonitor EMI */
    virtual void L_Busmon_ind(const Status l_status, const uint16_t time_stamp, const std::vector<uint8_t> lpdu);
    virtual void LM_Reset_ind();

    /* 3.3.4 Data Link Layer EMI */
    virtual void L_Data_con(const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status);
    virtual void L_Data_ind(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address);
    virtual void L_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status);
    virtual void L_SystemBroadcast_con(const Group_Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status);
    virtual void L_SystemBroadcast_ind(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address);

    /* 3.3.5 Network Layer EMI */
    /* N_Data_Individual service */
    // N_Data_Individual_req implemented in network_layer
    virtual void N_Data_Individual_con(const Ack_Request ack_request, const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, Status n_status);
    virtual void N_Data_Individual_ind(const Individual_Address destination_address, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu);
    /* N_Data_Group service */
    // N_Data_Group_req implemented in network_layer
    virtual void N_Data_Group_con(const Ack_Request ack_request, const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status);
    virtual void N_Data_Group_ind(const Group_Address destination_address, const Extended_Frame_Format extended_frame_format, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu);
    /* N_Data_Broadcast service */
    // N_Data_Broadcast_req implemented in network_layer
    virtual void N_Data_Broadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<NSDU> nsdu, const Status n_status);
    virtual void N_Data_Broadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<NSDU> nsdu);
    /* N_Data_SystemBroadcast service */
    // N_Data_SystemBroadcast_req implemented in network_layer
    virtual void N_Data_SystemBroadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Status n_status);
    virtual void N_Data_SystemBroadcast_ind(const Hop_Count_Type hop_count_type, std::shared_ptr<NSDU> nsdu, const Priority priority, const Individual_Address source_address);
    /* N_Poll_Data service */
    // N_Poll_Data_req implemented in network_layer
    virtual void N_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status);

    /* 3.3.6 Transport Layer EMI */
    /* T_Data_Group service */
    // T_Data_Group_req implemented in transport_layer
    virtual void T_Data_Group_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void T_Data_Group_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu);
    /* T_Data_Broadcast service */
    // T_Data_Broadcast_req implemented in transport_layer
    virtual void T_Data_Broadcast_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void T_Data_Broadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu);
    /* T_Data_SystemBroadcast service */
    // T_Data_SystemBroadcast_req implemented in transport_layer
    virtual void T_Data_SystemBroadcast_con(const Hop_Count_Type hop_count_type, const Priority priority, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void T_Data_SystemBroadcast_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, std::shared_ptr<TSDU> tsdu);
    /* T_Data_Individual service */
    // T_Data_Individual_req implemented in transport_layer
    virtual void T_Data_Individual_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void T_Data_Individual_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu);
    /* T_Connect service */
    // T_Connect_req implemented in transport_layer
    virtual void T_Connect_con(const Individual_Address destination_address, const TSAP_Connected tsap, const Status t_status);
    virtual void T_Connect_ind(const TSAP_Connected tsap);
    /* T_Disconnect service */
    // T_Disconnect_req implemented in transport_layer
    virtual void T_Disconnect_con(const Priority priority, const TSAP_Connected tsap, const Status t_status);
    virtual void T_Disconnect_ind(const TSAP_Connected tsap);
    /* T_Data_Connected service */
    // T_Data_Connected_req implemented in transport_layer
    TSAP_Connected current_tsap_connected{}; // stores TSAP between T_Connect_req and T_Data_Connected_req
    virtual void T_Data_Connected_con(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu);
    virtual void T_Data_Connected_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu);
    // stores TSDU between T_Data_Connected_req and T_Data_Connected_con
    struct {
        Priority priority{};
        std::shared_ptr<TSDU> tsdu{};
    } current_T_Data_Connected;
    /* T_Poll_Data service */
    // T_Poll_Data_req implemented in transport_layer
    virtual void T_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status);

    /* 3.3.7 Application Layer EMI */
    virtual void M_Connect_ind(const ASAP_Connected asap);
    virtual void M_Disconnect_ind(const ASAP_Connected asap);
    virtual void M_User_Data_Connected_con(const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu);
    virtual void M_User_Data_Connected_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Connected tsap, std::shared_ptr<TSDU> tsdu);
    virtual void A_Data_Group_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void A_Data_Group_ind(const Hop_Count_Type hop_count_type, const Priority priority, const Individual_Address source_address, const TSAP_Group tsap, std::shared_ptr<TSDU> tsdu);
    virtual void M_User_Data_Individual_con(const Ack_Request ack_request, const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu, const Status t_status);
    virtual void M_User_Data_Individual_ind(const Hop_Count_Type hop_count_type, const Priority priority, const TSAP_Individual tsap, std::shared_ptr<TSDU> tsdu);
    virtual void A_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status);

    /* 3.3.8 User Layer (i.e. Default) EMI */
    virtual void U_Value_Read_con(const Group_Object_Number group_object_number, const Group_Object_Communication_Flags ram_flags, const std::vector<uint8_t> value);
    virtual void U_Flags_Read_con(const Group_Object_Number group_object_number, const Group_Object_Communication_Flags ram_flags, const Group_Object_Config eeprom_flags, const Group_Object_Type value_type);
    virtual void U_Event_ind(const Connection_Number cr_id);
    virtual void U_User_Data_con(const std::vector<uint8_t> data);
    virtual void U_User_Data_ind(const std::vector<uint8_t> data);
    virtual void M_InterfaceObj_Data_con(const Control control, const Address destination_address, const uint8_t communication_type, const Hop_Count_Type hop_count_type, const std::shared_ptr<APDU> apdu);
    virtual void M_InterfaceObj_Data_ind(const Control control, const Address destination_address, const uint8_t communication_type, const Hop_Count_Type hop_count_type, const std::shared_ptr<APDU> apdu);

    /* default implementations */
    virtual void local_PEI_Switch_req(const System_Status system_status, const Destination_Layer ll, const Destination_Layer nl, const Destination_Layer tlg, const Destination_Layer tlc, const Destination_Layer tll, const Destination_Layer al, const Destination_Layer man, const Destination_Layer pei, const Destination_Layer usr);
    virtual void local_M_InterfaceObj_Data_req(const Control control, const Address destination_address, const uint8_t communication_type, const Hop_Count_Type hop_count_type, const std::shared_ptr<APDU> apdu);

private:
    void emi_con_internal(std::vector<uint8_t> data);
    void emi_ind_internal(std::vector<uint8_t> data);
};

}
