// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/EMI_User_Layer.h>

namespace KNX {

EMI_User_Layer::EMI_User_Layer(Application_Interface_Layer & application_interface_layer, Application_Layer & application_layer) :
    application_interface_layer(application_interface_layer),
    application_layer(application_layer)
{
    // application_interface_layer.flags

    //M_Connect_ind_initiator();
    //M_Disconnect_ind_initiator();
    //M_User_Data_Connected_con_initiator();
    //M_User_Data_Connected_ind_initiator();
    //M_User_Data_Individual_con_initiator();
    //M_User_Data_Individual_ind_initiator();
}

EMI_User_Layer::~EMI_User_Layer()
{
}

void EMI_User_Layer::U_User_Data_req(const std::vector<uint8_t> /*data*/, std::function<void(const Status u_status)> con)
{
    con(Status::not_ok);
}

void EMI_User_Layer::U_User_Data_ind(std::function<void(const std::vector<uint8_t> data)> ind)
{
}

void EMI_User_Layer::PC_Set_Value_req(const uint8_t length, const uint16_t address, const std::vector<uint8_t> data)
{
    assert(memory);

    assert(length == data.size());

    for (auto & mem : *memory) {
        if ((mem.first <= address) && (mem.first + mem.second.size() >= address + length)) {
            std::copy(std::cbegin(data), std::cend(data), std::begin(mem.second) + (address - mem.first));
            return;
        }
    }
}

void EMI_User_Layer::PC_Get_Value_req(const uint8_t length, const uint16_t address, std::function<void(const std::vector<uint8_t> data, const Status u_status)> con)
{
    assert(memory);

    for (auto & mem : *memory) {
        if ((mem.first <= address) && (mem.first + mem.second.size() >= address + length)) {
            std::vector<uint8_t> data;
            data.assign(std::cbegin(mem.second) + (address - mem.first), std::cbegin(mem.second) + (address - mem.first) + length);
            con(data, Status::ok);
            return;
        }
    }
}

void EMI_User_Layer::PEI_Identify_req(std::function<void(const Individual_Address individual_address, const Serial_Number serial_number, const Status u_status)> con)
{
    assert(device_object);
    assert(group_address_table);

    con(group_address_table->individual_address, device_object->serial_number()->serial_number, Status::ok);
}

void EMI_User_Layer::PEI_Switch_req(const System_Status /*system_status*/, const Destination_Layer /*ll*/, const Destination_Layer /*nl*/, const Destination_Layer /*tlg*/, const Destination_Layer /*tlc*/, const Destination_Layer /*tll*/, const Destination_Layer /*al*/, const Destination_Layer /*man*/, const Destination_Layer /*pei*/, const Destination_Layer /*usr*/)
{

}

void EMI_User_Layer::M_Connect_ind_initiator()
{
    application_layer.A_Connect_ind([this](const ASAP_Connected /*asap*/) -> void {

        M_Connect_ind_initiator();
    });
}

void EMI_User_Layer::M_Disconnect_ind_initiator()
{
    application_layer.A_Disconnect_ind([this](const ASAP_Connected /*asap*/) -> void {

        M_Disconnect_ind_initiator();
    });
}

void EMI_User_Layer::M_User_Data_Connected_con_initiator()
{
    // application_layer.A_User_Data_Connected_con([this](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status /*t_status*/) -> void {

    //     M_User_Data_Connected_con_initiator();
    // });
}

void EMI_User_Layer::M_User_Data_Connected_ind_initiator()
{
    // application_layer.A_User_Data_Connected_ind([this](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Connected /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {

    //     M_User_Data_Connected_ind_initiator();
    // });
}

void EMI_User_Layer::M_User_Data_Individual_con_initiator()
{
    // application_layer.A_User_Data_Individual_con([this](const Ack_Request /*ack_request*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/, const Status /*t_status*/) -> void {

    //     M_User_Data_Individual_con_initiator();
    // });
}


void EMI_User_Layer::M_User_Data_Individual_ind_initiator()
{
    // application_layer.A_User_Data_Individual_ind([this](const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const TSAP_Individual /*tsap*/, std::shared_ptr<TSDU> /*tsdu*/) -> void {

    //     M_User_Data_Individual_ind_initiator();
    // });
}

}
