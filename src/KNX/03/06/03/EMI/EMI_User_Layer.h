// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/04/01/Application_Interface_Layer.h>
#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/05/01/01_Group_Address_Table/Group_Address_Table.h>
#include <KNX/03/06/03/EMI/Destination_Layer.h>
#include <KNX/03/06/03/EMI/System_Status.h>

namespace KNX {

/** EMI User Layer */
class EMI_User_Layer
{
public:
    explicit EMI_User_Layer(Application_Interface_Layer & application_interface_layer, Application_Layer & application_layer);
    virtual ~EMI_User_Layer();

    virtual void U_User_Data_req(const std::vector<uint8_t> data, std::function<void(const Status u_status)> con);
    virtual void U_User_Data_ind(std::function<void(const std::vector<uint8_t> data)> ind);

    virtual void PC_Set_Value_req(const uint8_t length, const uint16_t address, const std::vector<uint8_t> data);
    virtual void PC_Get_Value_req(const uint8_t length, const uint16_t address, std::function<void(const std::vector<uint8_t> data, const Status u_status)> con);

    virtual void PEI_Identify_req(std::function<void(const Individual_Address individual_address, const Serial_Number serial_number, const Status u_status)> con);

    virtual void PEI_Switch_req(const System_Status system_status, const Destination_Layer ll, const Destination_Layer nl, const Destination_Layer tlg, const Destination_Layer tlc, const Destination_Layer tll, const Destination_Layer al, const Destination_Layer man, const Destination_Layer pei, const Destination_Layer usr);

    /** device object */
    std::shared_ptr<Device_Object> device_object;

    /** group address table */
    std::shared_ptr<Group_Address_Table> group_address_table;

    /** memory */
    std::map<Memory_Address, Memory_Data> * memory{};

protected:
    Application_Interface_Layer & application_interface_layer;
    Application_Layer & application_layer;

    void M_Connect_ind_initiator();
    void M_Disconnect_ind_initiator();

    void M_User_Data_Connected_con_initiator();
    void M_User_Data_Connected_ind_initiator();

    void M_User_Data_Individual_con_initiator();
    void M_User_Data_Individual_ind_initiator();
};

}
