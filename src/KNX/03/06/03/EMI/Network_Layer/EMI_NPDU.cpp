// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Network_Layer/EMI_NPDU.h>

#include <cassert>

namespace KNX {

EMI_NPDU::EMI_NPDU(const EMI_Message_Code message_code) :
    EMI_Message(message_code)
{
}

void EMI_NPDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    /* octet 1 */
    EMI_Message::fromData(first, first + 1);
    ++first;

    /* octet 2 */
    control = *first++;

    /* octet 3..4 */
    source_address.fromData(first, first + 2);
    first += 2;

    /* octet 5..6 */
    destination_address.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> EMI_NPDU::toData() const
{
    /* octet 1 */
    std::vector<uint8_t> data = EMI_Message::toData();

    /* octet 2 */
    data.push_back(control);

    /* octet 3..4 */
    data.push_back(source_address >> 8);
    data.push_back(source_address & 0xff);

    /* octet 5..6 */
    data.push_back(destination_address >> 8);
    data.push_back(destination_address & 0xff);

    return data;
}

}
