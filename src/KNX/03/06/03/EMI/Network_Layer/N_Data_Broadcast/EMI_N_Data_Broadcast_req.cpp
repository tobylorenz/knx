// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Broadcast/EMI_N_Data_Broadcast_req.h>

namespace KNX {

EMI_N_Data_Broadcast_req::EMI_N_Data_Broadcast_req() :
    EMI_N_Data_Broadcast(EMI_Message_Code::N_Data_Broadcast_req)
{
}

}
