// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/03/02/Address_Type.h>
#include <KNX/03/03/03/Hop_Count_Type.h>
#include <KNX/03/03/03/NSDU.h>
#include <KNX/03/06/03/EMI/Network_Layer/EMI_NPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * N_Data_Group message
 */
class KNX_EXPORT EMI_N_Data_Group :
    public EMI_NPDU
{
public:
    explicit EMI_N_Data_Group(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Address Type (AT) */
    Address_Type address_type{Address_Type::Group};

    /** Hop Count Type */
    Hop_Count_Type hop_count_type{Network_Layer_Parameter};

    /** octet count (LG) */
    uint8_t length{};

    virtual uint8_t length_calculated() const;

    /** TPDU */
    std::shared_ptr<NSDU> nsdu{};
};

}
