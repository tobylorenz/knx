// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual.h>

#include <cassert>

#include <KNX/03/03/03/Hop_Count.h>
#include <KNX/03/03/04/TPDU.h>

namespace KNX {

EMI_N_Data_Individual::EMI_N_Data_Individual(const EMI_Message_Code message_code) :
    EMI_NPDU(message_code)
{
}

void EMI_N_Data_Individual::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    /* octet 1..6 */
    EMI_NPDU::fromData(first, first + 6);
    first += 6;

    /* octet 7 */
    const uint8_t at_npci_lg = *first++;
    address_type = static_cast<Address_Type>((at_npci_lg >> 7) & 0x01);
    hop_count_type = (at_npci_lg >> 4) & 0x07;
    length = at_npci_lg & 0x0f;

    /* octet 8..N */
    nsdu = make_TPDU_Individual(first, last);
}

std::vector<uint8_t> EMI_N_Data_Individual::toData() const
{
    assert(nsdu);

    /* octet 1..6 */
    std::vector<uint8_t> data = EMI_NPDU::toData();

    /* octet 7 */
    const Hop_Count hop_count = hop_count_type;
    data.push_back(
        (static_cast<uint8_t>(address_type) << 7) |
        ((hop_count & 0x07) << 4) |
        length_calculated());

    /* octet 8..N */
    const std::vector<uint8_t> nsdu_data = nsdu->toData();
    data.insert(std::cend(data), std::cbegin(nsdu_data), std::cend(nsdu_data));

    return data;
}

uint8_t EMI_N_Data_Individual::length_calculated() const
{
    return nsdu ? nsdu->length_calculated() : 0;
}

}
