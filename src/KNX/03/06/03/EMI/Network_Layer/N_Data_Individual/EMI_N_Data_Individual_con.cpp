// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual_con.h>

namespace KNX {

EMI_N_Data_Individual_con::EMI_N_Data_Individual_con() :
    EMI_N_Data_Individual(EMI_Message_Code::N_Data_Individual_con)
{
}

}
