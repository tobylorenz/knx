// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Network_Layer/N_Data_Individual/EMI_N_Data_Individual.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * N_Data_Individual.req message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_05_02
 */
class KNX_EXPORT EMI_N_Data_Individual_req :
    public EMI_N_Data_Individual
{
public:
    EMI_N_Data_Individual_req();
};

}
