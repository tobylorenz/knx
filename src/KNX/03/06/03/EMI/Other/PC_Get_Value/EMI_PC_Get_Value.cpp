// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value.h>

#include <cassert>

namespace KNX {

EMI_PC_Get_Value::EMI_PC_Get_Value(const EMI_Message_Code message_code) :
    EMI_Message(message_code)
{
}

void EMI_PC_Get_Value::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    /* octet 1 */
    EMI_Message::fromData(first, first + 1);
    ++first;

    /* octet 2 */
    length = *first++;

    /* octet 3..4 */
    address = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> EMI_PC_Get_Value::toData() const
{
    std::vector<uint8_t> data = EMI_Message::toData();

    /* octet 2 */
    data.push_back(length);

    /* octet 3..4 */
    data.push_back(address >> 8);
    data.push_back(address & 0xff);

    return data;
}

}
