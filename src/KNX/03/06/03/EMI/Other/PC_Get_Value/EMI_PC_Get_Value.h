// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PC_Get_Value message
 */
class KNX_EXPORT EMI_PC_Get_Value :
    public EMI_Message
{
public:
    explicit EMI_PC_Get_Value(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Length */
    uint8_t length{};

    /** Address */
    uint16_t address{};
};

}
