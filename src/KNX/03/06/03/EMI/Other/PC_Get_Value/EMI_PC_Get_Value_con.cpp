// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value_con.h>

#include <cassert>

namespace KNX {

EMI_PC_Get_Value_con::EMI_PC_Get_Value_con() :
    EMI_PC_Get_Value(EMI_Message_Code::PC_Get_Value_con)
{
}

void EMI_PC_Get_Value_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 4);
    assert(std::distance(first, last) <= 19);

    /* octet 1..4 */
    EMI_PC_Get_Value::fromData(first, first + 4);
    first += 4;

    /* octet 5..N */
    data.assign(first, last);
}

std::vector<uint8_t> EMI_PC_Get_Value_con::toData() const
{
    std::vector<uint8_t> data = EMI_PC_Get_Value::toData();

    /* octet 5..N */
    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

}
