// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PC_Get_Value.con message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_09_04
 */
class KNX_EXPORT EMI_PC_Get_Value_con :
    public EMI_PC_Get_Value
{
public:
    EMI_PC_Get_Value_con();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Data */
    std::vector<uint8_t> data{};
};

}
