// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Other/PC_Get_Value/EMI_PC_Get_Value_req.h>

namespace KNX {

EMI_PC_Get_Value_req::EMI_PC_Get_Value_req() :
    EMI_PC_Get_Value(EMI_Message_Code::PC_Get_Value_req)
{
}

}
