// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Other/PC_Set_Value/EMI_PC_Set_Value_req.h>

#include <cassert>

namespace KNX {

EMI_PC_Set_Value_req::EMI_PC_Set_Value_req() :
    EMI_Message(EMI_Message_Code::PC_Set_Value_req)
{
}

void EMI_PC_Set_Value_req::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 4);
    assert(std::distance(first, last) <= 19);

    /* octet 1 */
    EMI_Message::fromData(first, first + 1);
    ++first;

    /* octet 2 */
    length = *first++;

    /* octet 3..4 */
    address = (*first++ << 8) | *first++;

    /* octet 5..N */
    data.assign(first, last);
}

std::vector<uint8_t> EMI_PC_Set_Value_req::toData() const
{
    std::vector<uint8_t> data = EMI_Message::toData();

    /* octet 2 */
    data.push_back(length);

    /* octet 3..4 */
    data.push_back(address >> 8);
    data.push_back(address & 0xff);

    /* octet 5..N */
    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

}
