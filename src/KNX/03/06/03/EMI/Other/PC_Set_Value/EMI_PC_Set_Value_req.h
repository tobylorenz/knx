// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PC_Set_Value.req message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_09_02
 */
class KNX_EXPORT EMI_PC_Set_Value_req :
    public EMI_Message
{
public:
    EMI_PC_Set_Value_req();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Length */
    uint8_t length{};

    /** Address */
    uint16_t address{};

    /** Data */
    std::vector<uint8_t> data{};
};

}
