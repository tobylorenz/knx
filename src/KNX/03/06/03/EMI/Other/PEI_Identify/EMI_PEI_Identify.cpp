// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify.h>

namespace KNX {

EMI_PEI_Identify::EMI_PEI_Identify(const EMI_Message_Code message_code) :
    EMI_Message(message_code)
{
}

}
