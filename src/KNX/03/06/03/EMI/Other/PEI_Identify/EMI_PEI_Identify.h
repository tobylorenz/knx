// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PEI_Identify message
 */
class KNX_EXPORT EMI_PEI_Identify :
    public EMI_Message
{
public:
    explicit EMI_PEI_Identify(const EMI_Message_Code message_code);
};

}
