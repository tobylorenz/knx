// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify_con.h>

#include <cassert>

namespace KNX {

EMI_PEI_Identify_con::EMI_PEI_Identify_con() :
    EMI_PEI_Identify(EMI_Message_Code::PEI_Identify_con)
{
}

void EMI_PEI_Identify_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 9);

    /* octet 1 */
    EMI_PEI_Identify::fromData(first, first + 1);
    ++first;

    /* octet 2..3 */
    individual_address = (*first++ << 8) | *first++;

    /* octet 4..9 */
    std::copy(first, first + 6, std::begin(serial_number.serial_number));
    first += 6;

    assert(first == last);
}

std::vector<uint8_t> EMI_PEI_Identify_con::toData() const
{
    std::vector<uint8_t> data = EMI_PEI_Identify::toData();

    /* octet 2..3 */
    data.push_back(individual_address >> 8);
    data.push_back(individual_address & 0xff);

    /* octet 4..9 */
    data.insert(std::cend(data), std::cbegin(serial_number.serial_number), std::cend(serial_number.serial_number));

    return data;
}

}
