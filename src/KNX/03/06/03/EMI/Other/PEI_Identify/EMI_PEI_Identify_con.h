// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/07/Serial_Number.h>
#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PEI_Identify.con message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_09_06
 */
class KNX_EXPORT EMI_PEI_Identify_con :
    public EMI_PEI_Identify
{
public:
    EMI_PEI_Identify_con();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Individual Address */
    Individual_Address individual_address{};

    /** Serial Number */
    Serial_Number serial_number{};
};

}
