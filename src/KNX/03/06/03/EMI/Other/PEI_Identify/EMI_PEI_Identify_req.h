// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Other/PEI_Identify/EMI_PEI_Identify.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PEI_Identify.req message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_09_05
 */
class KNX_EXPORT EMI_PEI_Identify_req :
    public EMI_PEI_Identify
{
public:
    EMI_PEI_Identify_req();
};

}
