# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_PEI_Switch_req.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_PEI_Switch_req.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/EMI_PEI_Switch_req.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/06/03/EMI/Other/PEI_Switch)
