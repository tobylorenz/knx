// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Other/PEI_Switch/EMI_PEI_Switch_req.h>

#include <cassert>

namespace KNX {

EMI_PEI_Switch_req::EMI_PEI_Switch_req() :
    EMI_Message(EMI_Message_Code::PEI_Switch_req)
{
}

void EMI_PEI_Switch_req::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 7);

    /* octet 1 */
    EMI_Message::fromData(first, first + 1);
    ++first;

    /* octet 2 */
    system_status = *first++;

    /* octet 3 */
    ll = static_cast<Destination_Layer>(*first >> 4);
    nl = static_cast<Destination_Layer>(*first++ & 0x0f);

    /* octet 4 */
    tlg = static_cast<Destination_Layer>(*first >> 4);
    tlc = static_cast<Destination_Layer>(*first++ & 0x0f);

    /* octet 5 */
    tll = static_cast<Destination_Layer>(*first >> 4);
    al = static_cast<Destination_Layer>(*first++ & 0x0f);

    /* octet 6 */
    man = static_cast<Destination_Layer>(*first >> 4);
    pei = static_cast<Destination_Layer>(*first++ & 0x0f);

    /* octet 7 */
    usr = static_cast<Destination_Layer>(*first >> 4);
    res = static_cast<Destination_Layer>(*first++ & 0x0f);

    assert(first == last);
}

std::vector<uint8_t> EMI_PEI_Switch_req::toData() const
{
    /* octet 1 */
    std::vector<uint8_t> data = EMI_Message::toData();

    /* octet 2 */
    data.push_back(system_status);

    /* octet 3 */
    data.push_back((ll << 4) | nl);

    /* octet 4 */
    data.push_back((tlg << 4) | tlc);

    /* octet 5 */
    data.push_back((tll << 4) | al);

    /* octet 6 */
    data.push_back((man << 4) | pei);

    /* octet 7 */
    data.push_back((usr << 4) | res);

    return data;
}

}
