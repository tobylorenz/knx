// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Destination_Layer.h>
#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/03/06/03/EMI/System_Status.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * PEI_Switch.req message (EMI2)
 *
 * Common values for System Status and Destination Layers:
 * -   ss, ll, nl, tlg, tlc, tll, al, man, pei, usr, res
 * - 0x00,  1,  2,   3,   4,   5,  6,   7,   8,   9,   A: Normal Mode
 * - 0x00,  1,  2,   3,   4,   5,  6,   7,   8,   8,   A: Application Layer
 * - 0x00,  1,  2,   3,   4,   4,  8,   8,   8,   0,   A: Transport Layer Remote
 * - 0x00,  1,  8,   3,   4,   5,  6,   7,   8,   0,   A: Data Link Layer
 * - 0x90,  1,  8,   3,   4,   5,  6,   7,   8,   0,   A: Data Link Layer (Busmonitor mode)
 * Although not mentioned in the spec, the following should also be possible:
 * - 0x00,  1,  2,   8,   8,   5,  6,   7,   8,   0,   A: Network Layer
 * - Management is same Application Layer
 *
 * @ingroup KNX_03_06_03_03_03_09_07
 */
class KNX_EXPORT EMI_PEI_Switch_req :
    public EMI_Message
{
public:
    EMI_PEI_Switch_req();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** System Status */
    System_Status system_status{};

    /** Data Link Layer (LL) */
    Destination_Layer ll{LL};

    /** Network Layer (NL) */
    Destination_Layer nl{NL};

    /** Transport Layer Group Oriented (TLG) */
    Destination_Layer tlg{TLG};

    /** Transport Layer Connection Oriented (TLC) */
    Destination_Layer tlc{TLC};

    /** Transport Layer Local (TLL) */
    Destination_Layer tll{TLL};

    /** Application Layer (AL) */
    Destination_Layer al{AL};

    /** Management part of the Application Layer (MAN) */
    Destination_Layer man{MAN};

    /** Physical External Interface (PEI) */
    Destination_Layer pei{PEI};

    /** Application (USR) */
    Destination_Layer usr{USR};

    /** reserved */
    Destination_Layer res{RES};
};

}
