// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Other/TM_Timer/EMI_TM_Timer_ind.h>

#include <cassert>

namespace KNX {

EMI_TM_Timer_ind::EMI_TM_Timer_ind() :
    EMI_Message(EMI_Message_Code::TM_Timer_ind)
{
}

void EMI_TM_Timer_ind::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    /* octet 1 */
    EMI_Message::fromData(first, first + 1);
    ++first;

    if (std::distance(first, last) == 1) {
        style = 1;

        /* octet 2 */
        timer_number = *first++;
    }
    if (std::distance(first, last) == 7) {
        style = 2;

        /* octet 2 */
        timer_parameter = *first++;

        /* octet 3 */
        timer_number = *first++;

        /* octet 4..8 */
        first += 5; // unused
    }

    assert(first == last);
}

std::vector<uint8_t> EMI_TM_Timer_ind::toData() const
{
    std::vector<uint8_t> data = EMI_Message::toData();

    if (style == 1) {
        /* octet 2 */
        data.push_back(timer_number);
    }
    if (style == 2) {
        /* octet 2 */
        data.push_back(timer_parameter);

        /* octet 3 */
        data.push_back(timer_number);

        /* octet 4..8 */
        data.push_back(0);
        data.push_back(0);
        data.push_back(0);
        data.push_back(0);
        data.push_back(0);
    }

    return data;
}

}
