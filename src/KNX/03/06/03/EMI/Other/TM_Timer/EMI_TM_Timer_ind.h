// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * TM_Timer.ind message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_09_08
 */
class KNX_EXPORT EMI_TM_Timer_ind :
    public EMI_Message
{
public:
    EMI_TM_Timer_ind();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Style */
    uint8_t style{}; // 1 (control unit) or 2 (BCU 2)

    /** timer parameter */
    uint8_t timer_parameter{};

    /** timer number */
    uint8_t timer_number{};
};

}
