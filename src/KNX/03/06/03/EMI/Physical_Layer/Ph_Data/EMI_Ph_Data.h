// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Ph_Data message
 */
class KNX_EXPORT EMI_Ph_Data :
    public EMI_Message
{
public:
    explicit EMI_Ph_Data(const EMI_Message_Code message_code);
};

}
