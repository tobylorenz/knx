// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Physical_Layer/Ph_Data/EMI_Ph_Data_con.h>

namespace KNX {

EMI_Ph_Data_con::EMI_Ph_Data_con() :
    EMI_Ph_Data(EMI_Message_Code::Ph_Data_con)
{
}

}
