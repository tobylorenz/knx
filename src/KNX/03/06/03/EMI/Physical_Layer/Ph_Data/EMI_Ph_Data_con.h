// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Physical_Layer/Ph_Data/EMI_Ph_Data.h>
#include <KNX/knx_export.h>

namespace KNX {

class KNX_EXPORT EMI_Ph_Data_con :
    public EMI_Ph_Data
{
public:
    EMI_Ph_Data_con();
};

}
