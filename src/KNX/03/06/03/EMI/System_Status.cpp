// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/System_Status.h>

namespace KNX {

System_Status::System_Status(const uint8_t system_status) :
    parity((system_status >> 7) & 0x01),
    dm((system_status >> 6) & 0x01),
    ue((system_status >> 5) & 0x01),
    se((system_status >> 4) & 0x01),
    ale((system_status >> 3) & 0x01),
    tle((system_status >> 2) & 0x01),
    llm((system_status >> 1) & 0x01),
    prog((system_status >> 0) & 0x01)
{
}

System_Status & System_Status::operator=(const uint8_t & system_status)
{
    parity = (system_status >> 7) & 0x01;
    dm = (system_status >> 6) & 0x01;
    ue = (system_status >> 5) & 0x01;
    se = (system_status >> 4) & 0x01;
    ale = (system_status >> 3) & 0x01;
    tle = (system_status >> 2) & 0x01;
    llm = (system_status >> 1) & 0x01;
    prog = (system_status >> 0) & 0x01;

    return *this;
}

System_Status::operator uint8_t() const
{
    return
        (static_cast<uint8_t>(parity_calculated()) << 7) |
        (static_cast<uint8_t>(dm) << 6) |
        (static_cast<uint8_t>(ue) << 5) |
        (static_cast<uint8_t>(se) << 4) |
        (static_cast<uint8_t>(ale) << 3) |
        (static_cast<uint8_t>(tle) << 2) |
        (static_cast<uint8_t>(llm) << 1) |
        (static_cast<uint8_t>(prog) << 0);
}

bool System_Status::parity_calculated() const
{
    return dm ^ ue ^ se ^ ale ^ tle ^ llm ^ prog;
}

}
