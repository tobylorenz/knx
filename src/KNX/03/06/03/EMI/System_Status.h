// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * System Status
 *
 * @ingroup KNX_03_06_03_03_01_03
 * @ingroup KNX_09_04_01_03_01_10_02_01_02
 *
 * @todo looks same as Programming_Mode_Property
 *
 * - 90h: Busmonitor (se=1)
 * - 12h: Data Link Layer (llm=1)
 * - 96h: Transport Layer (se=1, tle=1, llm=1)
 * - 1Eh: Application Layer (ale=1, tle=1, llm=1)
 * - C0h: Reset (dm=1)
 */
class KNX_EXPORT System_Status
{
public:
    System_Status() = default;
    explicit System_Status(const uint8_t system_status);
    System_Status & operator=(const uint8_t & system_status);
    operator uint8_t() const;

    /** calculate Parity */
    bool parity_calculated() const;

    /** even parity for the "system status" octet */
    bool parity{true};

    /** programming mode (ready to accept individual address), else normal operation mode */
    bool dm{false};

    /** user program enabled */
    bool ue{true};

    /** serial PEI-interface (message-protocol) enabled */
    bool se{true};

    /** Application Layer enabled */
    bool ale{true};

    /** Transport Layer enabled */
    bool tle{true};

    /** Link Layer enabled, else Busmonitor Mode */
    bool llm{true};

    /** programming mode, else normal operation mode */
    bool prog{false};
};

}
