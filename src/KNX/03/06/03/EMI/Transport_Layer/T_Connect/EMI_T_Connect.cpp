// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Transport_Layer/T_Connect/EMI_T_Connect.h>

#include <cassert>

namespace KNX {

EMI_T_Connect::EMI_T_Connect(const EMI_Message_Code message_code) :
    EMI_TPDU(message_code)
{
}

}
