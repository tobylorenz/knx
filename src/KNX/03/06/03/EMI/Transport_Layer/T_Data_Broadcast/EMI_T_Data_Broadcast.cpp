// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast.h>

#include <cassert>

#include <KNX/03/03/07/APDU.h>

namespace KNX {

EMI_T_Data_Broadcast::EMI_T_Data_Broadcast(const EMI_Message_Code message_code) :
    EMI_TPDU(message_code)
{
}

void EMI_T_Data_Broadcast::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    /* octet 1..6 */
    EMI_TPDU::fromData(first, first + 6);
    first += 6;

    /* octet 7 */
    hop_count_type = (*first >> 4) & 0x07;
    length = *first & 0x0f;
    ++first;

    /* octet 8..N */
    tsdu = make_APDU(first, last);
}

std::vector<uint8_t> EMI_T_Data_Broadcast::toData() const
{
    assert(tsdu);

    /* octet 1..6 */
    std::vector<uint8_t> data = EMI_TPDU::toData();

    /* octet 7 */
    data.push_back(
        (1 << 7) |
        (hop_count_type << 4) |
        length_calculated());

    /* octet 8..N */
    const std::vector<uint8_t> tsdu_data = tsdu->toData();
    data.insert(std::cend(data), std::cbegin(tsdu_data), std::cend(tsdu_data));

    return data;
}

uint8_t EMI_T_Data_Broadcast::length_calculated() const
{
    return tsdu ? tsdu->length_calculated() : 0;
}

}
