// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast_ind.h>

namespace KNX {

EMI_T_Data_Broadcast_ind::EMI_T_Data_Broadcast_ind() :
    EMI_T_Data_Broadcast(EMI_Message_Code::T_Data_Broadcast_ind)
{
}

}
