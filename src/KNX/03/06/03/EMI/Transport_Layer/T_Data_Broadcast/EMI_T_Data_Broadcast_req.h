// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Broadcast/EMI_T_Data_Broadcast.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * T_Data_Broadcast.req message (EMI2/IMI1+)
 *
 * @ingroup KNX_03_06_03_03_03_06_14
 */
class KNX_EXPORT EMI_T_Data_Broadcast_req :
    public EMI_T_Data_Broadcast
{
public:
    EMI_T_Data_Broadcast_req();
};

}
