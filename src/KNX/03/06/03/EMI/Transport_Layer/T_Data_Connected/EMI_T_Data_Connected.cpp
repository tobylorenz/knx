// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Connected/EMI_T_Data_Connected.h>

#include <cassert>

#include <KNX/03/03/07/APDU.h>

namespace KNX {

EMI_T_Data_Connected::EMI_T_Data_Connected(const EMI_Message_Code message_code) :
    EMI_TPDU(message_code)
{
}

void EMI_T_Data_Connected::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    /* octet 1..6 */
    EMI_TPDU::fromData(first, first + 6);
    first += 6;

    /* octet 7 */
    const uint8_t octet_7 = *first++;
    hop_count_type = (octet_7 >> 4) & 0x07;
    length = octet_7 & 0x0f;

    /* octet 8..N */
    tsdu = make_APDU(first, last);
}

std::vector<uint8_t> EMI_T_Data_Connected::toData() const
{
    assert(tsdu);

    /* octet 1..5 */
    std::vector<uint8_t> data = EMI_TPDU::toData();

    /* octet 7 */
    data.push_back(
        (hop_count_type << 4) |
        length_calculated());

    /* octet 8..N */
    std::vector<uint8_t> tsdu_data = tsdu->toData();
    tsdu_data[0] |= (1 << 6); // numbered
    data.insert(std::cend(data), std::cbegin(tsdu_data), std::cend(tsdu_data));

    return data;
}

uint8_t EMI_T_Data_Connected::length_calculated() const
{
    return tsdu ? tsdu->length_calculated() : 0;
}

}
