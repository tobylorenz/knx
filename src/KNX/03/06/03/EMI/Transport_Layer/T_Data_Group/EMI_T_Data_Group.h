// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/03/Hop_Count_Type.h>
#include <KNX/03/03/04/TSDU.h>
#include <KNX/03/06/03/EMI/Transport_Layer/EMI_TPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * T_Data_Group message
 */
class KNX_EXPORT EMI_T_Data_Group:
    public EMI_TPDU
{
public:
    explicit EMI_T_Data_Group(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Hop Count (HC) type */
    Hop_Count_Type hop_count_type{};

    /** length */
    uint8_t length{};

    virtual uint8_t length_calculated() const;

    /** TSDU = APDU */
    std::shared_ptr<TSDU> tsdu{};
};

}
