// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group_con.h>

namespace KNX {

EMI_T_Data_Group_con::EMI_T_Data_Group_con() :
    EMI_T_Data_Group(EMI_Message_Code::T_Data_Group_con)
{
}

}
