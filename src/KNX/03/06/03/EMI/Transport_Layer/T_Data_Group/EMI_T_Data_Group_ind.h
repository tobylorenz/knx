// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Group/EMI_T_Data_Group.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * T_Data_Group.ind message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_06_13
 */
class KNX_EXPORT EMI_T_Data_Group_ind :
    public EMI_T_Data_Group
{
public:
    EMI_T_Data_Group_ind();
};

}
