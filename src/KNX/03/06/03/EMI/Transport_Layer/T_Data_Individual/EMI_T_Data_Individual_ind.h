// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * T_Data_Individual.ind message (EMI2/ IMI1+/ EMI1+)
 *
 * @ingroup KNX_03_06_03_03_03_06_22
 */
class KNX_EXPORT EMI_T_Data_Individual_ind :
    public EMI_T_Data_Individual
{
public:
    EMI_T_Data_Individual_ind();
};

}
