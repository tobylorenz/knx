// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Transport_Layer/T_Data_Individual/EMI_T_Data_Individual_req.h>

namespace KNX {

EMI_T_Data_Individual_req::EMI_T_Data_Individual_req() :
    EMI_T_Data_Individual(EMI_Message_Code::T_Data_Individual_req)
{
}

}
