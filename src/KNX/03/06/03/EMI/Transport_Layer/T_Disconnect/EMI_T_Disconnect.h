// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Transport_Layer/EMI_TPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * T_Disconnect message
 */
class KNX_EXPORT EMI_T_Disconnect :
    public EMI_TPDU
{
public:
    explicit EMI_T_Disconnect(const EMI_Message_Code message_code);
};

}
