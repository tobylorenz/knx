// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect_con.h>

namespace KNX {

EMI_T_Disconnect_con::EMI_T_Disconnect_con() :
    EMI_T_Disconnect(EMI_Message_Code::T_Disconnect_con)
{
}

}
