// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Transport_Layer/T_Disconnect/EMI_T_Disconnect.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * T_Disconnect.req message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_06_05
 */
class KNX_EXPORT EMI_T_Disconnect_req :
    public EMI_T_Disconnect
{
public:
    EMI_T_Disconnect_req();
};

}
