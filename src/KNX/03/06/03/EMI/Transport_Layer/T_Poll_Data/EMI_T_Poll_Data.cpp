// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Transport_Layer/T_Poll_Data/EMI_T_Poll_Data.h>

#include <cassert>

namespace KNX {

EMI_T_Poll_Data::EMI_T_Poll_Data(const EMI_Message_Code message_code) :
    EMI_TPDU(message_code)
{
    control.poll = Poll::Poll_Data;
}

void EMI_T_Poll_Data::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 7);

    /* octet 1..6 */
    EMI_TPDU::fromData(first, first + 6);
    first += 6;

    /* octet 7 */
    nr_of_slots = *first++ & 0x0f;

    assert(first == last);
}

std::vector<uint8_t> EMI_T_Poll_Data::toData() const
{
    /* octet 1..6 */
    std::vector<uint8_t> data = EMI_TPDU::toData();

    /* octet 7 */
    data.push_back(nr_of_slots & 0x0f);

    return data;
}

}
