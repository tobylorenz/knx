// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Transport_Layer/EMI_TPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * T_Poll_Data message
 */
class KNX_EXPORT EMI_T_Poll_Data :
    public EMI_TPDU
{
public:
    explicit EMI_T_Poll_Data(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** nr of slots */
    uint8_t nr_of_slots{};
};

}
