// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Transport_Layer/T_Poll_Data/EMI_T_Poll_Data.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * T_Poll_Data.con message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_06_24
 */
class KNX_EXPORT EMI_T_Poll_Data_con :
    public EMI_T_Poll_Data
{
public:
    EMI_T_Poll_Data_con();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** poll data */
    std::vector<uint8_t> poll_data{};
};

}
