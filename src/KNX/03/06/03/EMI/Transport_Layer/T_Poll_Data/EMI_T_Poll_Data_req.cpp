// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/Transport_Layer/T_Poll_Data/EMI_T_Poll_Data_req.h>

namespace KNX {

EMI_T_Poll_Data_req::EMI_T_Poll_Data_req() :
    EMI_T_Poll_Data(EMI_Message_Code::T_Poll_Data_req)
{
}

}
