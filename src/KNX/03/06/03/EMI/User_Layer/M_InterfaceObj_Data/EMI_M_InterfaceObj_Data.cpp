// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data.h>

#include <cassert>

namespace KNX {

EMI_M_InterfaceObj_Data::EMI_M_InterfaceObj_Data(const EMI_Message_Code message_code) :
    EMI_Message(message_code)
{
}

void EMI_M_InterfaceObj_Data::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 7);

    /* octet 1 */
    EMI_Message::fromData(first, first + 1);
    ++first;

    /* octet 2 */
    control = *first++;

    /* octet 3..4 */
    destination_address.fromData(first, first + 2);
    first += 2;

    /* octet 5..6 */
    communication_type = *first++;
    ++first; // unused

    /* octet 7 */
    const uint8_t octet_7 = *first++;
    hop_count_type = (octet_7 >> 4) & 0x07;
    length = octet_7 & 0x0f;

    /* octet 8..N */
    apdu = make_APDU(first, last);
}

std::vector<uint8_t> EMI_M_InterfaceObj_Data::toData() const
{
    assert(apdu);

    std::vector<uint8_t> data = EMI_Message::toData();

    /* octet 2 */
    data.push_back(control);

    /* octet 3..4 */
    data.push_back(destination_address >> 8);
    data.push_back(destination_address & 0xff);

    /* octet 5..6 */
    data.push_back(communication_type);
    data.push_back(0); // unused

    /* octet 7 */
    data.push_back(
        (hop_count_type << 4) |
        length_calculated());

    /* octet 8..N */
    const std::vector<uint8_t> apdu_data = apdu->toData();
    data.insert(std::cend(data), std::cbegin(apdu_data), std::cend(apdu_data));

    return data;
}

uint8_t EMI_M_InterfaceObj_Data::length_calculated() const
{
    assert(apdu);

    return apdu->length_calculated();
}

}
