// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Control.h>
#include <KNX/03/03/03/Hop_Count_Type.h>
#include <KNX/03/03/07/APDU.h>
#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_InterfaceObj_Data message
 */
class KNX_EXPORT EMI_M_InterfaceObj_Data :
    public EMI_Message
{
public:
    explicit EMI_M_InterfaceObj_Data(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Control */
    Control control{};

    /** Destination Address */
    Address destination_address{};

    /** communication type */
    uint8_t communication_type{}; // 0=connection-oriented; >0=connectionless

    /** hop count type */
    Hop_Count_Type hop_count_type{Network_Layer_Parameter};

    /** length */
    uint8_t length{};

    virtual uint8_t length_calculated() const;

    /** APDU */
    std::shared_ptr<APDU> apdu;
};

}
