// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data_con.h>

namespace KNX {

EMI_M_InterfaceObj_Data_con::EMI_M_InterfaceObj_Data_con() :
    EMI_M_InterfaceObj_Data(EMI_Message_Code::M_InterfaceObj_Data_con)
{
}

}
