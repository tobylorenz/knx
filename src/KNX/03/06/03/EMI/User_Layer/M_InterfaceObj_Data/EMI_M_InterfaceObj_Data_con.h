// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/User_Layer/M_InterfaceObj_Data/EMI_M_InterfaceObj_Data.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_InterfaceObj_Data.con message (EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_08_09
 */
class KNX_EXPORT EMI_M_InterfaceObj_Data_con :
    public EMI_M_InterfaceObj_Data
{
public:
    EMI_M_InterfaceObj_Data_con();
};

}
