// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/User_Layer/U_Event/EMI_U_Event_ind.h>

#include <cassert>

namespace KNX {

EMI_U_Event_ind::EMI_U_Event_ind() :
    EMI_Message(EMI_Message_Code::U_Event_ind)
{
}

void EMI_U_Event_ind::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    EMI_Message::fromData(first, first + 1);
    ++first;

    cr_id = *first++;

    assert(first == last);
}

std::vector<uint8_t> EMI_U_Event_ind::toData() const
{
    std::vector<uint8_t> data = EMI_Message::toData();

    data.push_back(cr_id);

    return data;
}

}
