// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/Connection_Number.h>
#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * U_Event.ind message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_08_06
 */
class KNX_EXPORT EMI_U_Event_ind :
    public EMI_Message
{
public:
    EMI_U_Event_ind();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** connection number */
    Connection_Number cr_id{};
};

}
