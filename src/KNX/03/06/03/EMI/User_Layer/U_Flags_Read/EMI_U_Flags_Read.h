// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Group_Object_Number.h>
#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * U_Flags_Read message
 */
class KNX_EXPORT EMI_U_Flags_Read :
    public EMI_Message
{
public:
    explicit EMI_U_Flags_Read(const EMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Group Object Number */
    Group_Object_Number group_object_number{};
};

}
