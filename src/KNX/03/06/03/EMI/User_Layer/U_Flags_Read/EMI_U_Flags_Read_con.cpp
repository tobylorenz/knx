// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/User_Layer/U_Flags_Read/EMI_U_Flags_Read_con.h>

#include <cassert>

namespace KNX {

EMI_U_Flags_Read_con::EMI_U_Flags_Read_con() :
    EMI_U_Flags_Read(EMI_Message_Code::U_Flags_Read_con)
{
}

void EMI_U_Flags_Read_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    /* octet 1..2 */
    EMI_U_Flags_Read::fromData(first, first + 2);
    first += 2;

    /* octet 3 */
    ram_flags = *first++;

    /* octet 4 */
    eeprom_flags = *first++;

    /* octet 5 */
    value_type = static_cast<Group_Object_Type>(*first++);

    assert(first == last);
}

std::vector<uint8_t> EMI_U_Flags_Read_con::toData() const
{
    std::vector<uint8_t> data = EMI_U_Flags_Read::toData();

    /* octet 3 */
    data.push_back(ram_flags);

    /* octet 4 */
    data.push_back(eeprom_flags);

    /* octet 5 */
    data.push_back(static_cast<uint8_t>(value_type));

    return data;
}

}
