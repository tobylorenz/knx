// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/09_Group_Object_Table/Group_Object_Table.h>
#include <KNX/03/06/03/EMI/EEPROM_Flags.h>
#include <KNX/03/06/03/EMI/RAM_Flags.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Flags_Read/EMI_U_Flags_Read.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * U_Flags_Read.con message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_08_05
 */
class KNX_EXPORT EMI_U_Flags_Read_con :
    public EMI_U_Flags_Read
{
public:
    EMI_U_Flags_Read_con();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** RAM Flags */
    RAM_Flags ram_flags{};

    /** EEPROM flags */
    EEPROM_Flags eeprom_flags{};

    /** value type */
    Group_Object_Type value_type{};
};

}
