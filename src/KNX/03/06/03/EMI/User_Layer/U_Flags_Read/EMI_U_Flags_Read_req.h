// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/User_Layer/U_Flags_Read/EMI_U_Flags_Read.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * U_Flags_Read.req message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_08_04
 */
class KNX_EXPORT EMI_U_Flags_Read_req :
    public EMI_U_Flags_Read
{
public:
    EMI_U_Flags_Read_req();
};

}
