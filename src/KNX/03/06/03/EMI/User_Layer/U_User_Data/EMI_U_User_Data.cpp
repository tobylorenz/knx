// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/User_Layer/U_User_Data/EMI_U_User_Data.h>

#include <cassert>

namespace KNX {

EMI_U_User_Data::EMI_U_User_Data(const EMI_Message_Code message_code) :
    EMI_Message(message_code)
{
}

void EMI_U_User_Data::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 1);

    EMI_Message::fromData(first, first + 1);
    ++first;

    data.assign(first, last);
}

std::vector<uint8_t> EMI_U_User_Data::toData() const
{
    std::vector<uint8_t> data = EMI_Message::toData();

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

}
