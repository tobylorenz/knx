// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read.h>

#include <cassert>

namespace KNX {

EMI_U_Value_Read::EMI_U_Value_Read(const EMI_Message_Code message_code) :
    EMI_Message(message_code)
{
}

void EMI_U_Value_Read::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    EMI_Message::fromData(first, first + 1);
    ++first;

    group_object_number = Group_Object_Number(*first++);

    assert(first == last);
}

std::vector<uint8_t> EMI_U_Value_Read::toData() const
{
    std::vector<uint8_t> data = EMI_Message::toData();

    data.push_back(group_object_number);

    return data;
}

}
