// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read_con.h>

#include <cassert>

namespace KNX {

EMI_U_Value_Read_con::EMI_U_Value_Read_con() :
    EMI_U_Value_Read(EMI_Message_Code::U_Value_Read_con)
{
}

void EMI_U_Value_Read_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 3);

    /* octet 1..2 */
    EMI_U_Value_Read::fromData(first, first + 2);
    first += 2;

    /* octet 3 */
    ram_flags = *first++;

    /* octet 4..N */
    value.assign(first, last);
}

std::vector<uint8_t> EMI_U_Value_Read_con::toData() const
{
    std::vector<uint8_t> data = EMI_U_Value_Read::toData();

    /* octet 3 */
    data.push_back(ram_flags);

    /* octet 4..N */
    data.insert(std::cend(data), std::cbegin(value), std::cend(value));

    return data;
}

}
