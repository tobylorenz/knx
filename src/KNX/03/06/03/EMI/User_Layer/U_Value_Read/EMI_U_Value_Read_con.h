// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/EMI/RAM_Flags.h>
#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * U_Value_Read.con message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_08_03
 */
class KNX_EXPORT EMI_U_Value_Read_con :
    public EMI_U_Value_Read
{
public:
    EMI_U_Value_Read_con();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** RAM flags */
    RAM_Flags ram_flags{};

    /** value */
    std::vector<uint8_t> value{};
};

}
