// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/User_Layer/U_Value_Read/EMI_U_Value_Read_req.h>

namespace KNX {

EMI_U_Value_Read_req::EMI_U_Value_Read_req() :
    EMI_U_Value_Read(EMI_Message_Code::U_Value_Read_req)
{
}

}
