// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/EMI/User_Layer/U_Value_Write/EMI_U_Value_Write_req.h>

#include <cassert>

namespace KNX {

EMI_U_Value_Write_req::EMI_U_Value_Write_req() :
    EMI_Message(EMI_Message_Code::U_Value_Write_req)
{
}

void EMI_U_Value_Write_req::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 3);

    EMI_Message::fromData(first, first + 1);
    ++first;

    group_object_number = Group_Object_Number(*first++);

    write_mask.value_write_enable = (*first >> 7) & 0x01;
    write_mask.update_flag_write_enable = (*first >> 6) & 0x01;
    write_mask.data_request_flag_write_enable = (*first >> 5) & 0x01;
    write_mask.transmission_status_write_enable = (*first >> 4) & 0x01;
    ram_flags = (*first >> 0) & 0x0f;
    ++first;

    value.assign(first, last);
}

std::vector<uint8_t> EMI_U_Value_Write_req::toData() const
{
    std::vector<uint8_t> data = EMI_Message::toData();

    data.push_back(group_object_number);
    data.push_back(
        (static_cast<uint8_t>(write_mask.value_write_enable) << 7) |
        (static_cast<uint8_t>(write_mask.update_flag_write_enable) << 6) |
        (static_cast<uint8_t>(write_mask.data_request_flag_write_enable) << 5) |
        (static_cast<uint8_t>(write_mask.transmission_status_write_enable) << 4) |
        (static_cast<uint8_t>(ram_flags) << 0));
    data.insert(std::cend(data), std::cbegin(value), std::cend(value));

    return data;
}

}
