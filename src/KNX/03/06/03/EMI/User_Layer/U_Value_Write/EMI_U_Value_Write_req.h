// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Group_Object_Number.h>
#include <KNX/03/06/03/EMI/EMI_Message.h>
#include <KNX/03/06/03/EMI/RAM_Flags.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * U_Value_Write.req message (EMI1 / EMI2)
 *
 * @ingroup KNX_03_06_03_03_03_08_07
 */
class KNX_EXPORT EMI_U_Value_Write_req :
    public EMI_Message
{
public:
    EMI_U_Value_Write_req();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Group Object Number */
    Group_Object_Number group_object_number{};

    /** Write Mask */
    struct Write_Mask {
        /** value write enable */
        bool value_write_enable{false};

        /** update flag write enable */
        bool update_flag_write_enable{false};

        /** data request flag write enable */
        bool data_request_flag_write_enable{false};

        /** transmission status write enable */
        bool transmission_status_write_enable{false};
    };

    /** @copydoc Write_Mask */
    Write_Mask write_mask{};

    /** RAM flags */
    RAM_Flags ram_flags{};

    /** value */
    std::vector<uint8_t> value{};
};

}
