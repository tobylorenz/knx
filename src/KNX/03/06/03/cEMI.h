// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 4.1.4.3 Additional Information */

/* 4.1.4.3.1 Overview */
#include <KNX/03/06/03/cEMI/Additional_Info.h>

/* AddInfo-Type 01h: PL medium information */
#include <KNX/03/06/03/cEMI/Additional_Info/PL_medium_info.h>

/* 4.1.4.3.2 AddInfo-Type 02h: RF medium information */
#include <KNX/03/06/03/cEMI/Additional_Info/RF_medium_info.h>

/* AddInfo-Type 03h: Busmonitor - Status Info */
#include <KNX/03/06/03/cEMI/Additional_Info/Busmonitor_Status_Info.h>

/* AddInfo-Type 04h: Timestamp relative */
#include <KNX/03/06/03/cEMI/Additional_Info/Timestamp_relative.h>

/* AddInfo-Type 05h: Time delay until sending */
#include <KNX/03/06/03/cEMI/Additional_Info/Time_delay_until_sending.h>

/* 4.1.4.3.3 AddInfo-Type 06h: Extended relative timestamp */
#include <KNX/03/06/03/cEMI/Additional_Info/Extended_relative_timestamp.h>

/* 4.1.4.3.4 AddInfo-Type 07h: BiBat information */
#include <KNX/03/06/03/cEMI/Additional_Info/BiBat_info.h>

/* 4.1.4.3.5 AddInfo-Type 08h: RF Multi information */
#include <KNX/03/06/03/cEMI/Additional_Info/RF_Multi_info.h>

/* 4.1.4.3.6 AddInfo-Type 09h: Preamble and postamble */
#include <KNX/03/06/03/cEMI/Additional_Info/Preamble_and_postamble.h>

/* 4.1.4.3.7 AddInfo-Type 0Ah: RF Fast ack information */
#include <KNX/03/06/03/cEMI/Additional_Info/RF_Fast_Ack_info.h>

/* 4.1.4.3.8 AddInfo-Type FEh: Manufacturer specific data */
#include <KNX/03/06/03/cEMI/Additional_Info/Manufacturer_specific_data.h>

/* 4.1.5.3.2 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data.h>

/* 4.1.5.3.3 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_req.h>

/* 4.1.5.3.4 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_con.h>

/* 4.1.5.3.5 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_ind.h>

/* 4.1.5.6.2 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_req.h>

/* 4.1.5.6.3 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_con.h>

/* 4.1.5.7.2 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw.h>

/* 4.1.5.7.3 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_req.h>

/* 4.1.5.7.4 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_con.h>

/* 4.1.5.7.5 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_ind.h>

/* 4.1.5.7.6 */
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Busmon/CEMI_L_Busmon_ind.h>

/* 4.1.6.1 */
#include <KNX/03/06/03/cEMI/Transport_Layer/CEMI_TPDU.h>

/* 4.1.6.4.4 */
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Individual/CEMI_T_Data_Individual_req.h>

/* 4.1.6.4.5 */
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Individual/CEMI_T_Data_Individual_ind.h>

/* 4.1.6.5.4 */
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Connected/CEMI_T_Data_Connected_req.h>

/* 4.1.6.5.5 */
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Connected/CEMI_T_Data_Connected_ind.h>
