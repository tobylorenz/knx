// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info.h>

#include <cassert>

#include <KNX/03/06/03/cEMI/Additional_Info/PL_medium_info.h>
#include <KNX/03/06/03/cEMI/Additional_Info/RF_medium_info.h>
#include <KNX/03/06/03/cEMI/Additional_Info/Busmonitor_Status_Info.h>
#include <KNX/03/06/03/cEMI/Additional_Info/Timestamp_relative.h>
#include <KNX/03/06/03/cEMI/Additional_Info/Time_delay_until_sending.h>
#include <KNX/03/06/03/cEMI/Additional_Info/Extended_relative_timestamp.h>
#include <KNX/03/06/03/cEMI/Additional_Info/BiBat_info.h>
#include <KNX/03/06/03/cEMI/Additional_Info/RF_Multi_info.h>
#include <KNX/03/06/03/cEMI/Additional_Info/Preamble_and_postamble.h>
#include <KNX/03/06/03/cEMI/Additional_Info/RF_Fast_Ack_info.h>
#include <KNX/03/06/03/cEMI/Additional_Info/Manufacturer_specific_data.h>

namespace KNX {

void Additional_Info::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    type_id = static_cast<TypeId>(*first++);
    length = *first++;

    assert(first == last);
}

std::vector<uint8_t> Additional_Info::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(static_cast<uint8_t>(type_id));
    data.push_back(length);

    return data;
}

uint8_t Additional_Info::length_calculated() const
{
    return 0;
}

std::shared_ptr<Additional_Info> make_Additional_Info(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    Additional_Info ai;
    ai.fromData(first, first + 2);
    switch (ai.type_id) {
    case Additional_Info::TypeId::PL_medium_info: {
        std::shared_ptr<PL_medium_info> ai2 = std::make_shared<PL_medium_info>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::RF_medium_info: {
        std::shared_ptr<RF_medium_info> ai2 = std::make_shared<RF_medium_info>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::Busmonitor_Status_Info: {
        std::shared_ptr<Busmonitor_Status_Info> ai2 = std::make_shared<Busmonitor_Status_Info>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::Timestamp_relative: {
        std::shared_ptr<Timestamp_relative> ai2 = std::make_shared<Timestamp_relative>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::Time_delay_until_sending: {
        std::shared_ptr<Time_delay_until_sending> ai2 = std::make_shared<Time_delay_until_sending>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::Extended_relative_timestamp: {
        std::shared_ptr<Extended_relative_timestamp> ai2 = std::make_shared<Extended_relative_timestamp>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::BiBat_info: {
        std::shared_ptr<BiBat_info> ai2 = std::make_shared<BiBat_info>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::RF_Multi_info: {
        std::shared_ptr<RF_Multi_info> ai2 = std::make_shared<RF_Multi_info>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::Preamble_and_postamble: {
        std::shared_ptr<Preamble_and_postamble> ai2 = std::make_shared<Preamble_and_postamble>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::RF_Fast_Ack_info: {
        std::shared_ptr<RF_Fast_Ack_info> ai2 = std::make_shared<RF_Fast_Ack_info>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    case Additional_Info::TypeId::Manufacturer_specific_data: {
        std::shared_ptr<Manufacturer_specific_data> ai2 = std::make_shared<Manufacturer_specific_data>();
        ai2->fromData(first, first + 2 + ai.length);
        return ai2;
    }
    break;
    }
    return std::make_shared<Additional_Info>(ai);
}

}
