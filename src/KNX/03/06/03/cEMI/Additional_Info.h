// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>
#include <memory>
#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Additional Information
 *
 * @ingroup KNX_03_06_03_04_01_04_03_01
 */
class KNX_EXPORT Additional_Info
{
public:
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /** Type ID */
    enum class TypeId : uint8_t {
        PL_medium_info = 0x01,
        RF_medium_info = 0x02,
        Busmonitor_Status_Info = 0x03,
        Timestamp_relative = 0x04,
        Time_delay_until_sending = 0x05,
        Extended_relative_timestamp = 0x06,
        BiBat_info = 0x07,
        RF_Multi_info = 0x08,
        Preamble_and_postamble = 0x09,
        RF_Fast_Ack_info = 0x0A,
        Manufacturer_specific_data = 0xFE
    };

    /** @copydoc TypeId */
    TypeId type_id{};

    /** Len */
    uint8_t length{};

    virtual uint8_t length_calculated() const;
};

KNX_EXPORT std::shared_ptr<Additional_Info> make_Additional_Info(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
