// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/BiBat_info.h>

#include <cassert>

namespace KNX {

BiBat_info::BiBat_info() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::BiBat_info;
    length = 2;
}

void BiBat_info::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    ctrl = *first++;

    block = *first++;

    assert(first == last);
}

std::vector<uint8_t> BiBat_info::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back(ctrl);
    data.push_back(block);

    return data;
}

uint8_t BiBat_info::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        2;
}

}
