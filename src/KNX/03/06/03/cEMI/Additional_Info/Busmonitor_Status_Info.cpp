// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/Busmonitor_Status_Info.h>

#include <cassert>

namespace KNX {

Busmonitor_Status_Info::Busmonitor_Status_Info() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::Busmonitor_Status_Info;
    length = 1;
}

void Busmonitor_Status_Info::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 3);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    const uint8_t status = *first++;
    frame_error = (status >> 7) & 0x01;
    bit_error = (status >> 6) & 0x01;
    parity_error = (status >> 5) & 0x01;
    overflow = (status >> 4) & 0x01;
    lost = (status >> 3) & 0x01;
    sequence_number = status & 0x07;

    assert(first == last);
}

std::vector<uint8_t> Busmonitor_Status_Info::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back(
        (frame_error << 7) |
        (bit_error << 6) |
        (parity_error << 5) |
        (overflow << 4) |
        (lost << 3) |
        (sequence_number & 0x07));

    return data;
}

uint8_t Busmonitor_Status_Info::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        1;
}

}
