// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Additional_Info.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Busmonitor - Status Info
 *
 * @ingroup KNX_03_06_03_04_01_05_07_06
 */
class KNX_EXPORT Busmonitor_Status_Info : public Additional_Info
{
public:
    Busmonitor_Status_Info();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /* Frame error flags (F) */
    bool frame_error{false};

    /* Bit error flag (B) */
    bool bit_error{false};

    /* Parity error flag (P) */
    bool parity_error{false};

    /* Overflow flag */
    bool overflow{false};

    /* Lost flag (L) */
    bool lost{false};

    /* Sequence Number */
    uint8_t sequence_number{};
};

}
