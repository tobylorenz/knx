// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/Extended_relative_timestamp.h>

#include <cassert>

namespace KNX {

Extended_relative_timestamp::Extended_relative_timestamp() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::Extended_relative_timestamp;
    length = 4;
}

void Extended_relative_timestamp::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    timestamp =
        (*first++ << 24) |
        (*first++ << 16) |
        (*first++ << 8) |
        *first++;

    assert(first == last);
}

std::vector<uint8_t> Extended_relative_timestamp::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back((timestamp >> 24) & 0xff);
    data.push_back((timestamp >> 16) & 0xff);
    data.push_back((timestamp >> 8) & 0xff);
    data.push_back(timestamp & 0xff);

    return data;
}

uint8_t Extended_relative_timestamp::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        4;
}

}
