// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/Manufacturer_specific_data.h>

#include <cassert>

namespace KNX {

Manufacturer_specific_data::Manufacturer_specific_data() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::Manufacturer_specific_data;
    // m_length = 3; or more
}

void Manufacturer_specific_data::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 5);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    manufacturer_id = (*first++ << 8) | *first++;

    subfunction = *first++;

    manufacturer_specific_data.insert(std::cend(manufacturer_specific_data), first, last);
}

std::vector<uint8_t> Manufacturer_specific_data::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back(manufacturer_id >> 8);
    data.push_back(manufacturer_id & 0xff);
    data.insert(std::cend(data), std::cbegin(manufacturer_specific_data), std::cend(manufacturer_specific_data));

    return data;
}

uint8_t Manufacturer_specific_data::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        3 +
        static_cast<uint8_t>(manufacturer_specific_data.size());
}


}
