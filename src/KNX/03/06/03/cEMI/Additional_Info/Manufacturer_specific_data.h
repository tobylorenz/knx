// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Additional_Info.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Manufacturer specific data
 *
 * @ingroup KNX_03_06_03_04_01_04_03_08
 */
class KNX_EXPORT Manufacturer_specific_data : public Additional_Info
{
public:
    Manufacturer_specific_data();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Manufacturer ID */
    uint16_t manufacturer_id{};

    /** Subfunction */
    uint8_t subfunction{};

    /** Manufacturer specific data (variable length) */
    std::vector<uint8_t> manufacturer_specific_data{};
};

}
