// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/PL_medium_info.h>

#include <cassert>

namespace KNX {

PL_medium_info::PL_medium_info() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::PL_medium_info;
    length = 2;
}

void PL_medium_info::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    domain_address.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> PL_medium_info::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back(domain_address >> 8);
    data.push_back(domain_address & 0xff);

    return data;
}

uint8_t PL_medium_info::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        2;
}

}
