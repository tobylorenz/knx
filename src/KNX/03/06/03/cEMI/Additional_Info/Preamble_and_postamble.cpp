// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/Preamble_and_postamble.h>

#include <cassert>

namespace KNX {

Preamble_and_postamble::Preamble_and_postamble() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::Preamble_and_postamble;
    length = 3;
}

void Preamble_and_postamble::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    preamble_length = (*first++ << 8) | *first++;

    postamble_length = *first++;

    assert(first == last);

}

std::vector<uint8_t> Preamble_and_postamble::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back(preamble_length >> 8);
    data.push_back(preamble_length & 0xff);
    data.push_back(postamble_length);

    return data;
}

uint8_t Preamble_and_postamble::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        3;
}

}
