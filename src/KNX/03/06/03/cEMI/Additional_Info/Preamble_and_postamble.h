// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Additional_Info.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Preamble and postamble
 *
 * @ingroup KNX_03_06_03_04_01_04_03_06
 */
class KNX_EXPORT Preamble_and_postamble : public Additional_Info
{
public:
    Preamble_and_postamble();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Preamble length */
    uint16_t preamble_length{};

    /** Postamble length */
    uint8_t postamble_length{};
};

}
