// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/RF_Fast_Ack_info.h>

#include <cassert>

namespace KNX {

RF_Fast_Ack_info::RF_Fast_Ack_info() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::RF_Fast_Ack_info;
    // m_length = 2; or more
}

void RF_Fast_Ack_info::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    fast_ack.assign(first, last);
}

std::vector<uint8_t> RF_Fast_Ack_info::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    for (const uint16_t fa : fast_ack) {
        data.push_back(fa >> 8);
        data.push_back(fa & 0xff);
    }

    return data;
}

uint8_t RF_Fast_Ack_info::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        static_cast<uint8_t>(2 * fast_ack.size());
}

}
