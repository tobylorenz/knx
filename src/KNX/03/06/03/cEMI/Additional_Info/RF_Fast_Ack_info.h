// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Additional_Info.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF Fast Ack information
 *
 * @ingroup KNX_03_06_03_04_01_04_03_07
 */
class KNX_EXPORT RF_Fast_Ack_info : public Additional_Info
{
public:
    RF_Fast_Ack_info();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Fast Ack */
    std::vector<uint16_t> fast_ack{};
};

}
