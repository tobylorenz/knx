// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/RF_Multi_info.h>

#include <cassert>

namespace KNX {

RF_Multi_info::RF_Multi_info() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::RF_Multi_info;
    length = 4;
}

void RF_Multi_info::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    transmission_frequency = *first++;

    call_channel = *first++;

    fast_ack = *first++;

    reception_frequency = *first++;

    assert(first == last);
}

std::vector<uint8_t> RF_Multi_info::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back(transmission_frequency);
    data.push_back(call_channel);
    data.push_back(fast_ack);
    data.push_back(reception_frequency);

    return data;
}

uint8_t RF_Multi_info::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        4;
}

}
