// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/RF_medium_info.h>

#include <cassert>

namespace KNX {

RF_medium_info::RF_medium_info() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::RF_medium_info;
    length = 8;
}

void RF_medium_info::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 10);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    rf_info = *first++;

    std::copy(first, first + 6, std::begin(serial_number_doa));
    first += 6;

    link_frame_number = *first++;

    assert(first == last);
}

std::vector<uint8_t> RF_medium_info::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back(rf_info);
    data.insert(std::cend(data), std::cbegin(serial_number_doa), std::cend(serial_number_doa));
    data.push_back(link_frame_number);

    return data;
}

uint8_t RF_medium_info::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        8;
}

}
