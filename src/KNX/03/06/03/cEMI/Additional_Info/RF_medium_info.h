// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/06/03/cEMI/Additional_Info.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * RF medium information
 *
 * @ingroup KNX_03_06_03_04_01_04_03_02
 */
class KNX_EXPORT RF_medium_info : public Additional_Info
{
public:
    RF_medium_info();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Info */
    uint8_t rf_info{};

    /** Serial Number / Domain Address */
    std::array<uint8_t, 6> serial_number_doa{};

    /** Link Frame Number */
    uint8_t link_frame_number{};
};

}
