// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/Time_delay_until_sending.h>

#include <cassert>

namespace KNX {

Time_delay_until_sending::Time_delay_until_sending() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::Time_delay_until_sending;
    length = 2;
}

void Time_delay_until_sending::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    time_delay_until_sending =
        (*first++ << 24) |
        (*first++ << 16) |
        (*first++ << 8) |
        (*first++);

    assert(first == last);
}

std::vector<uint8_t> Time_delay_until_sending::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back((time_delay_until_sending >> 24) & 0xff);
    data.push_back((time_delay_until_sending >> 16) & 0xff);
    data.push_back((time_delay_until_sending >> 8) & 0xff);
    data.push_back(time_delay_until_sending & 0xff);

    return data;
}

uint8_t Time_delay_until_sending::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        4;
}

}
