// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Additional_Info.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Time delay until sending
 *
 * @ingroup KNX_03_06_03_04_01_05_07_03
 */
class KNX_EXPORT Time_delay_until_sending : public Additional_Info
{
public:
    Time_delay_until_sending();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Time Delay until sending */
    uint32_t time_delay_until_sending{};
};

}
