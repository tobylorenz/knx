// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Additional_Info/Timestamp_relative.h>

#include <cassert>

namespace KNX {

Timestamp_relative::Timestamp_relative() :
    Additional_Info()
{
    type_id = Additional_Info::TypeId::Timestamp_relative;
    length = 2;
}

void Timestamp_relative::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    Additional_Info::fromData(first, first + 2);
    first += 2;

    timestamp_relative = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> Timestamp_relative::toData() const
{
    std::vector<uint8_t> data = Additional_Info::toData();

    data.push_back(timestamp_relative >> 8);
    data.push_back(timestamp_relative & 0xff);

    return data;
}

uint8_t Timestamp_relative::length_calculated() const
{
    return
        Additional_Info::length_calculated() +
        2;
}

}
