// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Additional_Info.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Timestamp relative
 *
 * @ingroup KNX_03_06_03_04_01_05_07_06
 */
class KNX_EXPORT Timestamp_relative : public Additional_Info
{
public:
    Timestamp_relative();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t length_calculated() const override;

    /** Timestamp Relative */
    uint16_t timestamp_relative{};
};

}
