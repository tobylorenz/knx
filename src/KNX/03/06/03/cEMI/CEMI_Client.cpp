// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/CEMI_Client.h>

#include <cassert>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Busmon/CEMI_L_Busmon_ind.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_con.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_ind.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_req.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_con.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_req.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_con.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_ind.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_req.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_con.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_req.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropStateRead/CEMI_M_FuncPropStateRead_con.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropStateRead/CEMI_M_FuncPropStateRead_req.h>
#include <KNX/03/06/03/cEMI/Management/M_PropInfo/CEMI_M_PropInfo_ind.h>
#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_con.h>
#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_req.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_con.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_req.h>
#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset_ind.h>
#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset_req.h>
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Connected/CEMI_T_Data_Connected_ind.h>
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Connected/CEMI_T_Data_Connected_req.h>
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Individual/CEMI_T_Data_Individual_ind.h>
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Individual/CEMI_T_Data_Individual_req.h>

namespace KNX {

CEMI_Client::CEMI_Client(asio::io_context & io_context) :
    io_context(io_context),
    network_layer(),
    transport_layer(network_layer, io_context),
    application_layer(transport_layer, io_context),
    management_client()
{
    // interface objects
    device_object = management_client.interface_objects->device();
    (void) device_object->interface_object_type();
    (void) device_object->routing_count();
    group_address_table = management_client.interface_objects->group_address_table();
    (void) group_address_table->interface_object_type();
    std::shared_ptr<Group_Object_Association_Table> group_object_association_table = management_client.interface_objects->group_object_association_table();
    (void) group_object_association_table->interface_object_type();

    /* network layer */
    network_layer.device_object = device_object;
    network_layer.connect(*this);

    /* transport layer */
    transport_layer.group_address_table = group_address_table;

    /* application layer */
    application_layer.group_object_association_table = group_object_association_table;

    // management client
    management_client.M_PropRead_req = std::bind(&CEMI_Client::M_PropRead_req, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    this->M_PropRead_con = std::bind(&CEMI_Management_Client::M_PropRead_con, &management_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    management_client.M_PropWrite_req = std::bind(&CEMI_Client::M_PropWrite_req, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    this->M_PropWrite_con = std::bind(&CEMI_Management_Client::M_PropWrite_con, &management_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    this->M_PropInfo_ind = std::bind(&CEMI_Management_Client::M_PropInfo_ind, &management_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    management_client.M_FuncPropCommand_req = std::bind(&CEMI_Client::M_FuncPropCommand_req, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    this->M_FuncPropCommand_con = std::bind(&CEMI_Management_Client::M_FuncPropCommand_con, &management_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    management_client.M_FuncPropStateRead_req = std::bind(&CEMI_Client::M_FuncPropStateRead_req, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    this->M_FuncPropStateRead_con = std::bind(&CEMI_Management_Client::M_FuncPropStateRead_con, &management_client, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    management_client.M_Reset_req = std::bind(&CEMI_Client::M_Reset_req, this);
    this->M_Reset_ind = std::bind(&CEMI_Management_Client::M_Reset_ind, &management_client);
}

void CEMI_Client::cemi_con_ind(const std::vector<uint8_t> cemi_frame_data)
{
    const CEMI_Message_Code message_code = static_cast<CEMI_Message_Code>(cemi_frame_data.front());
    switch (message_code) {
    case CEMI_Message_Code::UNDEFINED:
        assert(false);
        break;
    case CEMI_Message_Code::L_Busmon_ind:
        cemi_ind(cemi_frame_data);
        break;
    case CEMI_Message_Code::L_Data_req:
        assert(false);
        break;
    case CEMI_Message_Code::L_Data_con:
        cemi_con(cemi_frame_data);
        break;
    case CEMI_Message_Code::L_Data_ind:
        cemi_ind(cemi_frame_data);
        break;
    case CEMI_Message_Code::L_Raw_req:
        assert(false);
        break;
    case CEMI_Message_Code::L_Raw_con:
        cemi_con(cemi_frame_data);
        break;
    case CEMI_Message_Code::L_Raw_ind:
        cemi_ind(cemi_frame_data);
        break;
    case CEMI_Message_Code::L_Poll_Data_req:
        assert(false);
        break;
    case CEMI_Message_Code::L_Poll_Data_con:
        cemi_con(cemi_frame_data);
        break;
    case CEMI_Message_Code::T_Data_Connected_req:
        assert(false);
        break;
    case CEMI_Message_Code::T_Data_Connected_ind:
        cemi_ind(cemi_frame_data);
        break;
    case CEMI_Message_Code::T_Data_Individual_req:
        assert(false);
        break;
    case CEMI_Message_Code::T_Data_Individual_ind:
        cemi_ind(cemi_frame_data);
        break;
    case CEMI_Message_Code::M_PropRead_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropRead_con:
        cemi_con(cemi_frame_data);
        break;
    case CEMI_Message_Code::M_PropWrite_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropWrite_con:
        cemi_con(cemi_frame_data);
        break;
    case CEMI_Message_Code::M_PropInfo_ind:
        cemi_ind(cemi_frame_data);
        break;
    case CEMI_Message_Code::M_FuncPropCommand_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_FuncPropStateRead_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_FuncPropCommand_con: // = M_FuncPropStateRead_con
        cemi_con(cemi_frame_data);
        break;
    case CEMI_Message_Code::M_Reset_ind:
        cemi_ind(cemi_frame_data);
        break;
    case CEMI_Message_Code::M_Reset_req:
        assert(false);
        break;
    }
}

void CEMI_Client::cemi_con(const std::vector<uint8_t> data)
{
    std::shared_ptr<CEMI_Message> frame = make_CEMI_Message(std::cbegin(data), std::cend(data));
    switch (frame->message_code) {
    case CEMI_Message_Code::UNDEFINED:
        assert(false);
        break;
    case CEMI_Message_Code::L_Busmon_ind:
        assert(false);
        break;
    case CEMI_Message_Code::L_Data_req:
        assert(false);
        break;
    case CEMI_Message_Code::L_Data_con:
        if (L_Data_con) {
            std::shared_ptr<CEMI_L_Data_con> frame2 = std::dynamic_pointer_cast<CEMI_L_Data_con>(frame);
            Frame_Format frame_format;
            frame_format.frame_type_parameter = to_frame_type_parameter(frame2->control.frame_type);
            const Status l_status = (frame2->control.confirm == Confirm::no_error) ? Status::ok : Status::not_ok;
            L_Data_con(frame2->extended_control.address_type, frame2->destination_address, frame_format, frame2->control.priority, frame2->source_address, frame2->lsdu, l_status);
        }
        break;
    case CEMI_Message_Code::L_Data_ind:
        assert(false);
        break;
    case CEMI_Message_Code::L_Raw_req:
        assert(false);
        break;
    case CEMI_Message_Code::L_Raw_con:
        if (L_Plain_Data_con) {
            std::shared_ptr<CEMI_L_Raw_con> frame2 = std::dynamic_pointer_cast<CEMI_L_Raw_con>(frame);
            L_Plain_Data_con(frame2->data, Status::ok);
        }
        break;
    case CEMI_Message_Code::L_Raw_ind:
        assert(false);
        break;
    case CEMI_Message_Code::L_Poll_Data_req:
        assert(false);
        break;
    case CEMI_Message_Code::L_Poll_Data_con:
        if (L_Poll_Data_con) {
            std::shared_ptr<CEMI_L_Poll_Data_con> frame2 = std::dynamic_pointer_cast<CEMI_L_Poll_Data_con>(frame);
            const Status l_status = (frame2->control.confirm == Confirm::no_error) ? Status::ok : Status::not_ok;
            L_Poll_Data_con(Group_Address(frame2->destination_address), frame2->poll_data, l_status);
        }
        break;
    case CEMI_Message_Code::T_Data_Connected_req:
        assert(false);
        break;
    case CEMI_Message_Code::T_Data_Connected_ind:
        assert(false);
        break;
    case CEMI_Message_Code::T_Data_Individual_req:
        assert(false);
        break;
    case CEMI_Message_Code::T_Data_Individual_ind:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropRead_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropRead_con:
        if (M_PropRead_con) {
            std::shared_ptr<CEMI_M_PropRead_con> frame2 = std::dynamic_pointer_cast<CEMI_M_PropRead_con>(frame);
            M_PropRead_con(frame2->interface_object_type, frame2->object_instance, frame2->property_id, frame2->data, frame2->error_code);
        }
        break;
    case CEMI_Message_Code::M_PropWrite_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropWrite_con:
        if (M_PropWrite_con) {
            std::shared_ptr<CEMI_M_PropWrite_con> frame2 = std::dynamic_pointer_cast<CEMI_M_PropWrite_con>(frame);
            M_PropWrite_con(frame2->interface_object_type, frame2->object_instance, frame2->property_id, frame2->error_code);
        }
        break;
    case CEMI_Message_Code::M_PropInfo_ind:
        assert(false);
        break;
    case CEMI_Message_Code::M_FuncPropCommand_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_FuncPropStateRead_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_FuncPropCommand_con: // = M_FuncPropStateRead_con
        if (M_FuncPropCommand_con) {
            std::shared_ptr<CEMI_M_FuncPropCommand_con> frame2 = std::dynamic_pointer_cast<CEMI_M_FuncPropCommand_con>(frame);
            M_FuncPropCommand_con(frame2->interface_object_type, frame2->object_instance, frame2->property_id, frame2->return_code, frame2->data);
        }
        break;
    case CEMI_Message_Code::M_Reset_ind:
        assert(false);
        break;
    case CEMI_Message_Code::M_Reset_req:
        assert(false);
        break;
    }
}

void CEMI_Client::cemi_ind(const std::vector<uint8_t> data)
{
    std::shared_ptr<CEMI_Message> frame = make_CEMI_Message(std::cbegin(data), std::cend(data));
    switch (frame->message_code) {
    case CEMI_Message_Code::UNDEFINED:
        assert(false);
        break;
    case CEMI_Message_Code::L_Busmon_ind:
        if (L_Busmon_ind) {
            std::shared_ptr<CEMI_L_Busmon_ind> frame2 = std::dynamic_pointer_cast<CEMI_L_Busmon_ind>(frame);
            const Status l_status{Status::ok}; // @todo l_status
            const uint16_t time_stamp{}; // @todo time_stamp
            L_Busmon_ind(l_status, time_stamp, frame2->data);
        }
        break;
    case CEMI_Message_Code::L_Data_req:
        assert(false);
        break;
    case CEMI_Message_Code::L_Data_con:
        assert(false);
        break;
    case CEMI_Message_Code::L_Data_ind:
        if (L_Data_ind) {
            std::shared_ptr<CEMI_L_Data_ind> frame2 = std::dynamic_pointer_cast<CEMI_L_Data_ind>(frame);
            Frame_Format frame_format;
            frame_format.frame_type_parameter = to_frame_type_parameter(frame2->control.frame_type);
            L_Data_ind(frame2->control.acknowledge, frame2->extended_control.address_type, frame2->destination_address, frame_format, frame2->lsdu, frame2->control.priority, frame2->source_address);
        }
        break;
    case CEMI_Message_Code::L_Raw_req:
        assert(false);
        break;
    case CEMI_Message_Code::L_Raw_con:
        assert(false);
        break;
    case CEMI_Message_Code::L_Raw_ind:
        if (L_Plain_Data_ind) {
            const Status l_status{Status::ok}; // @todo l_status
            std::shared_ptr<CEMI_L_Raw_ind> frame2 = std::dynamic_pointer_cast<CEMI_L_Raw_ind>(frame);
            L_Plain_Data_ind(frame2->data, l_status);
        }
        if (L_Busmon_ind) {
            const Status l_status{Status::ok}; // @todo l_status
            const uint16_t time_stamp{}; // @todo time_stamp
            std::shared_ptr<CEMI_L_Raw_ind> frame2 = std::dynamic_pointer_cast<CEMI_L_Raw_ind>(frame);
            L_Busmon_ind(l_status, time_stamp, frame2->data);
        }
        break;
    case CEMI_Message_Code::L_Poll_Data_req:
        assert(false);
        break;
    case CEMI_Message_Code::L_Poll_Data_con:
        assert(false);
        break;
    case CEMI_Message_Code::T_Data_Connected_req:
        assert(false);
        break;
    case CEMI_Message_Code::T_Data_Connected_ind:
        if (transport_layer.T_Data_Connected_ind) {
            //std::shared_ptr<CEMI_T_Data_Connected_ind> frame2 = std::dynamic_pointer_cast<CEMI_T_Data_Connected_ind>(frame);
            // @todo transport_layer.T_Data_Connected_ind(hop_count_type, priority, tsap, frame2->tsdu);
        }
        break;
    case CEMI_Message_Code::T_Data_Individual_req:
        assert(false);
        break;
    case CEMI_Message_Code::T_Data_Individual_ind:
        if (transport_layer.T_Data_Individual_ind) {
            //std::shared_ptr<CEMI_T_Data_Individual_ind> frame2 = std::dynamic_pointer_cast<CEMI_T_Data_Individual_ind>(frame);
            // @todo transport_layer.T_Data_Individual_ind(hop_count_type, priority, tsap, frame2->tsdu);
        }
        break;
    case CEMI_Message_Code::M_PropRead_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropRead_con:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropWrite_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropWrite_con:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropInfo_ind:
        if (M_PropInfo_ind) {
            std::shared_ptr<CEMI_M_PropInfo_ind> frame2 = std::dynamic_pointer_cast<CEMI_M_PropInfo_ind>(frame);
            M_PropInfo_ind(frame2->interface_object_type, frame2->object_instance, frame2->property_id, frame2->data);
        }
        break;
    case CEMI_Message_Code::M_FuncPropCommand_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_FuncPropStateRead_req:
        assert(false);
        break;
    case CEMI_Message_Code::M_FuncPropCommand_con: // = M_FuncPropStateRead_con
        assert(false);
        break;
    case CEMI_Message_Code::M_Reset_ind:
        if (M_Reset_ind) {
            //std::shared_ptr<CEMI_M_Reset_ind> frame2 = std::dynamic_pointer_cast<CEMI_M_Reset_ind>(frame);
            M_Reset_ind();
        }
        break;
    case CEMI_Message_Code::M_Reset_req:
        assert(false);
        break;
    }
}

/* Data Link Layer */

void CEMI_Client::L_Data_req(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)
{
    if (cemi_req) {
        CEMI_L_Data_req frame;
        frame.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
        frame.control.priority = priority;
        frame.control.acknowledge = ack_request;
        frame.extended_control.address_type = address_type;
        if (source_address == 0) {
            assert(group_address_table);
            frame.source_address = group_address_table->individual_address;
        } else {
            frame.source_address = source_address;
        }
        frame.destination_address = destination_address;
        frame.lsdu = lsdu;
        cemi_req(frame.toData());
    }
}

void CEMI_Client::L_SystemBroadcast_req(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority)
{
    if (cemi_req) {
        assert(group_address_table);
        CEMI_L_Data_req frame;
        frame.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
        frame.control.system_broadcast = System_Broadcast::System_Broadcast;
        frame.control.priority = priority;
        frame.control.acknowledge = ack_request;
        frame.extended_control.address_type = Address_Type::Group;
        frame.source_address = group_address_table->individual_address;
        frame.destination_address = destination_address;
        frame.lsdu = lsdu;
        cemi_req(frame.toData());
    }
}

void CEMI_Client::L_Poll_Data_req(const Group_Address destination_address, const uint8_t no_of_expected_poll_data)
{
    if (cemi_req) {
        CEMI_L_Poll_Data_req frame;
        frame.destination_address = destination_address;
        frame.number_of_slots = no_of_expected_poll_data;
        cemi_req(frame.toData());
    }
}

void CEMI_Client::L_Poll_Update_req(const uint8_t poll_data)
{
    this->poll_data = poll_data;
}

void CEMI_Client::L_Plain_Data_req(const uint32_t /*time*/, const std::vector<uint8_t> data)
{
    if (cemi_req) {
        CEMI_L_Raw_req frame;
        frame.data = data;
        cemi_req(frame.toData());
    }
}

/* Management Client */

void CEMI_Client::M_PropRead_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id)
{
    if (cemi_req) {
        CEMI_M_PropRead_req frame;
        frame.interface_object_type = interface_object_type;
        frame.object_instance = object_instance;
        frame.property_id = property_id;
        cemi_req(frame.toData());
    }
}

void CEMI_Client::M_PropWrite_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)
{
    if (cemi_req) {
        CEMI_M_PropWrite_req frame;
        frame.interface_object_type = interface_object_type;
        frame.object_instance = object_instance;
        frame.property_id = property_id;
        frame.data = data;
        cemi_req(frame.toData());
    }
}

void CEMI_Client::M_FuncPropCommand_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)
{
    if (cemi_req) {
        CEMI_M_FuncPropCommand_req frame;
        frame.interface_object_type = interface_object_type;
        frame.object_instance = object_instance;
        frame.property_id = property_id;
        frame.data = data;
        cemi_req(frame.toData());
    }
}

void CEMI_Client::M_FuncPropStateRead_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)
{
    if (cemi_req) {
        CEMI_M_FuncPropStateRead_req frame;
        frame.interface_object_type = interface_object_type;
        frame.object_instance = object_instance;
        frame.property_id = property_id;
        frame.data = data;
        cemi_req(frame.toData());
    }
}

void CEMI_Client::M_Reset_req()
{
    if (cemi_req) {
        CEMI_M_Reset_req frame;
        cemi_req(frame.toData());
    }
}

}
