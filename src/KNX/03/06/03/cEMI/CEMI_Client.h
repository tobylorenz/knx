// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Data_Link_Layer.h>
#include <KNX/03/03/03_Network_Layer.h>
#include <KNX/03/03/04_Transport_Layer.h>
#include <KNX/03/03/07_Application_Layer.h>
#include <KNX/03/06/03/cEMI/CEMI_Management_Client.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * cEMI Client
 *
 * @ingroup KNX_03_06_03_04_01_02
 * @ingroup KNX_03_06_03_04_01_07
 */
class KNX_EXPORT CEMI_Client :
    public Data_Link_Layer
{
public:
    explicit CEMI_Client(asio::io_context & io_context);

    /* command interface */
    std::function<void(const std::vector<uint8_t> data)> cemi_req;
    virtual void cemi_con_ind(const std::vector<uint8_t> data);
    virtual void cemi_con(const std::vector<uint8_t> data);
    virtual void cemi_ind(const std::vector<uint8_t> data);

    /** IO context */
    asio::io_context & io_context;

    /** Network Layer */
    Network_Layer network_layer;

    /** Transport Layer */
    Transport_Layer transport_layer;

    /** Application Layer */
    Application_Layer application_layer;

    /** cEMI Management Client */
    CEMI_Management_Client management_client;

protected:
    /* Data Link Layer */
    virtual void L_Data_req(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address) override;
    virtual void L_SystemBroadcast_req(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority) override;
    virtual void L_Poll_Data_req(const Group_Address destination_address, const uint8_t no_of_expected_poll_data) override;
    virtual void L_Poll_Update_req(const uint8_t poll_data) override;
    virtual void L_Plain_Data_req(const uint32_t time_stamp, const std::vector<uint8_t> data) override;

    /* Management Client */
    virtual void M_PropRead_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id);
    std::function<void(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const std::optional<CEMI_Error_Code> error_code)> M_PropRead_con;
    virtual void M_PropWrite_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data);
    std::function<void(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const std::optional<CEMI_Error_Code> error_code)> M_PropWrite_con;
    std::function<void(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)> M_PropInfo_ind;
    virtual void M_FuncPropCommand_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data);
    std::function<void(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, const Property_Value  data)> M_FuncPropCommand_con;
    virtual void M_FuncPropStateRead_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data);
    std::function<void(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, const Property_Value  data)> M_FuncPropStateRead_con;
    virtual void M_Reset_req();
    std::function<void()> M_Reset_ind;
};

}
