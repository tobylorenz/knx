// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/CEMI_Management_Client.h>

#include <optional>

#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_req.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropStateRead/CEMI_M_FuncPropStateRead_req.h>
#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_req.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_req.h>
#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset_req.h>

namespace KNX {

CEMI_Management_Client::CEMI_Management_Client() :
    interface_objects(std::make_shared<Interface_Objects>())
{
}

void CEMI_Management_Client::M_PropRead_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const std::optional<CEMI_Error_Code> error_code)
{
    // @todo CEMI_Management_Client::M_PropRead_con
}

void CEMI_Management_Client::M_PropWrite_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const std::optional<CEMI_Error_Code> error_code)
{
    // @todo CEMI_Management_Client::M_PropWrite_con
}

void CEMI_Management_Client::M_PropInfo_ind(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)
{
    // @todo CEMI_Management_Client::M_PropInfo_ind
}

void CEMI_Management_Client::M_FuncPropCommand_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, const Property_Value  data)
{
    // @todo CEMI_Management_Client::M_FuncPropCommand_con
}

void CEMI_Management_Client::M_FuncPropStateRead_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, const Property_Value  data)
{
    // @todo CEMI_Management_Client::M_FuncPropStateRead_con
}

void CEMI_Management_Client::M_Reset_ind()
{
    // @todo CEMI_Management_Client::M_Reset_ind
}

}
