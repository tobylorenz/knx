// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>
#include <optional>
#include <vector>

#include <KNX/03/03/07/Object_Type.h>
#include <KNX/03/03/07/Property_Value.h>
#include <KNX/03/03/07/Property_Id.h>
#include <KNX/03/05/01/Interface_Objects.h>
#include <KNX/03/06/03/cEMI/Management/CEMI_Error_Codes.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * cEMI Management Client
 *
 * @ingroup KNX_03_06_03_04_01_02
 */
class KNX_EXPORT CEMI_Management_Client
{
public:
    explicit CEMI_Management_Client();

    /** resources */
    std::shared_ptr<Interface_Objects> interface_objects;

    std::function<void (const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id)> M_PropRead_req; // @todo make this async functions req/con
    virtual void M_PropRead_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data, const std::optional<CEMI_Error_Code> error_code);

    std::function<void (const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)> M_PropWrite_req; // @todo make this async functions req/con
    virtual void M_PropWrite_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const std::optional<CEMI_Error_Code> error_code);

    virtual void M_PropInfo_ind(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data); // @todo make this async functions ind

    std::function<void (const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)> M_FuncPropCommand_req; // @todo make this async functions req/con
    virtual void M_FuncPropCommand_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, const Property_Value  data);

    std::function<void (const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)> M_FuncPropStateRead_req; // @todo make this async functions req/con
    virtual void M_FuncPropStateRead_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const std::optional<Property_Return_Code> return_code, const Property_Value  data);

    std::function<void ()> M_Reset_req;
    virtual void M_Reset_ind();
};

}
