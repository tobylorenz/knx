// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/CEMI_Management_Server.h>

#include <KNX/03/05/01/08_CEMI_Server/CEMI_Server_Object.h>
#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_con.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropStateRead/CEMI_M_FuncPropStateRead_con.h>
#include <KNX/03/06/03/cEMI/Management/M_PropInfo/CEMI_M_PropInfo_ind.h>
#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_con.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_con.h>
#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset_ind.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

CEMI_Management_Server::CEMI_Management_Server()
{
}

void CEMI_Management_Server::M_PropRead_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index)
{
    if (!M_PropRead_con) {
        return;
    }

    /* check if object is valid */
    std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_type_instance(interface_object_type, object_instance);
    if (!interface_object) {
        M_PropRead_con(interface_object_type, object_instance, property_id, 0, start_index, {}, CEMI_Error_Code::Void_DP);
        return;
    }

    /* check if property is valid */
    std::shared_ptr<Property> property = interface_object->property_by_id(property_id);
    if (!property) {
        M_PropRead_con(interface_object_type, object_instance, property_id, 0, start_index, {}, CEMI_Error_Code::Void_DP);
        return;
    }

    /* check if property is a Data Property */
    std::shared_ptr<Data_Property> data_property = std::dynamic_pointer_cast<Data_Property>(property);
    if (!data_property) {
        M_PropRead_con(interface_object_type, object_instance, property_id, 0, start_index, {}, CEMI_Error_Code::Void_DP);
        return;
    }

    /* check if start index is valid */
    if (start_index - 1 >= data_property->nr_of_elem) {
        M_PropRead_con(interface_object_type, object_instance, property_id, 0, start_index, {}, CEMI_Error_Code::Prop_Index_Range_Error);
        return;
    }

    /* check if count is valid */
    if (start_index - 1 + nr_of_elem > data_property->nr_of_elem) {
        M_PropRead_con(interface_object_type, object_instance, property_id, 0, start_index, {}, CEMI_Error_Code::Prop_Index_Range_Error);
        return;
    }

    /* index = 0: return max. number of elements */
    if (start_index == 0) {
        std::vector<uint8_t> data{};
        data.push_back(data_property->nr_of_elem >> 8);
        data.push_back(data_property->nr_of_elem & 0xff);
        M_PropRead_con(interface_object_type, object_instance, property_id, 1, start_index, data, std::nullopt);
        return;
    }

    /* read */
    const uint8_t property_datatype_size = (data_property->description.property_datatype == PDT_CONTROL) ? 1 : size(property->description.property_datatype);
    std::vector<uint8_t> data = data_property->toData();
    std::vector<uint8_t> return_data;
    return_data.assign(std::cbegin(data) + (start_index - 1) * property_datatype_size,
                       std::cbegin(data) + (start_index - 1 + nr_of_elem) * property_datatype_size);
    M_PropRead_con(interface_object_type, object_instance, property_id, nr_of_elem, start_index, return_data, std::nullopt);
}

void CEMI_Management_Server::M_PropWrite_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data)
{
    /* check if object is valid */
    std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_type_instance(interface_object_type, object_instance);
    if (!interface_object) {
        if (M_PropWrite_con) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Void_DP);
        }
        return;
    }

    /* check if property is valid */
    std::shared_ptr<Property> property = interface_object->property_by_id(property_id);
    if (!property) {
        if (M_PropWrite_con) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Void_DP);
        }
        return;
    }

    /* check if property is a Data Property */
    std::shared_ptr<Data_Property> data_property = std::dynamic_pointer_cast<Data_Property>(property);
    if (!data_property) {
        if (M_PropWrite_con) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Void_DP);
        }
        return;
    }

    /* check if start index is valid */
    if (start_index - 1 >= data_property->description.max_nr_of_elem) {
        if (M_PropWrite_con) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Prop_Index_Range_Error);
        }
        return;
    }

    /* check if count is valid */
    if (start_index - 1 + nr_of_elem > data_property->description.max_nr_of_elem) {
        if (M_PropWrite_con) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Prop_Index_Range_Error);
        }
        return;
    }

    /* write enable? */
    if (!data_property->description.write_enable) {
        if (M_PropWrite_con) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Read_Only);
        }
        return;
    }

    /* authorization level? */
    if (data_property->description.access.write_level < *current_level) {
        if (M_PropWrite_con) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Read_Only);
            return;
        }
    }

    /* index = 0: write max. number of elements */
    if (start_index == 0) {
        /* check data size is 16-bit */
        if (data.size() != 2) {
            if (M_PropWrite_con) {
                M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Prop_Index_Range_Error);
            }
            return;
        }

        const PropertyValue_Nr_Of_Elem new_nr_of_elem = (data[0] << 8) | (data[1]);

        /* check if new nr_of_elem is valid */
        if (new_nr_of_elem > data_property->description.max_nr_of_elem) {
            if (M_PropWrite_con) {
                M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Unspecified_Error);
            }
            return;
        }

        data_property->nr_of_elem = new_nr_of_elem;

        if (M_PropWrite_con) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, nr_of_elem, start_index, std::nullopt);
        }
        return;
    }

    /* write */
    if (data_property->description.property_datatype == PDT_CONTROL) {
        if (data.size() != 10) {
            if (M_PropWrite_con) {
                M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, CEMI_Error_Code::Unspecified_Error);
            }
            return;
        }

        data_property->fromData(std::cbegin(data), std::cend(data));
        if (M_PropWrite_con) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, nr_of_elem, start_index, std::nullopt);
        }
        return;
    }

    std::optional<CEMI_Error_Code> error_code;
    data_property->write(nr_of_elem, start_index, data, error_code);

    if (M_PropWrite_con) {
        if (error_code.has_value()) {
            M_PropWrite_con(interface_object_type, object_instance, property_id, 0, start_index, error_code);
        } else {
            M_PropWrite_con(interface_object_type, object_instance, property_id, nr_of_elem, start_index, std::nullopt);
        }
    }
}

void CEMI_Management_Server::M_FuncPropCommand_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)
{
    /* check if object is valid */
    std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_type_instance(interface_object_type, object_instance);
    if (!interface_object) {
        if (M_FuncPropCommand_con) {
            M_FuncPropCommand_con(interface_object_type, object_instance, property_id, std::nullopt, {});
        }
        return;
    }

    /* check if property is valid */
    std::shared_ptr<Property> property = interface_object->property_by_id(property_id);
    if (!property) {
        if (M_FuncPropCommand_con) {
            M_FuncPropCommand_con(interface_object_type, object_instance, property_id, std::nullopt, {});
        }
        return;
    }

    /* check if property is a Function Property */
    std::shared_ptr<Function_Property> function_property = std::dynamic_pointer_cast<Function_Property>(property);
    if (!function_property) {
        if (M_FuncPropCommand_con) {
            M_FuncPropCommand_con(interface_object_type, object_instance, property_id, std::nullopt, {});
        }
        return;
    }

    std::optional<Property_Return_Code> return_code{};
    Property_Value  output_data{};
    if (function_property->command) {
        function_property->command(data, return_code, output_data);
    }
    if (M_FuncPropCommand_con) {
        M_FuncPropCommand_con(interface_object_type, object_instance, property_id, return_code, output_data);
    }
}

void CEMI_Management_Server::M_FuncPropStateRead_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)
{
    /* check if object is valid */
    std::shared_ptr<Interface_Object> interface_object = interface_objects->object_by_type_instance(interface_object_type, object_instance);
    if (!interface_object) {
        if (M_FuncPropStateRead_con) {
            M_FuncPropStateRead_con(interface_object_type, object_instance, property_id, std::nullopt, {});
        }
        return;
    }

    /* check if property is valid */
    std::shared_ptr<Property> property = interface_object->property_by_id(property_id);
    if (!property) {
        if (M_FuncPropStateRead_con) {
            M_FuncPropStateRead_con(interface_object_type, object_instance, property_id, std::nullopt, {});
        }
        return;
    }

    /* check if property is a Function Property */
    std::shared_ptr<Function_Property> function_property = std::dynamic_pointer_cast<Function_Property>(property);
    if (!function_property) {
        if (M_FuncPropStateRead_con) {
            M_FuncPropStateRead_con(interface_object_type, object_instance, property_id, std::nullopt, {});
        }
        return;
    }

    std::optional<Property_Return_Code> return_code{};
    Property_Value  output_data{};
    if (function_property->state_read) {
        function_property->state_read(data, return_code, output_data);
    }
    if (M_FuncPropStateRead_con) {
        M_FuncPropStateRead_con(interface_object_type, object_instance, property_id, return_code, output_data);
    }
}

void CEMI_Management_Server::M_Reset_req()
{
    // @todo CEMI_Management_Server::M_Reset_req
    if (M_Reset_ind) {
        M_Reset_ind();
    }
}

}
