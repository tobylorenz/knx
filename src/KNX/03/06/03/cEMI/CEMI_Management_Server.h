// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <functional>
#include <memory>
#include <optional>
#include <vector>

#include <KNX/03/03/07/Level.h>
#include <KNX/03/03/07/Object_Type.h>
#include <KNX/03/03/07/Property_Value.h>
#include <KNX/03/03/07/Property_Id.h>
#include <KNX/03/03/07/Property_Return_Code.h>
#include <KNX/03/05/01/Interface_Objects.h>
#include <KNX/03/06/03/cEMI/Management/CEMI_Error_Codes.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * cEMI Management Server
 *
 * @ingroup KNX_03_06_03_04_01_02
 *
 * @note This has many similarities to Interface_Object_Server
 */
class KNX_EXPORT CEMI_Management_Server
{
public:
    explicit CEMI_Management_Server();

    /** resources */
    std::shared_ptr<Interface_Objects> interface_objects{};

    /** current level */
    std::shared_ptr<Level> current_level{};

    virtual void M_PropRead_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index); // @todo make this async functions req/con
    std::function<void (const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data, const std::optional<CEMI_Error_Code> error_code)> M_PropRead_con;

    virtual void M_PropWrite_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data); // @todo make this async functions req/con
    std::function<void (const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const std::optional<CEMI_Error_Code> error_code)> M_PropWrite_con;

    std::function<void (const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data)> M_PropInfo_ind; // @todo make this async functions ind

    virtual void M_FuncPropCommand_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data); // @todo make this async functions req/con
    std::function<void (const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, std::optional<Property_Return_Code> return_code, const Property_Value  data)> M_FuncPropCommand_con;

    virtual void M_FuncPropStateRead_req(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value data); // @todo make this async functions req/con
    std::function<void (const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, std::optional<Property_Return_Code> return_code, const Property_Value  data)> M_FuncPropStateRead_con;

    virtual void M_Reset_req();
    std::function<void ()> M_Reset_ind; // @todo make this async functions ind
};

}
