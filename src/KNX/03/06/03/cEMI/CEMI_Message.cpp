// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/CEMI_Message.h>

#include <cassert>
#include <numeric>

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Busmon/CEMI_L_Busmon_ind.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_con.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_ind.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_req.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_con.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_req.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_con.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_ind.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_req.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_con.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_req.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropStateRead/CEMI_M_FuncPropStateRead_con.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropStateRead/CEMI_M_FuncPropStateRead_req.h>
#include <KNX/03/06/03/cEMI/Management/M_PropInfo/CEMI_M_PropInfo_ind.h>
#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_con.h>
#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_req.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_con.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_req.h>
#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset_ind.h>
#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset_req.h>
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Connected/CEMI_T_Data_Connected_ind.h>
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Connected/CEMI_T_Data_Connected_req.h>
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Individual/CEMI_T_Data_Individual_ind.h>
#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Individual/CEMI_T_Data_Individual_req.h>

namespace KNX {

CEMI_Message::CEMI_Message()
{
}

CEMI_Message::CEMI_Message(const CEMI_Message_Code message_code) :
    message_code(message_code)
{
}

void CEMI_Message::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 1);

    message_code = static_cast<CEMI_Message_Code>(*first++);

    if (!is_Management_Message()) {
        additional_info_length = *first++;

        std::vector<uint8_t>::const_iterator end_of_additional_info = first + additional_info_length;
        while (first != end_of_additional_info) {
            std::shared_ptr<Additional_Info> ai = make_Additional_Info(first, end_of_additional_info);
            additional_info.push_back(ai);
            first += 2 + ai->length;
        }
    }

    // assert(first == end_of_additional_info); is already checked by above loop
}

std::vector<uint8_t> CEMI_Message::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(static_cast<uint8_t>(message_code));

    if (!is_Management_Message()) {
        data.push_back(additional_info_length_calculated());

        for (const auto & al : additional_info) {
            std::vector<uint8_t> ai_data = al->toData();
            data.insert(std::cend(data), std::cbegin(ai_data), std::cend(ai_data));
        }
    }

    return data;
}

bool CEMI_Message::is_Management_Message() const
{
    bool retval = false;
    switch (message_code) {
    case CEMI_Message_Code::UNDEFINED:
        assert(false);
        break;
    case CEMI_Message_Code::L_Busmon_ind:
    case CEMI_Message_Code::L_Data_req:
    case CEMI_Message_Code::L_Data_con:
    case CEMI_Message_Code::L_Data_ind:
    case CEMI_Message_Code::L_Raw_req:
    case CEMI_Message_Code::L_Raw_con:
    case CEMI_Message_Code::L_Raw_ind:
    case CEMI_Message_Code::L_Poll_Data_req:
    case CEMI_Message_Code::L_Poll_Data_con:
    case CEMI_Message_Code::T_Data_Connected_req:
    case CEMI_Message_Code::T_Data_Connected_ind:
    case CEMI_Message_Code::T_Data_Individual_req:
    case CEMI_Message_Code::T_Data_Individual_ind:
        retval = false;
        break;
    case CEMI_Message_Code::M_PropRead_req:
    case CEMI_Message_Code::M_PropRead_con:
    case CEMI_Message_Code::M_PropWrite_req:
    case CEMI_Message_Code::M_PropWrite_con:
    case CEMI_Message_Code::M_PropInfo_ind:
    case CEMI_Message_Code::M_FuncPropCommand_req:
    case CEMI_Message_Code::M_FuncPropStateRead_req:
    case CEMI_Message_Code::M_FuncPropCommand_con: // = M_FuncPropStateRead_con
    case CEMI_Message_Code::M_Reset_req:
    case CEMI_Message_Code::M_Reset_ind:
        retval = true;
        break;
    }
    return retval;
}

uint8_t CEMI_Message::additional_info_length_calculated() const
{
    return std::accumulate(
               std::cbegin(additional_info),
               std::cend(additional_info),
               0,
    [](uint8_t length, std::shared_ptr<Additional_Info> ai) {
        return std::move(length) + 2 + ai->length_calculated();
    });
}

std::shared_ptr<CEMI_Message> make_CEMI_Message(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 1);

    CEMI_Message_Code message_code = static_cast<CEMI_Message_Code>(*first);
    switch (message_code) {
    case CEMI_Message_Code::UNDEFINED:
        assert(false);
        break;
    case CEMI_Message_Code::L_Busmon_ind: {
        std::shared_ptr<CEMI_L_Busmon_ind> frame = std::make_shared<CEMI_L_Busmon_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::L_Data_req: {
        std::shared_ptr<CEMI_L_Data_req> frame = std::make_shared<CEMI_L_Data_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::L_Data_con: {
        std::shared_ptr<CEMI_L_Data_con> frame = std::make_shared<CEMI_L_Data_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::L_Data_ind: {
        std::shared_ptr<CEMI_L_Data_ind> frame = std::make_shared<CEMI_L_Data_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::L_Raw_req: {
        std::shared_ptr<CEMI_L_Raw_req> frame = std::make_shared<CEMI_L_Raw_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::L_Raw_con: {
        std::shared_ptr<CEMI_L_Raw_con> frame = std::make_shared<CEMI_L_Raw_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::L_Raw_ind: {
        std::shared_ptr<CEMI_L_Raw_ind> frame = std::make_shared<CEMI_L_Raw_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::L_Poll_Data_req: {
        std::shared_ptr<CEMI_L_Poll_Data_req> frame = std::make_shared<CEMI_L_Poll_Data_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::L_Poll_Data_con: {
        std::shared_ptr<CEMI_L_Poll_Data_con> frame = std::make_shared<CEMI_L_Poll_Data_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::T_Data_Connected_req: {
        std::shared_ptr<CEMI_T_Data_Connected_req> frame = std::make_shared<CEMI_T_Data_Connected_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::T_Data_Connected_ind: {
        std::shared_ptr<CEMI_T_Data_Connected_ind> frame = std::make_shared<CEMI_T_Data_Connected_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::T_Data_Individual_req: {
        std::shared_ptr<CEMI_T_Data_Individual_req> frame = std::make_shared<CEMI_T_Data_Individual_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::T_Data_Individual_ind: {
        std::shared_ptr<CEMI_T_Data_Individual_ind> frame = std::make_shared<CEMI_T_Data_Individual_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_PropRead_req: {
        std::shared_ptr<CEMI_M_PropRead_req> frame = std::make_shared<CEMI_M_PropRead_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_PropRead_con: {
        std::shared_ptr<CEMI_M_PropRead_con> frame = std::make_shared<CEMI_M_PropRead_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_PropWrite_req: {
        std::shared_ptr<CEMI_M_PropWrite_req> frame = std::make_shared<CEMI_M_PropWrite_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_PropWrite_con: {
        std::shared_ptr<CEMI_M_PropWrite_con> frame = std::make_shared<CEMI_M_PropWrite_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_PropInfo_ind: {
        std::shared_ptr<CEMI_M_PropInfo_ind> frame = std::make_shared<CEMI_M_PropInfo_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_FuncPropCommand_req: {
        std::shared_ptr<CEMI_M_FuncPropCommand_req> frame = std::make_shared<CEMI_M_FuncPropCommand_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_FuncPropStateRead_req: {
        std::shared_ptr<CEMI_M_FuncPropStateRead_req> frame = std::make_shared<CEMI_M_FuncPropStateRead_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_FuncPropCommand_con: { // = M_FuncPropStateRead_con
        std::shared_ptr<CEMI_M_FuncPropCommand_con> frame = std::make_shared<CEMI_M_FuncPropCommand_con>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_Reset_req: {
        std::shared_ptr<CEMI_M_Reset_req> frame = std::make_shared<CEMI_M_Reset_req>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    case CEMI_Message_Code::M_Reset_ind: {
        std::shared_ptr<CEMI_M_Reset_ind> frame = std::make_shared<CEMI_M_Reset_ind>();
        frame->fromData(first, last);
        return frame;
    }
    break;
    }

    std::shared_ptr<CEMI_Message> frame = std::make_shared<CEMI_Message>();
    frame->fromData(first, last);
    return frame;
}

}
