// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/06/03/cEMI/Additional_Info.h>
#include <KNX/03/06/03/cEMI/CEMI_Message_Code.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * cEMI Message
 *
 * @ingroup KNX_03_06_03_04_01_04_01
 */
class KNX_EXPORT CEMI_Message
{
public:
    CEMI_Message();
    explicit CEMI_Message(const CEMI_Message_Code message_code);

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /** Message Code */
    CEMI_Message_Code message_code{CEMI_Message_Code::UNDEFINED};

    /** is Management Message */
    virtual bool is_Management_Message() const;

    /** length of additional information (AddIL) */
    uint8_t additional_info_length{};

    virtual uint8_t additional_info_length_calculated() const;

    /** Additional Information */
    std::vector<std::shared_ptr<Additional_Info>> additional_info{};
};

KNX_EXPORT std::shared_ptr<CEMI_Message> make_CEMI_Message(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
