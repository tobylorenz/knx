// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * cEMI Message Codes
 *
 * @ingroup KNX_03_06_03_02
 */
enum class CEMI_Message_Code : uint8_t {
    UNDEFINED = 0x00,

    /* Data Link Layer */

    L_Busmon_ind = 0x2B, // L_Busmon.ind

    L_Data_req = 0x11, // L_Data.req
    L_Data_con = 0x2E, // L_Data.con
    L_Data_ind = 0x29, // L_Data.ind

    L_Raw_req = 0x10, // L_Raw.req
    L_Raw_con = 0x2F, // L_Raw.con
    L_Raw_ind = 0x2D, // L_Raw.ind

    L_Poll_Data_req = 0x13, // L_Poll_Data.req
    L_Poll_Data_con = 0x25, // L_Poll_Data.con

    /* Transport Layer */

    T_Data_Connected_req = 0x41, // T_Data_Connected.req
    T_Data_Connected_ind = 0x89, // T_Data_Connected.ind

    T_Data_Individual_req = 0x4A, // T_Data_Individual.req
    T_Data_Individual_ind = 0x94, // T_Data_Individual.ind

    /* Management */

    M_PropRead_req = 0xFC, // M_PropRead.req
    M_PropRead_con = 0xFB, // M_PropRead.con
    M_PropWrite_req = 0xF6, // M_PropWrite.req
    M_PropWrite_con = 0xF5, // M_PropWrite.con
    M_PropInfo_ind = 0xF7, // M_PropInfo_ind
    M_FuncPropCommand_req = 0xF8, // M_FuncPropCommand.req
    M_FuncPropStateRead_req = 0xF9, // M_FuncPropStateRead.req
    M_FuncPropCommand_con = 0xFA, // M_FuncPropCommand.con
    M_FuncPropStateRead_con = 0xFA, // M_FuncPropStateRead.con
    M_Reset_req = 0xF1, // M_Reset.req
    M_Reset_ind = 0xF0 // M_Reset.ind
};

}
