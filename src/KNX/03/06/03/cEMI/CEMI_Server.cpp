// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/CEMI_Server.h>

#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/06/03/cEMI/Additional_Info/Busmonitor_Status_Info.h>
#include <KNX/03/06/03/cEMI/Additional_Info/Timestamp_relative.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Busmon/CEMI_L_Busmon_ind.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_con.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_ind.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_req.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_con.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_req.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_con.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_ind.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_req.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_con.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_req.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropStateRead/CEMI_M_FuncPropStateRead_con.h>
#include <KNX/03/06/03/cEMI/Management/M_FuncPropStateRead/CEMI_M_FuncPropStateRead_req.h>
#include <KNX/03/06/03/cEMI/Management/M_PropInfo/CEMI_M_PropInfo_ind.h>
#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_con.h>
#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_req.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_con.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_req.h>
#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset_ind.h>
#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset_req.h>

namespace KNX {

CEMI_Server::CEMI_Server() :
    management_server()
{
}

void CEMI_Server::connect(Data_Link_Layer & data_link_layer)
{
    // data link layer
    this->L_Data_req = std::bind(&Data_Link_Layer::L_Data_req, &data_link_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
    data_link_layer.L_Data_con = std::bind(&CEMI_Server::L_Data_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
    data_link_layer.L_Data_ind = std::bind(&CEMI_Server::L_Data_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
    this->L_SystemBroadcast_req = std::bind(&Data_Link_Layer::L_SystemBroadcast_req, &data_link_layer, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    data_link_layer.L_SystemBroadcast_con = std::bind(&CEMI_Server::L_SystemBroadcast_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    data_link_layer.L_SystemBroadcast_ind = std::bind(&CEMI_Server::L_SystemBroadcast_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    this->L_Poll_Data_req = std::bind(&Data_Link_Layer::L_Poll_Data_req, &data_link_layer, std::placeholders::_1, std::placeholders::_2);
    data_link_layer.L_Poll_Data_con = std::bind(&CEMI_Server::L_Poll_Data_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    data_link_layer.L_Busmon_ind = std::bind(&CEMI_Server::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    this->L_Raw_req = std::bind(&Data_Link_Layer::L_Plain_Data_req, &data_link_layer, std::placeholders::_1, std::placeholders::_2);
    data_link_layer.L_Plain_Data_con = std::bind(&CEMI_Server::L_Raw_con, this, std::placeholders::_1, std::placeholders::_2);
    data_link_layer.L_Plain_Data_ind = std::bind(&CEMI_Server::L_Raw_ind, this, std::placeholders::_1);

    // management server
    this->M_PropRead_req = std::bind(&CEMI_Management_Server::M_PropRead_req, &management_server, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    management_server.M_PropRead_con = std::bind(&CEMI_Server::M_PropRead_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6, std::placeholders::_7);
    this->M_PropWrite_req = std::bind(&CEMI_Management_Server::M_PropWrite_req, &management_server, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    management_server.M_PropWrite_con = std::bind(&CEMI_Server::M_PropWrite_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    management_server.M_PropInfo_ind = std::bind(&CEMI_Server::M_PropInfo_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5, std::placeholders::_6);
    this->M_FuncPropCommand_req = std::bind(&CEMI_Management_Server::M_FuncPropCommand_req, &management_server, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    management_server.M_FuncPropCommand_con = std::bind(&CEMI_Server::M_FuncPropCommand_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    this->M_FuncPropStateRead_req = std::bind(&CEMI_Management_Server::M_FuncPropStateRead_req, &management_server, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);
    management_server.M_FuncPropStateRead_con = std::bind(&CEMI_Server::M_FuncPropStateRead_con, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5);
    this->M_Reset_req = std::bind(&CEMI_Management_Server::M_Reset_req, &management_server);
    management_server.M_Reset_ind = std::bind(&CEMI_Server::M_Reset_ind, this);
}

void CEMI_Server::cemi_req(const std::vector<uint8_t> data)
{
    std::shared_ptr<CEMI_Message> frame = make_CEMI_Message(std::cbegin(data), std::cend(data));
    switch (frame->message_code) {
    case CEMI_Message_Code::UNDEFINED:
        assert(false);
        break;
    case CEMI_Message_Code::L_Busmon_ind:
        assert(false);
        break;
    case CEMI_Message_Code::L_Data_req:
        if (L_Data_req) {
            std::shared_ptr<CEMI_L_Data_req> frame2 = std::dynamic_pointer_cast<CEMI_L_Data_req>(frame);
            Frame_Format frame_format;
            frame_format.frame_type_parameter = to_frame_type_parameter(frame2->control.frame_type);
            frame_format.extended_frame_format = frame2->extended_control.extended_frame_format;
            L_Data_req(frame2->control.acknowledge, frame2->extended_control.address_type, frame2->destination_address, frame_format, frame2->lsdu, frame2->control.priority, frame2->source_address);
        }
        break;
    case CEMI_Message_Code::L_Data_con:
        assert(false);
        break;
    case CEMI_Message_Code::L_Data_ind:
        assert(false);
        break;
    case CEMI_Message_Code::L_Raw_req:
        if (L_Raw_req) {
            std::shared_ptr<CEMI_L_Raw_req> frame2 = std::dynamic_pointer_cast<CEMI_L_Raw_req>(frame);
            L_Raw_req(0, frame2->data);
        }
        break;
    case CEMI_Message_Code::L_Raw_con:
        assert(false);
        break;
    case CEMI_Message_Code::L_Raw_ind:
        assert(false);
        break;
    case CEMI_Message_Code::L_Poll_Data_req:
        if (L_Poll_Data_req) {
            std::shared_ptr<CEMI_L_Poll_Data_req> frame2 = std::dynamic_pointer_cast<CEMI_L_Poll_Data_req>(frame);
            L_Poll_Data_req(Group_Address(frame2->destination_address), frame2->number_of_slots);
        }
        break;
    case CEMI_Message_Code::L_Poll_Data_con:
        assert(false);
        break;
    case CEMI_Message_Code::T_Data_Connected_req:
    case CEMI_Message_Code::T_Data_Connected_ind:
    case CEMI_Message_Code::T_Data_Individual_req:
    case CEMI_Message_Code::T_Data_Individual_ind:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropRead_req:
        if (M_PropRead_req) {
            std::shared_ptr<CEMI_M_PropRead_req> frame2 = std::dynamic_pointer_cast<CEMI_M_PropRead_req>(frame);
            M_PropRead_req(frame2->interface_object_type, frame2->object_instance, frame2->property_id, frame2->number_of_elements, frame2->start_index);
        }
        break;
    case CEMI_Message_Code::M_PropRead_con:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropWrite_req:
        if (M_PropWrite_req) {
            std::shared_ptr<CEMI_M_PropWrite_req> frame2 = std::dynamic_pointer_cast<CEMI_M_PropWrite_req>(frame);
            M_PropWrite_req(frame2->interface_object_type, frame2->object_instance, frame2->property_id, frame2->number_of_elements, frame2->start_index, frame2->data);
        }
        break;
    case CEMI_Message_Code::M_PropWrite_con:
        assert(false);
        break;
    case CEMI_Message_Code::M_PropInfo_ind:
        assert(false);
        break;
    case CEMI_Message_Code::M_FuncPropCommand_req:
        if (M_FuncPropCommand_req) {
            std::shared_ptr<CEMI_M_FuncPropCommand_req> frame2 = std::dynamic_pointer_cast<CEMI_M_FuncPropCommand_req>(frame);
            M_FuncPropCommand_req(frame2->interface_object_type, frame2->object_instance, frame2->property_id, frame2->data);
        }
        break;
    case CEMI_Message_Code::M_FuncPropStateRead_req:
        if (M_FuncPropStateRead_req) {
            std::shared_ptr<CEMI_M_FuncPropStateRead_req> frame2 = std::dynamic_pointer_cast<CEMI_M_FuncPropStateRead_req>(frame);
            M_FuncPropStateRead_req(frame2->interface_object_type, frame2->object_instance, frame2->property_id, frame2->data);
        }
        break;
    case CEMI_Message_Code::M_FuncPropCommand_con: // = M_FuncPropStateRead_con
        assert(false);
        break;
    case CEMI_Message_Code::M_Reset_ind:
        assert(false);
        break;
    case CEMI_Message_Code::M_Reset_req:
        if (M_Reset_req) {
            //std::shared_ptr<CEMI_M_Reset_req> frame2 = std::dynamic_pointer_cast<CEMI_M_Reset_req>(frame);
            M_Reset_req();
        }
        break;
    }
}

/* 4.1.5.3 L_Data service */

void CEMI_Server::L_Data_con(const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status)
{
    if (cemi_con) {
        CEMI_L_Data_con pdu;
        pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
        pdu.extended_control.address_type = address_type;
        pdu.extended_control.extended_frame_format = frame_format.extended_frame_format;
        pdu.destination_address = destination_address;
        pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
        pdu.control.priority = priority;
        pdu.source_address = source_address;
        pdu.lsdu = lsdu;
        pdu.control.confirm = (l_status == Status::ok) ? Confirm::no_error : Confirm::error;
        cemi_con(pdu.toData());
    }
}

void CEMI_Server::L_Data_ind(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)
{
    if (cemi_ind && (communication_mode().field1 == 0x00)) {
        CEMI_L_Data_ind pdu;
        pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
        pdu.control.acknowledge = ack_request;
        pdu.extended_control.address_type = address_type;
        pdu.extended_control.extended_frame_format = frame_format.extended_frame_format;
        // pdu.hop_count = 6; // @todo Get actual HC
        pdu.destination_address = destination_address;
        pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
        pdu.lsdu = lsdu;
        pdu.control.priority = priority;
        pdu.source_address = source_address;
        cemi_ind(pdu.toData());
    }
}

/* L_SystemBroadcast service */

void CEMI_Server::L_SystemBroadcast_con(const Group_Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status)
{
    if (cemi_con) {
        CEMI_L_Data_con pdu;
        pdu.destination_address = destination_address;
        pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
        pdu.control.priority = priority;
        pdu.source_address = source_address;
        pdu.lsdu = lsdu;
        pdu.control.confirm = (l_status == Status::ok) ? Confirm::no_error : Confirm::error;
        pdu.control.system_broadcast = System_Broadcast::System_Broadcast;
        cemi_con(pdu.toData());
    }
}

void CEMI_Server::L_SystemBroadcast_ind(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)
{
    if (cemi_ind && (communication_mode().field1 == 0x00)) {
        CEMI_L_Data_ind pdu;
        pdu.control.acknowledge = ack_request;
        pdu.destination_address = destination_address;
        pdu.control.frame_type = to_frame_type(frame_format.frame_type_parameter);
        pdu.lsdu = lsdu;
        pdu.control.priority = priority;
        pdu.source_address = source_address;
        pdu.control.system_broadcast = System_Broadcast::System_Broadcast;
        cemi_ind(pdu.toData());
    }
}

/* 4.1.5.6 L_Poll_Data service */

void CEMI_Server::L_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status)
{
    if (cemi_con) {
        CEMI_L_Poll_Data_con pdu;
        pdu.destination_address = destination_address;
        pdu.poll_data = poll_data_sequence;
        pdu.control.confirm = (l_status == Status::ok) ? Confirm::no_error : Confirm::error;
        pdu.control.poll = Poll::Poll_Data;
        pdu.source_address = management_server.interface_objects->group_address_table()->individual_address;
        cemi_con(pdu.toData());
    }
}

/* 4.1.5.7 L_Raw (L_Busmon) service */

void CEMI_Server::L_Raw_con(const std::vector<uint8_t> data, const Status /*l_status*/)
{
    if (cemi_con) {
        CEMI_L_Raw_con pdu;

        pdu.data = data;

        cemi_con(pdu.toData());
    }
}

void CEMI_Server::L_Raw_ind(const std::vector<uint8_t> data)
{
    if (cemi_ind && (communication_mode().field1 == 0x02)) {
        CEMI_L_Raw_ind pdu;

        pdu.data = data;

        cemi_ind(pdu.toData());
    }
}

void CEMI_Server::L_Busmon_ind(const Status /*l_status*/, const uint16_t time_stamp, const std::vector<uint8_t> lpdu)
{
    if (cemi_ind && (communication_mode().field1 == 0x01)) {
        CEMI_L_Busmon_ind pdu;

        // Busmonitor - Status Info (optional)
        std::shared_ptr<Busmonitor_Status_Info> busmonitor_status_info = std::make_shared<Busmonitor_Status_Info>();
        busmonitor_status_info->sequence_number = 0; // @todo increase count
        pdu.additional_info.push_back(busmonitor_status_info);

        // Timestamp relative (optional)
        std::shared_ptr<Timestamp_relative> timestamp_relative = std::make_shared<Timestamp_relative>();
        timestamp_relative->timestamp_relative = time_stamp;
        pdu.additional_info.push_back(timestamp_relative);

        // Raw Data
        pdu.data = lpdu;

        cemi_ind(pdu.toData());
    }
}

/* 4.1.7.3 Data Properties */

void CEMI_Server::M_PropRead_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data, const std::optional<CEMI_Error_Code> error_code)
{
    if (cemi_con) {
        CEMI_M_PropRead_con pdu;
        pdu.interface_object_type = interface_object_type;
        pdu.object_instance = object_instance;
        pdu.property_id = property_id;
        pdu.number_of_elements = nr_of_elem;
        pdu.start_index = start_index;
        pdu.data = data;
        pdu.error_code = error_code;
        cemi_con(pdu.toData());
    }
}

void CEMI_Server::M_PropWrite_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const std::optional<CEMI_Error_Code> error_code)
{
    if (cemi_con) {
        CEMI_M_PropWrite_con pdu;
        pdu.interface_object_type = interface_object_type;
        pdu.object_instance = object_instance;
        pdu.property_id = property_id;
        pdu.number_of_elements = nr_of_elem;
        pdu.start_index = start_index;
        pdu.error_code = error_code;
        cemi_con(pdu.toData());
    }
}

void CEMI_Server::M_PropInfo_ind(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data)
{
    if (cemi_ind) {
        CEMI_M_PropInfo_ind pdu;
        pdu.interface_object_type = interface_object_type;
        pdu.object_instance = object_instance;
        pdu.property_id = property_id;
        pdu.number_of_elements = nr_of_elem;
        pdu.start_index = start_index;
        pdu.data = data;
        cemi_ind(pdu.toData());
    }
}

/* 4.1.7.4 Function Properties */

void CEMI_Server::M_FuncPropCommand_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, std::optional<Property_Return_Code> return_code, const Property_Value  data)
{
    if (cemi_con) {
        CEMI_M_FuncPropCommand_con pdu;
        pdu.interface_object_type = interface_object_type;
        pdu.object_instance = object_instance;
        pdu.property_id = property_id;
        pdu.return_code = return_code;
        pdu.data = data;
        cemi_con(pdu.toData());
    }
}

void CEMI_Server::M_FuncPropStateRead_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, std::optional<Property_Return_Code> return_code, const Property_Value  data)
{
    if (cemi_con) {
        CEMI_M_FuncPropStateRead_con pdu;
        pdu.interface_object_type = interface_object_type;
        pdu.object_instance = object_instance;
        pdu.property_id = property_id;
        pdu.return_code = return_code;
        pdu.data = data;
        cemi_con(pdu.toData());
    }
}

/* 4.1.7.5 Further cEMI services */

void CEMI_Server::M_Reset_ind()
{
    if (cemi_ind) {
        CEMI_M_Reset_ind pdu;
        cemi_ind(pdu.toData());
    }
}

DPT_CommMode CEMI_Server::communication_mode() const
{
    if (management_server.interface_objects) {
        std::shared_ptr<Interface_Object> interface_object = management_server.interface_objects->object_by_type(OT_CEMI_SERVER);
        if (interface_object) {
            std::shared_ptr<CEMI_Server_Object> cemi_server_object = management_server.interface_objects->cemi_server();
            std::shared_ptr<Property> property = cemi_server_object->property_by_id(CEMI_Server_Object::PID_COMM_MODE);
            if (property) {
                std::shared_ptr<Communication_Mode_Property> communication_mode_property = cemi_server_object->communication_mode();
                return communication_mode_property->communication_mode;
            }
        }
    }

    return DPT_CommMode();
}

}
