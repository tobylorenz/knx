// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Data_Link_Layer.h>
#include <KNX/03/05/01/Interface_Objects.h>
#include <KNX/03/06/03/cEMI/CEMI_Management_Server.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * cEMI Server
 *
 * @ingroup KNX_03_06_03_04_01_02
 * @ingroup KNX_03_06_03_04_01_07
 */
class KNX_EXPORT CEMI_Server
{
public:
    explicit CEMI_Server();

    /** connect cEMI Server to Data Link Layer */
    void connect(Data_Link_Layer & data_link_layer);

    /* command interface */
    virtual void cemi_req(const std::vector<uint8_t> data); // @todo make this async functions req/con
    std::function<void(const std::vector<uint8_t> data)> cemi_con;
    std::function<void(const std::vector<uint8_t> data)> cemi_ind; // @todo make this async functions ind

    /** cEMI Management Server */
    CEMI_Management_Server management_server;

protected:
    /* 4.1.5.3 L_Data service */
    std::function<void(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address)> L_Data_req;
    virtual void L_Data_con(const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status);
    virtual void L_Data_ind(const Ack_Request ack_request, const Address_Type address_type, const Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address);

    /* L_SystemBroadcast service */
    std::function<void(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority)> L_SystemBroadcast_req;
    virtual void L_SystemBroadcast_con(const Group_Address destination_address, const Frame_Format frame_format, const Priority priority, const Individual_Address source_address, std::shared_ptr<LSDU> lsdu, const Status l_status);
    virtual void L_SystemBroadcast_ind(const Ack_Request ack_request, const Group_Address destination_address, const Frame_Format frame_format, std::shared_ptr<LSDU> lsdu, const Priority priority, const Individual_Address source_address);

    /* 4.1.5.6 L_Poll_Data service */
    std::function<void(const Group_Address destination_address, const uint8_t no_of_expected_poll_data)> L_Poll_Data_req;
    virtual void L_Poll_Data_con(const Group_Address destination_address, const std::vector<uint8_t> poll_data_sequence, const Status l_status);

    /* 4.1.5.7 L_Raw (L_Busmon) service */
    std::function<void(const uint32_t time, const std::vector<uint8_t> data)> L_Raw_req; // L_Plain_Data.req equivalent
    virtual void L_Raw_con(const std::vector<uint8_t> data, const Status l_status); // L_Plain_Data.con equivalent
    virtual void L_Raw_ind(const std::vector<uint8_t> data); // L_Plain_Data.ind equivalent
    virtual void L_Busmon_ind(const Status l_status, const uint16_t time_stamp, const std::vector<uint8_t> lpdu);

    /* 4.1.7.3 Data Properties */
    std::function<void(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index)> M_PropRead_req;
    virtual void M_PropRead_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data, const std::optional<CEMI_Error_Code> error_code);
    std::function<void(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data)> M_PropWrite_req;
    virtual void M_PropWrite_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const std::optional<CEMI_Error_Code> error_code);
    virtual void M_PropInfo_ind(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const PropertyValue_Nr_Of_Elem nr_of_elem, const PropertyValue_Start_Index start_index, const Property_Value  data);

    /* 4.1.7.4 Function Properties */
    std::function<void(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)> M_FuncPropCommand_req;
    virtual void M_FuncPropCommand_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, std::optional<Property_Return_Code> return_code, const Property_Value  data);
    std::function<void(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, const Property_Value  data)> M_FuncPropStateRead_req;
    virtual void M_FuncPropStateRead_con(const Object_Type interface_object_type, const Object_Instance object_instance, const Property_Id property_id, std::optional<Property_Return_Code> return_code, const Property_Value  data);

    /* 4.1.7.5 Further cEMI services */
    std::function<void()> M_Reset_req;
    virtual void M_Reset_ind();

private:
    DPT_CommMode communication_mode() const;
};

}
