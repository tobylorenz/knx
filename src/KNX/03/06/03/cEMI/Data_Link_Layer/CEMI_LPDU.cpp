// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Data_Link_Layer/CEMI_LPDU.h>

#include <cassert>

namespace KNX {

CEMI_LPDU::CEMI_LPDU(const CEMI_Message_Code message_code) :
    CEMI_Message(message_code)
{
}

void CEMI_LPDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 8);

    CEMI_Message::fromData(first, last);
    first += 2 + additional_info_length;

    assert(std::distance(first, last) >= 6);

    control = *first++;

    const uint8_t ctrl2 = *first++;
    extended_control = ctrl2;
    hop_count = (ctrl2 >> 4) & 0x07;

    source_address.fromData(first, first + 2);
    first += 2;

    destination_address.fromData(first, first + 2);
    first += 2;
}

std::vector<uint8_t> CEMI_LPDU::toData() const
{
    std::vector<uint8_t> data = CEMI_Message::toData();

    data.push_back(control);

    data.push_back(extended_control | (hop_count << 4));

    data.push_back(source_address >> 8);
    data.push_back(source_address & 0xff);

    data.push_back(destination_address >> 8);
    data.push_back(destination_address & 0xff);

    return data;
}

}
