// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>

#include <KNX/03/03/02/Address.h>
#include <KNX/03/03/02/Control.h>
#include <KNX/03/03/02/Extended_Control.h>
#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/03/Hop_Count.h>
#include <KNX/03/06/03/cEMI/CEMI_Message.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * cEMI LPDU
 *
 * @ingroup KNX_03_06_03_04_01_05
 */
class KNX_EXPORT CEMI_LPDU :
    public CEMI_Message
{
public:
    CEMI_LPDU(const CEMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Control field (Ctrl1) */
    Control control{};

    /** Extended Control field (Ctrl2) */
    Extended_Control extended_control{};

    /** Hop Count (Ctrl2.HC) */
    Hop_Count hop_count{};

    /** Source Address (SA) */
    Individual_Address source_address{};

    /** Destination Address (DA) */
    Address destination_address{};
};

}
