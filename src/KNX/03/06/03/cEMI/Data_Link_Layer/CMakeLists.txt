# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_LPDU.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_LPDU.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_LPDU.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/06/03/cEMI/Data_Link_Layer)

# sub directories
add_subdirectory(L_Busmon)
add_subdirectory(L_Data)
add_subdirectory(L_Poll_Data)
add_subdirectory(L_Raw)
