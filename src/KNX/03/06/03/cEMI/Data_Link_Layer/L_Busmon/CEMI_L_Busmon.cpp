// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Busmon/CEMI_L_Busmon.h>

#include <cassert>

namespace KNX {

CEMI_L_Busmon::CEMI_L_Busmon(const CEMI_Message_Code message_code) :
    CEMI_Message(message_code)
{
}

void CEMI_L_Busmon::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    CEMI_Message::fromData(first, last);
    first += 2 + additional_info_length;

    data.assign(first, last);
}

std::vector<uint8_t> CEMI_L_Busmon::toData() const
{
    std::vector<uint8_t> data = CEMI_Message::toData();

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

}
