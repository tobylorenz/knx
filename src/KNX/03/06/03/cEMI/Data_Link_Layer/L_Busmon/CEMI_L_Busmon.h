// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/CEMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Busmon message
 */
class KNX_EXPORT CEMI_L_Busmon :
    public CEMI_Message
{
public:
    explicit CEMI_L_Busmon(const CEMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** data */
    std::vector<uint8_t> data{};
};

}
