// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Busmon/CEMI_L_Busmon_ind.h>

namespace KNX {

CEMI_L_Busmon_ind::CEMI_L_Busmon_ind() :
    CEMI_L_Busmon(CEMI_Message_Code::L_Busmon_ind)
{
}

}
