// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Busmon/CEMI_L_Busmon.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Busmon.ind message
 *
 * @ingroup KNX_03_06_03_04_01_05_07_06
 */
class KNX_EXPORT CEMI_L_Busmon_ind :
    public CEMI_L_Busmon
{
public:
    CEMI_L_Busmon_ind();
};

}
