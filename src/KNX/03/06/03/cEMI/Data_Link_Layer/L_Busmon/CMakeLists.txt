# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_L_Busmon.h
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_L_Busmon_ind.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_L_Busmon.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_L_Busmon_ind.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_L_Busmon.h
        ${CMAKE_CURRENT_SOURCE_DIR}/CEMI_L_Busmon_ind.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/06/03/cEMI/Data_Link_Layer/L_Busmon)
