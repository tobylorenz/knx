// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data.h>

#include <cassert>

#include <KNX/03/03/03/N_Data_Broadcast/N_Data_Broadcast_PDU.h>
#include <KNX/03/03/03/N_Data_Group/N_Data_Group_PDU.h>
#include <KNX/03/03/03/N_Data_Individual/N_Data_Individual_PDU.h>
#include <KNX/03/03/03/N_Data_SystemBroadcast/N_Data_SystemBroadcast_PDU.h>

namespace KNX {

CEMI_L_Data::CEMI_L_Data(const CEMI_Message_Code message_code) :
    CEMI_LPDU(message_code)
{
    hop_count = 6;  // @todo Get actual HC
}

void CEMI_L_Data::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 9);

    CEMI_LPDU::fromData(first, last);
    first += 2 + additional_info_length + 6;

    length = *first++;

    if (control.system_broadcast == System_Broadcast::System_Broadcast) {
        lsdu = std::make_shared<N_Data_SystemBroadcast_PDU>();
    } else {
        switch (extended_control.address_type) {
        case Address_Type::Individual:
            lsdu = std::make_shared<N_Data_Individual_PDU>();
            break;
        case Address_Type::Group:
            if (destination_address == 0) {
                lsdu = std::make_shared<N_Data_Broadcast_PDU>();
            } else {
                lsdu = std::make_shared<N_Data_Group_PDU>();
            }
            break;
        }
    }
    lsdu->fromData(hop_count, first, first + length + 1);
    first += length + 1;

    assert(first == last);
}

std::vector<uint8_t> CEMI_L_Data::toData() const
{
    assert(lsdu);

    std::vector<uint8_t> data = CEMI_LPDU::toData();

    const std::vector<uint8_t> lsdu_data = lsdu->toData();

    data.push_back(length_calculated());

    data.insert(std::cend(data), std::cbegin(lsdu_data), std::cend(lsdu_data));

    return data;
}

uint8_t CEMI_L_Data::length_calculated() const
{
    assert(lsdu);

    return lsdu->length_calculated();
}

}
