// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/LSDU.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/CEMI_LPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Data message
 *
 * @ingroup KNX_03_06_03_04_01_05_03_02
 */
class KNX_EXPORT CEMI_L_Data :
    public CEMI_LPDU
{
public:
    explicit CEMI_L_Data(const CEMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /* NPDU */

    /** Length (L) */
    uint8_t length{};

    /** calculate Length (L) */
    virtual uint8_t length_calculated() const;

    /** LSDU = NPDU */
    std::shared_ptr<LSDU> lsdu{};
};

}
