// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Data.con
 *
 * @ingroup KNX_03_06_03_04_01_05_03_04
 */
class KNX_EXPORT CEMI_L_Data_con :
    public CEMI_L_Data
{
public:
    CEMI_L_Data_con();
};

}
