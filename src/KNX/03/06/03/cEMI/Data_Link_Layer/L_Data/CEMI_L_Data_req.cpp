// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Data/CEMI_L_Data_req.h>

namespace KNX {

CEMI_L_Data_req::CEMI_L_Data_req() :
    CEMI_L_Data(CEMI_Message_Code::L_Data_req)
{
}

}
