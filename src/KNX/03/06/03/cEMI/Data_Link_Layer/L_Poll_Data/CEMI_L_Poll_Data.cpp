// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data.h>

namespace KNX {

CEMI_L_Poll_Data::CEMI_L_Poll_Data(const CEMI_Message_Code message_code) :
    CEMI_LPDU(message_code)
{
}

}
