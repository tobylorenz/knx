// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Data_Link_Layer/CEMI_LPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Poll_Data message
 */
class KNX_EXPORT CEMI_L_Poll_Data :
    public CEMI_LPDU
{
public:
    explicit CEMI_L_Poll_Data(const CEMI_Message_Code message_code);
};

}
