// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_con.h>

#include <cassert>

namespace KNX {

CEMI_L_Poll_Data_con::CEMI_L_Poll_Data_con() :
    CEMI_L_Poll_Data(CEMI_Message_Code::L_Poll_Data_con)
{
}

void CEMI_L_Poll_Data_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 9);

    CEMI_L_Poll_Data::fromData(first, last);
    first += 2 + additional_info_length + 6;

    number_of_slots = (*first++) & 0x0f;

    poll_data.assign(first, first + number_of_slots);
    first += number_of_slots;

    assert(first == last);
}

std::vector<uint8_t> CEMI_L_Poll_Data_con::toData() const
{
    std::vector<uint8_t> data = CEMI_L_Poll_Data::toData();

    data.push_back(number_of_slots_calculated());

    data.insert(std::cend(data), std::cbegin(poll_data), std::cend(poll_data));

    return data;
}

uint4_t CEMI_L_Poll_Data_con::number_of_slots_calculated() const
{
    return poll_data.size() & 0x0f;
}

}
