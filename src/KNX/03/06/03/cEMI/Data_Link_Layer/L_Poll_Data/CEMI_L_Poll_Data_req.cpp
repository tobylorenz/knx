// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data_req.h>

namespace KNX {

CEMI_L_Poll_Data_req::CEMI_L_Poll_Data_req() :
    CEMI_L_Poll_Data(CEMI_Message_Code::L_Poll_Data_req)
{
}

void CEMI_L_Poll_Data_req::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 9);

    CEMI_L_Poll_Data::fromData(first, last);
    first += 2 + additional_info_length + 6;

    number_of_slots = (*first++) & 0x0f;

    assert(first == last);
}

std::vector<uint8_t> CEMI_L_Poll_Data_req::toData() const
{
    std::vector<uint8_t> data = CEMI_L_Poll_Data::toData();

    data.push_back(number_of_slots);

    return data;
}

}
