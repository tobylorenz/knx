// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Poll_Data/CEMI_L_Poll_Data.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Poll_Data.req message
 *
 * @ingroup KNX_03_06_03_04_01_05_06_02
 */
class KNX_EXPORT CEMI_L_Poll_Data_req :
    public CEMI_L_Poll_Data
{
public:
    CEMI_L_Poll_Data_req();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** number of slots */
    uint4_t number_of_slots{};
};

}
