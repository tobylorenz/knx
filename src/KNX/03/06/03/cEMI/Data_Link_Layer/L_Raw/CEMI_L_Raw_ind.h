// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * L_Raw.ind message
 *
 * @ingroup KNX_03_06_03_04_01_05_07_05
 */
class KNX_EXPORT CEMI_L_Raw_ind :
    public CEMI_L_Raw
{
public:
    CEMI_L_Raw_ind();
};

}
