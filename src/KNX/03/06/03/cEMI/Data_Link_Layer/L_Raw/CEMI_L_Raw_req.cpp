// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Data_Link_Layer/L_Raw/CEMI_L_Raw_req.h>

namespace KNX {

CEMI_L_Raw_req::CEMI_L_Raw_req() :
    CEMI_L_Raw(CEMI_Message_Code::L_Raw_req)
{
}

}
