// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/CEMI_Data_Property_Message.h>

#include <cassert>

namespace KNX {

CEMI_Data_Property_Message::CEMI_Data_Property_Message(const CEMI_Message_Code message_code) :
    CEMI_Property_Message(message_code)
{
}

void CEMI_Data_Property_Message::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 7);

    CEMI_Property_Message::fromData(first, first + 5);
    first += 5;

    number_of_elements = *first >> 4;
    start_index = ((*first++ & 0x0f) << 4) | *first++;

    assert(first == last);
}

std::vector<uint8_t> CEMI_Data_Property_Message::toData() const
{
    std::vector<uint8_t> data = CEMI_Property_Message::toData();

    data.push_back((number_of_elements << 4) | (start_index >> 8));
    data.push_back(start_index & 0xff);

    return data;
}

}
