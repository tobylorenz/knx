// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/PropertyValue_Nr_Of_Elem.h>
#include <KNX/03/03/07/PropertyValue_Start_Index.h>
#include <KNX/03/06/03/cEMI/Management/CEMI_Property_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Data Property Message
 *
 * @ingroup KNX_03_06_03_04_01_07_03_01
 */
class KNX_EXPORT CEMI_Data_Property_Message :
    public CEMI_Property_Message
{
public:
    explicit CEMI_Data_Property_Message(const CEMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** number of elements */
    PropertyValue_Nr_Of_Elem number_of_elements{};

    /** start index */
    PropertyValue_Start_Index start_index{};
};

}
