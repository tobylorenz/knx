// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Error Code Set
 *
 * @ingroup KNX_03_06_03_04_01_07_03_07_02
 */
enum CEMI_Error_Code : uint8_t {
    /** unknown error */
    Unspecified_Error = 0x00,

    /** write value not allowed (general, if not error 2 or 3) */
    Out_of_Range = 0x01,

    /** write value to high */
    Out_of_MaxRange = 0x02,

    /** write value to low */
    Out_of_MinRange = 0x03,

    /** memory can not be written or only with fault(s) */
    Memory_Error = 0x04,

    /** write access to a 'read only' or a write protected Property */
    Read_Only = 0x05,

    /** COMMAND not valid or not supported */
    Illegal_COMMAND = 0x06,

    /** read or write access to an non existing Property */
    Void_DP = 0x07,

    /** write access with wrong data type (Datapoint length) */
    Type_Conflict = 0x08,

    /** read or write access to a non existing Property array index */
    Prop_Index_Range_Error = 0x09,

    /** The Property exists but can at this moment not be written with a new value */
    Value_temporarily_not_writeable = 0x0A
};

}
