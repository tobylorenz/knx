// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/CEMI_Function_Property_Message.h>

namespace KNX {

CEMI_Function_Property_Message::CEMI_Function_Property_Message(const CEMI_Message_Code message_code) :
    CEMI_Property_Message(message_code)
{
}

}
