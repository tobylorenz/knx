// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Management/CEMI_Property_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Function Property Message
 *
 * @ingroup KNX_03_06_03_04_01_07_04
 */
class KNX_EXPORT CEMI_Function_Property_Message :
    public CEMI_Property_Message
{
public:
    explicit CEMI_Function_Property_Message(const CEMI_Message_Code message_code);
};

}
