// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/CEMI_Property_Message.h>

#include <cassert>

namespace KNX {

CEMI_Property_Message::CEMI_Property_Message(const CEMI_Message_Code message_code) :
    CEMI_Message(message_code)
{
}

void CEMI_Property_Message::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 5);

    CEMI_Message::fromData(first, last);
    ++first;

    interface_object_type = (*first++ << 8) | *first++;

    object_instance = *first++;

    property_id = *first++;

    assert(first == last);
}

std::vector<uint8_t> CEMI_Property_Message::toData() const
{
    std::vector<uint8_t> data = CEMI_Message::toData();

    data.push_back(interface_object_type >> 8);
    data.push_back(interface_object_type & 0xff);

    data.push_back(object_instance);

    data.push_back(property_id);

    return data;
}

}
