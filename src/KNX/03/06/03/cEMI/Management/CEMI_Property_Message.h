// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Object_Instance.h>
#include <KNX/03/03/07/Object_Type.h>
#include <KNX/03/03/07/Property_Id.h>
#include <KNX/03/06/03/cEMI/CEMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Property Message
 *
 * @ingroup KNX_03_06_03_04_01_07_03
 */
class KNX_EXPORT CEMI_Property_Message :
    public CEMI_Message
{
public:
    explicit CEMI_Property_Message(const CEMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** interface object type */
    Object_Type interface_object_type{};

    /** object instance */
    Object_Instance object_instance{};

    /** property id */
    Property_Id property_id{};
};

}
