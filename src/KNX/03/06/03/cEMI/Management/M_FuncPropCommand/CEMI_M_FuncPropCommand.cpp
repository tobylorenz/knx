// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand.h>

namespace KNX {

CEMI_M_FuncPropCommand::CEMI_M_FuncPropCommand(const CEMI_Message_Code message_code) :
    CEMI_Function_Property_Message(message_code)
{
}

}
