// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_con.h>

namespace KNX {

CEMI_M_FuncPropCommand_con::CEMI_M_FuncPropCommand_con() :
    CEMI_M_FuncPropCommand(CEMI_Message_Code::M_FuncPropCommand_con)
{
}

void CEMI_M_FuncPropCommand_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    CEMI_M_FuncPropCommand::fromData(first, first + 5);
    first += 5;

    if (first != last) {
        return_code = *first++;
    }

    data.assign(first, last);
}

std::vector<uint8_t> CEMI_M_FuncPropCommand_con::toData() const
{
    std::vector<uint8_t> data = CEMI_M_FuncPropCommand::toData();

    if (return_code.has_value()) {
        data.push_back(return_code.value());
    }

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

}
