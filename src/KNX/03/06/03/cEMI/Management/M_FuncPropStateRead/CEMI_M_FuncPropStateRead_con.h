// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Management/M_FuncPropCommand/CEMI_M_FuncPropCommand_con.h>

namespace KNX {

/**
 * M_FuncPropStateRead.con message
 *
 * @ingroup KNX_03_06_03_04_01_07_04_04
 * @note message is identical to CEMI_M_FuncPropCommand_con
 */
using CEMI_M_FuncPropStateRead_con = CEMI_M_FuncPropCommand_con;

}
