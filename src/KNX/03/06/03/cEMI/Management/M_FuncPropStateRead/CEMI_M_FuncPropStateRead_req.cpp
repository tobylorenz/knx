// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_FuncPropStateRead/CEMI_M_FuncPropStateRead_req.h>

namespace KNX {

CEMI_M_FuncPropStateRead_req::CEMI_M_FuncPropStateRead_req() :
    CEMI_Function_Property_Message(CEMI_Message_Code::M_FuncPropStateRead_req)
{
}

void CEMI_M_FuncPropStateRead_req::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    CEMI_Function_Property_Message::fromData(first, first + 5);
    first += 5;

    data.assign(first, last);
}

std::vector<uint8_t> CEMI_M_FuncPropStateRead_req::toData() const
{
    std::vector<uint8_t> data = CEMI_Function_Property_Message::toData();

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

}
