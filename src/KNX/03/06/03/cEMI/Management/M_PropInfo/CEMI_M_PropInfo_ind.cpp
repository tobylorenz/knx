// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_PropInfo/CEMI_M_PropInfo_ind.h>

namespace KNX {

CEMI_M_PropInfo_ind::CEMI_M_PropInfo_ind() :
    CEMI_Data_Property_Message(CEMI_Message_Code::M_PropInfo_ind)
{
}

void CEMI_M_PropInfo_ind::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    CEMI_Data_Property_Message::fromData(first, first + 7);
    first += 7;

    data.assign(first, last);
}

std::vector<uint8_t> CEMI_M_PropInfo_ind::toData() const
{
    std::vector<uint8_t> data = CEMI_Data_Property_Message::toData();

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

}
