// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_con.h>

namespace KNX {

CEMI_M_PropRead_con::CEMI_M_PropRead_con() :
    CEMI_M_PropRead(CEMI_Message_Code::M_PropRead_con)
{
}

void CEMI_M_PropRead_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    CEMI_M_PropRead::fromData(first, first + 5);
    first += 7;

    if (number_of_elements == 0) {
        error_code = static_cast<CEMI_Error_Code>(*first++);
        assert(first == last);
    } else {
        data.assign(first, last);
    }
}

std::vector<uint8_t> CEMI_M_PropRead_con::toData() const
{
    std::vector<uint8_t> data = CEMI_M_PropRead::toData();

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    if (error_code.has_value()) {
        data.push_back(static_cast<uint8_t>(error_code.value()));
    }

    return data;
}

}
