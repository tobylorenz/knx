// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <optional>

#include <KNX/03/03/07/Property_Value.h>
#include <KNX/03/06/03/cEMI/Management/CEMI_Error_Codes.h>
#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_PropRead.con message
 *
 * @ingroup KNX_03_06_03_04_01_07_03_03
 */
class KNX_EXPORT CEMI_M_PropRead_con :
    public CEMI_M_PropRead
{
public:
    CEMI_M_PropRead_con();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** data, if positive response (NoE>0) */
    Property_Value  data{};

    /** error code, if negative response (NoE=0) */
    std::optional<CEMI_Error_Code> error_code{};
};

}
