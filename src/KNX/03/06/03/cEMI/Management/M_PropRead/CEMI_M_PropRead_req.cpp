// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead_req.h>

namespace KNX {

CEMI_M_PropRead_req::CEMI_M_PropRead_req() :
    CEMI_M_PropRead(CEMI_Message_Code::M_PropRead_req)
{
}

}
