// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Management/M_PropRead/CEMI_M_PropRead.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_PropRead.req message
 *
 * @ingroup KNX_03_06_03_04_01_07_03_02
 */
class KNX_EXPORT CEMI_M_PropRead_req :
    public CEMI_M_PropRead
{
public:
    CEMI_M_PropRead_req();
};

}
