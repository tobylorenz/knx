// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite.h>

namespace KNX {

CEMI_M_PropWrite::CEMI_M_PropWrite(const CEMI_Message_Code message_code) :
    CEMI_Data_Property_Message(message_code)
{
}

}
