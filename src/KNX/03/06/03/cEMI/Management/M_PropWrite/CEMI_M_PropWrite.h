// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Management/CEMI_Data_Property_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_PropWrite message
 */
class KNX_EXPORT CEMI_M_PropWrite :
    public CEMI_Data_Property_Message
{
public:
    explicit CEMI_M_PropWrite(const CEMI_Message_Code message_code);
};

}
