// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_con.h>

namespace KNX {

CEMI_M_PropWrite_con::CEMI_M_PropWrite_con() :
    CEMI_M_PropWrite(CEMI_Message_Code::M_PropWrite_con)
{
}

void CEMI_M_PropWrite_con::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    CEMI_Data_Property_Message::fromData(first, first + 5);
    first += 7;

    if (first != last) {
        error_code = static_cast<CEMI_Error_Code>(*first++);
    }

    assert(first == last);
}

std::vector<uint8_t> CEMI_M_PropWrite_con::toData() const
{
    std::vector<uint8_t> data = CEMI_Data_Property_Message::toData();

    if (error_code.has_value()) {
        data.push_back(static_cast<uint8_t>(error_code.value()));
    }

    return data;
}

}
