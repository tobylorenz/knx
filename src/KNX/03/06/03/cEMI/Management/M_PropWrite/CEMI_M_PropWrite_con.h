// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <optional>

#include <KNX/03/06/03/cEMI/Management/CEMI_Error_Codes.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_PropWrite.con message
 *
 * @ingroup KNX_03_06_03_04_01_07_03_05
 */
class KNX_EXPORT CEMI_M_PropWrite_con :
    public CEMI_M_PropWrite
{
public:
    CEMI_M_PropWrite_con();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** error code */
    std::optional<CEMI_Error_Code> error_code{};
};

}
