// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite_req.h>

namespace KNX {

CEMI_M_PropWrite_req::CEMI_M_PropWrite_req() :
    CEMI_M_PropWrite(CEMI_Message_Code::M_PropWrite_req)
{
}

void CEMI_M_PropWrite_req::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    CEMI_M_PropWrite::fromData(first, first + 7);
    first += 7;

    data.assign(first, last);
}

std::vector<uint8_t> CEMI_M_PropWrite_req::toData() const
{
    std::vector<uint8_t> data = CEMI_M_PropWrite::toData();

    data.insert(std::cend(data), std::cbegin(this->data), std::cend(this->data));

    return data;
}

}
