// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Property_Value.h>
#include <KNX/03/06/03/cEMI/Management/M_PropWrite/CEMI_M_PropWrite.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_PropWrite.req message
 *
 * @ingroup KNX_03_06_03_04_01_07_03_04
 */
class KNX_EXPORT CEMI_M_PropWrite_req :
    public CEMI_M_PropWrite
{
public:
    CEMI_M_PropWrite_req();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Data */
    Property_Value  data{};
};

}
