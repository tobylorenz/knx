// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/CEMI_Message.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_Reset message
 */
class KNX_EXPORT CEMI_M_Reset :
    public CEMI_Message
{
public:
    explicit CEMI_M_Reset(const CEMI_Message_Code message_code);
};

}
