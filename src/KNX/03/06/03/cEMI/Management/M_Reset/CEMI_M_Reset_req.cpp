// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset_req.h>

namespace KNX {

CEMI_M_Reset_req::CEMI_M_Reset_req() :
    CEMI_M_Reset(CEMI_Message_Code::M_Reset_req)
{
}

}
