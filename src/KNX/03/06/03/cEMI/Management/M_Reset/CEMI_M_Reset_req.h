// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Management/M_Reset/CEMI_M_Reset.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * M_Reset.req message
 *
 * @ingroup KNX_03_06_03_04_01_07_05_01
 */
class KNX_EXPORT CEMI_M_Reset_req :
    public CEMI_M_Reset
{
public:
    CEMI_M_Reset_req();
};

}
