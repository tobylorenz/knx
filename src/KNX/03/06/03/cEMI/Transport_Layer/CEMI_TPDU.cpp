// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Transport_Layer/CEMI_TPDU.h>

#include <cassert>

#include <KNX/03/03/07/APDU.h>

namespace KNX {

CEMI_TPDU::CEMI_TPDU(const CEMI_Message_Code message_code) :
    CEMI_Message(message_code)
{
}

void CEMI_TPDU::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 11);

    CEMI_Message::fromData(first, last);
    first += 2 + additional_info_length;

    assert(std::distance(first, last) >= 9);

    std::copy(first, first + 6, std::begin(reserved));
    first += 6;

    length = *first++;

    const uint8_t tpci = *first; // no ++ as tsdu need to parse it as well
    data_control_flag = static_cast<Data_Control_Flag>(tpci >> 7);
    numbered = static_cast<Numbered>((tpci >> 6) & 0x01);
    sequence_number = (tpci >> 2) & 0x0f;
    control = static_cast<TPDU::Control>(tpci & 0x03);

    if (data_control_flag == Data_Control_Flag::Data) {
        /* octet 6..N */
        tsdu = make_APDU(first, last);
    }
}

std::vector<uint8_t> CEMI_TPDU::toData() const
{
    std::vector<uint8_t> data = CEMI_Message::toData();

    data.insert(std::cend(data), std::cbegin(reserved), std::cend(reserved));

    data.push_back(length);

    if (data_control_flag == Data_Control_Flag::Data) {
        assert(tsdu);

        data = tsdu->toData();

        // add TPCI data into first octet
        data[0] =
            (static_cast<uint8_t>(data_control_flag) << 7) |
            (static_cast<uint8_t>(numbered) << 6) |
            (static_cast<uint8_t>(sequence_number) << 2) |
            (data[0] & 0x03);
    } else {
        data.push_back(
            (static_cast<uint8_t>(data_control_flag) << 7) |
            (static_cast<uint8_t>(numbered) << 6) |
            (static_cast<uint8_t>(sequence_number) << 2) |
            static_cast<uint8_t>(control));
    }

    return data;
}

uint8_t CEMI_TPDU::length_calculated() const
{
    return tsdu ? tsdu->length_calculated() : 0;
}

}
