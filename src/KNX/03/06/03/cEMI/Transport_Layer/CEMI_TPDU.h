// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>
#include <memory>
#include <vector>

#include <KNX/03/03/04/Data_Control_Flag.h>
#include <KNX/03/03/04/Numbered.h>
#include <KNX/03/03/04/Sequence_Number.h>
#include <KNX/03/03/04/TPDU.h>
#include <KNX/03/03/04/TSDU.h>
#include <KNX/03/06/03/cEMI/CEMI_Message.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * cEMI TPDU
 *
 * @ingroup KNX_03_06_03_04_01_06
 */
class KNX_EXPORT CEMI_TPDU :
    public CEMI_Message
{
public:
    CEMI_TPDU(const CEMI_Message_Code message_code);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Reserved */
    std::array<uint8_t, 6> reserved{};

    /** Length (L) */
    uint8_t length{};

    /** calculate Length (L) */
    virtual uint8_t length_calculated() const;

    /** Data/Control Flag */
    Data_Control_Flag data_control_flag{Data_Control_Flag::Data};

    /** Numbered */
    Numbered numbered{Numbered::Has_No_SeqNo};

    /** Sequence Number (SeqNo) */
    Sequence_Number sequence_number{};

    /** @copydoc Control */
    TPDU::Control control{TPDU::Control::T_Connect};

    /** TSDU = APDU */
    std::shared_ptr<TSDU> tsdu{};
};

}
