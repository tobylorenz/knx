// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Connected/CEMI_T_Data_Connected.h>

namespace KNX {

CEMI_T_Data_Connected::CEMI_T_Data_Connected(const CEMI_Message_Code message_code) :
    CEMI_TPDU(message_code)
{
}

}
