// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Transport_Layer/CEMI_TPDU.h>
#include <KNX/knx_export.h>

namespace KNX {

class KNX_EXPORT CEMI_T_Data_Connected :
    public CEMI_TPDU
{
public:
    explicit CEMI_T_Data_Connected(const CEMI_Message_Code message_code);
};

}
