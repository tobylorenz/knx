// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Connected/CEMI_T_Data_Connected_req.h>

namespace KNX {

CEMI_T_Data_Connected_req::CEMI_T_Data_Connected_req() :
    CEMI_T_Data_Connected(CEMI_Message_Code::T_Data_Connected_req)
{
}

}
