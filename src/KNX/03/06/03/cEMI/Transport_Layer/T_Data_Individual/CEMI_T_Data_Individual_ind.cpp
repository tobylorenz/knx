// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Individual/CEMI_T_Data_Individual_ind.h>

namespace KNX {

CEMI_T_Data_Individual_ind::CEMI_T_Data_Individual_ind() :
    CEMI_T_Data_Individual(CEMI_Message_Code::T_Data_Individual_ind)
{
}

}
