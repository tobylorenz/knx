// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/06/03/cEMI/Transport_Layer/T_Data_Individual/CEMI_T_Data_Individual.h>
#include <KNX/knx_export.h>

namespace KNX {

class KNX_EXPORT CEMI_T_Data_Individual_ind :
    public CEMI_T_Data_Individual
{
public:
    CEMI_T_Data_Individual_ind();
};

}
