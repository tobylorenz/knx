// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 2 Message Format */
//#include <KNX/03/06/03/EMI2/EMI2_Message_Code.h>
//#include <KNX/03/06/03/cEMI/CEMI_Message_Code.h>

/* 3 EMI1 and EMI2 */
#include <KNX/03/06/03/EMI.h>

/* 4 cEMI */
#include <KNX/03/06/03/cEMI.h>
