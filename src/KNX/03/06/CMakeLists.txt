# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/01_Application_Programmers_Interface.h
        ${CMAKE_CURRENT_SOURCE_DIR}/02_Physical_External_Interface.h
        ${CMAKE_CURRENT_SOURCE_DIR}/03_External_Message_Interface.h)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/01_Application_Programmers_Interface.h
        ${CMAKE_CURRENT_SOURCE_DIR}/02_Physical_External_Interface.h
        ${CMAKE_CURRENT_SOURCE_DIR}/03_External_Message_Interface.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/06)

# sub directories
#add_subdirectory(01)
#add_subdirectory(02)
add_subdirectory(03)
