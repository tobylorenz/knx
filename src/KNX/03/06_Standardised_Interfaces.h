// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 Application Programmer's Interface */
#include <KNX/03/06/01_Application_Programmers_Interface.h>

/* 2 Physical External Interface */
#include <KNX/03/06/02_Physical_External_Interface.h>

/* 3 External Message Interface */
#include <KNX/03/06/03_External_Message_Interface.h>
