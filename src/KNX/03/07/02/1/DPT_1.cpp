// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_1::DPT_1(const uint16_t subnumber) :
    Datapoint_Type(1, subnumber)
{
}

void DPT_1::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    b = *first++ & 0x01;

    assert(first == last);
}

std::vector<uint8_t> DPT_1::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(b);

    return data;
}

}
