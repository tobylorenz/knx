// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 1.* 1-bit
 *
 * Datapoint Types "B1"
 *
 * @ingroup KNX_03_07_02_03_01
 * @ingroup KNX_03_07_02_04_02
 * @ingroup KNX_03_07_02_09_01
 */
class KNX_EXPORT DPT_1 : public Datapoint_Type
{
public:
    explicit DPT_1(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** b */
    bool b{false};
};

}
