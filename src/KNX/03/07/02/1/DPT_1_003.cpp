// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_003.h>

namespace KNX {

DPT_1_003::DPT_1_003() :
    DPT_1(3)
{
}

std::string DPT_1_003::text() const
{
    return b ? "Enable" : "Disable";
}

}
