// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_004.h>

namespace KNX {

DPT_1_004::DPT_1_004() :
    DPT_1(4)
{
}

std::string DPT_1_004::text() const
{
    return b ? "Ramp" : "No ramp";
}

}
