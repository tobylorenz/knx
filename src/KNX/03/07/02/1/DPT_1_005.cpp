// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_005.h>

namespace KNX {

DPT_1_005::DPT_1_005() :
    DPT_1(5)
{
}

std::string DPT_1_005::text() const
{
    return b ? "Alarm" : "No alarm";
}

}
