// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_007.h>

namespace KNX {

DPT_1_007::DPT_1_007() :
    DPT_1(7)
{
}

std::string DPT_1_007::text() const
{
    return b ? "Increase" : "Decrease";
}

}
