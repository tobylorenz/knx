// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_009.h>

namespace KNX {

DPT_1_009::DPT_1_009() :
    DPT_1(9)
{
}

std::string DPT_1_009::text() const
{
    return b ? "Close" : "Open";
}

}
