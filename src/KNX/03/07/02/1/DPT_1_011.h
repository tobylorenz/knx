// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/1/DPT_1.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 1.011 state
 *
 * @ingroup KNX_03_07_02_03_01
 */
class KNX_EXPORT DPT_1_011 : public DPT_1
{
public:
    explicit DPT_1_011();

    std::string text() const override;
};

using DPT_State = DPT_1_011;

}
