// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_014.h>

namespace KNX {

DPT_1_014::DPT_1_014() :
    DPT_1(14)
{
}

std::string DPT_1_014::text() const
{
    return b ? "Calculated" : "Fixed";
}

}
