// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_017.h>

namespace KNX {

DPT_1_017::DPT_1_017() :
    DPT_1(17)
{
}

std::string DPT_1_017::text() const
{
    return "trigger"; // same text for b=true/false
}

}
