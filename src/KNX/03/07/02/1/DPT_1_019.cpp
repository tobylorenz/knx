// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_019.h>

namespace KNX {

DPT_1_019::DPT_1_019() :
    DPT_1(19)
{
}

std::string DPT_1_019::text() const
{
    return b ? "open" : "closed";
}

}
