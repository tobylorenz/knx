// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_022.h>

namespace KNX {

DPT_1_022::DPT_1_022() :
    DPT_1(22)
{
}

std::string DPT_1_022::text() const
{
    return b ? "scene B" : "scene A";
}

}
