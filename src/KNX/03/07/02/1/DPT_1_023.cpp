// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_023.h>

namespace KNX {

DPT_1_023::DPT_1_023() :
    DPT_1(23)
{
}

std::string DPT_1_023::text() const
{
    return b ? "move Up/Down + StepStop mode (blind)" : "only move Up/Down mode (shutter)";
}

}
