// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/1/DPT_1_1200.h>

namespace KNX {

DPT_1_1200::DPT_1_1200() :
    DPT_1(1200)
{
}

std::string DPT_1_1200::text() const
{
    return b ? "Producer" : "Consumer";
}

}
