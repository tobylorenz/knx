// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/10/DPT_10.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_10::DPT_10(const uint16_t subnumber) :
    Datapoint_Type(10, subnumber)
{
}

void DPT_10::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    day = *first >> 5;
    hour = *first++ & 0x1f;

    minutes = *first++ & 0x3f;

    seconds = *first++ & 0x3f;

    assert(first == last);
}

std::vector<uint8_t> DPT_10::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((day << 5) | (hour & 0x1f));
    data.push_back(minutes & 0x3f);
    data.push_back(seconds & 0x3f);

    return data;
}

}
