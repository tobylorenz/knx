// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 10.* time
 *
 * Datapoint Types "N3N5r2N6r2N6"
 *
 * @ingroup KNX_03_07_02_03_11
 */
class KNX_EXPORT DPT_10 : public Datapoint_Type
{
public:
    explicit DPT_10(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** day (0..7) */
    uint3_t day{};

    /** hour (0..23) */
    uint5_t hour{};

    /** minutes (0..59) */
    uint6_t minutes{};

    /** seconds (0..59) */
    uint6_t seconds{};
};

}
