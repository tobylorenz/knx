// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/10/DPT_10_001.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_10_001::DPT_10_001() :
    DPT_10(1)
{
}

std::string DPT_10_001::text() const
{
    std::ostringstream oss;

    switch (day) {
    case 0:
        oss << "no day, ";
        break;
    case 1:
        oss << "Monday, ";
        break;
    case 2:
        oss << "Tuesday, ";
        break;
    case 3:
        oss << "Wednesday, ";
        break;
    case 4:
        oss << "Thursday, ";
        break;
    case 5:
        oss << "Friday, ";
        break;
    case 6:
        oss << "Saturday, ";
        break;
    case 7:
        oss << "Sunday, ";
        break;
    }

    oss << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(hour) << ':'
        << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(minutes) << ':'
        << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(seconds);

    return oss.str();
}

}
