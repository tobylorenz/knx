// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/10/DPT_10.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 10.001 time of day
 *
 * @ingroup KNX_03_07_02_03_11
 */
class KNX_EXPORT DPT_10_001 : public DPT_10
{
public:
    explicit DPT_10_001();

    std::string text() const override;
};

using DPT_TimeOfDay = DPT_10_001;

}
