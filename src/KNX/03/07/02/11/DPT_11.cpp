// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/11/DPT_11.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_11::DPT_11(const uint16_t subnumber) :
    Datapoint_Type(11, subnumber)
{
}

void DPT_11::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    day = *first++ & 0x1f;
    month = *first++ & 0x0f;
    year = *first++ & 0x7f;

    assert(first == last);
}

std::vector<uint8_t> DPT_11::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(day & 0x1f);
    data.push_back(month & 0x0f);
    data.push_back(year & 0x7f);

    return data;
}

}
