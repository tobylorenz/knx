// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 11.* date
 *
 * Datapoint Types "r3N5r3N4r1U7"
 *
 * @ingroup KNX_03_07_02_03_12
 */
class KNX_EXPORT DPT_11 : public Datapoint_Type
{
public:
    explicit DPT_11(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** day (1..31) */
    uint5_t day{};

    /** month (1..12) */
    uint4_t month{};

    /**
     * year (0..99)
     *
     * 0=2000..89=2089
     * 90=1990..99=1999
     */
    uint7_t year{};
};

}
