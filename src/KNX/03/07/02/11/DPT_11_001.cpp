// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/11/DPT_11_001.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_11_001::DPT_11_001() :
    DPT_11(1)
{
}

std::string DPT_11_001::text() const
{
    std::ostringstream oss;

    const uint16_t year4 = (year < 90) ? (2000 + year) : (1900 + year);
    oss << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(day)
        << '.'
        << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(month)
        << '.'
        << std::setw(4) << std::setfill('0') << static_cast<uint16_t>(year4);

    return oss.str();
}

}
