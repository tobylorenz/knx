// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/11/DPT_11.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 11.001 date
 *
 * @ingroup KNX_03_07_02_03_12
 */
class KNX_EXPORT DPT_11_001 : public DPT_11
{
public:
    explicit DPT_11_001();

    std::string text() const override;
};

using DPT_Date = DPT_11_001;

}
