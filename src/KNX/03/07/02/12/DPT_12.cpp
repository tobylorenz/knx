// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/12/DPT_12.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_12::DPT_12(const uint16_t subnumber) :
    Datapoint_Type(12, subnumber)
{
}

void DPT_12::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    unsigned_value = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_12::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(unsigned_value >> 24);
    data.push_back((unsigned_value >> 16) & 0xff);
    data.push_back((unsigned_value >> 8) & 0xff);
    data.push_back(unsigned_value & 0xff);

    return data;
}

}
