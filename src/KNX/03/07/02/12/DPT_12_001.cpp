// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/12/DPT_12_001.h>

#include <sstream>

namespace KNX {

DPT_12_001::DPT_12_001() :
    DPT_12(1)
{
}

std::string DPT_12_001::text() const
{
    std::ostringstream oss;
    oss << unsigned_value << " counter pulses";
    return oss.str();
}

}
