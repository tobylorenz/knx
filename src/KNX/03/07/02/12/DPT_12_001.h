// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/12/DPT_12.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 12.001 counter pulses (unsigned)
 *
 * @ingroup KNX_03_07_02_03_13_01
 */
class KNX_EXPORT DPT_12_001 : public DPT_12
{
public:
    explicit DPT_12_001();

    std::string text() const override;
};

using DPT_Value_4_Ucount = DPT_12_001;

}
