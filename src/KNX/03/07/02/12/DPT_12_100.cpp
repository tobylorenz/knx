// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/12/DPT_12_100.h>

#include <sstream>

namespace KNX {

DPT_12_100::DPT_12_100() :
    DPT_12(100)
{
}

std::string DPT_12_100::text() const
{
    std::ostringstream oss;
    oss << unsigned_value << " s";
    return oss.str();
}

}
