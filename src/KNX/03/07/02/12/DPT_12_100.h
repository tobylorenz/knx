// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/12/DPT_12.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 12.100 counter timesec (s)
 *
 * @ingroup KNX_03_07_02_03_13_02
 */
class KNX_EXPORT DPT_12_100 : public DPT_12
{
public:
    explicit DPT_12_100();

    std::string text() const override;
};

using DPT_LongTimePeriod_Sec = DPT_12_100;

}
