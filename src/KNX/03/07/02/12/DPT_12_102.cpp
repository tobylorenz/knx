// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/12/DPT_12_102.h>

#include <sstream>

namespace KNX {

DPT_12_102::DPT_12_102() :
    DPT_12(102)
{
}

std::string DPT_12_102::text() const
{
    std::ostringstream oss;
    oss << unsigned_value << " h";
    return oss.str();
}

}
