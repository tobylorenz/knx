// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/12/DPT_12_1200.h>

#include <sstream>

namespace KNX {

DPT_12_1200::DPT_12_1200() :
    DPT_12(1200)
{
}

std::string DPT_12_1200::text() const
{
    std::ostringstream oss;
    oss << unsigned_value << " litre";
    return oss.str();
}

}
