// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/13/DPT_13.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_13::DPT_13(const uint16_t subnumber) :
    Datapoint_Type(13, subnumber)
{
}

void DPT_13::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    signed_value = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_13::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(signed_value >> 24);
    data.push_back((signed_value >> 16) & 0xff);
    data.push_back((signed_value >> 8) & 0xff);
    data.push_back(signed_value & 0xff);

    return data;
}

}
