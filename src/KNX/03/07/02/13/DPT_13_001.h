// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/13/DPT_13.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 13.001 counter pulses (signed)
 *
 * @ingroup KNX_03_07_02_03_14_01
 */
class KNX_EXPORT DPT_13_001 : public DPT_13
{
public:
    explicit DPT_13_001();

    std::string text() const override;
};

using DPT_Value_4_Count = DPT_13_001;

}
