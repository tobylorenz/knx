// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/13/DPT_13_002.h>

#include <sstream>

namespace KNX {

DPT_13_002::DPT_13_002() :
    DPT_13(2)
{
}

std::string DPT_13_002::text() const
{
    std::ostringstream oss;
    oss << signed_value << " m³/h";
    return oss.str();
}

}
