// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/13/DPT_13.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 13.014 apparant energy (kVAh)
 *
 * @ingroup KNX_03_07_02_03_14_02
 */
class KNX_EXPORT DPT_13_014 : public DPT_13
{
public:
    explicit DPT_13_014();

    std::string text() const override;
};

using DPT_ApparantEnergy_kVAh = DPT_13_014;

}
