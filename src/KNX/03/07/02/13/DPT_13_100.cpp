// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/13/DPT_13_100.h>

#include <sstream>

namespace KNX {

DPT_13_100::DPT_13_100() :
    DPT_13(100)
{
}

std::string DPT_13_100::text() const
{
    std::ostringstream oss;
    oss << signed_value << " s";
    return oss.str();
}

}
