// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/13/DPT_13.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 13.1200 delta volume liquid (l)
 *
 * @ingroup KNX_03_07_02_09_03
 */
class KNX_EXPORT DPT_13_1200 : public DPT_13
{
public:
    explicit DPT_13_1200();

    std::string text() const override;
};

using DPT_DeltaVolumeLiquid_Litre = DPT_13_1200;

}
