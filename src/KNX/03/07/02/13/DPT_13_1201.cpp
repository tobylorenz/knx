// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/13/DPT_13_1201.h>

#include <sstream>

namespace KNX {

DPT_13_1201::DPT_13_1201() :
    DPT_13(1201)
{
}

std::string DPT_13_1201::text() const
{
    std::ostringstream oss;
    oss << signed_value << " m³";
    return oss.str();
}

}
