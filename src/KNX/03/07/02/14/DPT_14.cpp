// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14.h>

#include <cassert>
#include <iomanip>
#include <sstream>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_14::DPT_14(const uint16_t subnumber) :
    Datapoint_Type(14, subnumber)
{
}

void DPT_14::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    union {
        float f;
        uint32_t u{};
    };

    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    float_value = f;

    assert(first == last);
}

std::vector<uint8_t> DPT_14::toData() const
{
    std::vector<uint8_t> data;

    union {
        float f;
        uint32_t u{};
    };

    f = float_value;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);

    return data;
}

std::string DPT_14::text() const
{
    std::ostringstream oss;
    oss << std::fixed << std::setprecision(2) << float_value << unit();
    return oss.str();
}

}
