// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 14.* 4-byte float value
 *
 * Datapoint Types "F32"
 *
 * @ingroup KNX_03_07_02_03_15
 * @ingroup KNX_03_07_02_09_04
 */
class KNX_EXPORT DPT_14 : public Datapoint_Type
{
public:
    explicit DPT_14(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** float value */
    float float_value{0.0};

    /** unit */
    virtual std::string unit() const = 0;
};

}
