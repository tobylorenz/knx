// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_000.h>

namespace KNX {

DPT_14_000::DPT_14_000() :
    DPT_14(0)
{
}

std::string DPT_14_000::unit() const
{
    return " m/s²";
}

}
