// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_002.h>

namespace KNX {

DPT_14_002::DPT_14_002() :
    DPT_14(2)
{
}

std::string DPT_14_002::unit() const
{
    return " J/mol";
}

}
