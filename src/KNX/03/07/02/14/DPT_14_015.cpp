// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_015.h>

namespace KNX {

DPT_14_015::DPT_14_015() :
    DPT_14(15)
{
}

std::string DPT_14_015::unit() const
{
    return " S";
}

}
