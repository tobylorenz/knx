// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_017.h>

namespace KNX {

DPT_14_017::DPT_14_017() :
    DPT_14(17)
{
}

std::string DPT_14_017::unit() const
{
    return " kg/m³";
}

}
