// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_022.h>

namespace KNX {

DPT_14_022::DPT_14_022() :
    DPT_14(22)
{
}

std::string DPT_14_022::unit() const
{
    return " C/m²";
}

}
