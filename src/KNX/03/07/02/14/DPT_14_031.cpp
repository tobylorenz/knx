// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_031.h>

namespace KNX {

DPT_14_031::DPT_14_031() :
    DPT_14(31)
{
}

std::string DPT_14_031::unit() const
{
    return " J";
}

}
