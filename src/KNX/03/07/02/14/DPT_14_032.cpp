// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_032.h>

namespace KNX {

DPT_14_032::DPT_14_032() :
    DPT_14(32)
{
}

std::string DPT_14_032::unit() const
{
    return " N";
}

}
