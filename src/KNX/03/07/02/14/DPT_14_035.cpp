// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_035.h>

namespace KNX {

DPT_14_035::DPT_14_035() :
    DPT_14(35)
{
}

std::string DPT_14_035::unit() const
{
    return " J/K";
}

}
