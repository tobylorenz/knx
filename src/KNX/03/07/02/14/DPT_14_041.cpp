// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_041.h>

namespace KNX {

DPT_14_041::DPT_14_041() :
    DPT_14(41)
{
}

std::string DPT_14_041::unit() const
{
    return " cd/m²";
}

}
