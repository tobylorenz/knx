// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_047.h>

namespace KNX {

DPT_14_047::DPT_14_047() :
    DPT_14(47)
{
}

std::string DPT_14_047::unit() const
{
    return " Am²";
}

}
