// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_052.h>

namespace KNX {

DPT_14_052::DPT_14_052() :
    DPT_14(52)
{
}

std::string DPT_14_052::unit() const
{
    return " kg/s";
}

}
