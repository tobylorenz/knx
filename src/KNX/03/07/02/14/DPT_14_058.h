// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/14/DPT_14.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 14.058 pressure (Pa)
 *
 * @ingroup KNX_03_07_02_03_15
 */
class KNX_EXPORT DPT_14_058 : public DPT_14
{
public:
    explicit DPT_14_058();

    std::string unit() const override;
};

using DPT_Value_Pressure = DPT_14_058;

}
