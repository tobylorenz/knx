// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_063.h>

namespace KNX {

DPT_14_063::DPT_14_063() :
    DPT_14(63)
{
}

std::string DPT_14_063::unit() const
{
    return " sr";
}

}
