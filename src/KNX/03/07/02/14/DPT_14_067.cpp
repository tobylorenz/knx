// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_067.h>

namespace KNX {

DPT_14_067::DPT_14_067() :
    DPT_14(67)
{
}

std::string DPT_14_067::unit() const
{
    return " N/m";
}

}
