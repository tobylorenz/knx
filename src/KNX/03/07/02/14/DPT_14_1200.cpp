// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/14/DPT_14_1200.h>

namespace KNX {

DPT_14_1200::DPT_14_1200() :
    DPT_14(1200)
{
}

std::string DPT_14_1200::unit() const
{
    return " m³/h";
}

}
