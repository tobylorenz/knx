// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/14/DPT_14.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 14.1200 volume flux for meters (m³/h)
 *
 * @ingroup KNX_03_07_02_09_04
 */
class KNX_EXPORT DPT_14_1200 : public DPT_14
{
public:
    explicit DPT_14_1200();

    std::string unit() const override;
};

using DPT_Volume_Flux_Meter = DPT_14_1200;

}
