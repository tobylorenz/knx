// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/15/DPT_15.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_15::DPT_15(const uint16_t subnumber) :
    Datapoint_Type(15, subnumber)
{
}

void DPT_15::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    digits[0] = *first >> 4;
    digits[1] = *first & 0x0f;
    ++first;

    digits[2] = *first >> 4;
    digits[3] = *first & 0x0f;
    ++first;

    digits[4] = *first >> 4;
    digits[5] = *first & 0x0f;
    ++first;

    e = *first >> 7;
    p = (*first >> 6) & 0x01;
    d = (*first >> 5) & 0x01;
    c = (*first >> 4) & 0x01;
    index = *first & 0x0f;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_15::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(((digits[0] & 0x0f) << 4) |
                   (digits[1] & 0x0f));
    data.push_back(((digits[2] & 0x0f) << 4) |
                   (digits[3] & 0x0f));
    data.push_back(((digits[4] & 0x0f) << 4) |
                   (digits[5] & 0x0f));
    data.push_back((e << 7) | (p << 6) | (d << 5) | (c << 4) | (index & 0xff));

    return data;
}

}
