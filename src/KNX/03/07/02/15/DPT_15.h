// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 15.* entrance access
 *
 * Datapoint Types "U4U4U4U4U4U4B4N4"
 *
 * @ingroup KNX_03_07_02_03_16
 */
class KNX_EXPORT DPT_15 : public Datapoint_Type
{
public:
    explicit DPT_15(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** d */
    std::array<uint8_t, 6> digits{};

    /** e */
    bool e{false};

    /** p */
    bool p{false};

    /** d */
    bool d{false};

    /** c */
    bool c{false};

    /** index */
    uint4_t index{}; // actually an Enum, but defined in derived classes
};

}
