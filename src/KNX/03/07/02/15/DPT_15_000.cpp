// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/15/DPT_15_000.h>

#include <sstream>

namespace KNX {

DPT_15_000::DPT_15_000() :
    DPT_15(0)
{
}

std::string DPT_15_000::text() const
{
    std::ostringstream oss;
    oss << static_cast<uint16_t>(index)
        << ":"
        << static_cast<uint16_t>(digits[0])
        << static_cast<uint16_t>(digits[1])
        << static_cast<uint16_t>(digits[2])
        << static_cast<uint16_t>(digits[3])
        << static_cast<uint16_t>(digits[4])
        << static_cast<uint16_t>(digits[5])
        << " ("
        << (e ? 'E' : '-')
        << (p ? 'P' : '-')
        << (d ? 'D' : '-')
        << (c ? 'C' : '-')
        << ") = ("
        << "Detection error: " << (e ? "reading of access information code was not successful" : "no error")
        << ", Permission: " << (p ? "accepted" : "not accepted")
        << ", Read direction: " << (d ? "right to left" : "left to right")
        << ", Encryption of access information: " << (c ? "yes" : "no")
        << ")";
    return oss.str();
}

}
