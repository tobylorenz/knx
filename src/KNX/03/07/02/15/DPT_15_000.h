// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/15/DPT_15.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 15.000 entrance access
 *
 * @ingroup KNX_03_07_02_03_16
 */
class KNX_EXPORT DPT_15_000 : public DPT_15
{
public:
    explicit DPT_15_000();

    std::string text() const override;
};

using DPT_Access_Data = DPT_15_000;

}
