// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/16/DPT_16.h>

#include <cassert>

#include <KNX/03/07/02/ISO_8859_1.h>
#include <KNX/Exceptions.h>

namespace KNX {

DPT_16::DPT_16(const uint16_t subnumber) :
    Datapoint_Type(16, subnumber)
{
}

void DPT_16::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 14) {
        throw DataDoesntMatchDPTException(14, data_size);
    }

    std::copy(first, last, std::begin(characters));
    first += 14;

    assert(first == last);
}

std::vector<uint8_t> DPT_16::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(characters), std::cend(characters));

    return data;
}

std::string DPT_16::text() const
{
    std::string retval;

    /* copy all characters != 0 */
    for (auto c = std::cbegin(characters); c != std::cend(characters) && *c; ++c) {
        retval.append(iso_8859_1_to_utf_8.at(*c));
    }

    return retval;
}

}
