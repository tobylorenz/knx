// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 16.* character string
 *
 * Datapoint Types "A112"
 *
 * @ingroup KNX_03_07_02_03_17
 */
class KNX_EXPORT DPT_16 : public Datapoint_Type
{
public:
    explicit DPT_16(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** Character 1..14 */
    std::array<char, 14> characters{};
};

}
