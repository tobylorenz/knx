// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/16/DPT_16.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 16.000 Character String (ASCII)
 *
 * @ingroup KNX_03_07_02_03_17
 */
class KNX_EXPORT DPT_16_000 : public DPT_16
{
public:
    explicit DPT_16_000();
};

using DPT_String_ASCII = DPT_16_000;

}
