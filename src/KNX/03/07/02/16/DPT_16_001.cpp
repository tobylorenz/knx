// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/16/DPT_16_001.h>

namespace KNX {

DPT_16_001::DPT_16_001() :
    DPT_16(1)
{
}

}
