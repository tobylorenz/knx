// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/16/DPT_16.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 16.001 Character String (ISO 8859-1)
 *
 * @ingroup KNX_03_07_02_03_17
 */
class KNX_EXPORT DPT_16_001 : public DPT_16
{
public:
    explicit DPT_16_001();
};

using DPT_String_8859_1 = DPT_16_001;

}
