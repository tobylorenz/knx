// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/17/DPT_17.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_17::DPT_17(const uint16_t subnumber) :
    Datapoint_Type(17, subnumber)
{
}

void DPT_17::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    scene_number = *first++ & 0x3f;

    assert(first == last);
}

std::vector<uint8_t> DPT_17::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(scene_number & 0x3f);

    return data;
}

}
