// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 17.* scene number
 *
 * Datapoint Types "r2U6"
 *
 * @ingroup KNX_03_07_02_03_18
 */
class KNX_EXPORT DPT_17 : public Datapoint_Type
{
public:
    explicit DPT_17(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** scene number */
    uint6_t scene_number{};
};

}
