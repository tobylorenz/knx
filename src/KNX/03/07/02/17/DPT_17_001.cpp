// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/17/DPT_17_001.h>

#include <sstream>

namespace KNX {

DPT_17_001::DPT_17_001() :
    DPT_17(1)
{
}

std::string DPT_17_001::text() const
{
    std::ostringstream oss;
    oss << static_cast<uint16_t>(scene_number + 1);
    return oss.str();
}

}
