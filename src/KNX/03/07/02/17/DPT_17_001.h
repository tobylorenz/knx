// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/17/DPT_17.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 17.001 scene number
 *
 * @ingroup KNX_03_07_02_03_18
 */
class KNX_EXPORT DPT_17_001 : public DPT_17
{
public:
    explicit DPT_17_001();

    std::string text() const override;
};

using DPT_SceneNumber = DPT_17_001;

}
