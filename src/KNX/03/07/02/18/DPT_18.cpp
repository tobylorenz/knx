// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/18/DPT_18.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_18::DPT_18(const uint16_t subnumber) :
    Datapoint_Type(18, subnumber)
{
}

void DPT_18::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    c = *first >> 7;
    scene_number = *first & 0x3f;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_18::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((c << 7) | (scene_number & 0x3f));

    return data;
}

}
