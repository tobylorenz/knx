// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 18.* scene control
 *
 * Datapoint Types "B1r1U6"
 *
 * @ingroup KNX_03_07_02_03_19
 */
class KNX_EXPORT DPT_18 : public Datapoint_Type
{
public:
    explicit DPT_18(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** c */
    bool c{false};

    /** scene number */
    uint6_t scene_number{};
};

}
