// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/18/DPT_18_001.h>

#include <sstream>

namespace KNX {

DPT_18_001::DPT_18_001() :
    DPT_18(1)
{
}

std::string DPT_18_001::text() const
{
    std::ostringstream oss;
    oss << (c ? "Learn " : "Activate ")
        << static_cast<uint16_t>(scene_number + 1);
    return oss.str();
}

}
