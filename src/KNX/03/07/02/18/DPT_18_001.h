// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/18/DPT_18.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 18.001 scene control
 *
 * @ingroup KNX_03_07_02_03_19
 */
class KNX_EXPORT DPT_18_001 : public DPT_18
{
public:
    explicit DPT_18_001();

    std::string text() const override;
};

using DPT_SceneControl = DPT_18_001;

}
