// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/19/DPT_19.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_19::DPT_19(const uint16_t subnumber) :
    Datapoint_Type(19, subnumber)
{
}

void DPT_19::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 8) {
        throw DataDoesntMatchDPTException(8, data_size);
    }

    year = *first++;

    month = *first++ & 0x0f;

    day_of_month = *first++ & 0x1f;

    day_of_week = *first >> 5;
    hour_of_day = *first & 0x1f;
    ++first;

    minutes = *first++ & 0x3f;

    seconds = *first++ & 0x3f;

    f = *first >> 7;
    wd = (*first >> 6) & 0x01;
    nwd = (*first >> 5) & 0x01;
    ny = (*first >> 4) & 0x01;
    nd = (*first >> 3) & 0x01;
    ndow = (*first >> 2) & 0x01;
    nt = (*first >> 1) & 0x01;
    suti = (*first >> 0) & 0x01;
    ++first;

    clq = *first >> 7;
    src = *first >> 7;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_19::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(year);

    data.push_back(month & 0x0f);

    data.push_back(day_of_month & 0x1f);

    data.push_back(
        ((day_of_week & 0x07) << 5) |
        (hour_of_day & 0x1f));

    data.push_back(minutes & 0x3f);

    data.push_back(seconds & 0x3f);

    data.push_back(
        (f << 7) |
        (wd << 6) |
        (nwd << 5) |
        (ny << 4) |
        (nd << 3) |
        (ndow << 2) |
        (nt << 1) |
        (suti << 0));

    data.push_back(
        (clq << 7) |
        (src << 6));

    return data;
}

}
