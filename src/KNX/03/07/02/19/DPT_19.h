// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 19.* Date Time
 *
 * Datapoint Types "U8[r4U4][r3U5][U3U5][r2U6][r2U6]B16"
 *
 * @ingroup KNX_03_07_02_03_20
 */
class KNX_EXPORT DPT_19 : public Datapoint_Type
{
public:
    explicit DPT_19(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** year (0..255) */
    uint8_t year{};

    /** month (1..12) */
    uint4_t month{};

    /** day of month (1..31) */
    uint5_t day_of_month{};

    /** day of week (0..7) */
    uint3_t day_of_week{};

    /** hour of day (0..23) */
    uint5_t hour_of_day{};

    /** minutes (0..59) */
    uint6_t minutes{};

    /** seconds (0..59) */
    uint6_t seconds{};

    /** f */
    bool f{false};

    /** wd */
    bool wd{false};

    /** nwd */
    bool nwd{false};

    /** ny */
    bool ny{false};

    /** nd */
    bool nd{false};

    /** ndow */
    bool ndow{false};

    /** nt */
    bool nt{false};

    /** suti */
    bool suti{false};

    /** clq */
    bool clq{false};

    /** src */
    bool src{false};
};

}
