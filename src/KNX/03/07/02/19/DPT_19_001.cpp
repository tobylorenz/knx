// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/19/DPT_19_001.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_19_001::DPT_19_001() :
    DPT_19(1)
{
}

std::string DPT_19_001::text() const
{
    std::ostringstream oss;

    switch (day_of_week) {
    case 0:
        oss << "Any day, ";
        break;
    case 1:
        oss << "Monday, ";
        break;
    case 2:
        oss << "Tuesday, ";
        break;
    case 3:
        oss << "Wednesday, ";
        break;
    case 4:
        oss << "Thursday, ";
        break;
    case 5:
        oss << "Friday, ";
        break;
    case 6:
        oss << "Saturday, ";
        break;
    case 7:
        oss << "Sunday, ";
        break;
    }

    oss << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(day_of_month)
        << '.'
        << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(month)
        << '.'
        << std::setw(4) << std::setfill('0') << static_cast<uint16_t>(year + 1900)
        << ", "
        << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(hour_of_day)
        << ':'
        << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(minutes)
        << ':'
        << std::setw(2) << std::setfill('0') << static_cast<uint16_t>(seconds)
        << " ("
        << (f ? 'F' : '-')
        << (wd ? 'W' : '-')
        << (nwd ? 'V' : '-')
        << (ny ? 'Y' : '-')
        << (nd ? 'D' : '-')
        << (ndow ? 'O' : '-')
        << (nt ? 'T' : '-')
        << (suti ? 'S' : '-')
        << (clq ? 'Q' : '-')
        << (src ? 'R' : '-')
        << ") = ("
        << "Fault: " << (f ? "Fault" : "Normal (no fault)")
        << ", Working Day: " << (wd ? "Working day" : "Bank Holiday (No working day)")
        << ", No WD: " << (nwd ? "WD field not valid" : "WD field valid")
        << ", No Year: " << (ny ? "Year field not valid" : "Year field valid")
        << ", No Date: " << (nd ? "Month and Day of Month field not valid" : "Month and Day of Month field valid")
        << ", No Day of Week: " << (ndow ? "Day of week field not valid" : "Day of week field valid")
        << ", No Time: " << (nt ? "Hour of day, Minutes and Seconds field not valid" : "Hour of day, Minutes and Seconds field valid")
        << ", Standard Summer Time: " << (suti ? "Time = UT+X+1" : "Time = UT+X")
        << ", Quality of clock: " << (clq ? "clock with ext. sync signal" : "clock without ext. sync signal")
        << ", Synchronisation source reliability: " << (src ? "reliable synchronisation source (radio, Internet)" : "unreliable synchronisation source (mains, local quartz)")
        << ")";

    return oss.str();
}

}
