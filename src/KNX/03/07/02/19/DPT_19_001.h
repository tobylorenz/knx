// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/19/DPT_19.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 19.001 date time
 *
 * @ingroup KNX_03_07_02_03_20
 */
class KNX_EXPORT DPT_19_001 : public DPT_19
{
public:
    explicit DPT_19_001();

    std::string text() const override;
};

using DPT_DateTime = DPT_19_001;

}
