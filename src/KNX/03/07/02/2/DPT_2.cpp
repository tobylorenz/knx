// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/2/DPT_2.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_2::DPT_2(const uint16_t subnumber) :
    Datapoint_Type(2, subnumber)
{
}

void DPT_2::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    c = (*first >> 1) & 0x01;
    v = *first & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_2::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (c << 1) |
        (v << 0));

    return data;
}

}
