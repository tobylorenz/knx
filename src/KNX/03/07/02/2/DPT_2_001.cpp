// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/2/DPT_2_001.h>

#include <sstream>

namespace KNX {

DPT_2_001::DPT_2_001() :
    DPT_2(1)
{
}

std::string DPT_2_001::text() const
{
    std::ostringstream oss;
    oss
            << (c ? "control" : "no control")
            << ", "
            << (v ? "On" : "Off");
    return oss.str();
}

}
