// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/2/DPT_2_003.h>

#include <sstream>

namespace KNX {

DPT_2_003::DPT_2_003() :
    DPT_2(3)
{
}

std::string DPT_2_003::text() const
{
    std::ostringstream oss;
    oss
            << (c ? "control" : "no control")
            << ", "
            << (v ? "Enable" : "Disable");
    return oss.str();
}

}
