// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/2/DPT_2_004.h>

#include <sstream>

namespace KNX {

DPT_2_004::DPT_2_004() :
    DPT_2(4)
{
}

std::string DPT_2_004::text() const
{
    std::ostringstream oss;
    oss
            << (c ? "control" : "no control")
            << ", "
            << (v ? "Ramp" : "No ramp");
    return oss.str();
}

}
