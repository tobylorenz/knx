// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/2/DPT_2.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 2.006 binary value control
 *
 * @ingroup KNX_03_07_02_03_02
 */
class KNX_EXPORT DPT_2_006 : public DPT_2
{
public:
    explicit DPT_2_006();

    std::string text() const override;
};

using DPT_BinaryValue_Control = DPT_2_006;

}
