// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/2/DPT_2_009.h>

#include <sstream>

namespace KNX {

DPT_2_009::DPT_2_009() :
    DPT_2(9)
{
}

std::string DPT_2_009::text() const
{
    std::ostringstream oss;
    oss
            << (c ? "control" : "no control")
            << ", "
            << (v ? "Close" : "Open");
    return oss.str();
}

}
