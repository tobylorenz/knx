// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/2/DPT_2_010.h>

#include <sstream>

namespace KNX {

DPT_2_010::DPT_2_010() :
    DPT_2(10)
{
}

std::string DPT_2_010::text() const
{
    std::ostringstream oss;
    oss
            << (c ? "control" : "no control")
            << ", "
            << (v ? "Start" : "Stop");
    return oss.str();
}

}
