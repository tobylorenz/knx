// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/2/DPT_2_011.h>

#include <sstream>

namespace KNX {

DPT_2_011::DPT_2_011() :
    DPT_2(11)
{
}

std::string DPT_2_011::text() const
{
    std::ostringstream oss;
    oss
            << (c ? "control" : "no control")
            << ", "
            << (v ? "Active" : "Inactive");
    return oss.str();
}

}
