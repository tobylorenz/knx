// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/2/DPT_2_012.h>

#include <sstream>

namespace KNX {

DPT_2_012::DPT_2_012() :
    DPT_2(12)
{
}

std::string DPT_2_012::text() const
{
    std::ostringstream oss;
    oss
            << (c ? "control" : "no control")
            << ", "
            << (v ? "Inverted" : "Not inverted");
    return oss.str();
}

}
