// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 20.* 1-byte
 *
 * Datapoint Types "N8"
 *
 * @ingroup KNX_03_07_02_03_21
 * @ingroup KNX_03_07_02_04_03
 * @ingroup KNX_03_07_02_06_03
 * @ingroup KNX_03_07_02_07_01
 * @ingroup KNX_03_07_02_08_01
 * @ingroup KNX_03_07_02_09_05
 */
class KNX_EXPORT DPT_20 : public Datapoint_Type
{
public:
    explicit DPT_20(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** field1 */
    uint8_t field1{};
};

}
