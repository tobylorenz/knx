// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_002.h>

#include <sstream>

namespace KNX {

DPT_20_002::DPT_20_002() :
    DPT_20(2)
{
}

std::string DPT_20_002::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Building in use";
        break;
    case 1:
        oss << "Building not used";
        break;
    case 2:
        oss << "Building protection";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
