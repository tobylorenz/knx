// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_003.h>

#include <sstream>

namespace KNX {

DPT_20_003::DPT_20_003() :
    DPT_20(3)
{
}

std::string DPT_20_003::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "occupied";
        break;
    case 1:
        oss << "standby";
        break;
    case 2:
        oss << "not occupied";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
