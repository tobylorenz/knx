// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_004.h>

#include <sstream>

namespace KNX {

DPT_20_004::DPT_20_004() :
    DPT_20(4)
{
}

std::string DPT_20_004::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "High";
        break;
    case 1:
        oss << "Medium";
        break;
    case 2:
        oss << "Low";
        break;
    case 3:
        oss << "'void'";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
