// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_005.h>

#include <sstream>

namespace KNX {

DPT_20_005::DPT_20_005() :
    DPT_20(5)
{
}

std::string DPT_20_005::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "normal";
        break;
    case 1:
        oss << "presence simulation";
        break;
    case 2:
        oss << "night round";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
