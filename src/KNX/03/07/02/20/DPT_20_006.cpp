// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_006.h>

#include <sstream>

namespace KNX {

DPT_20_006::DPT_20_006() :
    DPT_20(6)
{
}

std::string DPT_20_006::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "no fault";
        break;
    case 1:
        oss << "system and functions of common interest";
        break;
    case 10:
        oss << "HVAC general FBs";
        break;
    case 11:
        oss << "HVAC Hot Water Heating";
        break;
    case 12:
        oss << "HVAC Direct Electrical Heating";
        break;
    case 13:
        oss << "HVAC Terminal Units";
        break;
    case 14:
        oss << "HVAC VAC";
        break;
    case 20:
        oss << "Lighting";
        break;
    case 30:
        oss << "Security";
        break;
    case 40:
        oss << "Load Management";
        break;
    case 50:
        oss << "Shutters and blinds";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
