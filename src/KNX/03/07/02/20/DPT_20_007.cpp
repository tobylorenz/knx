// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_007.h>

#include <sstream>

namespace KNX {

DPT_20_007::DPT_20_007() :
    DPT_20(7)
{
}

std::string DPT_20_007::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "simple alarm";
        break;
    case 2:
        oss << "basic alarm";
        break;
    case 3:
        oss << "extended alarm";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
