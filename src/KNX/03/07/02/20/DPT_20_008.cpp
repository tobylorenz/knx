// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_008.h>

#include <sstream>

namespace KNX {

DPT_20_008::DPT_20_008() :
    DPT_20(8)
{
}

std::string DPT_20_008::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "disabled (PSU/DPSU fixed off)";
        break;
    case 1:
        oss << "enabled (PSU/DPSU fixed on)";
        break;
    case 2:
        oss << "auto (PSU/DPSU automatic on/off)";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
