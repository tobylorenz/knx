// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_011.h>

#include <sstream>

namespace KNX {

DPT_20_011::DPT_20_011() :
    DPT_20(11)
{
}

std::string DPT_20_011::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "no fault";
        break;
    case 1:
        oss << "general device fault (e.g. RAM, EEPROM, UI, watchdog, ...)";
        break;
    case 2:
        oss << "communication fault";
        break;
    case 3:
        oss << "configuration fault";
        break;
    case 4:
        oss << "hardware fault";
        break;
    case 5:
        oss << "software fault";
        break;
    case 6:
        oss << "insufficient non volatile memory";
        break;
    case 7:
        oss << "insufficient volatile memory";
        break;
    case 8:
        oss << "memory allocation command with size 0 received";
        break;
    case 9:
        oss << "CRC-error";
        break;
    case 10:
        oss << "watchdog reset detected";
        break;
    case 11:
        oss << "invalid opcode detected";
        break;
    case 12:
        oss << "general protection fault";
        break;
    case 13:
        oss << "maximal table length exceeded";
        break;
    case 14:
        oss << "undefined load command received";
        break;
    case 15:
        oss << "Group Address Table is not sorted";
        break;
    case 16:
        oss << "invalid connection number (TSAP)";
        break;
    case 17:
        oss << "invalid Group Object number (ASAP)";
        break;
    case 18:
        oss << "Group Object Type exceeds (PID_MAX_APDU_LENGTH - 2)";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
