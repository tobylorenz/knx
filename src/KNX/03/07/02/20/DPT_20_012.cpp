// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_012.h>

#include <sstream>

namespace KNX {

DPT_20_012::DPT_20_012() :
    DPT_20(12)
{
}

std::string DPT_20_012::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "no fault";
        break;
    case 1:
        oss << "sensor fault";
        break;
    case 2:
        oss << "process fault / controller fault";
        break;
    case 3:
        oss << "actuator fault";
        break;
    case 4:
        oss << "other fault";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
