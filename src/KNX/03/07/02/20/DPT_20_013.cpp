// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_013.h>

#include <sstream>

namespace KNX {

DPT_20_013::DPT_20_013() :
    DPT_20(13)
{
}

std::string DPT_20_013::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "not active";
        break;
    case 1:
        oss << "1 s";
        break;
    case 2:
        oss << "2 s";
        break;
    case 3:
        oss << "3 s";
        break;
    case 4:
        oss << "5 s";
        break;
    case 5:
        oss << "10 s";
        break;
    case 6:
        oss << "15 s";
        break;
    case 7:
        oss << "20 s";
        break;
    case 8:
        oss << "30 s";
        break;
    case 9:
        oss << "45 s";
        break;
    case 10:
        oss << "1 min";
        break;
    case 11:
        oss << "1,25 min";
        break;
    case 12:
        oss << "1,5 min";
        break;
    case 13:
        oss << "2 min";
        break;
    case 14:
        oss << "2,5 min";
        break;
    case 15:
        oss << "3 min";
        break;
    case 16:
        oss << "5 min";
        break;
    case 17:
        oss << "15 min";
        break;
    case 18:
        oss << "20 min";
        break;
    case 19:
        oss << "30 min";
        break;
    case 20:
        oss << "1 h";
        break;
    case 21:
        oss << "2 h";
        break;
    case 22:
        oss << "3 h";
        break;
    case 23:
        oss << "5 h";
        break;
    case 24:
        oss << "12 h";
        break;
    case 25:
        oss << "24 h";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
