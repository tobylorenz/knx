// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_014.h>

#include <sstream>

namespace KNX {

DPT_20_014::DPT_20_014() :
    DPT_20(14)
{
}

std::string DPT_20_014::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "calm (no wind)";
        break;
    case 1:
        oss << "light air";
        break;
    case 2:
        oss << "light breeze";
        break;
    case 3:
        oss << "gentle breeze";
        break;
    case 4:
        oss << "moderate breeze";
        break;
    case 5:
        oss << "fresh breeze";
        break;
    case 6:
        oss << "strong breeze";
        break;
    case 7:
        oss << "near gale / moderate gale";
        break;
    case 8:
        oss << "fresh gale";
        break;
    case 9:
        oss << "strong gale";
        break;
    case 10:
        oss << "whole gale / storm";
        break;
    case 11:
        oss << "violent storm";
        break;
    case 12:
        oss << "hurricane";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
