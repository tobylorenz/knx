// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_017.h>

#include <sstream>

namespace KNX {

DPT_20_017::DPT_20_017() :
    DPT_20(17)
{
}

std::string DPT_20_017::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "inactive";
        break;
    case 1:
        oss << "digital input not inverted";
        break;
    case 2:
        oss << "digital input inverted";
        break;
    case 3:
        oss << "analog input -> 0% to 100%";
        break;
    case 4:
        oss << "temperature sensor input";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
