// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_021.h>

#include <sstream>

namespace KNX {

DPT_20_021::DPT_20_021() :
    DPT_20(21)
{
}

std::string DPT_20_021::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Cloudless";
        break;
    case 1:
        oss << "Sunny";
        break;
    case 2:
        oss << "Sunshiny";
        break;
    case 3:
        oss << "Lightly cloudy";
        break;
    case 4:
        oss << "Scattered clouds";
        break;
    case 5:
        oss << "Cloudy";
        break;
    case 6:
        oss << "Very cloudy";
        break;
    case 7:
        oss << "Almost overcast";
        break;
    case 8:
        oss << "Overcast";
        break;
    case 9:
        oss << "Sky obstructed from view";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
