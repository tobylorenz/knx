// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_022.h>

#include <sstream>

namespace KNX {

DPT_20_022::DPT_20_022() :
    DPT_20(22)
{
}

std::string DPT_20_022::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "do not send";
        break;
    case 1:
        oss << "send always";
        break;
    case 2:
        oss << "send if value changed during powerdown";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
