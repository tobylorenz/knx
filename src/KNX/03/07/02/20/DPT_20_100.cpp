// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_100.h>

#include <sstream>

namespace KNX {

DPT_20_100::DPT_20_100() :
    DPT_20(100)
{
}

std::string DPT_20_100::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "auto";
        break;
    case 1:
        oss << "oil";
        break;
    case 2:
        oss << "gas";
        break;
    case 3:
        oss << "solid state fuel";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
