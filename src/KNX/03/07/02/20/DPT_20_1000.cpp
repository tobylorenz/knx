// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1000.h>

#include <sstream>

namespace KNX {

DPT_20_1000::DPT_20_1000() :
    DPT_20(1000)
{
}

std::string DPT_20_1000::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0x00:
        oss << "Data Link Layer";
        break;
    case 0x01:
        oss << "Data Link Layer Busmonitor";
        break;
    case 0x02:
        oss << "Data Link Layer Raw Frames";
        break;
    case 0x06:
        oss << "cEMI Transport Layer";
        break;
    case 0xff:
        oss << "No Layer";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
