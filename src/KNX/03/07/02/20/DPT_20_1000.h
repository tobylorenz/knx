// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/20/DPT_20.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 20.1000 communication mode
 *
 * @note DPT_CommMode shall be encoded according the specification of PID_COMM_MODE (3/6/3).
 *
 * @ingroup KNX_03_06_03_04_02_02_04_02
 * @ingroup KNX_03_07_02_08_01
 */
class KNX_EXPORT DPT_20_1000 : public DPT_20
{
public:
    explicit DPT_20_1000();

    std::string text() const override;
};

using DPT_CommMode = DPT_20_1000;

}
