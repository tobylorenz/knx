// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1001.h>

#include <sstream>

namespace KNX {

DPT_20_1001::DPT_20_1001() :
    DPT_20(1001)
{
}

std::string DPT_20_1001::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "PL medium Domain Address";
        break;
    case 2:
        oss << "RF Control Octet and Serial Number or DoA";
        break;
    case 3:
        oss << "Busmonitor Error Flags";
        break;
    case 4:
        oss << "Relative timestamp";
        break;
    case 5:
        oss << "Time delay";
        break;
    case 6:
        oss << "Extended Relative Timestamp";
        break;
    case 7:
        oss << "BiBat information";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
