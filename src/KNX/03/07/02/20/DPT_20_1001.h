// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/20/DPT_20.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 20.1001 additional information type
 *
 * @ingroup KNX_03_07_02_08_01
 */
class KNX_EXPORT DPT_20_1001 : public DPT_20
{
public:
    explicit DPT_20_1001();

    std::string text() const override;
};

using DPT_AddInfoTypes = DPT_20_1001;

}
