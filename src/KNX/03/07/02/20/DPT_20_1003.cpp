// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1003.h>

#include <sstream>

namespace KNX {

DPT_20_1003::DPT_20_1003() :
    DPT_20(1003)
{
}

std::string DPT_20_1003::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "no filtering, all supported received frames shall be passed to the cEMI client using L_Data.ind";
        break;
    case 1:
        oss << "filtering by Domain Address";
        break;
    case 2:
        oss << "filtering by KNX Serial Number table";
        break;
    case 3:
        oss << "filtering by Domain Address and by Serial Number";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
