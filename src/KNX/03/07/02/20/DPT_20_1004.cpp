// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1004.h>

#include <sstream>

namespace KNX {

DPT_20_1004::DPT_20_1004() :
    DPT_20(1004)
{
}

std::string DPT_20_1004::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "KNX TP1";
        break;
    case 1:
        oss << "KNX PL110";
        break;
    case 2:
        oss << "KNX RF";
        break;
    case 5:
        oss << "KNX IP";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
