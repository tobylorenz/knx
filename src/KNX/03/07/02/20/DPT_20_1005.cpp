// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1005.h>

#include <sstream>

namespace KNX {

DPT_20_1005::DPT_20_1005() :
    DPT_20(1005)
{
}

std::string DPT_20_1005::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "default function";
        break;
    case 2:
        oss << "ON";
        break;
    case 3:
        oss << "OFF";
        break;
    case 4:
        oss << "Toggle";
        break;
    case 5:
        oss << "Dimming Up Down";
        break;
    case 6:
        oss << "Dimming Up";
        break;
    case 7:
        oss << "Dimming Down";
        break;
    case 8:
        oss << "On / Off";
        break;
    case 9:
        oss << "Timed On Off";
        break;
    case 10:
        oss << "Forced On";
        break;
    case 11:
        oss << "Forced Off";
        break;
    case 12:
        oss << "Shutter Up (for PB)";
        break;
    case 13:
        oss << "Shutter Down (for PB)";
        break;
    case 14:
        oss << "Shutter Up Down (for PB)";
        break;
    case 16:
        oss << "Forced Up";
        break;
    case 17:
        oss << "Forced Down";
        break;
    case 18:
        oss << "Wind Alarm";
        break;
    case 19:
        oss << "Rain Alarm";
        break;
    case 20:
        oss << "HVAC Mod Comfort / Economy";
        break;
    case 21:
        oss << "HVAC Mode Comfort / -";
        break;
    case 22:
        oss << "HVAC Mode Economy / -";
        break;
    case 23:
        oss << "HVAC Mode Building protection / HVAC mode auto";
        break;
    case 24:
        oss << "Shutter Stop";
        break;
    case 25:
        oss << "Timed Comfort Standby";
        break;
    case 26:
        oss << "Forced Comfort";
        break;
    case 27:
        oss << "Forced Building protection";
        break;
    case 28:
        oss << "Scene 1";
        break;
    case 29:
        oss << "Scene 2";
        break;
    case 30:
        oss << "Scene 3";
        break;
    case 31:
        oss << "Scene 4";
        break;
    case 32:
        oss << "Scene 5";
        break;
    case 33:
        oss << "Scene 6";
        break;
    case 34:
        oss << "Scene 7";
        break;
    case 35:
        oss << "Scene 8";
        break;
    case 36:
        oss << "Absolute dimming 25 %";
        break;
    case 37:
        oss << "Absolute dimming 50 %";
        break;
    case 38:
        oss << "Absolute dimming 75 %";
        break;
    case 39:
        oss << "Absolute dimming 100 %";
        break;
    case 40:
        oss << "Shutter Up / - (for switch)";
        break;
    case 41:
        oss << "Shutter Down / - (for switch)";
        break;
    case 42:
        oss << "Shutter Up / Down (for switch)";
        break;
    case 43:
        oss << "Shutter Down / Up (for switch)";
        break;
    case 44:
        oss << "Light sensor";
        break;
    case 45:
        oss << "System clock";
        break;
    case 46:
        oss << "Battery status";
        break;
    case 47:
        oss << "HVAC Mode Standby / -";
        break;
    case 48:
        oss << "HVAC Mode Auto / -";
        break;
    case 49:
        oss << "HVAC Mode Comfort / Standby";
        break;
    case 50:
        oss << "HVAC Mode Building protection / -";
        break;
    case 51:
        oss << "Timed toggle";
        break;
    case 52:
        oss << "Dimming Absolute switch";
        break;
    case 53:
        oss << "Scene switch";
        break;
    case 54:
        oss << "Smoke alarm";
        break;
    case 55:
        oss << "Sub detector";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
