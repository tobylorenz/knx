// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_101.h>

#include <sstream>

namespace KNX {

DPT_20_101::DPT_20_101() :
    DPT_20(101)
{
}

std::string DPT_20_101::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "reserved";
        break;
    case 1:
        oss << "1 stage";
        break;
    case 2:
        oss << "2 stage";
        break;
    case 3:
        oss << "modulating";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
