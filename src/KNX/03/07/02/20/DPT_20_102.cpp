// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_102.h>

#include <sstream>

namespace KNX {

DPT_20_102::DPT_20_102() :
    DPT_20(102)
{
}

std::string DPT_20_102::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Auto";
        break;
    case 1:
        oss << "Comfort";
        break;
    case 2:
        oss << "Standby";
        break;
    case 3:
        oss << "Economy";
        break;
    case 4:
        oss << "Building Protection";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
