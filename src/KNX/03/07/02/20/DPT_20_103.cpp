// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_103.h>

#include <sstream>

namespace KNX {

DPT_20_103::DPT_20_103() :
    DPT_20(103)
{
}

std::string DPT_20_103::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Auto";
        break;
    case 1:
        oss << "LegioProtect";
        break;
    case 2:
        oss << "Normal";
        break;
    case 3:
        oss << "Reduced";
        break;
    case 4:
        oss << "Off/FrostProtect";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
