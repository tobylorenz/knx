// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_104.h>

#include <sstream>

namespace KNX {

DPT_20_104::DPT_20_104() :
    DPT_20(104)
{
}

std::string DPT_20_104::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "None";
        break;
    case 1:
        oss << "Shift load priority";
        break;
    case 2:
        oss << "Absolute load priority";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
