// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_106.h>

#include <sstream>

namespace KNX {

DPT_20_106::DPT_20_106() :
    DPT_20(106)
{
}

std::string DPT_20_106::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Normal";
        break;
    case 1:
        oss << "EmergPressure";
        break;
    case 2:
        oss << "EmergDepressure";
        break;
    case 3:
        oss << "EmergPurge";
        break;
    case 4:
        oss << "EmergShutdown";
        break;
    case 5:
        oss << "EmergFire";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
