// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_107.h>

#include <sstream>

namespace KNX {

DPT_20_107::DPT_20_107() :
    DPT_20(107)
{
}

std::string DPT_20_107::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Auto";
        break;
    case 1:
        oss << "CoolingOnly";
        break;
    case 2:
        oss << "HeatingOnly";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
