// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_108.h>

#include <sstream>

namespace KNX {

DPT_20_108::DPT_20_108() :
    DPT_20(108)
{
}

std::string DPT_20_108::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "Heat stage A for normal heating";
        break;
    case 2:
        oss << "Heat stage B for heating with two stages (A + B)";
        break;
    case 3:
        oss << "Cool stage A for normal cooling";
        break;
    case 4:
        oss << "Cool stage B for cooling with two stages (A + B)";
        break;
    case 5:
        oss << "Heat/Cool for changeover applications";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
