// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_109.h>

#include <sstream>

namespace KNX {

DPT_20_109::DPT_20_109() :
    DPT_20(109)
{
}

std::string DPT_20_109::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "reserved";
        break;
    case 1:
        oss << "Fresh air, e.g. for fancoils";
        break;
    case 2:
        oss << "Supply Air. e.g. for VAV";
        break;
    case 3:
        oss << "Extract Air e.g. for VAV";
        break;
    case 4:
        oss << "Extract Air e.g. for VAV";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
