// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_110.h>

#include <sstream>

namespace KNX {

DPT_20_110::DPT_20_110() :
    DPT_20(110)
{
}

std::string DPT_20_110::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "Heat Stage A On/Off";
        break;
    case 2:
        oss << "Heat Stage A Proportional";
        break;
    case 3:
        oss << "Heat Stage B Proportional";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
