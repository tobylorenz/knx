// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_111.h>

#include <sstream>

namespace KNX {

DPT_20_111::DPT_20_111() :
    DPT_20(111)
{
}

std::string DPT_20_111::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "not running";
        break;
    case 1:
        oss << "permanently running";
        break;
    case 2:
        oss << "running in intervals";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
