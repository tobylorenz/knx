// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_112.h>

#include <sstream>

namespace KNX {

DPT_20_112::DPT_20_112() :
    DPT_20(112)
{
}

std::string DPT_20_112::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "autonomous";
        break;
    case 1:
        oss << "master";
        break;
    case 2:
        oss << "slave";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
