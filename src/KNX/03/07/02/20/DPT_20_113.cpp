// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_113.h>

#include <sstream>

namespace KNX {

DPT_20_113::DPT_20_113() :
    DPT_20(113)
{
}

std::string DPT_20_113::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "normal setpoint";
        break;
    case 1:
        oss << "alternative setpoint";
        break;
    case 2:
        oss << "building protection setpoint";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
