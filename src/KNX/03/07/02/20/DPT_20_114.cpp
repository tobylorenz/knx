// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_114.h>

#include <sstream>

namespace KNX {

DPT_20_114::DPT_20_114() :
    DPT_20(114)
{
}

std::string DPT_20_114::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "other device";
        break;
    case 1:
        oss << "oil meter";
        break;
    case 2:
        oss << "electricity meter";
        break;
    case 3:
        oss << "gas device";
        break;
    case 4:
        oss << "heat meter";
        break;
    case 5:
        oss << "steam meter";
        break;
    case 6:
        oss << "warm water meter";
        break;
    case 7:
        oss << "water meter";
        break;
    case 8:
        oss << "heat cost allocator";
        break;
    case 9:
        oss << "reserved";
        break;
    case 10:
        oss << "cooling load meter (outlet)";
        break;
    case 11:
        oss << "cooling load meter (inlet)";
        break;
    case 12:
        oss << "heat (inlet)";
        break;
    case 13:
        oss << "heat and cool";
        break;
    case 32:
        oss << "breaker (electricity)";
        break;
    case 33:
        oss << "valve (gas or water)";
        break;
    case 40:
        oss << "waste water meter";
        break;
    case 41:
        oss << "garbage";
        break;
    case 255:
        oss << "void device type";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
