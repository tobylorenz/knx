// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_115.h>

#include <sstream>

namespace KNX {

DPT_20_115::DPT_20_115() :
    DPT_20(115)
{
}

std::string DPT_20_115::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "inactive";
        break;
    case 1:
        oss << "humidification";
        break;
    case 2:
        oss << "dehumidification";
        break;
    case 3:
        oss << "reserved";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
