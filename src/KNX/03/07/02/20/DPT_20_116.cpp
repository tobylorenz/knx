// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_116.h>

#include <sstream>

namespace KNX {

DPT_20_116::DPT_20_116() :
    DPT_20(116)
{
}

std::string DPT_20_116::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "disabled";
        break;
    case 1:
        oss << "enable stage A";
        break;
    case 2:
        oss << "enable stage B";
        break;
    case 3:
        oss << "enable both stages";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
