// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_120.h>

#include <sstream>

namespace KNX {

DPT_20_120::DPT_20_120() :
    DPT_20(120)
{
}

std::string DPT_20_120::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "Air Damper";
        break;
    case 2:
        oss << "VAV";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
