// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1200.h>

#include <sstream>

namespace KNX {

DPT_20_1200::DPT_20_1200() :
    DPT_20(1200)
{
}

std::string DPT_20_1200::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Breaker/Valve is closed";
        break;
    case 1:
        oss << "Breaker/Valve is open";
        break;
    case 2:
        oss << "Breaker/Valve is released";
        break;
    case 255:
        oss << "invalid";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
