// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1202.h>

#include <sstream>

namespace KNX {

DPT_20_1202::DPT_20_1202() :
    DPT_20(1202)
{
}

std::string DPT_20_1202::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "unknown";
        break;
    case 1:
        oss << "temperature converted";
        break;
    case 2:
        oss << "at base condition";
        break;
    case 3:
        oss << "at measurement condition";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
