// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1203.h>

#include <sstream>

namespace KNX {

DPT_20_1203::DPT_20_1203() :
    DPT_20(1203)
{
}

std::string DPT_20_1203::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "closed";
        break;
    case 1:
        oss << "open on overload";
        break;
    case 2:
        oss << "open on overvoltage";
        break;
    case 3:
        oss << "open on load shedding";
        break;
    case 4:
        oss << "open on PLC or Euridis command";
        break;
    case 5:
        oss << "open on overheat with a current value over the maximum switching current value";
        break;
    case 6:
        oss << " open on overheat with a current value under the maximum switching current value";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
