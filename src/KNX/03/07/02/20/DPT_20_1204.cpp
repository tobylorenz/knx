// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1204.h>

#include <sstream>

namespace KNX {

DPT_20_1204::DPT_20_1204() :
    DPT_20(1204)
{
}

std::string DPT_20_1204::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "deactivated";
        break;
    case 1:
        oss << "activated without security";
        break;
    case 2:
        oss << "activated with security";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
