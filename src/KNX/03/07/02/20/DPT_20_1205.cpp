// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1205.h>

#include <sstream>

namespace KNX {

DPT_20_1205::DPT_20_1205() :
    DPT_20(1205)
{
}

std::string DPT_20_1205::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "New / Unlock (S-SFK) – Not Associated (G3-PLC)";
        break;
    case 1:
        oss << "New / Lock (S-FSK) – Associated (G3-PLC)";
        break;
    case 2:
        oss << "Registered (S-FSK) – reserved (G3-PLC)";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
