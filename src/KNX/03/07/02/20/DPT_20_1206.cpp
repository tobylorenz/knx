// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1206.h>

#include <sstream>

namespace KNX {

DPT_20_1206::DPT_20_1206() :
    DPT_20(1206)
{
}

std::string DPT_20_1206::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "no notice in progress";
        break;
    case 1:
        oss << "notice PE1 in progress";
        break;
    case 2:
        oss << "notice PE2 in progress";
        break;
    case 3:
        oss << "notice PE3 in progress";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
