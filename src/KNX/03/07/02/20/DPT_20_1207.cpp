// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1207.h>

#include <sstream>

namespace KNX {

DPT_20_1207::DPT_20_1207() :
    DPT_20(1207)
{
}

std::string DPT_20_1207::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "no peak event";
        break;
    case 1:
        oss << "PE1 in progress";
        break;
    case 2:
        oss << "PE2 in progress";
        break;
    case 3:
        oss << "PE3 in progress";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
