// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1208.h>

#include <sstream>

namespace KNX {

DPT_20_1208::DPT_20_1208() :
    DPT_20(1208)
{
}

std::string DPT_20_1208::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Historical";
        break;
    case 1:
        oss << "Standard";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
