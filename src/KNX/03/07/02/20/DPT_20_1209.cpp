// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_1209.h>

#include <sstream>

namespace KNX {

DPT_20_1209::DPT_20_1209() :
    DPT_20(1209)
{
}

std::string DPT_20_1209::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "None";
        break;
    case 1:
        oss << "Historical single-phase";
        break;
    case 2:
        oss << "Historical three-phase";
        break;
    case 3:
        oss << "Standard single-phase";
        break;
    case 4:
        oss << "Standard three-phase";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
