// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_121.h>

#include <sstream>

namespace KNX {

DPT_20_121::DPT_20_121() :
    DPT_20(121)
{
}

std::string DPT_20_121::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Backup Value";
        break;
    case 1:
        oss << "Keep Last State";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
