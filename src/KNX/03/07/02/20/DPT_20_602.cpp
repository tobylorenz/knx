// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_602.h>

#include <sstream>

namespace KNX {

DPT_20_602::DPT_20_602() :
    DPT_20(602)
{
}

std::string DPT_20_602::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "0 s (no fade)";
        break;
    case 1:
        oss << "0,7 s";
        break;
    case 2:
        oss << "1,0 s";
        break;
    case 3:
        oss << "1,4 s";
        break;
    case 4:
        oss << "2,0 s";
        break;
    case 5:
        oss << "2,8 s";
        break;
    case 6:
        oss << "4,0 s";
        break;
    case 7:
        oss << "5,7 s";
        break;
    case 8:
        oss << "8,0 s";
        break;
    case 9:
        oss << "11,3 s";
        break;
    case 10:
        oss << "16,0 s";
        break;
    case 11:
        oss << "22,6 s";
        break;
    case 12:
        oss << "32,0 s";
        break;
    case 13:
        oss << "45,3 s";
        break;
    case 14:
        oss << "64,0 s";
        break;
    case 15:
        oss << "90,5 s";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
