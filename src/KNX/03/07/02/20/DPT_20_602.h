// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/20/DPT_20.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 20.602 dali fade time
 *
 * @ingroup KNX_03_07_02_06_03
 */
class KNX_EXPORT DPT_20_602 : public DPT_20
{
public:
    explicit DPT_20_602();

    std::string text() const override;
};

using DPT_DALI_Fade_Time = DPT_20_602;

}
