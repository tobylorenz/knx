// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_603.h>

#include <sstream>

namespace KNX {

DPT_20_603::DPT_20_603() :
    DPT_20(603)
{
}

std::string DPT_20_603::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "BlinkingDisabled";
        break;
    case 1:
        oss << "WithoutAcknowledge";
        break;
    case 2:
        oss << "BlinkingWithAcknowledge";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
