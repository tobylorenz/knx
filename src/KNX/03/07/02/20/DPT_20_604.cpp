// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_604.h>

#include <sstream>

namespace KNX {

DPT_20_604::DPT_20_604() :
    DPT_20(604)
{
}

std::string DPT_20_604::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "automatic light control";
        break;
    case 1:
        oss << "manual light control";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
