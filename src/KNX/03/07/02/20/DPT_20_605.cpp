// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_605.h>

#include <sstream>

namespace KNX {

DPT_20_605::DPT_20_605() :
    DPT_20(605)
{
}

std::string DPT_20_605::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "one PB/binary input mode";
        break;
    case 2:
        oss << "two PBs/binary inputs mode";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
