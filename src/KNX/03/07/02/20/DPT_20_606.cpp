// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_606.h>

#include <sstream>

namespace KNX {

DPT_20_606::DPT_20_606() :
    DPT_20(606)
{
}

std::string DPT_20_606::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "inactive (no message sent)";
        break;
    case 1:
        oss << "SwitchOff message sent";
        break;
    case 2:
        oss << "SwitchOn message sent";
        break;
    case 3:
        oss << "inverse value of InfoOnOff is sent";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
