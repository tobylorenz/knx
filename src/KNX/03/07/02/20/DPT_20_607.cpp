// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_607.h>

#include <sstream>

namespace KNX {

DPT_20_607::DPT_20_607() :
    DPT_20(607)
{
}

std::string DPT_20_607::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "one PB/binary input; SwitchOnOff inverts on each transmission";
        break;
    case 2:
        oss << "one PB/binary input, On / DimUp message sent";
        break;
    case 3:
        oss << "one PB/binary input, Off / DimDown message sent";
        break;
    case 4:
        oss << "two PBs/binary inputs mode";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
