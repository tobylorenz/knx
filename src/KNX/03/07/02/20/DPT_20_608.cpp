// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_608.h>

#include <sstream>

namespace KNX {

DPT_20_608::DPT_20_608() :
    DPT_20(608)
{
}

std::string DPT_20_608::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "last actual value";
        break;
    case 1:
        oss << "value according additional parameter";
        break;
    case 2:
        oss << "last received absolute setvalue";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
