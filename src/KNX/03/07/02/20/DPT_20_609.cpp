// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_609.h>

#include <sstream>

namespace KNX {

DPT_20_609::DPT_20_609() :
    DPT_20(609)
{
}

std::string DPT_20_609::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "automatic";
        break;
    case 1:
        oss << "leading edge (inductive load)";
        break;
    case 2:
        oss << "trailing edge (capacitive load)";
        break;
    case 3:
        oss << "switch mode only (non-dimmable load)";
        break;
    case 4:
        oss << "automatic once";
        break;
    case 5:
        oss << "CFL, leading";
        break;
    case 6:
        oss << "CFL, trailing";
        break;
    case 7:
        oss << "LED, leading";
        break;
    case 8:
        oss << "LED, trailing";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
