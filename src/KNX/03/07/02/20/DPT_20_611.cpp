// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_611.h>

#include <sstream>

namespace KNX {

DPT_20_611::DPT_20_611() :
    DPT_20(611)
{
}

std::string DPT_20_611::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "Start Function Test (FT)";
        break;
    case 2:
        oss << "Start Duration Test (DT)";
        break;
    case 3:
        oss << "Start Partial Duration Test";
        break;
    case 4:
        oss << "Stop Test";
        break;
    case 5:
        oss << "Reset Function Test Done Flag";
        break;
    case 6:
        oss << "Reset Duration Test Done";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
