// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_612.h>

#include <sstream>

namespace KNX {

DPT_20_612::DPT_20_612() :
    DPT_20(612)
{
}

std::string DPT_20_612::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Restore Factory Default Settings Acc. DALI Cmd. 254";
        break;
    case 1:
        oss << "Goto Rest Mode Acc. DALI Cmd. 224";
        break;
    case 2:
        oss << "Goto Inhibit Mode Acc. DALI Cmd. 225";
        break;
    case 3:
        oss << "Re-Light / Reset Inhibit Acc. DALI Cmd. 226";
        break;
    case 4:
        oss << "Reset Lamp Time Resets the Lamp Emergency Time and the Lamp Total Operation Time. Acc. DALI Cmd. 232";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
