// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_613.h>

#include <sstream>

namespace KNX {

DPT_20_613::DPT_20_613() :
    DPT_20(613)
{
}

std::string DPT_20_613::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Reserved, no effect";
        break;
    case 1:
        oss << "Request Converter Status";
        break;
    case 2:
        oss << "Request Converter Test Result";
        break;
    case 3:
        oss << "Request Battery Info";
        break;
    case 4:
        oss << "Request Converter FT Info";
        break;
    case 5:
        oss << "Request Converter DT Info";
        break;
    case 6:
        oss << "Request Converter PDT Info";
        break;
    case 7:
        oss << "Request Converter Info";
        break;
    case 8:
        oss << "Request Converter Info Fix";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
