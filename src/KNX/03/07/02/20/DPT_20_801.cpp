// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_801.h>

#include <sstream>

namespace KNX {

DPT_20_801::DPT_20_801() :
    DPT_20(801)
{
}

std::string DPT_20_801::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "up";
        break;
    case 1:
        oss << "down";
        break;
    case 2:
        oss << "no change";
        break;
    case 3:
        oss << "value according additional parameter";
        break;
    case 4:
        oss << "stop";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
