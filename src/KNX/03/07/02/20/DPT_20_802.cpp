// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_802.h>

#include <sstream>

namespace KNX {

DPT_20_802::DPT_20_802() :
    DPT_20(802)
{
}

std::string DPT_20_802::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "up";
        break;
    case 1:
        oss << "down";
        break;
    case 2:
        oss << "no change";
        break;
    case 3:
        oss << "value according additional parameter";
        break;
    case 4:
        oss << "stop";
        break;
    case 5:
        oss << "updated value";
        break;
    case 6:
        oss << "value before locking";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
