// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_803.h>

#include <sstream>

namespace KNX {

DPT_20_803::DPT_20_803() :
    DPT_20(803)
{
}

std::string DPT_20_803::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 1:
        oss << "one push button/binary input";
        break;
    case 2:
        oss << "one push button/binary input, MoveUp / StepUp message sent";
        break;
    case 3:
        oss << "one push button/binary input, MoveDown / StepDown message sent";
        break;
    case 4:
        oss << "two push buttons/binary inputs mode";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
