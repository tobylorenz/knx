// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/20/DPT_20_804.h>

#include <sstream>

namespace KNX {

DPT_20_804::DPT_20_804() :
    DPT_20(804)
{
}

std::string DPT_20_804::text() const
{
    std::ostringstream oss;
    switch (field1) {
    case 0:
        oss << "Automatic Control";
        break;
    case 1:
        oss << "Manual Control";
        break;
    default:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
