// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/200/DPT_200.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_200::DPT_200(const uint16_t subnumber) :
    Datapoint_Type(200, subnumber)
{
}

void DPT_200::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    binary_value = *first++ & 0x01;

    status_command = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_200::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(binary_value);

    data.push_back(status_command);

    return data;
}

}
