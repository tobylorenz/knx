// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 200.* @note Not in MasterData yet
 *
 * Datapoint Types "B1Z8"
 *
 * @ingroup KNX_03_07_02_04_08
 */
class KNX_EXPORT DPT_200 : public Datapoint_Type
{
public:
    explicit DPT_200(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** binary value */
    bool binary_value{false};

    /** status/command */
    uint8_t status_command{};
};

}
