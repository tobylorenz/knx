// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/200/DPT_200_100.h>

#include <sstream>

namespace KNX {

DPT_200_100::DPT_200_100() :
    DPT_200(100)
{
}

std::string DPT_200_100::text() const
{
    std::ostringstream oss;
    oss << (binary_value ? "heating" : "cooling");
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
