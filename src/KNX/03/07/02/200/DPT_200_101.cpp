// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/200/DPT_200_101.h>

#include <sstream>

namespace KNX {

DPT_200_101::DPT_200_101() :
    DPT_200(101)
{
}

std::string DPT_200_101::text() const
{
    std::ostringstream oss;
    oss << (binary_value ? "high" : "low");
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
