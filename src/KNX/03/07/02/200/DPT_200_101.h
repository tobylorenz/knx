// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/200/DPT_200.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 200.101 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_08_02
 */
class KNX_EXPORT DPT_200_101 : public DPT_200
{
public:
    explicit DPT_200_101();

    std::string text() const override;
};

using DPT_BinaryValue_Z = DPT_200_101;

}
