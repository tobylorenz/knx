// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/201/DPT_201_100.h>

#include <sstream>

namespace KNX {

DPT_201_100::DPT_201_100() :
    DPT_201(100)
{
}

std::string DPT_201_100::text() const
{
    std::ostringstream oss;
    switch (mode) {
    case 0:
        oss << "Auto";
        break;
    case 1:
        oss << "Comfort";
        break;
    case 2:
        oss << "Standby";
        break;
    case 3:
        oss << "Economy";
        break;
    case 4:
        oss << "Bldg.Prot";
        break;
    default:
        oss << "reserved";
        break;
    }
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
