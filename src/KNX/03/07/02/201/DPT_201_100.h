// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/201/DPT_201.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 201.100 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_09_01
 */
class KNX_EXPORT DPT_201_100 : public DPT_201
{
public:
    explicit DPT_201_100();

    std::string text() const override;
};

using DPT_HVACMode_Z = DPT_201_100;

}
