// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/201/DPT_201_102.h>

#include <sstream>

namespace KNX {

DPT_201_102::DPT_201_102() :
    DPT_201(102)
{
}

std::string DPT_201_102::text() const
{
    std::ostringstream oss;
    switch (mode) {
    case 0:
        oss << "Auto";
        break;
    case 1:
        oss << "LegioProtect";
        break;
    case 2:
        oss << "Normal";
        break;
    case 3:
        oss << "Reduced";
        break;
    case 4:
        oss << "Off/FrostProtect";
        break;
    default:
        oss << "reserved";
        break;
    }
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
