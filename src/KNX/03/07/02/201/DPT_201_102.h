// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/201/DPT_201.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 201.102 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_09_02
 */
class KNX_EXPORT DPT_201_102 : public DPT_201
{
public:
    explicit DPT_201_102();

    std::string text() const override;
};

using DPT_DHWMode_Z = DPT_201_102;

}
