// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/201/DPT_201_104.h>

#include <sstream>

namespace KNX {

DPT_201_104::DPT_201_104() :
    DPT_201(104)
{
}

std::string DPT_201_104::text() const
{
    std::ostringstream oss;
    switch (mode) {
    case 0:
        oss << "Auto";
        break;
    case 1:
        oss << "Heat";
        break;
    case 2:
        oss << "Morning Warmup";
        break;
    case 3:
        oss << "Cool";
        break;
    case 4:
        oss << "Night Purge";
        break;
    case 5:
        oss << "Precool";
        break;
    case 6:
        oss << "Off";
        break;
    case 7:
        oss << "Test";
        break;
    case 8:
        oss << "Emergency Heat";
        break;
    case 9:
        oss << "Fan only";
        break;
    case 10:
        oss << "Free Cool";
        break;
    case 11:
        oss << "Ice";
        break;
    case 12:
        oss << "Maximum Heating";
        break;
    case 13:
        oss << "Economic Heat/Cool Mode";
        break;
    case 14:
        oss << "Dehumidification";
        break;
    case 15:
        oss << "Calibration Mode";
        break;
    case 16:
        oss << "Emergency Cool Mode";
        break;
    case 17:
        oss << "Emergency Steam Mode";
        break;
    case 20:
        oss << "NoDem";
        break;
    default:
        oss << "reserved";
        break;
    }
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
