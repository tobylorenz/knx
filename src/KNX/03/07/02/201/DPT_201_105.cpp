// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/201/DPT_201_105.h>

#include <sstream>

namespace KNX {

DPT_201_105::DPT_201_105() :
    DPT_201(105)
{
}

std::string DPT_201_105::text() const
{
    std::ostringstream oss;
    switch (mode) {
    case 0:
        oss << "disabled";
        break;
    case 1:
        oss << "enable stage A";
        break;
    case 2:
        oss << "enable stage B";
        break;
    case 3:
        oss << "enable both stages";
        break;
    }
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
