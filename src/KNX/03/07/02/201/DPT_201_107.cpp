// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/201/DPT_201_107.h>

#include <sstream>

namespace KNX {

DPT_201_107::DPT_201_107() :
    DPT_201(107)
{
}

std::string DPT_201_107::text() const
{
    std::ostringstream oss;
    switch (mode) {
    case 0:
        oss << "Building in use";
        break;
    case 1:
        oss << "Building not used";
        break;
    case 2:
        oss << "Building Protection";
        break;
    }
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
