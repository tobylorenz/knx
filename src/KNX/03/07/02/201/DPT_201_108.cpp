// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/201/DPT_201_108.h>

#include <sstream>

namespace KNX {

DPT_201_108::DPT_201_108() :
    DPT_201(108)
{
}

std::string DPT_201_108::text() const
{
    std::ostringstream oss;
    switch (mode) {
    case 0:
        oss << "Occupied";
        break;
    case 1:
        oss << "Standby";
        break;
    case 2:
        oss << "Not occupied";
        break;
    }
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
