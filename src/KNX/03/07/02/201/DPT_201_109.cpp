// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/201/DPT_201_109.h>

#include <sstream>

namespace KNX {

DPT_201_109::DPT_201_109() :
    DPT_201(109)
{
}

std::string DPT_201_109::text() const
{
    std::ostringstream oss;
    switch (mode) {
    case 0:
        oss << "Normal";
        break;
    case 1:
        oss << "EmergPressure";
        break;
    case 2:
        oss << "EmergDepressure";
        break;
    case 3:
        oss << "EmergPurge";
        break;
    case 4:
        oss << "EmergShutdown";
        break;
    case 5:
        oss << "EmergFire";
        break;
    default:
        oss << "reserved";
        break;
    }
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
