// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 202.* @note Not in MasterData yet
 *
 * Datapoint Types "U8Z8"
 *
 * @ingroup KNX_03_07_02_03_31
 * @ingroup KNX_03_07_02_03_32
 */
class KNX_EXPORT DPT_202 : public Datapoint_Type
{
public:
    explicit DPT_202(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Value */
    uint8_t value{};

    /** Status/Command */
    uint8_t status_command{};
};

}
