// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/202/DPT_202.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 202.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_31
 */
class KNX_EXPORT DPT_202_001 : public DPT_202
{
public:
    explicit DPT_202_001();

    std::string text() const override;
};

using DPT_RelValue_Z = DPT_202_001;

}
