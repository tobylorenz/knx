// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/202/DPT_202_002.h>

#include <sstream>

namespace KNX {

DPT_202_002::DPT_202_002() :
    DPT_202(2)
{
}

std::string DPT_202_002::text() const
{
    std::ostringstream oss;
    oss << static_cast<uint16_t>(value);
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
