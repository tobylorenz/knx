// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/203/DPT_203.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_203::DPT_203(const uint16_t subnumber) :
    Datapoint_Type(203, subnumber)
{
}

void DPT_203::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    value = (*first++ << 8) | (*first++);
    status_command = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_203::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(value >> 8);
    data.push_back(value & 0xff);

    data.push_back(status_command);

    return data;
}

}
