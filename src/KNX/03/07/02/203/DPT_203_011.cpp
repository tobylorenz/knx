// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/203/DPT_203_011.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_203_011::DPT_203_011() :
    DPT_203(11)
{
}

std::string DPT_203_011::text() const
{
    std::ostringstream oss;
    oss << std::fixed << std::setprecision(2) << value * 0.01 << " l/h";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
