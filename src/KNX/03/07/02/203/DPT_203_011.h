// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/203/DPT_203.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 203.011 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_34
 */
class KNX_EXPORT DPT_203_011 : public DPT_203
{
public:
    explicit DPT_203_011();

    std::string text() const override;
};

using DPT_UFlowRateLiter_h_Z = DPT_203_011; // DPT_UFlowRateLiter/h_Z

}
