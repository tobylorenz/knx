// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/203/DPT_203.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 203.013 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_36
 */
class KNX_EXPORT DPT_203_013 : public DPT_203
{
public:
    explicit DPT_203_013();

    std::string text() const override;
};

using DPT_UEICurrentμA_Z = DPT_203_013;

}
