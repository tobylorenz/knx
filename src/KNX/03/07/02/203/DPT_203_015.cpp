// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/203/DPT_203_015.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_203_015::DPT_203_015() :
    DPT_203(15)
{
}

std::string DPT_203_015::text() const
{
    std::ostringstream oss;
    oss << std::fixed << std::setprecision(2) << value * 0.05 << " mbar";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
