// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/203/DPT_203_100.h>

#include <sstream>

namespace KNX {

DPT_203_100::DPT_203_100() :
    DPT_203(100)
{
}

std::string DPT_203_100::text() const
{
    std::ostringstream oss;
    oss << value << " ppm";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
