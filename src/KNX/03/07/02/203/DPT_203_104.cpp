// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/203/DPT_203_104.h>

#include <sstream>

namespace KNX {

DPT_203_104::DPT_203_104() :
    DPT_203(104)
{
}

std::string DPT_203_104::text() const
{
    std::ostringstream oss;
    oss << value << " m³/h";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
