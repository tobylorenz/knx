// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/204/DPT_204.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 204.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_39
 */
class KNX_EXPORT DPT_204_001 : public DPT_204
{
public:
    explicit DPT_204_001();

    std::string text() const override;
};

using DPT_RelSignedValue_Z = DPT_204_001;

}
