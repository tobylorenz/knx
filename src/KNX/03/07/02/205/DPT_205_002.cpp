// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/205/DPT_205_002.h>

#include <sstream>

namespace KNX {

DPT_205_002::DPT_205_002() :
    DPT_205(2)
{
}

std::string DPT_205_002::text() const
{
    std::ostringstream oss;
    oss << value << " ms";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
