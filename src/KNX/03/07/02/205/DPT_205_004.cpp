// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/205/DPT_205_004.h>

#include <sstream>

namespace KNX {

DPT_205_004::DPT_205_004() :
    DPT_205(4)
{
}

std::string DPT_205_004::text() const
{
    std::ostringstream oss;
    oss << value * 100 << " ms";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
