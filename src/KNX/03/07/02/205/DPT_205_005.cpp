// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/205/DPT_205_005.h>

#include <sstream>

namespace KNX {

DPT_205_005::DPT_205_005() :
    DPT_205(5)
{
}

std::string DPT_205_005::text() const
{
    std::ostringstream oss;
    oss << value << " s";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
