// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/205/DPT_205_017.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_205_017::DPT_205_017() :
    DPT_205(17)
{
}

std::string DPT_205_017::text() const
{
    std::ostringstream oss;
    oss << std::fixed << std::setprecision(2) << value * 0.01 << " %";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
