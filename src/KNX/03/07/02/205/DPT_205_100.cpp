// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/205/DPT_205_100.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_205_100::DPT_205_100() :
    DPT_205(100)
{
}

std::string DPT_205_100::text() const
{
    std::ostringstream oss;
    oss << std::fixed << std::setprecision(2) << value * 0.02 << " °C";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
