// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/205/DPT_205.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 205.100 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_11_01
 */
class KNX_EXPORT DPT_205_100 : public DPT_205
{
public:
    explicit DPT_205_100();

    std::string text() const override;
};

using DPT_TempHVACAbs_Z = DPT_205_100;

}
