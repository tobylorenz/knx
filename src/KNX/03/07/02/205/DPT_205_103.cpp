// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/205/DPT_205_103.h>

#include <sstream>

namespace KNX {

DPT_205_103::DPT_205_103() :
    DPT_205(103)
{
}

std::string DPT_205_103::text() const
{
    std::ostringstream oss;
    oss << value << " ppm";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
