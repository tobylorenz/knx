// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/205/DPT_205.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 205.103 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_11_04
 */
class KNX_EXPORT DPT_205_103 : public DPT_205
{
public:
    explicit DPT_205_103();

    std::string text() const override;
};

using DPT_HVACAirQualRel_Z = DPT_205_103;

}
