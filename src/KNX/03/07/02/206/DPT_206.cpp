// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/206/DPT_206.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_206::DPT_206(const uint16_t subnumber) :
    Datapoint_Type(206, subnumber)
{
}

void DPT_206::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    time = (*first++ << 8) | (*first++);

    mode = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_206::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(time >> 8);
    data.push_back(time & 0xff);

    data.push_back(mode);

    return data;
}

}
