// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 206.* 16-bit unsigned value & 8-bit enum
 *
 * Datapoint Types "U16N8"
 *
 * @ingroup KNX_03_07_02_04_12
 */
class KNX_EXPORT DPT_206 : public Datapoint_Type
{
public:
    explicit DPT_206(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** time */
    uint16_t time{};

    /** mode */
    uint8_t mode{}; // actually an Enum, but defined in derived classes
};

}
