// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/206/DPT_206_100.h>

#include <sstream>

namespace KNX {

DPT_206_100::DPT_206_100() :
    DPT_206(100)
{
}

std::string DPT_206_100::text() const
{
    std::ostringstream oss;

    oss << "delay time: " << time << " min"
        << ", HVAC mode: ";
    switch (mode) {
    case 0:
        oss << "undefined";
        break;
    case 1:
        oss << "comfort";
        break;
    case 2:
        oss << "standby";
        break;
    case 3:
        oss << "economy";
        break;
    case 4:
        oss << "building protection";
        break;
    default:
        oss << "reserved";
        break;
    }

    return oss.str();
}

}
