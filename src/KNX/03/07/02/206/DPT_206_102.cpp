// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/206/DPT_206_102.h>

#include <sstream>

namespace KNX {

DPT_206_102::DPT_206_102() :
    DPT_206(102)
{
}

std::string DPT_206_102::text() const
{
    std::ostringstream oss;

    oss << "delay time: " << time << " min"
        << ", DHW mode: ";
    switch (mode) {
    case 0:
        oss << "undefined";
        break;
    case 1:
        oss << "legio protect";
        break;
    case 2:
        oss << "normal";
        break;
    case 3:
        oss << "reduced";
        break;
    case 4:
        oss << "off/frost protect";
        break;
    default:
        oss << "reserved";
        break;
    }

    return oss.str();
}

}
