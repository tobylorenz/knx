// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/206/DPT_206.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 206.102 time delay & DHW mode
 *
 * @ingroup KNX_03_07_02_04_12_02
 */
class KNX_EXPORT DPT_206_102 : public DPT_206
{
public:
    explicit DPT_206_102();

    std::string text() const override;
};

using DPT_DHWModeNext = DPT_206_102;

}
