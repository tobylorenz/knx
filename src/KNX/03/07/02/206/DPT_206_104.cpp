// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/206/DPT_206_104.h>

#include <sstream>

namespace KNX {

DPT_206_104::DPT_206_104() :
    DPT_206(104)
{
}

std::string DPT_206_104::text() const
{
    std::ostringstream oss;

    oss << "delay time: " << time << " min"
        << ", occupancy mode: ";
    switch (mode) {
    case 0:
        oss << "occupied";
        break;
    case 1:
        oss << "standby";
        break;
    case 2:
        oss << "not occupied";
        break;
    default:
        oss << "reserved";
        break;
    }

    return oss.str();
}

}
