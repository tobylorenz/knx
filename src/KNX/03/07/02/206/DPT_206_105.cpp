// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/206/DPT_206_105.h>

#include <sstream>

namespace KNX {

DPT_206_105::DPT_206_105() :
    DPT_206(105)
{
}

std::string DPT_206_105::text() const
{
    std::ostringstream oss;

    oss << "delay time: " << time << " min"
        << ", building mode: ";
    switch (mode) {
    case 0:
        oss << "building in use";
        break;
    case 1:
        oss << "building not used";
        break;
    case 2:
        oss << "building Protection";
        break;
    default:
        oss << "reserved";
        break;
    }

    return oss.str();
}

}
