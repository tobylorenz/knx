// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/206/DPT_206.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 206.105 time delay & building mode
 *
 * @ingroup KNX_03_07_02_04_12_04
 */
class KNX_EXPORT DPT_206_105 : public DPT_206
{
public:
    explicit DPT_206_105();

    std::string text() const override;
};

using DPT_BuildingModeNext = DPT_206_105;

}
