// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/207/DPT_207.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_207::DPT_207(const uint16_t subnumber) :
    Datapoint_Type(207, subnumber)
{
}

void DPT_207::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    value = *first++;

    attributes = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_207::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(value);

    data.push_back(static_cast<uint8_t>(attributes.to_ulong()));

    return data;
}

}
