// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/207/DPT_207_100.h>

#include <sstream>

namespace KNX {

DPT_207_100::DPT_207_100() :
    DPT_207(100)
{
}

std::string DPT_207_100::text() const
{
    std::ostringstream oss;

    oss << "Actual relative power: " << static_cast<uint16_t>(value) << "%"
        << ", validity: " << (attributes[0] ? "true" : "false")
        << ", burner failure: " << (attributes[1] ? "true" : "false")
        << ", stage 1 of base stage active: " << (attributes[2] ? "on" : "off")
        << ", stage 2 / modulation active: " << (attributes[3] ? "on" : "off");

    return oss.str();
}

}
