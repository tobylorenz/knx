// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/207/DPT_207_101.h>

#include <sstream>

namespace KNX {

DPT_207_101::DPT_207_101() :
    DPT_207(101)
{
}

std::string DPT_207_101::text() const
{
    std::ostringstream oss;

    oss << "Requested power reduction: " << static_cast<uint16_t>(value) << "%"
        << ", power reduction is necessary: " << (attributes[0] ? "true" : "false")
        << ", overload is critical: " << (attributes[1] ? "critical" : "uncritical");

    return oss.str();
}

}
