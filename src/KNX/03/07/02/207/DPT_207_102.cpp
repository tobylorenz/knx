// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/207/DPT_207_102.h>

#include <sstream>

namespace KNX {

DPT_207_102::DPT_207_102() :
    DPT_207(102)
{
}

std::string DPT_207_102::text() const
{
    std::ostringstream oss;

    oss << "Relative demand for modulating burner: " << static_cast<uint16_t>(value) << "%"
        << ", stage 1 of base stage: " << (attributes[0] ? "on" : "off")
        << ", stage 2 for two stage burner: " << (attributes[1] ? "on" : "off");

    return oss.str();
}

}
