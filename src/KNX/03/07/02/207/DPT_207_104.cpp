// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/207/DPT_207_104.h>

#include <sstream>

namespace KNX {

DPT_207_104::DPT_207_104() :
    DPT_207(104)
{
}

std::string DPT_207_104::text() const
{
    std::ostringstream oss;

    oss << "Absolute actuator position demand: " << static_cast<uint16_t>(value) << "%"
        << ", validity: " << (attributes[0] ? "true" : "false")
        << ", absolute load priority: " << (attributes[1] ? "true" : "false")
        << ", shift load priority: " << (attributes[2] ? "true" : "false")
        << ", emergency demand (heating or cooling) for room frost protection or de-icing: " << (attributes[3] ? "true" : "false");

    return oss.str();
}

}
