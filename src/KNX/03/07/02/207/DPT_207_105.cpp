// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/207/DPT_207_105.h>

#include <sstream>

namespace KNX {

DPT_207_105::DPT_207_105() :
    DPT_207(105)
{
}

std::string DPT_207_105::text() const
{
    std::ostringstream oss;

    oss << "actual actuator position: " << static_cast<uint16_t>(value) << "%"
        << ", actuator has a failure: " << (attributes[0] ? "true" : "false")
        << ", actuator position is manually overriden: " << (attributes[1] ? "true" : "false")
        << ", actuator is currently in calibration mode: " << (attributes[2] ? "active" : "inactive")
        << ", valve is currently executing a valve kick: " << (attributes[3] ? "active" : "inactive")
        << ", actuator is currently executing a synchronization of the stroke model: " << (attributes[4] ? "active" : "inactive");

    return oss.str();
}

}
