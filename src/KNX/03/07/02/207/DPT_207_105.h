// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/207/DPT_207.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 207.105 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_13_05
 */
class KNX_EXPORT DPT_207_105 : public DPT_207
{
public:
    explicit DPT_207_105();

    std::string text() const override;
};

using DPT_StatusAct = DPT_207_105;

}
