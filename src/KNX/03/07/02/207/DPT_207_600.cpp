// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/207/DPT_207_600.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_207_600::DPT_207_600() :
    DPT_207(600)
{
}

std::string DPT_207_600::text() const
{
    std::ostringstream oss;

    oss << "Current lighting level: " << std::fixed << std::setprecision(1) << value * 100.0 / 255.0 << " %"
        << ", validity: " << (attributes[0] ? "true" : "false")
        << ", actuator is locked: " << (attributes[1] ? "true" : "false")
        << ", forced on/off control: " << (attributes[2] ? "true" : "false")
        << ", night mode: " << (attributes[3] ? "true" : "false")
        << ", staircase lighting function is active: " << (attributes[4] ? "true" : "false")
        << ", actuator is in state DIMMING: " << (attributes[5] ? "true" : "false")
        << ", actuator setvalue is locally overriden: " << (attributes[6] ? "true" : "false")
        << ", General actuator failure: " << (attributes[7] ? "true" : "false");

    return oss.str();
}

}
