// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/209/DPT_209_100.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_209_100::DPT_209_100() :
    DPT_209(100)
{
}

std::string DPT_209_100::text() const
{
    std::ostringstream oss;

    oss << "common flow temperature: " << std::fixed << std::setprecision(2) << value * 0.02 << " °C"
        << ", validity: " << (attributes[0] ? "true" : "false")
        << ", some failure in boiler sequence: " << (attributes[1] ? "true" : "false")
        << ", boiler sequence switched off due to local summer/winter mode: " << (attributes[2] ? "true" : "false")
        << ", boiler sequence is permanently off: " << (attributes[3] ? "true" : "false")
        << ", boiler sequence is temporary not producing heat: " << (attributes[4] ? "true" : "false");

    return oss.str();
}

}
