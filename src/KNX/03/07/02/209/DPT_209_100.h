// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/209/DPT_209.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 209.100 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_14_01
 */
class KNX_EXPORT DPT_209_100 : public DPT_209
{
public:
    explicit DPT_209_100();

    std::string text() const override;
};

using DPT_StatusHPM = DPT_209_100;

}
