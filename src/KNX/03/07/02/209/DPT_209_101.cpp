// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/209/DPT_209_101.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_209_101::DPT_209_101() :
    DPT_209(101)
{
}

std::string DPT_209_101::text() const
{
    std::ostringstream oss;

    oss << "requested room temperature setpoint: " << std::fixed << std::setprecision(2) << value * 0.02 << " °C"
        << ", validity: " << (attributes[0] ? "true" : "false")
        << ", absolute load priority: " << (attributes[1] ? "true" : "false")
        << ", shift load priority: " << (attributes[2] ? "true" : "false")
        << ", emergency demand (heating or cooling) for room frost protection or de-icing: " << (attributes[3] ? "true" : "false");

    return oss.str();
}

}
