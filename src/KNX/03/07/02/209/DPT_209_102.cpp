// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/209/DPT_209_102.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_209_102::DPT_209_102() :
    DPT_209(102)
{
}

std::string DPT_209_102::text() const
{
    std::ostringstream oss;

    oss << "chilled water flow temperature in the cooling production segment: " << std::fixed << std::setprecision(1) << value * 100.0 / 255.0 << " %"
        << ", validity: " << (attributes[0] ? "true" : "false")
        << ", some failure in the chiller: " << (attributes[1] ? "true" : "false")
        << ", permanently off (manual switch or failure): " << (attributes[2] ? "true" : "false")
        << ", temporarily no cooling in the production segment available: " << (attributes[3] ? "true" : "false");

    return oss.str();
}

}
