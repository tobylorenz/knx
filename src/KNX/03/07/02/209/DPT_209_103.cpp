// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/209/DPT_209_103.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_209_103::DPT_209_103() :
    DPT_209(103)
{
}

std::string DPT_209_103::text() const
{
    std::ostringstream oss;

    oss << "actual temperature (flow or return) of the water temperature controller: " << std::fixed << std::setprecision(1) << value * 100.0 / 255.0 << " %"
        << ", validity: " << (attributes[0] ? "true" : "false")
        << ", some failure in the water temperature controller: " << (attributes[1] ? "true" : "false")
        << ", controller status: " << (attributes[2] ? "on" : "off");

    return oss.str();
}

}
