// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_21::DPT_21(const uint16_t subnumber) :
    Datapoint_Type(21, subnumber)
{
}

void DPT_21::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    attributes = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_21::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(static_cast<uint8_t>(attributes.to_ulong()));

    return data;
}

}
