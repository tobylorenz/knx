// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 21.* 8-bit set
 *
 * Datapoint Types "B8"
 *
 * @ingroup KNX_03_07_02_03_22
 * @ingroup KNX_03_07_02_04_04
 * @ingroup KNX_03_07_02_06_04
 * @ingroup KNX_03_07_02_08_02
 * @ingroup KNX_03_07_02_09_06
 */
class KNX_EXPORT DPT_21 : public Datapoint_Type
{
public:
    explicit DPT_21(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** attributes */
    std::bitset<8> attributes{};
};

}
