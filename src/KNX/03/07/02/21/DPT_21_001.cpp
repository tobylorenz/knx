// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_001.h>

namespace KNX {

DPT_21_001::DPT_21_001() :
    DPT_21(1),
    General_Status()
{
}

void DPT_21_001::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    DPT_21::fromData(first, last);
    General_Status::fromData(first, last);
}

std::vector<uint8_t> DPT_21_001::toData() const
{
    return DPT_21::toData();
}

std::string DPT_21_001::text() const
{
    return General_Status::text();
}

}
