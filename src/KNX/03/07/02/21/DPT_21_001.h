// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/21/DPT_21.h>
#include <KNX/03/07/02/General_Status.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 21.001 general status
 *
 * @ingroup KNX_03_07_02_03_22_01
 */
class KNX_EXPORT DPT_21_001 : public DPT_21, public General_Status
{
public:
    explicit DPT_21_001();

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;
    virtual std::string text() const;
};

using DPT_StatusGen = DPT_21_001;

}
