// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_002.h>

#include <sstream>

namespace KNX {

DPT_21_002::DPT_21_002() :
    DPT_21(2)
{
}

std::string DPT_21_002::text() const
{
    std::ostringstream oss;
    oss << "User Stopped: "
        << (attributes[0] ? "true" : "false")
        << ", Individual Address Duplication: "
        << (attributes[1] ? "true" : "false")
        << ", Verify Mode On: "
        << (attributes[2] ? "true" : "false")
        << ", Safe State On: "
        << (attributes[3] ? "true" : "false");
    return oss.str();
}

bool DPT_21_002::user_stopped() const
{
    return attributes[0];
}

void DPT_21_002::set_user_stopped(const bool user_stopped)
{
    attributes[0] = user_stopped;
}

bool DPT_21_002::individual_address_duplication() const
{
    return attributes[1];
}

void DPT_21_002::set_individual_address_duplication(const bool individual_address_duplication)
{
    attributes[1] = individual_address_duplication;
}

bool DPT_21_002::verify_mode_on() const
{
    return attributes[2];
}

void DPT_21_002::set_verify_mode_on(const bool verify_mode_on)
{
    attributes[2] = verify_mode_on;
}

bool DPT_21_002::safe_state_on() const
{
    return attributes[3];
}

void DPT_21_002::set_safe_state_on(const bool safe_state_on)
{
    attributes[3] = safe_state_on;
}

}
