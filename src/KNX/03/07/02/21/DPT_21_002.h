// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/21/DPT_21.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 21.002 device control
 *
 * @ingroup KNX_03_07_02_03_22_02
 * @ingroup KNX_03_05_01_04_02_14_02
 */
class KNX_EXPORT DPT_21_002 : public DPT_21
{
public:
    explicit DPT_21_002();

    std::string text() const override;

    /* User stopped */
    bool user_stopped() const;
    void set_user_stopped(const bool user_stopped);

    /* Individual Address duplication */
    bool individual_address_duplication() const;
    void set_individual_address_duplication(const bool individual_address_duplication);

    /* Verify Mode On */
    bool verify_mode_on() const;
    void set_verify_mode_on(const bool verify_mode_on);

    /* Safe State On */
    bool safe_state_on() const;
    void set_safe_state_on(const bool safe_state_on);
};

using DPT_Device_Control = DPT_21_002;

}
