// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_100.h>

#include <sstream>

namespace KNX {

DPT_21_100::DPT_21_100() :
    DPT_21(100)
{
}

std::string DPT_21_100::text() const
{
    std::ostringstream oss;
    oss << "ForceRequest: "
        << (attributes[0] ? "true" : "false")
        << ", Protection: "
        << (attributes[1] ? "true" : "false")
        << ", Oversupply: "
        << (attributes[2] ? "true" : "false")
        << ", Overrun: "
        << (attributes[3] ? "true" : "false")
        << ", DHWNorm: "
        << (attributes[4] ? "true" : "false")
        << ", DHWLegio: "
        << (attributes[5] ? "true" : "false")
        << ", RoomHConf: "
        << (attributes[6] ? "true" : "false")
        << ", RoomHMax: "
        << (attributes[7] ? "true" : "false");
    return oss.str();
}

}
