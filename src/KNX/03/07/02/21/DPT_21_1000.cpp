// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_1000.h>

#include <sstream>

namespace KNX {

DPT_21_1000::DPT_21_1000() :
    DPT_21(1000)
{
}

std::string DPT_21_1000::text() const
{
    std::ostringstream oss;
    oss << "Asynchronous: "
        << (attributes[0] ? "true" : "false")
        << ", BiBat Master: "
        << (attributes[1] ? "true" : "false")
        << ", BiBat Slave: "
        << (attributes[2] ? "true" : "false");
    return oss.str();
}

}
