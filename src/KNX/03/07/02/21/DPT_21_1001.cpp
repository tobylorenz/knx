// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_1001.h>

#include <sstream>

namespace KNX {

DPT_21_1001::DPT_21_1001() :
    DPT_21(1001)
{
}

std::string DPT_21_1001::text() const
{
    std::ostringstream oss;
    oss << "DoA: "
        << (attributes[0] ? "true" : "false")
        << ", KNX SN: "
        << (attributes[1] ? "true" : "false")
        << ", DoA and KNX SN: "
        << (attributes[2] ? "true" : "false");
    return oss.str();
}

}
