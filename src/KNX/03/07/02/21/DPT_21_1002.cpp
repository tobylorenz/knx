// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_1002.h>

#include <sstream>

namespace KNX {

DPT_21_1002::DPT_21_1002() :
    DPT_21(1002)
{
}

std::string DPT_21_1002::text() const
{
    std::ostringstream oss;
    oss << "Security Failure: "
        << (attributes[0] ? "There is a Security Failure." : "There is no Security Failure.");
    return oss.str();
}

}
