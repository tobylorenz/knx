// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_101.h>

#include <sstream>

namespace KNX {

DPT_21_101::DPT_21_101() :
    DPT_21(101)
{
}

std::string DPT_21_101::text() const
{
    std::ostringstream oss;
    oss << "ForceRequest: "
        << (attributes[0] ? "true" : "false");
    return oss.str();
}

}
