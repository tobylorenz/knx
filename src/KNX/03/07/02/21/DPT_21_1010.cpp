// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_1010.h>

#include <sstream>

namespace KNX {

DPT_21_1010::DPT_21_1010() :
    DPT_21(1010)
{
}

std::string DPT_21_1010::text() const
{
    std::ostringstream oss;
    oss << (attributes[0] ? "The visual effect of channel 1 is active." : "The visual effect of channel 1 is inactive.")
        << " "
        << (attributes[1] ? "The visual effect of channel 2 is active." : "The visual effect of channel 2 is inactive.")
        << " "
        << (attributes[2] ? "The visual effect of channel 3 is active." : "The visual effect of channel 3 is inactive.")
        << " "
        << (attributes[3] ? "The visual effect of channel 4 is active." : "The visual effect of channel 4 is inactive.")
        << " "
        << (attributes[4] ? "The visual effect of channel 5 is active." : "The visual effect of channel 5 is inactive.")
        << " "
        << (attributes[5] ? "The visual effect of channel 6 is active." : "The visual effect of channel 6 is inactive.")
        << " "
        << (attributes[6] ? "The visual effect of channel 7 is active." : "The visual effect of channel 7 is inactive.")
        << " "
        << (attributes[7] ? "The visual effect of channel 8 is active." : "The visual effect of channel 8 is inactive.");
    return oss.str();
}

}
