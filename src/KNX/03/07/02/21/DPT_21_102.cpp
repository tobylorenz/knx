// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_102.h>

#include <sstream>

namespace KNX {

DPT_21_102::DPT_21_102() :
    DPT_21(102)
{
}

std::string DPT_21_102::text() const
{
    std::ostringstream oss;
    oss << "Fault: "
        << (attributes[0] ? "true" : "false")
        << ", StatusECO: "
        << (attributes[1] ? "true" : "false")
        << ", TempFlowLimit: "
        << (attributes[2] ? "true" : "false")
        << ", TempReturnLimit: "
        << (attributes[3] ? "true" : "false")
        << ", StatusMorningBoost: "
        << (attributes[4] ? "true" : "false")
        << ", StatusStartOptim: "
        << (attributes[5] ? "true" : "false")
        << ", StatusStopOptim: "
        << (attributes[6] ? "true" : "false")
        << ", SummerMode: "
        << (attributes[7] ? "true" : "false");
    return oss.str();
}

}
