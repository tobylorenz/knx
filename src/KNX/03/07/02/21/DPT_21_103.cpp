// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_103.h>

#include <sstream>

namespace KNX {

DPT_21_103::DPT_21_103() :
    DPT_21(103)
{
}

std::string DPT_21_103::text() const
{
    std::ostringstream oss;
    oss << "Fault: "
        << (attributes[0] ? "fault" : "ok")
        << ", SDHWLoadActive: "
        << (attributes[1] ? "true" : "false")
        << ", SolarLoadSufficient: "
        << (attributes[2] ? "true" : "false");
    return oss.str();
}

}
