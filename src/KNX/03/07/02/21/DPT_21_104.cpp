// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_104.h>

#include <sstream>

namespace KNX {

DPT_21_104::DPT_21_104() :
    DPT_21(104)
{
}

std::string DPT_21_104::text() const
{
    std::ostringstream oss;
    oss << "Oil: "
        << (attributes[0] ? "true" : "false")
        << ", Gas: "
        << (attributes[1] ? "true" : "false")
        << ", SolidState: "
        << (attributes[2] ? "true" : "false");
    return oss.str();
}

}
