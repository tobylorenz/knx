// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_105.h>

#include <sstream>

namespace KNX {

DPT_21_105::DPT_21_105() :
    DPT_21(105)
{
}

std::string DPT_21_105::text() const
{
    std::ostringstream oss;
    oss << "Fault: "
        << (attributes[0] ? "true" : "false");
    return oss.str();
}

}
