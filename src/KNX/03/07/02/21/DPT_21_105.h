// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/21/DPT_21.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 21.105 room cooling controller status
 *
 * @ingroup KNX_03_07_02_04_04_06
 */
class KNX_EXPORT DPT_21_105 : public DPT_21
{
public:
    explicit DPT_21_105();

    std::string text() const override;
};

using DPT_StatusRCC = DPT_21_105;

}
