// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_106.h>

#include <sstream>

namespace KNX {

DPT_21_106::DPT_21_106() :
    DPT_21(106)
{
}

std::string DPT_21_106::text() const
{
    std::ostringstream oss;
    oss << "Fault: "
        << (attributes[0] ? "true" : "false")
        << ", FanActive: "
        << (attributes[1] ? "true" : "false")
        << ", Heat: "
        << (attributes[2] ? "true" : "false")
        << ", Cool: "
        << (attributes[3] ? "true" : "false");
    return oss.str();
}

}
