// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_107.h>

#include <sstream>

namespace KNX {

DPT_21_107::DPT_21_107() :
    DPT_21(107)
{
}

std::string DPT_21_107::text() const
{
    std::ostringstream oss;
    oss << "Effective value of the window status: "
        << (attributes[0] ? "true" : "false")
        << ", Effective value of the presence status: "
        << (attributes[1] ? "true" : "false")
        << ", Effective value of the comfort pus button: "
        << (attributes[2] ? "true" : "false")
        << ", Status of comfort prolongation User: "
        << (attributes[3] ? "true" : "false")
        << ", Status of HVAC Mode User: "
        << (attributes[4] ? "true" : "false");
    return oss.str();
}

}
