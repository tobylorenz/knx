// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_1200.h>

#include <sstream>

namespace KNX {

DPT_21_1200::DPT_21_1200() :
    DPT_21(1200)
{
}

std::string DPT_21_1200::text() const
{
    std::ostringstream oss;
    oss << "Status of Virtual Dry Contact 1: " << (attributes[0] ? "Virtual contact closed" : "Virtual contact open")
        << ", Status of Virtual Dry Contact 2: " << (attributes[1] ? "Virtual contact closed" : "Virtual contact open")
        << ", Status of Virtual Dry Contact 3: " << (attributes[2] ? "Virtual contact closed" : "Virtual contact open")
        << ", Status of Virtual Dry Contact 4: " << (attributes[3] ? "Virtual contact closed" : "Virtual contact open")
        << ", Status of Virtual Dry Contact 5: " << (attributes[4] ? "Virtual contact closed" : "Virtual contact open")
        << ", Status of Virtual Dry Contact 6: " << (attributes[5] ? "Virtual contact closed" : "Virtual contact open")
        << ", Status of Virtual Dry Contact 7: " << (attributes[6] ? "Virtual contact closed" : "Virtual contact open")
        << ", Status of Virtual Dry Contact 8: " << (attributes[7] ? "Virtual contact closed" : "Virtual contact open");
    return oss.str();
}

}
