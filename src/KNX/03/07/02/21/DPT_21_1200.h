// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/21/DPT_21.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 21.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_06
 */
class KNX_EXPORT DPT_21_1200 : public DPT_21
{
public:
    explicit DPT_21_1200();

    std::string text() const override;
};

using DPT_VirtualDryContact = DPT_21_1200;

}
