// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_1201.h>

#include <sstream>

namespace KNX {

DPT_21_1201::DPT_21_1201() :
    DPT_21(1201)
{
}

std::string DPT_21_1201::text() const
{
    std::ostringstream oss;
    oss << "Status of Phase 1: " << (attributes[0] ? "Phase present" : "Phase absent")
        << ", Status of Phase 2: " << (attributes[1] ? "Phase present" : "Phase absent")
        << ", Status of Phase 3: " << (attributes[2] ? "Phase present" : "Phase absent")
        << ", Status of Phase 4: " << (attributes[3] ? "Phase present" : "Phase absent")
        << ", Status of Phase 5: " << (attributes[4] ? "Phase present" : "Phase absent")
        << ", Status of Phase 6: " << (attributes[5] ? "Phase present" : "Phase absent")
        << ", Status of Phase 7: " << (attributes[6] ? "Phase present" : "Phase absent")
        << ", Status of Phase 8: " << (attributes[7] ? "Phase present" : "Phase absent");
    return oss.str();
}

}
