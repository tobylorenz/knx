// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/21/DPT_21_601.h>

#include <sstream>

namespace KNX {

DPT_21_601::DPT_21_601() :
    DPT_21(601)
{
}

std::string DPT_21_601::text() const
{
    std::ostringstream oss;
    oss << "LoadDetectionError: "
        << (attributes[0] ? "true" : "false")
        << ", Undervoltage: "
        << (attributes[1] ? "true" : "false")
        << ", Overcurrent: "
        << (attributes[2] ? "true" : "false")
        << ", Underload: "
        << (attributes[3] ? "true" : "false")
        << ", DefectiveLoad: "
        << (attributes[4] ? "true" : "false")
        << ", LampFailure: "
        << (attributes[5] ? "true" : "false")
        << ", Overheat: "
        << (attributes[6] ? "true" : "false");
    return oss.str();
}

}
