// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/210/DPT_210.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_210::DPT_210(const uint16_t subnumber) :
    Datapoint_Type(210, subnumber)
{
}

void DPT_210::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    value = (*first++ << 8) | (*first++);

    attributes = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> DPT_210::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(value >> 8);
    data.push_back(value & 0xff);

    const uint16_t attributes_value = attributes.to_ulong();
    data.push_back(attributes_value >> 8);
    data.push_back(attributes_value & 0xff);

    return data;
}

}
