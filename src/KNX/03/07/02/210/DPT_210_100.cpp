// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/210/DPT_210_100.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_210_100::DPT_210_100() :
    DPT_210(100)
{
}

std::string DPT_210_100::text() const
{
    std::ostringstream oss;

    oss << "flow temperature demand (setpoint): " << std::fixed << std::setprecision(1) << value * 100.0 / 255.0 << " %"
        << ", validity: " << (attributes[0] ? "true" : "false")
        << ", absolute load priority: " << (attributes[1] ? "true" : "false")
        << ", shift load priority: " << (attributes[2] ? "true" : "false")
        << ", flow temperature demand contains max. temperature limit: " << (attributes[3] ? "true" : "false")
        << ", flow temperature demand contains min. temperature limit: " << (attributes[4] ? "true" : "false")
        << ", heat demand from DHW: " << (attributes[5] ? "true" : "false")
        << ", demand from Room Heating or Cooling: " << (attributes[6] ? "true" : "false")
        << ", demand from Ventilation (Heating or Cooling): " << (attributes[7] ? "true" : "false")
        << ", demand from auxiliary heat or cool consumer: " << (attributes[8] ? "true" : "false")
        << ", request for water circulation in the primary distribution segment: " << (attributes[9] ? "true" : "false")
        << ", emergency demand: " << (attributes[10] ? "true" : "false")
        << ", demand from DHW while legionella function is active: " << (attributes[11] ? "true" : "false");

    return oss.str();
}

}
