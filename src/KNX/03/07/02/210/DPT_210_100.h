// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/210/DPT_210.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 210.100 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_15_01
 */
class KNX_EXPORT DPT_210_100 : public DPT_210
{
public:
    explicit DPT_210_100();

    std::string text() const override;
};

using DPT_TempFlowWaterDemAbs = DPT_210_100;

}
