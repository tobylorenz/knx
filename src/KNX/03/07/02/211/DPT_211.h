// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 211.* @note Not in MasterData yet
 *
 * Datapoint Types "U8N8"
 *
 * @ingroup KNX_03_07_02_04_16
 */
class KNX_EXPORT DPT_211 : public Datapoint_Type
{
public:
    explicit DPT_211(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** value */
    uint8_t value{};

    /** mode */
    uint8_t mode{};
};

}
