// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/211/DPT_211_100.h>

#include <sstream>

namespace KNX {

DPT_211_100::DPT_211_100() :
    DPT_211(100)
{
}

std::string DPT_211_100::text() const
{
    std::ostringstream oss;

    oss << "Energy demand of terminal unit controller: " << static_cast<uint16_t>(value) << " %"
        << ", Actual controller Mode: ";
    switch (mode) {
    case 0:
        oss << "Auto";
        break;
    case 1:
        oss << "Heat";
        break;
    case 2:
        oss << "Morning Warmup";
        break;
    case 3:
        oss << "Cool";
        break;
    case 4:
        oss << "Night Purge";
        break;
    case 5:
        oss << "Precool";
        break;
    case 6:
        oss << "Off";
        break;
    case 7:
        oss << "Test";
        break;
    case 8:
        oss << "Emergency Heat";
        break;
    case 9:
        oss << "Fan only";
        break;
    case 10:
        oss << "Free Cool";
        break;
    case 11:
        oss << "Ice";
        break;
    case 20:
        oss << "NoDem";
        break;
    default:
        oss << "reserved";
        break;
    }

    return oss.str();
}

}
