// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/211/DPT_211.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 211.100 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_16_01
 */
class KNX_EXPORT DPT_211_100 : public DPT_211
{
public:
    explicit DPT_211_100();

    std::string text() const override;
};

using DPT_EnergyDemWater = DPT_211_100;

}
