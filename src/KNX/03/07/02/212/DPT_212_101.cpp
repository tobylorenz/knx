// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/212/DPT_212_101.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_212_101::DPT_212_101() :
    DPT_212(101)
{
}

std::string DPT_212_101::text() const
{
    std::ostringstream oss;

    oss << "room temperature setpoint comfort: " << std::fixed << std::setprecision(2) << value1 * 0.02 << " °C"
        << ", room temperature setpoint standby: " << std::fixed << std::setprecision(2) << value2 * 0.02 << " °C"
        << ", room temperature setpoint economy: " << std::fixed << std::setprecision(2) << value3 * 0.02 << " °C";

    return oss.str();
}

}
