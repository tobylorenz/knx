// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/212/DPT_212.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 212.101 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_17_02
 */
class KNX_EXPORT DPT_212_101 : public DPT_212
{
public:
    explicit DPT_212_101();

    std::string text() const override;
};

using DPT_TempRoomSetpSet_3 = DPT_212_101; // DPT_TempRoomSetpSet[3]

}
