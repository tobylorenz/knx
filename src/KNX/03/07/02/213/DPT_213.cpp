// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/213/DPT_213.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_213::DPT_213(const uint16_t subnumber) :
    Datapoint_Type(213, subnumber)
{
}

void DPT_213::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 8) {
        throw DataDoesntMatchDPTException(8, data_size);
    }

    value1 = (*first++ << 8) | (*first++);

    value2 = (*first++ << 8) | (*first++);

    value3 = (*first++ << 8) | (*first++);

    value4 = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> DPT_213::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(value1 >> 8);
    data.push_back(value1 & 0xff);

    data.push_back(value2 >> 8);
    data.push_back(value2 & 0xff);

    data.push_back(value3 >> 8);
    data.push_back(value3 & 0xff);

    data.push_back(value4 >> 8);
    data.push_back(value4 & 0xff);

    return data;
}

}
