// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 213.* @note Not in MasterData yet
 *
 * Datapoint Types "V16V16V16V16"
 *
 * @ingroup KNX_03_07_02_04_18
 */
class KNX_EXPORT DPT_213 : public Datapoint_Type
{
public:
    explicit DPT_213(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** value 1 */
    int16_t value1{};

    /** value 2 */
    int16_t value2{};

    /** value 3 */
    int16_t value3{};

    /** value 4 */
    int16_t value4{};
};

}
