// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/213/DPT_213.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 213.100 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_18_01
 */
class KNX_EXPORT DPT_213_100 : public DPT_213
{
public:
    explicit DPT_213_100();

    std::string text() const override;
};

using DPT_TempRoomSetpSet_4 = DPT_213_100; // DPT_TempRoomSetpSet[4]

}
