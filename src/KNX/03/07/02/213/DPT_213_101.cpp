// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/213/DPT_213_101.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_213_101::DPT_213_101() :
    DPT_213(101)
{
}

std::string DPT_213_101::text() const
{
    std::ostringstream oss;

    oss << "DHW temperature setpoint for LegioProtect operation mode: " << std::fixed << std::setprecision(2) << value1 * 0.02 << " °C"
        << ", DHW temperature setpoint for Normal operating mode: " << std::fixed << std::setprecision(2) << value2 * 0.02 << " °C"
        << ", DHW temperature setpoint for Reducing operating mode: " << std::fixed << std::setprecision(2) << value3 * 0.02 << " °C"
        << ", DHW temperature setpoint for Off/FrostProtect operating mode: " << std::fixed << std::setprecision(2) << value3 * 0.02 << " °C";

    return oss.str();
}

}
