// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/213/DPT_213.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 213.101 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_18_02
 */
class KNX_EXPORT DPT_213_101 : public DPT_213
{
public:
    explicit DPT_213_101();

    std::string text() const override;
};

using DPT_TempDHWSetpSet_4 = DPT_213_101; // DPT_TempDHWSetpSet[4]

}
