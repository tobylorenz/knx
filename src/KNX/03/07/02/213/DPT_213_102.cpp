// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/213/DPT_213_102.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_213_102::DPT_213_102() :
    DPT_213(102)
{
}

std::string DPT_213_102::text() const
{
    std::ostringstream oss;

    oss << "room temperature setpoint shift comfort (delta value): " << std::fixed << std::setprecision(2) << value1 * 0.02 << " K"
        << ", room temperature setpoint shift standby (delta value): " << std::fixed << std::setprecision(2) << value2 * 0.02 << " K"
        << ", room temperature setpoint shift economy (delta value): " << std::fixed << std::setprecision(2) << value3 * 0.02 << " K"
        << ", room temperature setpoint shift building protection (delta value): " << std::fixed << std::setprecision(2) << value3 * 0.02 << " K";

    return oss.str();
}

}
