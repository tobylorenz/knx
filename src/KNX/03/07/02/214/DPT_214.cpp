// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/214/DPT_214.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_214::DPT_214(const uint16_t subnumber) :
    Datapoint_Type(214, subnumber)
{
}

void DPT_214::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    value1 = (*first++ << 8) | (*first++);

    value2 = *first++;

    attributes = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_214::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(value1 >> 8);
    data.push_back(value1 & 0xff);

    data.push_back(value2);

    data.push_back(static_cast<uint8_t>((attributes.to_ulong())));

    return data;
}

}
