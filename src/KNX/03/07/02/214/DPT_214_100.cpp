// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/214/DPT_214_100.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_214_100::DPT_214_100() :
    DPT_214(100)
{
}

std::string DPT_214_100::text() const
{
    std::ostringstream oss;

    oss << "flow temperature demand / requested boiler temperature: " << std::fixed << std::setprecision(2) << value1 * 0.02 << " °C"
        << ", relative demand limit: " << static_cast<uint16_t>(value2) << " %"
        << ", validity: " << (attributes[0] ? "true" : "false")
        << ", stage 1 can be activated by the BoC: " << (attributes[1] ? "enabled" : "disabled")
        << ", stage 1 is : " << (attributes[2] ? "forced" : "auto")
        << ", stage 2 can be activated by the BoC: " << (attributes[3] ? "enabled" : "disabled")
        << ", stage 2 is : " << (attributes[4] ? "forced" : "auto")
        << ", boiler pump is on: " << (attributes[5] ? "enabled" : "disabled");

    return oss.str();
}

}
