// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/214/DPT_214.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 214.100 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_19_01
 */
class KNX_EXPORT DPT_214_100 : public DPT_214
{
public:
    explicit DPT_214_100();

    std::string text() const override;
};

using DPT_PowerFlowWaterDemHPM = DPT_214_100;

}
