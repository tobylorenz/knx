// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/214/DPT_214_101.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_214_101::DPT_214_101() :
    DPT_214(101)
{
}

std::string DPT_214_101::text() const
{
    std::ostringstream oss;

    oss << "chilled water flow temperature demand: " << std::fixed << std::setprecision(2) << value1 * 0.02 << " °C"
        << ", relative demand limit: " << static_cast<uint16_t>(value2) << " %"
        << ", flow temperature valid: " << (attributes[0] ? "true" : "false")
        << ", relative demand limit valid: " << (attributes[1] ? "true" : "false")
        << ", chilled water pump enabled: " << (attributes[2] ? "true" : "false");

    return oss.str();
}

}
