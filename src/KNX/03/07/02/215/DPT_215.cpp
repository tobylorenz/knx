// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/215/DPT_215.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_215::DPT_215(const uint16_t subnumber) :
    Datapoint_Type(215, subnumber)
{
}

void DPT_215::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 5) {
        throw DataDoesntMatchDPTException(5, data_size);
    }

    value1 = (*first++ << 8) | (*first++);

    value2 = *first++;

    attributes = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> DPT_215::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(value1 >> 8);
    data.push_back(value1 & 0xff);

    data.push_back(value2);

    const uint16_t attributes_value = static_cast<uint16_t>(attributes.to_ulong());
    data.push_back(attributes_value >> 8);
    data.push_back(attributes_value & 0xff);

    return data;
}

}
