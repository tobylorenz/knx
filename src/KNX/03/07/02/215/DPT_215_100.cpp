// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/215/DPT_215_100.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_215_100::DPT_215_100() :
    DPT_215(100)
{
}

std::string DPT_215_100::text() const
{
    std::ostringstream oss;

    oss << "boiler temperature: " << std::fixed << std::setprecision(2) << value1 * 0.02 << " °C"
        << ", actual relative power of the burner: " << static_cast<uint16_t>(value2) << " %"
        << ", flow temperature valid: " << (attributes[0] ? "true" : "false")
        << ", actual relative power valid: " << (attributes[1] ? "true" : "false")
        << ", boiler failure: " << (attributes[2] ? "true" : "false")
        << ", boiler switched off due to local summer/winter mode: " << (attributes[3] ? "true" : "false")
        << ", permanently off (manual switch or failure): " << (attributes[4] ? "true" : "false")
        << ", boiler is temporary not providing heat: " << (attributes[5] ? "true" : "false")
        << ", stage 1 or base stage enabled: " << (attributes[6] ? "enable" : "disable")
        << ", stage 2 / modulation enabled: " << (attributes[7] ? "enable" : "disable")
        << ", for boiler with two stage burner: " << (attributes[8] ? "true" : "false")
        << ", power limit of boiler is reached, HPM is requested to enable next boiler in cascade: " << (attributes[9] ? "true" : "false")
        << ", boiler is in principle available but other boilers should be used with preference: " << (attributes[10] ? "true" : "false")
        << ", ChimneySweep function active: " << (attributes[11] ? "true" : "false");

    return oss.str();
}

}
