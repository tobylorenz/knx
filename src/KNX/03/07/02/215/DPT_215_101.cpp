// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/215/DPT_215_101.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_215_101::DPT_215_101() :
    DPT_215(101)
{
}

std::string DPT_215_101::text() const
{
    std::ostringstream oss;

    oss << "chilled water flow temperature: " << std::fixed << std::setprecision(2) << value1 * 0.02 << " °C"
        << ", actual relative power of the chiller: " << static_cast<uint16_t>(value2) << " %"
        << ", flow temperature valid: " << (attributes[0] ? "true" : "false")
        << ", actual relative power valid: " << (attributes[1] ? "true" : "false")
        << ", chiller running status: " << (attributes[2] ? "true" : "false")
        << ", chiller failure: " << (attributes[3] ? "true" : "false")
        << ", permanently off (manual switch of failure): " << (attributes[4] ? "true" : "false")
        << ", power limit of current stage is reached, next stage required: " << (attributes[5] ? "true" : "false")
        << ", power limit of chiller is reached, next chiller required: " << (attributes[6] ? "true" : "false")
        << ", reduce available, chiller is in principle available, but preferably another chiller is used: " << (attributes[7] ? "true" : "false");

    return oss.str();
}

}
