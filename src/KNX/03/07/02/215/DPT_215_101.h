// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/215/DPT_215.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 215.101 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_20_02
 */
class KNX_EXPORT DPT_215_101 : public DPT_215
{
public:
    explicit DPT_215_101();

    std::string text() const override;
};

using DPT_StatusCC = DPT_215_101;

}
