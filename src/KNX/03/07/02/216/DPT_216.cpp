// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/216/DPT_216.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_216::DPT_216(const uint16_t subnumber) :
    Datapoint_Type(216, subnumber)
{
}

void DPT_216::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 5) {
        throw DataDoesntMatchDPTException(5, data_size);
    }

    pnom = (*first++ << 8) | (*first++);

    bstage_limit = *first++;

    burner_type = *first++;

    fuel_type = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_216::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(pnom >> 8);
    data.push_back(pnom & 0xff);

    data.push_back(bstage_limit);

    data.push_back(burner_type);

    data.push_back(static_cast<uint8_t>(fuel_type.to_ulong()));

    return data;
}

}
