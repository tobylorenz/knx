// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 216.* @note Not in MasterData yet
 *
 * Datapoint Types "U16U8N8B8"
 *
 * @ingroup KNX_03_07_02_04_21
 */
class KNX_EXPORT DPT_216 : public Datapoint_Type
{
public:
    explicit DPT_216(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** nominal power */
    uint16_t pnom{};

    /** burner stage limit */
    uint8_t bstage_limit{};

    /** burner type */
    uint8_t burner_type{}; // actually an Enum, but defined in derived classes

    /** fuel type */
    std::bitset<8> fuel_type{};
};

}
