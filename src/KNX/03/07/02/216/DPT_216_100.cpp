// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/216/DPT_216_100.h>

#include <sstream>

namespace KNX {

DPT_216_100::DPT_216_100() :
    DPT_216(100)
{
}

std::string DPT_216_100::text() const
{
    std::ostringstream oss;

    oss << "Nominal power of burner/boiler: " << pnom << " kW"
        << ", relative power limit of stage 1: " << bstage_limit << " %"
        << ", burner type: ";
    switch (burner_type) {
    case 1:
        oss << "1 stage";
        break;
    case 2:
        oss << "2 stage";
        break;
    case 3:
        oss << "modulating";
        break;
    default:
        oss << "reserved";
        break;
    }
    oss << ", oil fuel supported: " << (fuel_type[0] ? "true" : "false")
        << ", gas fuel supported: " << (fuel_type[1] ? "true" : "false")
        << ", solid state fuel supported: " << (fuel_type[2] ? "true" : "false");

    return oss.str();
}

}
