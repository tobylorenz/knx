// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/217/DPT_217.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_217::DPT_217(const uint16_t subnumber) :
    Datapoint_Type(217, subnumber)
{
}

void DPT_217::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    magic_number = *first >> 3;

    version_number = ((*first++ & 0x07) << 2) | ((*first & 0xc0) >> 6);

    revision_number = *first++ & 0x3f;

    assert(first == last);
}

std::vector<uint8_t> DPT_217::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        ((magic_number & 0x1f) << 3) |
        ((version_number & 0x1c) >> 2));

    data.push_back(
        ((version_number & 0x03) << 6) |
        (revision_number & 0x3f));

    return data;
}

}
