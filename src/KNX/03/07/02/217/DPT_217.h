// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 217.* datapoint type version
 *
 * Datapoint Types "U5U5U6"
 *
 * @ingroup KNX_03_07_02_03_42
 */
class KNX_EXPORT DPT_217 : public Datapoint_Type
{
public:
    explicit DPT_217(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** magic number */
    uint5_t magic_number{};

    /** version number */
    uint5_t version_number{};

    /** revision number */
    uint6_t revision_number{};
};

}
