// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/217/DPT_217_001.h>

#include <sstream>

namespace KNX {

DPT_217_001::DPT_217_001() :
    DPT_217(1)
{
}

std::string DPT_217_001::text() const
{
    std::ostringstream oss;

    oss << static_cast<uint16_t>(magic_number)
        << "."
        << static_cast<uint16_t>(version_number)
        << "."
        << static_cast<uint16_t>(revision_number);

    return oss.str();
}

}
