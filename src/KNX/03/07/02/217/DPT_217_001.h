// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/217/DPT_217.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 217.001 DPT version
 *
 * @ingroup KNX_03_07_02_03_42
 */
class KNX_EXPORT DPT_217_001 : public DPT_217
{
public:
    explicit DPT_217_001();

    std::string text() const override;
};

using DPT_Version = DPT_217_001;

}
