// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/218/DPT_218.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_218::DPT_218(const uint16_t subnumber) :
    Datapoint_Type(218, subnumber)
{
}

void DPT_218::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 5) {
        throw DataDoesntMatchDPTException(5, data_size);
    }

    value = (*first++ << 24) |
            (*first++ << 16) |
            (*first++ << 8) |
            (*first++);

    status_command = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_218::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(value >> 24);
    data.push_back((value >> 16) & 0xff);
    data.push_back((value >> 8) & 0xff);
    data.push_back(value & 0xff);

    data.push_back(status_command);

    return data;
}

}
