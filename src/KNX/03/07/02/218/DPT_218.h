// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 218.* @note Not in MasterData yet
 *
 * Datapoint Types "V32Z8"
 *
 * @ingroup KNX_03_07_02_03_43
 */
class KNX_EXPORT DPT_218 : public Datapoint_Type
{
public:
    explicit DPT_218(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** value */
    int32_t value{};

    /** status/command */
    uint8_t status_command{};
};

}
