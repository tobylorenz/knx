// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/218/DPT_218_001.h>

#include <sstream>

namespace KNX {

DPT_218_001::DPT_218_001() :
    DPT_218(1)
{
}

std::string DPT_218_001::text() const
{
    std::ostringstream oss;
    oss << "volume in liter: " << value << " l";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
