// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/218/DPT_218_002.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_218_002::DPT_218_002() :
    DPT_218(2)
{
}

std::string DPT_218_002::text() const
{
    std::ostringstream oss;
    oss << "Flow reate: " << std::fixed << std::setprecision(4) << value * 0.0001 << " m³/h";
    // @note Cannot show status_command as unclear if status::text() or command::text()
    return oss.str();
}

}
