// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/218/DPT_218.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 218.002 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_43_02
 */
class KNX_EXPORT DPT_218_002 : public DPT_218
{
public:
    explicit DPT_218_002();

    std::string text() const override;
};

using DPT_FlowRate_m3_h_Z = DPT_218_002; // DPT_FlowRate_m3/h_Z

}
