// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/219/DPT_219.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_219::DPT_219(const uint16_t subnumber) :
    Datapoint_Type(219, subnumber)
{
}

void DPT_219::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    log_number = *first++;

    alarm_priority = *first++;

    application_area = *first++;

    error_class = *first++;

    attributes = *first++;

    alarm_status_attributes = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_219::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(log_number);

    data.push_back(alarm_priority);

    data.push_back(application_area);

    data.push_back(error_class);

    data.push_back(static_cast<uint8_t>(attributes.to_ulong()));

    data.push_back(static_cast<uint8_t>(alarm_status_attributes.to_ulong()));

    return data;
}

}
