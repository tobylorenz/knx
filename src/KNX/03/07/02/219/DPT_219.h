// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 219.* alarm info
 *
 * Datapoint Types "U8N8N8N8B8B8"
 *
 * @ingroup KNX_03_07_02_03_29
 */
class KNX_EXPORT DPT_219 : public Datapoint_Type
{
public:
    explicit DPT_219(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Log Number */
    uint8_t log_number{};

    /** Alarm Priority */
    uint8_t alarm_priority{}; // actually an Enum, but defined in derived classes

    /** Application Area */
    uint8_t application_area{}; // actually an Enum, but defined in derived classes

    /** Error Class */
    uint8_t error_class{}; // actually an Enum, but defined in derived classes

    /** attributes */
    std::bitset<8> attributes{};

    /** Alarm Status (attributes) */
    std::bitset<8> alarm_status_attributes{};
};

}
