// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/219/DPT_219_001.h>

#include <sstream>

namespace KNX {

DPT_219_001::DPT_219_001() :
    DPT_219(1)
{
}

std::string DPT_219_001::text() const
{
    std::ostringstream oss;

    oss << "Log Number: " << static_cast<uint16_t>(log_number)
        << ", Alarm Priority: ";
    switch (alarm_priority) {
    case 0:
        oss << "high priority";
        break;
    case 1:
        oss << "medium priority";
        break;
    case 2:
        oss << "low priority";
        break;
    case 3:
        oss << "no priority";
        break;
    }
    oss << ", Application Area: ";
    switch (application_area) {
    case 0:
        oss << "no fault";
        break;
    case 1:
        oss << "System & functions of common interest";
        break;
    case 10:
        oss << "HVAC General FB's";
        break;
    case 11:
        oss << "HVAC Hot Water Heating";
        break;
    case 12:
        oss << "HVAC Direct Electrical Heating";
        break;
    case 13:
        oss << "HVAC Terminal Units";
        break;
    case 14:
        oss << "HVAC VAC";
        break;
    case 20:
        oss << "Lighting";
        break;
    case 30:
        oss << "Security";
        break;
    case 40:
        oss << "Load Management";
        break;
    case 50:
        oss << "Shutters & Blinds";
        break;
    default:
        oss << "reserved";
        break;
    }
    oss << ", Error Class: ";
    switch (error_class) {
    case 0:
        oss << "no fault";
        break;
    case 1:
        oss << "general device fault (e.g. RAM, EEPROM, UI, Watchdog, ...)";
        break;
    case 2:
        oss << "communication fault";
        break;
    case 3:
        oss << "configuration fault";
        break;
    case 4:
        oss << "HW fault";
        break;
    case 5:
        oss << "SW fault";
        break;
    }
    oss << ", Ack_Sup: " << (attributes[0] ? "True" : "False");
    oss << ", TimeStamp_Sup: " << (attributes[1] ? "True" : "False");
    oss << ", AlarmText_Sup: " << (attributes[2] ? "True" : "False");
    oss << ", ErrorCode_Sup: " << (attributes[3] ? "True" : "False");
    oss << ", InAlarm: " << (alarm_status_attributes[0] ? "True" : "False");
    oss << ", AlarmUnAck: " << (alarm_status_attributes[1] ? "True" : "False");
    oss << ", Locked: " << (alarm_status_attributes[2] ? "True" : "False");

    return oss.str();
}

}
