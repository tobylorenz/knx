// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/219/DPT_219.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 219.001 alarm info
 *
 * @ingroup KNX_03_07_02_03_29
 */
class KNX_EXPORT DPT_219_001 : public DPT_219
{
public:
    explicit DPT_219_001();

    std::string text() const override;
};

using DPT_AlarmInfo = DPT_219_001;

}
