// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/22/DPT_22.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_22::DPT_22(const uint16_t subnumber) :
    Datapoint_Type(22, subnumber)
{
}

void DPT_22::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    attributes = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> DPT_22::toData() const
{
    std::vector<uint8_t> data;

    const uint16_t attributes_value = static_cast<uint16_t>(attributes.to_ulong());
    data.push_back(attributes_value >> 8);
    data.push_back(attributes_value & 0xff);

    return data;
}

}
