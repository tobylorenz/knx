// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 22.* 16-bit set
 *
 * Datapoint Types "B16"
 *
 * @ingroup KNX_03_07_02_04_05
 * @ingroup KNX_03_07_02_08_03
 */
class KNX_EXPORT DPT_22 : public Datapoint_Type
{
public:
    explicit DPT_22(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** attributes */
    std::bitset<16> attributes{};
};

}
