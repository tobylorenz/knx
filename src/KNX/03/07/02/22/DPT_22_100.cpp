// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/22/DPT_22_100.h>

#include <sstream>

namespace KNX {
DPT_22_100::DPT_22_100() :
    DPT_22(100)
{
}

std::string DPT_22_100::text() const
{
    std::ostringstream oss;
    oss << "Fault: "
        << (attributes[0] ? "true" : "false")
        << ", DHWLoadActive: "
        << (attributes[1] ? "true" : "false")
        << ", LegioProtActive: "
        << (attributes[2] ? "true" : "false")
        << ", DHWPushActive: "
        << (attributes[3] ? "true" : "false")
        << ", OtherEnergySourceActive: "
        << (attributes[4] ? "true" : "false")
        << ", SolarEnergyOnly: "
        << (attributes[5] ? "true" : "false")
        << ", SolarEnergySupport: "
        << (attributes[6] ? "true" : "false")
        << ", TempOptimShiftActive: "
        << (attributes[7] ? "true" : "false");
    return oss.str();
}

}
