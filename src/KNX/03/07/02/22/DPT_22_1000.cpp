// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/22/DPT_22_1000.h>

#include <sstream>

namespace KNX {

DPT_22_1000::DPT_22_1000() :
    DPT_22(1000)
{
}

DPT_22_1000::DPT_22_1000(const Medium_Code medium_code) :
    DPT_22(1000)
{
    attributes = static_cast<uint16_t>(medium_code);
}

std::string DPT_22_1000::text() const
{
    std::ostringstream oss;

    oss << "TP0: "
        << (attributes[0] ? "true" : "false")
        << ", TP1: "
        << (attributes[1] ? "true" : "false")
        << ", PL110: "
        << (attributes[2] ? "true" : "false")
        << ", PL132: "
        << (attributes[3] ? "true" : "false")
        << ", RF: "
        << (attributes[4] ? "true" : "false")
        << ", IP: "
        << (attributes[5] ? "true" : "false");

    return oss.str();
}

Medium_Code DPT_22_1000::medium_code() const
{
    return static_cast<Medium_Code>(attributes.to_ulong());
}

}
