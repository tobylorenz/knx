// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/22/DPT_22.h>
#include <KNX/03/08/01/Medium_Code.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 22.1000 media
 *
 * @ingroup KNX_03_07_02_08_03_01
 *
 * @see Medium_Code
 */
class KNX_EXPORT DPT_22_1000 : public DPT_22
{
public:
    explicit DPT_22_1000();
    explicit DPT_22_1000(const Medium_Code medium_code);

    std::string text() const override;
    Medium_Code medium_code() const;
};

using DPT_Media = DPT_22_1000;

}
