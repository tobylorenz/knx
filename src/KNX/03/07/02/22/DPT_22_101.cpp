// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/22/DPT_22_101.h>

#include <sstream>

namespace KNX {

DPT_22_101::DPT_22_101() :
    DPT_22(101)
{
}

std::string DPT_22_101::text() const
{
    std::ostringstream oss;
    oss << "Fault: "
        << (attributes[0] ? "true" : "false")
        << ", StatusEcoH: "
        << (attributes[1] ? "true" : "false")
        << ", TempFlowLimit: "
        << (attributes[2] ? "true" : "false")
        << ", TempFlowReturnLimit: "
        << (attributes[3] ? "true" : "false")
        << ", StatusMorningBoostH: "
        << (attributes[4] ? "true" : "false")
        << ", StatusStartOptim: "
        << (attributes[5] ? "true" : "false")
        << ", StatusStopOptim: "
        << (attributes[6] ? "true" : "false")
        << ", HeatingDisabled: "
        << (attributes[7] ? "true" : "false")
        << ", HeatCoolMode: "
        << (attributes[8] ? "heating" : "cooling")
        << ", StatusEcoC: "
        << (attributes[9] ? "true" : "false")
        << ", StatusPreCool: "
        << (attributes[10] ? "true" : "false")
        << ", CoolingDisabled: "
        << (attributes[11] ? "true" : "false")
        << ", DewPointStatus: "
        << (attributes[12] ? "alarm" : "no alarm")
        << ", FrostAlarm: "
        << (attributes[13] ? "alarm" : "no alarm")
        << ", OverheatAlarm: "
        << (attributes[14] ? "alarm" : "no alarm");
    return oss.str();
}

}
