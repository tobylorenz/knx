// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/22/DPT_22_1010.h>

#include <sstream>

namespace KNX {

DPT_22_1010::DPT_22_1010() :
    DPT_22(1010)
{
}

std::string DPT_22_1010::text() const
{
    std::ostringstream oss;
    oss << (attributes[0] ? "The visual effect of channel 1 is active" : "The visual effect of channel 1 is inactive")
        << ", "
        << (attributes[1] ? "The visual effect of channel 2 is active" : "The visual effect of channel 2 is inactive")
        << ", "
        << (attributes[2] ? "The visual effect of channel 3 is active" : "The visual effect of channel 3 is inactive")
        << ", "
        << (attributes[3] ? "The visual effect of channel 4 is active" : "The visual effect of channel 4 is inactive")
        << ", "
        << (attributes[4] ? "The visual effect of channel 5 is active" : "The visual effect of channel 5 is inactive")
        << ", "
        << (attributes[5] ? "The visual effect of channel 6 is active" : "The visual effect of channel 6 is inactive")
        << ", "
        << (attributes[6] ? "The visual effect of channel 7 is active" : "The visual effect of channel 7 is inactive")
        << ", "
        << (attributes[7] ? "The visual effect of channel 8 is active" : "The visual effect of channel 8 is inactive")
        << ", "
        << (attributes[8] ? "The visual effect of channel 9 is active" : "The visual effect of channel 9 is inactive")
        << ", "
        << (attributes[9] ? "The visual effect of channel 10 is active" : "The visual effect of channel 10 is inactive")
        << ", "
        << (attributes[10] ? "The visual effect of channel 11 is active" : "The visual effect of channel 11 is inactive")
        << ", "
        << (attributes[11] ? "The visual effect of channel 12 is active" : "The visual effect of channel 12 is inactive")
        << ", "
        << (attributes[12] ? "The visual effect of channel 13 is active" : "The visual effect of channel 13 is inactive")
        << ", "
        << (attributes[13] ? "The visual effect of channel 14 is active" : "The visual effect of channel 14 is inactive")
        << ", "
        << (attributes[14] ? "The visual effect of channel 15 is active" : "The visual effect of channel 15 is inactive")
        << ", "
        << (attributes[15] ? "The visual effect of channel 16 is active" : "The visual effect of channel 16 is inactive");
    return oss.str();
}

}
