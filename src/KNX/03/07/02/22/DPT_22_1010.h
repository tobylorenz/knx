// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/22/DPT_22.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 22.1010 channel activation for 16 channels
 *
 * @ingroup KNX_03_07_02_08_03_02
 */
class KNX_EXPORT DPT_22_1010 : public DPT_22
{
public:
    explicit DPT_22_1010();

    std::string text() const override;
};

using DPT_Channel_Activation_16 = DPT_22_1010;

}
