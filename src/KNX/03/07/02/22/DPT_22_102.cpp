// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/22/DPT_22_102.h>

#include <sstream>

namespace KNX {

DPT_22_102::DPT_22_102() :
    DPT_22(102)
{
}

std::string DPT_22_102::text() const
{
    std::ostringstream oss;
    oss << "Current Valve position: "
        << (attributes[0] ? "true" : "false")
        << ", Short Circuit: "
        << (attributes[1] ? "true" : "false")
        << ", Overload: "
        << (attributes[2] ? "true" : "false")
        << ", Valve kick: "
        << (attributes[3] ? "true" : "false")
        << ", Service mode: "
        << (attributes[4] ? "true" : "false")
        << ", Manual operation (overriden): "
        << (attributes[5] ? "true" : "false")
        << ", Forced Position: "
        << (attributes[6] ? "true" : "false")
        << ", Locked Position: "
        << (attributes[7] ? "true" : "false")
        << ", Calibration Mode: "
        << (attributes[8] ? "true" : "false");
    return oss.str();
}

}
