// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/22/DPT_22.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 22.102 combined status HVA
 *
 * @ingroup KNX_03_07_02_04_05_03
 */
class KNX_EXPORT DPT_22_102 : public DPT_22
{
public:
    explicit DPT_22_102();

    std::string text() const override;
};

using DPT_CombinedStatus_HVA = DPT_22_102;

}
