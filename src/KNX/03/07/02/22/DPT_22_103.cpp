// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/22/DPT_22_103.h>

#include <sstream>

namespace KNX {

DPT_22_103::DPT_22_103() :
    DPT_22(103)
{
}

std::string DPT_22_103::text() const
{
    std::ostringstream oss;
    oss << "General failure information: "
        << (attributes[0] ? "true" : "false")
        << ", Active Mode: "
        << (attributes[1] ? "true" : "false")
        << ", Dew Point Status: "
        << (attributes[2] ? "true" : "false")
        << ", Frost Alarm: "
        << (attributes[3] ? "true" : "false")
        << ", Overheat-Alarm: "
        << (attributes[4] ? "true" : "false")
        << ", Controller inactive: "
        << (attributes[5] ? "true" : "false")
        << ", Additional heating/cooling stage (2. Stage): "
        << (attributes[6] ? "true" : "false")
        << ", Heating Mode Enabled: "
        << (attributes[7] ? "true" : "false")
        << ", Cooling Mode Enabled: "
        << (attributes[8] ? "true" : "false");
    return oss.str();
}

}
