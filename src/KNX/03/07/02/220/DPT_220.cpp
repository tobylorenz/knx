// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/220/DPT_220.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_220::DPT_220(const uint16_t subnumber) :
    Datapoint_Type(220, subnumber)
{
}

void DPT_220::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    delay_time = (*first++ << 8) | (*first++);

    temp = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> DPT_220::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(delay_time >> 8);
    data.push_back(delay_time & 0xff);

    data.push_back(temp >> 8);
    data.push_back(temp & 0xff);

    return data;
}

}
