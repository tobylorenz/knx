// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/220/DPT_220_100.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_220_100::DPT_220_100() :
    DPT_220(100)
{
}

std::string DPT_220_100::text() const
{
    std::ostringstream oss;

    oss << "Time delay: " << delay_time << " min"
        << ", absolute temperature value: " << std::fixed << std::setprecision(2) << temp * 0.02 << " °C";

    return oss.str();
}

}
