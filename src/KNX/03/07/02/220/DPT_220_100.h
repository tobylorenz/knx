// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/220/DPT_220.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 220.100 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_22_01
 */
class KNX_EXPORT DPT_220_100 : public DPT_220
{
public:
    explicit DPT_220_100();

    std::string text() const override;
};

using DPT_TempHVACAbsNext = DPT_220_100;

}
