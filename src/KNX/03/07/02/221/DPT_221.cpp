// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/221/DPT_221.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_221::DPT_221(const uint16_t subnumber) :
    Datapoint_Type(221, subnumber)
{
}

void DPT_221::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    manufacturer_code = (*first++ << 8) | (*first++);
    incremented_number = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> DPT_221::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(manufacturer_code >> 8);
    data.push_back(manufacturer_code & 0xff);

    data.push_back(
        ((incremented_number >> 24) & 0xff) |
        ((incremented_number >> 16) & 0xff) |
        ((incremented_number >> 8) & 0xff) |
        (incremented_number & 0xff));

    return data;
}

}
