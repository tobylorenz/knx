// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 221.* @note Not in MasterData yet
 *
 * Datapoint Types "N16U32"
 *
 * @ingroup KNX_03_07_02_03_30
 */
class KNX_EXPORT DPT_221 : public Datapoint_Type
{
public:
    explicit DPT_221(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** manufacturer code */
    uint16_t manufacturer_code{}; // actually an Enum, but defined in derived class

    /** incremented number */
    uint32_t incremented_number{};
};

}
