// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/221/DPT_221_001.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_221_001::DPT_221_001() :
    DPT_221(1)
{
}

std::string DPT_221_001::text() const
{
    std::ostringstream oss;

    oss << std::setfill('0') << std::setw(4) << std::hex << manufacturer_code
        << std::setfill('0') << std::setw(8) << std::hex << incremented_number;

    return oss.str();
}

}
