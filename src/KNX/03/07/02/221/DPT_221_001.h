// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/221/DPT_221.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 221.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_30
 */
class KNX_EXPORT DPT_221_001 : public DPT_221
{
public:
    explicit DPT_221_001();

    std::string text() const override;
};

using DPT_SerNum = DPT_221_001;

}
