// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/222/DPT_222.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_222::DPT_222(const uint16_t subnumber) :
    Datapoint_Type(222, subnumber)
{
}

void DPT_222::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    temp_setp_comf.fromData(first, first + 2);
    first += 2;
    temp_setp_stdby.fromData(first, first + 2);
    first += 2;
    temp_setp_eco.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> DPT_222::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> temp_setp_comf_data = temp_setp_comf.toData();
    data.insert(std::cend(data), std::cbegin(temp_setp_comf_data), std::cend(temp_setp_comf_data));

    const std::vector<uint8_t> temp_setp_stdby_data = temp_setp_stdby.toData();
    data.insert(std::cend(data), std::cbegin(temp_setp_stdby_data), std::cend(temp_setp_stdby_data));

    const std::vector<uint8_t> temp_setp_eco_data = temp_setp_eco.toData();
    data.insert(std::cend(data), std::cbegin(temp_setp_eco_data), std::cend(temp_setp_eco_data));

    return data;
}

}
