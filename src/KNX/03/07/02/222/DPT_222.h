// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/03/07/02/KNX_Float.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 222.* 3x 2-byte float value
 *
 * Datapoint Types "F16F16F16"
 *
 * @ingroup KNX_03_07_02_04_23
 */
class KNX_EXPORT DPT_222 : public Datapoint_Type
{
public:
    explicit DPT_222(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** room temperature setpoint (shift) comfort */
    KNX_Float temp_setp_comf;

    /** room temperature setpoint (shift) standby */
    KNX_Float temp_setp_stdby;

    /** room temperature setpoint (shift) economy */
    KNX_Float temp_setp_eco;
};

}
