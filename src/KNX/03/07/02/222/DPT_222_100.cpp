// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/222/DPT_222_100.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_222_100::DPT_222_100() :
    DPT_222(100)
{
}

std::string DPT_222_100::text() const
{
    std::ostringstream oss;

    oss << "TempSetpComf: " << std::fixed << std::setprecision(2) << temp_setp_comf << " °C"
        << ", TempSetpStdby: " << std::fixed << std::setprecision(2) << temp_setp_stdby << " °C"
        << ", TempSetpEco: " << std::fixed << std::setprecision(2) << temp_setp_eco << " °C";

    return oss.str();
}

}
