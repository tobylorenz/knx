// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/222/DPT_222.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 222.101 room temperature setpoint shift
 *
 * @ingroup KNX_03_07_02_04_23_02
 */
class KNX_EXPORT DPT_222_101 : public DPT_222
{
public:
    explicit DPT_222_101();

    std::string text() const override;
};

using DPT_TempRoomSetpSetShiftF16_3 = DPT_222_101; // DPT_TempRoomSetpSetShiftF16[3]

}
