// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/223/DPT_223.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_223::DPT_223(const uint16_t subnumber) :
    Datapoint_Type(223, subnumber)
{
}

void DPT_223::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    energy_dem = *first++;
    contr_mode_act = *first++;
    hvac_emerg_mode = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_223::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(energy_dem);
    data.push_back(contr_mode_act);
    data.push_back(hvac_emerg_mode);

    return data;
}

}
