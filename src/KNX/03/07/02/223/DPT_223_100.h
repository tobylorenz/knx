// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/223/DPT_223.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 223.100 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_24_01
 */
class KNX_EXPORT DPT_223_100 : public DPT_223
{
public:
    explicit DPT_223_100();

    std::string text() const override;
};

using DPT_EnergyDemAir = DPT_223_100;

}
