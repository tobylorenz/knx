// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/224/DPT_224.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_224::DPT_224(const uint16_t subnumber) :
    Datapoint_Type(224, subnumber)
{
}

void DPT_224::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    temp_setp_cooling = (*first << 8) | (*first++);
    temp_setp_heating = (*first << 8) | (*first++);
    contr_mode_act = *first++;
    hvac_emerg_mode = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_224::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(temp_setp_cooling >> 8);
    data.push_back(temp_setp_cooling & 0xff);
    data.push_back(temp_setp_heating >> 8);
    data.push_back(temp_setp_heating & 0xff);
    data.push_back(contr_mode_act);
    data.push_back(hvac_emerg_mode);

    return data;
}

}
