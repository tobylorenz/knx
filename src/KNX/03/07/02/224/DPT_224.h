// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 224.* @note Not in MasterData yet
 *
 * Datapoint Types "V16V16N8N8"
 *
 * @ingroup KNX_03_07_02_04_25
 */
class KNX_EXPORT DPT_224 : public Datapoint_Type
{
public:
    explicit DPT_224(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** TempSetpCooling */
    int16_t temp_setp_cooling{};

    /** TempSetpHeating */
    int16_t temp_setp_heating{};

    /** ContrModeAct */
    uint8_t contr_mode_act{};

    /** HVACEmergMode */
    uint8_t hvac_emerg_mode{};
};

}
