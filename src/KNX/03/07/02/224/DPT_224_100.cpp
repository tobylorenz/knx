// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/224/DPT_224_100.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_224_100::DPT_224_100() :
    DPT_224(100)
{
}

std::string DPT_224_100::text() const
{
    std::ostringstream oss;

    oss << "Supply air temperature cooling setpoint: " << std::fixed << std::setprecision(2) << temp_setp_cooling * 0.02 << " °C"
        << ", Supply air temperature heating setpoint: " << std::fixed << std::setprecision(2) << temp_setp_heating * 0.02 << " °C"
        << ", Actual controller Mode: ";
    switch (contr_mode_act) {
    case 0:
        oss << "Auto";
        break;
    case 1:
        oss << "Heat";
        break;
    case 2:
        oss << "Morning Warmup";
        break;
    case 3:
        oss << "Cool";
        break;
    case 4:
        oss << "Night Purge";
        break;
    case 5:
        oss << "Precool";
        break;
    case 6:
        oss << "Off";
        break;
    case 7:
        oss << "Test";
        break;
    case 8:
        oss << "Emergency Heat";
        break;
    case 9:
        oss << "Fan only";
        break;
    case 10:
        oss << "Free Cool";
        break;
    case 11:
        oss << "Ice";
        break;
    case 20:
        oss << "NoDem";
        break;
    default:
        oss << "reserved";
        break;
    }
    oss << ", Actual HVAC Emergency Mode: ";
    switch (hvac_emerg_mode) {
    case 0:
        oss << "Normal";
        break;
    case 1:
        oss << "EmergPressure";
        break;
    case 2:
        oss << "EmergDepressure";
        break;
    case 3:
        oss << "EmergPurge";
        break;
    case 4:
        oss << "EmergShutdown";
        break;
    case 5:
        oss << "EmergFire";
        break;
    default:
        oss << "reserved";
        break;
    }

    return oss.str();
}

}
