// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/225/DPT_225.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_225::DPT_225(const uint16_t subnumber) :
    Datapoint_Type(225, subnumber)
{
}

void DPT_225::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    time = (*first << 8) | (*first++);
    percent = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_225::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(time >> 8);
    data.push_back(time & 0xff);
    data.push_back(percent);

    return data;
}

}
