// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/225/DPT_225_001.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_225_001::DPT_225_001() :
    DPT_225(1)
{
}

std::string DPT_225_001::text() const
{
    std::ostringstream oss;

    oss << "time period: " << time * 100 << " ms"
        << ", percent: " << std::fixed << std::setprecision(1) << percent * 100.0 / 255.0 << " %";

    return oss.str();
}

}
