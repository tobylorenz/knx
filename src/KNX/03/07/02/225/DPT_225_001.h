// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/225/DPT_225.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 225.001 scaling speed
 *
 * @ingroup KNX_03_07_02_03_44_01
 */
class KNX_EXPORT DPT_225_001 : public DPT_225
{
public:
    explicit DPT_225_001();

    std::string text() const override;
};

using DPT_ScalingSpeed = DPT_225_001;

}
