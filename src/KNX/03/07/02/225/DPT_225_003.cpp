// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/225/DPT_225_003.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_225_003::DPT_225_003() :
    DPT_225(3)
{
}

std::string DPT_225_003::text() const
{
    std::ostringstream oss;

    oss << "Delay Time: " << time << " min"
        << ", Tariff: " << static_cast<uint16_t>(percent);

    return oss.str();
}

}
