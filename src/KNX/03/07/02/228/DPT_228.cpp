// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/228/DPT_228.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_228::DPT_228(const uint16_t subnumber) :
    Datapoint_Type(228, subnumber)
{
}

void DPT_228::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    prio = (*first >> 3) & 0x1f;
    size = *first & 0x07;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_228::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((prio << 3) | size);

    return data;
}

}
