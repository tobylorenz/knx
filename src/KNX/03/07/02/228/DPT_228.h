// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 228.* @note Not in MasterData yet
 *
 * @note ingroup: Not in specification
 */
class KNX_EXPORT DPT_228 : public Datapoint_Type
{
public:
    explicit DPT_228(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Prio */
    uint5_t prio{};

    /** Size */
    uint3_t size{};
};

}
