// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/228/DPT_228_1000.h>

#include <sstream>

namespace KNX {

DPT_228_1000::DPT_228_1000() :
    DPT_228(1000)
{
}

std::string DPT_228_1000::text() const
{
    std::ostringstream oss;

    oss << "Prio: " << prio
        << ", Size: " << size;

    return oss.str();
}

}
