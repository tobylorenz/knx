// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/228/DPT_228.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 228.1000 @note Not in MasterData yet
 *
 * @note ingroup: Not in specification
 */
class KNX_EXPORT DPT_228_1000 : public DPT_228
{
public:
    explicit DPT_228_1000();

    std::string text() const override;
};

using DPT_Adaptive_Selection = DPT_228_1000;

}
