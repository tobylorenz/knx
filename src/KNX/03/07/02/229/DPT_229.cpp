// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/229/DPT_229.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_229::DPT_229(const uint16_t subnumber) :
    Datapoint_Type(229, subnumber)
{
}

void DPT_229::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    count_val = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | (*first++);
    val_inf_field = *first++;
    status_command = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_229::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((count_val >> 24) & 0xff);
    data.push_back((count_val >> 16) & 0xff);
    data.push_back((count_val >> 8) & 0xff);
    data.push_back(count_val & 0xff);

    data.push_back(val_inf_field);

    data.push_back(status_command);

    return data;
}

}
