// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 229.* 4-1-1 byte combined information
 *
 * Datapoint Types "V32N8Z8"
 *
 * @ingroup KNX_03_07_02_03_45
 */
class KNX_EXPORT DPT_229 : public Datapoint_Type
{
public:
    explicit DPT_229(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** CountVal */
    int32_t count_val{};

    /** ValInfField */
    uint8_t val_inf_field{};

    /** Status/Command */
    uint8_t status_command;
};

}
