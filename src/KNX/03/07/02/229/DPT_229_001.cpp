// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/229/DPT_229_001.h>

#include <sstream>

namespace KNX {

DPT_229_001::DPT_229_001() :
    DPT_229(1)
{
}

std::string DPT_229_001::text() const
{
    std::ostringstream oss;

    oss << "CountVal: " << count_val
        << ", ValInfField: ";
    switch (val_inf_field) {
    case 0:
        oss << "energy, 0.001 Wh";
        break;
    case 1:
        oss << "energy, 0.01 Wh";
        break;
    case 2:
        oss << "energy, 0.1 Wh";
        break;
    case 3:
        oss << "energy, 1 Wh";
        break;
    case 4:
        oss << "energy, 10 Wh";
        break;
    case 5:
        oss << "energy, 100 Wh";
        break;
    case 6:
        oss << "energy, 1000 Wh";
        break;
    case 7:
        oss << "energy, 10000 Wh";
        break;
    case 8:
        oss << "energy, 0.001 kJ";
        break;
    case 9:
        oss << "energy, 0.01 kJ";
        break;
    case 10:
        oss << "energy, 0.1 kJ";
        break;
    case 11:
        oss << "energy, 1 kJ";
        break;
    case 12:
        oss << "energy, 10 kJ";
        break;
    case 13:
        oss << "energy, 100 kJ";
        break;
    case 14:
        oss << "energy, 1000 kJ";
        break;
    case 15:
        oss << "energy, 10000 kJ";
        break;
    case 16:
        oss << "volume, 0.001 l";
        break;
    case 17:
        oss << "volume, 0.01 l";
        break;
    case 18:
        oss << "volume, 0.1 l";
        break;
    case 19:
        oss << "volume, 1 l";
        break;
    case 20:
        oss << "volume, 10 l";
        break;
    case 21:
        oss << "volume, 100 l";
        break;
    case 22:
        oss << "volume, 1000 l";
        break;
    case 23:
        oss << "volume, 10000 l";
        break;
    case 24:
        oss << "mass, 0.001 kg";
        break;
    case 25:
        oss << "mass, 0.01 kg";
        break;
    case 26:
        oss << "mass, 0.1 kg";
        break;
    case 27:
        oss << "mass, 1 kg";
        break;
    case 28:
        oss << "mass, 10 kg";
        break;
    case 29:
        oss << "mass, 100 kg";
        break;
    case 30:
        oss << "mass, 1000 kg";
        break;
    case 31:
        oss << "mass, 10000 kg";
        break;
    case 40:
        oss << "power, 0.001 W";
        break;
    case 41:
        oss << "power, 0.01 W";
        break;
    case 42:
        oss << "power, 0.1 W";
        break;
    case 43:
        oss << "power, 1 W";
        break;
    case 44:
        oss << "power, 10 W";
        break;
    case 45:
        oss << "power, 100 W";
        break;
    case 46:
        oss << "power, 1000 W";
        break;
    case 47:
        oss << "power, 10000 W";
        break;
    case 48:
        oss << "power, 0.001 kJ/h";
        break;
    case 49:
        oss << "power, 0.01 kJ/h";
        break;
    case 50:
        oss << "power, 0.1 kJ/h";
        break;
    case 51:
        oss << "power, 1 kJ/h";
        break;
    case 52:
        oss << "power, 10 kJ/h";
        break;
    case 53:
        oss << "power, 100 kJ/h";
        break;
    case 54:
        oss << "power, 1000 kJ/h";
        break;
    case 55:
        oss << "power, 10000 kJ/h";
        break;
    case 56:
        oss << "volume flow, 0.001 l/h";
        break;
    case 57:
        oss << "volume flow, 0.01 l/h";
        break;
    case 58:
        oss << "volume flow, 0.1 l/h";
        break;
    case 59:
        oss << "volume flow, 1 l/h";
        break;
    case 60:
        oss << "volume flow, 10 l/h";
        break;
    case 61:
        oss << "volume flow, 100 l/h";
        break;
    case 62:
        oss << "volume flow, 1000 l/h";
        break;
    case 63:
        oss << "volume flow, 10000 l/h";
        break;
    case 64:
        oss << "volume flow, 0.0001 l/min";
        break;
    case 65:
        oss << "volume flow, 0.001 l/min";
        break;
    case 66:
        oss << "volume flow, 0.01 l/min";
        break;
    case 67:
        oss << "volume flow, 0.1 l/min";
        break;
    case 68:
        oss << "volume flow, 1 l/min";
        break;
    case 69:
        oss << "volume flow, 10 l/min";
        break;
    case 70:
        oss << "volume flow, 100 l/min";
        break;
    case 71:
        oss << "volume flow, 1000 l/min";
        break;
    case 72:
        oss << "volume flow, 0.001 ml/s";
        break;
    case 73:
        oss << "volume flow, 0.01 ml/s";
        break;
    case 74:
        oss << "volume flow, 0.1 ml/s";
        break;
    case 75:
        oss << "volume flow, 1 ml/s";
        break;
    case 76:
        oss << "volume flow, 10 ml/s";
        break;
    case 77:
        oss << "volume flow, 100 ml/s";
        break;
    case 78:
        oss << "volume flow, 1000 ml/s";
        break;
    case 79:
        oss << "volume flow, 10000 ml/s";
        break;
    case 80:
        oss << "mass flow, 0.001 kg/h";
        break;
    case 81:
        oss << "mass flow, 0.01 kg/h";
        break;
    case 82:
        oss << "mass flow, 0.1 kg/h";
        break;
    case 83:
        oss << "mass flow, 1 kg/h";
        break;
    case 84:
        oss << "mass flow, 10 kg/h";
        break;
    case 85:
        oss << "mass flow, 100 kg/h";
        break;
    case 86:
        oss << "mass flow, 1000 kg/h";
        break;
    case 87:
        oss << "mass flow, 10000 kg/h";
        break;
    case 110:
        oss << "Units for HCA";
        break;
    case 128:
        oss << "energy, 0.1 MWh";
        break;
    case 129:
        oss << "energy, 1 MWh";
        break;
    case 136:
        oss << "energy, 0.1 GJ";
        break;
    case 137:
        oss << "energy, 1 GJ";
        break;
    case 168:
        oss << "power, 0.1 MW";
        break;
    case 169:
        oss << "power, 1 MW";
        break;
    case 176:
        oss << "power, 0.1 GJ/h";
        break;
    case 177:
        oss << "power, 1 GJ/h";
        break;
    default:
        oss << "reserved";
        break;
    }

    // @note Cannot show status_command as unclear if status::text() or command::text()

    return oss.str();
}

}
