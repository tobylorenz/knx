// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/229/DPT_229.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 229.001 metering value (value,encoding,cmd)
 *
 * @ingroup KNX_03_07_02_03_45_01
 */
class KNX_EXPORT DPT_229_001 : public DPT_229
{
public:
    explicit DPT_229_001();

    std::string text() const override;
};

using DPT_MeteringValue = DPT_229_001;

}
