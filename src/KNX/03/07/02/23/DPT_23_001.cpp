// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/23/DPT_23_001.h>

#include <sstream>

namespace KNX {

DPT_23_001::DPT_23_001() :
    DPT_23(1)
{
}

std::string DPT_23_001::text() const
{
    std::ostringstream oss;
    switch (s) {
    case 0:
        oss << "off";
        break;
    case 1:
        oss << "on";
        break;
    case 2:
        oss << "off/on";
        break;
    case 3:
        oss << "on/off";
        break;
    }
    return oss.str();
}

}
