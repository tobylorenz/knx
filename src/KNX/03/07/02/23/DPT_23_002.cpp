// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/23/DPT_23_002.h>

#include <sstream>

namespace KNX {

DPT_23_002::DPT_23_002() :
    DPT_23(2)
{
}

std::string DPT_23_002::text() const
{
    std::ostringstream oss;
    switch (s) {
    case 0:
        oss << "no alarm is used";
        break;
    case 1:
        oss << "alarm position is UP";
        break;
    case 2:
        oss << "alarm position is DOWN";
        break;
    case 3:
        oss << "reserved";
        break;
    }
    return oss.str();
}

}
