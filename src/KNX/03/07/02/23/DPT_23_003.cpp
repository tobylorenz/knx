// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/23/DPT_23_003.h>

#include <sstream>

namespace KNX {

DPT_23_003::DPT_23_003() :
    DPT_23(3)
{
}

std::string DPT_23_003::text() const
{
    std::ostringstream oss;
    switch (s) {
    case 0:
        oss << "Up";
        break;
    case 1:
        oss << "Down";
        break;
    case 2:
        oss << "UpDown";
        break;
    case 3:
        oss << "DownUp";
        break;
    }
    return oss.str();
}

}
