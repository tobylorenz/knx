// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/23/DPT_23_102.h>

#include <sstream>

namespace KNX {

DPT_23_102::DPT_23_102() :
    DPT_23(102)
{
}

std::string DPT_23_102::text() const
{
    std::ostringstream oss;
    switch (s) {
    case 0:
        oss << "Comfort/Economy";
        break;
    case 1:
        oss << "Comfort/Nothing";
        break;
    case 2:
        oss << "Economy/Nothing";
        break;
    case 3:
        oss << "Building prot/Auto";
        break;
    }
    return oss.str();
}

}
