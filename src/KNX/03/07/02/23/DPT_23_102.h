// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/23/DPT_23.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 23.102 HVAC push button action
 *
 * @ingroup KNX_03_07_02_04_06
 */
class KNX_EXPORT DPT_23_102: public DPT_23
{
public:
    explicit DPT_23_102();

    std::string text() const override;
};

using DPT_HVAC_PB_Action = DPT_23_102;

}
