// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/230/DPT_230.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_230::DPT_230(const uint16_t subnumber) :
    Datapoint_Type(230, subnumber)
{
}

void DPT_230::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 8) {
        throw DataDoesntMatchDPTException(8, data_size);
    }

    manufact_id = (*first++ << 8) | (*first++);
    ident_number = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | (*first++);
    version = *first++;
    medium = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_230::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(manufact_id >> 8);
    data.push_back(manufact_id & 0xff);

    data.push_back((ident_number >> 24) & 0xff);
    data.push_back((ident_number >> 16) & 0xff);
    data.push_back((ident_number >> 8) & 0xff);
    data.push_back(ident_number & 0xff);

    data.push_back(version);

    data.push_back(medium);

    return data;
}

}
