// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 230.* MBus address
 *
 * Datapoint Types "U16U32U8N8"
 *
 * @ingroup KNX_03_07_02_08_06
 */
class KNX_EXPORT DPT_230 : public Datapoint_Type
{
public:
    explicit DPT_230(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** ManufactID */
    uint16_t manufact_id{};

    /** IdentNumber */
    uint32_t ident_number{};

    /** Version */
    uint8_t version{};

    /** Medium */
    uint8_t medium{};
};

}
