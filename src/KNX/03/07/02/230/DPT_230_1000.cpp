// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/230/DPT_230_1000.h>

#include <sstream>

namespace KNX {

DPT_230_1000::DPT_230_1000() :
    DPT_230(1000)
{
}

std::string DPT_230_1000::text() const
{
    std::ostringstream oss;

    oss << "Manufacturer identification: "
        << manufact_id
        << ", Identification number: "
        << ident_number
        << ", Device Version: "
        << static_cast<uint16_t>(version)
        << ", Measured medium: ";
    switch (medium) {
    case 0:
        oss << "Other";
        break;
    case 1:
        oss << "Oil meter";
        break;
    case 2:
        oss << "Electricity meter";
        break;
    case 3:
        oss << "Gas meter";
        break;
    case 4:
        oss << "Heat meter";
        break;
    case 5:
        oss << "Steam meter";
        break;
    case 6:
        oss << "Warm Water meter (30 °C to 90 °C)";
        break;
    case 7:
        oss << "Water meter";
        break;
    case 8:
        oss << "Heat cost allocator";
        break;
    case 9:
        oss << "Compressed Air";
        break;
    case 10:
        oss << "Cooling Load meter (inlet)";
        break;
    case 11:
        oss << "Cooling Load meter (outlet)";
        break;
    case 12:
        oss << "Heat (inlet)";
        break;
    case 13:
        oss << "Heat and Cool";
        break;
    case 14:
        oss << "Bus/System";
        break;
    case 15:
        oss << "Unknown Device type";
        break;
    case 32:
        oss << "Breaker (electricity)";
        break;
    case 33:
        oss << "Valve (gas or water)";
        break;
    case 40:
        oss << "Waste water meter";
        break;
    case 41:
        oss << "Garbage";
        break;
    case 55:
        oss << "Radio converter (meter side)";
        break;
    default:
        oss << "reserved";
        break;
    }

    return oss.str();
}

}
