// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/230/DPT_230.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 230.1000 MBus address
 *
 * @ingroup KNX_03_07_02_08_06
 */
class KNX_EXPORT DPT_230_1000 : public DPT_230
{
public:
    explicit DPT_230_1000();

    std::string text() const override;
};

using DPT_MBus_Address = DPT_230_1000;

}
