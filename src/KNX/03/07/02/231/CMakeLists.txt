# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_231.h
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_231_001.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_231.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_231_001.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_231.h
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_231_001.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/07/02/231)
