// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/231/DPT_231.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_231::DPT_231(const uint16_t subnumber) :
    Datapoint_Type(231, subnumber)
{
}

void DPT_231::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    std::copy(first, first + 4, std::begin(character));
    first += 4;

    assert(first == last);
}

std::vector<uint8_t> DPT_231::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(character), std::cend(character));

    return data;
}

}
