// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/231/DPT_231_001.h>

#include <sstream>

namespace KNX {

DPT_231_001::DPT_231_001() :
    DPT_231(1)
{
}

std::string DPT_231_001::text() const
{
    std::ostringstream oss;

    oss << character[0] << character[1]
        << "-"
        << character[2] << character[3];

    return oss.str();
}

}
