// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/231/DPT_231.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 231.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_46_01
 */
class KNX_EXPORT DPT_231_001 : public DPT_231
{
public:
    explicit DPT_231_001();

    std::string text() const override;
};

using DPT_Locale_ASCII = DPT_231_001;

}
