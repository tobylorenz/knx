// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/232/DPT_232.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_232::DPT_232(const uint16_t subnumber) :
    Datapoint_Type(232, subnumber)
{
}

void DPT_232::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    r = *first++;
    g = *first++;
    b = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_232::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(r);
    data.push_back(g);
    data.push_back(b);

    return data;
}

}
