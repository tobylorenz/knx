// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/232/DPT_232_600.h>

#include <sstream>

namespace KNX {

DPT_232_600::DPT_232_600() :
    DPT_232(600)
{
}

std::string DPT_232_600::text() const
{
    std::ostringstream oss;

    oss << static_cast<uint16_t>(r)
        << "/"
        << static_cast<uint16_t>(g)
        << "/"
        << static_cast<uint16_t>(b);

    return oss.str();
}

}
