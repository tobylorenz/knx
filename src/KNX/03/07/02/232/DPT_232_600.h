// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/232/DPT_232.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 232.600 RGB value 3x(0..255)
 *
 * @ingroup KNX_03_07_02_06_06_01
 */
class KNX_EXPORT DPT_232_600 : public DPT_232
{
public:
    explicit DPT_232_600();

    std::string text() const override;
};

using DPT_Colour_RGB = DPT_232_600;

}
