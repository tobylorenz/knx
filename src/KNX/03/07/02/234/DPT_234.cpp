// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/234/DPT_234.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_234::DPT_234(const uint16_t subnumber) :
    Datapoint_Type(234, subnumber)
{
}

void DPT_234::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    std::copy(first, first + 2, std::begin(character));
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> DPT_234::toData() const
{
    std::vector<uint8_t> data;

    data.insert(std::cend(data), std::cbegin(character), std::cend(character));

    return data;
}

}
