// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 234.* language code ISO 639-1
 *
 * Datapoint Types "A8A8"
 *
 * @ingroup KNX_03_07_02_03_47
 */
class KNX_EXPORT DPT_234 : public Datapoint_Type
{
public:
    explicit DPT_234(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** character */
    std::array<char, 2> character;
};

}
