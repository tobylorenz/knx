// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/234/DPT_234.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 234.001 language code (ASCII)
 *
 * @ingroup KNX_03_07_02_03_47_01
 */
class KNX_EXPORT DPT_234_001 : public DPT_234
{
public:
    explicit DPT_234_001();

    std::string text() const override;
};

using DPT_LanguageCodeAlpha2_ASCII = DPT_234_001;

}
