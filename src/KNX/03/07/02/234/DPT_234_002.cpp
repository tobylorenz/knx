// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/234/DPT_234_002.h>

#include <sstream>

namespace KNX {

DPT_234_002::DPT_234_002() :
    DPT_234(2)
{
}

std::string DPT_234_002::text() const
{
    std::ostringstream oss;

    oss << character[0] << character[1];

    return oss.str();
}

}
