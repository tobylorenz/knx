// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/235/DPT_235.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_235::DPT_235(const uint16_t subnumber) :
    Datapoint_Type(235, subnumber)
{
}

void DPT_235::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    active_electrical_energy = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | (*first++);
    tariff = *first++;
    validity = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_235::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((active_electrical_energy >> 24) & 0xff);
    data.push_back((active_electrical_energy >> 16) & 0xff);
    data.push_back((active_electrical_energy >> 8) & 0xff);
    data.push_back(active_electrical_energy & 0xff);

    data.push_back(tariff);

    data.push_back(static_cast<uint8_t>(validity.to_ulong()));

    return data;
}

}
