// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 235.* Signed value with classification and validity
 *
 * Datapoint Types "V32U8B8"
 *
 * @ingroup KNX_03_07_02_03_48
 */
class KNX_EXPORT DPT_235 : public Datapoint_Type
{
public:
    explicit DPT_235(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Active Electrical Energy */
    int32_t active_electrical_energy{};

    /** Tariff */
    uint8_t tariff{};

    /** Validity */
    std::bitset<8> validity;
};

}
