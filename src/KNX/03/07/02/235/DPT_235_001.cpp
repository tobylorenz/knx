// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/235/DPT_235_001.h>

#include <sstream>

namespace KNX {

DPT_235_001::DPT_235_001() :
    DPT_235(1)
{
}

std::string DPT_235_001::text() const
{
    std::ostringstream oss;

    oss << "ActiveElectricalEnergy: " << active_electrical_energy << " Wh"
        << ", Tariff: " << static_cast<uint16_t>(tariff)
        << ", Tariff valid: " << (validity[0] ? "not valid" : "valid")
        << ", ActiveElectricalEnergy valid: " << (validity[1] ? "not valid" : "valid");

    return oss.str();
}

}
