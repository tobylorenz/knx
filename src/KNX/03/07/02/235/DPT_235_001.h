// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/235/DPT_235.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 235.001 electrical energy with tariff
 *
 * @ingroup KNX_03_07_02_03_48
 */
class KNX_EXPORT DPT_235_001 : public DPT_235
{
public:
    explicit DPT_235_001();

    std::string text() const override;
};

using DPT_Tariff_ActiveEnergy = DPT_235_001;

}
