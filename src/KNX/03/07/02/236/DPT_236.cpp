// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/236/DPT_236.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_236::DPT_236(const uint16_t subnumber) :
    Datapoint_Type(236, subnumber)
{
}

void DPT_236::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    d = *first >> 7;
    p = (*first >> 4) & 0x07;
    m = *first & 0x0f;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_236::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((d << 7) | (p << 4) | m);

    return data;
}

}
