// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/236/DPT_236_001.h>

#include <sstream>

namespace KNX {

DPT_236_001::DPT_236_001() :
    DPT_236(1)
{
}

std::string DPT_236_001::text() const
{
    std::ostringstream oss;

    oss << (d ? "deactivation of priority" : "activation of priority")
        << ", priority level: Level " << static_cast<uint16_t>(p)
        << ", mode level: Level " << static_cast<uint16_t>(m);

    return oss.str();
}

}
