// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/236/DPT_236.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 236.001 priority control
 *
 * @ingroup KNX_03_07_02_03_49_01
 */
class KNX_EXPORT DPT_236_001 : public DPT_236
{
public:
    explicit DPT_236_001();

    std::string text() const override;
};

using DPT_Prioritised_Mode_Control = DPT_236_001;

}
