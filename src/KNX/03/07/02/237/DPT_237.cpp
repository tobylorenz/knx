// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/237/DPT_237.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_237::DPT_237(const uint16_t subnumber) :
    Datapoint_Type(237, subnumber)
{
}

void DPT_237::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    ce = (*first >> 2) & 0x01;
    bf = (*first >> 1) & 0x01;
    lf = *first & 0x01;
    ++first;

    rr = (*first >> 7) & 0x01;
    ai = (*first >> 6) & 0x01;
    addr = *first & 0x3f;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_237::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((ce << 2) | (bf << 1) | (lf << 0));
    data.push_back((rr << 7) | (ai << 6) | addr);

    return data;
}

}
