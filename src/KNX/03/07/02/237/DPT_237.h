// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 237.* configuration/ diagnostics
 *
 * Datapoint Types "B10U6"
 *
 * @ingroup KNX_03_07_02_06_07
 */
class KNX_EXPORT DPT_237 : public Datapoint_Type
{
public:
    explicit DPT_237(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** CE */
    bool ce{false};

    /** BF */
    bool bf{false};

    /** LF */
    bool lf{false};

    /** RR */
    bool rr{false};

    /** AI */
    bool ai{false};

    /** Addr */
    uint6_t addr{};
};

}
