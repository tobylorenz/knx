// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/237/DPT_237_600.h>

#include <sstream>

namespace KNX {

DPT_237_600::DPT_237_600() :
    DPT_237(600)
{
}

std::string DPT_237_600::text() const
{
    std::ostringstream oss;

    oss << "DALI Device/Group Address: " << static_cast<uint16_t>(addr)
        << ", Address Indicator: " << (ai ? "Group Address" : "Device Address")
        << ", Read or Response: " << (rr ? "Read" : "Response or spontaneous sending")
        << ", Lamp Failure: " << (lf ? "error" : "no error")
        << ", Ballast Failure: " << (bf ? "error" : "no error")
        << ", Convertor Error: " << (ce ? "error" : "no error");

    return oss.str();
}

}
