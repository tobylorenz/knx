// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/237/DPT_237.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 237.600 diagnostic value
 *
 * @ingroup KNX_03_07_02_06_07_01
 */
class KNX_EXPORT DPT_237_600 : public DPT_237
{
public:
    explicit DPT_237_600();

    std::string text() const override;
};

using DPT_DALI_Control_Gear_Diagnostic = DPT_237_600;

}
