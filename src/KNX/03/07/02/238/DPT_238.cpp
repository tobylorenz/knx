// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/238/DPT_238.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_238::DPT_238(const uint16_t subnumber) :
    Datapoint_Type(238, subnumber)
{
}

void DPT_238::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    b6 = (*first >> 7) & 0x01;
    b5 = (*first >> 6) & 0x01;
    value = *first & 0x3f;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_238::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((b6 << 7) | (b5 << 6) | value);

    return data;
}

}
