// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 238.* configuration/ diagnostics
 *
 * Datapoint Types "B2U6"
 *
 * @ingroup KNX_03_07_02_03_50
 * @ingroup KNX_03_07_02_06_08
 */
class KNX_EXPORT DPT_238 : public Datapoint_Type
{
public:
    explicit DPT_238(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** b6 */
    bool b6{false};

    /** b5 */
    bool b5{false};

    /** value */
    uint6_t value{};
};

}
