// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/238/DPT_238_001.h>

#include <sstream>

namespace KNX {

DPT_238_001::DPT_238_001() :
    DPT_238(1)
{
}

std::string DPT_238_001::text() const
{
    std::ostringstream oss;

    oss << "Scene Number: " << static_cast<uint16_t>(value)
        << "Scene Activation: " << (b5 ? "inactive" : "active")
        << "Storage function: " << (b6 ? "disable" : "enable");

    return oss.str();
}

}
