// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/238/DPT_238.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 238.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_50_01
 */
class KNX_EXPORT DPT_238_001 : public DPT_238
{
public:
    explicit DPT_238_001();

    std::string text() const override;
};

using DPT_SceneConfig = DPT_238_001;

}
