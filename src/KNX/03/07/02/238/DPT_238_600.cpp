// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/238/DPT_238_600.h>

#include <sstream>

namespace KNX {

DPT_238_600::DPT_238_600() :
    DPT_238(600)
{
}

std::string DPT_238_600::text() const
{
    std::ostringstream oss;

    oss << "Device Address: " << static_cast<uint16_t>(value)
        << "Lamp Failure: " << (b5 ? "error" : "no error")
        << "Ballast Failure: " << (b6 ? "error" : "no error");

    return oss.str();
}

}
