// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/238/DPT_238.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 238.600 diagnostic value
 *
 * @ingroup KNX_03_07_02_06_08_01
 */
class KNX_EXPORT DPT_238_600 : public DPT_238
{
public:
    explicit DPT_238_600();

    std::string text() const override;
};

using DPT_DALI_Diagnostics = DPT_238_600;

}
