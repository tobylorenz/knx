// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/239/DPT_239.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_239::DPT_239(const uint16_t subnumber) :
    Datapoint_Type(239, subnumber)
{
}

void DPT_239::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    setvalue = *first++;
    ca = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_239::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(setvalue);
    data.push_back(ca);

    return data;
}

}
