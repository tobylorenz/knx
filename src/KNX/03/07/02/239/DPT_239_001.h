// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/239/DPT_239.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 239.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_51_01
 */
class KNX_EXPORT DPT_239_001 : public DPT_239
{
public:
    explicit DPT_239_001();

    std::string text() const override;
};

using DPT_FlaggedScaling = DPT_239_001;

}
