// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/24/DPT_24.h>

namespace KNX {

DPT_24::DPT_24(const uint16_t subnumber) :
    Datapoint_Type(24, subnumber)
{
}

void DPT_24::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    str.assign(first, last);
}

std::vector<uint8_t> DPT_24::toData() const
{
    std::vector<uint8_t> data;

    data.assign(std::cbegin(str), std::cend(str));

    return data;
}

}
