// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/24/DPT_24_001.h>

namespace KNX {

DPT_24_001::DPT_24_001() :
    DPT_24(1)
{
}

std::string DPT_24_001::text() const
{
    return str;
}

}
