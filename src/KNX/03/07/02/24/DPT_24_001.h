// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/24/DPT_24.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 24.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_24
 */
class KNX_EXPORT DPT_24_001 : public DPT_24
{
public:
    explicit DPT_24_001();

    std::string text() const override;
};

using DPT_VarString_8859_1 = DPT_24_001;

}
