// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/240/DPT_240.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_240::DPT_240(const uint16_t subnumber) :
    Datapoint_Type(240, subnumber)
{
}

void DPT_240::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    height_position = *first++;
    slats_position = *first++;
    attributes = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_240::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(height_position);
    data.push_back(slats_position);
    data.push_back(static_cast<uint8_t>(attributes.to_ulong()));

    return data;
}

}
