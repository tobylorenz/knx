// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/240/DPT_240_800.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_240_800::DPT_240_800() :
    DPT_240(800)
{
}

std::string DPT_240_800::text() const
{
    std::ostringstream oss;

    oss << "Height Position: " << std::fixed << std::setprecision(1) << height_position * 100.0 / 255.0 << " %"
        << ", Slats Position: " << std::fixed << std::setprecision(1) << slats_position * 100.0 / 255.0 << " %"
        << ", Validity Height Position: " << (attributes[0] ? "true" : "false")
        << ", Validity Slats Position: " << (attributes[1] ? "true" : "false");

    return oss.str();
}

}
