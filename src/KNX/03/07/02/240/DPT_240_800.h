// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/240/DPT_240.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 240.800 combined position
 *
 * @ingroup KNX_03_07_02_07_02_01
 */
class KNX_EXPORT DPT_240_800 : public DPT_240
{
public:
    explicit DPT_240_800();

    std::string text() const override;
};

using DPT_CombinedPosition = DPT_240_800;

}
