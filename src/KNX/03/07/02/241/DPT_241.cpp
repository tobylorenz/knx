// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/241/DPT_241.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_241::DPT_241(const uint16_t subnumber) :
    Datapoint_Type(241, subnumber)
{
}

void DPT_241::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    height_position = *first++;
    slats_position = *first++;
    attributes = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> DPT_241::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(height_position);
    data.push_back(slats_position);
    const uint16_t attributes_data = static_cast<uint16_t>(attributes.to_ulong());
    data.push_back(attributes_data >> 8);
    data.push_back(attributes_data & 0xff);

    return data;
}

}
