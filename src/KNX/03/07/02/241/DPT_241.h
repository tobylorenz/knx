// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 241.* status
 *
 * Datapoint Types "U8U8B16"
 *
 * @ingroup KNX_03_07_02_07_03
 */
class KNX_EXPORT DPT_241 : public Datapoint_Type
{
public:
    explicit DPT_241(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** HeightPosition */
    uint8_t height_position{};

    /** SlatsPosition */
    uint8_t slats_position{};

    /** Attributes */
    std::bitset<16> attributes{};
};

}
