// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/241/DPT_241_800.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_241_800::DPT_241_800() :
    DPT_241(800)
{
}

std::string DPT_241_800::text() const
{
    std::ostringstream oss;

    oss << "Height Position: " << std::fixed << std::setprecision(1) << height_position * 100.0 / 255.0 << " %"
        << ", Slats Position: " << std::fixed << std::setprecision(1) << slats_position * 100.0 / 255.0 << " %"
        << ", Upper end position reached: " << (attributes[0] ? "true" : "false")
        << ", Lower end position reached: " << (attributes[1] ? "true" : "false")
        << ", Lower predefined position reached: " << (attributes[2] ? "true" : "false")
        << ", Target position is reached or drive is moving: " << (attributes[3] ? "target position is reached" : "drive is moving")
        << ", Restriction of target height position: " << (attributes[4] ? "true" : "false")
        << ", Restriction of target slats position: " << (attributes[5] ? "true" : "false")
        << ", At least one of the inputs Wind-/Rain-/Frost-Alarm is 'in alarm': " << (attributes[6] ? "true" : "false")
        << ", Up/down position is forced by MoveUpDownForced input: " << (attributes[7] ? "true" : "false")
        << ", Movement is locked: " << (attributes[8] ? "true" : "false")
        << ", Actuator setvalue is locally overriden: " << (attributes[9] ? "true" : "false")
        << ", General failure of the actuator or the drive: " << (attributes[10] ? "true" : "false")
        << ", Validity of field HeightPosition: " << (attributes[14] ? "true" : "false")
        << ", Validity of field SlatsPosition: " << (attributes[15] ? "true" : "false");

    return oss.str();
}

}
