// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/241/DPT_241.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 241.800 status sunblind & shutter actuator
 *
 * @ingroup KNX_03_07_02_07_03_04
 */
class KNX_EXPORT DPT_241_800 : public DPT_241
{
public:
    explicit DPT_241_800();

    std::string text() const override;
};

using DPT_StatusSAB = DPT_241_800;

}
