// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 242.* status
 *
 * Datapoint Types "U16U16U8r6B2"
 *
 * @ingroup KNX_03_07_02_06_09
 */
class KNX_EXPORT DPT_242 : public Datapoint_Type
{
public:
    explicit DPT_242(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** x-axis */
    uint16_t x_axis{};

    /** y-axis */
    uint16_t y_axis{};

    /** brightness */
    uint8_t brightness{};

    /** C */
    bool c{false};

    /** B */
    bool b{false};
};

}
