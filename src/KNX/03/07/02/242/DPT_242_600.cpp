// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/242/DPT_242_600.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_242_600::DPT_242_600() :
    DPT_242(600)
{
}

std::string DPT_242_600::text() const
{
    std::ostringstream oss;

    oss << "x-axis: " << x_axis
        << ", y-axis: " << y_axis
        << ", brightness: " << std::fixed << std::setprecision(1) << brightness * 100.0 / 255.0 << " %"
        << ", Validity xy: " << (c ? "valid" : "invalid")
        << ", Validity brightness: " << (b ? "valid" : "invalid");
    return oss.str();
}

}
