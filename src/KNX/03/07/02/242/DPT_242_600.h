// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/242/DPT_242.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 242.600 colour xyY
 *
 * @ingroup KNX_03_07_02_06_09
 */
class KNX_EXPORT DPT_242_600 : public DPT_242
{
public:
    explicit DPT_242_600();

    std::string text() const override;
};

using DPT_Colour_xyY = DPT_242_600;

}
