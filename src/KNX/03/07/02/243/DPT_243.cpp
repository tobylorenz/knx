// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/243/DPT_243.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_243::DPT_243(const uint16_t subnumber) :
    Datapoint_Type(243, subnumber)
{
}

void DPT_243::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 8) {
        throw DataDoesntMatchDPTException(8, data_size);
    }

    time_period = (*first++ << 8) | (*first++);
    y_axis = (*first++ << 8) | (*first++);
    x_axis = (*first++ << 8) | (*first++);
    brightness = *first++;

    c = (*first >> 1) & 0x01;
    b = *first & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_243::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(time_period >> 8);
    data.push_back(time_period & 0xff);

    data.push_back(y_axis >> 8);
    data.push_back(y_axis & 0xff);

    data.push_back(x_axis >> 8);
    data.push_back(x_axis & 0xff);

    data.push_back(brightness);

    data.push_back((c << 1) | b);

    return data;
}

}
