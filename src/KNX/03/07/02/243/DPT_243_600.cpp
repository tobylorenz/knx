// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/243/DPT_243_600.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_243_600::DPT_243_600() :
    DPT_243(600)
{
}

std::string DPT_243_600::text() const
{
    std::ostringstream oss;
    oss << "Time Period: " << time_period << " ms"
        << ", x-axis: " << x_axis
        << ", y-axis: " << y_axis
        << ", Brightness: " << std::fixed << std::setprecision(1) << brightness * 100.0 / 255.0 << " %"
        << ", Colour valid: " << (c ? "valid" : "invalid")
        << ", Brightness valid: " << (b ? "valid" : "invalid");
    return oss.str();
}

}
