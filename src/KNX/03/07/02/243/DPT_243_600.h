// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/243/DPT_243.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 243.600 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_06_10
 */
class KNX_EXPORT DPT_243_600 : public DPT_243
{
public:
    explicit DPT_243_600();

    std::string text() const override;
};

using DPT_Colour_Transition_xyY = DPT_243_600;

}
