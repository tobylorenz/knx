# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sources/headers
target_sources(KNX
    INTERFACE
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_244.h
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_244_600.h
    PRIVATE
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_244.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_244_600.cpp)

# install
install(
    FILES
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_244.h
        ${CMAKE_CURRENT_SOURCE_DIR}/DPT_244_600.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/KNX/03/07/02/244)
