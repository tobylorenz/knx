// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/244/DPT_244.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_244::DPT_244(const uint16_t subnumber) :
    Datapoint_Type(244, subnumber)
{
}

void DPT_244::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    cm = *first >> 4;
    hs = *first & 0x0f;
    ++first;

    fp = (*first >> 6) & 0x03;
    dp = (*first >> 4) & 0x03;
    pp = (*first >> 2) & 0x03;
    cf = *first  & 0x03;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_244::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((cm << 4) | static_cast<uint8_t>(hs.to_ulong()));
    data.push_back((fp << 6) | (dp << 4) | (pp << 2) | cf);

    return data;
}

}
