// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 244.* Converter Status
 *
 * Datapoint Types "N4B4N2N2N2N2"
 *
 * @ingroup KNX_03_07_02_06_11
 */
class KNX_EXPORT DPT_244 : public Datapoint_Type
{
public:
    explicit DPT_244(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** CM */
    uint4_t cm{};

    /** HS */
    std::bitset<4> hs;

    /** FP */
    uint2_t fp{};

    /** DP */
    uint2_t dp{};

    /** PP */
    uint2_t pp{};

    /** CF */
    uint2_t cf{};
};

}
