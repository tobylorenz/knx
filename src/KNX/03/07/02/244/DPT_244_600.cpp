// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/244/DPT_244_600.h>

#include <sstream>

namespace KNX {

DPT_244_600::DPT_244_600() :
    DPT_244(600)
{
}

std::string DPT_244_600::text() const
{
    std::ostringstream oss;

    oss << "Converter Mode: ";
    switch (cm) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "Normal mode active, all OK";
        break;
    case 2:
        oss << "Inhibit mode active";
        break;
    case 3:
        oss << "Hardwired inhibit mode active";
        break;
    case 4:
        oss << "Rest mode active";
        break;
    case 5:
        oss << "Emergency mode active";
        break;
    case 6:
        oss << "Extended emergency mode active";
        break;
    case 7:
        oss << "FT in progress";
        break;
    case 8:
        oss << "DT in progress";
        break;
    case 9:
        oss << "PDT in progress";
        break;
    default:
        oss << "Reserved";
        break;
    }

    oss << ", Hardware Status: "
        << (hs[0] ? "Hardware inhibit is active" : "Hardware inhibit is inactive")
        << ",."
        << (hs[1] ? "Hardware switch is on" : "Hardware switch is off");

    oss << ", Function Test Pending: ";
    switch (fp) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "No test pending";
        break;
    case 2:
        oss << "Test pending";
        break;
    case 3:
        oss << "Reserved";
        break;
    }

    oss << ", Duration Test Pending: ";
    switch (dp) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "No test pending";
        break;
    case 2:
        oss << "Test pending";
        break;
    case 3:
        oss << "Reserved";
        break;
    }

    oss << ", Partial Duration Test Pending: ";
    switch (pp) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "No test pending";
        break;
    case 2:
        oss << "Test pending";
        break;
    case 3:
        oss << "Reserved";
        break;
    }

    oss << ", Converter Test Pending: ";
    switch (cf) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "No test pending";
        break;
    case 2:
        oss << "Test pending";
        break;
    case 3:
        oss << "Reserved";
        break;
    }

    return oss.str();
}

}
