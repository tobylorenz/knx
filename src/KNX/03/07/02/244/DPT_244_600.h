// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/244/DPT_244.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 244.600 DALI converter status
 *
 * @ingroup KNX_03_07_02_06_11
 */
class KNX_EXPORT DPT_244_600 : public DPT_244
{
public:
    explicit DPT_244_600();

    std::string text() const override;
};

using DPT_Converter_Status = DPT_244_600;

}
