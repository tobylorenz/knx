// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/245/DPT_245.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_245::DPT_245(const uint16_t subnumber) :
    Datapoint_Type(245, subnumber)
{
}

void DPT_245::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    ltrf = *first >> 4;
    ltrd = *first & 0x0f;
    ++first;

    ltrp = *first >> 4;
    ++first;

    sf = (*first >> 6) & 0x03;
    sd = (*first >> 4) & 0x03;
    sp = (*first >> 2) & 0x03;
    ++first;

    ldtr.unsigned_value = (*first++ << 8) | (*first++);

    lpdtr = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_245::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((ltrf << 4) | ltrd);
    data.push_back(ltrp << 4);
    data.push_back((sf << 6) | (sd << 4) | (sp << 2));
    data.push_back(ldtr.unsigned_value >> 8);
    data.push_back(ldtr.unsigned_value & 0xff);
    data.push_back(lpdtr);

    return data;
}

}
