// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/7/DPT_7_006.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 245.* Converter test result
 *
 * Datapoint Types "N4N4N4N2N2N2N2U16U8"
 *
 * @ingroup KNX_03_07_02_06_12
 */
class KNX_EXPORT DPT_245 : public Datapoint_Type
{
public:
    explicit DPT_245(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** LTRF */
    uint4_t ltrf{};

    /** LTRD */
    uint4_t ltrd{};

    /** LTRP */
    uint4_t ltrp{};

    /** SF */
    uint2_t sf{};

    /** SD */
    uint2_t sd{};

    /** SP */
    uint2_t sp{};

    /** LDTR */
    DPT_TimePeriodMin ldtr{};

    /** LPDTR */
    uint8_t lpdtr{};
};

}
