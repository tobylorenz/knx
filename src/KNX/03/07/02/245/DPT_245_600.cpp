// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/245/DPT_245_600.h>

#include <sstream>

namespace KNX {

DPT_245_600::DPT_245_600() :
    DPT_245(600)
{
}

std::string DPT_245_600::text() const
{
    std::ostringstream oss;

    oss << "Latest Test Result FT: ";
    switch (ltrf) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "Passed in time";
        break;
    case 2:
        oss << "Passed max delay exceeded";
        break;
    case 3:
        oss << "Failed, test executed in time";
        break;
    case 4:
        oss << "failed, max delay exceeded";
        break;
    case 5:
        oss << "Test manually stopped";
        break;
    default:
        oss << "Reserved, do not use";
        break;
    }

    oss << ", Last Test Result DT: ";
    switch (ltrd) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "Passed in time";
        break;
    case 2:
        oss << "Passed max delay exceeded";
        break;
    case 3:
        oss << "Failed, test executed in time";
        break;
    case 4:
        oss << "failed, max delay exceeded";
        break;
    case 5:
        oss << "Test manually stopped";
        break;
    default:
        oss << "Reserved, do not use";
        break;
    }

    oss << ", Last Test Result PDT: ";
    switch (ltrp) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "Passed in time";
        break;
    case 2:
        oss << "Passed max delay exceeded";
        break;
    case 3:
        oss << "Failed, test executed in time";
        break;
    case 4:
        oss << "failed, max delay exceeded";
        break;
    case 5:
        oss << "Test manually stopped";
        break;
    default:
        oss << "Reserved, do not use";
        break;
    }

    oss << ", Start Method of Last FT: ";
    switch (sf) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "Started automatically";
        break;
    case 2:
        oss << "Started by Gateway";
        break;
    case 3:
        oss << "Reserved";
    }

    oss << ", Start Method of Last DT: ";
    switch (sd) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "Started automatically";
        break;
    case 2:
        oss << "Started by Gateway";
        break;
    case 3:
        oss << "Reserved";
    }

    oss << ", Start Method of Last PDT: ";
    switch (sp) {
    case 0:
        oss << "Unknown";
        break;
    case 1:
        oss << "Started automatically";
        break;
    case 2:
        oss << "Started by Gateway";
        break;
    case 3:
        oss << "Reserved";
    }

    oss << ", Last Duration Test Result: ";
    if (ldtr.unsigned_value >= 510) {
        oss << "510 min or longer";
    } else {
        oss << ldtr.text();
    }

    oss << ", Last PDT Result: ";
    switch (lpdtr) {
    case 0:
        oss << " deep discharge point";
        break;
    case 254:
        oss << "fully charged";
        break;
    case 255:
        oss << "unknown";
        break;
    default:
        oss << lpdtr << " / 254";
        break;
    }

    return oss.str();
}

}
