// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/245/DPT_245.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 245.600 DALI converter test result
 *
 * @ingroup KNX_03_07_02_06_12
 */
class KNX_EXPORT DPT_245_600 : public DPT_245
{
public:
    explicit DPT_245_600();

    std::string text() const override;
};

using DPT_Converter_Test_Result = DPT_245_600;

}
