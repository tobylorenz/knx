// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/246/DPT_246.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_246::DPT_246(const uint16_t subnumber) :
    Datapoint_Type(246, subnumber)
{
}

void DPT_246::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    bs = *first++;
    bcl = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_246::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(static_cast<uint8_t>(bs.to_ulong()));
    data.push_back(bcl);

    return data;
}

}
