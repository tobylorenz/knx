// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 246.* Battery Information
 *
 * Datapoint Types "r5B3U8"
 *
 * @ingroup KNX_03_07_02_06_13
 */
class KNX_EXPORT DPT_246 : public Datapoint_Type
{
public:
    explicit DPT_246(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** BS */
    std::bitset<3> bs;

    /** BCL */
    uint8_t bcl{};
};

}
