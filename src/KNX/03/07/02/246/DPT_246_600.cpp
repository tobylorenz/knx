// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/246/DPT_246_600.h>

#include <sstream>

namespace KNX {

DPT_246_600::DPT_246_600() :
    DPT_246(600)
{
}

std::string DPT_246_600::text() const
{
    std::ostringstream oss;

    oss << "Battery Status: "
        << "Battery Failure: " << (bs[0] ? "true" : "failure")
        << ", Battery Duration Failure: " << (bs[1] ? "true" : "failure")
        << ", Battery Fully Charged: " << (bs[2] ? "true" : "failure");

    oss << ", Battery Charge Level: ";
    switch (bcl) {
    case 0:
        oss << "deep discharge point";
        break;
    case 254:
        oss << "fully charged";
        break;
    case 255:
        oss << "unknown or not supported";
        break;
    default:
        oss << static_cast<uint16_t>(bcl) <<  " / 254";
        break;
    }

    return oss.str();
}

}
