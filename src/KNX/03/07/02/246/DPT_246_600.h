// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/246/DPT_246.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 246.600 Battery Information
 *
 * @ingroup KNX_03_07_02_06_13
 */
class KNX_EXPORT DPT_246_600 : public DPT_246
{
public:
    explicit DPT_246_600();

    std::string text() const override;
};

using DPT_Battery_Info = DPT_246_600;

}
