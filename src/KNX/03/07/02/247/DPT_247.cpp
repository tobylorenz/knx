// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/247/DPT_247.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_247::DPT_247(const uint16_t subnumber) :
    Datapoint_Type(247, subnumber)
{
}

void DPT_247::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 12) {
        throw DataDoesntMatchDPTException(12, data_size);
    }

    date_time.fromData(first, first + 8);
    first += 8;

    time_period_1.unsigned_value = (*first++ << 8) | *first++;

    time_period_2.unsigned_value = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_247::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> date_time_data = date_time.toData();
    data.insert(std::cend(data), std::cbegin(date_time_data), std::cend(date_time_data));

    data.push_back(time_period_1.unsigned_value >> 8);
    data.push_back(time_period_1.unsigned_value & 0xff);

    data.push_back(time_period_2.unsigned_value >> 8);
    data.push_back(time_period_2.unsigned_value & 0xff);

    return data;
}

}
