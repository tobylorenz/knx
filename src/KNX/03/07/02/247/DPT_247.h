// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/19/DPT_19_001.h>
#include <KNX/03/07/02/7/DPT_7_005.h>
#include <KNX/03/07/02/7/DPT_7_007.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 247.* @note Not in MasterData yet
 *
 * Datapoint Types "U8[r4U4][r3U5][U3U5][r2U6][r2U6]B8[B1r7]U16U16"
 *
 * @ingroup KNX_03_07_02_06_14
 */
class KNX_EXPORT DPT_247 : public Datapoint_Type
{
public:
    explicit DPT_247(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Date Time */
    DPT_DateTime date_time;

    /** Time Period 1 */
    DPT_TimePeriodHrs time_period_1{};

    /** Time Period 2 */
    DPT_TimePeriodSec time_period_2{};
};

}
