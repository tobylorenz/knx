// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/247/DPT_247_600.h>

#include <sstream>

namespace KNX {

DPT_247_600::DPT_247_600() :
    DPT_247(600)
{
}

std::string DPT_247_600::text() const
{
    std::ostringstream oss;

    oss << "Start Date and Time: "
        << date_time.text();

    oss << ", Time interval automated test: "
        << time_period_1.text();

    oss << ", Time period of the preceding test execution: "
        << time_period_2.text();

    return oss.str();
}

}
