// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/247/DPT_247.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 247.600 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_06_14
 */
class KNX_EXPORT DPT_247_600 : public DPT_247
{
public:
    explicit DPT_247_600();

    std::string text() const override;
};

using DPT_Converter_Test_Info = DPT_247_600;

}
