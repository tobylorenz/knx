// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/248/DPT_248.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_248::DPT_248(const uint16_t subnumber) :
    Datapoint_Type(248, subnumber)
{
}

void DPT_248::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    info_valid = *first++;
    em_min_level = *first++;
    em_max_level = *first++;
    rated_dur.unsigned_value = *first++;
    gear_feat = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_248::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(info_valid);
    data.push_back(em_min_level);
    data.push_back(em_max_level);
    data.push_back(rated_dur.unsigned_value);
    data.push_back(gear_feat.to_ulong());

    return data;
}

}
