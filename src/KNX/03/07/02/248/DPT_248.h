// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/7/DPT_7_006.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 248.* @note Not in MasterData yet
 *
 * Datapoint Types "N8U8U8U8B8"
 *
 * @ingroup KNX_03_07_02_06_15
 */
class KNX_EXPORT DPT_248 : public Datapoint_Type
{
public:
    explicit DPT_248(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** InfoValid */
    uint8_t info_valid{};

    /** EmMinLevel */
    uint8_t em_min_level{};

    /** EmMaxLevel */
    uint8_t em_max_level{};

    /** RatedDur */
    DPT_TimePeriodMin rated_dur{};

    /** GearFeat */
    std::bitset<8> gear_feat{};
};

}
