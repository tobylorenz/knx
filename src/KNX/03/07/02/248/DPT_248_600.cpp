// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/248/DPT_248_600.h>

#include <sstream>

namespace KNX {

DPT_248_600::DPT_248_600() :
    DPT_248(600)
{
}

std::string DPT_248_600::text() const
{
    std::ostringstream oss;

    oss << "Information Valid: ";
    switch (info_valid) {
    case 0:
        oss << "Converter is not existing or no information about the converter available";
        break;
    case 1:
        oss << "Converter information is valid";
        break;
    case 2:
        oss << "Converter information is invalid";
        break;
    default:
        oss << "Reserved. Shall be 0";
    }

    oss << ", Emergency Minimum Brightness Level: ";
    if (em_min_level == 255) {
        oss << "Unknown";
    } else {
        oss << static_cast<uint16_t>(em_min_level);
    }

    oss << ", Emergency Maximum Brightness Level: ";
    if (em_max_level == 255) {
        oss << "Unknown";
    } else {
        oss << static_cast<uint16_t>(em_max_level);
    }

    oss << ", Rated Duration: "
        << rated_dur.text();

    oss << ", Gear Features: "
        << (gear_feat[0] ? "integral emergency control gear" : "no integral emergency control gear")
        << ", "
        << (gear_feat[1] ? "maintained control gear" : "no maintained control gear")
        << ", "
        << (gear_feat[2] ? "switched maintained control gear" : "no switched maintained control gear")
        << ", "
        << (gear_feat[3] ? "auto test capability" : "no auto test capability")
        << ", "
        << (gear_feat[4] ? "adjustable emergency level" : "no adjustable emergency level")
        << ", "
        << (gear_feat[5] ? "hardwired inhibit supported" : "")
        << ", "
        << (gear_feat[6] ? "physical selection supported" : "no physical selection supported")
        << ", "
        << (gear_feat[7] ? "re-light in rest mode supported" : "no re-light in rest mode supported");

    return oss.str();
}

}
