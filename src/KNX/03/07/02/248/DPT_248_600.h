// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/248/DPT_248.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 248.600 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_06_15
 */
class KNX_EXPORT DPT_248_600 : public DPT_248
{
public:
    explicit DPT_248_600();

    std::string text() const override;
};

using DPT_Converter_Info_Fix = DPT_248_600;

}
