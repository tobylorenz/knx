// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/249/DPT_249.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_249::DPT_249(const uint16_t subnumber) :
    Datapoint_Type(249, subnumber)
{
}

void DPT_249::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    time_period.unsigned_value = (*first++ << 8) | (*first++);
    absolute_colour_temperature = (*first++ << 8) | (*first++);
    absolute_brightness.unsigned_value = *first++;
    masks = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_249::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(time_period.unsigned_value >> 8);
    data.push_back(time_period.unsigned_value & 0xff);
    data.push_back(absolute_colour_temperature >> 8);
    data.push_back(absolute_colour_temperature & 0xff);
    data.push_back(absolute_brightness.unsigned_value);
    data.push_back(static_cast<uint8_t>(masks.to_ulong()));

    return data;
}

}
