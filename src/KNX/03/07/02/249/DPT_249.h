// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/5/DPT_5_001.h>
#include <KNX/03/07/02/7/DPT_7_004.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 249.* brightness colour temperature transition
 *
 * Datapoint Types "U16U16U8B8"
 *
 * @ingroup KNX_03_07_02_06_16
 */
class KNX_EXPORT DPT_249 : public Datapoint_Type
{
public:
    explicit DPT_249(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Time Period */
    DPT_TimePeriod100MSec time_period{};

    /** Absolute Colour Temperature */
    uint16_t absolute_colour_temperature{};

    /** Absolute Brightness */
    DPT_Scaling absolute_brightness{};

    /** Masks */
    std::bitset<8> masks{};
};

}
