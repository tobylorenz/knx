// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/249/DPT_249_600.h>

#include <sstream>

namespace KNX {

DPT_249_600::DPT_249_600() :
    DPT_249(600)
{
}

std::string DPT_249_600::text() const
{
    std::ostringstream oss;

    oss << "Time Period: "
        << time_period.text();

    oss << ", Absolute Colour Temperature: "
        << absolute_colour_temperature << " K";

    oss << ", Absolute Brightness: "
        << absolute_brightness.text();

    oss << ", Masks: "
        << "validity of the Time Period: " << (masks[2] ? "valid" : "invalid")
        << ", validity of the Absolute Colour Temperature: " << (masks[1] ? "valid" : "invalid")
        << ", validity of the absolute brightness: " << (masks[0] ? "valid" : "invalid");

    return oss.str();
}

}
