// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/249/DPT_249.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 249.600 brightness colour temperature transition
 *
 * @ingroup KNX_03_07_02_06_16
 */
class KNX_EXPORT DPT_249_600 : public DPT_249
{
public:
    explicit DPT_249_600();

    std::string text() const override;
};

using DPT_Brightness_Colour_Temperature_Transition = DPT_249_600;

}
