// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/25/DPT_25.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_25::DPT_25(const uint16_t subnumber) :
    Datapoint_Type(25, subnumber)
{
}

void DPT_25::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    busy = *first >> 4;
    nak = *first++ & 0x0f;

    assert(first == last);
}

std::vector<uint8_t> DPT_25::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((busy << 4) | (nak & 0x0f));

    return data;
}

}
