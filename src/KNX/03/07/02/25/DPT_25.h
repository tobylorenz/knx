// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 25.* 2-nibble set
 *
 * Datapoint Types "U4U4"
 *
 * @ingroup KNX_03_07_02_08_04
 */
class KNX_EXPORT DPT_25 : public Datapoint_Type
{
public:
    explicit DPT_25(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Busy */
    uint4_t busy{};

    /** Nak */
    uint4_t nak{};
};

}
