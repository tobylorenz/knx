// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/25/DPT_25_1000.h>

#include <sstream>

namespace KNX {

DPT_25_1000::DPT_25_1000() :
    DPT_25(1000)
{
}

std::string DPT_25_1000::text() const
{
    std::ostringstream oss;

    oss << "Busy: "
        << static_cast<uint16_t>(busy)
        << ", Nak: "
        << static_cast<uint16_t>(nak);

    return oss.str();
}

}
