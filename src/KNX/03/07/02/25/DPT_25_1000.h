// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/25/DPT_25.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 25.1000 busy/nak repetitions
 *
 * @ingroup KNX_03_07_02_08_04
 */
class KNX_EXPORT DPT_25_1000 : public DPT_25
{
public:
    explicit DPT_25_1000();

    std::string text() const override;
};

using DPT_DoubleNibble = DPT_25_1000;

}
