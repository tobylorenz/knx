// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/250/DPT_250.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_250::DPT_250(const uint16_t subnumber) :
    Datapoint_Type(250, subnumber)
{
}

void DPT_250::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    cct = (*first >> 3) & 0x01;
    step_code_colour_temp = *first & 0x07;
    ++first;

    cb = (*first >> 3) & 0x01;
    step_code_brightness = *first & 0x07;
    ++first;

    masks = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_250::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((cct << 3) | step_code_colour_temp);
    data.push_back((cb << 3) | step_code_brightness);
    data.push_back(static_cast<uint8_t>(masks.to_ulong()));

    return data;
}

}
