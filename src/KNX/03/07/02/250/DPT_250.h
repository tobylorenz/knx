// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 250.* status
 *
 * Datapoint Types "r4B1U3r4B1U3B8"
 *
 * @ingroup KNX_03_07_02_06_17
 */
class KNX_EXPORT DPT_250 : public Datapoint_Type
{
public:
    explicit DPT_250(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** CCT */
    bool cct{false};

    /** Step Code Colour Temp */
    uint3_t step_code_colour_temp{};

    /** CB */
    bool cb{false};

    /** Step Code Brightness */
    uint3_t step_code_brightness{};

    /** Masks */
    std::bitset<8> masks{};
};

}
