// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/250/DPT_250_600.h>

#include <sstream>

namespace KNX {

DPT_250_600::DPT_250_600() :
    DPT_250(600)
{
}

std::string DPT_250_600::text() const
{
    std::ostringstream oss;

    oss << "CCT: "
        << (cct ? "increase" : "decrease");

    oss << ", Step Code Colour Temperature: ";
    if (step_code_colour_temp == 0) {
        oss << "Break";
    } else {
        oss << static_cast<uint16_t>(step_code_colour_temp);
    }

    oss << ", CB: "
        << (cb ? "increase" : "decrease");

    oss << ", Step Code Brightness: ";
    if (step_code_brightness == 0) {
        oss << "Break";
    } else {
        oss << static_cast<uint16_t>(step_code_brightness);
    }

    oss << ", Masks: "
        << "validity of the fields CCT and Step Code Colour Temperature: " << (masks[1] ? "valid" : "invalid")
        << ", validity of the fields CB and Step Code Brightness: " << (masks[0] ? "valid" : "invalid");

    return oss.str();
}

}
