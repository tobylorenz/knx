// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/250/DPT_250.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 250.600 brightness colour temperature control
 *
 * @ingroup KNX_03_07_02_06_17
 */
class KNX_EXPORT DPT_250_600 : public DPT_250
{
public:
    explicit DPT_250_600();

    std::string text() const override;
};

using DPT_Brightness_Colour_Temperature_Control = DPT_250_600;

}
