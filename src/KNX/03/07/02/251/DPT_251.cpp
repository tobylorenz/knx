// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/251/DPT_251.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_251::DPT_251(const uint16_t subnumber) :
    Datapoint_Type(251, subnumber)
{
}

void DPT_251::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    r = *first++;
    g = *first++;
    b = *first++;
    w = *first++;
    ++first; // reserved
    masks = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_251::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(r);
    data.push_back(g);
    data.push_back(b);
    data.push_back(w);
    data.push_back(0); // reserved
    data.push_back(static_cast<uint8_t>(masks.to_ulong()));

    return data;
}

}
