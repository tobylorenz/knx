// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 251.* Colour RGBW
 *
 * Datapoint Types "U8U8U8U8r8r4B4"
 *
 * @ingroup KNX_03_07_02_06_18
 */
class KNX_EXPORT DPT_251 : public Datapoint_Type
{
public:
    explicit DPT_251(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** R */
    uint8_t r{};

    /** G */
    uint8_t g{};

    /** B */
    uint8_t b{};

    /** W */
    uint8_t w{};

    /** mR/mG/mR/mW */
    std::bitset<4> masks{};
};

}
