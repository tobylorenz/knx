// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/251/DPT_251_600.h>

#include <sstream>

namespace KNX {

DPT_251_600::DPT_251_600() :
    DPT_251(600)
{
}

std::string DPT_251_600::text() const
{
    std::ostringstream oss;

    oss << "Colour Level Red: "
        << static_cast<uint16_t>(r);

    oss << ", Colour Level Green: "
        << static_cast<uint16_t>(g);

    oss << ", Colour Level Blue: "
        << static_cast<uint16_t>(b);

    oss << ", Colour Level White: "
        << static_cast<uint16_t>(w);

    oss << ", Masks: "
        << " validity of the field R: " << (masks[3] ? "valid" : "not valid")
        << ", validity of the field G: " << (masks[2] ? "valid" : "not valid")
        << ", validity of the field B: " << (masks[1] ? "valid" : "not valid")
        << ", validity of the field W: " << (masks[0] ? "valid" : "not valid");

    return oss.str();
}

}
