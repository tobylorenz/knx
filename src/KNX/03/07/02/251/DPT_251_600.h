// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/251/DPT_251.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 251.600 RGBW value 4x(0..100%)
 *
 * @ingroup KNX_03_07_02_06_18
 */
class KNX_EXPORT DPT_251_600 : public DPT_251
{
public:
    explicit DPT_251_600();

    std::string text() const override;
};

using DPT_Colour_RGBW = DPT_251_600;

}
