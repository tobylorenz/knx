// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/252/DPT_252.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_252::DPT_252(const uint16_t subnumber) :
    Datapoint_Type(252, subnumber)
{
}

void DPT_252::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 5) {
        throw DataDoesntMatchDPTException(5, data_size);
    }

    cr = (*first >> 3) & 0x01;
    step_code_colour_red = *first & 0x07;
    ++first;

    cg = (*first >> 3) & 0x01;
    step_code_colour_green = *first & 0x07;
    ++first;

    cb = (*first >> 3) & 0x01;
    step_code_colour_blue = *first & 0x07;
    ++first;

    cw = (*first >> 3) & 0x01;
    step_code_colour_white = *first & 0x07;
    ++first;

    masks = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_252::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((cr << 3) | step_code_colour_red);
    data.push_back((cg << 3) | step_code_colour_green);
    data.push_back((cb << 3) | step_code_colour_blue);
    data.push_back((cw << 3) | step_code_colour_white);
    data.push_back(static_cast<uint8_t>(masks.to_ulong()));

    return data;
}

}
