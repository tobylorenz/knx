// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 252.* Relative Control RGBW
 *
 * Datapoint Types "r4B1U3r4B1U3r4B1U3r4B1U3B8"
 *
 * @ingroup KNX_03_07_02_06_19
 */
class KNX_EXPORT DPT_252 : public Datapoint_Type
{
public:
    explicit DPT_252(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** CR */
    bool cr{false};

    /** Step Code Colour Red */
    uint3_t step_code_colour_red{};

    /** CG */
    bool cg{false};

    /** Step Code Colour Green */
    uint3_t step_code_colour_green{};

    /** CB */
    bool cb{false};

    /** Step Code Colour Green */
    uint3_t step_code_colour_blue{};

    /** CW */
    bool cw{false};

    /** Step Code Colour White */
    uint3_t step_code_colour_white{};

    /** Masks */
    std::bitset<8> masks{};
};

}
