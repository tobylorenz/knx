// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/252/DPT_252_600.h>

#include <sstream>

namespace KNX {

DPT_252_600::DPT_252_600() :
    DPT_252(600)
{
}

std::string DPT_252_600::text() const
{
    std::ostringstream oss;

    oss << "Colour Component Red: "
        << (cr ? "increase" : "decrease");

    oss << ", Step Code Colour Red: ";
    if (step_code_colour_red == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_colour_red);
    }

    oss << ", Colour Component Green: "
        << (cg ? "increase" : "decrease");

    oss << ", Step Code Colour Green: ";
    if (step_code_colour_green == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_colour_green);
    }

    oss << ", Colour Component Blue: "
        << (cb ? "increase" : "decrease");

    oss << ", Step Code Colour Blue: ";
    if (step_code_colour_blue == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_colour_blue);
    }

    oss << ", Colour Component White: "
        << (cw ? "increase" : "decrease");

    oss << ", Step Code Colour White: ";
    if (step_code_colour_white == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_colour_white);
    }

    oss << ", Masks: "
        << " validity of the fields CR and Step Code Colour Red: " << (masks[3] ? "valid" : "invalid")
        << ", validity of the fields CG and Step Code Colour Green: " << (masks[2] ? "valid" : "invalid")
        << ", validity of the fields CB and Step Code Colour Blue: " << (masks[1] ? "valid" : "invalid")
        << ", validity of the fields CW and Step Code Colour White: " << (masks[0] ? "valid" : "invalid");

    return oss.str();
}

}
