// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/252/DPT_252.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 252.600 RGBW relative control
 *
 * @ingroup KNX_03_07_02_06_19
 */
class KNX_EXPORT DPT_252_600 : public DPT_252
{
public:
    explicit DPT_252_600();

    std::string text() const override;
};

using DPT_Relative_Control_RGBW = DPT_252_600;

}
