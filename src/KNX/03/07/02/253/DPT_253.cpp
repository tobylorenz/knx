// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/253/DPT_253.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_253::DPT_253(const uint16_t subnumber) :
    Datapoint_Type(253, subnumber)
{
}

void DPT_253::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    cs = (*first >> 3) & 0x01;
    step_code_saturation = *first & 0x07;
    ++first;

    cc = (*first >> 3) & 0x01;
    step_code_colour = *first & 0x07;
    ++first;

    cb = (*first >> 3) & 0x01;
    step_code_brightness = *first & 0x07;
    ++first;

    masks = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_253::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((cs << 3) | step_code_saturation);
    data.push_back((cc << 3) | step_code_colour);
    data.push_back((cb << 3) | step_code_brightness);
    data.push_back(static_cast<uint8_t>(masks.to_ulong()));

    return data;
}

}
