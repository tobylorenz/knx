// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/253/DPT_253_600.h>

#include <sstream>

namespace KNX {

DPT_253_600::DPT_253_600() :
    DPT_253(600)
{
}

std::string DPT_253_600::text() const
{
    std::ostringstream oss;

    oss << "Colour Saturation: "
        << (cs ? "increase distance to white point" : "decrease distance to white point");

    oss << ", Step Code Colour Saturation: ";
    if (step_code_saturation == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_saturation);
    }

    oss << ", Colour: "
        << (cc ? "increase colour wave length" : "decrease colour wave length");

    oss << ", Step Code Colour: ";
    if (step_code_colour == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_colour);
    }

    oss << ", Brightness: "
        << (cb ? "increase brightness" : "decrease brightness");

    oss << ", Step Code Brightness: ";
    if (step_code_brightness == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_brightness);
    }

    oss << ", Masks: "
        << " validity of the fields CS and Step Code Saturation: " << (masks[2] ? "valid" : "invalid")
        << ", validity of the fields CC and Step Code Colour: " << (masks[1] ? "valid" : "invalid")
        << ", validity of the fields CB and Step Code Brightness: " << (masks[0] ? "valid" : "invalid");

    return oss.str();
}

}
