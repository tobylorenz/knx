// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/253/DPT_253.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 253.600 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_06_20
 */
class KNX_EXPORT DPT_253_600 : public DPT_253
{
public:
    explicit DPT_253_600();

    std::string text() const override;
};

using DPT_Relative_Control_xyY = DPT_253_600;

}
