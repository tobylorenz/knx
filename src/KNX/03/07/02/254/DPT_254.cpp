// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/254/DPT_254.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_254::DPT_254(const uint16_t subnumber) :
    Datapoint_Type(254, subnumber)
{
}

void DPT_254::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    cr = (*first >> 3) & 0x01;
    step_code_colour_red = *first & 0x07;
    ++first;

    cg = (*first >> 3) & 0x01;
    step_code_colour_green = *first & 0x07;
    ++first;

    cb = (*first >> 3) & 0x01;
    step_code_colour_blue = *first & 0x07;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_254::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((cr << 3) | step_code_colour_red);
    data.push_back((cg << 3) | step_code_colour_green);
    data.push_back((cb << 3) | step_code_colour_blue);

    return data;
}

}
