// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 254.* Relative Control RGB
 *
 * Datapoint Types "r4B1U3r4B1U3r4B1U3"
 *
 * @ingroup KNX_03_07_02_06_21
 */
class KNX_EXPORT DPT_254 : public Datapoint_Type
{
public:
    explicit DPT_254(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** CR */
    bool cr{false};

    /** Step Code Colour Red */
    uint3_t step_code_colour_red{};

    /** CG */
    bool cg{false};

    /** Step Code Colour Green */
    uint3_t step_code_colour_green{};

    /** CB */
    bool cb{false};

    /** Step Code Colour Green */
    uint3_t step_code_colour_blue{};
};

}
