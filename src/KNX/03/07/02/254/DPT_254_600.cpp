// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/254/DPT_254_600.h>

#include <sstream>

namespace KNX {

DPT_254_600::DPT_254_600() :
    DPT_254(600)
{
}

std::string DPT_254_600::text() const
{
    std::ostringstream oss;

    oss << "Colour Red: "
        << (cr ? "increase" : "decrease");

    oss << ", Step Code Colour Red: ";
    if (step_code_colour_red == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_colour_red);
    }

    oss << ", Colour Green: "
        << (cg ? "increase" : "decrease");

    oss << ", Step Code Colour Green: ";
    if (step_code_colour_green == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_colour_green);
    }

    oss << ", Colour Blue: "
        << (cb ? "increase" : "decrease");

    oss << ", Step Code Colour Blue: ";
    if (step_code_colour_blue == 0) {
        oss << "stop fading";
    } else {
        oss << static_cast<uint16_t>(step_code_colour_blue);
    }

    return oss.str();
}

}
