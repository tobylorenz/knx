// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/254/DPT_254.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 254.600 RGB relative control
 *
 * @ingroup KNX_03_07_02_06_21
 */
class KNX_EXPORT DPT_254_600 : public DPT_254
{
public:
    explicit DPT_254_600();

    std::string text() const override;
};

using DPT_Relative_Control_RGB = DPT_254_600;

}
