// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/255/DPT_255.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_255::DPT_255(const uint16_t subnumber) :
    Datapoint_Type(255, subnumber)
{
}

void DPT_255::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 8) {
        throw DataDoesntMatchDPTException(8, data_size);
    }

    union {
        float f;
        uint32_t u{};
    };

    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    longitude = f;

    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    latitude = f;

    assert(first == last);
}

std::vector<uint8_t> DPT_255::toData() const
{
    std::vector<uint8_t> data;

    union {
        float f;
        uint32_t u{};
    };

    f = longitude;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);

    f = latitude;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);

    return data;
}

}
