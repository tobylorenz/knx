// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/255/DPT_255_001.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_255_001::DPT_255_001() :
    DPT_255(1)
{
}

std::string DPT_255_001::text() const
{
    std::ostringstream oss;

    oss << "Latitude: " << std::fixed << std::setprecision(7) << longitude
        << ", Longitude: " << std::fixed << std::setprecision(7) << latitude;

    return oss.str();
}

}
