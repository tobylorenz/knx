// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/255/DPT_255.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 255.001 geographical location (longitude and latitude) expressed in degrees
 *
 * @ingroup KNX_03_07_02_03_52_01
 */
class KNX_EXPORT DPT_255_001 : public DPT_255
{
public:
    explicit DPT_255_001();

    std::string text() const override;
};

using DPT_GeographicalLocation = DPT_255_001;

}
