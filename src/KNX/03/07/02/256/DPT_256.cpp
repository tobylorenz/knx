// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/256/DPT_256.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_256::DPT_256(const uint16_t subnumber) :
    Datapoint_Type(256, subnumber)
{
}

void DPT_256::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 16) {
        throw DataDoesntMatchDPTException(16, data_size);
    }

    start_date_time.fromData(first, first + 8);
    first += 8;
    stop_date_time.fromData(first, first + 8);
    first += 8;

    assert(first == last);
}

std::vector<uint8_t> DPT_256::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> start_date_time_data = start_date_time.toData();
    data.insert(std::cend(data), std::cbegin(start_date_time_data), std::cend(start_date_time_data));
    const std::vector<uint8_t> stop_date_time_data = start_date_time.toData();
    data.insert(std::cend(data), std::cbegin(stop_date_time_data), std::cend(stop_date_time_data));

    return data;
}

}
