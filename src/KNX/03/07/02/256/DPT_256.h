// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/19/DPT_19_001.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 256.* @note Not in MasterData yet
 *
 * Datapoint Types "U8[r4U4][r3U5][U3U5][r2U6][r2U6]B16U8[r4U4][r3U5][U3U5][r2U6][r2U6]B16"
 *
 * @ingroup KNX_03_07_02_03_53
 */
class KNX_EXPORT DPT_256 : public Datapoint_Type
{
public:
    explicit DPT_256(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Start Date Time */
    DPT_DateTime start_date_time;

    /** Stop Date Time */
    DPT_DateTime stop_date_time;
};

}
