// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/256/DPT_256_001.h>

#include <sstream>

namespace KNX {

DPT_256_001::DPT_256_001() :
    DPT_256(1)
{
}

std::string DPT_256_001::text() const
{
    std::ostringstream oss;

    oss << "Start Date Time: "
        << start_date_time.text();

    oss << ", Stop Date Time: "
        << stop_date_time.text();

    return oss.str();
}

}
