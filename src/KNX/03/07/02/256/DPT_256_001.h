// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/256/DPT_256.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 256.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_53
 */
class KNX_EXPORT DPT_256_001 : public DPT_256
{
public:
    explicit DPT_256_001();

    std::string text() const override;
};

using DPT_DateTime_Period = DPT_256_001;

}
