// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/257/DPT_257.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_257::DPT_257(const uint16_t subnumber) :
    Datapoint_Type(257, subnumber)
{
}

void DPT_257::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 12) {
        throw DataDoesntMatchDPTException(12, data_size);
    }

    union {
        float f;
        uint32_t u{};
    };

    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    phase_1 = f;
    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    phase_2 = f;
    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    phase_3 = f;

    assert(first == last);
}

std::vector<uint8_t> DPT_257::toData() const
{
    std::vector<uint8_t> data;

    union {
        float f;
        uint32_t u{};
    };

    f = phase_1;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);
    f = phase_2;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);
    f = phase_3;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);

    return data;
}

}
