// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 257.* @note Not in MasterData yet
 *
 * Datapoint Types "F32F32F32"
 *
 * @ingroup KNX_03_07_02_09_08
 */
class KNX_EXPORT DPT_257 : public Datapoint_Type
{
public:
    explicit DPT_257(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Phase 1 */
    float phase_1{0.0};

    /** Phase 2 */
    float phase_2{0.0};

    /** Phase 3 */
    float phase_3{0.0};
};

}
