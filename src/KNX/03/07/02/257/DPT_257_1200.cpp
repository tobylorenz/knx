// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/257/DPT_257_1200.h>

#include <sstream>

#include <KNX/03/07/02/14/DPT_14_020.h>

namespace KNX {

DPT_257_1200::DPT_257_1200() :
    DPT_257(1200)
{
}

std::string DPT_257_1200::text() const
{
    std::ostringstream oss;

    DPT_Value_Electric_CurrentDensity dpt_14_020;
    dpt_14_020.float_value = phase_1;
    oss << "Phase 1: "
        << dpt_14_020.text();
    dpt_14_020.float_value = phase_2;
    oss << ", Phase 2: "
        << dpt_14_020.text();
    dpt_14_020.float_value = phase_3;
    oss << ", Phase 3: "
        << dpt_14_020.text();


    return oss.str();
}

}
