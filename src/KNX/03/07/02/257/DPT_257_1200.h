// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/257/DPT_257.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 257.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_08
 *
 * @see DPT_Value_Electric_CurrentDensity
 */
class KNX_EXPORT DPT_257_1200 : public DPT_257
{
public:
    explicit DPT_257_1200();

    std::string text() const override;
};

using DPT_Value_Electric_Current_3 = DPT_257_1200;

}
