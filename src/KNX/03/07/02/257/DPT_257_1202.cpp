// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/257/DPT_257_1202.h>

#include <sstream>

#include <KNX/03/07/02/14/DPT_14_080.h>

namespace KNX {

DPT_257_1202::DPT_257_1202() :
    DPT_257(1202)
{
}

std::string DPT_257_1202::text() const
{
    std::ostringstream oss;

    DPT_Value_ApparentPower dpt_14_080;
    dpt_14_080.float_value = phase_1;
    oss << "Phase 1: "
        << dpt_14_080.text();
    dpt_14_080.float_value = phase_2;
    oss << ", Phase 2: "
        << dpt_14_080.text();
    dpt_14_080.float_value = phase_3;
    oss << ", Phase 3: "
        << dpt_14_080.text();

    return oss.str();
}

}
