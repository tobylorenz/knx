// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/26/DPT_26.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_26::DPT_26(const uint16_t subnumber) :
    Datapoint_Type(26, subnumber)
{
}

void DPT_26::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    B = (*first >> 6) & 0x01;
    SceneNumber = (*first++ & 0x3f);

    assert(first == last);
}

std::vector<uint8_t> DPT_26::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((B << 6) | (SceneNumber & 0x3f));

    return data;
}

}
