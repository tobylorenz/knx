// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/26/DPT_26_001.h>

#include <sstream>

namespace KNX {

DPT_26_001::DPT_26_001() :
    DPT_26(1)
{
}

std::string DPT_26_001::text() const
{
    std::ostringstream oss;

    oss << (B ? "scene is inactive" : "scene is active")
        << ", Scene number: "
        << static_cast<uint16_t>(SceneNumber);

    return oss.str();
}

}
