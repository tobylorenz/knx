// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/26/DPT_26.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 26.001 scene information
 *
 * @ingroup KNX_03_07_02_03_25
 */
class KNX_EXPORT DPT_26_001 : public DPT_26
{
public:
    explicit DPT_26_001();

    std::string text() const override;
};

using DPT_SceneInfo = DPT_26_001;

}
