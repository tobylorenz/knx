// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/265/DPT_265.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_265::DPT_265(const uint16_t subnumber) :
    Datapoint_Type(265, subnumber)
{
}

void DPT_265::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 9) {
        throw DataDoesntMatchDPTException(9, data_size);
    }

    date_time.fromData(first, first + 8);
    first += 8;

    binary_information = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_265::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> date_time_data = date_time.toData();
    data.insert(std::cend(data), std::cbegin(date_time_data), std::cend(date_time_data));

    data.push_back(binary_information);

    return data;
}

}
