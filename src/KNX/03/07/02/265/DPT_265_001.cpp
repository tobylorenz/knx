// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/265/DPT_265_001.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/07/02/1/DPT_1_001.h>

namespace KNX {

DPT_265_001::DPT_265_001() :
    DPT_265(1)
{
}

std::string DPT_265_001::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_Switch dpt_1_001;
    dpt_1_001.b = binary_information;
    oss << ", Binary Information: "
        << dpt_1_001.text();

    return oss.str();
}

}
