// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/265/DPT_265_005.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/07/02/1/DPT_1_005.h>

namespace KNX {

DPT_265_005::DPT_265_005() :
    DPT_265(5)
{
}

std::string DPT_265_005::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_Alarm dpt_1_005;
    dpt_1_005.b = binary_information;
    oss << ", Binary Information: "
        << dpt_1_005.text();

    return oss.str();
}

}
