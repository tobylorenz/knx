// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/265/DPT_265_009.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/07/02/1/DPT_1_009.h>

namespace KNX {

DPT_265_009::DPT_265_009() :
    DPT_265(9)
{
}

std::string DPT_265_009::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_OpenClose dpt_1_009;
    dpt_1_009.b = binary_information;
    oss << ", Binary Information: "
        << dpt_1_009.text();

    return oss.str();
}

}
