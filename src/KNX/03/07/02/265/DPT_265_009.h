// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/265/DPT_265.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 265.009 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_54
 *
 * @see DPT_OpenClose
 */
class KNX_EXPORT DPT_265_009 : public DPT_265
{
public:
    explicit DPT_265_009();

    std::string text() const override;
};

using DPT_DateTime_OpenClose = DPT_265_009;

}
