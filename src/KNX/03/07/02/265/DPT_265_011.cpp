// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/265/DPT_265_011.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/07/02/1/DPT_1_011.h>

namespace KNX {

DPT_265_011::DPT_265_011() :
    DPT_265(11)
{
}

std::string DPT_265_011::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_State dpt_1_011;
    dpt_1_011.b = binary_information;
    oss << ", Binary Information: "
        << dpt_1_011.text();

    return oss.str();
}

}
