// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/265/DPT_265.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 265.012 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_54
 *
 * @see DPT_Invert
 */
class KNX_EXPORT DPT_265_012 : public DPT_265
{
public:
    explicit DPT_265_012();

    std::string text() const override;
};

using DPT_DateTime_Invert = DPT_265_012;

}
