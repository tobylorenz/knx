// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/265/DPT_265_1200.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/07/02/1/DPT_1_1200.h>

namespace KNX {

DPT_265_1200::DPT_265_1200() :
    DPT_265(1200)
{
}

std::string DPT_265_1200::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_ConsumerProducer dpt_1_1200;
    dpt_1_1200.b = binary_information;
    oss << ", Binary Information: "
        << dpt_1_1200.text();

    return oss.str();
}

}
