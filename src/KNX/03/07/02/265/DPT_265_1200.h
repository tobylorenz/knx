// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/265/DPT_265.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 265.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_09
 *
 * @see DPT_ConsumerProducer
 */
class KNX_EXPORT DPT_265_1200 : public DPT_265
{
public:
    explicit DPT_265_1200();

    std::string text() const override;
};

using DPT_DateTime_ConsumerProducer = DPT_265_1200;

}
