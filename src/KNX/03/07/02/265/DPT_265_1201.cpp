// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/265/DPT_265_1201.h>

#include <iomanip>
#include <sstream>

#include <KNX/03/07/02/1/DPT_1_1201.h>

namespace KNX {

DPT_265_1201::DPT_265_1201() :
    DPT_265(1201)
{
}

std::string DPT_265_1201::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_EnergyDirection dpt_1_1201;
    dpt_1_1201.b = binary_information;
    oss << ", Binary Information: "
        << dpt_1_1201.text();

    return oss.str();
}

}
