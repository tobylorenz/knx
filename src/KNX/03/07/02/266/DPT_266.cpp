// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/266/DPT_266.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_266::DPT_266(const uint16_t subnumber) :
    Datapoint_Type(266, subnumber)
{
}

void DPT_266::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 12) {
        throw DataDoesntMatchDPTException(12, data_size);
    }

    date_time.fromData(first, first + 8);
    first += 8;

    union {
        float f;
        uint32_t u{};
    };

    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    float_value = f;

    assert(first == last);
}

std::vector<uint8_t> DPT_266::toData() const
{
    std::vector<uint8_t> data = date_time.toData();

    union {
        float f;
        uint32_t u{};
    };

    f = float_value;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);

    return data;
}

}
