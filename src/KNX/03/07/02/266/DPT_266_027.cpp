// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/266/DPT_266_027.h>

#include <sstream>

#include <KNX/03/07/02/14/DPT_14_027.h>

namespace KNX {

DPT_266_027::DPT_266_027() :
    DPT_266(27)
{
}

std::string DPT_266_027::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_Value_Electric_Potential dpt_14_027;
    dpt_14_027.float_value = float_value;
    oss << ", Float: "
        << dpt_14_027.text();

    return oss.str();
}

}
