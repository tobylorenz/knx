// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/266/DPT_266_056.h>

#include <sstream>

#include <KNX/03/07/02/14/DPT_14_056.h>

namespace KNX {

DPT_266_056::DPT_266_056() :
    DPT_266(56)
{
}

std::string DPT_266_056::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_Value_Power dpt_14_056;
    dpt_14_056.float_value = float_value;
    oss << ", Float: "
        << dpt_14_056.text();

    return oss.str();
}

}
