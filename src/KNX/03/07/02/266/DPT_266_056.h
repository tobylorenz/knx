// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/266/DPT_266.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 266.056 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_55
 *
 * @see DPT_Value_Power
 */
class KNX_EXPORT DPT_266_056 : public DPT_266
{
public:
    explicit DPT_266_056();

    std::string text() const override;
};

using DPT_DateTime_Value_Power = DPT_266_056;

}
