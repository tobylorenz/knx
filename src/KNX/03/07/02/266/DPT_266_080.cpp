// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/266/DPT_266_080.h>

#include <sstream>

#include <KNX/03/07/02/14/DPT_14_080.h>

namespace KNX {

DPT_266_080::DPT_266_080() :
    DPT_266(80)
{
}

std::string DPT_266_080::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_Value_ApparentPower dpt_14_080;
    dpt_14_080.float_value = float_value;
    oss << ", Float: "
        << dpt_14_080.text();

    return oss.str();
}

}
