// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/267/DPT_267.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_267::DPT_267(const uint16_t subnumber) :
    Datapoint_Type(267, subnumber)
{
}

void DPT_267::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size < 8) {
        throw DataDoesntMatchDPTException(8, data_size);
    }

    date_time.fromData(first, first + 8);
    first += 8;

    str.fromData(first, last);
}

std::vector<uint8_t> DPT_267::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> date_time_data = date_time.toData();
    data.insert(std::cend(data), std::cbegin(date_time_data), std::cend(date_time_data));

    const std::vector<uint8_t> str_data = str.toData();
    data.insert(std::cend(data), std::cbegin(str_data), std::cend(str_data));

    return data;
}

}
