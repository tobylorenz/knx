// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/267/DPT_267_001.h>

#include <sstream>

namespace KNX {

DPT_267_001::DPT_267_001() :
    DPT_267(1)
{
}

std::string DPT_267_001::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    oss << ", String: "
        << str.text();

    return oss.str();
}

}
