// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/268/DPT_268.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_268::DPT_268(const uint16_t subnumber) :
    Datapoint_Type(268, subnumber)
{
}

void DPT_268::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 9) {
        throw DataDoesntMatchDPTException(9, data_size);
    }

    date_time.fromData(first, first + 8);
    first += 8;

    field1 = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_268::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> date_time_data = date_time.toData();
    data.insert(std::cend(data), std::cbegin(date_time_data), std::cend(date_time_data));

    data.push_back(field1);

    return data;
}

}
