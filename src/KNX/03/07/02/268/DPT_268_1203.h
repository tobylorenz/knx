// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/268/DPT_268.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 268.1203 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_10
 *
 * @see DPT_Breaker_Status
 */
class KNX_EXPORT DPT_268_1203 : public DPT_268
{
public:
    explicit DPT_268_1203();

    std::string text() const override;
};

using DPT_DateTime_Breaker_Status = DPT_268_1203;

}
