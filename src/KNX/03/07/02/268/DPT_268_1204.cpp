// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/268/DPT_268_1204.h>

#include <sstream>

#include <KNX/03/07/02/20/DPT_20_1204.h>

namespace KNX {

DPT_268_1204::DPT_268_1204() :
    DPT_268(1204)
{
}

std::string DPT_268_1204::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_Euridis_Communication_Interface_Status dpt_20_1204;
    dpt_20_1204.field1 = field1;
    oss << ", Field: "
        << dpt_20_1204.text();

    return oss.str();
}

}
