// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/269/DPT_269.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_269::DPT_269(const uint16_t subnumber) :
    Datapoint_Type(269, subnumber)
{
}

void DPT_269::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 14) {
        throw DataDoesntMatchDPTException(14, data_size);
    }

    date_time.fromData(first, first + 8);
    first += 8;

    tariff_active_energy.fromData(first, first + 6);
    first += 6;

    assert(first == last);
}

std::vector<uint8_t> DPT_269::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> date_time_data = date_time.toData();
    data.insert(std::cend(data), std::cbegin(date_time_data), std::cend(date_time_data));

    const std::vector<uint8_t> tariff_active_energy_data = tariff_active_energy.toData();
    data.insert(std::cend(data), std::cbegin(tariff_active_energy_data), std::cend(tariff_active_energy_data));

    return data;
}

}
