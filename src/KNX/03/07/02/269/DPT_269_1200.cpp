// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/269/DPT_269_1200.h>

#include <sstream>

namespace KNX {

DPT_269_1200::DPT_269_1200() :
    DPT_269(1200)
{
}

std::string DPT_269_1200::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    oss << ", Tariff Active energy: "
        << tariff_active_energy.text();

    return oss.str();
}

}
