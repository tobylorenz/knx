// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/269/DPT_269.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 269.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_11
 */
class KNX_EXPORT DPT_269_1200 : public DPT_269
{
public:
    explicit DPT_269_1200();

    std::string text() const override;
};

using DPT_DateTime_Tariff_ActiveEnergy = DPT_269_1200;

}
