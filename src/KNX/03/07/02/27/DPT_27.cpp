// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/27/DPT_27.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_27::DPT_27(const uint16_t subnumber) :
    Datapoint_Type(27, subnumber)
{
}

void DPT_27::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    m = (*first++ << 8) | (*first++);
    s = (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> DPT_27::toData() const
{
    std::vector<uint8_t> data;

    const uint16_t m_value = static_cast<uint16_t>(m.to_ulong());
    data.push_back(m_value >> 8);
    data.push_back(m_value & 0xff);

    const uint16_t s_value = static_cast<uint16_t>(s.to_ulong());
    data.push_back(s_value >> 8);
    data.push_back(s_value & 0xff);

    return data;
}

}
