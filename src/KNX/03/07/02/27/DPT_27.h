// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 27.* 32-bit set
 *
 * Datapoint Types "B32"
 *
 * @ingroup KNX_03_07_02_03_26
 */
class KNX_EXPORT DPT_27 : public Datapoint_Type
{
public:
    explicit DPT_27(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Mask Bit Info On Off Output */
    std::bitset<16> m{};

    /** Info On Off Output */
    std::bitset<16> s{};
};

}
