// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/27/DPT_27_001.h>

#include <sstream>

namespace KNX {

DPT_27_001::DPT_27_001() :
    DPT_27(1)
{
}

std::string DPT_27_001::text() const
{
    std::ostringstream oss;

    oss << "Output 1: " << (s[0] ? "On" : "Off") << " " << (m[0] ? "valid" : "not valid")
        << ", "
        << "Output 2: " << (s[1] ? "On" : "Off") << " " << (m[1] ? "valid" : "not valid")
        << ", "
        << "Output 3: " << (s[2] ? "On" : "Off") << " " << (m[2] ? "valid" : "not valid")
        << ", "
        << "Output 4: " << (s[3] ? "On" : "Off") << " " << (m[3] ? "valid" : "not valid")
        << ", "
        << "Output 5: " << (s[4] ? "On" : "Off") << " " << (m[4] ? "valid" : "not valid")
        << ", "
        << "Output 6: " << (s[5] ? "On" : "Off") << " " << (m[5] ? "valid" : "not valid")
        << ", "
        << "Output 7: " << (s[6] ? "On" : "Off") << " " << (m[6] ? "valid" : "not valid")
        << ", "
        << "Output 8: " << (s[7] ? "On" : "Off") << " " << (m[7] ? "valid" : "not valid")
        << ", "
        << "Output 9: " << (s[8] ? "On" : "Off") << " " << (m[8] ? "valid" : "not valid")
        << ", "
        << "Output 10: " << (s[9] ? "On" : "Off") << " " << (m[9] ? "valid" : "not valid")
        << ", "
        << "Output 11: " << (s[10] ? "On" : "Off") << " " << (m[10] ? "valid" : "not valid")
        << ", "
        << "Output 12: " << (s[11] ? "On" : "Off") << " " << (m[11] ? "valid" : "not valid")
        << ", "
        << "Output 13: " << (s[12] ? "On" : "Off") << " " << (m[12] ? "valid" : "not valid")
        << ", "
        << "Output 14: " << (s[13] ? "On" : "Off") << " " << (m[13] ? "valid" : "not valid")
        << ", "
        << "Output 15: " << (s[14] ? "On" : "Off") << " " << (m[14] ? "valid" : "not valid")
        << ", "
        << "Output 16: " << (s[15] ? "On" : "Off") << " " << (m[15] ? "valid" : "not valid");

    return oss.str();
}

}
