// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/270/DPT_270.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_270::DPT_270(const uint16_t subnumber) :
    Datapoint_Type(270, subnumber)
{
}

void DPT_270::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 20) {
        throw DataDoesntMatchDPTException(20, data_size);
    }

    date_time.fromData(first, first + 8);
    first += 8;

    union {
        float f;
        uint32_t u{};
    };

    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    phase_1 = f;

    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    phase_2 = f;

    u = (*first++ << 24) | (*first++ << 16) | (*first++ << 8) | *first++;
    phase_3 = f;

    assert(first == last);
}

std::vector<uint8_t> DPT_270::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> date_time_data = date_time.toData();
    data.insert(std::cend(data), std::cbegin(date_time_data), std::cend(date_time_data));

    union {
        float f;
        uint32_t u{};
    };

    f = phase_1;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);

    f = phase_2;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);

    f = phase_3;
    data.push_back(u >> 24);
    data.push_back((u >> 16) & 0xff);
    data.push_back((u >> 8) & 0xff);
    data.push_back(u & 0xff);

    return data;
}

}
