// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/270/DPT_270_1200.h>

#include <sstream>

#include <KNX/03/07/02/257/DPT_257_1200.h>

namespace KNX {

DPT_270_1200::DPT_270_1200() :
    DPT_270(1200)
{
}

std::string DPT_270_1200::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_Value_Electric_Current_3 dpt_257_1200;
    dpt_257_1200.phase_1 = phase_1;
    dpt_257_1200.phase_2 = phase_2;
    dpt_257_1200.phase_3 = phase_3;
    oss << ", "
        << dpt_257_1200.text();

    return oss.str();
}

}
