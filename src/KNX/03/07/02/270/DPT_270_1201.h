// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/270/DPT_270.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 270.1201 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_12
 *
 * @see DPT_Value_Electric_Potential_3
 */
class KNX_EXPORT DPT_270_1201 : public DPT_270
{
public:
    explicit DPT_270_1201();

    std::string text() const override;
};

using DPT_DateTime_Value_Electric_Potential_3 = DPT_270_1201;

}
