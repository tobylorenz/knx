// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/270/DPT_270_1202.h>

#include <sstream>

#include <KNX/03/07/02/257/DPT_257_1202.h>

namespace KNX {

DPT_270_1202::DPT_270_1202() :
    DPT_270(1202)
{
}

std::string DPT_270_1202::text() const
{
    std::ostringstream oss;

    oss << "Date and Time: "
        << date_time.text();

    DPT_Value_ApparentPower_3 dpt_257_1202;
    dpt_257_1202.phase_1 = phase_1;
    dpt_257_1202.phase_2 = phase_2;
    dpt_257_1202.phase_3 = phase_3;
    oss << ", "
        << dpt_257_1202.text();

    return oss.str();
}

}
