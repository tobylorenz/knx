// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/271/DPT_271.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_271::DPT_271(const uint16_t subnumber) :
    Datapoint_Type(271, subnumber)
{
}

void DPT_271::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 7) {
        throw DataDoesntMatchDPTException(7, data_size);
    }

    time.fromData(first, first + 3);
    first += 3;

    item_number = (*first >> 4) & 0x0f;
    total_number_of_item = *first & 0x0f;
    ++first;

    tariff.fromData(first, first + 1);
    first += 1;

    day_profile = (*first >> 2) & 0x3f;
    dry_contact = *first & 0x03;
    ++first;

    vc = (*first++) & 0x7f;

    assert(first == last);
}

std::vector<uint8_t> DPT_271::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> time_data = time.toData();
    data.insert(std::cend(data), std::cbegin(time_data), std::cend(time_data));

    data.push_back((item_number << 4) | total_number_of_item);

    const std::vector<uint8_t> tariff_data = tariff.toData();
    data.insert(std::cend(data), std::cbegin(tariff_data), std::cend(tariff_data));

    data.push_back((day_profile << 2) | dry_contact);

    data.push_back(static_cast<uint8_t>(vc.to_ulong()));

    return data;
}

}
