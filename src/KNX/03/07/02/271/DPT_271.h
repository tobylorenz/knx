// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/10/DPT_10_001.h>
#include <KNX/03/07/02/5/DPT_5_006.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 271.* @note Not in MasterData yet
 *
 * Datapoint Types "[N3U5][r2U6][r2U6][U4U4]U8[U6N2][r1B7]"
 *
 * @ingroup KNX_03_07_02_09_13
 */
class KNX_EXPORT DPT_271 : public Datapoint_Type
{
public:
    explicit DPT_271(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Time */
    DPT_TimeOfDay time{};

    /** Item Number */
    uint4_t item_number{};

    /** Total Number Of Item */
    uint4_t total_number_of_item{};

    /** Tariff */
    DPT_Tariff tariff{};

    /** Day Profile # */
    uint6_t day_profile{};

    /** Dry Contact */
    uint2_t dry_contact{};

    /** VC */
    std::bitset<7> vc{};
};

}
