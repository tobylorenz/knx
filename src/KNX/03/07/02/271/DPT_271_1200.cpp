// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/271/DPT_271_1200.h>

#include <sstream>

namespace KNX {

DPT_271_1200::DPT_271_1200() :
    DPT_271(1200)
{
}

std::string DPT_271_1200::text() const
{
    std::ostringstream oss;

    oss << "Time: "
        << time.text();

    oss << ", Item Number: "
        << static_cast<uint16_t>(item_number);

    oss << ", Totel Number Of Items: "
        << static_cast<uint16_t>(total_number_of_item);

    oss << ", Tariff: "
        << tariff.text();

    oss << ", Day Profile: "
        << static_cast<uint16_t>(day_profile);

    oss << ", Dry Contact: ";
    switch (dry_contact) {
    case 0:
        oss << "The dry contact remains in the sale position (no switching).";
        break;
    case 1:
        oss << "If the contract is contract Temp, then this positions of the dry contact and the virtual contact #1 is defined by the object 'Tempo dry contact configuration'. "
            << "If the contract is not Tempo, then the dry contact remains in the same position (no switching).";
        break;
    case 2:
        oss << "The dry contact switches to open position and remains in open position.";
        break;
    case 3:
        oss << "The dry contact switches to closed position and remains in closed position.";
        break;
    }

    oss << ", VC1: " << (vc[0] ? "closed" : "open")
        << ", VC2: " << (vc[1] ? "closed" : "open")
        << ", VC3: " << (vc[2] ? "closed" : "open")
        << ", VC4: " << (vc[3] ? "closed" : "open")
        << ", VC5: " << (vc[4] ? "closed" : "open")
        << ", VC6: " << (vc[5] ? "closed" : "open")
        << ", VC7: " << (vc[6] ? "closed" : "open");

    return oss.str();
}

}
