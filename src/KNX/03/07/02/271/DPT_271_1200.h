// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/271/DPT_271.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 271.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_13
 */
class KNX_EXPORT DPT_271_1200 : public DPT_271
{
public:
    explicit DPT_271_1200();

    std::string text() const override;
};

using DPT_TariffDayProfile = DPT_271_1200;

}
