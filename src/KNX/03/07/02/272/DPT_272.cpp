// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/272/DPT_272.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_272::DPT_272(const uint16_t subnumber) :
    Datapoint_Type(272, subnumber)
{
}

void DPT_272::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 7) {
        throw DataDoesntMatchDPTException(7, data_size);
    }

    info_valid = *first++;

    time_period_1.unsigned_value = (*first++ << 8) | (*first++);

    time_period_2.unsigned_value = (*first++ << 8) | (*first++);

    em_level = *first++;

    dali_sa = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_272::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(info_valid);

    data.push_back(time_period_1.unsigned_value >> 8);
    data.push_back(time_period_1.unsigned_value & 0xff);

    data.push_back(time_period_2.unsigned_value >> 8);
    data.push_back(time_period_2.unsigned_value & 0xff);

    data.push_back(em_level);

    data.push_back(dali_sa);

    return data;
}

}
