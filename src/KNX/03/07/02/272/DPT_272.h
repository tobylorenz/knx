// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/7/DPT_7_007.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 272.* @note Not in MasterData yet
 *
 * Datapoint Types "N8U16U16U8U8"
 *
 * @ingroup KNX_03_07_02_06_22
 */
class KNX_EXPORT DPT_272 : public Datapoint_Type
{
public:
    explicit DPT_272(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** InfoValid */
    uint8_t info_valid{};

    /** TimePeriod1 */
    DPT_TimePeriodHrs time_period_1{};

    /** TimePeriod2 */
    DPT_TimePeriodHrs time_period_2{};

    /** EmLevel */
    uint8_t em_level{};

    /** DALI_SA */
    uint8_t dali_sa{};
};

}
