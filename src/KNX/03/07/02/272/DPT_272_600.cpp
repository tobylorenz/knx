// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/272/DPT_272_600.h>

#include <sstream>

namespace KNX {

DPT_272_600::DPT_272_600() :
    DPT_272(600)
{
}

std::string DPT_272_600::text() const
{
    std::ostringstream oss;

    oss << "Information Valid: ";
    switch (info_valid) {
    case 0:
        oss << "Converter is not existing or no information about the converter available";
        break;
    case 1:
        oss << "Converter information is valid";
        break;
    case 2:
        oss << "Converter information is invalid";
        break;
    default:
        oss << "Reserved; shall not be used.";
        break;
    }

    oss << ", Lamp Emergency Time: "
        << time_period_1.text();

    oss << ", Lamp Total Operation Time: "
        << time_period_2.text();

    oss << ", Emergency Level: ";
    if (em_level == 255) {
        oss << "Emergency Level unknown";
    } else {
        oss << static_cast<uint16_t>(em_level);
    }

    oss << ", DALI Short Address: "
        << static_cast<uint16_t>(dali_sa);

    return oss.str();
}

}
