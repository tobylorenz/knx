// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/272/DPT_272.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 272.600 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_06_22
 */
class KNX_EXPORT DPT_272_600 : public DPT_272
{
public:
    explicit DPT_272_600();

    std::string text() const override;
};

using DPT_Converter_Info = DPT_272_600;

}
