// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/273/DPT_273.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_273::DPT_273(const uint16_t subnumber) :
    Datapoint_Type(273, subnumber)
{
}

void DPT_273::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 8) {
        throw DataDoesntMatchDPTException(8, data_size);
    }

    mask = *first++;

    delay_time.unsigned_value = (*first++ << 8) | (*first++);

    probability.unsigned_value = *first++;

    maximum_value.fromData(first, first + 1);
    first += 2;

    minimum_value.fromData(first, first + 1);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> DPT_273::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(static_cast<uint8_t>(mask.to_ulong()));

    data.push_back(delay_time.unsigned_value >> 8);
    data.push_back(delay_time.unsigned_value & 0xff);

    data.push_back(probability.unsigned_value);

    const std::vector<uint8_t> maximum_value_data = maximum_value.toData();
    data.insert(std::cend(data), std::cbegin(maximum_value_data), std::cend(maximum_value_data));

    const std::vector<uint8_t> minimum_value_data = minimum_value.toData();
    data.insert(std::cend(data), std::cbegin(minimum_value_data), std::cend(minimum_value_data));

    return data;
}

}
