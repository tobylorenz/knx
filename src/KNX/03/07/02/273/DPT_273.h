// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/7/DPT_7_006.h>
#include <KNX/03/07/02/5/DPT_5_001.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/03/07/02/KNX_Float.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 273.* @note Not in MasterData yet
 *
 * Datapoint Types "B8U16U8F16F16"
 *
 * @ingroup KNX_03_07_02_10_01
 */
class KNX_EXPORT DPT_273 : public Datapoint_Type
{
public:
    explicit DPT_273(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Mask */
    std::bitset<8> mask{};

    /** DelayTime */
    DPT_TimePeriodMin delay_time{};

    /** Probability */
    DPT_Scaling probability{};

    /** Maximum Value */
    KNX_Float maximum_value{0.0};

    /** Minimum Value */
    KNX_Float minimum_value{0.0};
};

}
