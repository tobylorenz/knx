// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/273/DPT_273_003.h>

#include <sstream>

#include <KNX/03/07/02/9/DPT_9_007.h>

namespace KNX {

DPT_273_003::DPT_273_003() :
    DPT_273(3)
{
}

std::string DPT_273_003::text() const
{
    std::ostringstream oss;

    oss << "Mask: "
        << " validity of the Delay Time: " << (mask[3] ? "valid" : "not valid")
        << ", validity of the Probability: " << (mask[2] ? "valid" : "not valid")
        << ", validity of the Maximum Value: " << (mask[1] ? "valid" : "not valid")
        << ", validity of the Minimum Value: " << (mask[0] ? "valid" : "not valid");

    oss << ", Delay Time: "
        << delay_time.text();

    oss << ", Probability: "
        << probability.text();

    DPT_Value_Humidity dpt_9_007;
    dpt_9_007.float_value = maximum_value;
    oss << ", Maximum Value: "
        << dpt_9_007.text();
    dpt_9_007.float_value = minimum_value;
    oss << ", Minimum Value: "
        << dpt_9_007.text();

    return oss.str();
}

}
