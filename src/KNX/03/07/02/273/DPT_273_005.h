// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/273/DPT_273.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 273.005 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_10_01
 *
 * @see DPT_Value_AirQuality
 */
class KNX_EXPORT DPT_273_005 : public DPT_273
{
public:
    explicit DPT_273_005();

    std::string text() const override;
};

using DPT_Forecast_CO2 = DPT_273_005;

}
