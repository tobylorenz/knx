// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/273/DPT_273.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 273.006 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_10_01
 *
 * @see DPT_Concentration_µgm3
 */
class KNX_EXPORT DPT_273_006 : public DPT_273
{
public:
    explicit DPT_273_006();

    std::string text() const override;
};

using DPT_Forecast_AirPollutants = DPT_273_006;

}
