// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/274/DPT_274.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_274::DPT_274(const uint16_t subnumber) :
    Datapoint_Type(274, subnumber)
{
}

void DPT_274::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 6) {
        throw DataDoesntMatchDPTException(6, data_size);
    }

    mask = *first++;
    delay_time.unsigned_value = (*first++ << 8) | (*first++);
    probability.unsigned_value = *first++;
    maximum_value = *first++;
    minimum_value = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_274::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(static_cast<uint8_t>(mask.to_ulong()));
    data.push_back(delay_time.unsigned_value >> 8);
    data.push_back(delay_time.unsigned_value & 0xff);
    data.push_back(probability.unsigned_value);
    data.push_back(maximum_value);
    data.push_back(minimum_value);

    return data;
}

}
