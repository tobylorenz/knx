// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/274/DPT_274_001.h>

#include <sstream>

#include <KNX/03/07/02/5/DPT_5_003.h>

namespace KNX {

DPT_274_001::DPT_274_001() :
    DPT_274(1)
{
}

std::string DPT_274_001::text() const
{
    std::ostringstream oss;

    oss << "Mask: "
        << " validity of the Delay Time: " << (mask[3] ? "valid" : "not valid")
        << ", validity of the Probability: " << (mask[2] ? "valid" : "not valid")
        << ", validity of the Maximum Value: " << (mask[1] ? "valid" : "not valid")
        << ", validity of the Minimum Value: " << (mask[0] ? "valid" : "not valid");

    oss << ", Delay Time: "
        << delay_time.text();

    oss << ", Probability: "
        << probability.text();

    DPT_Angle dpt_5_003;
    dpt_5_003.unsigned_value = maximum_value;
    oss << ", Maximum Value: "
        << dpt_5_003.text();
    dpt_5_003.unsigned_value = minimum_value;
    oss << ", Minimum Value: "
        << dpt_5_003.text();

    return oss.str();
}

}
