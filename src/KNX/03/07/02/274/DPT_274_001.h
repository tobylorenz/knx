// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/274/DPT_274.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 274.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_10_02
 *
 * @see DPT_Angle
 */
class KNX_EXPORT DPT_274_001 : public DPT_274
{
public:
    explicit DPT_274_001();

    std::string text() const override;
};

using DPT_Forecast_Wind_Direction = DPT_274_001;

}
