// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/03/07/02/KNX_Float.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 275.* F16F16F16F16
 *
 * Datapoint Types "F16F16F16F16"
 */
class KNX_EXPORT DPT_275 : public Datapoint_Type
{
public:
    explicit DPT_275(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** room temperature setpoint (shift) comfort */
    KNX_Float temp_setp_comf;

    /** room temperature setpoint (shift) standby */
    KNX_Float temp_setp_stdby;

    /** room temperature setpoint (shift) economy */
    KNX_Float temp_setp_eco;

    /** room temperature setpoint (shift) building protection */
    KNX_Float temp_setp_b_prot;
};

}
