// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/275/DPT_275_100.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_275_100::DPT_275_100() :
    DPT_275(100)
{
}

std::string DPT_275_100::text() const
{
    std::ostringstream oss;

    oss << "TempSetpComf: " << std::fixed << std::setprecision(2) << temp_setp_comf << " °C"
        << ", TempSetpStdby: " << std::fixed << std::setprecision(2) << temp_setp_stdby << " °C"
        << ", TempSetpEco: " << std::fixed << std::setprecision(2) << temp_setp_eco << " °C"
        << ", TempSetpBProt: " << std::fixed << std::setprecision(2) << temp_setp_b_prot << " °C";

    return oss.str();
}

}
