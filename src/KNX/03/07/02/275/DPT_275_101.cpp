// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/275/DPT_275_101.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_275_101::DPT_275_101() :
    DPT_275(101)
{
}

std::string DPT_275_101::text() const
{
    std::ostringstream oss;

    oss << "TempSetpShiftComf: " << std::fixed << std::setprecision(2) << temp_setp_comf << " K"
        << ", TempSetpShiftStdby: " << std::fixed << std::setprecision(2) << temp_setp_stdby << " K"
        << ", TempSetpShiftEco: " << std::fixed << std::setprecision(2) << temp_setp_eco << " K"
        << ", TempSetpShiftBProt: " << std::fixed << std::setprecision(2) << temp_setp_b_prot << " K";

    return oss.str();
}

}
