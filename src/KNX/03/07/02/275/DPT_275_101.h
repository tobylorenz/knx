// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/275/DPT_275.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 275.101 Temperature setpoint shift setting for 4 HVAC Modes
 *
 * @note ingroup: Not in specification
 */
class KNX_EXPORT DPT_275_101 : public DPT_275
{
public:
    explicit DPT_275_101();

    std::string text() const override;
};

using DPT_TempRoomSetpSetShiftF16_4 = DPT_275_101; // DPT_TempRoomSetpSetShiftF16[4]

}
