// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/276/DPT_276.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_276::DPT_276(const uint16_t subnumber) :
    Datapoint_Type(276, subnumber)
{
}

void DPT_276::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 4) {
        throw DataDoesntMatchDPTException(4, data_size);
    }

    duty_cycle_1.unsigned_value = *first++;
    duty_cycle_2.unsigned_value = *first++;
    duty_cycle_3.unsigned_value = *first++;

    ta.b = (*first >> 4) & 0x01;
    update.b = (*first >> 3) & 0x01;
    dca3.b = (*first >> 2) & 0x01;
    dca2.b = (*first >> 1) & 0x01;
    dca1.b = *first & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_276::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(duty_cycle_1.unsigned_value);
    data.push_back(duty_cycle_2.unsigned_value);
    data.push_back(duty_cycle_3.unsigned_value);
    data.push_back(
        (ta.b << 4) |
        (update.b << 3) |
        (dca3.b << 2) |
        (dca2.b << 1) |
        dca1.b);

    return data;
}

}
