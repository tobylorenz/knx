// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/1/DPT_1_001.h>
#include <KNX/03/07/02/1/DPT_1_005.h>
#include <KNX/03/07/02/5/DPT_5_001.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 276.* @note Not in MasterData yet
 *
 * Datapoint Types "U8U8U8r3B5"
 *
 * @ingroup KNX_03_07_02_09_14
 */
class KNX_EXPORT DPT_276 : public Datapoint_Type
{
public:
    explicit DPT_276(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Duty Cycle 1 */
    DPT_Scaling duty_cycle_1{};

    /** Duty Cycle 2 */
    DPT_Scaling duty_cycle_2{};

    /** Duty Cycle 3 */
    DPT_Scaling duty_cycle_3{};

    /** TA */
    DPT_Alarm ta{};

    /** Update */
    DPT_Switch update{};

    /** DCA3 */
    DPT_Alarm dca3{};

    /** DCA2 */
    DPT_Alarm dca2{};

    /** DCA1 */
    DPT_Alarm dca1{};
};

}
