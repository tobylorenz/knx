// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/276/DPT_276_1200.h>

#include <sstream>

namespace KNX {

DPT_276_1200::DPT_276_1200() :
    DPT_276(1200)
{
}

std::string DPT_276_1200::text() const
{
    std::ostringstream oss;

    oss << "Current duty cycle of channel F1: "
        << duty_cycle_1.text();

    oss << ", Current duty cycle of channel F2: "
        << duty_cycle_2.text();

    oss << ", Current duty cycle of channel F3: "
        << duty_cycle_3.text();

    oss << ", Duty Cycle Alarm of channel F1: "
        << dca1.text();

    oss << ", Duty Cycle Alarm of channel F2: "
        << dca2.text();

    oss << ", Duty Cycle Alarm of channel F3: "
        << dca3.text();

    oss << ", Software update is in progress: "
        << update.text();

    oss << ", TIC alarm: "
        << ta.text();

    return oss.str();
}

}
