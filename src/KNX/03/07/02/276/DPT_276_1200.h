// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/276/DPT_276.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 276.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_14
 */
class KNX_EXPORT DPT_276_1200 : public DPT_276
{
public:
    explicit DPT_276_1200();

    std::string text() const override;
};

using DPT_ERL_Status = DPT_276_1200;

}
