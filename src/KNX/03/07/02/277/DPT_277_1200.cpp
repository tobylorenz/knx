// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/277/DPT_277_1200.h>

#include <sstream>

namespace KNX {

DPT_277_1200::DPT_277_1200() :
    DPT_277(1200)
{
}

std::string DPT_277_1200::text() const
{
    std::ostringstream oss;

    oss << "String: "
        << str.text();

    for (uint8_t n = 0; n < 4; ++n) {
        oss << ", Energy Register " << static_cast<uint16_t>(n + 1) << ": "
            << energy_registers[n].text();
    }

    return oss.str();
}

}
