// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/277/DPT_277.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 277.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_15
 */
class KNX_EXPORT DPT_277_1200 : public DPT_277
{
public:
    explicit DPT_277_1200();

    std::string text() const override;
};

using DPT_4_EnergyRegisters = DPT_277_1200;

}
