// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/279/DPT_279.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_279::DPT_279(const uint16_t subnumber) :
    Datapoint_Type(279, subnumber)
{
}

void DPT_279::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 48) {
        throw DataDoesntMatchDPTException(48, data_size);
    }

    str.fromData(first, first + 12);
    first += 12;

    for (uint8_t n = 0; n < 6; ++n) {
        energy_registers[n].fromData(first, first + 6);
        first += 6;
    }

    assert(first == last);
}

std::vector<uint8_t> DPT_279::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> str_data = str.toData();
    data.insert(std::cend(data), std::cbegin(str_data), std::cend(str_data));

    for (uint8_t n = 0; n < 6; ++n) {
        const std::vector<uint8_t> energy_register_data = energy_registers[n].toData();
        data.insert(std::cend(data), std::cbegin(energy_register_data), std::cend(energy_register_data));
    }

    return data;
}

}
