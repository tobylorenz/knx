// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/279/DPT_279_1200.h>

#include <sstream>

namespace KNX {

DPT_279_1200::DPT_279_1200() :
    DPT_279(1200)
{
}

std::string DPT_279_1200::text() const
{
    std::ostringstream oss;

    oss << "String: "
        << str.text();

    for (uint8_t n = 0; n < 6; ++n) {
        oss << ", Energy Register " << static_cast<uint16_t>(n + 1) << ": "
            << energy_registers[n].text();
    }

    return oss.str();
}

}
