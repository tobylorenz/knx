// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/279/DPT_279.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 279.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_15
 */
class KNX_EXPORT DPT_279_1200 : public DPT_279
{
public:
    explicit DPT_279_1200();

    std::string text() const override;
};

using DPT_6_EnergyRegisters = DPT_279_1200;

}
