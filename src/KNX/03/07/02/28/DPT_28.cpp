// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/28/DPT_28.h>

#include <cassert>

namespace KNX {

DPT_28::DPT_28(const uint16_t subnumber) :
    Datapoint_Type(28, subnumber)
{
}

void DPT_28::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    str.assign(first, last);
}

std::vector<uint8_t> DPT_28::toData() const
{
    std::vector<uint8_t> data;

    data.assign(std::cbegin(str), std::cend(str));

    return data;
}

}
