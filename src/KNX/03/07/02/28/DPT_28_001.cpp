// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/28/DPT_28_001.h>

namespace KNX {

DPT_28_001::DPT_28_001() :
    DPT_28(1)
{
}

std::string DPT_28_001::text() const
{
    return str;
}

}
