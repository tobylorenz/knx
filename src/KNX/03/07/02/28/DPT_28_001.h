// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/28/DPT_28.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 28.001 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_03_27_01
 */
class KNX_EXPORT DPT_28_001 : public DPT_28
{
public:
    explicit DPT_28_001();

    std::string text() const override;
};

using DPT_UTF_8 = DPT_28_001; // DPT_UTF-8

}
