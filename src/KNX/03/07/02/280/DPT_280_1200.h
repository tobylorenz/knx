// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/280/DPT_280.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 280.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_15
 */
class KNX_EXPORT DPT_280_1200 : public DPT_280
{
public:
    explicit DPT_280_1200();

    std::string text() const override;
};

using DPT_11_EnergyRegisters = DPT_280_1200;

}
