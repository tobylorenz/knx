// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/281/DPT_281.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 281.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_16
 */
class KNX_EXPORT DPT_281_1200 : public DPT_281
{
public:
    explicit DPT_281_1200();

    std::string text() const override;
};

using DPT_DateTime_4_EnergyRegisters = DPT_281_1200;

}
