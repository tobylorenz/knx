// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>
#include <bitset>

#include <KNX/03/07/02/19/DPT_19_001.h>
#include <KNX/03/07/02/235/DPT_235_001.h>
#include <KNX/03/07/02/28/DPT_28_001.h>
#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 282.* @note Not in MasterData yet
 *
 * Datapoint Types "U8[r4U4][r3U5][U3U5][r2U6][r2U6]B16A[12](V32U8B8)[5]"
 *
 * @ingroup KNX_03_07_02_09_16
 */
class KNX_EXPORT DPT_282 : public Datapoint_Type
{
public:
    explicit DPT_282(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** Date Time */
    DPT_DateTime date_time;

    /** string */
    DPT_28_001 str;

    /** Energy Registers */
    std::array<DPT_Tariff_ActiveEnergy, 5> energy_registers;
};

}
