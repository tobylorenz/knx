// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/282/DPT_282.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 282.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_16
 */
class KNX_EXPORT DPT_282_1200 : public DPT_282
{
public:
    explicit DPT_282_1200();

    std::string text() const override;
};

using DPT_DateTime_5_EnergyRegisters = DPT_282_1200;

}
