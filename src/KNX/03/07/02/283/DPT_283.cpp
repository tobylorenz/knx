// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/283/DPT_283.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_283::DPT_283(const uint16_t subnumber) :
    Datapoint_Type(283, subnumber)
{
}

void DPT_283::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 56) {
        throw DataDoesntMatchDPTException(56, data_size);
    }

    date_time.fromData(first, first + 8);
    first += 8;

    str.fromData(first, first + 12);
    first += 12;

    for (uint8_t n = 0; n < 6; ++n) {
        energy_registers[n].fromData(first, first + 6);
        first += 6;
    }

    assert(first == last);
}

std::vector<uint8_t> DPT_283::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> date_time_data = date_time.toData();
    data.insert(std::cend(data), std::cbegin(date_time_data), std::cend(date_time_data));

    const std::vector<uint8_t> str_data = str.toData();
    data.insert(std::cend(data), std::cbegin(str_data), std::cend(str_data));

    for (uint8_t n = 0; n < 6; ++n) {
        const std::vector<uint8_t> energy_register_data = energy_registers[n].toData();
        data.insert(std::cend(data), std::cbegin(energy_register_data), std::cend(energy_register_data));
    }

    return data;
}

}
