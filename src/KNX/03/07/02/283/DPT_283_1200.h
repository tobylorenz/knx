// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/283/DPT_283.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 283.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_16
 */
class KNX_EXPORT DPT_283_1200 : public DPT_283
{
public:
    explicit DPT_283_1200();

    std::string text() const override;
};

using DPT_DateTime_6_EnergyRegisters = DPT_283_1200;

}
