// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/284/DPT_284.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 284.1200 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_09_16
 */
class KNX_EXPORT DPT_284_1200 : public DPT_284
{
public:
    explicit DPT_284_1200();

    std::string text() const override;
};

using DPT_DateTime_11_EnergyRegisters = DPT_284_1200;

}
