// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/29/DPT_29.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_29::DPT_29(const uint16_t subnumber) :
    Datapoint_Type(29, subnumber)
{
}

void DPT_29::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 8) {
        throw DataDoesntMatchDPTException(8, data_size);
    }

    signed_value =
        (static_cast<int64_t>(*first++) << 56) |
        (static_cast<int64_t>(*first++) << 48) |
        (static_cast<int64_t>(*first++) << 40) |
        (static_cast<int64_t>(*first++) << 32) |
        (static_cast<int64_t>(*first++) << 24) |
        (static_cast<int64_t>(*first++) << 16) |
        (static_cast<int64_t>(*first++) << 8) |
        (static_cast<int64_t>(*first++));

    assert(first == last);
}

std::vector<uint8_t> DPT_29::toData() const
{
    std::vector<uint8_t> data;

    data.push_back((signed_value >> 56) & 0xff);
    data.push_back((signed_value >> 48) & 0xff);
    data.push_back((signed_value >> 40) & 0xff);
    data.push_back((signed_value >> 32) & 0xff);
    data.push_back((signed_value >> 24) & 0xff);
    data.push_back((signed_value >> 16) & 0xff);
    data.push_back((signed_value >> 8) & 0xff);
    data.push_back(signed_value & 0xff);

    return data;
}

}
