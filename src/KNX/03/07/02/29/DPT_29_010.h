// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/29/DPT_29.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 29.010 active energy (Wh)
 *
 * @ingroup KNX_03_07_02_03_28_01
 */
class KNX_EXPORT DPT_29_010 : public DPT_29
{
public:
    explicit DPT_29_010();

    std::string text() const override;
};

using DPT_ActiveEnergy_V64 = DPT_29_010;

}
