// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/29/DPT_29_011.h>

#include <sstream>

namespace KNX {

DPT_29_011::DPT_29_011() :
    DPT_29(11)
{
}

std::string DPT_29_011::text() const
{
    std::ostringstream oss;
    oss << signed_value << " VAh";
    return oss.str();
}

}
