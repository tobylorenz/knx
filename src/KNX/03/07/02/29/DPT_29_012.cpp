// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/29/DPT_29_012.h>

#include <sstream>

namespace KNX {

DPT_29_012::DPT_29_012() :
    DPT_29(12)
{
}

std::string DPT_29_012::text() const
{
    std::ostringstream oss;
    oss << signed_value << " VARh";
    return oss.str();
}

}
