// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/29/DPT_29.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 29.012 reactive energy (VARh)
 *
 * @ingroup KNX_03_07_02_03_28_01
 */
class KNX_EXPORT DPT_29_012 : public DPT_29
{
public:
    explicit DPT_29_012();

    std::string text() const override;
};

using DPT_ReactiveEnergy_V64 = DPT_29_012;

}
