// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/3/DPT_3.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_3::DPT_3(const uint16_t subnumber) :
    Datapoint_Type(3, subnumber)
{
}

void DPT_3::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    c = (*first >> 3) & 0x01;
    step_code = *first & 0x07;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_3::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (c << 3) |
        (step_code & 0x07));

    return data;
}

}
