// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/3/DPT_3.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 3.007 dimming control
 *
 * @ingroup KNX_03_07_02_03_03_01
 */
class KNX_EXPORT DPT_3_007 : public DPT_3
{
public:
    explicit DPT_3_007();

    std::string text() const override;
};

using DPT_Control_Dimming = DPT_3_007;

}
