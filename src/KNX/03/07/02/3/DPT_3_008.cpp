// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/3/DPT_3_008.h>

#include <sstream>

namespace KNX {

DPT_3_008::DPT_3_008() :
    DPT_3(8)
{
}

std::string DPT_3_008::text() const
{
    std::ostringstream oss;
    oss << (c ? "Down" : "Up") << ", ";
    switch (step_code) {
    case 0:
        oss << "Break";
        break;
    case 1:
        oss << "100 %";
        break;
    case 2:
        oss << "50 %";
        break;
    case 3:
        oss << "25 %";
        break;
    case 4:
        oss << "12 %"; // 12.5 %
        break;
    case 5:
        oss << "6 %"; // 6.25 %
        break;
    case 6:
        oss << "3 %"; // 3.125 %
        break;
    case 7:
        oss << "1 %"; // 1.5625 %
        break;
    }
    return oss.str();
}

}
