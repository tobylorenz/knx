// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/30/DPT_30.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_30::DPT_30(const uint16_t subnumber) :
    Datapoint_Type(30, subnumber)
{
}

void DPT_30::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 3) {
        throw DataDoesntMatchDPTException(3, data_size);
    }

    channel_activation = (*first++ << 16) | (*first++ << 8) | (*first++);

    assert(first == last);
}

std::vector<uint8_t> DPT_30::toData() const
{
    std::vector<uint8_t> data;

    const uint32_t channel_activation_value = channel_activation.to_ulong();
    data.push_back((channel_activation_value >> 16) & 0xff);
    data.push_back((channel_activation_value >> 8) & 0xff);
    data.push_back(channel_activation_value & 0xff);

    return data;
}

}
