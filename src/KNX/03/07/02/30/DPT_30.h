// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <bitset>

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 30.* 24 times channel activation
 *
 * Datapoint Types "B24"
 *
 * @ingroup KNX_03_07_02_08_05
 */
class KNX_EXPORT DPT_30 : public Datapoint_Type
{
public:
    explicit DPT_30(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** channel activation */
    std::bitset<24> channel_activation{};
};

}
