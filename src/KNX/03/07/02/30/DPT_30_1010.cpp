// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/30/DPT_30_1010.h>

namespace KNX {

DPT_30_1010::DPT_30_1010() :
    DPT_30(1010)
{
}

std::string DPT_30_1010::text() const
{
    return channel_activation.to_string();
}

}
