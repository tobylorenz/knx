// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/30/DPT_30.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 30.1010 activation state 0..23
 *
 * @ingroup KNX_03_07_02_08_05_01
 */
class KNX_EXPORT DPT_30_1010 : public DPT_30
{
public:
    explicit DPT_30_1010();

    std::string text() const override;
};

using DPT_Channel_Activation_24 = DPT_30_1010;

}
