// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/31/DPT_31.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_31::DPT_31(const uint16_t subnumber) :
    Datapoint_Type(31, subnumber)
{
}

void DPT_31::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    s = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_31::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(s);

    return data;
}

}
