// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/31/DPT_31_101.h>

#include <sstream>

namespace KNX {

DPT_31_101::DPT_31_101() :
    DPT_31(101)
{
}

std::string DPT_31_101::text() const
{
    std::ostringstream oss;
    switch (s) {
    case 0:
        oss << "pressed: Comfort, released: Economy";
        break;
    case 1:
        oss << "pressed: Comfort, released: (no transmission)";
        break;
    case 2:
        oss << "pressed: Economy, released: (no transmission)";
        break;
    case 3:
        oss << "pressed: Building prot., released: Auto";
        break;
    case 4:
        oss << "pressed: Building prot., released: (no transmission)";
        break;
    case 5:
        oss << "pressed: Auto, released: (no transmission)";
        break;
    case 6:
        oss << "pressed: Standby, released: (no transmission)";
        break;
    case 7:
        oss << "pressed: Comfort, released: Standby";
        break;
    }
    return oss.str();
}

}
