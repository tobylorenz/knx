// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/31/DPT_31.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 31.101 @note Not in MasterData yet
 *
 * @ingroup KNX_03_07_02_04_07_01
 */
class KNX_EXPORT DPT_31_101 : public DPT_31
{
public:
    explicit DPT_31_101();

    std::string text() const override;
};

using DPT_PB_Action_HVAC_Extended = DPT_31_101;

}
