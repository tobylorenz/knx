// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/4/DPT_4.h>

#include <cassert>

#include <KNX/03/07/02/ISO_8859_1.h>
#include <KNX/Exceptions.h>

namespace KNX {

DPT_4::DPT_4(const uint16_t subnumber) :
    Datapoint_Type(4, subnumber)
{
}

void DPT_4::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    character = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_4::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(character);

    return data;
}

std::string DPT_4::text() const
{
    return iso_8859_1_to_utf_8.at(character);
}

}
