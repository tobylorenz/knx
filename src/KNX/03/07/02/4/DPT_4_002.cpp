// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/4/DPT_4_002.h>

namespace KNX {

DPT_4_002::DPT_4_002() :
    DPT_4(2)
{
}

}
