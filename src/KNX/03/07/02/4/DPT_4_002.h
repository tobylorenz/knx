// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/4/DPT_4.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 4.002 character (ISO 8859-1)
 *
 * @ingroup KNX_03_07_02_03_04
 */
class KNX_EXPORT DPT_4_002 : public DPT_4
{
public:
    explicit DPT_4_002();
};

using DPT_Char_8859_1 = DPT_4_002;

}
