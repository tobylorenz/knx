// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/5/DPT_5_001.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_5_001::DPT_5_001() :
    DPT_5(1)
{
}

std::string DPT_5_001::text() const
{
    std::ostringstream oss;
    oss << std::fixed << std::setprecision(1) << unsigned_value * 100.0 / 255.0 << " %";
    return oss.str();
}

}
