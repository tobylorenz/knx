// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/5/DPT_5.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 5.001 percentage (0..100%)
 *
 * @ingroup KNX_03_07_02_03_05_01
 */
class KNX_EXPORT DPT_5_001 : public DPT_5
{
public:
    explicit DPT_5_001();

    std::string text() const override;
};

using DPT_Scaling = DPT_5_001;

}
