// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/5/DPT_5_004.h>

#include <sstream>

namespace KNX {

DPT_5_004::DPT_5_004() :
    DPT_5(4)
{
}

std::string DPT_5_004::text() const
{
    std::ostringstream oss;
    oss << static_cast<uint16_t>(unsigned_value) << " %";
    return oss.str();
}

}
