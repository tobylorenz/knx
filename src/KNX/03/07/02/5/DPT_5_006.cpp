// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/5/DPT_5_006.h>

#include <sstream>

namespace KNX {

DPT_5_006::DPT_5_006() :
    DPT_5(6)
{
}

std::string DPT_5_006::text() const
{
    std::ostringstream oss;
    switch (unsigned_value) {
    case 0:
        oss << "no tariff available";
        break;
    case 255:
        oss << "reserved";
        break;
    default:
        oss << "tariff " << static_cast<uint16_t>(unsigned_value);
    }
    return oss.str();
}

}
