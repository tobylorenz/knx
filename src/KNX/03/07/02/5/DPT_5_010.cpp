// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/5/DPT_5_010.h>

#include <sstream>

namespace KNX {

DPT_5_010::DPT_5_010() :
    DPT_5(10)
{
}

std::string DPT_5_010::text() const
{
    std::ostringstream oss;
    oss << static_cast<uint16_t>(unsigned_value) << " counter pulses";
    return oss.str();
}

}
