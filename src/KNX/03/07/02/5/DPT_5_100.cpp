// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/5/DPT_5_100.h>

#include <sstream>

namespace KNX {

DPT_5_100::DPT_5_100() :
    DPT_5(100)
{
}

std::string DPT_5_100::text() const
{
    std::ostringstream oss;
    oss << static_cast<uint16_t>(unsigned_value) << " fan stage";
    return oss.str();
}

}
