// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/5/DPT_5.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 5.100 fan stage (0..255)
 *
 * @note ingroup: Not in specification
 */
class KNX_EXPORT DPT_5_100 : public DPT_5
{
public:
    explicit DPT_5_100();

    std::string text() const override;
};

using DPT_FanStage = DPT_5_100;

}
