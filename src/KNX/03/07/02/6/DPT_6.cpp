// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/6/DPT_6.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_6::DPT_6(const uint16_t subnumber) :
    Datapoint_Type(6, subnumber)
{
}

void DPT_6::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    signed_value = *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_6::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(signed_value);

    return data;
}

}
