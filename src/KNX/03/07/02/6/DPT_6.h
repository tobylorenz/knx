// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 6.* 8-bit signed value
 *
 * Datapoint Types "V8"
 *
 * @ingroup KNX_03_07_02_03_06
 */
class KNX_EXPORT DPT_6 : public Datapoint_Type
{
public:
    explicit DPT_6(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    /** rel. signed value */
    int8_t signed_value{};
};

}
