// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/6/DPT_6_001.h>

#include <sstream>

namespace KNX {

DPT_6_001::DPT_6_001() :
    DPT_6(1)
{
}

std::string DPT_6_001::text() const
{
    std::ostringstream oss;
    oss << static_cast<int16_t>(signed_value) << " %";
    return oss.str();
}

}
