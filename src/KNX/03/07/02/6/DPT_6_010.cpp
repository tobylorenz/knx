// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/6/DPT_6_010.h>

#include <sstream>

namespace KNX {

DPT_6_010::DPT_6_010() :
    DPT_6(10)
{
}

std::string DPT_6_010::text() const
{
    std::ostringstream oss;
    oss << static_cast<int16_t>(signed_value) << " counter pulses";
    return oss.str();
}

}
