// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/6/DPT_6.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 6.010 counter pulses (-128..127)
 *
 * @ingroup KNX_03_07_02_03_06_01
 */
class KNX_EXPORT DPT_6_010 : public DPT_6
{
public:
    explicit DPT_6_010();

    std::string text() const override;
};

using DPT_Value_1_Count = DPT_6_010;

}
