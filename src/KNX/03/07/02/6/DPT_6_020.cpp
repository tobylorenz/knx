// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/6/DPT_6_020.h>

#include <cassert>
#include <sstream>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_6_020::DPT_6_020() :
    Datapoint_Type(6, 20)
{
}

void DPT_6_020::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    a = (*first >> 7) & 0x01;
    b = (*first >> 6) & 0x01;
    c = (*first >> 5) & 0x01;
    d = (*first >> 4) & 0x01;
    e = (*first >> 3) & 0x01;
    f = static_cast<F>(*first  & 0x07);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> DPT_6_020::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (a << 7) |
        (b << 6) |
        (c << 5) |
        (d << 4) |
        (e << 3) |
        (static_cast<uint8_t>(f) & 0x07));

    return data;
}

std::string DPT_6_020::text() const
{
    std::ostringstream oss;
    oss << (a ? "A clear" : "A set") << ", "
        << (b ? "B clear" : "B set") << ", "
        << (c ? "C clear" : "C set") << ", "
        << (d ? "D clear" : "D set") << ", "
        << (e ? "E clear" : "E set");
    switch (f) {
    case F::mode_0_is_active:
        oss << ", mode 0 is active";
        break;
    case F::mode_1_is_active:
        oss << ", mode 1 is active";
        break;
    case F::mode_2_is_active:
        oss << ", mode 2 is active";
        break;
    }
    return oss.str();
}

}
