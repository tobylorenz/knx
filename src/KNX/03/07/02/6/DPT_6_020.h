// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/6/DPT_6.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 6.020 status with mode
 *
 * Datapoint Types "B5N3"
 *
 * @ingroup KNX_03_07_02_03_07
 */
class KNX_EXPORT DPT_6_020 : public Datapoint_Type
{
public:
    explicit DPT_6_020();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;

    bool a{false};
    bool b{false};
    bool c{false};
    bool d{false};
    bool e{false};
    enum class F : uint8_t { // actually uint3_t
        mode_0_is_active = 1,
        mode_1_is_active = 2,
        mode_2_is_active = 4
    };
    F f{F::mode_0_is_active};

    std::string text() const override;
};

using DPT_Status_Mode3 = DPT_6_020;

}
