// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/7/DPT_7.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_7::DPT_7(const uint16_t subnumber) :
    Datapoint_Type(7, subnumber)
{
}

void DPT_7::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    unsigned_value = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_7::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(unsigned_value >> 8);
    data.push_back(unsigned_value & 0xff);

    return data;
}

}
