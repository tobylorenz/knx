// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/7/DPT_7_001.h>

#include <sstream>

namespace KNX {

DPT_7_001::DPT_7_001() :
    DPT_7(1)
{
}

std::string DPT_7_001::text() const
{
    std::ostringstream oss;
    oss << unsigned_value << " pulses";
    return oss.str();
}

}
