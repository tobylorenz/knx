// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/7/DPT_7_003.h>

#include <sstream>

namespace KNX {

DPT_7_003::DPT_7_003() :
    DPT_7(3)
{
}

std::string DPT_7_003::text() const
{
    std::ostringstream oss;
    oss << 10 * unsigned_value << " ms";
    return oss.str();
}

}
