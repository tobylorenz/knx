// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/7/DPT_7_004.h>

#include <sstream>

namespace KNX {

DPT_7_004::DPT_7_004() :
    DPT_7(4)
{
}

std::string DPT_7_004::text() const
{
    std::ostringstream oss;
    oss << 100 * unsigned_value << " ms";
    return oss.str();
}

}
