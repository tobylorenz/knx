// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/7/DPT_7_005.h>

#include <sstream>

namespace KNX {

DPT_7_005::DPT_7_005() :
    DPT_7(5)
{
}

std::string DPT_7_005::text() const
{
    std::ostringstream oss;
    oss << unsigned_value << " s";
    return oss.str();
}

}
