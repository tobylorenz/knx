// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/7/DPT_7_007.h>

#include <sstream>

namespace KNX {

DPT_7_007::DPT_7_007() :
    DPT_7(7)
{
}

std::string DPT_7_007::text() const
{
    std::ostringstream oss;
    oss << unsigned_value << " h";
    return oss.str();
}

}
