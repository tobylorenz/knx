// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/7/DPT_7.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 7.007 time (h)
 *
 * @ingroup KNX_03_07_02_03_08_02
 */
class KNX_EXPORT DPT_7_007 : public DPT_7
{
public:
    explicit DPT_7_007();

    std::string text() const override;
};

using DPT_TimePeriodHrs = DPT_7_007;

}
