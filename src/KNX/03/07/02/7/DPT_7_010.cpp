// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/7/DPT_7_010.h>

#include <sstream>

#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

DPT_7_010::DPT_7_010() :
    DPT_7(10)
{
}

std::string DPT_7_010::text() const
{
    std::ostringstream oss;

    switch (unsigned_value) {
    case PDT_CONTROL:
        oss << "PDT_CONTROL";
        break;
    case PDT_CHAR:
        oss << "PDT_CHAR";
        break;
    case PDT_UNSIGNED_CHAR:
        oss << "PDT_UNSIGNED_CHAR";
        break;
    case PDT_INT:
        oss << "PDT_INT";
        break;
    case PDT_UNSIGNED_INT:
        oss << "PDT_UNSIGNED_INT";
        break;
    case PDT_KNX_FLOAT:
        oss << "PDT_KNX_FLOAT";
        break;
    case PDT_DATE:
        oss << "PDT_DATE";
        break;
    case PDT_TIME:
        oss << "PDT_TIME";
        break;
    case PDT_LONG:
        oss << "PDT_LONG";
        break;
    case PDT_UNSIGNED_LONG:
        oss << "PDT_UNSIGNED_LONG";
        break;
    case PDT_FLOAT:
        oss << "PDT_FLOAT";
        break;
    case PDT_DOUBLE:
        oss << "PDT_DOUBLE";
        break;
    case PDT_CHAR_BLOCK:
        oss << "PDT_CHAR_BLOCK";
        break;
    case PDT_POLL_GROUP_SETTINGS:
        oss << "PDT_POLL_GROUP_SETTINGS";
        break;
    case PDT_SHORT_CHAR_BLOCK:
        oss << "PDT_SHORT_CHAR_BLOCK";
        break;
    case PDT_DATE_TIME:
        oss << "PDT_DATE_TIME";
        break;
    case PDT_VARIABLE_LENGTH:
        oss << "PDT_VARIABLE_LENGTH";
        break;
    case PDT_GENERIC_01:
        oss << "PDT_GENERIC_01";
        break;
    case PDT_GENERIC_02:
        oss << "PDT_GENERIC_02";
        break;
    case PDT_GENERIC_03:
        oss << "PDT_GENERIC_03";
        break;
    case PDT_GENERIC_04:
        oss << "PDT_GENERIC_04";
        break;
    case PDT_GENERIC_05:
        oss << "PDT_GENERIC_05";
        break;
    case PDT_GENERIC_06:
        oss << "PDT_GENERIC_06";
        break;
    case PDT_GENERIC_07:
        oss << "PDT_GENERIC_07";
        break;
    case PDT_GENERIC_08:
        oss << "PDT_GENERIC_08";
        break;
    case PDT_GENERIC_09:
        oss << "PDT_GENERIC_09";
        break;
    case PDT_GENERIC_10:
        oss << "PDT_GENERIC_10";
        break;
    case PDT_GENERIC_11:
        oss << "PDT_GENERIC_11";
        break;
    case PDT_GENERIC_12:
        oss << "PDT_GENERIC_12";
        break;
    case PDT_GENERIC_13:
        oss << "PDT_GENERIC_13";
        break;
    case PDT_GENERIC_14:
        oss << "PDT_GENERIC_14";
        break;
    case PDT_GENERIC_15:
        oss << "PDT_GENERIC_15";
        break;
    case PDT_GENERIC_16:
        oss << "PDT_GENERIC_16";
        break;
    case PDT_GENERIC_17:
        oss << "PDT_GENERIC_17";
        break;
    case PDT_GENERIC_18:
        oss << "PDT_GENERIC_18";
        break;
    case PDT_GENERIC_19:
        oss << "PDT_GENERIC_19";
        break;
    case PDT_GENERIC_20:
        oss << "PDT_GENERIC_20";
        break;
    case 0x25:
        oss << "0x25";
        break;
    case 0x26:
        oss << "0x26";
        break;
    case 0x27:
        oss << "0x27";
        break;
    case 0x28:
        oss << "0x28";
        break;
    case 0x29:
        oss << "0x29";
        break;
    case 0x2A:
        oss << "0x2A";
        break;
    case 0x2B:
        oss << "0x2B";
        break;
    case 0x2C:
        oss << "0x2C";
        break;
    case 0x2D:
        oss << "0x2D";
        break;
    case 0x2E:
        oss << "0x2E";
        break;
    case PDT_UTF8: // PDT_UTF-8
        oss << "PDT_UTF8";
        break;
    case PDT_VERSION:
        oss << "PDT_VERSION";
        break;
    case PDT_ALARM_INFO:
        oss << "PDT_ALARM_INFO";
        break;
    case PDT_BINARY_INFORMATION:
        oss << "PDT_BINARY_INFORMATION";
        break;
    case PDT_BITSET8:
        oss << "PDT_BITSET8";
        break;
    case PDT_BITSET16:
        oss << "PDT_BITSET16";
        break;
    case PDT_ENUM8:
        oss << "PDT_ENUM8";
        break;
    case PDT_SCALING:
        oss << "PDT_SCALING";
        break;
    case 0x37:
        oss << "0x37";
        break;
    case 0x38:
        oss << "0x38";
        break;
    case 0x39:
        oss << "0x39";
        break;
    case 0x3A:
        oss << "0x3A";
        break;
    case 0x3B:
        oss << "0x3B";
        break;
    case PDT_NE_VL:
        oss << "PDT_NE_VL";
        break;
    case PDT_NE_FL:
        oss << "PDT_NE_FL";
        break;
    case PDT_FUNCTION:
        oss << "PDT_FUNCTION";
        break;
    case PDT_ESCAPE:
        oss << "PDT_ESCAPE";
        break;
    }

    return oss.str();
}

}
