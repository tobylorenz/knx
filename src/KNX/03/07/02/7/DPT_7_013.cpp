// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/7/DPT_7_013.h>

#include <sstream>

namespace KNX {

DPT_7_013::DPT_7_013() :
    DPT_7(13)
{
}

std::string DPT_7_013::text() const
{
    std::ostringstream oss;
    oss << unsigned_value << " lux";
    return oss.str();
}

}
