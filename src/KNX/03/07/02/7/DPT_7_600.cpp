// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/7/DPT_7_600.h>

#include <sstream>

namespace KNX {

DPT_7_600::DPT_7_600() :
    DPT_7(600)
{
}

std::string DPT_7_600::text() const
{
    std::ostringstream oss;
    oss << unsigned_value << " K";
    return oss.str();
}

}
