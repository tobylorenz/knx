// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/8/DPT_8.h>

#include <cassert>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_8::DPT_8(const uint16_t subnumber) :
    Datapoint_Type(8, subnumber)
{
}

void DPT_8::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    signed_value = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> DPT_8::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(signed_value >> 8);
    data.push_back(signed_value & 0xff);

    return data;
}

}
