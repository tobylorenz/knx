// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/8/DPT_8_001.h>

#include <sstream>

namespace KNX {

DPT_8_001::DPT_8_001() :
    DPT_8(1)
{
}

std::string DPT_8_001::text() const
{
    std::ostringstream oss;
    oss << signed_value << " pulses";
    return oss.str();
}

}
