// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/8/DPT_8_004.h>

#include <sstream>

namespace KNX {

DPT_8_004::DPT_8_004() :
    DPT_8(4)
{
}

std::string DPT_8_004::text() const
{
    std::ostringstream oss;
    oss << 100 * signed_value << " ms";
    return oss.str();
}

}
