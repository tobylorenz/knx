// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/8/DPT_8.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 8.004 time lag (100 ms)
 *
 * @ingroup KNX_03_07_02_03_09_02
 */
class KNX_EXPORT DPT_8_004 : public DPT_8
{
public:
    explicit DPT_8_004();

    std::string text() const override;
};

using DPT_DeltaTime100MSec = DPT_8_004;

}
