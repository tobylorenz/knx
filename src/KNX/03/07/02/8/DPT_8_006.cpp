// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/8/DPT_8_006.h>

#include <sstream>

namespace KNX {

DPT_8_006::DPT_8_006() :
    DPT_8(6)
{
}

std::string DPT_8_006::text() const
{
    std::ostringstream oss;
    oss << signed_value << " min";
    return oss.str();
}

}
