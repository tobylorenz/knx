// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/8/DPT_8_010.h>

#include <iomanip>
#include <sstream>

namespace KNX {

DPT_8_010::DPT_8_010() :
    DPT_8(10)
{
}

std::string DPT_8_010::text() const
{
    std::ostringstream oss;
    oss << std::fixed << std::setprecision(2) << signed_value * 0.01 << " %";
    return oss.str();
}

}
