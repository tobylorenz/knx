// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/9/DPT_9.h>

#include <cassert>
#include <iomanip>
#include <sstream>

#include <KNX/Exceptions.h>

namespace KNX {

DPT_9::DPT_9(const uint16_t subnumber) :
    Datapoint_Type(9, subnumber)
{
}

void DPT_9::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    float_value.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> DPT_9::toData() const
{
    std::vector<uint8_t> data;

    const std::vector<uint8_t> float_value_data = float_value.toData();
    data.insert(std::cend(data), std::cbegin(float_value_data), std::cend(float_value_data));

    return data;
}

std::string DPT_9::text() const
{
    std::ostringstream oss;
    oss << std::fixed << std::setprecision(2) << float_value << unit();
    return oss.str();
}

}
