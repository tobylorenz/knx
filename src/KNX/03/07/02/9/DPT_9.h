// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/Datapoint_Type.h>
#include <KNX/03/07/02/KNX_Float.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * 9.* 2-byte float value
 *
 * Datapoint Types "F16"
 *
 * @ingroup KNX_03_07_02_03_10
 */
class KNX_EXPORT DPT_9 : public Datapoint_Type
{
public:
    explicit DPT_9(const uint16_t subnumber);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    std::string text() const override;

    /** float value */
    KNX_Float float_value{0.0};

    /** unit */
    virtual std::string unit() const = 0;
};

}
