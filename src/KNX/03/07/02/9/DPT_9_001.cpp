// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/9/DPT_9_001.h>

namespace KNX {

DPT_9_001::DPT_9_001() :
    DPT_9(1)
{
}

std::string DPT_9_001::unit() const
{
    return " °C";
}

}
