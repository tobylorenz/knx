// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/9/DPT_9.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 9.009 air flow (m³/h)
 *
 * @ingroup KNX_03_07_02_03_10
 */
class KNX_EXPORT DPT_9_009 : public DPT_9
{
public:
    explicit DPT_9_009();

    std::string unit() const override;
};

using DPT_Value_Airflow = DPT_9_009;

}
