// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/9/DPT_9_021.h>

namespace KNX {

DPT_9_021::DPT_9_021() :
    DPT_9(21)
{
}

std::string DPT_9_021::unit() const
{
    return " mA";
}

}
