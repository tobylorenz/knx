// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/9/DPT_9_024.h>

namespace KNX {

DPT_9_024::DPT_9_024() :
    DPT_9(24)
{
}

std::string DPT_9_024::unit() const
{
    return " kW";
}

}
