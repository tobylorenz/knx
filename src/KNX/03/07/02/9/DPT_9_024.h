// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/9/DPT_9.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * 9.024 power (kW)
 *
 * @ingroup KNX_03_07_02_03_10
 */
class KNX_EXPORT DPT_9_024 : public DPT_9
{
public:
    explicit DPT_9_024();

    std::string unit() const override;
};

using DPT_Power = DPT_9_024;

}
