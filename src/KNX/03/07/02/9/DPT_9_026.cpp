// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/9/DPT_9_026.h>

namespace KNX {

DPT_9_026::DPT_9_026() :
    DPT_9(26)
{
}

std::string DPT_9_026::unit() const
{
    return " l/m²";
}

}
