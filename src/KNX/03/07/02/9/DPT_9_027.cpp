// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/9/DPT_9_027.h>

namespace KNX {

DPT_9_027::DPT_9_027() :
    DPT_9(27)
{
}

std::string DPT_9_027::unit() const
{
    return " °F";
}

}
