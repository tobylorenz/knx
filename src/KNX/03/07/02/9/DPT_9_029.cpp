// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/9/DPT_9_029.h>

namespace KNX {

DPT_9_029::DPT_9_029() :
    DPT_9(29)
{
}

std::string DPT_9_029::unit() const
{
    return " g/m³";
}

}
