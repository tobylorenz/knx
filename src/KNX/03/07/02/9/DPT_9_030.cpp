// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/9/DPT_9_030.h>

namespace KNX {

DPT_9_030::DPT_9_030() :
    DPT_9(30)
{
}

std::string DPT_9_030::unit() const
{
    return " µg/m³";
}

}
