// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/Datapoint_Type.h>

#include <KNX/03/07/02_Datapoint_Types.h>

namespace KNX {

Datapoint_Type::Datapoint_Type(const uint16_t main_number, const uint16_t subnumber) :
    main_number(main_number),
    subnumber(subnumber)
{
}

std::shared_ptr<Datapoint_Type> make_Datapoint_Type(const uint16_t main_number, const uint16_t sub_number)
{
    std::shared_ptr<Datapoint_Type> datapoint_type{};
    switch (main_number) {
    case 1:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_1_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_1_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_1_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_1_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_1_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_1_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_1_007>();
            break;
        case 8:
            datapoint_type = std::make_shared<DPT_1_008>();
            break;
        case 9:
            datapoint_type = std::make_shared<DPT_1_009>();
            break;
        case 10:
            datapoint_type = std::make_shared<DPT_1_010>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_1_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_1_012>();
            break;
        case 13:
            datapoint_type = std::make_shared<DPT_1_013>();
            break;
        case 14:
            datapoint_type = std::make_shared<DPT_1_014>();
            break;
        case 15:
            datapoint_type = std::make_shared<DPT_1_015>();
            break;
        case 16:
            datapoint_type = std::make_shared<DPT_1_016>();
            break;
        case 17:
            datapoint_type = std::make_shared<DPT_1_017>();
            break;
        case 18:
            datapoint_type = std::make_shared<DPT_1_018>();
            break;
        case 19:
            datapoint_type = std::make_shared<DPT_1_019>();
            break;
        case 21:
            datapoint_type = std::make_shared<DPT_1_021>();
            break;
        case 22:
            datapoint_type = std::make_shared<DPT_1_022>();
            break;
        case 23:
            datapoint_type = std::make_shared<DPT_1_023>();
            break;
        case 24:
            datapoint_type = std::make_shared<DPT_1_024>();
            break;
        case 100:
            datapoint_type = std::make_shared<DPT_1_100>();
            break;
        case 1200:
            datapoint_type = std::make_shared<DPT_1_1200>();
            break;
        case 1201:
            datapoint_type = std::make_shared<DPT_1_1201>();
            break;
        default:
            break;
        }
        break;
    case 2:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_2_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_2_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_2_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_2_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_2_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_2_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_2_007>();
            break;
        case 8:
            datapoint_type = std::make_shared<DPT_2_008>();
            break;
        case 9:
            datapoint_type = std::make_shared<DPT_2_009>();
            break;
        case 10:
            datapoint_type = std::make_shared<DPT_2_010>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_2_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_2_012>();
            break;
        default:
            break;
        }
        break;
    case 3:
        switch (sub_number) {
        case 7:
            datapoint_type = std::make_shared<DPT_3_007>();
            break;
        case 8:
            datapoint_type = std::make_shared<DPT_3_008>();
            break;
        default:
            break;
        }
        break;
    case 4:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_4_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_4_002>();
            break;
        default:
            break;
        }
        break;
    case 5:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_5_001>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_5_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_5_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_5_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_5_006>();
            break;
        case 10:
            datapoint_type = std::make_shared<DPT_5_010>();
            break;
        case 100:
            datapoint_type = std::make_shared<DPT_5_100>();
            break;
        default:
            break;
        }
        break;
    case 6:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_6_001>();
            break;
        case 10:
            datapoint_type = std::make_shared<DPT_6_010>();
            break;
        case 20:
            datapoint_type = std::make_shared<DPT_6_020>();
            break;
        default:
            break;
        }
        break;
    case 7:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_7_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_7_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_7_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_7_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_7_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_7_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_7_007>();
            break;
        case 10:
            datapoint_type = std::make_shared<DPT_7_010>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_7_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_7_012>();
            break;
        case 13:
            datapoint_type = std::make_shared<DPT_7_013>();
            break;
        case 600:
            datapoint_type = std::make_shared<DPT_7_600>();
            break;
        default:
            break;
        }
        break;
    case 8:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_8_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_8_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_8_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_8_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_8_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_8_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_8_007>();
            break;
        case 10:
            datapoint_type = std::make_shared<DPT_8_010>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_8_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_8_012>();
            break;
        default:
            break;
        }
        break;
    case 9:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_9_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_9_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_9_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_9_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_9_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_9_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_9_007>();
            break;
        case 8:
            datapoint_type = std::make_shared<DPT_9_008>();
            break;
        case 9:
            datapoint_type = std::make_shared<DPT_9_009>();
            break;
        case 10:
            datapoint_type = std::make_shared<DPT_9_010>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_9_011>();
            break;
        case 20:
            datapoint_type = std::make_shared<DPT_9_020>();
            break;
        case 21:
            datapoint_type = std::make_shared<DPT_9_021>();
            break;
        case 22:
            datapoint_type = std::make_shared<DPT_9_022>();
            break;
        case 23:
            datapoint_type = std::make_shared<DPT_9_023>();
            break;
        case 24:
            datapoint_type = std::make_shared<DPT_9_024>();
            break;
        case 25:
            datapoint_type = std::make_shared<DPT_9_025>();
            break;
        case 26:
            datapoint_type = std::make_shared<DPT_9_026>();
            break;
        case 27:
            datapoint_type = std::make_shared<DPT_9_027>();
            break;
        case 28:
            datapoint_type = std::make_shared<DPT_9_028>();
            break;
        case 29:
            datapoint_type = std::make_shared<DPT_9_029>();
            break;
        case 30:
            datapoint_type = std::make_shared<DPT_9_030>();
            break;
        default:
            break;
        }
        break;
    case 10:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_10_001>();
            break;
        default:
            break;
        }
        break;
    case 11:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_11_001>();
            break;
        default:
            break;
        }
        break;
    case 12:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_12_001>();
            break;
        case 100:
            datapoint_type = std::make_shared<DPT_12_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_12_101>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_12_102>();
            break;
        case 1200:
            datapoint_type = std::make_shared<DPT_12_1200>();
            break;
        case 1201:
            datapoint_type = std::make_shared<DPT_12_1201>();
            break;
        default:
            break;
        }
        break;
    case 13:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_13_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_13_002>();
            break;
        case 10:
            datapoint_type = std::make_shared<DPT_13_010>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_13_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_13_012>();
            break;
        case 13:
            datapoint_type = std::make_shared<DPT_13_013>();
            break;
        case 14:
            datapoint_type = std::make_shared<DPT_13_014>();
            break;
        case 15:
            datapoint_type = std::make_shared<DPT_13_015>();
            break;
        case 16:
            datapoint_type = std::make_shared<DPT_13_016>();
            break;
        case 100:
            datapoint_type = std::make_shared<DPT_13_100>();
            break;
        case 1200:
            datapoint_type = std::make_shared<DPT_13_1200>();
            break;
        case 1201:
            datapoint_type = std::make_shared<DPT_13_1201>();
            break;
        default:
            break;
        }
        break;
    case 14:
        switch (sub_number) {
        case 0:
            datapoint_type = std::make_shared<DPT_14_000>();
            break;
        case 1:
            datapoint_type = std::make_shared<DPT_14_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_14_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_14_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_14_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_14_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_14_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_14_007>();
            break;
        case 8:
            datapoint_type = std::make_shared<DPT_14_008>();
            break;
        case 9:
            datapoint_type = std::make_shared<DPT_14_009>();
            break;
        case 10:
            datapoint_type = std::make_shared<DPT_14_010>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_14_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_14_012>();
            break;
        case 13:
            datapoint_type = std::make_shared<DPT_14_013>();
            break;
        case 14:
            datapoint_type = std::make_shared<DPT_14_014>();
            break;
        case 15:
            datapoint_type = std::make_shared<DPT_14_015>();
            break;
        case 16:
            datapoint_type = std::make_shared<DPT_14_016>();
            break;
        case 17:
            datapoint_type = std::make_shared<DPT_14_017>();
            break;
        case 18:
            datapoint_type = std::make_shared<DPT_14_018>();
            break;
        case 19:
            datapoint_type = std::make_shared<DPT_14_019>();
            break;
        case 20:
            datapoint_type = std::make_shared<DPT_14_020>();
            break;
        case 21:
            datapoint_type = std::make_shared<DPT_14_021>();
            break;
        case 22:
            datapoint_type = std::make_shared<DPT_14_022>();
            break;
        case 23:
            datapoint_type = std::make_shared<DPT_14_023>();
            break;
        case 24:
            datapoint_type = std::make_shared<DPT_14_024>();
            break;
        case 25:
            datapoint_type = std::make_shared<DPT_14_025>();
            break;
        case 26:
            datapoint_type = std::make_shared<DPT_14_026>();
            break;
        case 27:
            datapoint_type = std::make_shared<DPT_14_027>();
            break;
        case 28:
            datapoint_type = std::make_shared<DPT_14_028>();
            break;
        case 29:
            datapoint_type = std::make_shared<DPT_14_029>();
            break;
        case 30:
            datapoint_type = std::make_shared<DPT_14_030>();
            break;
        case 31:
            datapoint_type = std::make_shared<DPT_14_031>();
            break;
        case 32:
            datapoint_type = std::make_shared<DPT_14_032>();
            break;
        case 33:
            datapoint_type = std::make_shared<DPT_14_033>();
            break;
        case 34:
            datapoint_type = std::make_shared<DPT_14_034>();
            break;
        case 35:
            datapoint_type = std::make_shared<DPT_14_035>();
            break;
        case 36:
            datapoint_type = std::make_shared<DPT_14_036>();
            break;
        case 37:
            datapoint_type = std::make_shared<DPT_14_037>();
            break;
        case 38:
            datapoint_type = std::make_shared<DPT_14_038>();
            break;
        case 39:
            datapoint_type = std::make_shared<DPT_14_039>();
            break;
        case 40:
            datapoint_type = std::make_shared<DPT_14_040>();
            break;
        case 41:
            datapoint_type = std::make_shared<DPT_14_041>();
            break;
        case 42:
            datapoint_type = std::make_shared<DPT_14_042>();
            break;
        case 43:
            datapoint_type = std::make_shared<DPT_14_043>();
            break;
        case 44:
            datapoint_type = std::make_shared<DPT_14_044>();
            break;
        case 45:
            datapoint_type = std::make_shared<DPT_14_045>();
            break;
        case 46:
            datapoint_type = std::make_shared<DPT_14_046>();
            break;
        case 47:
            datapoint_type = std::make_shared<DPT_14_047>();
            break;
        case 48:
            datapoint_type = std::make_shared<DPT_14_048>();
            break;
        case 49:
            datapoint_type = std::make_shared<DPT_14_049>();
            break;
        case 50:
            datapoint_type = std::make_shared<DPT_14_050>();
            break;
        case 51:
            datapoint_type = std::make_shared<DPT_14_051>();
            break;
        case 52:
            datapoint_type = std::make_shared<DPT_14_052>();
            break;
        case 53:
            datapoint_type = std::make_shared<DPT_14_053>();
            break;
        case 54:
            datapoint_type = std::make_shared<DPT_14_054>();
            break;
        case 55:
            datapoint_type = std::make_shared<DPT_14_055>();
            break;
        case 56:
            datapoint_type = std::make_shared<DPT_14_056>();
            break;
        case 57:
            datapoint_type = std::make_shared<DPT_14_057>();
            break;
        case 58:
            datapoint_type = std::make_shared<DPT_14_058>();
            break;
        case 59:
            datapoint_type = std::make_shared<DPT_14_059>();
            break;
        case 60:
            datapoint_type = std::make_shared<DPT_14_060>();
            break;
        case 61:
            datapoint_type = std::make_shared<DPT_14_061>();
            break;
        case 62:
            datapoint_type = std::make_shared<DPT_14_062>();
            break;
        case 63:
            datapoint_type = std::make_shared<DPT_14_063>();
            break;
        case 64:
            datapoint_type = std::make_shared<DPT_14_064>();
            break;
        case 65:
            datapoint_type = std::make_shared<DPT_14_065>();
            break;
        case 66:
            datapoint_type = std::make_shared<DPT_14_066>();
            break;
        case 67:
            datapoint_type = std::make_shared<DPT_14_067>();
            break;
        case 68:
            datapoint_type = std::make_shared<DPT_14_068>();
            break;
        case 69:
            datapoint_type = std::make_shared<DPT_14_069>();
            break;
        case 70:
            datapoint_type = std::make_shared<DPT_14_070>();
            break;
        case 71:
            datapoint_type = std::make_shared<DPT_14_071>();
            break;
        case 72:
            datapoint_type = std::make_shared<DPT_14_072>();
            break;
        case 73:
            datapoint_type = std::make_shared<DPT_14_073>();
            break;
        case 74:
            datapoint_type = std::make_shared<DPT_14_074>();
            break;
        case 75:
            datapoint_type = std::make_shared<DPT_14_075>();
            break;
        case 76:
            datapoint_type = std::make_shared<DPT_14_076>();
            break;
        case 77:
            datapoint_type = std::make_shared<DPT_14_077>();
            break;
        case 78:
            datapoint_type = std::make_shared<DPT_14_078>();
            break;
        case 79:
            datapoint_type = std::make_shared<DPT_14_079>();
            break;
        case 80:
            datapoint_type = std::make_shared<DPT_14_080>();
            break;
        case 1200:
            datapoint_type = std::make_shared<DPT_14_1200>();
            break;
        case 1201:
            datapoint_type = std::make_shared<DPT_14_1201>();
            break;
        default:
            break;
        }
        break;
    case 15:
        switch (sub_number) {
        case 0:
            datapoint_type = std::make_shared<DPT_15_000>();
            break;
        default:
            break;
        }
        break;
    case 16:
        switch (sub_number) {
        case 0:
            datapoint_type = std::make_shared<DPT_16_000>();
            break;
        case 1:
            datapoint_type = std::make_shared<DPT_16_001>();
            break;
        default:
            break;
        }
        break;
    case 17:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_17_001>();
            break;
        default:
            break;
        }
        break;
    case 18:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_18_001>();
            break;
        default:
            break;
        }
        break;
    case 19:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_19_001>();
            break;
        default:
            break;
        }
        break;
    case 20:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_20_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_20_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_20_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_20_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_20_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_20_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_20_007>();
            break;
        case 8:
            datapoint_type = std::make_shared<DPT_20_008>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_20_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_20_012>();
            break;
        case 13:
            datapoint_type = std::make_shared<DPT_20_013>();
            break;
        case 14:
            datapoint_type = std::make_shared<DPT_20_014>();
            break;
        case 17:
            datapoint_type = std::make_shared<DPT_20_017>();
            break;
        case 20:
            datapoint_type = std::make_shared<DPT_20_020>();
            break;
        case 21:
            datapoint_type = std::make_shared<DPT_20_021>();
            break;
        case 22:
            datapoint_type = std::make_shared<DPT_20_022>();
            break;
        case 100:
            datapoint_type = std::make_shared<DPT_20_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_20_101>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_20_102>();
            break;
        case 103:
            datapoint_type = std::make_shared<DPT_20_103>();
            break;
        case 104:
            datapoint_type = std::make_shared<DPT_20_104>();
            break;
        case 105:
            datapoint_type = std::make_shared<DPT_20_105>();
            break;
        case 106:
            datapoint_type = std::make_shared<DPT_20_106>();
            break;
        case 107:
            datapoint_type = std::make_shared<DPT_20_107>();
            break;
        case 108:
            datapoint_type = std::make_shared<DPT_20_108>();
            break;
        case 109:
            datapoint_type = std::make_shared<DPT_20_109>();
            break;
        case 110:
            datapoint_type = std::make_shared<DPT_20_110>();
            break;
        case 111:
            datapoint_type = std::make_shared<DPT_20_111>();
            break;
        case 112:
            datapoint_type = std::make_shared<DPT_20_112>();
            break;
        case 113:
            datapoint_type = std::make_shared<DPT_20_113>();
            break;
        case 114:
            datapoint_type = std::make_shared<DPT_20_114>();
            break;
        case 115:
            datapoint_type = std::make_shared<DPT_20_115>();
            break;
        case 116:
            datapoint_type = std::make_shared<DPT_20_116>();
            break;
        case 120:
            datapoint_type = std::make_shared<DPT_20_120>();
            break;
        case 121:
            datapoint_type = std::make_shared<DPT_20_121>();
            break;
        case 122:
            datapoint_type = std::make_shared<DPT_20_122>();
            break;
        case 600:
            datapoint_type = std::make_shared<DPT_20_600>();
            break;
        case 601:
            datapoint_type = std::make_shared<DPT_20_601>();
            break;
        case 602:
            datapoint_type = std::make_shared<DPT_20_602>();
            break;
        case 603:
            datapoint_type = std::make_shared<DPT_20_603>();
            break;
        case 604:
            datapoint_type = std::make_shared<DPT_20_604>();
            break;
        case 605:
            datapoint_type = std::make_shared<DPT_20_605>();
            break;
        case 606:
            datapoint_type = std::make_shared<DPT_20_606>();
            break;
        case 607:
            datapoint_type = std::make_shared<DPT_20_607>();
            break;
        case 608:
            datapoint_type = std::make_shared<DPT_20_608>();
            break;
        case 609:
            datapoint_type = std::make_shared<DPT_20_609>();
            break;
        case 610:
            datapoint_type = std::make_shared<DPT_20_610>();
            break;
        case 611:
            datapoint_type = std::make_shared<DPT_20_611>();
            break;
        case 612:
            datapoint_type = std::make_shared<DPT_20_612>();
            break;
        case 613:
            datapoint_type = std::make_shared<DPT_20_613>();
            break;
        case 801:
            datapoint_type = std::make_shared<DPT_20_801>();
            break;
        case 802:
            datapoint_type = std::make_shared<DPT_20_802>();
            break;
        case 803:
            datapoint_type = std::make_shared<DPT_20_803>();
            break;
        case 804:
            datapoint_type = std::make_shared<DPT_20_804>();
            break;
        case 1000:
            datapoint_type = std::make_shared<DPT_20_1000>();
            break;
        case 1001:
            datapoint_type = std::make_shared<DPT_20_1001>();
            break;
        case 1002:
            datapoint_type = std::make_shared<DPT_20_1002>();
            break;
        case 1003:
            datapoint_type = std::make_shared<DPT_20_1003>();
            break;
        case 1004:
            datapoint_type = std::make_shared<DPT_20_1004>();
            break;
        case 1005:
            datapoint_type = std::make_shared<DPT_20_1005>();
            break;
        case 1200:
            datapoint_type = std::make_shared<DPT_20_1200>();
            break;
        case 1202:
            datapoint_type = std::make_shared<DPT_20_1202>();
            break;
        case 1203:
            datapoint_type = std::make_shared<DPT_20_1203>();
            break;
        case 1204:
            datapoint_type = std::make_shared<DPT_20_1204>();
            break;
        case 1205:
            datapoint_type = std::make_shared<DPT_20_1205>();
            break;
        case 1206:
            datapoint_type = std::make_shared<DPT_20_1206>();
            break;
        case 1207:
            datapoint_type = std::make_shared<DPT_20_1207>();
            break;
        case 1208:
            datapoint_type = std::make_shared<DPT_20_1208>();
            break;
        case 1209:
            datapoint_type = std::make_shared<DPT_20_1209>();
            break;
        default:
            break;
        }
        break;
    case 21:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_21_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_21_002>();
            break;
        case 100:
            datapoint_type = std::make_shared<DPT_21_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_21_101>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_21_102>();
            break;
        case 103:
            datapoint_type = std::make_shared<DPT_21_103>();
            break;
        case 104:
            datapoint_type = std::make_shared<DPT_21_104>();
            break;
        case 105:
            datapoint_type = std::make_shared<DPT_21_105>();
            break;
        case 106:
            datapoint_type = std::make_shared<DPT_21_106>();
            break;
        case 107:
            datapoint_type = std::make_shared<DPT_21_107>();
            break;
        case 601:
            datapoint_type = std::make_shared<DPT_21_601>();
            break;
        case 1000:
            datapoint_type = std::make_shared<DPT_21_1000>();
            break;
        case 1001:
            datapoint_type = std::make_shared<DPT_21_1001>();
            break;
        case 1002:
            datapoint_type = std::make_shared<DPT_21_1002>();
            break;
        case 1010:
            datapoint_type = std::make_shared<DPT_21_1010>();
            break;
        case 1200:
            datapoint_type = std::make_shared<DPT_21_1200>();
            break;
        case 1201:
            datapoint_type = std::make_shared<DPT_21_1201>();
            break;
        default:
            break;
        }
        break;
    case 22:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_22_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_22_101>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_22_102>();
            break;
        case 103:
            datapoint_type = std::make_shared<DPT_22_103>();
            break;
        case 1000:
            datapoint_type = std::make_shared<DPT_22_1000>();
            break;
        case 1010:
            datapoint_type = std::make_shared<DPT_22_1010>();
            break;
        default:
            break;
        }
        break;
    case 23:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_23_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_23_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_23_003>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_23_102>();
            break;
        default:
            break;
        }
        break;
    case 24:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_24_001>();
            break;
        default:
            break;
        }
        break;
    case 25:
        switch (sub_number) {
        case 1000:
            datapoint_type = std::make_shared<DPT_25_1000>();
            break;
        default:
            break;
        }
        break;
    case 26:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_26_001>();
            break;
        default:
            break;
        }
        break;
    case 27:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_27_001>();
            break;
        default:
            break;
        }
        break;
    case 28:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_28_001>();
            break;
        default:
            break;
        }
        break;
    case 29:
        switch (sub_number) {
        case 10:
            datapoint_type = std::make_shared<DPT_29_010>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_29_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_29_012>();
            break;
        default:
            break;
        }
        break;
    case 30:
        switch (sub_number) {
        case 1010:
            datapoint_type = std::make_shared<DPT_30_1010>();
            break;
        default:
            break;
        }
        break;
    case 31:
        switch (sub_number) {
        case 101:
            datapoint_type = std::make_shared<DPT_31_101>();
            break;
        default:
            break;
        }
        break;
    case 200:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_200_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_200_101>();
            break;
        default:
            break;
        }
        break;
    case 201:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_201_100>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_201_102>();
            break;
        case 104:
            datapoint_type = std::make_shared<DPT_201_104>();
            break;
        case 105:
            datapoint_type = std::make_shared<DPT_201_105>();
            break;
        case 107:
            datapoint_type = std::make_shared<DPT_201_107>();
            break;
        case 108:
            datapoint_type = std::make_shared<DPT_201_108>();
            break;
        case 109:
            datapoint_type = std::make_shared<DPT_201_109>();
            break;
        default:
            break;
        }
        break;
    case 202:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_202_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_202_002>();
            break;
        default:
            break;
        }
        break;
    case 203:
        switch (sub_number) {
        case 2:
            datapoint_type = std::make_shared<DPT_203_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_203_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_203_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_203_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_203_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_203_007>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_203_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_203_012>();
            break;
        case 13:
            datapoint_type = std::make_shared<DPT_203_013>();
            break;
        case 14:
            datapoint_type = std::make_shared<DPT_203_014>();
            break;
        case 15:
            datapoint_type = std::make_shared<DPT_203_015>();
            break;
        case 17:
            datapoint_type = std::make_shared<DPT_203_017>();
            break;
        case 100:
            datapoint_type = std::make_shared<DPT_203_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_203_101>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_203_102>();
            break;
        case 104:
            datapoint_type = std::make_shared<DPT_203_104>();
            break;
        default:
            break;
        }
        break;
    case 204:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_204_001>();
            break;
        default:
            break;
        }
        break;
    case 205:
        switch (sub_number) {
        case 2:
            datapoint_type = std::make_shared<DPT_205_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_205_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_205_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_205_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_205_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_205_007>();
            break;
        case 17:
            datapoint_type = std::make_shared<DPT_205_017>();
            break;
        case 100:
            datapoint_type = std::make_shared<DPT_205_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_205_101>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_205_102>();
            break;
        case 103:
            datapoint_type = std::make_shared<DPT_205_103>();
            break;
        default:
            break;
        }
        break;
    case 206:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_206_100>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_206_102>();
            break;
        case 104:
            datapoint_type = std::make_shared<DPT_206_104>();
            break;
        case 105:
            datapoint_type = std::make_shared<DPT_206_105>();
            break;
        default:
            break;
        }
        break;
    case 207:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_207_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_207_101>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_207_102>();
            break;
        case 104:
            datapoint_type = std::make_shared<DPT_207_104>();
            break;
        case 105:
            datapoint_type = std::make_shared<DPT_207_105>();
            break;
        case 600:
            datapoint_type = std::make_shared<DPT_207_600>();
            break;
        default:
            break;
        }
        break;
    case 209:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_209_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_209_101>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_209_102>();
            break;
        case 103:
            datapoint_type = std::make_shared<DPT_209_103>();
            break;
        default:
            break;
        }
        break;
    case 210:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_210_100>();
            break;
        default:
            break;
        }
        break;
    case 211:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_211_100>();
            break;
        default:
            break;
        }
        break;
    case 212:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_212_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_212_101>();
            break;
        default:
            break;
        }
        break;
    case 213:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_213_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_213_101>();
            break;
        case 102:
            datapoint_type = std::make_shared<DPT_213_102>();
            break;
        default:
            break;
        }
        break;
    case 214:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_214_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_214_101>();
            break;
        default:
            break;
        }
        break;
    case 215:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_215_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_215_101>();
            break;
        default:
            break;
        }
        break;
    case 216:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_216_100>();
            break;
        default:
            break;
        }
        break;
    case 217:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_217_001>();
            break;
        default:
            break;
        }
        break;
    case 218:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_218_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_218_002>();
            break;
        default:
            break;
        }
        break;
    case 219:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_219_001>();
            break;
        default:
            break;
        }
        break;
    case 220:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_220_100>();
            break;
        default:
            break;
        }
        break;
    case 221:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_221_001>();
            break;
        default:
            break;
        }
        break;
    case 222:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_222_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_222_101>();
            break;
        default:
            break;
        }
        break;
    case 223:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_223_100>();
            break;
        default:
            break;
        }
        break;
    case 224:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_224_100>();
            break;
        default:
            break;
        }
        break;
    case 225:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_225_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_225_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_225_003>();
            break;
        default:
            break;
        }
        break;
    case 228:
        switch (sub_number) {
        case 1000:
            datapoint_type = std::make_shared<DPT_228_1000>();
            break;
        default:
            break;
        }
        break;
    case 229:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_229_001>();
            break;
        default:
            break;
        }
        break;
    case 230:
        switch (sub_number) {
        case 1000:
            datapoint_type = std::make_shared<DPT_230_1000>();
            break;
        default:
            break;
        }
        break;
    case 231:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_231_001>();
            break;
        default:
            break;
        }
        break;
    case 232:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_232_600>();
            break;
        default:
            break;
        }
        break;
    case 234:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_234_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_234_002>();
            break;
        default:
            break;
        }
        break;
    case 235:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_235_001>();
            break;
        default:
            break;
        }
        break;
    case 236:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_236_001>();
            break;
        default:
            break;
        }
        break;
    case 237:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_237_600>();
            break;
        default:
            break;
        }
        break;
    case 238:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_238_001>();
            break;
        case 600:
            datapoint_type = std::make_shared<DPT_238_600>();
            break;
        default:
            break;
        }
        break;
    case 239:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_239_001>();
            break;
        default:
            break;
        }
        break;
    case 240:
        switch (sub_number) {
        case 800:
            datapoint_type = std::make_shared<DPT_240_800>();
            break;
        default:
            break;
        }
        break;
    case 241:
        switch (sub_number) {
        case 800:
            datapoint_type = std::make_shared<DPT_241_800>();
            break;
        default:
            break;
        }
        break;
    case 242:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_242_600>();
            break;
        default:
            break;
        }
        break;
    case 243:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_243_600>();
            break;
        default:
            break;
        }
        break;
    case 244:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_244_600>();
            break;
        default:
            break;
        }
        break;
    case 245:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_245_600>();
            break;
        default:
            break;
        }
        break;
    case 246:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_246_600>();
            break;
        default:
            break;
        }
        break;
    case 247:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_247_600>();
            break;
        default:
            break;
        }
        break;
    case 248:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_248_600>();
            break;
        default:
            break;
        }
        break;
    case 249:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_249_600>();
            break;
        default:
            break;
        }
        break;
    case 250:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_250_600>();
            break;
        default:
            break;
        }
        break;
    case 251:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_251_600>();
            break;
        default:
            break;
        }
        break;
    case 252:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_252_600>();
            break;
        default:
            break;
        }
        break;
    case 253:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_253_600>();
            break;
        default:
            break;
        }
        break;
    case 254:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_254_600>();
            break;
        default:
            break;
        }
        break;
    case 255:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_255_001>();
            break;
        default:
            break;
        }
        break;
    case 256:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_256_001>();
            break;
        default:
            break;
        }
        break;
    case 257:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_257_1200>();
            break;
        case 1201:
            datapoint_type = std::make_shared<DPT_257_1201>();
            break;
        case 1202:
            datapoint_type = std::make_shared<DPT_257_1202>();
            break;
        default:
            break;
        }
        break;
    case 265:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_265_001>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_265_005>();
            break;
        case 9:
            datapoint_type = std::make_shared<DPT_265_009>();
            break;
        case 11:
            datapoint_type = std::make_shared<DPT_265_011>();
            break;
        case 12:
            datapoint_type = std::make_shared<DPT_265_012>();
            break;
        case 1200:
            datapoint_type = std::make_shared<DPT_265_1200>();
            break;
        case 1201:
            datapoint_type = std::make_shared<DPT_265_1201>();
            break;
        default:
            break;
        }
        break;
    case 266:
        switch (sub_number) {
        case 27:
            datapoint_type = std::make_shared<DPT_266_027>();
            break;
        case 56:
            datapoint_type = std::make_shared<DPT_266_056>();
            break;
        case 80:
            datapoint_type = std::make_shared<DPT_266_080>();
            break;
        default:
            break;
        }
        break;
    case 267:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_267_001>();
            break;
        default:
            break;
        }
        break;
    case 268:
        switch (sub_number) {
        case 1203:
            datapoint_type = std::make_shared<DPT_268_1203>();
            break;
        case 1204:
            datapoint_type = std::make_shared<DPT_268_1204>();
            break;
        case 1205:
            datapoint_type = std::make_shared<DPT_268_1205>();
            break;
        case 1206:
            datapoint_type = std::make_shared<DPT_268_1206>();
            break;
        default:
            break;
        }
        break;
    case 269:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_269_1200>();
            break;
        default:
            break;
        }
        break;
    case 270:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_270_1200>();
            break;
        case 1201:
            datapoint_type = std::make_shared<DPT_270_1201>();
            break;
        case 1202:
            datapoint_type = std::make_shared<DPT_270_1202>();
            break;
        default:
            break;
        }
        break;
    case 271:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_271_1200>();
            break;
        default:
            break;
        }
        break;
    case 272:
        switch (sub_number) {
        case 600:
            datapoint_type = std::make_shared<DPT_272_600>();
            break;
        default:
            break;
        }
        break;
    case 273:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_273_001>();
            break;
        case 2:
            datapoint_type = std::make_shared<DPT_273_002>();
            break;
        case 3:
            datapoint_type = std::make_shared<DPT_273_003>();
            break;
        case 4:
            datapoint_type = std::make_shared<DPT_273_004>();
            break;
        case 5:
            datapoint_type = std::make_shared<DPT_273_005>();
            break;
        case 6:
            datapoint_type = std::make_shared<DPT_273_006>();
            break;
        case 7:
            datapoint_type = std::make_shared<DPT_273_007>();
            break;
        default:
            break;
        }
        break;
    case 274:
        switch (sub_number) {
        case 1:
            datapoint_type = std::make_shared<DPT_274_001>();
            break;
        default:
            break;
        }
        break;
    case 275:
        switch (sub_number) {
        case 100:
            datapoint_type = std::make_shared<DPT_275_100>();
            break;
        case 101:
            datapoint_type = std::make_shared<DPT_275_101>();
            break;
        default:
            break;
        }
        break;
    case 276:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_276_1200>();
            break;
        default:
            break;
        }
        break;
    case 277:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_277_1200>();
            break;
        default:
            break;
        }
        break;
    case 278:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_278_1200>();
            break;
        default:
            break;
        }
        break;
    case 279:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_279_1200>();
            break;
        default:
            break;
        }
        break;
    case 280:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_280_1200>();
            break;
        default:
            break;
        }
        break;
    case 281:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_281_1200>();
            break;
        default:
            break;
        }
        break;
    case 282:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_282_1200>();
            break;
        default:
            break;
        }
        break;
    case 283:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_283_1200>();
            break;
        default:
            break;
        }
        break;
    case 284:
        switch (sub_number) {
        case 1200:
            datapoint_type = std::make_shared<DPT_284_1200>();
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
    return datapoint_type;
}

}
