// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <string>
#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Base class for
 * - KNX (Property) Data Type = ETS DatapointType (DPT) = Format and Encoding
 * - KNX Datapoint Type = ETS DatapointSubtype (DPST) = Range and Unit
 *
 * @ingroup KNX_03_07_02_01_01
 * @ingroup KNX_03_07_03_05
 */
class KNX_EXPORT Datapoint_Type
{
public:
    explicit Datapoint_Type(const uint16_t main_number, const uint16_t subnumber);

    /* Format and Encoding */

    /**
     * Main Number (according to KNX spec)
     *
     * @note In ETS project file this is DatapointType Number.
     */
    const uint16_t main_number{};

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) = 0;
    virtual std::vector<uint8_t> toData() const = 0;

    /* Range and Unit */

    /**
     * Subnumber (according to KNX spec)
     *
     * @note In ETS project file this is DatapointSubtype Number.
     */
    const uint16_t subnumber{};

    /** get value as text */
    virtual std::string text() const = 0;
};

KNX_EXPORT std::shared_ptr<Datapoint_Type> make_Datapoint_Type(const uint16_t main_number, const uint16_t sub_number);

}
