// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/General_Command.h>

#include <cassert>
#include <sstream>

#include <KNX/Exceptions.h>

namespace KNX {

General_Command::General_Command(const uint8_t command) :
    command(static_cast<Command>(command))
{
}

General_Command & General_Command::operator=(const uint8_t & command)
{
    this->command = static_cast<Command>(command);

    return *this;
}

General_Command::operator uint8_t() const
{
    return static_cast<uint8_t>(command);
}

void General_Command::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    command = static_cast<Command>(*first++);

    assert(first == last);
}

std::vector<uint8_t> General_Command::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(static_cast<uint8_t>(command));

    return data;
}

std::string General_Command::text() const
{
    std::ostringstream oss;
    switch (command) {
    case Command::NormalWrite:
        oss << "NormalWrite";
        break;
    case Command::Override:
        oss << "Override";
        break;
    case Command::Release:
        oss << "Release";
        break;
    case Command::SetOSV:
        oss << "SetOSV";
        break;
    case Command::ResetOSV:
        oss << "ResetOSV";
        break;
    case Command::AlarmAck:
        oss << "AlarmAck";
        break;
    case Command::SetToDefault:
        oss << "SetToDefault";
        break;
    }
    return oss.str();
}

}
