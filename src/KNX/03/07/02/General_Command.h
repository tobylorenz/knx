// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <string>
#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * General Command
 *
 * @ingroup KNX_03_07_02_04_01
 */
class KNX_EXPORT General_Command
{
public:
    General_Command() = default;
    explicit General_Command(const uint8_t command);
    General_Command & operator=(const uint8_t & command);
    operator uint8_t() const;
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;
    virtual std::string text() const;

    /** Command */
    enum Command : uint8_t {
        NormalWrite = 0,
        Override = 1,
        Release = 2,
        SetOSV = 3,
        ResetOSV = 4,
        AlarmAck = 5,
        SetToDefault = 6,
        // reserved = 7..255
    };

    /** @copydoc Command */
    Command command{};
};

}
