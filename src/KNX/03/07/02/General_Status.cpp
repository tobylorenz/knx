// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/General_Status.h>

#include <cassert>
#include <sstream>

#include <KNX/Exceptions.h>

namespace KNX {

General_Status::General_Status(const uint8_t status) :
    out_of_service((status >> 0) & 0x01),
    fault((status >> 1) & 0x01),
    overridden((status >> 2) & 0x01),
    in_alarm((status >> 3) & 0x01),
    alarm_un_ack((status >> 4) & 0x01)
{
}

General_Status & General_Status::operator=(const uint8_t & status)
{
    out_of_service = (status >> 0) & 0x01;
    fault = (status >> 1) & 0x01;
    overridden = (status >> 2) & 0x01;
    in_alarm = (status >> 3) & 0x01;
    alarm_un_ack = (status >> 4) & 0x01;

    return *this;
}

General_Status::operator uint8_t() const
{
    return (out_of_service << 0) |
           (fault << 1) |
           (overridden << 2) |
           (in_alarm << 3) |
           (alarm_un_ack << 4);
}

void General_Status::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 1) {
        throw DataDoesntMatchDPTException(1, data_size);
    }

    out_of_service = (*first >> 0) & 0x01;
    fault = (*first >> 1) & 0x01;
    overridden = (*first >> 2) & 0x01;
    in_alarm = (*first >> 3) & 0x01;
    alarm_un_ack = (*first >> 4) & 0x01;
    ++first;

    assert(first == last);
}

std::vector<uint8_t> General_Status::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (out_of_service << 0) |
        (fault << 1) |
        (overridden << 2) |
        (in_alarm << 3) |
        (alarm_un_ack << 4));

    return data;
}

std::string General_Status::text() const
{
    std::ostringstream oss;
    oss << "OutOfService: " << (out_of_service ? "true" : "false")
        << ", Fault: " << (fault ? "true" : "false")
        << ", Overridden: " << (overridden ? "true" : "false")
        << ", InAlarm: " << (in_alarm ? "true" : "false")
        << ", AlarmUnAck: " << (in_alarm ? "unacknowledged" : "acknowledged");
    return oss.str();
}

}
