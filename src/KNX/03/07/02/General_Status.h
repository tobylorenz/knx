// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <string>
#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * General Status
 *
 * @ingroup KNX_03_07_02_03_22_01
 * @ingroup KNX_03_07_02_04_01
 */
class KNX_EXPORT General_Status
{
public:
    General_Status() = default;
    explicit General_Status(const uint8_t status);
    General_Status & operator=(const uint8_t & status);
    operator uint8_t() const;
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;
    virtual std::string text() const;

    /** OutOfService */
    bool out_of_service{false};

    /** Fault */
    bool fault{false};

    /** Overridden */
    bool overridden{false};

    /** InAlarm */
    bool in_alarm{false};

    /** AlarmUnAck */
    bool alarm_un_ack{false};
};

}
