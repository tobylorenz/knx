// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/ISO_8859_1.h>

namespace KNX {

const std::map<unsigned char, std::string> iso_8859_1_to_utf_8 {
    {0x00, "\x00"}, // NUL
    {0x01, "\x01"}, // SOH
    {0x02, "\x02"}, // STX
    {0x03, "\x03"}, // ETX
    {0x04, "\x04"}, // EOT
    {0x05, "\x05"}, // ENQ
    {0x06, "\x06"}, // ACK
    {0x07, "\x07"}, // BEL
    {0x08, "\x08"}, // BS
    {0x09, "\x09"}, // HT
    {0x0a, "\x0a"}, // LF
    {0x0b, "\x0b"}, // VT
    {0x0c, "\x0c"}, // FF
    {0x0d, "\x0d"}, // CR
    {0x0e, "\x0e"}, // SO
    {0x0f, "\x0f"}, // SI
    {0x10, "\x10"}, // DLE
    {0x11, "\x11"}, // DC1
    {0x12, "\x12"}, // DC2
    {0x13, "\x13"}, // DC3
    {0x14, "\x14"}, // DC4
    {0x15, "\x15"}, // NAK
    {0x16, "\x16"}, // SYN
    {0x17, "\x17"}, // ETB
    {0x18, "\x18"}, // CAN
    {0x19, "\x19"}, // EM
    {0x1a, "\x1a"}, // SUB
    {0x1b, "\x1b"}, // ESC
    {0x1c, "\x1c"}, // FS
    {0x1d, "\x1d"}, // GS
    {0x1e, "\x1e"}, // RS
    {0x1f, "\x1f"}, // US
    {0x20, " "},
    {0x21, "!"},
    {0x22, "\""},
    {0x23, "#"},
    {0x24, "$"},
    {0x25, "%"},
    {0x26, "&"},
    {0x27, "'"},
    {0x28, "("},
    {0x29, ")"},
    {0x2a, "*"},
    {0x2b, "+"},
    {0x2c, ","},
    {0x2d, "-"},
    {0x2e, "."},
    {0x2f, "/"},
    {0x30, "0"},
    {0x31, "1"},
    {0x32, "2"},
    {0x33, "3"},
    {0x34, "4"},
    {0x35, "5"},
    {0x36, "6"},
    {0x37, "7"},
    {0x38, "8"},
    {0x39, "9"},
    {0x3a, ":"},
    {0x3b, ";"},
    {0x3c, "<"},
    {0x3d, "="},
    {0x3e, ">"},
    {0x3f, "?"},
    {0x40, "@"},
    {0x41, "A"},
    {0x42, "B"},
    {0x43, "C"},
    {0x44, "D"},
    {0x45, "E"},
    {0x46, "F"},
    {0x47, "G"},
    {0x48, "H"},
    {0x49, "I"},
    {0x4a, "J"},
    {0x4b, "K"},
    {0x4c, "L"},
    {0x4d, "M"},
    {0x4e, "N"},
    {0x4f, "O"},
    {0x50, "P"},
    {0x51, "Q"},
    {0x52, "R"},
    {0x53, "S"},
    {0x54, "T"},
    {0x55, "U"},
    {0x56, "V"},
    {0x57, "W"},
    {0x58, "X"},
    {0x59, "Y"},
    {0x5a, "Z"},
    {0x5b, "["},
    {0x5c, "\\"},
    {0x5d, "]"},
    {0x5e, "^"},
    {0x5f, "_"},
    {0x60, "`"},
    {0x61, "a"},
    {0x62, "b"},
    {0x63, "c"},
    {0x64, "d"},
    {0x65, "e"},
    {0x66, "f"},
    {0x67, "g"},
    {0x68, "h"},
    {0x69, "i"},
    {0x6a, "j"},
    {0x6b, "k"},
    {0x6c, "l"},
    {0x6d, "m"},
    {0x6e, "n"},
    {0x6f, "o"},
    {0x70, "p"},
    {0x71, "q"},
    {0x72, "r"},
    {0x73, "s"},
    {0x74, "t"},
    {0x75, "u"},
    {0x76, "v"},
    {0x77, "w"},
    {0x78, "x"},
    {0x79, "y"},
    {0x7a, "z"},
    {0x7b, "{"},
    {0x7c, "|"},
    {0x7d, "}"},
    {0x7e, "~"},
    {0x7f, ""}, // undefined
    {0x80, ""}, // undefined
    {0x81, ""}, // undefined
    {0x82, ""}, // undefined
    {0x83, ""}, // undefined
    {0x84, ""}, // undefined
    {0x85, ""}, // undefined
    {0x86, ""}, // undefined
    {0x87, ""}, // undefined
    {0x88, ""}, // undefined
    {0x89, ""}, // undefined
    {0x8a, ""}, // undefined
    {0x8b, ""}, // undefined
    {0x8c, ""}, // undefined
    {0x8d, ""}, // undefined
    {0x8e, ""}, // undefined
    {0x8f, ""}, // undefined
    {0x90, ""}, // undefined
    {0x91, ""}, // undefined
    {0x92, ""}, // undefined
    {0x93, ""}, // undefined
    {0x94, ""}, // undefined
    {0x95, ""}, // undefined
    {0x96, ""}, // undefined
    {0x97, ""}, // undefined
    {0x98, ""}, // undefined
    {0x99, ""}, // undefined
    {0x9a, ""}, // undefined
    {0x9b, ""}, // undefined
    {0x9c, ""}, // undefined
    {0x9d, ""}, // undefined
    {0x9e, ""}, // undefined
    {0x9f, ""}, // undefined
    {0xa0, "\xc2\x80"}, // no-break space
    {0xa1, "¡"},
    {0xa2, "¢"},
    {0xa3, "£"},
    {0xa4, "¤"},
    {0xa5, "¥"},
    {0xa6, "¦"},
    {0xa7, "§"},
    {0xa8, "¨"},
    {0xa9, "©"},
    {0xaa, "ª"},
    {0xab, "«"},
    {0xac, "¬"},
    {0xad, "\xc2\xad"}, // soft hyphen ­
    {0xae, "®"},
    {0xaf, "¯"},
    {0xb0, "°"},
    {0xb1, "±"},
    {0xb2, "²"},
    {0xb3, "³"},
    {0xb4, "´"},
    {0xb5, "µ"},
    {0xb6, "¶"},
    {0xb7, "·"},
    {0xb8, "¸"},
    {0xb9, "¹"},
    {0xba, "º"},
    {0xbb, "»"},
    {0xbc, "¼"},
    {0xbd, "½"},
    {0xbe, "¾"},
    {0xbf, "¿"},
    {0xc0, "À"},
    {0xc1, "Á"},
    {0xc2, "Â"},
    {0xc3, "Ã"},
    {0xc4, "Ä"},
    {0xc5, "Å"},
    {0xc6, "Æ"},
    {0xc7, "Ç"},
    {0xc8, "È"},
    {0xc9, "É"},
    {0xca, "Ê"},
    {0xcb, "Ë"},
    {0xcc, "Ì"},
    {0xcd, "Í"},
    {0xce, "Î"},
    {0xcf, "Ï"},
    {0xd0, "Ð"},
    {0xd1, "Ñ"},
    {0xd2, "Ò"},
    {0xd3, "Ó"},
    {0xd4, "Ô"},
    {0xd5, "Õ"},
    {0xd6, "Ö"},
    {0xd7, "×"},
    {0xd8, "Ø"},
    {0xd9, "Ù"},
    {0xda, "Ú"},
    {0xdb, "Û"},
    {0xdc, "Ü"},
    {0xdd, "Ý"},
    {0xde, "Þ"},
    {0xdf, "ß"},
    {0xe0, "à"},
    {0xe1, "á"},
    {0xe2, "â"},
    {0xe3, "ã"},
    {0xe4, "ä"},
    {0xe5, "å"},
    {0xe6, "æ"},
    {0xe7, "ç"},
    {0xe8, "è"},
    {0xe9, "é"},
    {0xea, "ê"},
    {0xeb, "ë"},
    {0xec, "ì"},
    {0xed, "í"},
    {0xee, "î"},
    {0xef, "ï"},
    {0xf0, "ð"},
    {0xf1, "ñ"},
    {0xf2, "ò"},
    {0xf3, "ó"},
    {0xf4, "ô"},
    {0xf5, "õ"},
    {0xf6, "ö"},
    {0xf7, "÷"},
    {0xf8, "ø"},
    {0xf9, "ù"},
    {0xfa, "ú"},
    {0xfb, "û"},
    {0xfc, "ü"},
    {0xfd, "ý"},
    {0xfe, "þ"},
    {0xff, "ÿ"}
};

}
