// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <map>
#include <string>

#include <KNX/knx_export.h>

namespace KNX {

/** maps ISO 8859-1 characters to UTF-8 characters */
KNX_EXPORT extern const std::map<unsigned char, std::string> iso_8859_1_to_utf_8;

}
