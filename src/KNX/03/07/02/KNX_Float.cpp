// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/02/KNX_Float.h>

#include <cassert>
#include <cmath>
#include <limits>

#include <KNX/Exceptions.h>

namespace KNX {

KNX_Float::KNX_Float(const float value)
{
    operator =(value);
}

KNX_Float & KNX_Float::operator=(const float & value)
{
    /* check for NaN */
    if (std::isnan(value)) {
        sign = 0;
        exponent = 0x0f;
        mantissa = 0x7ff;
        return * this;
    }

    /* initially set sign, exponent, mantissa */
    sign = (value < 0);
    int exponent_;
    float float_mantissa = frexp(value * 100, &exponent_);
    assert(float_mantissa > -1.0);
    assert(float_mantissa < 1.0);

    /* normalize exponent and mantissa */
    while ((float_mantissa >= -1024.0) && (float_mantissa < 1024.0) && (exponent_ > 0)) {
        float_mantissa *= 2.0;
        exponent_--;
    }
    assert(float_mantissa >= -2048.0);
    assert(float_mantissa <= 2047.0);
    assert(exponent_ <= 15);

    /* float to int (two's compliment notation) */
    if (sign) {
        mantissa = static_cast<int16_t>(round(float_mantissa + 2048));
    } else {
        mantissa = static_cast<int16_t>(round(float_mantissa));
    }
    exponent = exponent_;

    assert(sign <= 1);
    assert(exponent <= 15);
    assert(mantissa >= 0);
    assert(mantissa <= 2047);
    return *this;
}

KNX_Float::operator double() const
{
    const int16_t sign_mantissa = sign ? (mantissa - 2048) : mantissa;
    return 0.01 * sign_mantissa * pow(2, exponent);
}

void KNX_Float::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    const uint16_t data_size = std::distance(first, last);
    if (data_size != 2) {
        throw DataDoesntMatchDPTException(2, data_size);
    }

    sign = *first >> 7;
    exponent = (*first >> 3) & 0x0f;
    mantissa =
        ((*first++ & 0x07) << 8) |
        (*first++);

    assert(first == last);
}

std::vector<uint8_t> KNX_Float::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(
        (sign << 7) |
        (exponent << 3) |
        (mantissa >> 8));
    data.push_back(mantissa & 0xff);

    return data;
}

bool KNX_Float::invalid() const
{
    return (sign == 0) && (exponent == 0x0f) && (mantissa == 0x7ff);
}

}
