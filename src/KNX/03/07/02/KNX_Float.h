// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * KNX Float
 *
 * Comparison of float values:
 * - double: width=64, sign=1, exponent=11, mantissa=52
 * - float: width=32, sign=1, exponent=8, mantissa=23
 * - KNX: width=16, sign=1, exponent=4, mantissa=11
 */
class KNX_EXPORT KNX_Float
{
public:
    KNX_Float() = default;
    explicit KNX_Float(const float value);
    KNX_Float & operator=(const float & value);
    operator double() const;

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /** 0x7fff (670760.96) stands for "invalid" */
    bool invalid() const;

protected:
    /** sign */
    uint8_t sign{}; // 0..1

    /** exponent */
    uint8_t exponent{}; // 0..15

    /** mantissa */
    int16_t mantissa{}; // -2048 .. 2047 with sign (0 .. 2047 without sign)
};

}
