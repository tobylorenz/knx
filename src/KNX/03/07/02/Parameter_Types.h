// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/07/02/1/DPT_1_001.h>
#include <KNX/03/07/02/1/DPT_1_002.h>
#include <KNX/03/07/02/1/DPT_1_003.h>
#include <KNX/03/07/02/1/DPT_1_008.h>
#include <KNX/03/07/02/1/DPT_1_012.h>
#include <KNX/03/07/02/1/DPT_1_021.h>
#include <KNX/03/07/02/1/DPT_1_022.h>
#include <KNX/03/07/02/1/DPT_1_023.h>
#include <KNX/03/07/02/5/DPT_5_001.h>
#include <KNX/03/07/02/5/DPT_5_003.h>
#include <KNX/03/07/02/5/DPT_5_010.h>
#include <KNX/03/07/02/7/DPT_7_001.h>
#include <KNX/03/07/02/7/DPT_7_005.h>
#include <KNX/03/07/02/7/DPT_7_013.h>
#include <KNX/03/07/02/9/DPT_9_002.h>
#include <KNX/03/07/02/9/DPT_9_004.h>
#include <KNX/03/07/02/13/DPT_13_010.h>
#include <KNX/03/07/02/14/DPT_14_056.h>
#include <KNX/03/07/02/17/DPT_17_001.h>
#include <KNX/03/07/02/19/DPT_19_001.h>
#include <KNX/03/07/02/20/DPT_20_013.h>
#include <KNX/03/07/02/20/DPT_20_102.h>
#include <KNX/03/07/02/20/DPT_20_112.h>
#include <KNX/03/07/02/23/DPT_23_001.h>
#include <KNX/03/07/02/23/DPT_23_002.h>
#include <KNX/03/07/02/23/DPT_23_003.h>
#include <KNX/03/07/02/23/DPT_23_102.h>
#include <KNX/03/07/02/31/DPT_31_101.h>
#include <KNX/03/07/02/228/DPT_228_1000.h>

namespace KNX {

/* 11 Parameter Types */

using PART_Switch_Value = DPT_Switch;
using PART_Boolean = DPT_Bool;
using PART_UpDown_Action = DPT_UpDown;
using PART_Invert = DPT_Invert;
using PART_Logical = DPT_LogicalFunction;
using PART_Scene_Value = DPT_Scene_AB;
using PART_Blind_Mode = DPT_ShutterBlinds_Mode;
using PART_Enable = DPT_Enable;
using PART_Scene_Number = DPT_SceneNumber;
using PART_Date_Time = DPT_DateTime;
using PART_Cycle_Time = DPT_Time_Delay;
using PART_Time_Delay = DPT_Time_Delay;
using PART_Prewarning_Delay = DPT_Time_Delay;
using PART_HVACMode = DPT_HVACMode;
using PART_MasterSlaveMode = DPT_MasterSlaveMode;
using PART_Adaptive_Selection = DPT_Adaptive_Selection;
using PART_OnOff_Action = DPT_OnOffAction;
using PART_Alarm_Reaction = DPT_Alarm_Reaction;
using PART_UpDown_Switch_Action = DPT_UpDown_Action;
using PART_PB_Action_HVAC = DPT_HVAC_PB_Action;
// using PART_UpDown_Action_Extended = DPT_XXX; // 23.xxx
// using PART_Byte_Value = Value; U8
using PART_Dimming_Value = DPT_Scaling;
using PART_Adjustable_Selection = DPT_Value_1_Ucount;
using PART_Shutter_Position = DPT_Scaling;
using PART_Slat_Position = DPT_Scaling;
using PART_Orientation = DPT_Angle;
using PART_Render_Value = DPT_Value_2_Ucount;
using PART_Light_Value = DPT_Brightness;
using PART_Move_UpDown_Time = DPT_TimePeriodSec;
using PART_COV_Lux = DPT_Value_Lux;
using PART_Value_Tempd = DPT_Value_Tempd;
// using PART_Input_Connected = No DPT is defined. B4
using PART_PB_Action_HVAC_Extended = DPT_PB_Action_HVAC_Extended;
using PART_COV_Power = DPT_Value_Power;
using PART_COV_Energy = DPT_ActiveEnergy;

}
