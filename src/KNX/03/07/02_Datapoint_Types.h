// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 Introduction */
#include <KNX/03/07/02/Datapoint_Type.h>

/* 2 Overview */

/* 3 Datapoint Types for common use (Subnumber: 0..99) */

/* 3.1 Datapoint Types B1 */
#include <KNX/03/07/02/1/DPT_1_001.h>
#include <KNX/03/07/02/1/DPT_1_002.h>
#include <KNX/03/07/02/1/DPT_1_003.h>
#include <KNX/03/07/02/1/DPT_1_004.h>
#include <KNX/03/07/02/1/DPT_1_005.h>
#include <KNX/03/07/02/1/DPT_1_006.h>
#include <KNX/03/07/02/1/DPT_1_007.h>
#include <KNX/03/07/02/1/DPT_1_008.h>
#include <KNX/03/07/02/1/DPT_1_009.h>
#include <KNX/03/07/02/1/DPT_1_010.h>
#include <KNX/03/07/02/1/DPT_1_011.h>
#include <KNX/03/07/02/1/DPT_1_012.h>
#include <KNX/03/07/02/1/DPT_1_013.h>
#include <KNX/03/07/02/1/DPT_1_014.h>
#include <KNX/03/07/02/1/DPT_1_015.h>
#include <KNX/03/07/02/1/DPT_1_016.h>
#include <KNX/03/07/02/1/DPT_1_017.h>
#include <KNX/03/07/02/1/DPT_1_018.h>
#include <KNX/03/07/02/1/DPT_1_019.h>
#include <KNX/03/07/02/1/DPT_1_021.h>
#include <KNX/03/07/02/1/DPT_1_022.h>
#include <KNX/03/07/02/1/DPT_1_023.h>
#include <KNX/03/07/02/1/DPT_1_024.h>

/* 3.2 Datapoint Types B2 */
#include <KNX/03/07/02/2/DPT_2_001.h>
#include <KNX/03/07/02/2/DPT_2_002.h>
#include <KNX/03/07/02/2/DPT_2_003.h>
#include <KNX/03/07/02/2/DPT_2_004.h>
#include <KNX/03/07/02/2/DPT_2_005.h>
#include <KNX/03/07/02/2/DPT_2_006.h>
#include <KNX/03/07/02/2/DPT_2_007.h>
#include <KNX/03/07/02/2/DPT_2_008.h>
#include <KNX/03/07/02/2/DPT_2_009.h>
#include <KNX/03/07/02/2/DPT_2_010.h>
#include <KNX/03/07/02/2/DPT_2_011.h>
#include <KNX/03/07/02/2/DPT_2_012.h>

/* 3.3 Datapoint Types B1U3 */
/* 3.3.1 DPT_Control_Dimming */
#include <KNX/03/07/02/3/DPT_3_007.h>
/* 3.3.2 DPT_Control_Blinds */
#include <KNX/03/07/02/3/DPT_3_008.h>

/* 3.4 Datapoint Types "Character Set" */
#include <KNX/03/07/02/4/DPT_4_001.h>
#include <KNX/03/07/02/4/DPT_4_002.h>

/* 3.5 Datapoint Types "8-Bit Unsigned Value" */
/* 3.5.1 Scaled values */
#include <KNX/03/07/02/5/DPT_5_001.h>
#include <KNX/03/07/02/5/DPT_5_003.h>
#include <KNX/03/07/02/5/DPT_5_004.h>
#include <KNX/03/07/02/5/DPT_5_005.h>
/* 3.5.2 Non-scaled values */
/* 3.5.2.1 DPT_Value_1_Ucount */
#include <KNX/03/07/02/5/DPT_5_010.h>
/* 3.5.2.2 DPT for tariff information */
#include <KNX/03/07/02/5/DPT_5_006.h>

/* 3.6 Datapoint Types V8 */
/* 3.6.1 Signed Relative Value */
#include <KNX/03/07/02/6/DPT_6_001.h>
#include <KNX/03/07/02/6/DPT_6_010.h>

/* 3.7 Datapoint Type "Status with Mode" */
#include <KNX/03/07/02/6/DPT_6_020.h>

/* 3.8 Datapoint Types "2-Octet Unsigned Value" */
/* 3.8.1 2-octet unsigned counter value */
#include <KNX/03/07/02/7/DPT_7_001.h>
#include <KNX/03/07/02/7/DPT_7_010.h>
/* 3.8.2 Time Period */
#include <KNX/03/07/02/7/DPT_7_002.h>
#include <KNX/03/07/02/7/DPT_7_003.h>
#include <KNX/03/07/02/7/DPT_7_004.h>
#include <KNX/03/07/02/7/DPT_7_005.h>
#include <KNX/03/07/02/7/DPT_7_006.h>
#include <KNX/03/07/02/7/DPT_7_007.h>
/* 3.8.3 Other U16 Datapoint Types */
#include <KNX/03/07/02/7/DPT_7_011.h>
#include <KNX/03/07/02/7/DPT_7_012.h>
#include <KNX/03/07/02/7/DPT_7_013.h>

/* 3.9 Datapoint Types "2-Octet Signed Value" */
/* 3.9.1 2-octet signed counter value */
#include <KNX/03/07/02/8/DPT_8_001.h>
#include <KNX/03/07/02/8/DPT_8_010.h>
/* 3.9.2 Delta Time */
#include <KNX/03/07/02/8/DPT_8_002.h>
#include <KNX/03/07/02/8/DPT_8_003.h>
#include <KNX/03/07/02/8/DPT_8_004.h>
#include <KNX/03/07/02/8/DPT_8_005.h>
#include <KNX/03/07/02/8/DPT_8_006.h>
#include <KNX/03/07/02/8/DPT_8_007.h>
/* 3.9.3 Other V16 Datapoint Types */
#include <KNX/03/07/02/8/DPT_8_011.h>
#include <KNX/03/07/02/8/DPT_8_012.h>

/* 3.10 Datapoint Types "2-Octet Float Value" */
#include <KNX/03/07/02/9/DPT_9_001.h>
#include <KNX/03/07/02/9/DPT_9_002.h>
#include <KNX/03/07/02/9/DPT_9_003.h>
#include <KNX/03/07/02/9/DPT_9_004.h>
#include <KNX/03/07/02/9/DPT_9_005.h>
#include <KNX/03/07/02/9/DPT_9_006.h>
#include <KNX/03/07/02/9/DPT_9_007.h>
#include <KNX/03/07/02/9/DPT_9_008.h>
#include <KNX/03/07/02/9/DPT_9_009.h>
#include <KNX/03/07/02/9/DPT_9_010.h>
#include <KNX/03/07/02/9/DPT_9_011.h>
#include <KNX/03/07/02/9/DPT_9_020.h>
#include <KNX/03/07/02/9/DPT_9_021.h>
#include <KNX/03/07/02/9/DPT_9_022.h>
#include <KNX/03/07/02/9/DPT_9_023.h>
#include <KNX/03/07/02/9/DPT_9_024.h>
#include <KNX/03/07/02/9/DPT_9_025.h>
#include <KNX/03/07/02/9/DPT_9_026.h>
#include <KNX/03/07/02/9/DPT_9_027.h>
#include <KNX/03/07/02/9/DPT_9_028.h>
#include <KNX/03/07/02/9/DPT_9_029.h>
#include <KNX/03/07/02/9/DPT_9_030.h>

/* 3.11 Datapoint Types "Time" */
#include <KNX/03/07/02/10/DPT_10_001.h>

/* 3.12 Datapoint Type "Date" */
#include <KNX/03/07/02/11/DPT_11_001.h>

/* 3.13 Datapoint Types "4-Octet Unsigned Value" */
/* 3.13.1 General */
#include <KNX/03/07/02/12/DPT_12_001.h>
/* 3.13.2 Operating hours */
#include <KNX/03/07/02/12/DPT_12_100.h>
#include <KNX/03/07/02/12/DPT_12_101.h>
#include <KNX/03/07/02/12/DPT_12_102.h>

/* 3.14 Datapoint Types "4-Octet Signed Value" */
/* 3.14.1 4 Octet signed counter value */
#include <KNX/03/07/02/13/DPT_13_001.h>
#include <KNX/03/07/02/13/DPT_13_002.h>
/* 3.14.2 DPTs for electrical energy */
#include <KNX/03/07/02/13/DPT_13_010.h>
#include <KNX/03/07/02/13/DPT_13_011.h>
#include <KNX/03/07/02/13/DPT_13_012.h>
#include <KNX/03/07/02/13/DPT_13_013.h>
#include <KNX/03/07/02/13/DPT_13_014.h>
#include <KNX/03/07/02/13/DPT_13_015.h>
#include <KNX/03/07/02/13/DPT_13_016.h>
/* 3.14.3 4 Octet signed time period */
#include <KNX/03/07/02/13/DPT_13_100.h>

/* 3.15 Datapoint Types "4-Octet Float Value" */
#include <KNX/03/07/02/14/DPT_14_000.h>
#include <KNX/03/07/02/14/DPT_14_001.h>
#include <KNX/03/07/02/14/DPT_14_002.h>
#include <KNX/03/07/02/14/DPT_14_003.h>
#include <KNX/03/07/02/14/DPT_14_004.h>
#include <KNX/03/07/02/14/DPT_14_005.h>
#include <KNX/03/07/02/14/DPT_14_006.h>
#include <KNX/03/07/02/14/DPT_14_007.h>
#include <KNX/03/07/02/14/DPT_14_008.h>
#include <KNX/03/07/02/14/DPT_14_009.h>
#include <KNX/03/07/02/14/DPT_14_010.h>
#include <KNX/03/07/02/14/DPT_14_011.h>
#include <KNX/03/07/02/14/DPT_14_012.h>
#include <KNX/03/07/02/14/DPT_14_013.h>
#include <KNX/03/07/02/14/DPT_14_014.h>
#include <KNX/03/07/02/14/DPT_14_015.h>
#include <KNX/03/07/02/14/DPT_14_016.h>
#include <KNX/03/07/02/14/DPT_14_017.h>
#include <KNX/03/07/02/14/DPT_14_018.h>
#include <KNX/03/07/02/14/DPT_14_019.h>
#include <KNX/03/07/02/14/DPT_14_020.h>
#include <KNX/03/07/02/14/DPT_14_021.h>
#include <KNX/03/07/02/14/DPT_14_022.h>
#include <KNX/03/07/02/14/DPT_14_023.h>
#include <KNX/03/07/02/14/DPT_14_024.h>
#include <KNX/03/07/02/14/DPT_14_025.h>
#include <KNX/03/07/02/14/DPT_14_026.h>
#include <KNX/03/07/02/14/DPT_14_027.h>
#include <KNX/03/07/02/14/DPT_14_028.h>
#include <KNX/03/07/02/14/DPT_14_029.h>
#include <KNX/03/07/02/14/DPT_14_030.h>
#include <KNX/03/07/02/14/DPT_14_031.h>
#include <KNX/03/07/02/14/DPT_14_032.h>
#include <KNX/03/07/02/14/DPT_14_033.h>
#include <KNX/03/07/02/14/DPT_14_034.h>
#include <KNX/03/07/02/14/DPT_14_035.h>
#include <KNX/03/07/02/14/DPT_14_036.h>
#include <KNX/03/07/02/14/DPT_14_037.h>
#include <KNX/03/07/02/14/DPT_14_038.h>
#include <KNX/03/07/02/14/DPT_14_039.h>
#include <KNX/03/07/02/14/DPT_14_040.h>
#include <KNX/03/07/02/14/DPT_14_041.h>
#include <KNX/03/07/02/14/DPT_14_042.h>
#include <KNX/03/07/02/14/DPT_14_043.h>
#include <KNX/03/07/02/14/DPT_14_044.h>
#include <KNX/03/07/02/14/DPT_14_045.h>
#include <KNX/03/07/02/14/DPT_14_046.h>
#include <KNX/03/07/02/14/DPT_14_047.h>
#include <KNX/03/07/02/14/DPT_14_048.h>
#include <KNX/03/07/02/14/DPT_14_049.h>
#include <KNX/03/07/02/14/DPT_14_050.h>
#include <KNX/03/07/02/14/DPT_14_051.h>
#include <KNX/03/07/02/14/DPT_14_052.h>
#include <KNX/03/07/02/14/DPT_14_053.h>
#include <KNX/03/07/02/14/DPT_14_054.h>
#include <KNX/03/07/02/14/DPT_14_055.h>
#include <KNX/03/07/02/14/DPT_14_056.h>
#include <KNX/03/07/02/14/DPT_14_057.h>
#include <KNX/03/07/02/14/DPT_14_058.h>
#include <KNX/03/07/02/14/DPT_14_059.h>
#include <KNX/03/07/02/14/DPT_14_060.h>
#include <KNX/03/07/02/14/DPT_14_061.h>
#include <KNX/03/07/02/14/DPT_14_062.h>
#include <KNX/03/07/02/14/DPT_14_063.h>
#include <KNX/03/07/02/14/DPT_14_064.h>
#include <KNX/03/07/02/14/DPT_14_065.h>
#include <KNX/03/07/02/14/DPT_14_066.h>
#include <KNX/03/07/02/14/DPT_14_067.h>
#include <KNX/03/07/02/14/DPT_14_068.h>
#include <KNX/03/07/02/14/DPT_14_069.h>
#include <KNX/03/07/02/14/DPT_14_070.h>
#include <KNX/03/07/02/14/DPT_14_071.h>
#include <KNX/03/07/02/14/DPT_14_072.h>
#include <KNX/03/07/02/14/DPT_14_073.h>
#include <KNX/03/07/02/14/DPT_14_074.h>
#include <KNX/03/07/02/14/DPT_14_075.h>
#include <KNX/03/07/02/14/DPT_14_076.h>
#include <KNX/03/07/02/14/DPT_14_077.h>
#include <KNX/03/07/02/14/DPT_14_078.h>
#include <KNX/03/07/02/14/DPT_14_079.h>
#include <KNX/03/07/02/14/DPT_14_080.h>

/* 3.16 Datapoint Type DPT_Acecss_Data */
#include <KNX/03/07/02/15/DPT_15_000.h>

/* 3.17 Datapoint Types "String" */
#include <KNX/03/07/02/16/DPT_16_000.h>
#include <KNX/03/07/02/16/DPT_16_001.h>

/* 3.18 Datapoint Type Scene Number */
#include <KNX/03/07/02/17/DPT_17_001.h>

/* 3.19 Datapoint Type DPT_SceneControl */
#include <KNX/03/07/02/18/DPT_18_001.h>

/* 3.20 Datapoint Type DPT_DateTime */
#include <KNX/03/07/02/19/DPT_19_001.h>

/* 3.21 Datapoint Types N8 */
#include <KNX/03/07/02/20/DPT_20_001.h>
#include <KNX/03/07/02/20/DPT_20_002.h>
#include <KNX/03/07/02/20/DPT_20_003.h>
#include <KNX/03/07/02/20/DPT_20_004.h>
#include <KNX/03/07/02/20/DPT_20_005.h>
#include <KNX/03/07/02/20/DPT_20_006.h>
#include <KNX/03/07/02/20/DPT_20_007.h>
#include <KNX/03/07/02/20/DPT_20_008.h>
#include <KNX/03/07/02/20/DPT_20_011.h>
#include <KNX/03/07/02/20/DPT_20_012.h>
#include <KNX/03/07/02/20/DPT_20_013.h>
#include <KNX/03/07/02/20/DPT_20_014.h>
#include <KNX/03/07/02/20/DPT_20_017.h>
#include <KNX/03/07/02/20/DPT_20_020.h>
#include <KNX/03/07/02/20/DPT_20_021.h>
#include <KNX/03/07/02/20/DPT_20_022.h>

/* 3.22 Datapoint Type B8 */
/* 3.22.1 Datapoint Type "General Status" */
#include <KNX/03/07/02/21/DPT_21_001.h>
/* 3.22.2 Datapoint Type "Device Control" */
#include <KNX/03/07/02/21/DPT_21_002.h>

/* 3.23 Datapoint Types N2 */
#include <KNX/03/07/02/23/DPT_23_001.h>
#include <KNX/03/07/02/23/DPT_23_002.h>
#include <KNX/03/07/02/23/DPT_23_003.h>

/* 3.24 Datapoint Type DPT_VarString_8859_1 */
#include <KNX/03/07/02/24/DPT_24_001.h>

/* 3.25 Datapoint Type DPT_SceneInfo */
#include <KNX/03/07/02/26/DPT_26_001.h>

/* 3.26 Datatype B32 */
#include <KNX/03/07/02/27/DPT_27_001.h>

/* 3.27 Datapoint Type Unicode UTF-8 String A[n] */
/* 3.27.1 DPT_UTF-8 */
#include <KNX/03/07/02/28/DPT_28_001.h>

/* 3.28 Datapoint Types V64 */
/* 3.28.1 DPTs for electrical energy */
#include <KNX/03/07/02/29/DPT_29_010.h>
#include <KNX/03/07/02/29/DPT_29_011.h>
#include <KNX/03/07/02/29/DPT_29_012.h>

/* 3.29 Datapoint Type DPT_AlarmInfo */
#include <KNX/03/07/02/219/DPT_219_001.h>

/* 3.30 Datapoint Type DPT_SerNum */
#include <KNX/03/07/02/221/DPT_221_001.h>

/* 3.31 Datapoint Types "Unsigned Relative Value" */
#include <KNX/03/07/02/202/DPT_202_001.h>

/* 3.32 Datapoint Types "Unsigned Counter Value" */
#include <KNX/03/07/02/202/DPT_202_002.h>

/* 3.33 Datapoint Types "Time Period..._Z" */
#include <KNX/03/07/02/203/DPT_203_002.h>
#include <KNX/03/07/02/203/DPT_203_003.h>
#include <KNX/03/07/02/203/DPT_203_004.h>
#include <KNX/03/07/02/203/DPT_203_005.h>
#include <KNX/03/07/02/203/DPT_203_006.h>
#include <KNX/03/07/02/203/DPT_203_007.h>

/* 3.34 Datapoint Types "Unsigned Flow Rate l/h" */
#include <KNX/03/07/02/203/DPT_203_011.h>

/* 3.35 Datapoint Types "Unsigned Counter Value" */
#include <KNX/03/07/02/203/DPT_203_012.h>

/* 3.36 Datapoint Types "Unsigned Electric Current uA" */
#include <KNX/03/07/02/203/DPT_203_013.h>

/* 3.37 Datapoint Types "Power in kW" */
#include <KNX/03/07/02/203/DPT_203_014.h>

/* 3.38 Datapoint Type "Atmospheric Pressure with Status/Command" */
#include <KNX/03/07/02/203/DPT_203_015.h>

/* 3.38.1 Datapoint Type "DPT_PercentU16_Z" */
#include <KNX/03/07/02/203/DPT_203_017.h>

/* 3.39 Datapoint Types "Signed Relative Value" */
#include <KNX/03/07/02/204/DPT_204_001.h>

/* 3.40 Datapoint Types "DeltaTime...Z" */
#include <KNX/03/07/02/205/DPT_205_002.h>
#include <KNX/03/07/02/205/DPT_205_003.h>
#include <KNX/03/07/02/205/DPT_205_004.h>
#include <KNX/03/07/02/205/DPT_205_005.h>
#include <KNX/03/07/02/205/DPT_205_006.h>
#include <KNX/03/07/02/205/DPT_205_007.h>

/* 3.41 Datapoint Type "16 bit Signed Relative Value" */
#include <KNX/03/07/02/205/DPT_205_017.h>

/* 3.42 Datapoint Type DPT_Version */
#include <KNX/03/07/02/217/DPT_217_001.h>

/* 3.43 Datatype V32Z8 */
/* 3.43.1 Datapoint Type "Volume in Liter" */
#include <KNX/03/07/02/218/DPT_218_001.h>
/* 3.43.2 Datapoint Type "Flow Rate in m3/h_Z" */
#include <KNX/03/07/02/218/DPT_218_002.h>

/* 3.44 Datatype U16U8 */
/* 3.44.1 Datapoint Type "Scaling speed" */
#include <KNX/03/07/02/225/DPT_225_001.h>
/* 3.44.2 Datapoint Type "Scaling step time" */
#include <KNX/03/07/02/225/DPT_225_002.h>
/* 3.44.3 DPT_TariffNext */
#include <KNX/03/07/02/225/DPT_225_003.h>

/* 3.45 Datatype V32N8Z8 */
/* 3.45.1 Datapoint Type "MeteringValue" */
#include <KNX/03/07/02/229/DPT_229_001.h>

/* 3.46 Datatypes A8A8A8A8 */
/* 3.46.1 DPT_Locale_ASCII */
#include <KNX/03/07/02/231/DPT_231_001.h>

/* 3.47 Datapoint Types A8A8 */
/* 3.47.1 DPT_LanguageCodeAlpha2_ASCII */
#include <KNX/03/07/02/234/DPT_234_001.h>
/* 3.47.2 Datapoint Type DPT_RegionCodeAlpha2_ASCII */
#include <KNX/03/07/02/234/DPT_234_002.h>

/* 3.48 DPT_Tariff_ActiveEnergy */
#include <KNX/03/07/02/235/DPT_235_001.h>

/* 3.49 DPT_Prioritised_Mode_Control */
#include <KNX/03/07/02/236/DPT_236_001.h>

/* 3.50 Datapoint Types B2U6 */
/* 3.50.1 DPT_SceneConfig */
#include <KNX/03/07/02/238/DPT_238_001.h>

/* 3.51 Datapoint Types U8r7B1 */
/* 3.51.1 DPT_FlaggedScaling */
#include <KNX/03/07/02/239/DPT_239_001.h>

/* 3.52 Datapoint Types F32F32 */
/* 3.52.1 DPT_Geographical Location */
#include <KNX/03/07/02/255/DPT_255_001.h>

/* 3.53 Datapoint Types "DPT_DateTime_Period" */
#include <KNX/03/07/02/256/DPT_256_001.h>

/* 3.54 Datepoint Types B1 with Date and Time */
#include <KNX/03/07/02/265/DPT_265_001.h>
#include <KNX/03/07/02/265/DPT_265_005.h>
#include <KNX/03/07/02/265/DPT_265_009.h>
#include <KNX/03/07/02/265/DPT_265_011.h>
#include <KNX/03/07/02/265/DPT_265_012.h>

/* 3.55 Datepoint Types 4 octets Float with Date and Time */
#include <KNX/03/07/02/266/DPT_266_027.h>
#include <KNX/03/07/02/266/DPT_266_056.h>
#include <KNX/03/07/02/266/DPT_266_080.h>

/* 3.56 Datepoint Type DPT_UTF-8 with Date and Time */
#include <KNX/03/07/02/267/DPT_267_001.h>

/* 4 Datapoint Types for HVAC (Subnumber: 100..499) */

/* 4.1 Simple Datapoint Types with STATUS/COMMAND Z8 field */

/* 4.2 Datapoint Types B1 */
#include <KNX/03/07/02/1/DPT_1_100.h>

/* @note further DPTs found in MasterData */
#include <KNX/03/07/02/5/DPT_5_100.h>

/* 4.3 Datapoint Types N8 */
#include <KNX/03/07/02/20/DPT_20_100.h>
#include <KNX/03/07/02/20/DPT_20_101.h>
#include <KNX/03/07/02/20/DPT_20_102.h>
#include <KNX/03/07/02/20/DPT_20_103.h>
#include <KNX/03/07/02/20/DPT_20_104.h>
#include <KNX/03/07/02/20/DPT_20_105.h>
#include <KNX/03/07/02/20/DPT_20_106.h>
#include <KNX/03/07/02/20/DPT_20_107.h>
#include <KNX/03/07/02/20/DPT_20_108.h>
#include <KNX/03/07/02/20/DPT_20_109.h>
#include <KNX/03/07/02/20/DPT_20_110.h>
#include <KNX/03/07/02/20/DPT_20_111.h>
#include <KNX/03/07/02/20/DPT_20_112.h>
#include <KNX/03/07/02/20/DPT_20_113.h>
#include <KNX/03/07/02/20/DPT_20_114.h>
#include <KNX/03/07/02/20/DPT_20_115.h>
#include <KNX/03/07/02/20/DPT_20_116.h>
#include <KNX/03/07/02/20/DPT_20_120.h>
#include <KNX/03/07/02/20/DPT_20_121.h>
#include <KNX/03/07/02/20/DPT_20_122.h>

/* 4.4 Data Type "8-Bit Set" */
/* 4.4.1 Datapoint Type "Forcing Signal" */
#include <KNX/03/07/02/21/DPT_21_100.h>
/* 4.4.2 Datapoint Type "Forcing Signal Cool" */
#include <KNX/03/07/02/21/DPT_21_101.h>
/* 4.4.3 Datapoint Type "Room Heating Controller Status" */
#include <KNX/03/07/02/21/DPT_21_102.h>
/* 4.4.4 Datapoint Type "Solar DHW Controller Status" */
#include <KNX/03/07/02/21/DPT_21_103.h>
/* 4.4.5 Datapoint Type "Fuel Type Set" */
#include <KNX/03/07/02/21/DPT_21_104.h>
/* 4.4.6 Datapoint Type "Room Cooling Controller Status" */
#include <KNX/03/07/02/21/DPT_21_105.h>
/* 4.4.7 Datapoint Type "Ventilation Controller Status" */
#include <KNX/03/07/02/21/DPT_21_106.h>
/* @note further DPTs found in MasterData */
#include <KNX/03/07/02/21/DPT_21_107.h>

/* 4.5 Data Type "16-Bit Set" */
/* 4.5.1 Datapoint Type "DHW Controller Status" */
#include <KNX/03/07/02/22/DPT_22_100.h>
/* 4.5.2 Datapoint Type "RHCC Status" */
#include <KNX/03/07/02/22/DPT_22_101.h>
/* @note further DPTs found in MasterData */
#include <KNX/03/07/02/22/DPT_22_102.h>
#include <KNX/03/07/02/22/DPT_22_103.h>

/* 4.6 Datapoint Types N2 */
#include <KNX/03/07/02/23/DPT_23_102.h>

/* 4.7 Datapoint Types N3 */
/* 4.7.1 Datapoint Type DPT_PB_Action_HVAC_Extended */
#include <KNX/03/07/02/31/DPT_31_101.h>

/* 4.8 Data Type "Boolean with Status/Command" */
/* 4.8.1 Datapoint Type "Heat/Cool_Z" */
#include <KNX/03/07/02/200/DPT_200_100.h>
/* 4.8.2 Datapoint Type "DPT_BinaryValue_Z" */
#include <KNX/03/07/02/200/DPT_200_101.h>

/* 4.9 Data Type "8-Bit Enum with Status/Command" */
/* 4.9.1 Datapoint Type "HVAC Operating Mode" */
#include <KNX/03/07/02/201/DPT_201_100.h>
/* 4.9.2 Datapoint Type "DHW Mode" */
#include <KNX/03/07/02/201/DPT_201_102.h>
/* 4.9.3 Datapoint Type "HVAC Controlling Mode" */
#include <KNX/03/07/02/201/DPT_201_104.h>
/* 4.9.4 Datapoint Type "Enable Heat/Cool Stage" */
#include <KNX/03/07/02/201/DPT_201_105.h>
/* 4.9.5 Datapoint Type "Building Mode" */
#include <KNX/03/07/02/201/DPT_201_107.h>
/* 4.9.6 Datapoint Type "Occupancy Mode" */
#include <KNX/03/07/02/201/DPT_201_108.h>
/* 4.9.7 Datapoint Type "HVAC Emergency Mode" */
#include <KNX/03/07/02/201/DPT_201_109.h>

/* 4.10 Data Type "16-Bit Unsigned Value with Status/Command" */
/* 4.10.1 Datapoint Type "HVAC Air Quality" */
#include <KNX/03/07/02/203/DPT_203_100.h>
/* 4.10.2 Datapoint Type "Wind Speed with Status/Command" */
#include <KNX/03/07/02/203/DPT_203_101.h>
/* 4.10.3 Datapoint Type "Sun Intensity with Status/Command" */
#include <KNX/03/07/02/203/DPT_203_102.h>
/* 4.10.4 Datapoint Type "HVAC Air Flow Absolute Value" */
#include <KNX/03/07/02/203/DPT_203_104.h>

/* 4.11 Data Type "16-Bit Signed Value with Status/Command" */
/* 4.11.1 Datapoint Type "HVAC absolute Temperature" */
#include <KNX/03/07/02/205/DPT_205_100.h>
/* 4.11.2 Datapoint Type "HVAC relative Temperature" */
#include <KNX/03/07/02/205/DPT_205_101.h>
/* 4.11.3 Datapoint Type "HVAC Air Flow Relative Value" */
#include <KNX/03/07/02/205/DPT_205_102.h>
/* 4.11.4 Datapoint Type "HVAC Air Quality Relative Value" */
#include <KNX/03/07/02/205/DPT_205_103.h>

/* 4.12 Data Type "16-Bit Unsigned Value & 8-Bit Enum" */
/* 4.12.1 Datapoint Type "HVAC Mode & Time delay" */
#include <KNX/03/07/02/206/DPT_206_100.h>
/* 4.12.2 Datapoint Type "DHW Mode & Time delay" */
#include <KNX/03/07/02/206/DPT_206_102.h>
/* 4.12.3 Datapoint Type "Occupancy Mode & Time delay" */
#include <KNX/03/07/02/206/DPT_206_104.h>
/* 4.12.4 Datapoint Type "Building Mode & Time delay" */
#include <KNX/03/07/02/206/DPT_206_105.h>

/* 4.13 Data Type "8-Bit Unsigned Value & 8-Bit Set" */
/* 4.13.1 Datapoint Type "Status Burner Controller" */
#include <KNX/03/07/02/207/DPT_207_100.h>
/* 4.13.2 Datapoint Type "Locking Signal" */
#include <KNX/03/07/02/207/DPT_207_101.h>
/* 4.13.3 Datapoint Type "Boiler Controller Demand Signal" */
#include <KNX/03/07/02/207/DPT_207_102.h>
/* 4.13.4 Datapoint Type "Actuator Position Demand" */
#include <KNX/03/07/02/207/DPT_207_104.h>
/* 4.13.5 Datapoint Type "Actuator Position Status" */
#include <KNX/03/07/02/207/DPT_207_105.h>

/* 4.14 Data Type "16-Bit Signed Value & 8-Bit Set" */
/* 4.14.1 Datapoint Type "Heat Producer Manager Status" */
#include <KNX/03/07/02/209/DPT_209_100.h>
/* 4.14.2 Datapoint Type "Room Temperature Demand" */
#include <KNX/03/07/02/209/DPT_209_101.h>
/* 4.14.3 Datapoint Type "Cold Water Producer Manager Status" */
#include <KNX/03/07/02/209/DPT_209_102.h>
/* 4.14.4 Datapoint Type "Water Temperature Controller Status" */
#include <KNX/03/07/02/209/DPT_209_103.h>

/* 4.15 Data Type "16-Bit Signed Value & 16-Bit Set" */
/* 4.15.1 Datapoint Type "Consumer Flow Temperature Demand" */
#include <KNX/03/07/02/210/DPT_210_100.h>

/* 4.16 Data Type "8-Bit Unsigned Value & 8-Bit Enum" */
/* 4.16.1 Datapoint Type "EnergyDemWater" */
#include <KNX/03/07/02/211/DPT_211_100.h>

/* 4.17 Data Type "3x 16-Bit Signed Value " */
/* 4.17.1 Datapoint Type "3x set of RoomTemperature Setpoint Shift values" */
#include <KNX/03/07/02/212/DPT_212_100.h>
/* 4.17.2 Datapoint Type "3x set of RoomTemperature Absolute Setpoint values" */
#include <KNX/03/07/02/212/DPT_212_101.h>

/* 4.18 Data Type "4x 16-Bit Signed Value " */
/* 4.18.1 Datapoint Type "4x set of RoomTemperature setpoints " */
#include <KNX/03/07/02/213/DPT_213_100.h>
/* 4.18.2 Datapoint Type "4x set of DHWTemperature setpoints " */
#include <KNX/03/07/02/213/DPT_213_101.h>
/* 4.18.3 Datapoint Type "4x set of RoomTemperature setpoint shift values " */
#include <KNX/03/07/02/213/DPT_213_102.h>

/* 4.19 Data Type "16-Bit Signed & 8-Bit Unsigned Value & 8-Bit Set" */
/* 4.19.1 Datapoint Type "Heat Prod. Manager Demand Signal" */
#include <KNX/03/07/02/214/DPT_214_100.h>
/* 4.19.2 Datapoint Type "Cold Water Prod. Manager Demand Signal" */
#include <KNX/03/07/02/214/DPT_214_101.h>

/* 4.20 Data Type "V16U8B16" */
/* 4.20.1 Datapoint Type "Status Boiler Controller" */
#include <KNX/03/07/02/215/DPT_215_100.h>
/* 4.20.2 Datapoint Type "Status Chiller Controller" */
#include <KNX/03/07/02/215/DPT_215_101.h>

/* 4.21 Data Type "U16U8N8B8" */
/* 4.21.1 Datapoint Type "Heat Producer Specification" */
#include <KNX/03/07/02/216/DPT_216_100.h>

/* 4.22 Data Type "16-Bit Unsigned Value & 16-Bit Signed Value" */
/* 4.21.1 Datapoint Type "Next Temperature & Time Delay" */
#include <KNX/03/07/02/220/DPT_220_100.h>

/* 4.23 Data Type "3x 16-Float Value" */
/* 4.23.1 Datapoint Type "3x set of RoomTemperature Setpoint Values " */
#include <KNX/03/07/02/222/DPT_222_100.h>
/* 4.23.2 Datapoint Type "3x set of RoomTemperature Setpoint Shift Values " */
#include <KNX/03/07/02/222/DPT_222_101.h>

/* 4.24 Data Type V8N8N8 */
/* 4.24.1 Datapoint Type "EnergyDemAir" */
#include <KNX/03/07/02/223/DPT_223_100.h>

/* 4.25 Data Type V16V16N8N8 */
/* 4.25.1 Datapoint Type "TempSupplyAirSetpSet" */
#include <KNX/03/07/02/224/DPT_224_100.h>

/* @note further DPTs found in MasterData */
#include <KNX/03/07/02/275/DPT_275_100.h>
#include <KNX/03/07/02/275/DPT_275_101.h>

/* 5 Datapoint Types for Load Management (Subnumber: 500..599) */

/* 6 Datapoint Types for Lighting (Subnumber: 600..799) */

/* 6.2 Datapoint Types U16 */
#include <KNX/03/07/02/7/DPT_7_600.h>

/* 6.3 Datapoint Types N8 */
#include <KNX/03/07/02/20/DPT_20_600.h>
#include <KNX/03/07/02/20/DPT_20_601.h>
#include <KNX/03/07/02/20/DPT_20_602.h>
#include <KNX/03/07/02/20/DPT_20_603.h>
#include <KNX/03/07/02/20/DPT_20_604.h>
#include <KNX/03/07/02/20/DPT_20_605.h>
#include <KNX/03/07/02/20/DPT_20_606.h>
#include <KNX/03/07/02/20/DPT_20_607.h>
#include <KNX/03/07/02/20/DPT_20_608.h>
#include <KNX/03/07/02/20/DPT_20_609.h>
#include <KNX/03/07/02/20/DPT_20_610.h>
#include <KNX/03/07/02/20/DPT_20_611.h>
#include <KNX/03/07/02/20/DPT_20_612.h>
#include <KNX/03/07/02/20/DPT_20_613.h>

/* 6.4 Datapoint Types B8 */
/* 6.4.1 Datapoint Type "Lighting Actuator Error Information" */
#include <KNX/03/07/02/21/DPT_21_601.h>

/* 6.5 Datapoint Types U8B8 */
/* 6.5.1 Datapoint Type "Status Lighting Actuator" */
#include <KNX/03/07/02/207/DPT_207_600.h>

/* 6.6 Datapoint Types U8U8U8 */
/* 6.6.1 DPT_Colour_RGB */
#include <KNX/03/07/02/232/DPT_232_600.h>

/* 6.7 Datapoint Types B10U6 */
/* 6.7.1 DPT_DALI_Control_Gear_Diagnostics */
#include <KNX/03/07/02/237/DPT_237_600.h>

/* 6.8 Datapoint Types B2U6 */
/* 6.8.1 DPT_DALI_Diagnostics */
#include <KNX/03/07/02/238/DPT_238_600.h>

/* 6.9 DPT_Colour_xyY (C_xyY) */
#include <KNX/03/07/02/242/DPT_242_600.h>

/* 6.10 DPT_Colour_Transition_xyY */
#include <KNX/03/07/02/243/DPT_243_600.h>

/* 6.11 DPT_Converter_Status */
#include <KNX/03/07/02/244/DPT_244_600.h>

/* 6.12 DPT_Converter_Test_Result */
#include <KNX/03/07/02/245/DPT_245_600.h>

/* 6.13 DPT_Battery_Info */
#include <KNX/03/07/02/246/DPT_246_600.h>

/* 6.14 DPT_Converter_Test_Info */
#include <KNX/03/07/02/247/DPT_247_600.h>

/* 6.15 DPT_Converter_Info_Fix */
#include <KNX/03/07/02/248/DPT_248_600.h>

/* 6.16 DPT_Brightness_Colour_Temperature_Transition */
#include <KNX/03/07/02/249/DPT_249_600.h>

/* 6.17 DPT_Brightness_Colour_Temperature_Control */
#include <KNX/03/07/02/250/DPT_250_600.h>

/* 6.18 DPT Colour_RGBW */
#include <KNX/03/07/02/251/DPT_251_600.h>

/* 6.19 DPT_Relative_Control_RGBW */
#include <KNX/03/07/02/252/DPT_252_600.h>

/* 6.20 DPT Relative_Control_xyY */
#include <KNX/03/07/02/253/DPT_253_600.h>

/* 6.21 DPT_Relative_Control_RGB */
#include <KNX/03/07/02/254/DPT_254_600.h>

/* 6.22 DPT_Converter_Info (DPT_CI) */
#include <KNX/03/07/02/272/DPT_272_600.h>

/* 7 Datapoint Types for shutters and blinds (Subnumber: 800..999) */

/* 7.1 Datapoint Types N8 */
#include <KNX/03/07/02/20/DPT_20_801.h>
#include <KNX/03/07/02/20/DPT_20_802.h>
#include <KNX/03/07/02/20/DPT_20_803.h>
#include <KNX/03/07/02/20/DPT_20_804.h>

/* 7.2 Datapoint Types U8U8B8 */
/* 7.2.1 Datapoint Type "Combined Position" */
#include <KNX/03/07/02/240/DPT_240_800.h>

/* 7.3 Datapoint Types U8U8B16 */
/* 7.4 Status Shutter & Sunblind Actuator */
#include <KNX/03/07/02/241/DPT_241_800.h>

/* 8 Datapoint Types for System (Subnumber: 1000..1199) */

/* 8.1 Datapoint Types N8 */
#include <KNX/03/07/02/20/DPT_20_1000.h>
#include <KNX/03/07/02/20/DPT_20_1001.h>
#include <KNX/03/07/02/20/DPT_20_1002.h>
#include <KNX/03/07/02/20/DPT_20_1003.h>
#include <KNX/03/07/02/20/DPT_20_1004.h>
#include <KNX/03/07/02/20/DPT_20_1005.h>

/* 8.2 Datapoint Types B8 */
/* 8.2.1 Datapoint Type "RF Communication Mode Info" */
#include <KNX/03/07/02/21/DPT_21_1000.h>
/* 8.2.2 Datapoint Type "cEMI Server Supported RF Filtering Modes" */
#include <KNX/03/07/02/21/DPT_21_1001.h>
/* 8.2.3 Datapoint Type "Security Report" */
#include <KNX/03/07/02/21/DPT_21_1002.h>
/* 8.2.4 Datapoint Type "Channel Activation for 8 channels" */
#include <KNX/03/07/02/21/DPT_21_1010.h>

/* 8.3 Datatype B16 */
/* 8.3.1 Datapoint Type "Media" */
#include <KNX/03/07/02/22/DPT_22_1000.h>
/* 8.3.2 Datapoint Type "Channel Activation for 16 channels" */
#include <KNX/03/07/02/22/DPT_22_1010.h>

/* 8.4 Datatype U4U4 */
#include <KNX/03/07/02/25/DPT_25_1000.h>

/* 8.5 Datapoint Types B24 */
/* 8.5.1 Datapoint Type "Channel Activation for 24 channels" */
#include <KNX/03/07/02/30/DPT_30_1010.h>

/* @note further DPTs found in MasterData */
#include <KNX/03/07/02/228/DPT_228_1000.h>

/* 8.6 Datapoint Type "MBus Address" */
#include <KNX/03/07/02/230/DPT_230_1000.h>

/* 9 Datapoint Types for Metering (Subnumber: 1200..1299) */

/* 9.1 Datapoint Types B1 */
#include <KNX/03/07/02/1/DPT_1_1200.h>
#include <KNX/03/07/02/1/DPT_1_1201.h>

/* 9.2 Datapoint Types U32 */
#include <KNX/03/07/02/12/DPT_12_1200.h>
#include <KNX/03/07/02/12/DPT_12_1201.h>

/* 9.3 Datapoint Types V32 */
#include <KNX/03/07/02/13/DPT_13_1200.h>
#include <KNX/03/07/02/13/DPT_13_1201.h>

/* 9.4 Datapoint Types F32 */
#include <KNX/03/07/02/14/DPT_14_1200.h>
#include <KNX/03/07/02/14/DPT_14_1201.h>

/* 9.5 Datapoint Types N8 */
#include <KNX/03/07/02/20/DPT_20_1200.h>
#include <KNX/03/07/02/20/DPT_20_1202.h>
#include <KNX/03/07/02/20/DPT_20_1203.h>
#include <KNX/03/07/02/20/DPT_20_1204.h>
#include <KNX/03/07/02/20/DPT_20_1205.h>
#include <KNX/03/07/02/20/DPT_20_1206.h>
#include <KNX/03/07/02/20/DPT_20_1207.h>
#include <KNX/03/07/02/20/DPT_20_1208.h>
#include <KNX/03/07/02/20/DPT_20_1209.h>

/* 9.6 Datapoint Types B8 */
#include <KNX/03/07/02/21/DPT_21_1200.h>

/* 9.7 Datapoint Types r5B3 */
#include <KNX/03/07/02/21/DPT_21_1201.h>

/* 9.8 Datapoint Types F32F32F32 */
#include <KNX/03/07/02/257/DPT_257_1200.h>
#include <KNX/03/07/02/257/DPT_257_1201.h>
#include <KNX/03/07/02/257/DPT_257_1202.h>

/* 9.9 Datapoint Types B1 with Date and Time */
#include <KNX/03/07/02/265/DPT_265_1200.h>
#include <KNX/03/07/02/265/DPT_265_1201.h>

/* 9.10 Datapoint Types Enum8 with Date and Time */
#include <KNX/03/07/02/268/DPT_268_1203.h>
#include <KNX/03/07/02/268/DPT_268_1204.h>
#include <KNX/03/07/02/268/DPT_268_1205.h>
#include <KNX/03/07/02/268/DPT_268_1206.h>

/* 9.11 Datapoint Types DPT_Tariff_ActiveEnergy with Date and Time */
#include <KNX/03/07/02/269/DPT_269_1200.h>

/* 9.12 Datapoint Types F32F32F32 with Date and Time */
#include <KNX/03/07/02/270/DPT_270_1200.h>
#include <KNX/03/07/02/270/DPT_270_1201.h>
#include <KNX/03/07/02/270/DPT_270_1202.h>

/* 9.13 Datapoint Types TariffDayProfile */
#include <KNX/03/07/02/271/DPT_271_1200.h>

/* 9.14 Datapoint Types DPT_ERL_Status */
#include <KNX/03/07/02/276/DPT_276_1200.h>

/* 9.15 Datapoint Types DPT_UTF-8 and N DPT_Tariff_ActiveEnergy */
#include <KNX/03/07/02/277/DPT_277_1200.h>
#include <KNX/03/07/02/278/DPT_278_1200.h>
#include <KNX/03/07/02/279/DPT_279_1200.h>
#include <KNX/03/07/02/280/DPT_280_1200.h>

/* 9.16 Datapoint Types DPT_UTF-8 and N DPT_Tariff_ActiveEnergy with Date and Time */
#include <KNX/03/07/02/281/DPT_281_1200.h>
#include <KNX/03/07/02/282/DPT_282_1200.h>
#include <KNX/03/07/02/283/DPT_283_1200.h>
#include <KNX/03/07/02/284/DPT_284_1200.h>

/* 10 Datapoint types for weather encoding */

/* 10.1 Forecasts for F16 values */
#include <KNX/03/07/02/273/DPT_273_001.h>
#include <KNX/03/07/02/273/DPT_273_002.h>
#include <KNX/03/07/02/273/DPT_273_003.h>
#include <KNX/03/07/02/273/DPT_273_004.h>
#include <KNX/03/07/02/273/DPT_273_005.h>
#include <KNX/03/07/02/273/DPT_273_006.h>
#include <KNX/03/07/02/273/DPT_273_007.h>

/* 10.2 Forecasts for U8 values */
#include <KNX/03/07/02/274/DPT_274_001.h>

/* 11 Parameter Types */
#include <KNX/03/07/02/Parameter_Types.h>
