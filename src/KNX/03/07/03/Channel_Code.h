// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Channel Codes
 *
 * @ingroup KNX_03_07_03_06_01
 */
enum class Channel_Code : uint16_t {
    /* 000h to 0FFh: sensors (old) */
    CH_PB_Toggle = 0x0002,
    CH_PB_Timed = 0x0003,
    CH_PB_Timed_Info = 0x0004,
    CH_PB_Dimmer = 0x0005,
    CH_PB_Dimmer_Toggle = 0x0006,
    CH_PB_Scene_Numbered = 0x0007,
    CH_PB_Scene = 0x0008,
    CH_Switch = 0x0009,
    CH_Switch_Info = 0x000A,
    CH_Switch_Forced = 0x000B,
    CH_Switch_Dimmer = 0x000C,
    CH_Switch_Dimmer_Info = 0x000D,
    CH_Switch_Dimmer_Toggle = 0x000E,
    CH_Switch_Scene_Numbered = 0x000F,
    CH_Switch_Scene = 0x0010,
    CH_Light_Setpoint_Controller = 0x0011,
    CH_Light_Sensor = 0x0012,
    CH_Motion_Detector_Basic = 0x0013,
    CH_Motion_Detector_Complex = 0x0014,
    CH_Switch_Shutter = 0x0015,
    CH_Switch_Blind = 0x0016,
    CH_PB_Shutter = 0x0017,
    CH_PB_Blind = 0x0018,
    CH_PB_Shutter_Toggle = 0x0019,
    CH_PB_Blind_Toggle = 0x001A,
    CH_Wind_Alarm_Sensor = 0x001B,
    CH_Rain_Alarm_Sensor = 0x001C,
    CH_Frost_Alarm_Sensor = 0x001D,
    CH_Switch_Operation_Mode = 0x001E,
    CH_Logical_Sensor = 0x001F,
    CH_PushButton = 0x0020, // CH_PushPutton
    CH_Battery_Status = 0x0021,
    CH_Smoke_Detector_Basic = 0x0022,
    CH_Window_Door_Contact_Basic = 0x0023,
    CH_Outside_Temperature_Sensor = 0x0024,
    CH_Room_Temperature_Sensor = 0x0025, // CH_Room_Temparature_Sensor
    CH_PB_HVAC_Mode = 0x0027,
    CH_Switch_HVAC_Heating_Enabled = 0x0028,
    CH_PB_Dimming_Value = 0x0029,
    CH_Push_Button_Info = 0x002A,
    CH_Forced_Info = 0x002B,
    CH_PB_Dimming_Value_Info = 0x002C,
    CH_Switch_Dimming_Value_Info = 0x002D,
    CH_PB_HVAC_Mode_1 = 0x002E,
    CH_PB_Shutter_1 = 0x002F,

    /* 100h to 1FFh: actuators (old) */
    CH_Status_Info = 0x0100,
    CH_Binary_Actuator_Basic = 0x0101,
    CH_Light_Actuator_Complex = 0x0102,
    CH_Light_Actuator_Scene = 0x0103,
    CH_Light_Actuator_Controlled = 0x0104,
    CH_Dimming_Actuator_Basic = 0x0105,
    CH_Dimming_Actuator_Complex = 0x0106,
    CH_Dimming_Actuator_Scene = 0x0107,
    CH_Shutter_Actuator_Basic_Wind = 0x0108,
    CH_ShutterBlinds_Actuator_Basic_Wind = 0x0109,
    CH_Shutter_Actuator_Basic_Rain = 0x010A,
    CH_ShutterBlinds_Actuator_Basic_Rain = 0x010B,
    CH_ShutterBlinds_Actuator_Basic = 0x010C,
    CH_ShutterBlinds_Actuator_Scene = 0x010D,
    CH_Logical_Actuator = 0x010E,

    /* 120h to 1FFh: Other */
    CH_Info_Adaptable = 0x0120,

    /* 200h to 3FFh: functional modules (old) */
    CH_Logic_AndOr = 0x0200,
    CH_8_Scenes_4_Outputs_Basic = 0x0201,
    CH_8_Scenes_4_Outputs_Complex = 0x0202,
    CH_Scene_Converter = 0x0203,
    CH_Event_Scheduler_4_Outputs = 0x0204,
    CH_DateTime_Scheduler = 0x0205,
    CH_System_Clock_Master = 0x0206,
    CH_HVAC_Mode_Scheduler = 0x0207,
    CH_Room_Regulator_Type_A = 0x0208,
    CH_System_Clock_Slave = 0x0209,
    CH_LightSensor_Slave = 0x020A,
    CH_Light_Setpoint_Controller_Info = 0x020B,

    /* 300h to 4FFh: generic channels (old) */
    CH_Generic_Switch1 = 0x0300,
    CH_Generic_Switch2 = 0x0301,
    CH_Generic_Switch3 = 0x0302,
    CH_Generic_Switch4 = 0x0303,
    CH_Generic_Switch_Info1 = 0x0304,
    CH_Generic_Switch_Info2 = 0x0305,
    CH_Generic_Switch_FixedPos1 = 0x0306,
    CH_Generic_Switch_FixedPos2 = 0x0307,
    CH_Generic_PB_Info_1 = 0x0308,
    CH_Generic_PB2 = 0x0309,
    CH_Generic_PB3 = 0x030A,
    CH_Generic_PB4 = 0x030B,
    CH_Generic_PB_1_2_1 = 0x030C, // CH_Generic_PB_1/2_1
    CH_Generic_PB_1_2_Info_1 = 0x030D, // CH_Generic_PB_1/2_Info_1
    CH_Generic_PB_1_2_Info_2 = 0x030E, // CH_Generic_PB_1/2_Info_2
    CH_Generic_Binary_Contact = 0x030F,
    CH_Generic_Switch_5 = 0x311,
    CH_Generic_PB_1_2_Info_3 = 0x0312, // CH_Generic_PB_1/2_Info_3
    CH_Generic_PB_Info_4 = 0x0313, // CH_Generic_PB_Info_4
    CH_Generic_PB_1_2_2 = 0x0314, // CH_Generic_PB_1/2_2
    CH_Generic_Switch_Lightning_Complex_Info = 0x0315,
    CH_Generic_PB_1_2_Info_5 = 0x0316, // CH_Generic_PB_1/2_Info_5

    /* 400h to 43Fh: Binary Switch On/Off */

    /* 440h to 47Fh: Dimming Actuator */

    /* 480h to 4BFh: Shutter */
    CH_ShutterBlinds_Actuator_Scene_1 = 0x0480,

    /* 4C0h to 4FFh: Anti-Intrusion */

    /* 500h to 57Fh: Heating */
    CH_Heating_Valve_Actuator = 0x0501,
    CH_HVACMode_Display = 0x0502,
    CH_Electrical_Heating_Actuator_Type_A = 0x0503,
    CH_Electrical_Heating_Enable_Disable = 0x0504,
    CH_HVAC_Mode_Display2 = 0x0505,
    CH_FAN_COIL_HMI = 0x0507,
    CH_MASTER_FANCOIL_CONTROLLER = 0x0508,
    CH_SLAVE_FANCOIL_CONTROLLER = 0x0509,

    CH_ElectricalEnergy_Tariff_Sensor = 0x0580,
    CH_Energy_Tariff_Display = 0x0581,
    CH_Tariff_Sensor = 0x0582,
    CH_Tariff_Display = 0x0583,
};

}
