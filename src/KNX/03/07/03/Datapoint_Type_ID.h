// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 5 Datapoint Type Identifiers */
#include <KNX/03/07/02_Datapoint_Types.h>
