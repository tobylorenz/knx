// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

// @note All this is contained in ETS MasterData InterfaceObjectTypes

/* 2.1 Assignment scheme */

/* 2.2 Overview System Interface Objects */

/* 2.3 Overview Application Interface Objects */

/* 2.3.1 Assignment scheme */

/* 2.3.2 Overview assigned Object Types for Application Interface Objects */
