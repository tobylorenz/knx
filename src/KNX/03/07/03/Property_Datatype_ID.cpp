// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

uint8_t size(Property_Type property_type)
{
    switch (property_type) {
    case PDT_CONTROL:
        return 1; // 1 in read direction. 10 in write direction.
    case PDT_CHAR:
        return 1;
    case PDT_UNSIGNED_CHAR:
        return 1;
    case PDT_INT:
        return 2;
    case PDT_UNSIGNED_INT:
        return 2;
    case PDT_KNX_FLOAT:
        return 2;
    case PDT_DATE:
        return 3;
    case PDT_TIME:
        return 3;
    case PDT_LONG:
        return 4;
    case PDT_UNSIGNED_LONG:
        return 4;
    case PDT_FLOAT:
        return 4;
    case PDT_DOUBLE:
        return 8;
    case PDT_CHAR_BLOCK:
        return 10;
    case PDT_POLL_GROUP_SETTINGS:
        return 3;
    case PDT_SHORT_CHAR_BLOCK:
        return 5;
    case PDT_DATE_TIME:
        return 8;
    case PDT_VARIABLE_LENGTH:
        break; // variable
    case PDT_GENERIC_01:
        return 1;
    case PDT_GENERIC_02:
        return 2;
    case PDT_GENERIC_03:
        return 3;
    case PDT_GENERIC_04:
        return 4;
    case PDT_GENERIC_05:
        return 5;
    case PDT_GENERIC_06:
        return 6;
    case PDT_GENERIC_07:
        return 7;
    case PDT_GENERIC_08:
        return 8;
    case PDT_GENERIC_09:
        return 9;
    case PDT_GENERIC_10:
        return 10;
    case PDT_GENERIC_11:
        return 11;
    case PDT_GENERIC_12:
        return 12;
    case PDT_GENERIC_13:
        return 13;
    case PDT_GENERIC_14:
        return 14;
    case PDT_GENERIC_15:
        return 15;
    case PDT_GENERIC_16:
        return 16;
    case PDT_GENERIC_17:
        return 17;
    case PDT_GENERIC_18:
        return 18;
    case PDT_GENERIC_19:
        return 19;
    case PDT_GENERIC_20:
        return 20;
    case PDT_UTF8:
        break; // variable
    case PDT_VERSION:
        return 2;
    case PDT_ALARM_INFO:
        return 6;
    case PDT_BINARY_INFORMATION:
        return 1; // 1 bit
    case PDT_BITSET8:
        return 1;
    case PDT_BITSET16:
        return 2;
    case PDT_ENUM8:
        return 1;
    case PDT_SCALING:
        return 1;
    case PDT_NE_VL:
        break; // undefined
    case PDT_NE_FL:
        break; // undefined
    case PDT_FUNCTION:
        break; // usage dependent
    case PDT_ESCAPE:
        break; // defined or undefined
    default:
        assert(false);
    }

    return 0;
}

}
