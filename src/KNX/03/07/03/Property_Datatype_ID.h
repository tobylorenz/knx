// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/07/Property_Type.h>
#include <KNX/types.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Property Datatypes Identifiers
 *
 * @ingroup KNX_03_03_07_03_04_03_03
 * @ingroup KNX_03_07_03_04
 */
enum /*: Property_Type*/ {
    PDT_CONTROL = 0x00,
    PDT_CHAR = 0x01,
    PDT_UNSIGNED_CHAR = 0x02,
    PDT_INT = 0x03,
    PDT_UNSIGNED_INT = 0x04,
    PDT_KNX_FLOAT = 0x05,
    PDT_DATE = 0x06,
    PDT_TIME = 0x07,
    PDT_LONG = 0x08,
    PDT_UNSIGNED_LONG = 0x09,
    PDT_FLOAT = 0x0A,
    PDT_DOUBLE = 0x0B,
    PDT_CHAR_BLOCK = 0x0C,
    PDT_POLL_GROUP_SETTINGS = 0x0D,
    PDT_SHORT_CHAR_BLOCK = 0x0E,
    PDT_DATE_TIME = 0x0F,
    PDT_VARIABLE_LENGTH = 0x10,
    PDT_GENERIC_01 = 0x11,
    PDT_GENERIC_02 = 0x12,
    PDT_GENERIC_03 = 0x13,
    PDT_GENERIC_04 = 0x14,
    PDT_GENERIC_05 = 0x15,
    PDT_GENERIC_06 = 0x16,
    PDT_GENERIC_07 = 0x17,
    PDT_GENERIC_08 = 0x18,
    PDT_GENERIC_09 = 0x19,
    PDT_GENERIC_10 = 0x1A,
    PDT_GENERIC_11 = 0x1B,
    PDT_GENERIC_12 = 0x1C,
    PDT_GENERIC_13 = 0x1D,
    PDT_GENERIC_14 = 0x1E,
    PDT_GENERIC_15 = 0x1F,
    PDT_GENERIC_16 = 0x20,
    PDT_GENERIC_17 = 0x21,
    PDT_GENERIC_18 = 0x22,
    PDT_GENERIC_19 = 0x23,
    PDT_GENERIC_20 = 0x24,
    // 0x25..0x2E: Reserved
    PDT_UTF8 = 0x2F, // PDT_UDT-8
    PDT_VERSION = 0x30,
    PDT_ALARM_INFO = 0x31,
    PDT_BINARY_INFORMATION = 0x32,
    PDT_BITSET8 = 0x33,
    PDT_BITSET16 = 0x34,
    PDT_ENUM8 = 0x35,
    PDT_SCALING = 0x36,
    // 0x37..0x3B: Reserved
    PDT_NE_VL = 0x3C,
    PDT_NE_FL = 0x3D,
    PDT_FUNCTION = 0x3E,
    PDT_ESCAPE = 0x3F
};

KNX_EXPORT uint8_t size(Property_Type property_type);

}
