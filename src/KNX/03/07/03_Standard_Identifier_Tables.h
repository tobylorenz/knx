// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 2 Interface Object Types */
#include <KNX/03/07/03/Interface_Object_Type.h>

/* 3 Property Identifiers */
#include <KNX/03/03/07/Property_Id.h>

/* 3.1 Assignment scheme */

/* 3.2 Interface Object Type independent standard Properties */
#include <KNX/03/05/01/Interface_Object.h>

/* 3.3.1 Device Object Intrface Object (Object Type = 0) */
#include <KNX/03/05/01/00_Device/Device_Object.h>

/* 3.3.2 Polling Master Interface Object (Object Type = 10) */
#include <KNX/03/05/01/10_Polling_Master/Polling_Master_Object.h>

/* 4 Property Datatypes Identifiers */
#include <KNX/03/07/03/Property_Datatype_ID.h>

/* 5 Datapoint Type Identifiers */
#include <KNX/03/07/03/Datapoint_Type_ID.h>

/* 6 Identifiers from E-Mode */

/* 6.1 Channel Codes */
#include <KNX/03/07/03/Channel_Code.h>

/* 6.2 Connection Codes */
#include <KNX/03/07/03/Connection_Code.h>
