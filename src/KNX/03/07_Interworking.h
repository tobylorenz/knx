// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 Interworking Model */
#include <KNX/03/07/01_Interworking_Model.h>

/* 2 Datapoint Types */
#include <KNX/03/07/02_Datapoint_Types.h>

/* 3 Standard Identifier Tables */
#include <KNX/03/07/03_Standard_Identifier_Tables.h>
