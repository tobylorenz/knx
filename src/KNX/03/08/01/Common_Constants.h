// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Identifier for KNXnet/IP protocol version 1.0
 *
 * @ingroup KNX_03_08_01_05_02
 */
enum : uint8_t {
    KNXNETIP_VERSION_10 = 0x10,
    KNXNETIP_VERSION_13 = 0x13, // see AN159 KNX/IP secure
};

/**
 * Constant size of KNXnet/IP header as defined in protocol version 1.0
 *
 * @ingroup KNX_03_08_01_05_02
 */
enum : uint8_t {
    HEADER_SIZE_10 = 0x06,
};

}
