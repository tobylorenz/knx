// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Connection type
 *
 * @ingroup KNX_03_08_01_05_04
 * @ingroup KNX_03_08_02_07_08_01
 */
enum class Connection_Type_Code : uint8_t {
    /** Undefined */
    UNDEFINED = 0x00,

    // Overview = 0x01,

    // Core = 0x02,

    /** Device Management Connection */
    DEVICE_MGMT_CONNECTION = 0x03,

    /** Tunnelling Connection */
    TUNNEL_CONNECTION = 0x04,

    /** Routing Connection */
    ROUTING_CONNECTION = 0x05,  // @note the constant name is not in the spec

    /** Remote Logging Connection */
    REMLOG_CONNECTION = 0x06,

    /** Remote Configuration and Diagnosis Connection */
    REMCONF_CONNECTION = 0x07,

    /** Object Server Connection */
    OBJSVR_CONNECTION = 0x08,

    // SECURITY_CONNECTION = 0x09,
};

}
