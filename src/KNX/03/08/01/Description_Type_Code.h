// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Description type code
 *
 * @ingroup KNX_03_08_01_05_06
 * @ingroup KNX_03_08_02_07_05_04_01
 * @ingroup KNX_AN185_02_04_01_02
 */
enum class Description_Type_Code : uint8_t {
    /** Undefined */
    UNDEFINED = 0x00,

    /** Device Information */
    DEVICE_INFO = 0x01,

    /** Supported Service Families */
    SUPP_SVC_FAMILIES = 0x02,

    /** IP Configuration*/
    IP_CONFIG = 0x03,

    /** Current IP Configuration */
    IP_CUR_CONFIG = 0x04,

    /** KNX Addresses */
    KNX_ADDRESSES = 0x05,

    /** Secured Services */
    SECURED_SERVICES = 0x06,

    /** Tunnelling Information */
    TUNNELLING_INFO = 0x07,

    /** Extended Device Information */
    EXTENDED_DEVICE_INFO = 0x08,

    // 0x09..0xFD: Reserved for future use.

    /** Manufacturer Data */
    MFR_DATA = 0xFE,

    // 0xFF: Not used.
};

}
