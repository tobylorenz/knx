// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Error codes
 *
 * @ingroup KNX_03_08_01_05_05
 * @ingroup KNX_AN184_02_08_01
 * @ingroup KNX_AN185_02_08_03
 */
enum class Error_Code : uint8_t {
    /* Common */
    E_NO_ERROR = 0x00,
    E_HOST_PROTOCOL_TYPE = 0x01,
    E_VERSION_NOT_SUPPORTED = 0x02,
    // 0x03
    E_SEQUENCE_NUMBER = 0x04,
    E_ERROR = 0x0F,

    /* Service specific */
    E_CONNECTION_ID = 0x21,
    E_CONNECTION_TYPE = 0x22,
    E_CONNECTION_OPTION = 0x23,
    E_NO_MORE_CONNECTIONS = 0x24,
    E_NO_MORE_UNIQUE_CONNECTIONS = 0x25,
    E_DATA_CONNECTION = 0x26,
    E_KNX_CONNECTION = 0x27,
    E_AUTHORISATION_ERROR = 0x28,
    E_TUNNELING_LAYER = 0x29,
    E_NO_TUNNELLING_ADDRESS = 0x2D,
    E_CONNECTION_IN_USE = 0x2E,
};

}
