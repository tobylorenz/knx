// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Host protocol code
 *
 * @ingroup KNX_03_08_01_05_07
 * @ingroup KNX_03_08_02_08_06_02
 */
enum class Host_Protocol_Code : uint8_t {
    /** Undefined */
    UNDEFINED = 0x00,

    /** IPv4 UDP */
    IPV4_UDP = 0x01,

    /** IPv4 TCP */
    IPV4_TCP = 0x02,

    // IPV6_UDP to be defined

    // IPV6_TCP to be defined
};

}
