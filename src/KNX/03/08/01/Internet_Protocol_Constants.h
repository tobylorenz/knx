// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Port Numnber
 *
 * @ingroup KNX_03_08_01_05_09
 */
constexpr uint16_t Port_Number{3671};

/**
 * System Setup Multicast Address
 *
 * @ingroup KNX_03_08_01_05_09
 */
constexpr std::array<uint8_t, 4> System_Setup_Multicast_Address{{224, 0, 23, 12}};

}
