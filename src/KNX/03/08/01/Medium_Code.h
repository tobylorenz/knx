// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

//#include <KNX/03/07/02/22/DPT_22_1000.h>
#include <KNX/types.h>

namespace KNX {

/**
 * KNX medium code
 *
 * @ingroup KNX_03_08_01_05_06
 * @ingroup KNX_03_08_02_07_05_04_02
 *
 * @see DPT_22_1000
 */
enum class Medium_Code : uint8_t {
    /** Undefined */
    UNDEFINED = 0x00,

    /** TP0 / reserved */
    TP0 = 0x01,

    /** TP1 */
    TP1 = 0x02,

    /** PL110 */
    PL110 = 0x04,

    /** PL132 / reserved */
    PL132 = 0x08,

    /** RF */
    RF = 0x10,

    /** IP */
    IP = 0x20,
};

}
