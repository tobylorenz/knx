// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Service type number
 *
 * @ingroup KNX_03_08_01_05_03
 */
enum class Service_Type_Identifier : uint16_t {
    UNDEFINED = 0x0000,

    /* Core */
    SEARCH_REQUEST = 0x0201,
    SEARCH_RESPONSE = 0x202,
    DESCRIPTION_REQUEST = 0x0203,
    DESCRIPTION_RESPONSE = 0x0204,
    CONNECT_REQUEST = 0x0205,
    CONNECT_RESPONSE = 0x0206,
    CONNECTIONSTATE_REQUEST = 0x0207,
    CONNECTIONSTATE_RESPONSE = 0x0208,
    DISCONNECT_REQUEST = 0x0209,
    DISCONNECT_RESPONSE = 0x020A,
    SEARCH_REQUEST_EXT = 0x020B,
    SEARCH_RESPONSE_EXT = 0x020C,

    /* Device Management */
    DEVICE_CONFIGURATION_REQUEST = 0x0310,
    DEVICE_CONFIGURATION_ACK = 0x0311,

    /* Tunnelling */
    TUNNELING_REQUEST = 0x0420,
    TUNNELING_ACK = 0x0421,
    TUNNELING_FEATURE_GET = 0x0422,
    TUNNELING_FEATURE_RESPONSE = 0x0423,
    TUNNELING_FEATURE_SET = 0x0424,
    TUNNELING_FEATURE_INFO = 0x0425,

    /* Routing */
    ROUTING_INDICATION = 0x0530,
    ROUTING_LOST_MESSAGE = 0x0531,
    ROUTING_BUSY = 0x0532, // see 3/8/5
    ROUTING_SYSTEM_BROADCAST = 0x0533, // see WireShark

    /* Remote Logging */

    /* Remote Configuration and Diagnosis (see 3/8/7) */
    REMOTE_DIAGNOSTIC_REQUEST = 0x0740,
    REMOTE_DIAGNOSTIC_RESPONSE = 0x0741,
    REMOTE_BASIC_CONFIGURATION_REQUEST = 0x0742,
    REMOTE_RESET_REQUEST = 0x0743,

    /* Object Server */

    /* Security */
    //SECURE_WRAPPER = 0x0950, // see WireShark
    //SECURE_SESSION_REQUEST = 0x0951, // see WireShark
    //SECURE_SESSION_RESPONSE = 0x0952, // see WireShark
    //SECURE_SESSION_AUTHENTICATE = 0x0953, // see WireShark
    //SECURE_SESSION_STATUS = 0x0954, // see WireShark
    //SECURE_TIMER_NOTIFY = 0x0955, // see WireShark
    SECURE_WRAPPER = 0xAA00,
    SECURE_CHANNEL_REQUEST = 0xAA01,
    SECURE_CHANNEL_RESPONSE = 0xAA02,
    SECURE_CHANNEL_AUTHORIZE = 0xAA03,
    SECURE_CHANNEL_STATUS = 0xAA04,
    // 0xAA05,
    SECURE_GROUP_SYNC_REQUEST = 0xAA06,
    SECURE_GROUP_SYNC_RESPONSE = 0xAA07,
    // @todo from AN159 IP Secure to WireShark standard
};

}
