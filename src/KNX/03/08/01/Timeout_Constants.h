// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <chrono>

namespace KNX {

/**
 * Search Timeout
 *
 * @ingroup KNX_03_08_02_04_02
 */
constexpr const std::chrono::seconds SEARCH_TIMEOUT{2}; // @todo Where is this defined? Does this come from IPRoutingLatency?

/**
 * Connect Request Timeout
 *
 * @ingroup KNX_03_08_01_05_08
 * @ingroup KNX_03_08_02_05_02
 */
constexpr const std::chrono::seconds CONNECT_REQUEST_TIMEOUT{10};

/**
 * Connectionstate Request Timeout
 *
 * @ingroup KNX_03_08_01_05_08
 * @ingroup KNX_03_08_02_05_04
 */
constexpr const std::chrono::seconds CONNECTIONSTATE_REQUEST_TIMEOUT{10};

/**
 * Device Configuration Request Timeout
 *
 * @ingroup KNX_03_08_01_05_08
 * @ingroup KNX_03_08_03_02_03_02
 */
constexpr const std::chrono::seconds DEVICE_CONFIGURATION_REQUEST_TIMEOUT{10};

/**
 * Tunneling Request Timeout
 *
 * @ingroup KNX_03_08_01_05_08
 * @ingroup KNX_03_08_04_02_06
 */
constexpr const std::chrono::seconds TUNNELLING_REQUEST_TIMEOUT{1};

/**
 * Connection Alive Timeout
 *
 * @ingroup KNX_03_08_01_05_08
 * @ingroup KNX_03_08_02_05_04
 */
constexpr const std::chrono::seconds CONNECTION_ALIVE_TIME{120};

}
