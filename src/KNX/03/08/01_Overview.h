// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 5.2 Common constants */
#include <KNX/03/08/01/Common_Constants.h>

/* 5.3 KNXnet/IP services */
#include <KNX/03/08/01/Services.h>

/* 5.4 Connection types */
#include <KNX/03/08/01/Connection_Types.h>

/* 5.5 Error codes */
#include <KNX/03/08/01/Error_Codes.h>

/* 5.6 Description Information Block (DIB) */
#include <KNX/03/08/01/Description_Type_Code.h>
#include <KNX/03/08/01/Medium_Code.h>

/* 5.7 Host protocol codes */
#include <KNX/03/08/01/Host_Protocol_Codes.h>

/* 5.8 Timeout constants */
#include <KNX/03/08/01/Timeout_Constants.h>

/* 5.9 Internet Protocol constants */
#include <KNX/03/08/01/Internet_Protocol_Constants.h>
