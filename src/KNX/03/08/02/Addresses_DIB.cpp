// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Addresses_DIB.h>

#include <cassert>

namespace KNX {

Addresses_DIB::Addresses_DIB() :
    Description_Information_Block()
{
    description_type_code = Description_Type_Code::KNX_ADDRESSES;
}

void Addresses_DIB::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 4);

    Description_Information_Block::fromData(first, first + 2);
    first += 2;

    individual_address.fromData(first, first + 2);
    first += 2;

    while (first != last) {
        additional_individual_addresses.push_back(Individual_Address((*first++ << 8) | *first++));
    }

    assert(first == last);
}

std::vector<uint8_t> Addresses_DIB::toData() const
{
    std::vector<uint8_t> data = Description_Information_Block::toData();

    data.push_back(individual_address >> 8);
    data.push_back(individual_address & 0xff);
    for (const auto & aia : additional_individual_addresses) {
        data.push_back(aia >> 8);
        data.push_back(aia & 0xff);
    }

    return data;
}

uint8_t Addresses_DIB::structure_length_calculated() const
{
    return
        Description_Information_Block::structure_length_calculated() +
        2 +
        static_cast<uint8_t>(2 * additional_individual_addresses.size());
}

}
