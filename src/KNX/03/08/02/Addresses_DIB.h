// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/05/01/11_IP_Parameter/Additional_Individual_Addresses_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Individual_Address_Property.h>
#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * KNX Addresses DIB
 *
 * @ingroup KNX_03_08_02_07_05_04_06
 * @todo Most values are kept in IP_Parameter_Object
 */
class KNX_EXPORT Addresses_DIB : public Description_Information_Block
{
public:
    Addresses_DIB();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    Individual_Address individual_address{}; // @see Individual_Address_Property
    std::vector<Individual_Address> additional_individual_addresses{}; // @see Additional_Individual_Addresses_Property
};

}
