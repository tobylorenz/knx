// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Connect_Request_Frame.h>

#include <cassert>

#include <KNX/03/08/01/Common_Constants.h>

namespace KNX {

Connect_Request_Frame::Connect_Request_Frame() :
    Frame(Service_Type_Identifier::CONNECT_REQUEST)
{
}

void Connect_Request_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 6));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 6);

    uint8_t structure_length = *first;
    control_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    assert(std::distance(first, last) >= 4);

    structure_length = *first;
    data_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    assert(std::distance(first, last) >= 2);

    structure_length = *first;
    connection_request_information = make_Connection_Request_Information(first, first + structure_length);
    first += structure_length;

    assert(first == last);
}

std::vector<uint8_t> Connect_Request_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    std::vector<uint8_t> control_endpoint_data = control_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(control_endpoint_data), std::cend(control_endpoint_data));

    std::vector<uint8_t> data_endpoint_data = data_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(data_endpoint_data), std::cend(data_endpoint_data));

    std::vector<uint8_t> connection_request_information_data = connection_request_information->toData();
    data.insert(std::cend(data), std::cbegin(connection_request_information_data), std::cend(connection_request_information_data));

    return data;
}

uint16_t Connect_Request_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        control_endpoint->structure_length_calculated() +
        data_endpoint->structure_length_calculated() +
        connection_request_information->structure_length_calculated();
}

}
