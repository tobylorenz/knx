// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Connection_Request_Information.h>
#include <KNX/03/08/02/Frame.h>
#include <KNX/03/08/02/Host_Protocol_Address_Information.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Connect Request Frame
 *
 * @ingroup KNX_03_08_02_07_08_01
 */
class KNX_EXPORT Connect_Request_Frame : public Frame
{
public:
    Connect_Request_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    std::shared_ptr<Host_Protocol_Address_Information> control_endpoint{};
    std::shared_ptr<Host_Protocol_Address_Information> data_endpoint{};
    std::shared_ptr<Connection_Request_Information> connection_request_information{};
};

}
