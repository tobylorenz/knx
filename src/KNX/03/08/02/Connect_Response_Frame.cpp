// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Connect_Response_Frame.h>

#include <cassert>

namespace KNX {

Connect_Response_Frame::Connect_Response_Frame() :
    Frame(Service_Type_Identifier::CONNECT_RESPONSE)
{
}

void Connect_Response_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 2));

    Frame::fromData(first, first + 6);
    first += header_length;

    assert(std::distance(first, last) >= 2);

    communication_channel_id = *first++;

    status = static_cast<Error_Code>(*first++);

    if (status == Error_Code::E_NO_ERROR) {
        assert(std::distance(first, last) >= 4);

        uint8_t structure_length = *first;
        data_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
        first += structure_length;

        assert(std::distance(first, last) >= 2);

        structure_length = *first;
        connection_response_data_block = make_Connection_Response_Data_Block(first, first + structure_length);
        first += structure_length;
    }

    // assert(first == last); taken out as examples show trailing padding bytes are possible
}

std::vector<uint8_t> Connect_Response_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    data.push_back(communication_channel_id);
    data.push_back(static_cast<uint8_t>(status));

    if (status == Error_Code::E_NO_ERROR) {
        std::vector<uint8_t> data_endpoint_data = data_endpoint->toData();
        data.insert(std::cend(data), std::cbegin(data_endpoint_data), std::cend(data_endpoint_data));

        std::vector<uint8_t> connection_response_data_block_data = connection_response_data_block->toData();
        data.insert(std::cend(data), std::cbegin(connection_response_data_block_data), std::cend(connection_response_data_block_data));
    }

    return data;
}

uint16_t Connect_Response_Frame::total_length_calculated() const
{
    uint16_t total_length =
        Frame::total_length_calculated() +
        2;
    if (status == Error_Code::E_NO_ERROR) {
        total_length += data_endpoint->structure_length_calculated();
        total_length += connection_response_data_block->structure_length_calculated();
    }
    return total_length;
}

}
