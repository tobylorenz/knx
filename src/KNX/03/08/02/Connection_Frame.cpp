// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Connection_Frame.h>

#include <cassert>

namespace KNX {

Connection_Frame::Connection_Frame() :
    Frame()
{
}

Connection_Frame::Connection_Frame(const Service_Type_Identifier service_type_identifier) :
    Frame(service_type_identifier)
{
}

void Connection_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 4));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 4);

    structure_length = *first++;

    communication_channel_id = *first++;

    sequence_counter = *first++;

    service_type_specific = *first++;
}

std::vector<uint8_t> Connection_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    data.push_back(structure_length_calculated());
    data.push_back(communication_channel_id);
    data.push_back(sequence_counter);
    data.push_back(service_type_specific);

    return data;
}

uint16_t Connection_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        structure_length_calculated();
}

uint8_t Connection_Frame::structure_length_calculated() const
{
    return 4;
}

}
