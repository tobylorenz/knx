// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Communication_Channel_Id.h>
#include <KNX/03/08/02/Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Connection Frame
 *
 * @ingroup KNX_03_08_02_05_03
 */
class KNX_EXPORT Connection_Frame : public Frame
{
public:
    Connection_Frame();
    explicit Connection_Frame(const Service_Type_Identifier service_type_identifier);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    virtual uint8_t structure_length_calculated() const;

    uint8_t structure_length{4};
    Communication_Channel_Id communication_channel_id{};
    uint8_t sequence_counter{};
    uint8_t service_type_specific{};

    // connection type specific header (variable length, optional)
};

}
