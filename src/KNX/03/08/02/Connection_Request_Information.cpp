// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Connection_Request_Information.h>

#include <cassert>

#include <KNX/03/08/04/Tunnelling_Connection_Request_Information.h>

namespace KNX {

Connection_Request_Information::Connection_Request_Information()
{
}

Connection_Request_Information::Connection_Request_Information(const Connection_Type_Code connection_type_code) :
    connection_type_code(connection_type_code)
{
}

void Connection_Request_Information::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    structure_length = *first++;

    connection_type_code = static_cast<Connection_Type_Code>(*first++);

    assert(first == last);
}

std::vector<uint8_t> Connection_Request_Information::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(structure_length_calculated());
    data.push_back(static_cast<uint8_t>(connection_type_code));

    return data;
}

uint8_t Connection_Request_Information::structure_length_calculated() const
{
    return 2;
}

std::shared_ptr<Connection_Request_Information> make_Connection_Request_Information(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    Connection_Request_Information cri;
    cri.fromData(first, first + 2);
    switch (cri.connection_type_code) {
    case Connection_Type_Code::UNDEFINED:
        break;
    case Connection_Type_Code::DEVICE_MGMT_CONNECTION:
        break;
    case Connection_Type_Code::TUNNEL_CONNECTION: {
        std::shared_ptr<Tunnelling_Connection_Request_Information> cri2 = std::make_shared<Tunnelling_Connection_Request_Information>();
        cri2->fromData(first, last);
        return cri2;
    }
    break;
    case Connection_Type_Code::ROUTING_CONNECTION:
        break;
    case Connection_Type_Code::REMLOG_CONNECTION:
        break;
    case Connection_Type_Code::REMCONF_CONNECTION:
        break;
    case Connection_Type_Code::OBJSVR_CONNECTION:
        break;
    }
    return std::make_shared<Connection_Request_Information>(cri);
}

}
