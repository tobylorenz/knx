// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/08/01/Connection_Types.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Connection Request Information (CRI)
 *
 * @ingroup KNX_03_08_02_07_05_02
 */
class KNX_EXPORT Connection_Request_Information
{
public:
    Connection_Request_Information();
    explicit Connection_Request_Information(const Connection_Type_Code connection_type_code);

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    virtual uint8_t structure_length_calculated() const;

    uint8_t structure_length{2};
    Connection_Type_Code connection_type_code{Connection_Type_Code::UNDEFINED};

    // host protocol independent data (variable length, optional)

    // host protocol dependent data (variable length, optional)
};

KNX_EXPORT std::shared_ptr<Connection_Request_Information> make_Connection_Request_Information(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
