// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Connection_Response_Data_Block.h>

#include <cassert>

#include <KNX/03/08/04/Tunnelling_Connection_Response_Data_Block.h>

namespace KNX {

void Connection_Response_Data_Block::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    structure_length = *first++;
    connection_type_code = static_cast<Connection_Type_Code>(*first++);

    assert(first == last);
}

std::vector<uint8_t> Connection_Response_Data_Block::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(structure_length_calculated());
    data.push_back(static_cast<uint8_t>(connection_type_code));

    return data;
}

uint8_t Connection_Response_Data_Block::structure_length_calculated() const
{
    return 2;
}

std::shared_ptr<Connection_Response_Data_Block> make_Connection_Response_Data_Block(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    Connection_Response_Data_Block crdb;
    crdb.fromData(first, first + 2);

    switch (crdb.connection_type_code) {
    case Connection_Type_Code::UNDEFINED:
        break;
    case Connection_Type_Code::DEVICE_MGMT_CONNECTION:
        break;
    case Connection_Type_Code::TUNNEL_CONNECTION: {
        std::shared_ptr<Tunnelling_Connection_Response_Data_Block> crdb2 = std::make_shared<Tunnelling_Connection_Response_Data_Block>();
        crdb2->fromData(first, last);
        return crdb2;
    }
    break;
    case Connection_Type_Code::ROUTING_CONNECTION:
        break;
    case Connection_Type_Code::REMLOG_CONNECTION:
        break;
    case Connection_Type_Code::REMCONF_CONNECTION:
        break;
    case Connection_Type_Code::OBJSVR_CONNECTION:
        break;
    }
    return std::make_shared<Connection_Response_Data_Block>(crdb);
}

}
