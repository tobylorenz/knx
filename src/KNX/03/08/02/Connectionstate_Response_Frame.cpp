// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Connectionstate_Response_Frame.h>

#include <cassert>

namespace KNX {

Connectionstate_Response_Frame::Connectionstate_Response_Frame() :
    Frame(Service_Type_Identifier::CONNECTIONSTATE_RESPONSE)
{
}

void Connectionstate_Response_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 2));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) == 2);

    communication_channel_id = *first++;

    status = static_cast<Error_Code>(*first++);

    assert(first == last);
}

std::vector<uint8_t> Connectionstate_Response_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    data.push_back(communication_channel_id);
    data.push_back(static_cast<uint8_t>(status));

    return data;
}

uint16_t Connectionstate_Response_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        2;
}

}
