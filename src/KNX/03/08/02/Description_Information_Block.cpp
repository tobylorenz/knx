// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Description_Information_Block.h>

#include <cassert>

#include <KNX/03/08/02/Addresses_DIB.h>
#include <KNX/03/08/02/Device_Information_DIB.h>
#include <KNX/03/08/02/IP_Config_DIB.h>
#include <KNX/03/08/02/IP_Current_Config_DIB.h>
#include <KNX/03/08/02/Manufacturer_Data_DIB.h>
#include <KNX/03/08/02/Supported_Service_Families_DIB.h>
#include <KNX/03/08/04/Extended_Device_Information_DIB.h>
#include <KNX/03/08/04/Tunnelling_Information_DIB.h>

namespace KNX {

void Description_Information_Block::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    structure_length = *first++;

    description_type_code = static_cast<Description_Type_Code>(*first++);

    assert(first == last);
}

std::vector<uint8_t> Description_Information_Block::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(structure_length_calculated());
    data.push_back(static_cast<uint8_t>(description_type_code));

    return data;
}

uint8_t Description_Information_Block::structure_length_calculated() const
{
    return 2;
}

std::shared_ptr<Description_Information_Block> make_Description_Information_Block(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    Description_Information_Block dib;
    dib.fromData(first, first + 2);

    switch (dib.description_type_code) {
    case Description_Type_Code::UNDEFINED:
        break;
    case Description_Type_Code::DEVICE_INFO: {
        std::shared_ptr<Device_Information_DIB> dib2 = std::make_shared<Device_Information_DIB>();
        dib2->fromData(first, last);
        return dib2;
    }
    break;
    case Description_Type_Code::SUPP_SVC_FAMILIES: {
        std::shared_ptr<Supported_Service_Families_DIB> dib2 = std::make_shared<Supported_Service_Families_DIB>();
        dib2->fromData(first, last);
        return dib2;
    }
    break;
    case Description_Type_Code::IP_CONFIG: {
        std::shared_ptr<IP_Config_DIB> dib2 = std::make_shared<IP_Config_DIB>();
        dib2->fromData(first, last);
        return dib2;
    }
    break;
    case Description_Type_Code::IP_CUR_CONFIG: {
        std::shared_ptr<IP_Current_Config_DIB> dib2 = std::make_shared<IP_Current_Config_DIB>();
        dib2->fromData(first, last);
        return dib2;
    }
    break;
    case Description_Type_Code::KNX_ADDRESSES: {
        std::shared_ptr<Addresses_DIB> dib2 = std::make_shared<Addresses_DIB>();
        dib2->fromData(first, last);
        return dib2;
    }
    break;
    case Description_Type_Code::SECURED_SERVICES: {
        // @todo SECURED_SERVICES
    }
    break;
    case Description_Type_Code::TUNNELLING_INFO: {
        std::shared_ptr<Tunnelling_Information_DIB> dib2 = std::make_shared<Tunnelling_Information_DIB>();
        dib2->fromData(first, last);
        return dib2;
    }
    break;
    case Description_Type_Code::EXTENDED_DEVICE_INFO: {
        std::shared_ptr<Extended_Device_Information_DIB> dib2 = std::make_shared<Extended_Device_Information_DIB>();
        dib2->fromData(first, last);
        return dib2;
    }
    break;
    case Description_Type_Code::MFR_DATA: {
        std::shared_ptr<Manufacturer_Data_DIB> dib2 = std::make_shared<Manufacturer_Data_DIB>();
        dib2->fromData(first, last);
        return dib2;
    }
    break;
    }
    return nullptr;
}

}
