// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/08/01/Description_Type_Code.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Description Information Block (DIB)
 *
 * @ingroup KNX_03_08_02_07_05_04_01
 */
class KNX_EXPORT Description_Information_Block
{
public:
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    virtual uint8_t structure_length_calculated() const;

    /** Structure Length */
    uint8_t structure_length{4};

    /** Descriptor Type Code */
    Description_Type_Code description_type_code{Description_Type_Code::UNDEFINED};
};

KNX_EXPORT std::shared_ptr<Description_Information_Block> make_Description_Information_Block(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
