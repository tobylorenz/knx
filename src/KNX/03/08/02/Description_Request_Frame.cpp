// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Description_Request_Frame.h>

#include <cassert>

#include <KNX/03/08/01/Common_Constants.h>

namespace KNX {

Description_Request_Frame::Description_Request_Frame() :
    Frame(Service_Type_Identifier::DESCRIPTION_REQUEST)
{
}

void Description_Request_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 2));

    Frame::fromData(first, last);
    first += header_length;

    std::uint8_t structure_length = *first;
    control_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    assert(first == last);
}

std::vector<uint8_t> Description_Request_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    std::vector<uint8_t> control_endpoint_data = control_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(control_endpoint_data), std::cend(control_endpoint_data));

    return data;
}

uint16_t Description_Request_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        control_endpoint->structure_length_calculated();
}

}
