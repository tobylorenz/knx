// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Description_Response_Frame.h>

#include <cassert>
#include <numeric>

namespace KNX {

Description_Response_Frame::Description_Response_Frame() :
    Frame(Service_Type_Identifier::DESCRIPTION_RESPONSE)
{
}

void Description_Response_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 4));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 4);

    std::uint8_t structure_length = *first;
    device_hardware = make_Device_Information_DIB(first, first + structure_length);
    first += structure_length;

    assert(std::distance(first, last) >= 2);

    structure_length = *first;
    supported_service_families = make_Supported_Service_Families_DIB(first, first + structure_length);
    first += structure_length;

    while (first != last) {
        structure_length = *first;
        other_device_information.push_back(make_Description_Information_Block(first, first + structure_length));
        first += structure_length;
    }

    assert(first == last);
}

std::vector<uint8_t> Description_Response_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    std::vector<uint8_t> device_hardware_data = device_hardware->toData();
    data.insert(std::cend(data), std::cbegin(device_hardware_data), std::cend(device_hardware_data));

    std::vector<uint8_t> supported_service_families_data = supported_service_families->toData();
    data.insert(std::cend(data), std::cbegin(supported_service_families_data), std::cend(supported_service_families_data));

    for (const auto & odi : other_device_information) {
        std::vector<uint8_t> other_device_information_data = odi->toData();
        data.insert(std::cend(data), std::cbegin(other_device_information_data), std::cend(other_device_information_data));
    }

    return data;
}

uint16_t Description_Response_Frame::total_length_calculated() const
{
    return std::accumulate(
               std::cbegin(other_device_information),
               std::cend(other_device_information),
               Frame::total_length_calculated() +
               device_hardware->structure_length_calculated() +
               supported_service_families->structure_length_calculated(),
    [](uint16_t total_length, std::shared_ptr<Description_Information_Block> odi) {
        return std::move(total_length) + odi->structure_length_calculated();
    });
}

}
