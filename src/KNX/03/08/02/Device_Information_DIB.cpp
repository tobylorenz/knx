// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Device_Information_DIB.h>

#include <cassert>

namespace KNX {

Device_Information_DIB::Device_Information_DIB() :
    Description_Information_Block()
{
    description_type_code = Description_Type_Code::DEVICE_INFO;
}

void Device_Information_DIB::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 54);

    Description_Information_Block::fromData(first, first + 2);
    first += 2;

    medium = static_cast<Medium_Code>(*first++);

    device_status = *first++;

    individual_address.fromData(first, first + 2);
    first += 2;

    project_installation_identifier = (*first++ << 8) | *first++;

    std::copy(first, first + 6, std::begin(device_serial_number.serial_number));
    first += 6;

    std::copy(first, first + 4, std::begin(device_routing_multicast_address));
    first += 4;

    std::copy(first, first + 6, std::begin(device_mac_address));
    first += 6;

    std::copy(first, first + 30, std::begin(device_friendly_name));
    first += 30;

    assert(first == last);
}

std::vector<uint8_t> Device_Information_DIB::toData() const
{
    std::vector<uint8_t> data = Description_Information_Block::toData();

    data.push_back(static_cast<uint8_t>(medium));
    data.push_back(device_status);
    data.push_back(individual_address >> 8);
    data.push_back(individual_address & 0xff);
    data.push_back(project_installation_identifier >> 8);
    data.push_back(project_installation_identifier & 0xff);
    data.insert(std::cend(data), std::cbegin(device_serial_number.serial_number), std::cend(device_serial_number.serial_number));
    data.insert(std::cend(data), std::cbegin(device_routing_multicast_address), std::cend(device_routing_multicast_address));
    data.insert(std::cend(data), std::cbegin(device_mac_address), std::cend(device_mac_address));
    data.insert(std::cend(data), std::cbegin(device_friendly_name), std::cend(device_friendly_name));

    return data;
}

uint8_t Device_Information_DIB::structure_length_calculated() const
{
    return
        Description_Information_Block::structure_length_calculated() +
        52;
}

std::shared_ptr<Device_Information_DIB> make_Device_Information_DIB(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    std::shared_ptr<Device_Information_DIB> dib = std::make_shared<Device_Information_DIB>();
    dib->fromData(first, last);
    return dib;
}

}
