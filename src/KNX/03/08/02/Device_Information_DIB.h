// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>
#include <memory>

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/03/07/Serial_Number.h>
#include <KNX/03/05/01/00_Device/Programming_Mode_Property.h>
#include <KNX/03/05/01/08_CEMI_Server/Medium_Type_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Friendly_Name_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Individual_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/MAC_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Project_Installation_Identification_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Routing_Multicast_Address_Property.h>
#include <KNX/03/05/01/generic/Serial_Number_Property.h>
#include <KNX/03/08/01/Medium_Code.h>
#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Device Information DIB
 *
 * @ingroup KNX_03_08_02_07_05_04_02
 * @todo Most values are kept in IP_Parameter_Object
 */
class KNX_EXPORT Device_Information_DIB :
    public Description_Information_Block
{
public:
    Device_Information_DIB();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    Medium_Code medium{Medium_Code::UNDEFINED}; // @see Medium_Type_Property
    uint8_t device_status{}; // @see Programming_Mode_Property
    Individual_Address individual_address{}; // @see Individual_Address_Property
    uint16_t project_installation_identifier{}; // @see Project_Installation_Identification_Property
    Serial_Number device_serial_number{}; // @see Serial_Number_Property
    std::array<uint8_t, 4> device_routing_multicast_address{}; // @see Routing_Multicast_Address_Property
    std::array<uint8_t, 6> device_mac_address{}; // @see MAC_Address_Property
    std::array<char, 30> device_friendly_name{}; // @see Friendly_Name_Property
};

KNX_EXPORT std::shared_ptr<Device_Information_DIB> make_Device_Information_DIB(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
