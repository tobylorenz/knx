// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Disconnect_Request_Frame.h>

#include <cassert>

#include <KNX/03/08/01/Common_Constants.h>

namespace KNX {

Disconnect_Request_Frame::Disconnect_Request_Frame() :
    Frame(Service_Type_Identifier::DISCONNECT_REQUEST)
{
}

void Disconnect_Request_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 4));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 4);

    communication_channel_id = *first++;

    ++first; // reserved

    uint8_t structure_length = *first;
    control_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    // assert(first == last); taken out as examples show trailing padding bytes are allowed
}

std::vector<uint8_t> Disconnect_Request_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    data.push_back(communication_channel_id);
    data.push_back(0);

    std::vector<uint8_t> control_endpoint_data = control_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(control_endpoint_data), std::cend(control_endpoint_data));

    return data;
}

uint16_t Disconnect_Request_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        2 +
        control_endpoint->structure_length_calculated();
}

}
