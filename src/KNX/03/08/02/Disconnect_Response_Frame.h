// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/01/Error_Codes.h>
#include <KNX/03/08/02/Communication_Channel_Id.h>
#include <KNX/03/08/02/Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Disconnect Response Frame
 *
 * @ingroup KNX_03_08_02_07_08_06
 */
class KNX_EXPORT Disconnect_Response_Frame : public Frame
{
public:
    Disconnect_Response_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    Communication_Channel_Id communication_channel_id{};
    Error_Code status{Error_Code::E_NO_ERROR};
};

}
