// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Frame.h>

#include <cassert>

#include <KNX/03/08/02/Connect_Request_Frame.h>
#include <KNX/03/08/02/Connect_Response_Frame.h>
#include <KNX/03/08/02/Connectionstate_Request_Frame.h>
#include <KNX/03/08/02/Connectionstate_Response_Frame.h>
#include <KNX/03/08/02/Description_Request_Frame.h>
#include <KNX/03/08/02/Description_Response_Frame.h>
#include <KNX/03/08/02/Disconnect_Request_Frame.h>
#include <KNX/03/08/02/Disconnect_Response_Frame.h>
#include <KNX/03/08/02/Search_Request_Extended_Frame.h>
#include <KNX/03/08/02/Search_Request_Frame.h>
#include <KNX/03/08/02/Search_Response_Extended_Frame.h>
#include <KNX/03/08/02/Search_Response_Frame.h>
#include <KNX/03/08/03/Device_Configuration_Ack_Frame.h>
#include <KNX/03/08/03/Device_Configuration_Request_Frame.h>
#include <KNX/03/08/04/Tunnelling_Ack_Frame.h>
#include <KNX/03/08/04/Tunnelling_Feature_Get_Frame.h>
#include <KNX/03/08/04/Tunnelling_Feature_Info_Frame.h>
#include <KNX/03/08/04/Tunnelling_Feature_Response_Frame.h>
#include <KNX/03/08/04/Tunnelling_Feature_Set_Frame.h>
#include <KNX/03/08/04/Tunnelling_Request_Frame.h>
#include <KNX/03/08/05/Routing_Busy_Frame.h>
#include <KNX/03/08/05/Routing_Indication_Frame.h>
#include <KNX/03/08/05/Routing_Lost_Message_Frame.h>
#include <KNX/03/08/05/Routing_System_Broadcast_Frame.h>
#include <KNX/03/08/07/Remote_Basic_Configuration_Request_Frame.h>
#include <KNX/03/08/07/Remote_Diagnostic_Request_Frame.h>
#include <KNX/03/08/07/Remote_Diagnostic_Response_Frame.h>
#include <KNX/03/08/07/Remote_Reset_Request_Frame.h>
#include <KNX/03/08/09/Secure_Channel_Authorize_Frame.h>
#include <KNX/03/08/09/Secure_Channel_Request_Frame.h>
#include <KNX/03/08/09/Secure_Channel_Response_Frame.h>
#include <KNX/03/08/09/Secure_Channel_Status_Frame.h>
#include <KNX/03/08/09/Secure_Group_Sync_Request_Frame.h>
#include <KNX/03/08/09/Secure_Group_Sync_Response_Frame.h>
#include <KNX/03/08/09/Secure_Wrapper_Frame.h>

namespace KNX {

Frame::Frame()
{
}

Frame::Frame(const Service_Type_Identifier service_type_identifier) :
    service_type_identifier(service_type_identifier)
{
}

void Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= HEADER_SIZE_10);

    header_length = *first++;

    protocol_version = *first++;

    service_type_identifier = static_cast<Service_Type_Identifier>((*first++ << 8) | *first++);

    total_length = (*first++ << 8) | *first++;
}

std::vector<uint8_t> Frame::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(header_length);
    data.push_back(protocol_version);
    data.push_back(static_cast<uint16_t>(service_type_identifier) >> 8);
    data.push_back(static_cast<uint16_t>(service_type_identifier) & 0xff);
    data.push_back(total_length_calculated() >> 8);
    data.push_back(total_length_calculated() & 0xff);

    return data;
}

uint16_t Frame::total_length_calculated() const
{
    return HEADER_SIZE_10;
}

std::shared_ptr<Frame> make_Frame(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    Frame frame;
    frame.fromData(first, last);
    switch (frame.service_type_identifier) {
    case Service_Type_Identifier::UNDEFINED:
        break;

    /* Core */
    case Service_Type_Identifier::SEARCH_REQUEST: {
        std::shared_ptr<Search_Request_Frame> frame2 = std::make_shared<Search_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::SEARCH_RESPONSE: {
        std::shared_ptr<Search_Response_Frame> frame2 = std::make_shared<Search_Response_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::DESCRIPTION_REQUEST: {
        std::shared_ptr<Description_Request_Frame> frame2 = std::make_shared<Description_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::DESCRIPTION_RESPONSE: {
        std::shared_ptr<Description_Response_Frame> frame2 = std::make_shared<Description_Response_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::CONNECT_REQUEST: {
        std::shared_ptr<Connect_Request_Frame> frame2 = std::make_shared<Connect_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::CONNECT_RESPONSE: {
        std::shared_ptr<Connect_Response_Frame> frame2 = std::make_shared<Connect_Response_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::CONNECTIONSTATE_REQUEST: {
        std::shared_ptr<Connectionstate_Request_Frame> frame2 = std::make_shared<Connectionstate_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::CONNECTIONSTATE_RESPONSE: {
        std::shared_ptr<Connectionstate_Response_Frame> frame2 = std::make_shared<Connectionstate_Response_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::DISCONNECT_REQUEST: {
        std::shared_ptr<Disconnect_Request_Frame> frame2 = std::make_shared<Disconnect_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::DISCONNECT_RESPONSE: {
        std::shared_ptr<Disconnect_Response_Frame> frame2 = std::make_shared<Disconnect_Response_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::SEARCH_REQUEST_EXT: {
        std::shared_ptr<Search_Request_Extended_Frame> frame2 = std::make_shared<Search_Request_Extended_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::SEARCH_RESPONSE_EXT: {
        std::shared_ptr<Search_Response_Extended_Frame> frame2 = std::make_shared<Search_Response_Extended_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;

    /* Device Management */
    case Service_Type_Identifier::DEVICE_CONFIGURATION_REQUEST: {
        std::shared_ptr<Device_Configuration_Request_Frame> frame2 = std::make_shared<Device_Configuration_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::DEVICE_CONFIGURATION_ACK: {
        std::shared_ptr<Device_Configuration_Ack_Frame> frame2 = std::make_shared<Device_Configuration_Ack_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;

    /* Tunnelling */
    case Service_Type_Identifier::TUNNELING_REQUEST: {
        std::shared_ptr<Tunnelling_Request_Frame> frame2 = std::make_shared<Tunnelling_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::TUNNELING_ACK: {
        std::shared_ptr<Tunnelling_Ack_Frame> frame2 = std::make_shared<Tunnelling_Ack_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::TUNNELING_FEATURE_GET: {
        std::shared_ptr<Tunnelling_Feature_Get_Frame> frame2 = std::make_shared<Tunnelling_Feature_Get_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::TUNNELING_FEATURE_RESPONSE: {
        std::shared_ptr<Tunnelling_Feature_Response_Frame> frame2 = std::make_shared<Tunnelling_Feature_Response_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::TUNNELING_FEATURE_SET: {
        std::shared_ptr<Tunnelling_Feature_Set_Frame> frame2 = std::make_shared<Tunnelling_Feature_Set_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::TUNNELING_FEATURE_INFO: {
        std::shared_ptr<Tunnelling_Feature_Info_Frame> frame2 = std::make_shared<Tunnelling_Feature_Info_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;

    /* Routing */
    case Service_Type_Identifier::ROUTING_INDICATION: {
        std::shared_ptr<Routing_Indication_Frame> frame2 = std::make_shared<Routing_Indication_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::ROUTING_LOST_MESSAGE: {
        std::shared_ptr<Routing_Lost_Message_Frame> frame2 = std::make_shared<Routing_Lost_Message_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::ROUTING_BUSY: {
        std::shared_ptr<Routing_Busy_Frame> frame2 = std::make_shared<Routing_Busy_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::ROUTING_SYSTEM_BROADCAST: {
        std::shared_ptr<Routing_System_Broadcast_Frame> frame2 = std::make_shared<Routing_System_Broadcast_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;

    /* Remote Logging */

    /* Remote Configuration and Diagnosis */
    case Service_Type_Identifier::REMOTE_DIAGNOSTIC_REQUEST: {
        std::shared_ptr<Remote_Diagnostic_Request_Frame> frame2 = std::make_shared<Remote_Diagnostic_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::REMOTE_DIAGNOSTIC_RESPONSE: {
        std::shared_ptr<Remote_Diagnostic_Response_Frame> frame2 = std::make_shared<Remote_Diagnostic_Response_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::REMOTE_BASIC_CONFIGURATION_REQUEST: {
        std::shared_ptr<Remote_Basic_Configuration_Request_Frame> frame2 = std::make_shared<Remote_Basic_Configuration_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::REMOTE_RESET_REQUEST: {
        std::shared_ptr<Remote_Reset_Request_Frame> frame2 = std::make_shared<Remote_Reset_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;

    /* Object Server */

    /* Security */
    case Service_Type_Identifier::SECURE_WRAPPER: {
        std::shared_ptr<Secure_Wrapper_Frame> frame2 = std::make_shared<Secure_Wrapper_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::SECURE_CHANNEL_REQUEST: {
        std::shared_ptr<Secure_Channel_Request_Frame> frame2 = std::make_shared<Secure_Channel_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::SECURE_CHANNEL_RESPONSE: {
        std::shared_ptr<Secure_Channel_Response_Frame> frame2 = std::make_shared<Secure_Channel_Response_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::SECURE_CHANNEL_AUTHORIZE: {
        std::shared_ptr<Secure_Channel_Authorize_Frame> frame2 = std::make_shared<Secure_Channel_Authorize_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::SECURE_CHANNEL_STATUS: {
        std::shared_ptr<Secure_Channel_Status_Frame> frame2 = std::make_shared<Secure_Channel_Status_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::SECURE_GROUP_SYNC_REQUEST: {
        std::shared_ptr<Secure_Group_Sync_Request_Frame> frame2 = std::make_shared<Secure_Group_Sync_Request_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    case Service_Type_Identifier::SECURE_GROUP_SYNC_RESPONSE: {
        std::shared_ptr<Secure_Group_Sync_Response_Frame> frame2 = std::make_shared<Secure_Group_Sync_Response_Frame>();
        frame2->fromData(first, last);
        return frame2;
    }
    break;
    }
    return std::make_shared<Frame>(frame);
}

}
