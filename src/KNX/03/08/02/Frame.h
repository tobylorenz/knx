// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/08/01/Common_Constants.h>
#include <KNX/03/08/01/Services.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Frame
 *
 * @ingroup KNX_03_08_02_02_03
 * @ingroup KNX_03_08_02_07_01
 */
class KNX_EXPORT Frame
{
public:
    Frame();
    explicit Frame(const Service_Type_Identifier service_type_identifier);

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    virtual uint16_t total_length_calculated() const;

    uint8_t header_length{HEADER_SIZE_10};
    uint8_t protocol_version{KNXNETIP_VERSION_10};
    Service_Type_Identifier service_type_identifier{Service_Type_Identifier::UNDEFINED};
    uint16_t total_length{HEADER_SIZE_10};
};

KNX_EXPORT std::shared_ptr<Frame> make_Frame(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
