// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Host_Protocol_Address_Information.h>

#include <cassert>

#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>

namespace KNX {

void Host_Protocol_Address_Information::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    structure_length = *first++;

    host_protocol_code = static_cast<Host_Protocol_Code>(*first++);

    assert(first == last);
}

std::vector<uint8_t> Host_Protocol_Address_Information::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(structure_length_calculated());
    data.push_back(static_cast<uint8_t>(host_protocol_code));

    return data;
}

uint8_t Host_Protocol_Address_Information::structure_length_calculated() const
{
    return 2;
}

bool operator==(const Host_Protocol_Address_Information & a, const Host_Protocol_Address_Information & b)
{
    return a.host_protocol_code == b.host_protocol_code;
}

bool operator!=(const Host_Protocol_Address_Information & a, const Host_Protocol_Address_Information & b)
{
    return !(a == b);
}

bool operator<(const Host_Protocol_Address_Information & a, const Host_Protocol_Address_Information & b)
{
    return a.host_protocol_code < b.host_protocol_code;
}

std::shared_ptr<Host_Protocol_Address_Information> make_Host_Protocol_Address_Information(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    Host_Protocol_Address_Information hpai;
    hpai.fromData(first, first + 2);
    switch (hpai.host_protocol_code) {
    case Host_Protocol_Code::UNDEFINED:
        break;
    case Host_Protocol_Code::IPV4_UDP:
    case Host_Protocol_Code::IPV4_TCP: {
        std::shared_ptr<IP_Host_Protocol_Address_Information> hpai2 = std::make_shared<IP_Host_Protocol_Address_Information>();
        hpai2->fromData(first, last);
        return hpai2;
    }
    break;
    }
    return nullptr;
}

}
