// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/08/01/Host_Protocol_Codes.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Host Protocol Address Information (HPAI)
 *
 * @ingroup KNX_03_08_02_07_05_01
 */
class KNX_EXPORT Host_Protocol_Address_Information
{
public:
    Host_Protocol_Address_Information() = default;
    virtual ~Host_Protocol_Address_Information() = default;

    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    virtual uint8_t structure_length_calculated() const;

    uint8_t structure_length{2};
    Host_Protocol_Code host_protocol_code{Host_Protocol_Code::UNDEFINED};

    // host protocol dependent data (variable length)
};

KNX_EXPORT bool operator==(const Host_Protocol_Address_Information & a, const Host_Protocol_Address_Information & b);
KNX_EXPORT bool operator!=(const Host_Protocol_Address_Information & a, const Host_Protocol_Address_Information & b);
KNX_EXPORT bool operator<(const Host_Protocol_Address_Information & a, const Host_Protocol_Address_Information & b);

KNX_EXPORT std::shared_ptr<Host_Protocol_Address_Information> make_Host_Protocol_Address_Information(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
