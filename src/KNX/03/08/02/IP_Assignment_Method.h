// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

// see IP_Assignment_Method_Property and Current_IP_Assignment_Method_Property
enum class IP_Assignment_Method : uint8_t {
    /** manually */
    manually = 0x01,

    /** DHCP */
    DHCP = 0x04
};

}
