// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

// @see IP_Capabilities_Property
enum class IP_Capabilities : uint8_t {
    IP_Capabilities_0 = 0,

    /** DHCP */
    DHCP = 0x02
};

}
