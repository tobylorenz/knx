// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/IP_Config_DIB.h>

#include <cassert>

namespace KNX {

IP_Config_DIB::IP_Config_DIB() :
    Description_Information_Block()
{
    description_type_code = Description_Type_Code::IP_CONFIG;
}

void IP_Config_DIB::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 16);

    Description_Information_Block::fromData(first, first + 2);
    first += 2;

    std::copy(first, first + 4, std::begin(ip_address));
    first += 4;

    std::copy(first, first + 4, std::begin(subnet_mask));
    first += 4;

    std::copy(first, first + 4, std::begin(default_gateway));
    first += 4;

    ip_capabilities = static_cast<IP_Capabilities>(*first++);

    ip_assignment_method = static_cast<IP_Assignment_Method>(*first++);

    assert(first == last);
}

std::vector<uint8_t> IP_Config_DIB::toData() const
{
    std::vector<uint8_t> data = Description_Information_Block::toData();
    data.insert(std::cend(data), std::cbegin(ip_address), std::cend(ip_address));
    data.insert(std::cend(data), std::cbegin(subnet_mask), std::cend(subnet_mask));
    data.insert(std::cend(data), std::cbegin(default_gateway), std::cend(default_gateway));
    data.push_back(static_cast<uint8_t>(ip_capabilities));
    data.push_back(static_cast<uint8_t>(ip_assignment_method));
    return data;
}

uint8_t IP_Config_DIB::structure_length_calculated() const
{
    return
        Description_Information_Block::structure_length_calculated() +
        14;
}

}
