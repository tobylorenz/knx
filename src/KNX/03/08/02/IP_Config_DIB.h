// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/05/01/11_IP_Parameter/Default_Gateway_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Assignment_Method_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/IP_Capabilities_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Subnet_Mask_Property.h>
#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/03/08/02/IP_Assignment_Method.h>
#include <KNX/03/08/02/IP_Capabilities.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * IP Configuration DIB
 *
 * @ingroup KNX_03_08_02_07_05_04_04
 * @todo Most values are kept in IP_Parameter_Object
 */
class KNX_EXPORT IP_Config_DIB : public Description_Information_Block
{
public:
    IP_Config_DIB();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    std::array<uint8_t, 4> ip_address{}; // @see IP_Address_Property
    std::array<uint8_t, 4> subnet_mask{}; // @see Subnet_Mask_Property
    std::array<uint8_t, 4> default_gateway{}; // @see Default_Gateway_Property

    IP_Capabilities ip_capabilities{}; // @see IP_Capabilities_Property

    IP_Assignment_Method ip_assignment_method{}; // @see IP_Assignment_Method_Property
};

}
