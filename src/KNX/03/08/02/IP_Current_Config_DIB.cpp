// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/IP_Current_Config_DIB.h>

#include <cassert>

namespace KNX {

IP_Current_Config_DIB::IP_Current_Config_DIB() :
    Description_Information_Block()
{
    description_type_code = Description_Type_Code::IP_CUR_CONFIG;
}

void IP_Current_Config_DIB::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 20);

    Description_Information_Block::fromData(first, first + 2);
    first += 2;

    std::copy(first, first + 4, std::begin(current_ip_address));
    first += 4;

    std::copy(first, first + 4, std::begin(current_subnet_mask));
    first += 4;

    std::copy(first, first + 4, std::begin(current_default_gateway));
    first += 4;

    std::copy(first, first + 4, std::begin(dhcp_server));
    first += 4;

    current_ip_assignment_method = static_cast<IP_Assignment_Method>(*first++);

    ++first; // reserved

    assert(first == last);
}

std::vector<uint8_t> IP_Current_Config_DIB::toData() const
{
    std::vector<uint8_t> data = Description_Information_Block::toData();
    data.insert(std::cend(data), std::cbegin(current_ip_address), std::cend(current_ip_address));
    data.insert(std::cend(data), std::cbegin(current_subnet_mask), std::cend(current_subnet_mask));
    data.insert(std::cend(data), std::cbegin(current_default_gateway), std::cend(current_default_gateway));
    data.insert(std::cend(data), std::cbegin(dhcp_server), std::cend(dhcp_server));
    data.push_back(static_cast<uint8_t>(current_ip_assignment_method));
    data.push_back(0);
    return data;
}

uint8_t IP_Current_Config_DIB::structure_length_calculated() const
{
    return
        Description_Information_Block::structure_length_calculated() +
        18;
}

}
