// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/05/01/11_IP_Parameter/Current_Default_Gateway_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Current_IP_Address_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Current_IP_Assignment_Method_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Current_Subnet_Mask_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/DHCP_BootP_Server_Property.h>
#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/03/08/02/IP_Assignment_Method.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * IP Current Configuration DIB
 *
 * @ingroup KNX_03_08_02_07_05_04_05
 * @todo Most values are kept in IP_Parameter_Object
 */
class KNX_EXPORT IP_Current_Config_DIB : public Description_Information_Block
{
public:
    IP_Current_Config_DIB();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    std::array<uint8_t, 4> current_ip_address{}; // @see Current_IP_Address_Property
    std::array<uint8_t, 4> current_subnet_mask{}; // @see Current_Subnet_Mask_Property
    std::array<uint8_t, 4> current_default_gateway{}; // @see Current_Default_Gateway_Property
    std::array<uint8_t, 4> dhcp_server{}; // @see DHCP_BootP_Server_Property
    IP_Assignment_Method current_ip_assignment_method{}; // @Current_IP_Assignment_Method_Property
};

}
