// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>

#include <cassert>

namespace KNX {

IP_Host_Protocol_Address_Information::IP_Host_Protocol_Address_Information() :
    Host_Protocol_Address_Information()
{
    host_protocol_code = Host_Protocol_Code::IPV4_UDP;
}

IP_Host_Protocol_Address_Information::IP_Host_Protocol_Address_Information(const std::array<uint8_t, 4> ip_address, const uint16_t ip_port_number) :
    IP_Host_Protocol_Address_Information()
{
    this->ip_address = ip_address;
    this->ip_port_number = ip_port_number;
}

void IP_Host_Protocol_Address_Information::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 8);

    Host_Protocol_Address_Information::fromData(first, first + 2);
    first += 2;

    std::copy(first, first + 4, std::begin(ip_address));
    first += 4;

    ip_port_number = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> IP_Host_Protocol_Address_Information::toData() const
{
    assert((host_protocol_code == Host_Protocol_Code::IPV4_TCP) || (host_protocol_code == Host_Protocol_Code::IPV4_UDP));

    std::vector<uint8_t> data = Host_Protocol_Address_Information::toData();

    data.insert(std::cend(data), std::cbegin(ip_address), std::cend(ip_address));
    data.push_back(ip_port_number >> 8);
    data.push_back(ip_port_number & 0xff);

    return data;
}

uint8_t IP_Host_Protocol_Address_Information::structure_length_calculated() const
{
    assert((host_protocol_code == Host_Protocol_Code::IPV4_TCP) || (host_protocol_code == Host_Protocol_Code::IPV4_UDP));

    return
        Host_Protocol_Address_Information::structure_length_calculated() +
        6;
}

bool operator==(const IP_Host_Protocol_Address_Information & a, const IP_Host_Protocol_Address_Information & b)
{
    return
        (static_cast<Host_Protocol_Address_Information>(a) == static_cast<Host_Protocol_Address_Information>(b)) &&
        (a.ip_address == b.ip_address) &&
        (a.ip_port_number == b.ip_port_number);
}

bool operator!=(const IP_Host_Protocol_Address_Information & a, const IP_Host_Protocol_Address_Information & b)
{
    return !(a == b);
}

bool operator<(const IP_Host_Protocol_Address_Information & a, const IP_Host_Protocol_Address_Information & b)
{
    return
        (static_cast<Host_Protocol_Address_Information>(a) < static_cast<Host_Protocol_Address_Information>(b)) ||
        (a.ip_address < b.ip_address) ||
        (a.ip_port_number < b.ip_port_number);
}

}
