// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/08/02/Host_Protocol_Address_Information.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Host Protocol Address Information (HPAI)
 *
 * @ingroup KNX_03_08_02_08_06_02
 */
class KNX_EXPORT IP_Host_Protocol_Address_Information : public Host_Protocol_Address_Information
{
public:
    IP_Host_Protocol_Address_Information();
    IP_Host_Protocol_Address_Information(const std::array<uint8_t, 4> ip_address, const uint16_t ip_port_number);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    std::array<uint8_t, 4> ip_address{};
    uint16_t ip_port_number{};
};

KNX_EXPORT bool operator==(const IP_Host_Protocol_Address_Information & a, const IP_Host_Protocol_Address_Information & b);
KNX_EXPORT bool operator!=(const IP_Host_Protocol_Address_Information & a, const IP_Host_Protocol_Address_Information & b);
KNX_EXPORT bool operator<(const IP_Host_Protocol_Address_Information & a, const IP_Host_Protocol_Address_Information & b);

}
