// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Manufacturer_Data_DIB.h>

#include <cassert>

namespace KNX {

Manufacturer_Data_DIB::Manufacturer_Data_DIB() :
    Description_Information_Block()
{
    description_type_code = Description_Type_Code::MFR_DATA;
}

void Manufacturer_Data_DIB::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 4);

    Description_Information_Block::fromData(first, first + 2);
    first += 2;

    manufacturer_id = (*first++ << 8) | *first++;

    manufacturer_specific_data.assign(first, last);
}

std::vector<uint8_t> Manufacturer_Data_DIB::toData() const
{
    std::vector<uint8_t> data = Description_Information_Block::toData();

    data.push_back(manufacturer_id >> 8);
    data.push_back(manufacturer_id & 0xff);
    data.insert(std::cend(data), std::cbegin(manufacturer_specific_data), std::cend(manufacturer_specific_data));

    return data;
}

uint8_t Manufacturer_Data_DIB::structure_length_calculated() const
{
    return
        Description_Information_Block::structure_length_calculated() +
        2 +
        static_cast<uint8_t>(manufacturer_specific_data.size());
}

}
