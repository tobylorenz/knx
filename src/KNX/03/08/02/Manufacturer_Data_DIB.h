// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Manufacturer Data DIB
 *
 * @ingroup KNX_03_08_02_07_05_04_07
 */
class KNX_EXPORT Manufacturer_Data_DIB : public Description_Information_Block
{
public:
    Manufacturer_Data_DIB();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    uint16_t manufacturer_id{};
    std::vector<uint8_t> manufacturer_specific_data{};
};

}
