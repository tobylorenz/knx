// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Request_DIBs_SRP.h>

#include <cassert>

namespace KNX {

Request_DIBs_SRP::Request_DIBs_SRP() :
    Search_Request_Parameter()
{
    type_code = Search_Request_Parameter_Type_Code::Request_DIBs;
}

void Request_DIBs_SRP::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 8);

    Search_Request_Parameter::fromData(first, first + 2);
    first += 2;

    description_types.assign(first, last);

    // assert(first == last);
}

std::vector<uint8_t> Request_DIBs_SRP::toData() const
{
    std::vector<uint8_t> data = Search_Request_Parameter::toData();
    data.insert(std::cend(data), std::cbegin(description_types), std::cend(description_types));
    return data;
}

uint8_t Request_DIBs_SRP::structure_length_calculated() const
{
    return
        Search_Request_Parameter::structure_length_calculated() +
        static_cast<uint8_t>(description_types.size());
}

}
