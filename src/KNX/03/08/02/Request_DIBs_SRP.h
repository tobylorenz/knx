// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/03/08/02/Search_Request_Parameter.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * SRP Type "Request DIBs"
 *
 * @ingroup KNX_AN184_02_02_02_01_03_06
 */
class KNX_EXPORT Request_DIBs_SRP : public Search_Request_Parameter
{
public:
    Request_DIBs_SRP();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    std::vector<uint8_t> description_types{};
};

}
