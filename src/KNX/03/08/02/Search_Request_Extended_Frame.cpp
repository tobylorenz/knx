// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Search_Request_Extended_Frame.h>

#include <cassert>
#include <numeric>

#include <KNX/03/08/01/Common_Constants.h>

namespace KNX {

Search_Request_Extended_Frame::Search_Request_Extended_Frame() :
    Frame(Service_Type_Identifier::SEARCH_REQUEST_EXT)
{
}

void Search_Request_Extended_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 2));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 2);

    std::uint8_t structure_length = *first;
    discovery_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    while (first != last) {
        structure_length = *first;
        search_request_parameters.push_back(make_Search_Request_Parameter(first, first + structure_length));
        first += structure_length;
    }

    assert(first == last);
}

std::vector<uint8_t> Search_Request_Extended_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    std::vector<uint8_t> discovery_endpoint_data = discovery_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(discovery_endpoint_data), std::cend(discovery_endpoint_data));

    for (const auto & search_request_parameter : search_request_parameters) {
        std::vector<uint8_t> search_request_parameter_data = search_request_parameter->toData();
        data.insert(std::cend(data), std::cbegin(search_request_parameter_data), std::cend(search_request_parameter_data));
    }

    return data;
}

uint16_t Search_Request_Extended_Frame::total_length_calculated() const
{
    return std::accumulate(
               std::cbegin(search_request_parameters),
               std::cend(search_request_parameters),
               Frame::total_length_calculated() +
               discovery_endpoint->structure_length_calculated(),
    [](uint16_t total_length, std::shared_ptr<Search_Request_Parameter> search_request_parameter) {
        return std::move(total_length) + search_request_parameter->structure_length_calculated();
    });
}

}
