// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Search_Request_Frame.h>

#include <cassert>

#include <KNX/03/08/01/Common_Constants.h>

namespace KNX {

Search_Request_Frame::Search_Request_Frame() :
    Frame(Service_Type_Identifier::SEARCH_REQUEST)
{
}

void Search_Request_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 2));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 2);

    std::uint8_t structure_length = *first;
    discovery_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    assert(first == last);
}

uint16_t Search_Request_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        discovery_endpoint->structure_length_calculated();
}

std::vector<uint8_t> Search_Request_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    std::vector<uint8_t> discovery_endpoint_data = discovery_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(discovery_endpoint_data), std::cend(discovery_endpoint_data));

    return data;
}

}
