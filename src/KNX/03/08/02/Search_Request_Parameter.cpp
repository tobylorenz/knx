// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Search_Request_Parameter.h>

#include <cassert>

#include <KNX/03/08/02/Request_DIBs_SRP.h>
#include <KNX/03/08/02/Select_By_MAC_Address_SRP.h>
#include <KNX/03/08/02/Select_By_Programming_Mode_SRP.h>
#include <KNX/03/08/02/Select_By_Service_SRP.h>

namespace KNX {

void Search_Request_Parameter::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    structure_length = *first++;

    mandatory = (*first >> 8) & 0x01;
    type_code = static_cast<Search_Request_Parameter_Type_Code>(*first & 0x7f);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Search_Request_Parameter::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(structure_length_calculated());
    data.push_back(
        (static_cast<uint8_t>(mandatory) << 7) |
        (static_cast<uint8_t>(type_code)));

    return data;
}

uint8_t Search_Request_Parameter::structure_length_calculated() const
{
    return 2;
}

std::shared_ptr<Search_Request_Parameter> make_Search_Request_Parameter(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    Search_Request_Parameter srp;
    srp.fromData(first, first + 2);

    switch (srp.type_code) {
    case Search_Request_Parameter_Type_Code::Invalid:
        break;
    case Search_Request_Parameter_Type_Code::Select_By_Programming_Mode: {
        std::shared_ptr<Search_Request_Parameter> srp2 = std::make_shared<Select_By_Programming_Mode_SRP>();
        srp2->fromData(first, last);
        return srp2;
    }
    break;
    case Search_Request_Parameter_Type_Code::Select_By_MAC_Address: {
        std::shared_ptr<Search_Request_Parameter> srp2 = std::make_shared<Select_By_MAC_Address_SRP>();
        srp2->fromData(first, last);
        return srp2;
    }
    break;
    case Search_Request_Parameter_Type_Code::Select_By_Service: {
        std::shared_ptr<Search_Request_Parameter> srp2 = std::make_shared<Select_By_Service_SRP>();
        srp2->fromData(first, last);
        return srp2;
    }
    break;
    case Search_Request_Parameter_Type_Code::Request_DIBs: {
        std::shared_ptr<Search_Request_Parameter> srp2 = std::make_shared<Request_DIBs_SRP>();
        srp2->fromData(first, last);
        return srp2;
    }
    break;
    }
    return nullptr;
}

}
