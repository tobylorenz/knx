// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <memory>
#include <vector>

#include <KNX/03/08/02/Search_Request_Parameter_Type_Code.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Search Request Parameter (SRP)
 *
 * @ingroup KNX_AN_184_02_02_02_01_03
 */
class Search_Request_Parameter
{
public:
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;
    virtual uint8_t structure_length_calculated() const;

    uint8_t structure_length{2};
    bool mandatory{false};
    Search_Request_Parameter_Type_Code type_code{};
};

KNX_EXPORT std::shared_ptr<Search_Request_Parameter> make_Search_Request_Parameter(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
