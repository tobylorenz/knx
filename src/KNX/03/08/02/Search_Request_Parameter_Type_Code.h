// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Serach Request Parameter Type Code
 *
 * @ingroup KNX_AN184_02_02_02_01_03_02
 */
enum class Search_Request_Parameter_Type_Code : uint8_t { // actually uint8_t
    /** Invalid */
    Invalid = 0x00,

    /** Select By Programming Mode */
    Select_By_Programming_Mode = 0x01,

    /** Select By MAC Address */
    Select_By_MAC_Address = 0x02,

    /** Select By Service SRP */
    Select_By_Service = 0x03,

    /** Request DIBs */
    Request_DIBs = 0x04,

    // 0x05 .. 0x7F are reserved
};

}
