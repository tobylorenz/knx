// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Search_Response_Extended_Frame.h>

#include <cassert>
#include <numeric>

namespace KNX {

Search_Response_Extended_Frame::Search_Response_Extended_Frame() :
    Frame(Service_Type_Identifier::SEARCH_RESPONSE_EXT)
{
}

void Search_Response_Extended_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 6));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 2);

    std::uint8_t structure_length = *first;
    control_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    while (first != last) {
        structure_length = *first;
        description_information_blocks.push_back(make_Description_Information_Block(first, first + structure_length));
        first += structure_length;
    }

    assert(first == last);
}

std::vector<uint8_t> Search_Response_Extended_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    std::vector<uint8_t> control_endpoint_data = control_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(control_endpoint_data), std::cend(control_endpoint_data));

    for (const auto & description_information_block : description_information_blocks) {
        std::vector<uint8_t> description_information_block_data = description_information_block->toData();
        data.insert(std::cend(data), std::cbegin(description_information_block_data), std::cend(description_information_block_data));
    }

    return data;
}

uint16_t Search_Response_Extended_Frame::total_length_calculated() const
{
    return std::accumulate(
               std::cbegin(description_information_blocks),
               std::cend(description_information_blocks),
               Frame::total_length_calculated() +
               control_endpoint->structure_length_calculated(),
    [](uint16_t total_length, std::shared_ptr<Description_Information_Block> description_information_block) {
        return std::move(total_length) + description_information_block->structure_length_calculated();
    });
}

}
