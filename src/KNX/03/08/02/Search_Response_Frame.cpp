// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Search_Response_Frame.h>

#include <cassert>

namespace KNX {

Search_Response_Frame::Search_Response_Frame() :
    Frame(Service_Type_Identifier::SEARCH_RESPONSE)
{
}

void Search_Response_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 6));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 6);

    std::uint8_t structure_length = *first;
    control_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    assert(std::distance(first, last) >= 4);

    structure_length = *first;
    device_hardware = make_Device_Information_DIB(first, first + structure_length);
    first += structure_length;

    assert(std::distance(first, last) >= 2);

    structure_length = *first;
    supported_service_families = make_Supported_Service_Families_DIB(first, first + structure_length);
    first += structure_length;

    assert(first == last);
}

std::vector<uint8_t> Search_Response_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    std::vector<uint8_t> control_endpoint_data = control_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(control_endpoint_data), std::cend(control_endpoint_data));

    std::vector<uint8_t> device_hardware_data = device_hardware->toData();
    data.insert(std::cend(data), std::cbegin(device_hardware_data), std::cend(device_hardware_data));

    std::vector<uint8_t> supported_service_families_data = supported_service_families->toData();
    data.insert(std::cend(data), std::cbegin(supported_service_families_data), std::cend(supported_service_families_data));

    return data;
}

uint16_t Search_Response_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        control_endpoint->structure_length_calculated() +
        device_hardware->structure_length_calculated() +
        supported_service_families->structure_length_calculated();
}

}
