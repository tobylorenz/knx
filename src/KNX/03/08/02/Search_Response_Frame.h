// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Device_Information_DIB.h>
#include <KNX/03/08/02/Frame.h>
#include <KNX/03/08/02/Host_Protocol_Address_Information.h>
#include <KNX/03/08/02/Supported_Service_Families_DIB.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Search Response Frame
 *
 * @ingroup KNX_03_08_02_07_06_02
 */
class KNX_EXPORT Search_Response_Frame : public Frame
{
public:
    Search_Response_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    std::shared_ptr<Host_Protocol_Address_Information> control_endpoint{};
    std::shared_ptr<Device_Information_DIB> device_hardware{};
    std::shared_ptr<Supported_Service_Families_DIB> supported_service_families{};
};

}
