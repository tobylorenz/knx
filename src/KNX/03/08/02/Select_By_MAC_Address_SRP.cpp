// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Select_By_MAC_Address_SRP.h>

#include <cassert>

namespace KNX {

Select_By_MAC_Address_SRP::Select_By_MAC_Address_SRP() :
    Search_Request_Parameter()
{
    type_code = Search_Request_Parameter_Type_Code::Select_By_MAC_Address;
}

void Select_By_MAC_Address_SRP::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 8);

    Search_Request_Parameter::fromData(first, first + 2);
    first += 2;

    std::copy(first, first + 6, std::begin(mac_address));
    first += 6;

    assert(first == last);
}

std::vector<uint8_t> Select_By_MAC_Address_SRP::toData() const
{
    std::vector<uint8_t> data = Search_Request_Parameter::toData();
    data.insert(std::cend(data), std::cbegin(mac_address), std::cend(mac_address));
    return data;
}

uint8_t Select_By_MAC_Address_SRP::structure_length_calculated() const
{
    return
        Search_Request_Parameter::structure_length_calculated() +
        6;
}

}
