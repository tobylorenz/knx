// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/08/02/Search_Request_Parameter.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * SRP Type "Select by MAC Address"
 *
 * @ingroup KNX_AN184_02_02_02_01_03_04
 */
class KNX_EXPORT Select_By_MAC_Address_SRP : public Search_Request_Parameter
{
public:
    Select_By_MAC_Address_SRP();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    std::array<uint8_t, 6> mac_address{};
};

}
