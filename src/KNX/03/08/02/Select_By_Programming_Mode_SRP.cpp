// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Select_By_Programming_Mode_SRP.h>

#include <cassert>

namespace KNX {

Select_By_Programming_Mode_SRP::Select_By_Programming_Mode_SRP() :
    Search_Request_Parameter()
{
    type_code = Search_Request_Parameter_Type_Code::Select_By_Programming_Mode;
}

}
