// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Search_Request_Parameter.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * SRP Type "Select by Programming Mode"
 *
 * @ingroup KNX_AN184_02_02_02_01_03_03
 */
class KNX_EXPORT Select_By_Programming_Mode_SRP : public Search_Request_Parameter
{
public:
    Select_By_Programming_Mode_SRP();
};

}
