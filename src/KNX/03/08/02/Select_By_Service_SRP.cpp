// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Select_By_Service_SRP.h>

#include <cassert>

namespace KNX {

Select_By_Service_SRP::Select_By_Service_SRP() :
    Search_Request_Parameter()
{
    type_code = Search_Request_Parameter_Type_Code::Select_By_Service;
}

void Select_By_Service_SRP::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 8);

    Search_Request_Parameter::fromData(first, first + 2);
    first += 2;

    service_family_id = *first++;

    minimum_version = *first++;

    assert(first == last);
}

std::vector<uint8_t> Select_By_Service_SRP::toData() const
{
    std::vector<uint8_t> data = Search_Request_Parameter::toData();
    data.push_back(service_family_id);
    data.push_back(minimum_version);
    return data;
}

uint8_t Select_By_Service_SRP::structure_length_calculated() const
{
    return
        Search_Request_Parameter::structure_length_calculated() +
        2;
}

}
