// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/02/Supported_Service_Families_DIB.h>

#include <cassert>

namespace KNX {

Supported_Service_Families_DIB::Supported_Service_Families_DIB() :
    Description_Information_Block()
{
    description_type_code = Description_Type_Code::SUPP_SVC_FAMILIES;
}

void Supported_Service_Families_DIB::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= 2);

    Description_Information_Block::fromData(first, first + 2);
    first += 2;

    while (first != last) {
        service_families[*first] = *(first + 1);
        first += 2;
    }

    assert(first == last);
}

std::vector<uint8_t> Supported_Service_Families_DIB::toData() const
{
    std::vector<uint8_t> data = Description_Information_Block::toData();

    for (const auto & sf : service_families) {
        data.push_back(sf.first);
        data.push_back(sf.second);
    }

    return data;
}

uint8_t Supported_Service_Families_DIB::structure_length_calculated() const
{
    return
        Description_Information_Block::structure_length_calculated() +
        static_cast<uint8_t>(2 * service_families.size());
}

std::shared_ptr<Supported_Service_Families_DIB> make_Supported_Service_Families_DIB(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    std::shared_ptr<Supported_Service_Families_DIB> dib = std::make_shared<Supported_Service_Families_DIB>();
    dib->fromData(first, last);
    return dib;
}

}
