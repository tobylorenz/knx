// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <map>
#include <memory>

#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Supported Service Families DIB
 *
 * @ingroup KNX_03_08_02_07_05_04_03
 */
class KNX_EXPORT Supported_Service_Families_DIB : public Description_Information_Block
{
public:
    Supported_Service_Families_DIB();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    // Service Family (Connection_Type_Code) -> Service Version
    std::map<uint8_t, uint8_t> service_families{};
};

KNX_EXPORT std::shared_ptr<Supported_Service_Families_DIB> make_Supported_Service_Families_DIB(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

}
