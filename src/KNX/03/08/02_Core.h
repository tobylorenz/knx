// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 2.2 Frame format */
#include <KNX/03/08/02/Frame.h>

/* 5.3 Connection Header */
#include <KNX/03/08/02/Connection_Frame.h>

/* 7.5.1 Host Protocol Address Information (HPAI) */
#include <KNX/03/08/02/Host_Protocol_Address_Information.h>

/* 7.5.2 Connection Request Information (CRI) */
#include <KNX/03/08/02/Connection_Request_Information.h>

/* 7.5.3 Connection Request Data Block (CRD) */
#include <KNX/03/08/02/Connection_Response_Data_Block.h>

/* 7.5.4 Description Information Block (DIB) */
#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/03/08/02/Device_Information_DIB.h>
#include <KNX/03/08/02/Supported_Service_Families_DIB.h>
#include <KNX/03/08/02/IP_Config_DIB.h>
#include <KNX/03/08/02/IP_Current_Config_DIB.h>
#include <KNX/03/08/02/Addresses_DIB.h>
#include <KNX/03/08/02/Manufacturer_Data_DIB.h>

/* 7.6 Discovery */
#include <KNX/03/08/02/Search_Request_Frame.h>
#include <KNX/03/08/02/Search_Response_Frame.h>

/* 7.7 Self description */
#include <KNX/03/08/02/Description_Request_Frame.h>
#include <KNX/03/08/02/Description_Response_Frame.h>

/* 7.8 Connection management */
#include <KNX/03/08/02/Connect_Request_Frame.h>
#include <KNX/03/08/02/Connect_Response_Frame.h>
#include <KNX/03/08/02/Connectionstate_Request_Frame.h>
#include <KNX/03/08/02/Connectionstate_Response_Frame.h>
#include <KNX/03/08/02/Disconnect_Request_Frame.h>
#include <KNX/03/08/02/Disconnect_Response_Frame.h>

/* 8.6.2 Host Protocol Address Information */
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>

/* AN185 KNXnet/IP Core v2 */
#include <KNX/03/08/02/Search_Request_Parameter.h>
#include <KNX/03/08/02/Select_By_Programming_Mode_SRP.h>
#include <KNX/03/08/02/Select_By_MAC_Address_SRP.h>
#include <KNX/03/08/02/Select_By_Service_SRP.h>
#include <KNX/03/08/02/Request_DIBs_SRP.h>
