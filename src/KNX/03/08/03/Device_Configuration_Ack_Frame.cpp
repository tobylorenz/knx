// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/03/Device_Configuration_Ack_Frame.h>

namespace KNX {

Device_Configuration_Ack_Frame::Device_Configuration_Ack_Frame() :
    Connection_Frame(Service_Type_Identifier::DEVICE_CONFIGURATION_ACK)
{
}

}
