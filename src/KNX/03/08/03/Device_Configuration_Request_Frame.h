// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Connection_Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Device Configuration Request
 *
 * @ingroup KNX_03_08_03_04_02_06
 */
class KNX_EXPORT Device_Configuration_Request_Frame : public Connection_Frame
{
public:
    Device_Configuration_Request_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    std::vector<uint8_t> cemi_frame_data{};
};

}
