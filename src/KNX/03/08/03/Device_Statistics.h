// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/05/01/11_IP_Parameter/Message_Transmit_To_IP_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Message_Transmit_To_KNX_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Queue_Overflow_To_IP_Property.h>
#include <KNX/03/05/01/11_IP_Parameter/Queue_Overflow_To_KNX_Property.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Device statistics and status information
 *
 * @ingroup KNX_03_08_03_03_06
 * @todo All values are kept in IP_Parameter_Object
 */
class KNX_EXPORT Device_Statistics
{
public:
    /** Queue overflow to IP */
    uint16_t queue_overflow_to_ip{}; // @see Queue_Overflow_To_IP_Property

    /** Queue overflow to KNX */
    uint16_t queue_overflow_to_KNX{}; // @see Queue_Overflow_To_KNX_Property

    /** Telegrams transmitted to IP */
    uint32_t telegrams_transmitted_to_IP{}; // @see Message_Transmit_To_IP_Property

    /** Telegrams transmitted to KNX */
    uint32_t telegrams_transmitted_to_KNX{}; // @see Message_Transmit_To_KNX_Property
};

}
