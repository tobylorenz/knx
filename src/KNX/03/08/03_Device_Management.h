// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 2.5 KNXnet/IP Parameter Object */
#include <KNX/03/05/01/11_IP_Parameter/IP_Parameter_Object.h>

/* 3.6 Device statistics and status information */
#include <KNX/03/08/03/Device_Statistics.h>

/* 4.2.6 DEVICE_CONFIGURATION_REQUEST */
#include <KNX/03/08/03/Device_Configuration_Request_Frame.h>

/* 4.2.7 DEVICE_CONFIGURATION_ACK */
#include <KNX/03/08/03/Device_Configuration_Ack_Frame.h>
