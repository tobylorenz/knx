// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/04/Extended_Device_Information_DIB.h>

#include <cassert>

namespace KNX {

Extended_Device_Information_DIB::Extended_Device_Information_DIB() :
    Description_Information_Block()
{
    description_type_code = Description_Type_Code::EXTENDED_DEVICE_INFO;
}

void Extended_Device_Information_DIB::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 16);

    Description_Information_Block::fromData(first, first + 2);
    first += 2;

    apdu_length = (*first++ << 8) | *first++;
    first += 2;

    device_descriptor_type_0.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> Extended_Device_Information_DIB::toData() const
{
    std::vector<uint8_t> data = Description_Information_Block::toData();

    data.push_back(apdu_length >> 8);
    data.push_back(apdu_length & 0xff);

    std::vector<uint8_t> device_descriptor_type_0_data = device_descriptor_type_0.toData();
    data.insert(std::cend(data), std::cbegin(device_descriptor_type_0_data), std::cend(device_descriptor_type_0_data));

    return data;
}

uint8_t Extended_Device_Information_DIB::structure_length_calculated() const
{
    return
        Description_Information_Block::structure_length_calculated() +
        4;
}

}
