// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/03/05/01/Device_Descriptor_Type_0.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Extended Device Information DIB
 *
 * @ingroup KNX_AN185_02_04_02
 */
class KNX_EXPORT Extended_Device_Information_DIB : public Description_Information_Block
{
public:
    Extended_Device_Information_DIB();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    /** Maximal Local APDU length */
    uint16_t apdu_length{};

    /** Device Descriptor Type 0 */
    Device_Descriptor_Type_0 device_descriptor_type_0{}; // @see Device_Descriptor_Property
};

}
