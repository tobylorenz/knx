// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/04/Tunnelling_Ack_Frame.h>

#include <cassert>

namespace KNX {

Tunnelling_Ack_Frame::Tunnelling_Ack_Frame() :
    Connection_Frame(Service_Type_Identifier::TUNNELING_ACK)
{
}

}
