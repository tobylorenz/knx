// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/01/Error_Codes.h>
#include <KNX/03/08/02/Connection_Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Tunnelling Ack Frame
 *
 * @ingroup KNX_03_08_04_04_04_07
 */
class KNX_EXPORT Tunnelling_Ack_Frame : public Connection_Frame
{
public:
    Tunnelling_Ack_Frame();

    uint8_t & status{service_type_specific}; // type: Error_Code
};

}
