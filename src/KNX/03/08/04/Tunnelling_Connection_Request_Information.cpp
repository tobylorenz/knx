// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/04/Tunnelling_Connection_Request_Information.h>

#include <cassert>

namespace KNX {

Tunnelling_Connection_Request_Information::Tunnelling_Connection_Request_Information() :
    Connection_Request_Information(Connection_Type_Code::TUNNEL_CONNECTION)
{
}

Tunnelling_Connection_Request_Information::Tunnelling_Connection_Request_Information(const Tunnelling_Layer_Type_Code layer) :
    Connection_Request_Information(Connection_Type_Code::TUNNEL_CONNECTION),
    layer(layer)
{
}

void Tunnelling_Connection_Request_Information::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    Connection_Request_Information::fromData(first, first + 2);
    first += 2;

    layer = static_cast<Tunnelling_Layer_Type_Code>(*first++);
    ++first; // reserved

    if (structure_length == 6) {
        individual_address.fromData(first, first + 2);
        first += 2;
    }

    assert(first == last);
}

std::vector<uint8_t> Tunnelling_Connection_Request_Information::toData() const
{
    std::vector<uint8_t> data = Connection_Request_Information::toData();

    data.push_back(static_cast<uint8_t>(layer));
    data.push_back(0); // reserved

    if (individual_address) {
        data.push_back(individual_address >> 8);
        data.push_back(individual_address & 0xff);
    }

    return data;
}

uint8_t Tunnelling_Connection_Request_Information::structure_length_calculated() const
{
    uint8_t structure_length =
        Connection_Request_Information::structure_length_calculated() +
        2;
    if (individual_address) {
        structure_length += 2;
    }

    return structure_length;
};

}
