// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Connection_Request_Information.h>
#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/08/04/Tunnelling_Layer_Type_Code.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Tunnelling Connection Request Information (CRI)
 *
 * @ingroup KNX_03_08_04_04_04_03
 * @ingroup KNX_AN185_02_02_01
 */
class KNX_EXPORT Tunnelling_Connection_Request_Information : public Connection_Request_Information
{
public:
    Tunnelling_Connection_Request_Information();
    explicit Tunnelling_Connection_Request_Information(const Tunnelling_Layer_Type_Code layer);

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    Tunnelling_Layer_Type_Code layer{Tunnelling_Layer_Type_Code::UNDEFINED};

    /* Extended CRI */

    Individual_Address individual_address{};
};

}
