// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/04/Tunnelling_Connection_Response_Data_Block.h>

#include <cassert>

namespace KNX {

Tunnelling_Connection_Response_Data_Block::Tunnelling_Connection_Response_Data_Block() :
    Connection_Response_Data_Block()
{
    connection_type_code = Connection_Type_Code::TUNNEL_CONNECTION;
}

void Tunnelling_Connection_Response_Data_Block::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    Connection_Response_Data_Block::fromData(first, first + 2);
    first += 2;

    individual_address.fromData(first, first + 2);
    first += 2;

    assert(first == last);
}

std::vector<uint8_t> Tunnelling_Connection_Response_Data_Block::toData() const
{
    std::vector<uint8_t> data = Connection_Response_Data_Block::toData();

    data.push_back(individual_address >> 8);
    data.push_back(individual_address & 0xff);

    return data;
}

uint8_t Tunnelling_Connection_Response_Data_Block::structure_length_calculated() const
{
    return
        Connection_Response_Data_Block::structure_length_calculated() +
        2;
};

}
