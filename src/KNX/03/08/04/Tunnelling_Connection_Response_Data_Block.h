// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/08/02/Connection_Response_Data_Block.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Tunnelling Connection Response Data Block (CRD)
 *
 * @ingroup KNX_03_08_04_04_04_04
 */
class KNX_EXPORT Tunnelling_Connection_Response_Data_Block : public Connection_Response_Data_Block
{
public:
    Tunnelling_Connection_Response_Data_Block();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    Individual_Address individual_address{};
};

}
