// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Tunnelling Feature Identifier
 *
 * @ingroup KNX_AN185_02_02_02_08
 */
enum class Tunnelling_Feature_Id : uint8_t {
    UNDEFINED = 0x00,

    /** Supported EMI Type */
    SUPPORTED_EMI_TYPE = 0x01,

    /** Host Device Descriptor Type 0 */
    HOST_DEVICE_DEVICE_DESCRIPTOR_TYPE_0 = 0x02,

    /** Bus connection status */
    BUS_CONNECTION_STATUS = 0x03,

    /** KNX Manufacturer Code */
    KNX_MANUFACTURER_CODE = 0x04,

    /** Active EMI type */
    ACTIVE_EMI_TYPE = 0x05,

    /** Interface Individual Address */
    INDIVIDUAL_ADDRESS = 0x06,

    /** Max. APDU Length */
    MAX_APDU_LENGTH = 0x07,

    /** Interface Feature Info service Enable */
    INFO_SERVICE_ENABLE = 0x08
};

}
