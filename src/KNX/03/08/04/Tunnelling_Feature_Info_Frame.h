// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/01/Error_Codes.h>
#include <KNX/03/08/02/Connection_Frame.h>
#include <KNX/03/08/04/Tunnelling_Feature_Id.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Tunnelling Feature Info Frame
 *
 * @ingroup KNX_AN185_02_02_02_07
 */
class KNX_EXPORT Tunnelling_Feature_Info_Frame : public Connection_Frame
{
public:
    Tunnelling_Feature_Info_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    /** Feature Identifier */
    Tunnelling_Feature_Id feature_identifier{Tunnelling_Feature_Id::UNDEFINED};

    /** Feature Value */
    std::vector<uint8_t> feature_value{};
};

}
