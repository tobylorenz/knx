// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/04/Tunnelling_Feature_Set_Frame.h>

#include <cassert>

namespace KNX {

Tunnelling_Feature_Set_Frame::Tunnelling_Feature_Set_Frame() :
    Connection_Frame(Service_Type_Identifier::TUNNELING_FEATURE_SET)
{
}

void Tunnelling_Feature_Set_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 4));

    Connection_Frame::fromData(first, last);
    first += header_length + 4;

    feature_identifier = static_cast<Tunnelling_Feature_Id>(*first++);
    ++first; // reserved
    feature_value.assign(first, last);
}

std::vector<uint8_t> Tunnelling_Feature_Set_Frame::toData() const
{
    std::vector<uint8_t> data = Connection_Frame::toData();

    data.push_back(static_cast<uint8_t>(feature_identifier));
    data.push_back(0); // reserved
    data.insert(std::cend(data), std::cbegin(feature_value), std::cend(feature_value));

    return data;
}

uint16_t Tunnelling_Feature_Set_Frame::total_length_calculated() const
{
    return
        Connection_Frame::structure_length_calculated() +
        2 +
        static_cast<uint8_t>(feature_value.size());
}

}
