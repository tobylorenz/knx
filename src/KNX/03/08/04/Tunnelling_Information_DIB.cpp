// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/04/Tunnelling_Information_DIB.h>

#include <cassert>

namespace KNX {

Tunnelling_Information_DIB::Tunnelling_Information_DIB() :
    Description_Information_Block()
{
    description_type_code = Description_Type_Code::TUNNELLING_INFO;
}

void Tunnelling_Information_DIB::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 16);

    Description_Information_Block::fromData(first, first + 2);
    first += 2;

    apdu_length = (*first++ << 8) | *first++;
    first += 2;

    while (first != last) {
        Tunnelling_Slot_Information tsi;
        tsi.fromData(first, first + 4);
        tunnelling_slot_information.push_back(tsi);
        first += 4;
    }

    assert(first == last);
}

std::vector<uint8_t> Tunnelling_Information_DIB::toData() const
{
    std::vector<uint8_t> data = Description_Information_Block::toData();

    data.push_back(apdu_length >> 8);
    data.push_back(apdu_length & 0xff);

    for (const auto & tsi : tunnelling_slot_information) {
        std::vector<uint8_t> tsi_data = tsi.toData();
        data.insert(std::cend(data), std::cbegin(tsi_data), std::cend(tsi_data));
    }

    return data;
}

uint8_t Tunnelling_Information_DIB::structure_length_calculated() const
{
    return
        Description_Information_Block::structure_length_calculated() +
        2 +
        static_cast<uint8_t>(4 * tunnelling_slot_information.size());
}

}
