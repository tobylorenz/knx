// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/03/08/04/Tunnelling_Slot_Information.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Tunnelling Information DIB
 *
 * @ingroup KNX_AN185_02_04_01
 */
class KNX_EXPORT Tunnelling_Information_DIB : public Description_Information_Block
{
public:
    Tunnelling_Information_DIB();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    /** APDU-length for bus access */
    uint16_t apdu_length{};

    /** Tunnelling Slot 1..n Information */
    std::vector<Tunnelling_Slot_Information> tunnelling_slot_information{};
};

}
