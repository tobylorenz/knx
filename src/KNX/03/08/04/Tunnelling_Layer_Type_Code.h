// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Tunnelling KNX layers
 *
 * @ingroup KNX_03_08_04_04_04_03
 */
enum class Tunnelling_Layer_Type_Code : uint8_t {
    UNDEFINED = 0x00,

    /** Data Link Layer tunnel */
    TUNNEL_LINKLAYER = 0x02,

    /** Raw tunnel */
    TUNNEL_RAW = 0x04,

    /** Busmonitor tunnel */
    TUNNEL_BUSMONITOR = 0x80
};

}
