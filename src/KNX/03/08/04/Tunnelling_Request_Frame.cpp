// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/04/Tunnelling_Request_Frame.h>

#include <cassert>

namespace KNX {

Tunnelling_Request_Frame::Tunnelling_Request_Frame() :
    Connection_Frame(Service_Type_Identifier::TUNNELING_REQUEST)
{
}

void Tunnelling_Request_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 4));

    Connection_Frame::fromData(first, last);
    first += header_length + 4;

    cemi_frame_data.assign(first, last);
}

std::vector<uint8_t> Tunnelling_Request_Frame::toData() const
{
    std::vector<uint8_t> data = Connection_Frame::toData();

    data.insert(std::cend(data), std::cbegin(cemi_frame_data), std::cend(cemi_frame_data));

    return data;
}

uint16_t Tunnelling_Request_Frame::total_length_calculated() const
{
    return
        Connection_Frame::total_length_calculated() +
        static_cast<uint8_t>(cemi_frame_data.size());
}

}
