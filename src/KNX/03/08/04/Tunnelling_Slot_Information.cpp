// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/04/Tunnelling_Slot_Information.h>

namespace KNX {

void Tunnelling_Slot_Information::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 4);

    individual_address.fromData(first, first + 2);
    first += 2;

    status = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> Tunnelling_Slot_Information::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(individual_address >> 8);
    data.push_back(individual_address & 0xff);

    data.push_back(status >> 8);
    data.push_back(status & 0xff);

    return data;
}

}
