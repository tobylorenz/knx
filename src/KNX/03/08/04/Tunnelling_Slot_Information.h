// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <KNX/03/03/02/Individual_Address.h>
#include <KNX/03/08/04/Tunnelling_Slot_Status.h>

namespace KNX {

/**
 * Tunnelling Slot Information
 *
 * @ingroup KNX_AN185_02_04_01
 */
class Tunnelling_Slot_Information
{
public:
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;

    /** Individual Address */
    Individual_Address individual_address{};

    /** Status */
    Tunnelling_Slot_Status status{};
};

}
