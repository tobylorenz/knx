// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/04/Tunnelling_Slot_Status.h>

namespace KNX {

Tunnelling_Slot_Status::Tunnelling_Slot_Status(const uint16_t status) :
    usable((status >> 2) & 0x01),
    authorised((status >> 1) & 0x01),
    free((status >> 0) & 0x01)
{
}

Tunnelling_Slot_Status & Tunnelling_Slot_Status::operator=(const uint16_t & status)
{
    usable = (status >> 2) & 0x01;
    authorised = (status >> 1) & 0x01;
    free = (status >> 0) & 0x01;

    return *this;
}

Tunnelling_Slot_Status::operator uint16_t() const
{
    return
        (static_cast<uint16_t>(usable) << 2) |
        (static_cast<uint16_t>(authorised) << 1) |
        (static_cast<uint16_t>(free) << 0);
}

}
