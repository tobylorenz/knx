// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/types.h>

namespace KNX {

/**
 * Tunnelling Slot Status
 *
 * @ingroup KNX_AN185_02_04_01
 */
class Tunnelling_Slot_Status
{
public:
    Tunnelling_Slot_Status() = default;
    explicit Tunnelling_Slot_Status(const uint16_t status);
    Tunnelling_Slot_Status & operator=(const uint16_t & status);
    operator uint16_t() const;

    /** Usable (U) */
    bool usable{false};

    /** Authorised (A) */
    bool authorised{false};

    /** Free (F) */
    bool free{false};
};

}
