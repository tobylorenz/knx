// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 4.4.3 Connection Request Information (CRI) */
#include <KNX/03/08/04/Tunnelling_Connection_Request_Information.h>

/* 4.4.4 Connection Response Data Block (CRD) */
#include <KNX/03/08/04/Tunnelling_Connection_Response_Data_Block.h>

/* 4.4.6 TUNNELLING_REQUEST */
#include <KNX/03/08/04/Tunnelling_Request_Frame.h>

/* 4.4.7 TUNNELLING_ACK */
#include <KNX/03/08/04/Tunnelling_Ack_Frame.h>
