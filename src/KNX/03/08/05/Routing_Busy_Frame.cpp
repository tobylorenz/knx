// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/05/Routing_Busy_Frame.h>

#include <cassert>

namespace KNX {

Routing_Busy_Frame::Routing_Busy_Frame() :
    Frame(Service_Type_Identifier::ROUTING_BUSY)
{
}

void Routing_Busy_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= HEADER_SIZE_10 + 6);

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) == 6);

    structure_length = *first++;
    device_state = *first++;
    routing_busy_wait_time = (*first++ << 8) | *first++;
    routing_busy_control_field = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> Routing_Busy_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    data.push_back(structure_length_calculated());
    data.push_back(device_state);
    data.push_back(routing_busy_wait_time >> 8);
    data.push_back(routing_busy_wait_time & 0xff);
    data.push_back(routing_busy_control_field >> 8);
    data.push_back(routing_busy_control_field & 0xff);

    return data;
}

uint16_t Routing_Busy_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        2 +
        structure_length_calculated();
}

uint8_t Routing_Busy_Frame::structure_length_calculated() const
{
    return 4;
}

}
