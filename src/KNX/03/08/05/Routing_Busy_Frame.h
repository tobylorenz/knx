// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Routing Busy Frame
 *
 * @ingroup KNX_03_08_05_05_04
 */
class KNX_EXPORT Routing_Busy_Frame : public Frame
{
public:
    Routing_Busy_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    virtual uint8_t structure_length_calculated() const;

    uint8_t structure_length{4};
    uint8_t device_state{};
    uint16_t routing_busy_wait_time{}; // in milliseconds
    uint16_t routing_busy_control_field{};
};

}
