// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/05/Routing_Indication_Frame.h>

#include <cassert>

namespace KNX {

Routing_Indication_Frame::Routing_Indication_Frame() :
    Frame(Service_Type_Identifier::ROUTING_INDICATION)
{
}

void Routing_Indication_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= HEADER_SIZE_10);

    Frame::fromData(first, last);
    first += header_length;

    cemi_frame_data.assign(first, last);
}

std::vector<uint8_t> Routing_Indication_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    data.insert(std::cend(data), std::cbegin(cemi_frame_data), std::cend(cemi_frame_data));

    return data;
}

uint16_t Routing_Indication_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        static_cast<uint8_t>(cemi_frame_data.size());
}

}
