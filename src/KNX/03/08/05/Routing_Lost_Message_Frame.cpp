// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/05/Routing_Lost_Message_Frame.h>

#include <cassert>

namespace KNX {

Routing_Lost_Message_Frame::Routing_Lost_Message_Frame() :
    Frame(Service_Type_Identifier::ROUTING_INDICATION)
{
}

void Routing_Lost_Message_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= HEADER_SIZE_10 + 4);

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) == 4);

    structure_length = *first++;
    device_state = *first++;
    number_of_lost_messages = (*first++ << 8) | *first++;

    assert(first == last);
}

std::vector<uint8_t> Routing_Lost_Message_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    data.push_back(structure_length_calculated());
    data.push_back(device_state);
    data.push_back(number_of_lost_messages >> 8);
    data.push_back(number_of_lost_messages & 0xff);

    return data;
}

uint16_t Routing_Lost_Message_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        structure_length_calculated();
}

uint8_t Routing_Lost_Message_Frame::structure_length_calculated() const
{
    return 4;
}

}
