// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Routing Lost Message Frame
 *
 * @ingroup KNX_03_08_05_05_03
 */
class KNX_EXPORT Routing_Lost_Message_Frame : public Frame
{
public:
    Routing_Lost_Message_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    virtual uint8_t structure_length_calculated() const;

    uint8_t structure_length{4};
    uint8_t device_state{};
    uint16_t number_of_lost_messages{};
};

}
