// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/05/Routing_System_Broadcast_Frame.h>

#include <cassert>

namespace KNX {

Routing_System_Broadcast_Frame::Routing_System_Broadcast_Frame() :
    Frame(Service_Type_Identifier::ROUTING_SYSTEM_BROADCAST)
{
}

void Routing_System_Broadcast_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= HEADER_SIZE_10);

    Frame::fromData(first, last);
    first += header_length;

    // @todo Routing_System_Broadcast_Frame::fromData
}

std::vector<uint8_t> Routing_System_Broadcast_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    // @todo Routing_System_Broadcast_Frame::toData

    return data;
}

uint16_t Routing_System_Broadcast_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        0; // @todo Routing_System_Broadcast_Frame::total_length_calculated
}

}
