// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Routing System Broadcast Frame
 */
class KNX_EXPORT Routing_System_Broadcast_Frame : public Frame
{
public:
    Routing_System_Broadcast_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    // @todo Routing_System_Broadcast_Frame
};

}
