// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 5.2 ROUTING_INDICATION */
#include <KNX/03/08/05/Routing_Indication_Frame.h>

/* 5.3 ROUTING_LOST_MESSAGE */
#include <KNX/03/08/05/Routing_Lost_Message_Frame.h>

/* 5.4 ROUTING_BUSY */
#include <KNX/03/08/05/Routing_Busy_Frame.h>
