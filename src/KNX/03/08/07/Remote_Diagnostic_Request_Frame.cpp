// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/07/Remote_Diagnostic_Request_Frame.h>

#include <cassert>

namespace KNX {

Remote_Diagnostic_Request_Frame::Remote_Diagnostic_Request_Frame() :
    Frame(Service_Type_Identifier::REMOTE_DIAGNOSTIC_REQUEST)
{
}

void Remote_Diagnostic_Request_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 4));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 4);

    std::uint8_t structure_length = *first;
    discovery_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    assert(std::distance(first, last) >= 2);

    structure_length = *first;
    selector = make_Selector(first, first + structure_length);
    first += structure_length;

    assert(first == last);
}

std::vector<uint8_t> Remote_Diagnostic_Request_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    std::vector<uint8_t> discovery_endpoint_data = discovery_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(discovery_endpoint_data), std::cend(discovery_endpoint_data));

    std::vector<uint8_t> selector_data = selector->toData();
    data.insert(std::cend(data), std::cbegin(selector_data), std::cend(selector_data));

    return data;
}

uint16_t Remote_Diagnostic_Request_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        discovery_endpoint->structure_length_calculated() +
        selector->structure_length_calculated();
}

}
