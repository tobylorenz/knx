// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Description_Information_Block.h>
#include <KNX/03/08/02/Frame.h>
#include <KNX/03/08/02/Host_Protocol_Address_Information.h>
#include <KNX/03/08/07/Selector.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Remote Diagnostic Response
 *
 * @ingroup KNX_03_08_07_04_04_02
 */
class KNX_EXPORT Remote_Diagnostic_Response_Frame : public Frame
{
public:
    Remote_Diagnostic_Response_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    std::shared_ptr<Selector> selector{};
    std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks{};
};

}
