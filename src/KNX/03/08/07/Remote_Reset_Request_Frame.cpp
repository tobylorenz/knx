// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/07/Remote_Reset_Request_Frame.h>

#include <cassert>

namespace KNX {

Remote_Reset_Request_Frame::Remote_Reset_Request_Frame() :
    Frame(Service_Type_Identifier::REMOTE_RESET_REQUEST)
{
}

void Remote_Reset_Request_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 4));

    Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 4);

    std::uint8_t structure_length = *first;
    selector = make_Selector(first, first + structure_length);
    first += structure_length;

    assert(std::distance(first, last) >= 2);

    reset_command = static_cast<Reset_Command_Type_Code>(*first++);
    ++first; // reserved

    assert(first == last);
}

std::vector<uint8_t> Remote_Reset_Request_Frame::toData() const
{
    std::vector<uint8_t> data = Frame::toData();

    std::vector<uint8_t> selector_data = selector->toData();
    data.insert(std::cend(data), std::cbegin(selector_data), std::cend(selector_data));

    data.push_back(static_cast<uint8_t>(reset_command));
    data.push_back(0); // reserved

    return data;
}

uint16_t Remote_Reset_Request_Frame::total_length_calculated() const
{
    return
        Frame::total_length_calculated() +
        selector->structure_length_calculated() +
        2;
}

}
