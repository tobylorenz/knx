// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/02/Frame.h>
#include <KNX/03/08/07/Reset_Command_Type_Code.h>
#include <KNX/03/08/07/Selector.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Remote Reset Request Frame
 *
 * @ingroup KNX_03_08_07_04_04_04
 */
class KNX_EXPORT Remote_Reset_Request_Frame : public Frame
{
public:
    Remote_Reset_Request_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    std::shared_ptr<Selector> selector{};
    Reset_Command_Type_Code reset_command{Reset_Command_Type_Code::Undefined};
};

}
