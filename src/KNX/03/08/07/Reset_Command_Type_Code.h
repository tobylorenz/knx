// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Reset Command Type
 *
 * @ingroup KNX_03_08_07_04_07
 */
enum class Reset_Command_Type_Code : uint8_t {
    Undefined = 0x00,
    Restart = 0x01,
    Master_Reset = 0x02,
};

}
