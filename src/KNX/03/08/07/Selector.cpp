// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/07/Selector.h>

#include <cassert>

namespace KNX {

void Selector::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 2);

    structure_length = *first++;

    selector_type_code = static_cast<Selector_Type_Code>(*first++);

    assert(first == last);
}

std::vector<uint8_t> Selector::toData() const
{
    std::vector<uint8_t> data;

    data.push_back(structure_length);
    data.push_back(static_cast<uint8_t>(selector_type_code));

    return data;
}

uint8_t Selector::structure_length_calculated() const
{
    return 2;
}

std::shared_ptr<Selector> make_Selector(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    Selector selector;
    selector.fromData(first, first + 2);
    switch (selector.selector_type_code) {
    case Selector_Type_Code::Undefined:
        break;

    case Selector_Type_Code::PrgMode: {
        std::shared_ptr<PrgMode_Selector> selector2 = std::make_shared<PrgMode_Selector>();
        selector2->fromData(first, last);
        return selector2;
    }
    break;

    case Selector_Type_Code::MAC: {
        std::shared_ptr<MAC_Selector> selector2 = std::make_shared<MAC_Selector>();
        selector2->fromData(first, last);
        return selector2;
    }
    break;
    }
    return nullptr;
}

PrgMode_Selector::PrgMode_Selector() :
    Selector()
{
    selector_type_code = Selector_Type_Code::PrgMode;
}

MAC_Selector::MAC_Selector() :
    Selector()
{
    selector_type_code = Selector_Type_Code::MAC;
}

void MAC_Selector::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) == 6);

    Selector::fromData(first, first + 2);
    first += 2;

    std::copy(first, first + 4, std::begin(mac_address));
    first += 4;

    assert(first == last);
}

std::vector<uint8_t> MAC_Selector::toData() const
{
    std::vector<uint8_t> data = Selector::toData();

    data.insert(std::cend(data), std::cbegin(mac_address), std::cend(mac_address));

    return data;
};

uint8_t MAC_Selector::structure_length_calculated() const
{
    return
        Selector::structure_length_calculated() +
        6;
}

}
