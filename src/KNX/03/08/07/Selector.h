// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>
#include <memory>
#include <vector>

#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * Selector
 *
 * @ingroup KNX_03_08_07_04_06
 */
enum class Selector_Type_Code : uint8_t {
    Undefined = 0x00,
    PrgMode = 0x01,
    MAC = 0x02,
};

/**
 * Selector
 *
 * @ingroup KNX_03_08_07_04_06
 */
class KNX_EXPORT Selector
{
public:
    virtual void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);
    virtual std::vector<uint8_t> toData() const;
    virtual uint8_t structure_length_calculated() const;

    uint8_t structure_length{2};
    Selector_Type_Code selector_type_code{Selector_Type_Code::Undefined};
};

std::shared_ptr<Selector> make_Selector(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last);

/**
 * PrgMode Selector
 *
 * @ingroup KNX_03_08_07_04_06_01
 */
class KNX_EXPORT PrgMode_Selector : public Selector
{
public:
    PrgMode_Selector();
};

/**
 * MAC Selector
 *
 * @ingroup KNX_03_08_07_04_06_02
 */
class KNX_EXPORT MAC_Selector : public Selector
{
public:
    MAC_Selector();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint8_t structure_length_calculated() const override;

    std::array<uint8_t, 6> mac_address{};
};

}
