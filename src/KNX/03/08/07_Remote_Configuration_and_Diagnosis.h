// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 4.4.1 REMOTE_DIAGNOSTIC_REQUEST */
#include <KNX/03/08/07/Remote_Diagnostic_Request_Frame.h>

/* 4.4.2 REMOTE_DIAGNOSTIC_RESPONSE */
#include <KNX/03/08/07/Remote_Diagnostic_Response_Frame.h>

/* 4.4.3 REMOTE_BASIC_CONFIGURATION_REQUEST */
#include <KNX/03/08/07/Remote_Basic_Configuration_Request_Frame.h>

/* 4.4.4 REMOTE_RESET_REQUEST */
#include <KNX/03/08/07/Remote_Reset_Request_Frame.h>

/* 4.6 SELECTOR */
#include <KNX/03/08/07/Selector.h>

/* 4.7 RESET COMMAND */
