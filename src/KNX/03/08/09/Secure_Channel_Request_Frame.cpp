// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/09/Secure_Channel_Request_Frame.h>

#include <cassert>

namespace KNX {

Secure_Channel_Request_Frame::Secure_Channel_Request_Frame() :
    Secure_Frame(Service_Type_Identifier::SECURE_CHANNEL_REQUEST)
{
}

void Secure_Channel_Request_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 38));

    Secure_Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 38);

    uint8_t structure_length = *first;
    control_endpoint = make_Host_Protocol_Address_Information(first, first + structure_length);
    first += structure_length;

    assert(std::distance(first, last) == 36);

    std::copy(first, first + 36, std::begin(client_public_value));
    first += 36;

    assert(first == last);
}

std::vector<uint8_t> Secure_Channel_Request_Frame::toData() const
{
    std::vector<uint8_t> data = Secure_Frame::toData();

    std::vector<uint8_t> control_endpoint_data = control_endpoint->toData();
    data.insert(std::cend(data), std::cbegin(control_endpoint_data), std::cend(control_endpoint_data));

    data.insert(std::cend(data), std::cbegin(client_public_value), std::cend(client_public_value));

    return data;
}

uint16_t Secure_Channel_Request_Frame::total_length_calculated() const
{
    return
        Secure_Frame::total_length_calculated() +
        control_endpoint->structure_length_calculated() +
        36;
}

}
