// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/08/02/Host_Protocol_Address_Information.h>
#include <KNX/03/08/09/Secure_Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Secure Channel Request Frame
 *
 * @ingroup KNX_AN159_02_04_04
 */
class KNX_EXPORT Secure_Channel_Request_Frame : public Secure_Frame
{
public:
    Secure_Channel_Request_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    // unencrypted data

    std::shared_ptr<Host_Protocol_Address_Information> control_endpoint{};
    std::array<uint8_t, 36> client_public_value{};
};

}
