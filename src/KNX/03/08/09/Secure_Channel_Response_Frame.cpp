// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/09/Secure_Channel_Response_Frame.h>

#include <cassert>

namespace KNX {

Secure_Channel_Response_Frame::Secure_Channel_Response_Frame() :
    Secure_Frame(Service_Type_Identifier::SECURE_CHANNEL_RESPONSE)
{
}

void Secure_Channel_Response_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 2));

    Secure_Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 2);

    secure_channel_index = (*first++ << 8) | *first++;

    if (secure_channel_index != 0) {
        assert(std::distance(first, last) == 52);

        std::copy(first, first + 36, std::begin(server_public_value));
        first += 36;
        std::copy(first, first + 16, std::begin(message_authentication_code));
        first += 16;
    }

    assert(first == last);
}

std::vector<uint8_t> Secure_Channel_Response_Frame::toData() const
{
    std::vector<uint8_t> data = Secure_Frame::toData();

    data.push_back(secure_channel_index >> 8);
    data.push_back(secure_channel_index & 0xff);
    if (secure_channel_index != 0) {
        data.insert(std::cend(data), std::cbegin(server_public_value), std::cend(server_public_value));
        data.insert(std::cend(data), std::cbegin(message_authentication_code), std::cend(message_authentication_code));
    }

    return data;
}

uint16_t Secure_Channel_Response_Frame::total_length_calculated() const
{
    uint16_t total_length =
        Secure_Frame::total_length_calculated() +
        2;
    if (secure_channel_index != 0) {
        total_length += 52;
    }
    return total_length;
}

}
