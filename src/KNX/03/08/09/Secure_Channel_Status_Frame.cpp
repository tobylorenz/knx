// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/09/Secure_Channel_Status_Frame.h>

#include <cassert>

namespace KNX {

Secure_Channel_Status_Frame::Secure_Channel_Status_Frame() :
    Secure_Frame(Service_Type_Identifier::SECURE_CHANNEL_STATUS)
{
}

void Secure_Channel_Status_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 2));

    Secure_Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) == 2);

    status = static_cast<Status>(*first++);
    ++first;

    assert(first == last);
}

std::vector<uint8_t> Secure_Channel_Status_Frame::toData() const
{
    std::vector<uint8_t> data = Secure_Frame::toData();

    data.push_back(static_cast<uint8_t>(status));
    data.push_back(0);

    return data;
}

uint16_t Secure_Channel_Status_Frame::total_length_calculated() const
{
    return
        Secure_Frame::total_length_calculated() +
        2;
}

}
