// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/08/09/Secure_Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Secure Channel Status Frame
 *
 * @ingroup KNX_AN159_02_04_07
 */
class KNX_EXPORT Secure_Channel_Status_Frame : public Secure_Frame
{
public:
    Secure_Channel_Status_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    enum class Status {
        STATUS_AUTHORIZATION_SUCCESS = 0x00,
        STATUS_AUTHORIZATION_FAILED = 0x01,
        STATUS_ERROR_UNAUTHORIZED = 0x02,
        STATUS_TIMEOUT = 0x03
    };

    Status status{Status::STATUS_AUTHORIZATION_SUCCESS};
};

}
