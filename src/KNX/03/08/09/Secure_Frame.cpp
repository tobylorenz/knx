// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/09/Secure_Frame.h>

namespace KNX {

Secure_Frame::Secure_Frame() :
    Frame()
{
    protocol_version = KNXNETIP_VERSION_13;
}

Secure_Frame::Secure_Frame(const Service_Type_Identifier service_type_identifier) :
    Frame(service_type_identifier)
{
    protocol_version = KNXNETIP_VERSION_13;
}

}
