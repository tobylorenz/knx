// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/08/02/Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

class KNX_EXPORT Secure_Frame : public Frame
{
public:
    Secure_Frame();
    explicit Secure_Frame(const Service_Type_Identifier service_type_identifier);
};

}
