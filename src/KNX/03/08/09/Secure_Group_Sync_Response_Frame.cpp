// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/09/Secure_Group_Sync_Response_Frame.h>

#include <cassert>

namespace KNX {

Secure_Group_Sync_Response_Frame::Secure_Group_Sync_Response_Frame() :
    Secure_Frame(Service_Type_Identifier::SECURE_GROUP_SYNC_RESPONSE)
{
}

void Secure_Group_Sync_Response_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 22));

    Secure_Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) == 22);

    std::copy(first, first + 6, std::begin(time_stamp));
    first += 6;

    std::copy(first, first + 16, std::begin(message_authentication_code));
    first += 16;

    assert(first == last);
}

std::vector<uint8_t> Secure_Group_Sync_Response_Frame::toData() const
{
    std::vector<uint8_t> data = Secure_Frame::toData();

    data.insert(std::cend(data), std::cbegin(time_stamp), std::cend(time_stamp));
    data.insert(std::cend(data), std::cbegin(message_authentication_code), std::cend(message_authentication_code));

    return data;
}

uint16_t Secure_Group_Sync_Response_Frame::total_length_calculated() const
{
    return
        Secure_Frame::total_length_calculated() +
        22;
}

}
