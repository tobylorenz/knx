// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>

#include <KNX/03/08/09/Secure_Frame.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Secure Group Sync Response Frame
 *
 * @ingroup KNX_AN159_02_04_09
 */
class KNX_EXPORT Secure_Group_Sync_Response_Frame : public Secure_Frame
{
public:
    Secure_Group_Sync_Response_Frame();

    void fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last) override;
    std::vector<uint8_t> toData() const override;
    uint16_t total_length_calculated() const override;

    // unencrypted data

    std::array<uint8_t, 6> time_stamp{};
    std::array<uint8_t, 16> message_authentication_code{};
};

}
