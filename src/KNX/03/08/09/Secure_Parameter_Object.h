// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

namespace KNX {

enum class Secure_Parameter_Object {
    PID_GROUP_KEY = 51,
    PID_DEVICE_AUTHENTICATION_CODE = 52,
    PID_PASSWORD_HASHES = 53,
    PID_SECURED_SERVICES = 54,
    PID_MULTICAST_LATENCY_TOLERANCE = 55,
    PID_DOMAIN_SEND_MASK = 56,
    PID_DOMAIN_ACCESS_MASK = 57,
};

}
