// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/09/Secure_Wrapper_Frame.h>

#include <cassert>

namespace KNX {

Secure_Wrapper_Frame::Secure_Wrapper_Frame() :
    Secure_Frame(Service_Type_Identifier::SECURE_WRAPPER)
{
}

void Secure_Wrapper_Frame::fromData(std::vector<uint8_t>::const_iterator first, std::vector<uint8_t>::const_iterator last)
{
    assert(std::distance(first, last) >= (HEADER_SIZE_10 + 26));

    Secure_Frame::fromData(first, last);
    first += header_length;

    assert(std::distance(first, last) >= 26);

    secure_channel_index = (*first++ << 8) | *first++;

    std::copy(first, first + 6, std::begin(sequence_identifier_group_counter));
    first += 6;

    security_domain_mask = (*first++ << 8) | *first++;

    assert(std::distance(first, last) >= 16);

    uint16_t structure_length = total_length - 32;
    original_frame.assign(first, first + structure_length);
    first += structure_length;

    assert(std::distance(first, last) >= 16);

    std::copy(first, first + 16, std::begin(message_authentication_code));
    first += 16;

    assert(first == last);
}

std::vector<uint8_t> Secure_Wrapper_Frame::toData() const
{
    std::vector<uint8_t> data = Secure_Frame::toData();

    data.push_back(secure_channel_index >> 8);
    data.push_back(secure_channel_index & 0xff);
    data.insert(std::cend(data), std::cbegin(sequence_identifier_group_counter), std::cend(sequence_identifier_group_counter));
    data.push_back(security_domain_mask >> 8);
    data.push_back(security_domain_mask & 0xff);
    data.insert(std::cend(data), std::cbegin(original_frame), std::cend(original_frame));
    data.insert(std::cend(data), std::cbegin(message_authentication_code), std::cend(message_authentication_code));

    return data;
}

uint16_t Secure_Wrapper_Frame::total_length_calculated() const
{
    return
        Secure_Frame::total_length_calculated() +
        26 +
        static_cast<uint8_t>(original_frame.size());
}

}
