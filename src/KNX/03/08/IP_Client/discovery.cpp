// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <functional>
#include <iostream>
#include <memory>
#include <string>

#include <asio.hpp>
#include <yaml-cpp/yaml.h>

#include <KNX/03/02/06/IP_Data_Link_Layer.h>
#include <KNX/03/03/03/Network_Layer.h>
#include <KNX/03/03/04/Transport_Layer.h>
#include <KNX/03/03/07/Application_Layer.h>
#include <KNX/03/08/02_Core.h>
#include <KNX/03/08/IP_Client/yaml_output.h>
#include <KNX/03/08/IP_Communication_Channel.h>
#include <KNX/03/08/IP_Discovery_Channel.h>

namespace KNX {

class Discovery
{
public:
    explicit Discovery(asio::io_context & io_context) :
        m_io_context(io_context),
        m_discovery_channel(m_io_context),
        m_communication_channel(m_io_context),
        m_group_address_table(std::make_shared<Group_Address_Table>()),
        m_group_object_association_table(std::make_shared<Group_Object_Association_Table>()),
        m_data_link_layer(),
        m_network_layer(),
        m_transport_layer(io_context),
        m_application_layer() {
        const TSAP_Group tsap = m_group_address_table->group_addresses.insert(8193); // Zeit, DPST-10-1

        m_group_object_association_table->add_entry(tsap, 1);

        m_network_layer.connect(m_data_link_layer);

        m_transport_layer.group_address_table = m_group_address_table;
        m_transport_layer.connect(m_network_layer);

        m_application_layer.group_object_association_table = m_group_object_association_table;
        m_application_layer.connect(m_transport_layer);
        m_application_layer.A_GroupValue_Write_ind = std::bind(&Discovery::on_A_GroupValue_Write_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4);

        m_discovery_channel.bind();
    }

    void search_request() {
        m_discovery_channel.on_search_response = std::bind(&Discovery::on_search_response, this, std::placeholders::_1);
        m_discovery_channel.search_request();
    }

protected:
    asio::io_context & m_io_context;
    IP_Discovery_Channel m_discovery_channel;
    IP_Communication_Channel m_communication_channel;
    std::shared_ptr<Group_Address_Table> m_group_address_table{nullptr};
    std::shared_ptr<Group_Object_Association_Table> m_group_object_association_table{nullptr};
    IP_Data_Link_Layer m_data_link_layer;
    Network_Layer m_network_layer;
    Transport_Layer m_transport_layer;
    Application_Layer m_application_layer;

    void on_search_response(const Search_Response_Frame & search_response_frame) {
        YAML::Emitter emitter;
        emitter << YAML::BeginMap << search_response_frame << YAML::EndMap;
        std::cout << emitter.c_str() << std::endl;

        m_communication_channel.on_connect = std::bind(&Discovery::on_control_connect, this);
        std::shared_ptr<IP_Host_Protocol_Address_Information> hpai = std::dynamic_pointer_cast<IP_Host_Protocol_Address_Information>(search_response_frame.control_endpoint);
        m_communication_channel.connect(*hpai);
    }

    void on_control_connect() {
        m_communication_channel.on_description_response = std::bind(&Discovery::on_description_response, this, std::placeholders::_1);
        m_communication_channel.description_request();
    }

    void on_description_response(const Description_Response_Frame & description_response_frame) {
        YAML::Emitter emitter;
        emitter << YAML::BeginMap << description_response_frame << YAML::EndMap;
        std::cout << emitter.c_str() << std::endl;

        for (const std::shared_ptr<Description_Information_Block> & dib : description_response_frame.other_device_information) {
            if (dib->description_type_code() == Description_Type_Code::KNX_ADDRESSES) {
                const std::shared_ptr<Addresses_DIB> addresses_dib = std::dynamic_pointer_cast<Addresses_DIB>(dib);
                m_group_address_table->set_individual_address(addresses_dib->individual_address());
            }
        }

        m_communication_channel.on_connect_response = std::bind(&Discovery::on_connect_response, this, std::placeholders::_1);
        m_communication_channel.tunnel_connect_request(Tunnelling_Layer_Type_Code::TUNNEL_LINKLAYER);
    }

    void on_connect_response(const Connect_Response_Frame & connect_response_frame) {
        YAML::Emitter emitter;
        emitter << YAML::BeginMap << connect_response_frame << YAML::EndMap;
        std::cout << emitter.c_str() << std::endl;

        m_communication_channel.on_tunnelling_request = std::bind(&Discovery::on_tunnelling_request, this, std::placeholders::_1);
    }

    void on_tunnelling_request(const Tunnelling_Request_Frame & frame) {
        const std::vector<uint8_t> cemi_frame_data = frame.cemi_frame_data();
        m_data_link_layer.Ph_Data_ind(cemi_frame_data);
    }

    void on_A_GroupValue_Write_ind(const ASAP_Group & asap, const Priority priority, const Hop_Count_Type hop_count_type, const std::vector<uint8_t> & data) {
        std::cout << "A_GroupValue_Write_ind(asap=" << asap
                  << ", priority=" << static_cast<uint16_t>(priority)
                  << ", hop_count_type=" << static_cast<uint16_t>(hop_count_type)
                  << ", data=" << to_string(data)
                  << ")" << std::endl;
    }
};

int main()
{
    asio::io_context io_context;
    Discovery discovery(io_context);
    discovery.search_request();
    io_context.run();

    return 0;
}

}
