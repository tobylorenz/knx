// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/IP_Client/yaml_output.h>

#include <array>
#include <cstdint>
#include <iostream>
#include <map>
#include <string>
#include <vector>

namespace KNX {

std::string to_hex(const uint8_t value)
{
    std::ostringstream os;
    os.width(2);
    os.fill('0');
    os << std::hex << static_cast<uint16_t>(value);
    return os.str();
}

std::string to_hex(const uint16_t value)
{
    std::ostringstream os;
    os.width(4);
    os.fill('0');
    os << std::hex << value;
    return os.str();
}

std::string to_string(const std::array<uint8_t, 4> & ip_address)
{
    std::ostringstream os;
    os << std::dec
       << static_cast<uint16_t>(ip_address[0])
       << "."
       << static_cast<uint16_t>(ip_address[1])
       << "."
       << static_cast<uint16_t>(ip_address[2])
       << "."
       << static_cast<uint16_t>(ip_address[3]);
    return os.str();
}

std::string to_string(const std::array<uint8_t, 6> & mac_address)
{
    std::ostringstream os;
    os << to_hex(mac_address[0])
       << " "
       << to_hex(mac_address[1])
       << " "
       << to_hex(mac_address[2])
       << " "
       << to_hex(mac_address[3])
       << " "
       << to_hex(mac_address[4])
       << " "
       << to_hex(mac_address[5]);
    return os.str();
}

std::string to_string(const std::array<char, 30> & name)
{
    std::string namestr(std::cbegin(name), std::cend(name));
    while (namestr.back() == 0) {
        namestr.pop_back();
    }
    return namestr;
}

std::string to_string(const std::map<uint8_t, uint8_t> & service_families)
{
    std::ostringstream os;
    for (auto const & f : service_families) {
        std::cout << to_hex(f.first) << "=" << to_hex(f.second);
        std::cout << ", ";
    }
    return os.str();
}

std::string to_string(const std::vector<uint16_t> & additional_addresses)
{
    std::ostringstream os;
    for (auto const & a : additional_addresses) {
        os << to_hex(a) << ",";
    }
    return os.str();
}

std::string to_string(const std::vector<uint8_t> & data)
{
    std::ostringstream os;
    for (const uint8_t & d : data) {
        os << to_hex(d) << " ";
    }
    return os.str();
}

/* 2 KNXnet/IP frames */

/* 2.2 Frame format */

YAML::Emitter & operator<<(YAML::Emitter & out, const Frame & obj)
{
    out << YAML::Key << "Frame";
    out << YAML::BeginMap;
    out << YAML::Key << "header_length" << YAML::Value << static_cast<uint16_t>(obj.header_length());
    out << YAML::Key << "protocol_version" << YAML::Value << "0x" + to_hex(obj.protocol_version());
    out << YAML::Key << "service_type_identifier" << YAML::Value << "0x" + to_hex(static_cast<uint16_t>(obj.service_type_identifier()));
    out << YAML::Key << "total_length" << YAML::Value << obj.total_length();
    out << YAML::EndMap;
    return out;
}

/* 7.5 Placeholders */

/* 7.5.1 Host Protocol Address Information (HPAI) */

YAML::Emitter & operator<<(YAML::Emitter & out, const Host_Protocol_Address_Information & obj)
{
    out << YAML::Key << "Host_Protocol_Address_Information";
    out << YAML::BeginMap;
    out << YAML::Key << "structure_length" << YAML::Value << static_cast<uint16_t>(obj.structure_length());
    out << YAML::Key << "host_protocol_code" << YAML::Value << static_cast<uint16_t>(obj.host_protocol_code());
    out << YAML::EndMap;
    return out;
}

/* 7.5.2 Connection Request Information (CRI) */

YAML::Emitter & operator<<(YAML::Emitter & out, const Connection_Request_Information & obj)
{
    out << YAML::Key << "Connection_Request_Information";
    out << YAML::BeginMap;
    out << YAML::Key << "structure_length" << YAML::Value << static_cast<const uint16_t>(obj.structure_length());
    out << YAML::Key << "connection_type_code" << YAML::Value << to_hex(static_cast<uint16_t>(obj.connection_type_code()));
    out << YAML::EndMap;
    return out;
}

/* 7.5.3 Connection Response Data Block (CRD) */

YAML::Emitter & operator<<(YAML::Emitter & out, const Connection_Response_Data_Block & obj)
{
    out << YAML::Key << "Connection_Response_Data_Block";
    out << YAML::BeginMap;
    out << YAML::Key << "structure_length" << YAML::Value << static_cast<const uint16_t>(obj.structure_length());
    out << YAML::Key << "connection_type_code" << YAML::Value << to_hex(static_cast<uint16_t>(obj.connection_type_code()));
    out << YAML::EndMap;
    return out;
}

/* 7.5.4 Description Information Block (DIB) */

YAML::Emitter & operator<<(YAML::Emitter & out, const Description_Information_Block & obj)
{
    out << YAML::Key << "Description_Information_Block";
    out << YAML::BeginMap;
    out << YAML::Key << "structure_length" << YAML::Value << static_cast<uint16_t>(obj.structure_length());
    out << YAML::Key << "description_type_code" << YAML::Value << static_cast<uint16_t>(obj.description_type_code());
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const Device_Information_DIB & obj)
{
    out << YAML::Key << "Device_Information_DIB";
    out << YAML::BeginMap;
    out << static_cast<const Description_Information_Block>(obj);
    out << YAML::Key << "medium" << YAML::Value << static_cast<uint16_t>(obj.medium());
    out << YAML::Key << "device_status" << YAML::Value << static_cast<uint16_t>(obj.device_status());
    out << YAML::Key << "individual_address" << YAML::Value << "0x" + to_hex(obj.individual_address());
    out << YAML::Key << "project_installation_identifier" << YAML::Value << obj.project_installation_identifier();
    out << YAML::Key << "device_serial_number" << YAML::Value << to_string(obj.device_serial_number());
    out << YAML::Key << "device_routing_multicast_address" << YAML::Value << to_string(obj.device_routing_multicast_address());
    out << YAML::Key << "device_mac_address" << YAML::Value << to_string(obj.device_mac_address());
    out << YAML::Key << "device_friendly_name" << YAML::Value << to_string(obj.device_friendly_name());
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const Supported_Service_Families_DIB & obj)
{
    out << YAML::Key << "Supported_Service_Families_DIB";
    out << YAML::BeginMap;
    out << static_cast<const Description_Information_Block>(obj);
    out << YAML::Key << "service_families";
    out << YAML::BeginMap;
    for (const auto & sf : obj.service_families()) {
        out << YAML::Key << "0x" + to_hex(sf.first) << YAML::Value << "0x" + to_hex(sf.second);
    }
    out << YAML::EndMap;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const IP_Config_DIB & obj)
{
    out << YAML::Key << "IP_Config_DIB";
    out << YAML::BeginMap;
    out << static_cast<const Description_Information_Block>(obj);
    out << YAML::Key << "ip_address" << YAML::Value << to_string(obj.ip_address());
    out << YAML::Key << "subnet_mask" << YAML::Value << to_string(obj.subnet_mask());
    out << YAML::Key << "default_gateway" << YAML::Value << to_string(obj.default_gateway());
    out << YAML::Key << "ip_capabilities" << YAML::Value << static_cast<uint16_t>(obj.ip_capabilities());
    out << YAML::Key << "ip_assignment_method" << YAML::Value << static_cast<uint16_t>(obj.ip_assignment_method());
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const IP_Current_Config_DIB & obj)
{
    out << YAML::Key << "IP_Current_Config_DIB";
    out << YAML::BeginMap;
    out << static_cast<const Description_Information_Block>(obj);
    out << YAML::Key << "current_ip_address" << YAML::Value << to_string(obj.current_ip_address());
    out << YAML::Key << "current_subnet_mask" << YAML::Value << to_string(obj.current_subnet_mask());
    out << YAML::Key << "current_default_gateway" << YAML::Value << to_string(obj.current_default_gateway());
    out << YAML::Key << "dhcp_server" << YAML::Value << to_string(obj.dhcp_server());
    out << YAML::Key << "current_ip_assignment_method" << YAML::Value << static_cast<uint16_t>(obj.current_ip_assignment_method());
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const Addresses_DIB & obj)
{
    out << YAML::Key << "KNX_Addresses_DIB";
    out << YAML::BeginMap;
    out << static_cast<const Description_Information_Block>(obj);
    out << YAML::Key << "individual_address" << YAML::Value << static_cast<uint16_t>(obj.individual_address());
    out << YAML::Key << "additional_individual_addresses" << YAML::Value << obj.additional_individual_addresses();
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const Manufacturer_Data_DIB & obj)
{
    out << YAML::Key << "Manufacturer_Data_DIB";
    out << YAML::BeginMap;
    out << static_cast<const Description_Information_Block>(obj);
    out << YAML::Key << "manufacturer_id" << YAML::Value << static_cast<uint16_t>(obj.manufacturer_id());
    out << YAML::Key << "manufacturer_specific_data" << YAML::Value << obj.manufacturer_specific_data();
    out << YAML::EndMap;
    return out;
}

/* 7.6 Discovery */

YAML::Emitter & operator<<(YAML::Emitter & out, const Search_Request_Frame & obj)
{
    out << YAML::Key << "Search_Request";
    out << YAML::BeginMap;
    out << dynamic_cast<const Frame &>(obj);
    out << *obj.discovery_endpoint;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const Search_Response_Frame & obj)
{
    out << YAML::Key << "Search_Response";
    out << YAML::BeginMap;
    out << dynamic_cast<const Frame &>(obj);
    out << *obj.control_endpoint;
    out << *obj.device_hardware;
    out << *obj.supported_service_families;
    out << YAML::EndMap;
    return out;
}

/* 7.7 Self description */

YAML::Emitter & operator<<(YAML::Emitter & out, const Description_Request_Frame & obj)
{
    out << YAML::Key << "Description_Request";
    out << YAML::BeginMap;
    out << dynamic_cast<const Frame &>(obj);
    out << *obj.control_endpoint;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const Description_Response_Frame & obj)
{
    out << YAML::Key << "Description_Response";
    out << YAML::BeginMap;
    out << dynamic_cast<const Frame &>(obj);
    out << *obj.device_hardware;
    out << *obj.supported_service_families;
    for (const auto & dib : obj.other_device_information) {
        out << *dib;
    }
    out << YAML::EndMap;
    return out;
}

/* 7.8 Connection management */

YAML::Emitter & operator<<(YAML::Emitter & out, const Connect_Request_Frame & obj)
{
    out << YAML::Key << "Description_Request";
    out << YAML::BeginMap;
    out << dynamic_cast<const Frame &>(obj);
    out << *obj.control_endpoint;
    out << *obj.data_endpoint;
    out << *obj.connection_request_information;
    out << YAML::EndMap;
    return out;
}

YAML::Emitter & operator<<(YAML::Emitter & out, const Connect_Response_Frame & obj)
{
    out << YAML::Key << "Connect_Response";
    out << YAML::BeginMap;
    out << dynamic_cast<const Frame &>(obj);
    out << YAML::Key << "communication_channel_id" << YAML::Value << static_cast<uint16_t>(obj.communication_channel_id());
    out << YAML::Key << "status" << YAML::Value << "0x" + to_hex(static_cast<uint8_t>(obj.status()));
    out << *obj.data_endpoint;
    out << *obj.connection_response_data_block;
    out << YAML::EndMap;
    return out;
}

/* 8.6.2 Host Protocol Address Information */

YAML::Emitter & operator<<(YAML::Emitter & out, const IP_Host_Protocol_Address_Information & obj)
{
    out << YAML::Key << "IP_Host_Protocol_Address_Information";
    out << YAML::BeginMap;
    out << static_cast<const Host_Protocol_Address_Information>(obj);
    out << YAML::Key << "structure_length" << YAML::Value << static_cast<uint16_t>(obj.structure_length());
    out << YAML::Key << "host_protocol_code" << YAML::Value << static_cast<uint16_t>(obj.host_protocol_code());
    switch (obj.host_protocol_code()) {
    case Host_Protocol_Code::UNDEFINED:
        break;
    case Host_Protocol_Code::IPV4_UDP:
    case Host_Protocol_Code::IPV4_TCP:
        out << YAML::Key << "ip_address" << YAML::Value << to_string(obj.ip_address());
        out << YAML::Key << "ip_port_number" << YAML::Value << obj.ip_port_number();
        break;
    }
    out << YAML::EndMap;
    return out;
}

}
