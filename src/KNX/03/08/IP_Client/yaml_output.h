// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <array>
#include <map>
#include <string>
#include <vector>

#include <yaml-cpp/yaml.h>

#include <KNX/03/08/02_Core.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

// @todo split this into string conversion functions and YAML output functions
// @todo shift YAML output functions to class definition/implementation files
KNX_EXPORT std::string to_hex(const uint8_t value);

KNX_EXPORT std::string to_hex(const uint16_t value);

KNX_EXPORT std::string to_string(const std::array<uint8_t, 4> & ip_address);

KNX_EXPORT std::string to_string(const std::array<uint8_t, 6> & mac_address);

KNX_EXPORT std::string to_string(const std::array<char, 30> & name);

KNX_EXPORT std::string to_string(const std::map<uint8_t, uint8_t> & service_families);

KNX_EXPORT std::string to_string(const std::vector<uint16_t> & additional_addresses);

KNX_EXPORT std::string to_string(const std::vector<uint8_t> & data);

/* 2 KNXnet/IP frames */

/* 2.2 Frame format */

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Frame & obj);

/* 7.5 Placeholders */

/* 7.5.1 Host Protocol Address Information (HPAI) */

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Host_Protocol_Address_Information & obj);

/* 7.5.2 Connection Request Information (CRI) */

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Connection_Request_Information & obj);

/* 7.5.3 Connection Response Data Block (CRD) */

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Connection_Response_Data_Block & obj);

/* 7.5.4 Description Information Block (DIB) */

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Description_Information_Block & obj);

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Device_Information_DIB & obj);

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Supported_Service_Families_DIB & obj);

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const IP_Config_DIB & obj);

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const IP_Current_Config_DIB & obj);

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Addresses_DIB & obj);

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Manufacturer_Data_DIB & obj);

/* 7.6 Discovery */

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Search_Request_Frame & obj);

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Search_Response_Frame & obj);

/* 7.7 Self description */

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Description_Request_Frame & obj);
KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Description_Response_Frame & obj);

/* 7.8 Connection management */

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Connect_Request_Frame & obj);

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const Connect_Response_Frame & obj);

/* 8.6.2 Host Protocol Address Information */

KNX_EXPORT YAML::Emitter & operator<<(YAML::Emitter & out, const IP_Host_Protocol_Address_Information & obj);

}
