// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/IP_Communication_Channel.h>

#include <cassert>
#include <iostream>

#include <boost/current_function.hpp>

#include <KNX/03/08/01/Connection_Types.h>
#include <KNX/03/08/01/Timeout_Constants.h>
#include <KNX/03/08/02/Connect_Request_Frame.h>
#include <KNX/03/08/02/Connect_Response_Frame.h>
#include <KNX/03/08/02/Connectionstate_Request_Frame.h>
#include <KNX/03/08/02/Connectionstate_Response_Frame.h>
#include <KNX/03/08/02/Description_Request_Frame.h>
#include <KNX/03/08/02/Description_Response_Frame.h>
#include <KNX/03/08/02/Disconnect_Request_Frame.h>
#include <KNX/03/08/02/Disconnect_Response_Frame.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/02_Core.h>
#include <KNX/03/08/04/Tunnelling_Ack_Frame.h>
#include <KNX/03/08/04/Tunnelling_Connection_Request_Information.h>
#include <KNX/03/08/05/Routing_Busy_Frame.h>
#include <KNX/03/08/05/Routing_Indication_Frame.h>
#include <KNX/03/08/05/Routing_Lost_Message_Frame.h>
#include <KNX/03/08/05/Routing_System_Broadcast_Frame.h>
#include <KNX/03/08/07/Remote_Basic_Configuration_Request_Frame.h>
#include <KNX/03/08/07/Remote_Diagnostic_Request_Frame.h>
#include <KNX/03/08/07/Remote_Diagnostic_Response_Frame.h>
#include <KNX/03/08/07/Remote_Reset_Request_Frame.h>

namespace KNX {

IP_Communication_Channel::IP_Communication_Channel(asio::io_context & io_context, const IP_Host_Protocol_Address_Information & remote_control_endpoint) :
    remote_control_endpoint(remote_control_endpoint),
    m_io_context(io_context),
    m_control_socket(io_context),
    m_data_socket(io_context),
    connection_alive_timer(io_context),
    connect_request_timer(io_context),
    connectionstate_request_timer(io_context),
    device_configuration_request_timer(io_context),
    tunnelling_request_timer(io_context)
{
    /* open control socket */
    asio::error_code error;
    m_control_socket.open(asio::ip::udp::v4(), error);
    switch (error.value()) {
    case 0: // success (socket opened)
        break;
    default:
        std::cerr << __func__ << ": on control open: " << error.message().c_str() << std::endl;
        return;
    }

    /* prepare a buffer for up to 1024 octets */
    m_control_response_data.resize(1024);

    /* remote control endpoint */
    asio::ip::udp::endpoint asio_remote_control_endpoint;
    asio_remote_control_endpoint.address(asio::ip::address_v4(remote_control_endpoint.ip_address));
    asio_remote_control_endpoint.port(remote_control_endpoint.ip_port_number);

    /* start initiators */
    async_control_connect(asio_remote_control_endpoint);

    /* open data socket */
    m_data_socket.open(asio::ip::udp::v4(), error);
    switch (error.value()) {
    case 0: // success (socket opened)
        break;
    default:
        std::cerr << __func__ << ": on data open: " << error.message().c_str() << std::endl;
        return;
    }

    /* bind local endpoint */
    m_data_socket.bind(asio::ip::udp::endpoint(), error);
    switch (error.value()) {
    case 0: // success (socket bind)
        break;
    default:
        std::cerr << __func__ << ": on bind: " << error.message().c_str() << std::endl;
        break;
    }

    /* prepare a buffer for up to 1024 octets */
    m_data_response_data.resize(1024);
}

IP_Communication_Channel::~IP_Communication_Channel()
{
    /* shutdown control socket */
    asio::error_code error;
    m_control_socket.shutdown(asio::ip::udp::socket::shutdown_both, error);
    switch (error.value()) {
    case 0: // success (socket shutdown)
        break;
    case asio::error::not_connected:
        break;
    default:
        std::cerr << __func__ << ": on control shutdown: " << error.message().c_str() << std::endl;
        break;
    }

    /* shutdown data socket */
    m_data_socket.shutdown(asio::ip::udp::socket::shutdown_both, error);
    switch (error.value()) {
    case 0: // success (socket shutdown)
        break;
    case asio::error::not_connected:
        break;
    default:
        std::cerr << __func__ << ": on data shutdown: " << error.message().c_str() << std::endl;
        break;
    }

    /* close control socket */
    m_control_socket.close(error);
    switch (error.value()) {
    case 0: // success (socket closed)
        break;
    default:
        std::cerr << __func__ << ": on control close: " << error.message().c_str() << std::endl;
        break;
    }

    /* close data socket */
    m_data_socket.close(error);
    switch (error.value()) {
    case 0: // success (socket closed)
        break;
    default:
        std::cerr << __func__ << ": on data close: " << error.message().c_str() << std::endl;
        break;
    }
}

/* --- Core --- */

/* description */

void IP_Communication_Channel::description_req()
{
    /* define request */
    Description_Request_Frame description_request_frame;
    description_request_frame.control_endpoint = std::make_shared<IP_Host_Protocol_Address_Information>(local_control_endpoint());

    /* start initiators */
    async_send_description_request(description_request_frame.toData());
}

void IP_Communication_Channel::description_res(const std::shared_ptr<Device_Information_DIB> device_hardware, const std::shared_ptr<Supported_Service_Families_DIB> supported_service_families, const std::vector<std::shared_ptr<Description_Information_Block>> other_device_information)
{
    /* define request */
    Description_Response_Frame description_response_frame;
    description_response_frame.device_hardware = device_hardware;
    description_response_frame.supported_service_families = supported_service_families;
    description_response_frame.other_device_information = other_device_information;

    /* start initiators */
    async_send_description_response(description_response_frame.toData());
}

/* connect */

void IP_Communication_Channel::connect_req(const std::shared_ptr<Connection_Request_Information> connection_request_information)
{
    /* remember connection request information */
    m_connection_request_information = connection_request_information;

    /* define request */
    Connect_Request_Frame connect_request_frame;
    connect_request_frame.control_endpoint = std::make_shared<IP_Host_Protocol_Address_Information>(local_control_endpoint());
    connect_request_frame.data_endpoint = std::make_shared<IP_Host_Protocol_Address_Information>(local_data_endpoint());
    connect_request_frame.connection_request_information = m_connection_request_information;

    /* start initiators */
    async_send_connect_request(connect_request_frame.toData());
    connect_request_timer_restart();
}

void IP_Communication_Channel::connect_res(const Communication_Channel_Id communication_channel_id, const Error_Code status, const std::shared_ptr<Host_Protocol_Address_Information> data_endpoint, const std::shared_ptr<Connection_Response_Data_Block> connection_response_data_block)
{
    /* define request */
    Connect_Response_Frame connect_response_frame;
    connect_response_frame.communication_channel_id = communication_channel_id;
    connect_response_frame.status = status; // E_NO_ERROR, E_CONNECTION_TYPE, E_CONNECTION_OPTION, E_NO_MORE_CONNECTIONS
    connect_response_frame.data_endpoint = data_endpoint;
    connect_response_frame.connection_response_data_block = connection_response_data_block;

    /* start initiators */
    async_send_connect_response(connect_response_frame.toData());
}

/* connectionstate */

void IP_Communication_Channel::connectionstate_req()
{
    /* define request */
    Connectionstate_Request_Frame connectionstate_request_frame;
    connectionstate_request_frame.communication_channel_id = communication_channel_id;
    connectionstate_request_frame.control_endpoint = std::make_shared<IP_Host_Protocol_Address_Information>(local_control_endpoint());

    /* start initiators */
    async_send_connectionstate_request(connectionstate_request_frame.toData());
    connectionstate_request_timer_restart();
}

void IP_Communication_Channel::connectionstate_res(const Error_Code status)
{
    /* define request */
    Connectionstate_Response_Frame connectionstate_response_frame;
    connectionstate_response_frame.communication_channel_id = communication_channel_id;
    connectionstate_response_frame.status = status; // E_NO_ERROR, E_CONNECTION_ID, E_DATA_CONNECTION, E_KNX_CONNECTION

    /* start initiators */
    async_send_connectionstate_response(connectionstate_response_frame.toData());
}

/* disconnect */

void IP_Communication_Channel::disconnect_req()
{
    /* define request */
    Disconnect_Request_Frame disconnect_request_frame;
    disconnect_request_frame.communication_channel_id = communication_channel_id;
    disconnect_request_frame.control_endpoint = std::make_shared<IP_Host_Protocol_Address_Information>(local_control_endpoint());

    /* start initiators */
    async_send_disconnect_request(disconnect_request_frame.toData());
}

void IP_Communication_Channel::disconnect_res(const Error_Code status)
{
    /* define request */
    Disconnect_Response_Frame disconnect_response_frame;
    disconnect_response_frame.communication_channel_id = communication_channel_id;
    disconnect_response_frame.status = status;

    /* start initiators */
    async_send_disconnect_response(disconnect_response_frame.toData());
}

/* --- Device Management --- */

/* device configuration request */

void IP_Communication_Channel::device_configuration_req(const std::vector<uint8_t> & cemi_frame_data)
{
    /* define request */
    std::shared_ptr<Device_Configuration_Request_Frame> device_configuration_request_frame = std::make_shared<Device_Configuration_Request_Frame>();
    device_configuration_request_frame->communication_channel_id = communication_channel_id;
    device_configuration_request_frame->sequence_counter = tx.sequence_counter++;
    device_configuration_request_frame->cemi_frame_data = cemi_frame_data;

    /* add to transmit queue */
    tx.queue.push_back(device_configuration_request_frame);

    /* start initiators */
    if (state == Connection_State::Open_Idle) {
        state = Connection_State::Open_Wait;
        tx.rep_count = 1;

        async_send_device_configuration_request();
        device_configuration_request_timer_restart();
        connection_alive_timer_restart();
    }
}

/* --- Tunnelling --- */

/* tunnelling request */

void IP_Communication_Channel::tunnelling_req(const std::vector<uint8_t> cemi_frame_data)
{
    /* define request */
    std::shared_ptr<Tunnelling_Request_Frame> tunnelling_request_frame = std::make_shared<Tunnelling_Request_Frame>();
    tunnelling_request_frame->communication_channel_id = communication_channel_id;
    tunnelling_request_frame->sequence_counter = tx.sequence_counter++;
    tunnelling_request_frame->cemi_frame_data = cemi_frame_data;

    /* add to transmit queue */
    tx.queue.push_back(tunnelling_request_frame);

    /* start initiators */
    if (state == Connection_State::Open_Idle) {
        state = Connection_State::Open_Wait;
        tx.rep_count = 1;

        async_send_tunnelling_request();
        tunnelling_request_timer_restart();
        connection_alive_timer_restart();
    }
}

/* tunnelling feature get */

void IP_Communication_Channel::tunnelling_feature_get_req(const Tunnelling_Feature_Id feature_identifier)
{
    /* define request */
    std::shared_ptr<Tunnelling_Feature_Get_Frame> tunnelling_feature_get_frame = std::make_shared<Tunnelling_Feature_Get_Frame>();
    tunnelling_feature_get_frame->communication_channel_id = communication_channel_id;
    tunnelling_feature_get_frame->sequence_counter = tx.sequence_counter++;
    tunnelling_feature_get_frame->feature_identifier = feature_identifier;

    /* add to transmit queue */
    tx.queue.push_back(tunnelling_feature_get_frame);

    /* start initiators */
    if (state == Connection_State::Open_Idle) {
        state = Connection_State::Open_Wait;
        tx.rep_count = 1;

        async_send_tunnelling_feature_get();
        connection_alive_timer_restart();
    }
}

/* tunnelling feature response */

void IP_Communication_Channel::tunnelling_feature_res(const Tunnelling_Feature_Id feature_identifier, const Error_Code return_code, const std::vector<uint8_t> feature_value)
{
    /* define request */
    std::shared_ptr<Tunnelling_Feature_Response_Frame> tunnelling_feature_response_frame = std::make_shared<Tunnelling_Feature_Response_Frame>();
    tunnelling_feature_response_frame->communication_channel_id = communication_channel_id;
    tunnelling_feature_response_frame->sequence_counter = tx.sequence_counter++;
    tunnelling_feature_response_frame->feature_identifier = feature_identifier;
    tunnelling_feature_response_frame->return_code = return_code;
    tunnelling_feature_response_frame->feature_value = feature_value;

    /* add to transmit queue */
    tx.queue.push_back(tunnelling_feature_response_frame);

    /* start initiators */
    if (state == Connection_State::Open_Idle) {
        state = Connection_State::Open_Wait;
        tx.rep_count = 1;

        async_send_tunnelling_feature_response();
        connection_alive_timer_restart();
    }
}

/* tunnelling feature set */

void IP_Communication_Channel::tunnelling_feature_set_req(const Tunnelling_Feature_Id feature_identifier, const std::vector<uint8_t> feature_value)
{
    /* define request */
    std::shared_ptr<Tunnelling_Feature_Set_Frame> tunnelling_feature_set_frame = std::make_shared<Tunnelling_Feature_Set_Frame>();
    tunnelling_feature_set_frame->communication_channel_id = communication_channel_id;
    tunnelling_feature_set_frame->sequence_counter = tx.sequence_counter++;
    tunnelling_feature_set_frame->feature_identifier = feature_identifier;
    tunnelling_feature_set_frame->feature_value = feature_value;

    /* add to transmit queue */
    tx.queue.push_back(tunnelling_feature_set_frame);

    /* start initiators */
    if (state == Connection_State::Open_Idle) {
        state = Connection_State::Open_Wait;
        tx.rep_count = 1;

        async_send_tunnelling_feature_set();
        connection_alive_timer_restart();
    }
}

/* tunnelling feature info */

void IP_Communication_Channel::tunnelling_feature_info_req(const Tunnelling_Feature_Id feature_identifier, const std::vector<uint8_t> feature_value)
{
    /* define request */
    std::shared_ptr<Tunnelling_Feature_Info_Frame> tunnelling_feature_info_frame = std::make_shared<Tunnelling_Feature_Info_Frame>();
    tunnelling_feature_info_frame->communication_channel_id = communication_channel_id;
    tunnelling_feature_info_frame->sequence_counter = tx.sequence_counter++;
    tunnelling_feature_info_frame->feature_identifier = feature_identifier;
    tunnelling_feature_info_frame->feature_value = feature_value;

    /* add to transmit queue */
    tx.queue.push_back(tunnelling_feature_info_frame);

    /* start initiators */
    if (state == Connection_State::Open_Idle) {
        state = Connection_State::Open_Wait;
        tx.rep_count = 1;

        async_send_tunnelling_feature_info();
        connection_alive_timer_restart();
    }
}

/* --- Routing --- */

/* routing indication */

void IP_Communication_Channel::routing_indication_req(const std::vector<uint8_t> cemi_frame_data)
{
    /* define request */
    Routing_Indication_Frame routing_indication_frame;
    routing_indication_frame.cemi_frame_data = cemi_frame_data;

    /* start initiators */
    async_send_routing_indication(routing_indication_frame.toData());
}

/* routing lost */

void IP_Communication_Channel::routing_lost_req(const uint8_t device_state, const uint16_t number_of_lost_messages)
{
    /* define request */
    Routing_Lost_Message_Frame routing_lost_message_frame;
    routing_lost_message_frame.device_state = device_state;
    routing_lost_message_frame.number_of_lost_messages = number_of_lost_messages;

    /* start initiators */
    async_send_routing_lost(routing_lost_message_frame.toData());
}

/* routing busy */

void IP_Communication_Channel::routing_busy_req(const uint8_t device_state, const uint16_t routing_busy_wait_time, const uint16_t routing_busy_control_field)
{
    /* define request */
    Routing_Busy_Frame routing_busy_frame;
    routing_busy_frame.device_state = device_state;
    routing_busy_frame.routing_busy_wait_time = routing_busy_wait_time;
    routing_busy_frame.routing_busy_control_field = routing_busy_control_field;

    /* start initiators */
    async_send_routing_busy(routing_busy_frame.toData());
}

/* routing system broadcast */

void IP_Communication_Channel::routing_system_broadcast_req()
{
    /* define request */
    Routing_System_Broadcast_Frame routing_system_broadcast_frame;
    // @todo routing_system_broadcast

    /* start initiators */
    async_send_routing_system_broadcast(routing_system_broadcast_frame.toData());
}

/* --- Remote Logging --- */

/* --- Remote Configuration and Diagnosis --- */

/* remote diagnostic request */

void IP_Communication_Channel::remote_diagnostic_req(const std::shared_ptr<Host_Protocol_Address_Information> discovery_endpoint, const std::shared_ptr<Selector> selector)
{
    /* define request */
    Remote_Diagnostic_Request_Frame remote_diagnostic_request_frame;
    remote_diagnostic_request_frame.discovery_endpoint = discovery_endpoint;
    remote_diagnostic_request_frame.selector = selector;

    /* start initiators */
    async_send_remote_diagnostic_request(remote_diagnostic_request_frame.toData());
}

void IP_Communication_Channel::remote_diagnostic_res(const std::shared_ptr<Selector> selector, const std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks)
{
    /* define request */
    Remote_Diagnostic_Response_Frame remote_diagnostic_response_frame;
    remote_diagnostic_response_frame.selector = selector;
    remote_diagnostic_response_frame.description_information_blocks = description_information_blocks;

    /* start initiators */
    async_send_remote_diagnostic_response(remote_diagnostic_response_frame.toData());
}

/* remote basic configuration request */

void IP_Communication_Channel::remote_basic_configuration_req(const std::shared_ptr<Host_Protocol_Address_Information> discovery_endpoint, const std::shared_ptr<Selector> selector, const std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks)
{
    /* define request */
    Remote_Basic_Configuration_Request_Frame remote_basic_configuration_request_frame;
    remote_basic_configuration_request_frame.discovery_endpoint = discovery_endpoint;
    remote_basic_configuration_request_frame.selector = selector;
    remote_basic_configuration_request_frame.description_information_blocks = description_information_blocks;

    /* start initiators */
    async_send_remote_basic_configuration_request(remote_basic_configuration_request_frame.toData());
}

/* remote reset request */

void IP_Communication_Channel::remote_reset_req(const std::shared_ptr<Selector> selector, const Reset_Command_Type_Code reset_command)
{
    /* define request */
    Remote_Reset_Request_Frame remote_reset_request_frame;
    remote_reset_request_frame.selector = selector;
    remote_reset_request_frame.reset_command = reset_command;

    /* start initiators */
    async_send_remote_reset_request(remote_reset_request_frame.toData());
}

/* --- Object Server --- */

/* --- Security --- */

/* protected */

IP_Host_Protocol_Address_Information IP_Communication_Channel::local_control_endpoint() const
{
    IP_Host_Protocol_Address_Information hpai;

    asio::error_code error;
    asio::ip::udp::endpoint asio_local_control_endpoint = m_control_socket.local_endpoint(error);
    switch (error.value()) {
    case 0: // success (local endpoint retrieved)
        break;
    default:
        std::cerr << __func__ << ": on local_endpoint: " << error.message().c_str() << std::endl;
        return hpai;
    }

    hpai.host_protocol_code = Host_Protocol_Code::IPV4_UDP;
    hpai.ip_address = asio_local_control_endpoint.address().to_v4().to_bytes();
    hpai.ip_port_number = asio_local_control_endpoint.port();
    return hpai;
}

IP_Host_Protocol_Address_Information IP_Communication_Channel::local_data_endpoint() const
{
    IP_Host_Protocol_Address_Information hpai;

    asio::error_code error;
    asio::ip::udp::endpoint asio_local_data_endpoint = m_data_socket.local_endpoint(error);
    switch (error.value()) {
    case 0: // success (local endpoint retrieved)
        break;
    default:
        std::cerr << __func__ << ": on local_endpoint: " << error.message().c_str() << std::endl;
        return hpai;
    }

    hpai.host_protocol_code = Host_Protocol_Code::IPV4_UDP;
    hpai.ip_address = asio_local_data_endpoint.address().to_v4().to_bytes();

    // bind can only reserve the port, but doesn't know which route / interface / local IP address to use
    // so take it from connected control endpoint
    if ((hpai.ip_address[0] == 0) && (hpai.ip_address[1] == 0) && (hpai.ip_address[2] == 0) && (hpai.ip_address[3] == 0)) {
        hpai.ip_address = local_control_endpoint().ip_address;
    }

    hpai.ip_port_number = asio_local_data_endpoint.port();
    return hpai;
}

void IP_Communication_Channel::async_control_connect(const asio::ip::udp::endpoint & remote_control_endpoint)
{
    m_control_socket.async_connect(
        remote_control_endpoint,
        std::bind(&IP_Communication_Channel::control_connected, this, std::placeholders::_1));
}

void IP_Communication_Channel::control_connected(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // success (socket connected)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    };

    /* start initiators */
    state = Connection_State::Open_Idle;
    async_receive_control_response();
}

void IP_Communication_Channel::async_data_connect()
{
    asio::ip::udp::endpoint asio_remote_data_endpoint;
    asio_remote_data_endpoint.address(asio::ip::address_v4(m_remote_data_endpoint.ip_address));
    asio_remote_data_endpoint.port(m_remote_data_endpoint.ip_port_number);

    m_data_socket.async_connect(
        asio_remote_data_endpoint,
        std::bind(&IP_Communication_Channel::data_connected, this, std::placeholders::_1));
}

void IP_Communication_Channel::data_connected(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // success (socket connected)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    };

    /* start initiators */
    async_receive_data_response();
}

/* connection alive timer */

void IP_Communication_Channel::connection_alive_timer_restart()
{
    connection_alive_timer.expires_after(CONNECTION_ALIVE_TIME); // cancel pending timers
    connection_alive_timer.async_wait(
        std::bind(&IP_Communication_Channel::connection_alive_timer_expired, this, std::placeholders::_1));
}

void IP_Communication_Channel::connection_alive_timer_stop()
{
    connection_alive_timer.cancel();
}

void IP_Communication_Channel::connection_alive_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // success (timer expired)
        break;
    case asio::error::operation_aborted: // timer cancelled
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    if (state != Connection_State::Closed) {
        // similar to Transport_Layer E16 -> A6
        disconnect_req();
        if (disconnect_ind) {
            disconnect_ind(std::make_shared<IP_Host_Protocol_Address_Information>(remote_control_endpoint));
        }
        tunnelling_request_timer_stop();
        connection_alive_timer_stop();
        state = Connection_State::Closed;
    }
}

/* response multiplexer */

void IP_Communication_Channel::async_receive_control_response()
{
    m_control_socket.async_receive(
        asio::buffer(m_control_response_data),
        std::bind(&IP_Communication_Channel::control_response_received, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::control_response_received(const std::error_code & error, std::size_t bytes_transferred)
{
    assert(bytes_transferred <= m_control_response_data.size());

    switch (error.value()) {
    case 0: // success (buffer received)
        break;
    case asio::error::operation_aborted:
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    /* determine frame type */
    Frame frame;
    frame.fromData(std::cbegin(m_control_response_data), std::cbegin(m_control_response_data) + bytes_transferred);
    if (frame.total_length != bytes_transferred) {
        std::cerr << __func__ << ": Unexpected Frame length " << frame.total_length << " != " << bytes_transferred << std::endl;
        return;
    }

    /* handle frame */
    switch (frame.service_type_identifier) {
    case Service_Type_Identifier::UNDEFINED:
        std::cerr << __func__ << ": Unexpected Service_Type_Identifier 0x" << std::hex << static_cast<uint16_t>(frame.service_type_identifier) << std::endl;
        break;
    case Service_Type_Identifier::SEARCH_REQUEST:
        std::cerr << __func__ << ": Unexpected SEARCH_REQUEST" << std::endl; // only on discovery channel
        break;
    case Service_Type_Identifier::SEARCH_RESPONSE:
        std::cerr << __func__ << ": Unexpected SEARCH_RESPONSE" << std::endl; // only on discovery channel
        break;
    case Service_Type_Identifier::DESCRIPTION_REQUEST: {
        Description_Request_Frame description_request_frame;
        description_request_frame.fromData(std::cbegin(m_control_response_data), std::cbegin(m_control_response_data) + bytes_transferred);

        if (description_ind) {
            description_ind(description_request_frame.control_endpoint);
        }
    }
    break;
    case Service_Type_Identifier::DESCRIPTION_RESPONSE: {
        Description_Response_Frame description_response_frame;
        description_response_frame.fromData(std::cbegin(m_control_response_data), std::cbegin(m_control_response_data) + bytes_transferred);

        if (description_Acon) {
            description_Acon(description_response_frame.device_hardware, description_response_frame.supported_service_families, description_response_frame.other_device_information);
        }
    }
    break;
    case Service_Type_Identifier::CONNECT_REQUEST: {
        Connect_Request_Frame connect_request_frame;
        connect_request_frame.fromData(std::cbegin(m_control_response_data), std::cbegin(m_control_response_data) + bytes_transferred);

        if (connect_ind) {
            connect_ind(connect_request_frame.control_endpoint, connect_request_frame.data_endpoint, connect_request_frame.connection_request_information);
        }
    }
    break;
    case Service_Type_Identifier::CONNECT_RESPONSE: {
        Connect_Response_Frame connect_response_frame;
        connect_response_frame.fromData(std::cbegin(m_control_response_data), std::cbegin(m_control_response_data) + bytes_transferred);

        switch (connect_response_frame.status) {
        case Error_Code::E_NO_ERROR: {
            connect_request_timer_stop();
            communication_channel_id = connect_response_frame.communication_channel_id;
            m_remote_data_endpoint = *std::dynamic_pointer_cast<IP_Host_Protocol_Address_Information>(connect_response_frame.data_endpoint);

            /* start initiators */
            async_data_connect();
        }
        break;
        case Error_Code::E_CONNECTION_TYPE:
            break;
        case Error_Code::E_CONNECTION_OPTION:
            break;
        case Error_Code::E_NO_MORE_CONNECTIONS:
            break;
        default:
            break;
        }

        if (connect_Acon) {
            connect_Acon(connect_response_frame.status, connect_response_frame.data_endpoint, connect_response_frame.connection_response_data_block);
        }
    }
    break;
    case Service_Type_Identifier::CONNECTIONSTATE_REQUEST: {
        Connectionstate_Request_Frame connectionstate_request_frame;
        connectionstate_request_frame.fromData(std::cbegin(m_control_response_data), std::cbegin(m_control_response_data) + bytes_transferred);

        if (connectionstate_request_frame.communication_channel_id != communication_channel_id) {
            connect_res(connectionstate_request_frame.communication_channel_id, Error_Code::E_CONNECTION_ID, std::make_shared<IP_Host_Protocol_Address_Information>(local_data_endpoint()), std::make_shared<Connection_Response_Data_Block>());
        }
        // E_DATA_CONNECTION
        // E_KNX_CONNECTION
        connect_res(connectionstate_request_frame.communication_channel_id, Error_Code::E_NO_ERROR, std::make_shared<IP_Host_Protocol_Address_Information>(local_data_endpoint()), std::make_shared<Connection_Response_Data_Block>()); // @todo Tunnelling_Connection_Response_Data_Block

        if (connectionstate_ind) {
            connectionstate_ind(connectionstate_request_frame.control_endpoint);
        }
    }
    break;
    case Service_Type_Identifier::CONNECTIONSTATE_RESPONSE: {
        Connectionstate_Response_Frame connectionstate_response_frame;
        connectionstate_response_frame.fromData(std::cbegin(m_control_response_data), std::cbegin(m_control_response_data) + bytes_transferred);

        switch (connectionstate_response_frame.status) {
        case Error_Code::E_NO_ERROR:
            connectionstate_request_timer_stop();
            break;
        case Error_Code::E_CONNECTION_ID:
            break;
        case Error_Code::E_DATA_CONNECTION:
            break;
        case Error_Code::E_KNX_CONNECTION:
            break;
        default:
            break;
        }

        if (connectionstate_Acon) {
            connectionstate_Acon(connectionstate_response_frame.status);
        }
    }
    break;
    case Service_Type_Identifier::DISCONNECT_REQUEST: {
        Disconnect_Request_Frame disconnect_request_frame;
        disconnect_request_frame.fromData(std::cbegin(m_control_response_data), std::cbegin(m_control_response_data) + bytes_transferred);

        if (disconnect_request_frame.communication_channel_id != communication_channel_id) {
            disconnect_res(Error_Code::E_CONNECTION_ID);
        }

        // @todo terminate the control and data channels

        if (disconnect_ind) {
            disconnect_ind(disconnect_request_frame.control_endpoint);
        }
    }
    break;
    case Service_Type_Identifier::DISCONNECT_RESPONSE: {
        Disconnect_Response_Frame disconnect_response_frame;
        disconnect_response_frame.fromData(std::cbegin(m_control_response_data), std::cbegin(m_control_response_data) + bytes_transferred);

        switch (disconnect_response_frame.status) {
        case Error_Code::E_NO_ERROR:
            break;
        default:
            break;
        }

        // @todo terminate the control and data channels

        if (disconnect_Acon) {
            disconnect_Acon(disconnect_response_frame.status);
        }
    }
    break;
    case Service_Type_Identifier::SEARCH_REQUEST_EXT:
        std::cerr << __func__ << ": Unexpected SEARCH_REQUEST_EXT" << std::endl; // only on discovery channel
        break;
    case Service_Type_Identifier::SEARCH_RESPONSE_EXT:
        std::cerr << __func__ << ": Unexpected SEARCH_RESPONSE_EXT" << std::endl; // only on discovery channel
        break;
    case Service_Type_Identifier::DEVICE_CONFIGURATION_REQUEST:
        std::cerr << __func__ << ": Unexpected DEVICE_CONFIGURATION_REQUEST" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::DEVICE_CONFIGURATION_ACK:
        std::cerr << __func__ << ": Unexpected DEVICE_CONFIGURATION_ACK" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::TUNNELING_REQUEST:
        std::cerr << __func__ << ": Unexpected TUNNELING_REQUEST" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::TUNNELING_ACK:
        std::cerr << __func__ << ": Unexpected TUNNELING_ACK" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::TUNNELING_FEATURE_GET:
        std::cerr << __func__ << ": Unexpected TUNNELING_FEATURE_GET" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::TUNNELING_FEATURE_RESPONSE:
        std::cerr << __func__ << ": Unexpected TUNNELING_FEATURE_RESPONSE" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::TUNNELING_FEATURE_SET:
        std::cerr << __func__ << ": Unexpected TUNNELING_FEATURE_SET" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::TUNNELING_FEATURE_INFO:
        std::cerr << __func__ << ": Unexpected TUNNELING_FEATURE_INFO" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::ROUTING_INDICATION:
        std::cerr << __func__ << ": Unexpected ROUTING_INDICATION" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::ROUTING_LOST_MESSAGE:
        std::cerr << __func__ << ": Unexpected ROUTING_LOST_MESSAGE" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::ROUTING_BUSY:
        std::cerr << __func__ << ": Unexpected ROUTING_BUSY" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::ROUTING_SYSTEM_BROADCAST:
        std::cerr << __func__ << ": Unexpected ROUTING_SYSTEM_BROADCAST" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::REMOTE_DIAGNOSTIC_REQUEST:
        std::cerr << __func__ << ": Unexpected REMOTE_DIAGNOSTIC_REQUEST" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::REMOTE_DIAGNOSTIC_RESPONSE:
        std::cerr << __func__ << ": Unexpected REMOTE_DIAGNOSTIC_RESPONSE" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::REMOTE_BASIC_CONFIGURATION_REQUEST:
        std::cerr << __func__ << ": Unexpected REMOTE_BASIC_CONFIGURATION_REQUEST" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::REMOTE_RESET_REQUEST:
        std::cerr << __func__ << ": Unexpected REMOTE_RESET_REQUEST" << std::endl; // only on data channel
        break;
    case Service_Type_Identifier::SECURE_WRAPPER:
        std::cerr << __func__ << ": Unimplemented SECURE_WRAPPER" << std::endl; // on control or data channel?
        // @todo SECURE_WRAPPER
        break;
    case Service_Type_Identifier::SECURE_CHANNEL_REQUEST:
        std::cerr << __func__ << ": Unimplemented SECURE_CHANNEL_REQUEST" << std::endl; // on control or data channel?
        // @todo SECURE_CHANNEL_REQUEST
        break;
    case Service_Type_Identifier::SECURE_CHANNEL_RESPONSE:
        std::cerr << __func__ << ": Unimplemented SECURE_CHANNEL_RESPONSE" << std::endl; // on control or data channel?
        // @todo SECURE_CHANNEL_RESPONSE
        break;
    case Service_Type_Identifier::SECURE_CHANNEL_AUTHORIZE:
        std::cerr << __func__ << ": Unimplemented SECURE_CHANNEL_AUTHORIZE" << std::endl; // on control or data channel?
        // @todo SECURE_CHANNEL_AUTHORIZE
        break;
    case Service_Type_Identifier::SECURE_CHANNEL_STATUS:
        std::cerr << __func__ << ": Unimplemented SECURE_CHANNEL_STATUS" << std::endl; // on control or data channel?
        // @todo SECURE_CHANNEL_STATUS
        break;
    case Service_Type_Identifier::SECURE_GROUP_SYNC_REQUEST:
        std::cerr << __func__ << ": Unimplemented SECURE_GROUP_SYNC_REQUEST" << std::endl; // on control or data channel?
        // @todo SECURE_GROUP_SYNC_REQUEST
        break;
    case Service_Type_Identifier::SECURE_GROUP_SYNC_RESPONSE:
        std::cerr << __func__ << ": Unimplemented SECURE_GROUP_SYNC_RESPONSE" << std::endl; // on control or data channel?
        // @todo SECURE_GROUP_SYNC_RESPONSE
        break;
    default:
        // ignore all illegal messages and not send any answers
        break;
    }

    /* start initiators */
    async_receive_control_response();
}

void IP_Communication_Channel::async_receive_data_response()
{
    m_data_socket.async_receive(
        asio::buffer(m_data_response_data),
        std::bind(&IP_Communication_Channel::data_response_received, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::data_response_received(const std::error_code & error, std::size_t bytes_transferred)
{
    assert(bytes_transferred <= m_data_response_data.size());

    switch (error.value()) {
    case 0: // success (buffer received)
        break;
    case asio::error::operation_aborted:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    /* determine frame type */
    Frame frame;
    frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);
    if (frame.total_length != bytes_transferred) {
        std::cerr << __func__ << ": Unexpected Frame length " << frame.total_length << " != " << bytes_transferred << std::endl;
        return;
    }

    /* handle frame */
    switch (frame.service_type_identifier) {
    case Service_Type_Identifier::UNDEFINED:
        std::cerr << __func__ << ": Unexpected Service_Type_Identifier 0x" << std::hex << static_cast<uint16_t>(frame.service_type_identifier) << std::endl;
        break;
    case Service_Type_Identifier::SEARCH_REQUEST:
        std::cerr << __func__ << ": Unexpected SEARCH_REQUEST" << std::endl; // only on discovery channel
        break;
    case Service_Type_Identifier::SEARCH_RESPONSE:
        std::cerr << __func__ << ": Unexpected SEARCH_RESPONSE" << std::endl; // only on discovery channel
        break;
    case Service_Type_Identifier::DESCRIPTION_REQUEST:
        std::cerr << __func__ << ": Unexpected DESCRIPTION_REQUEST" << std::endl; // only on control channel
        break;
    case Service_Type_Identifier::DESCRIPTION_RESPONSE:
        std::cerr << __func__ << ": Unexpected DESCRIPTION_RESPONSE" << std::endl; // only on control channel
        break;
    case Service_Type_Identifier::CONNECT_REQUEST:
        std::cerr << __func__ << ": Unexpected CONNECT_REQUEST" << std::endl; // only on control channel
        break;
    case Service_Type_Identifier::CONNECT_RESPONSE:
        std::cerr << __func__ << ": Unexpected CONNECT_RESPONSE" << std::endl; // only on control channel
        break;
    case Service_Type_Identifier::CONNECTIONSTATE_REQUEST:
        std::cerr << __func__ << ": Unexpected CONNECTIONSTATE_REQUEST" << std::endl; // only on control channel
        break;
    case Service_Type_Identifier::CONNECTIONSTATE_RESPONSE:
        std::cerr << __func__ << ": Unexpected CONNECTIONSTATE_RESPONSE" << std::endl; // only on control channel
        break;
    case Service_Type_Identifier::DISCONNECT_REQUEST:
        std::cerr << __func__ << ": Unexpected DISCONNECT_REQUEST" << std::endl; // only on control channel
        break;
    case Service_Type_Identifier::DISCONNECT_RESPONSE:
        std::cerr << __func__ << ": Unexpected DISCONNECT_RESPONSE" << std::endl; // only on control channel
        break;
    case Service_Type_Identifier::SEARCH_REQUEST_EXT:
        std::cerr << __func__ << ": Unexpected SEARCH_REQUEST_EXT" << std::endl; // only on discovery channel
        break;
    case Service_Type_Identifier::SEARCH_RESPONSE_EXT:
        std::cerr << __func__ << ": Unexpected SEARCH_RESPONSE_EXT" << std::endl; // only on discovery channel
        break;
    case Service_Type_Identifier::DEVICE_CONFIGURATION_REQUEST: {
        Device_Configuration_Request_Frame device_configuration_request_frame;
        device_configuration_request_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (device_configuration_request_frame.communication_channel_id != communication_channel_id) {
            device_configuration_ack(Error_Code::E_CONNECTION_ID, device_configuration_request_frame.sequence_counter);
        }
        if (device_configuration_request_frame.sequence_counter != rx.sequence_counter) {
            device_configuration_ack(Error_Code::E_SEQUENCE_NUMBER, device_configuration_request_frame.sequence_counter);
        }

        if (device_configuration_ind) {
            device_configuration_ind(device_configuration_request_frame.cemi_frame_data);
        }
    }
    break;
    case Service_Type_Identifier::DEVICE_CONFIGURATION_ACK: {
        assert(state == Connection_State::Open_Wait);

        Device_Configuration_Ack_Frame device_configuration_ack_frame;
        device_configuration_ack_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (device_configuration_ack_frame.sequence_counter == tx.queue.front()->sequence_counter) {
            const Error_Code error_code = static_cast<Error_Code>(device_configuration_ack_frame.status);
            if (error_code == Error_Code::E_NO_ERROR) {
                tx.queue.pop_front();
                state = Connection_State::Open_Idle;
                device_configuration_request_timer_stop();
                if (device_configuration_con) {
                    device_configuration_con(Status::ok);
                }
            } else {
                assert(false);
            }
        } else {
            assert(false);
        }
        // @todo handle errors: state, communication_channel, sequence_counter, status
    }
    break;
    case Service_Type_Identifier::TUNNELING_REQUEST: {
        Tunnelling_Request_Frame tunnelling_request_frame;
        tunnelling_request_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (tunnelling_request_frame.communication_channel_id == communication_channel_id) {
            if (tunnelling_request_frame.sequence_counter == rx.sequence_counter) {
                if (state != Connection_State::Closed) {
                    // similar to Transport_Layer E04 -> A2
                    tunnelling_ack_req(Error_Code::E_NO_ERROR, tunnelling_request_frame.sequence_counter);
                    rx.sequence_counter++;
                    if (tunnelling_ind) {
                        tunnelling_ind(tunnelling_request_frame.cemi_frame_data);
                    }
                    connection_alive_timer_restart();
                }
            } else { // tunnelling_request_frame.sequence_counter != rx.sequence_counter
                if (tunnelling_request_frame.sequence_counter == (rx.sequence_counter - 1)) {
                    if (state != Connection_State::Closed) {
                        // similar to Transport_Layer E05 -> A3
                        tunnelling_ack_req(Error_Code::E_NO_ERROR, tunnelling_request_frame.sequence_counter);
                        connection_alive_timer_restart();
                    }
                } else { // tunnelling_request_frame.sequence_counter != (rx.sequence_counter - 1)
                    if (state != Connection_State::Closed) {
                        // similar to Transport_Layer E06 -> A4
                        tunnelling_ack_req(Error_Code::E_SEQUENCE_NUMBER, tunnelling_request_frame.sequence_counter);
                        connection_alive_timer_restart();
                    }
                }
            }
        } else { // tunnelling_request_frame.communication_channel_id != communication_channel_id
            // similar to Transport_Layer E07 -> A10
            tunnelling_ack_req(Error_Code::E_CONNECTION_ID, tunnelling_request_frame.sequence_counter);
            //disconnect_req();
        }
    }
    break;
    case Service_Type_Identifier::TUNNELING_ACK: {
        Tunnelling_Ack_Frame tunnelling_ack_frame;
        tunnelling_ack_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        assert(state == Connection_State::Open_Wait);

        if (tunnelling_ack_frame.sequence_counter == tx.queue.front()->sequence_counter) {
            const Error_Code error_code = static_cast<Error_Code>(tunnelling_ack_frame.status);
            if (error_code == Error_Code::E_NO_ERROR) {
                const Service_Type_Identifier service_type_identifier = tx.queue.front()->service_type_identifier;
                tx.queue.pop_front();
                state = Connection_State::Open_Idle;
                switch (service_type_identifier) {
                case Service_Type_Identifier::TUNNELING_REQUEST:
                    tunnelling_request_timer_stop();
                    if (tunnelling_con) {
                        tunnelling_con(Status::ok);
                    }
                    break;
                case Service_Type_Identifier::TUNNELING_FEATURE_GET:
                    if (tunnelling_feature_get_con) {
                        tunnelling_feature_get_con(Status::ok);
                    }
                    break;
                case Service_Type_Identifier::TUNNELING_FEATURE_RESPONSE:
                    if (tunnelling_feature_response_con) {
                        tunnelling_feature_response_con(Status::ok);
                    }
                    break;
                case Service_Type_Identifier::TUNNELING_FEATURE_SET:
                    if (tunnelling_feature_set_con) {
                        tunnelling_feature_set_con(Status::ok);
                    }
                    break;
                case Service_Type_Identifier::TUNNELING_FEATURE_INFO:
                    if (tunnelling_feature_info_con) {
                        tunnelling_feature_info_con(Status::ok);
                    }
                    break;
                default:
                    assert(false);
                    break;
                }
            } else {
                assert(false);
            }
        } else {
            assert(false);
        }
        // @todo handle errors: state, communication_channel, sequence_counter, status
    }
    break;
    case Service_Type_Identifier::TUNNELING_FEATURE_GET: {
        Tunnelling_Feature_Get_Frame tunnelling_feature_get_frame;
        tunnelling_feature_get_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (tunnelling_feature_get_frame.communication_channel_id != communication_channel_id) {
            // @todo Error_Code::E_CONNECTION_ID
        }
        if (tunnelling_feature_get_frame.sequence_counter != rx.sequence_counter) {
            // @todo Error_Code::E_SEQUENCE_NUMBER
        }

        if (tunnelling_feature_get_ind) {
            tunnelling_feature_get_ind(tunnelling_feature_get_frame.feature_identifier);
        }
    }
    break;
    case Service_Type_Identifier::TUNNELING_FEATURE_RESPONSE: {
        Tunnelling_Feature_Response_Frame tunnelling_feature_response_frame;
        tunnelling_feature_response_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (tunnelling_feature_response_frame.communication_channel_id != communication_channel_id) {
            // @todo Error_Code::E_CONNECTION_ID
        }
        if (tunnelling_feature_response_frame.sequence_counter != rx.sequence_counter) {
            // @todo Error_Code::E_SEQUENCE_NUMBER
        }

        if (tunnelling_feature_response_ind) {
            tunnelling_feature_response_ind(tunnelling_feature_response_frame.feature_identifier, tunnelling_feature_response_frame.return_code, tunnelling_feature_response_frame.feature_value);
        }
    }
    break;
    case Service_Type_Identifier::TUNNELING_FEATURE_SET: {
        Tunnelling_Feature_Set_Frame tunnelling_feature_set_frame;
        tunnelling_feature_set_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (tunnelling_feature_set_frame.communication_channel_id != communication_channel_id) {
            // @todo Error_Code::E_CONNECTION_ID
        }
        if (tunnelling_feature_set_frame.sequence_counter != rx.sequence_counter) {
            // @todo Error_Code::E_SEQUENCE_NUMBER
        }

        if (tunnelling_feature_set_ind) {
            tunnelling_feature_set_ind(tunnelling_feature_set_frame.feature_identifier, tunnelling_feature_set_frame.feature_value);
        }
    }
    break;
    case Service_Type_Identifier::TUNNELING_FEATURE_INFO: {
        Tunnelling_Feature_Info_Frame tunnelling_feature_info_frame;
        tunnelling_feature_info_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (tunnelling_feature_info_frame.communication_channel_id != communication_channel_id) {
            // @todo Error_Code::E_CONNECTION_ID
        }
        if (tunnelling_feature_info_frame.sequence_counter != rx.sequence_counter) {
            // @todo Error_Code::E_SEQUENCE_NUMBER
        }

        if (tunnelling_feature_info_ind) {
            tunnelling_feature_info_ind(tunnelling_feature_info_frame.feature_identifier, tunnelling_feature_info_frame.feature_value);
        }
    }
    break;
    case Service_Type_Identifier::ROUTING_INDICATION: {
        Routing_Indication_Frame routing_indication_frame;
        routing_indication_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (routing_indication_ind) {
            routing_indication_ind(routing_indication_frame.cemi_frame_data);
        }
    }
    break;
    case Service_Type_Identifier::ROUTING_LOST_MESSAGE: {
        Routing_Lost_Message_Frame routing_lost_message_frame;
        routing_lost_message_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (routing_lost_ind) {
            routing_lost_ind(routing_lost_message_frame.device_state, routing_lost_message_frame.number_of_lost_messages);
        }
    }
    break;
    case Service_Type_Identifier::ROUTING_BUSY: {
        Routing_Busy_Frame routing_busy_frame;
        routing_busy_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (routing_busy_ind) {
            routing_busy_ind(routing_busy_frame.device_state, routing_busy_frame.routing_busy_wait_time, routing_busy_frame.routing_busy_control_field);
        }
    }
    break;
    case Service_Type_Identifier::ROUTING_SYSTEM_BROADCAST: {
        Routing_System_Broadcast_Frame routing_system_broadcast_frame;
        routing_system_broadcast_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (routing_system_broadcast_ind) {
            routing_system_broadcast_ind();
        }
    }
    break;
    case Service_Type_Identifier::REMOTE_DIAGNOSTIC_REQUEST: {
        Remote_Diagnostic_Request_Frame remote_diagnostic_request_frame;
        remote_diagnostic_request_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (remote_diagnostic_ind) {
            remote_diagnostic_ind(remote_diagnostic_request_frame.discovery_endpoint, remote_diagnostic_request_frame.selector);
        }
    }
    break;
    case Service_Type_Identifier::REMOTE_DIAGNOSTIC_RESPONSE: {
        Remote_Diagnostic_Response_Frame remote_diagnostic_response_frame;
        remote_diagnostic_response_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (remote_diagnostic_Acon) {
            remote_diagnostic_Acon(remote_diagnostic_response_frame.selector, remote_diagnostic_response_frame.description_information_blocks);
        }
    }
    break;
    case Service_Type_Identifier::REMOTE_BASIC_CONFIGURATION_REQUEST: {
        Remote_Basic_Configuration_Request_Frame remote_basic_configuration_request_frame;
        remote_basic_configuration_request_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (remote_basic_configuration_ind) {
            remote_basic_configuration_ind(remote_basic_configuration_request_frame.discovery_endpoint, remote_basic_configuration_request_frame.selector, remote_basic_configuration_request_frame.description_information_blocks);
        }
    }
    break;
    case Service_Type_Identifier::REMOTE_RESET_REQUEST: {
        Remote_Reset_Request_Frame remote_reset_request_frame;
        remote_reset_request_frame.fromData(std::cbegin(m_data_response_data), std::cbegin(m_data_response_data) + bytes_transferred);

        if (remote_reset_ind) {
            remote_reset_ind(remote_reset_request_frame.selector, remote_reset_request_frame.reset_command);
        }
    }
    break;
    case Service_Type_Identifier::SECURE_WRAPPER:
        std::cerr << __func__ << ": Unimplemented SECURE_WRAPPER" << std::endl; // on control or data channel?
        // @todo SECURE_WRAPPER
        break;
    case Service_Type_Identifier::SECURE_CHANNEL_REQUEST:
        std::cerr << __func__ << ": Unimplemented SECURE_CHANNEL_REQUEST" << std::endl; // on control or data channel?
        // @todo SECURE_CHANNEL_REQUEST
        break;
    case Service_Type_Identifier::SECURE_CHANNEL_RESPONSE:
        std::cerr << __func__ << ": Unimplemented SECURE_CHANNEL_RESPONSE" << std::endl; // on control or data channel?
        // @todo SECURE_CHANNEL_RESPONSE
        break;
    case Service_Type_Identifier::SECURE_CHANNEL_AUTHORIZE:
        std::cerr << __func__ << ": Unimplemented SECURE_CHANNEL_AUTHORIZE" << std::endl; // on control or data channel?
        // @todo SECURE_CHANNEL_AUTHORIZE
        break;
    case Service_Type_Identifier::SECURE_CHANNEL_STATUS:
        std::cerr << __func__ << ": Unimplemented SECURE_CHANNEL_STATUS" << std::endl; // on control or data channel?
        // @todo SECURE_CHANNEL_STATUS
        break;
    case Service_Type_Identifier::SECURE_GROUP_SYNC_REQUEST:
        std::cerr << __func__ << ": Unimplemented SECURE_GROUP_SYNC_REQUEST" << std::endl; // on control or data channel?
        // @todo SECURE_GROUP_SYNC_REQUEST
        break;
    case Service_Type_Identifier::SECURE_GROUP_SYNC_RESPONSE:
        std::cerr << __func__ << ": Unimplemented SECURE_GROUP_SYNC_RESPONSE" << std::endl; // on control or data channel?
        // @todo SECURE_GROUP_SYNC_RESPONSE
        break;
    default:
        // ignore all illegal messages and not send any answers
        break;
    }

    /* start initiators */
    async_receive_data_response();
    if ((state == Connection_State::Open_Idle) && (!tx.queue.empty())) {
        state = Connection_State::Open_Wait;

        const Service_Type_Identifier service_type_identifier = tx.queue.front()->service_type_identifier;
        switch (service_type_identifier) {
        case Service_Type_Identifier::TUNNELING_REQUEST:
            async_send_tunnelling_request();
            break;
        case Service_Type_Identifier::TUNNELING_FEATURE_GET:
            async_send_tunnelling_feature_get();
            break;
        case Service_Type_Identifier::TUNNELING_FEATURE_RESPONSE:
            async_send_tunnelling_feature_response();
            break;
        case Service_Type_Identifier::TUNNELING_FEATURE_SET:
            async_send_tunnelling_feature_set();
            break;
        case Service_Type_Identifier::TUNNELING_FEATURE_INFO:
            async_send_tunnelling_feature_info();
            break;
        default:
            assert(false);
            break;
        }
        connection_alive_timer_restart();
    }
}

/* --- Core --- */

/* description request */

void IP_Communication_Channel::async_send_description_request(const std::vector<uint8_t> data)
{
    m_control_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::description_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::description_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (description_Lcon) {
        description_Lcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* description response */

void IP_Communication_Channel::async_send_description_response(const std::vector<uint8_t> data)
{
    m_control_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::description_response_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::description_response_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (description_Rcon) {
        description_Rcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* connect request timer */

void IP_Communication_Channel::connect_request_timer_restart()
{
    connect_request_timer.expires_after(CONNECT_REQUEST_TIMEOUT); // cancel pending timers
    connect_request_timer.async_wait(
        std::bind(&IP_Communication_Channel::connect_request_timer_expired, this, std::placeholders::_1));
}

void IP_Communication_Channel::connect_request_timer_stop()
{
    connect_request_timer.cancel();
}

void IP_Communication_Channel::connect_request_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // success (timer expired)
        break;
    case asio::error::operation_aborted: // timer cancelled
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    /* cancel pending socket operations */
    m_control_socket.cancel();
}

/* connect request */

void IP_Communication_Channel::async_send_connect_request(const std::vector<uint8_t> data)
{
    m_control_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::connect_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::connect_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (connect_Lcon) {
        connect_Lcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* connect response */

void IP_Communication_Channel::async_send_connect_response(const std::vector<uint8_t> data)
{
    m_control_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::connect_response_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::connect_response_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (connect_Rcon) {
        connect_Rcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* connectionstate request timer */

void IP_Communication_Channel::connectionstate_request_timer_restart()
{
    connectionstate_request_timer.expires_after(CONNECTIONSTATE_REQUEST_TIMEOUT); // cancel pending timers
    connectionstate_request_timer.async_wait(
        std::bind(&IP_Communication_Channel::connectionstate_request_timer_expired, this, std::placeholders::_1));
}

void IP_Communication_Channel::connectionstate_request_timer_stop()
{
    connectionstate_request_timer.cancel();
}

void IP_Communication_Channel::connectionstate_request_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // success (timer expired)
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    /* cancel pending socket operations */
    m_control_socket.cancel();
}

/* connectionstate request */

void IP_Communication_Channel::async_send_connectionstate_request(const std::vector<uint8_t> data)
{
    m_control_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::connectionstate_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::connectionstate_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (connectionstate_Lcon) {
        connectionstate_Lcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* connectionstate response */

void IP_Communication_Channel::async_send_connectionstate_response(const std::vector<uint8_t> data)
{
    m_control_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::connectionstate_response_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::connectionstate_response_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (connectionstate_Rcon) {
        connectionstate_Rcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* disconnect request */

void IP_Communication_Channel::async_send_disconnect_request(const std::vector<uint8_t> data)
{
    m_control_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::disconnect_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::disconnect_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (disconnect_Lcon) {
        disconnect_Lcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* disconnect response */

void IP_Communication_Channel::async_send_disconnect_response(const std::vector<uint8_t> data)
{
    m_control_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::disconnect_response_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::disconnect_response_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (disconnect_Rcon) {
        disconnect_Rcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* --- Device Management --- */

/* device configuration request timer */

void IP_Communication_Channel::device_configuration_request_timer_restart()
{
    device_configuration_request_timer.expires_after(DEVICE_CONFIGURATION_REQUEST_TIMEOUT); // cancel pending timers
    device_configuration_request_timer.async_wait(
        std::bind(&IP_Communication_Channel::device_configuration_request_timer_expired, this, std::placeholders::_1));
}

void IP_Communication_Channel::device_configuration_request_timer_stop()
{
    device_configuration_request_timer.cancel();
}

void IP_Communication_Channel::device_configuration_request_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // success (timer expired)
        break;
    case asio::error::operation_aborted: // timer cancelled
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    if (tx.rep_count < tx.max_rep_count) {
        ++tx.rep_count;

        async_send_device_configuration_request();
        device_configuration_request_timer_restart();
        connection_alive_timer_restart();
    } else {
        // @todo handle error
    }
}

/* device configuration request */

void IP_Communication_Channel::async_send_device_configuration_request()
{
    m_data_socket.async_send(
        asio::buffer(tx.queue.front()->toData()),
        std::bind(&IP_Communication_Channel::device_configuration_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::device_configuration_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        // No _con yet. Wait for Ack.
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    // @todo handle error
}

/* device configuration ack */

void IP_Communication_Channel::device_configuration_ack(const Error_Code status, const uint8_t sequence_counter)
{
    /* define request */
    std::shared_ptr<Device_Configuration_Ack_Frame> device_configuration_ack_frame = std::make_shared<Device_Configuration_Ack_Frame>();
    device_configuration_ack_frame->communication_channel_id = communication_channel_id;
    device_configuration_ack_frame->sequence_counter = sequence_counter;
    device_configuration_ack_frame->status = static_cast<uint8_t>(status); // E_NO_ERROR

    /* start initiators */
    async_send_device_configuration_ack();
    connection_alive_timer_restart();
}

void IP_Communication_Channel::async_send_device_configuration_ack()
{
    m_data_socket.async_send(
        asio::buffer(tx.queue.front()->toData()),
        std::bind(&IP_Communication_Channel::device_configuration_ack_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::device_configuration_ack_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        // do nothing
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    // @todo handle error
}

/* --- Tunnelling --- */

/* tunnelling request timer */

void IP_Communication_Channel::tunnelling_request_timer_restart()
{
    tunnelling_request_timer.expires_after(TUNNELLING_REQUEST_TIMEOUT); // cancel pending timers
    tunnelling_request_timer.async_wait(
        std::bind(&IP_Communication_Channel::tunnelling_request_timer_expired, this, std::placeholders::_1));
}

void IP_Communication_Channel::tunnelling_request_timer_stop()
{
    tunnelling_request_timer.cancel();
}

void IP_Communication_Channel::tunnelling_request_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // success (timer expired)
        break;
    case asio::error::operation_aborted: // timer cancelled
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    if (tx.rep_count < tx.max_rep_count) {
        ++tx.rep_count;

        async_send_tunnelling_request();
        tunnelling_request_timer_restart();
        connection_alive_timer_restart();
    } else {
        // @todo handle error
    }
}

/* tunneling request */

void IP_Communication_Channel::async_send_tunnelling_request()
{
    m_data_socket.async_send(
        asio::buffer(tx.queue.front()->toData()),
        std::bind(&IP_Communication_Channel::tunnelling_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::tunnelling_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        // No _con yet. Wait for Ack.
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    // @todo handle error
}

/* tunnelling ack */

void IP_Communication_Channel::tunnelling_ack_req(const Error_Code status, const uint8_t sequence_counter)
{
    /* define request */
    std::shared_ptr<Tunnelling_Ack_Frame> tunnelling_ack_frame = std::make_shared<Tunnelling_Ack_Frame>();
    tunnelling_ack_frame->communication_channel_id = communication_channel_id;
    tunnelling_ack_frame->sequence_counter = sequence_counter;
    tunnelling_ack_frame->status = static_cast<uint8_t>(status);

    /* start initiators */
    async_send_tunnelling_ack(tunnelling_ack_frame);
}

void IP_Communication_Channel::async_send_tunnelling_ack(std::shared_ptr<Tunnelling_Ack_Frame> tunnelling_ack_frame)
{
    m_data_socket.async_send(
        asio::buffer(tunnelling_ack_frame->toData()),
        std::bind(&IP_Communication_Channel::tunnelling_ack_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::tunnelling_ack_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        // do nothing
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    };

    // @todo handle error
}

/* tunnelling feature get */

void IP_Communication_Channel::async_send_tunnelling_feature_get()
{
    m_data_socket.async_send(
        asio::buffer(tx.queue.front()->toData()),
        std::bind(&IP_Communication_Channel::tunnelling_feature_get_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::tunnelling_feature_get_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        // No _con yet. Wait for Ack.
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    // @todo handle error
}

/* tunnelling feature response */

void IP_Communication_Channel::async_send_tunnelling_feature_response()
{
    m_data_socket.async_send(
        asio::buffer(tx.queue.front()->toData()),
        std::bind(&IP_Communication_Channel::tunnelling_feature_response_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::tunnelling_feature_response_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        // No _con yet. Wait for Ack.
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    // @todo handle error
}

/* tunnelling feature set */

void IP_Communication_Channel::async_send_tunnelling_feature_set()
{
    m_data_socket.async_send(
        asio::buffer(tx.queue.front()->toData()),
        std::bind(&IP_Communication_Channel::tunnelling_feature_set_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::tunnelling_feature_set_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        // No _con yet. Wait for Ack.
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    // @todo handle error
}

/* tunnelling feature info */

void IP_Communication_Channel::async_send_tunnelling_feature_info()
{
    m_data_socket.async_send(
        asio::buffer(tx.queue.front()->toData()),
        std::bind(&IP_Communication_Channel::tunnelling_feature_info_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::tunnelling_feature_info_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        // No _con yet. Wait for Ack.
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    // @todo handle error
}

/* --- Routing --- */

/* routing indication */

void IP_Communication_Channel::async_send_routing_indication(const std::vector<uint8_t> data)
{
    m_data_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::routing_indication_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::routing_indication_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    };

    /* call handler */
    if (routing_indication_con) {
        routing_indication_con((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* routing lost */

void IP_Communication_Channel::async_send_routing_lost(const std::vector<uint8_t> routing_lost_data)
{
    m_data_socket.async_send(
        asio::buffer(routing_lost_data),
        std::bind(&IP_Communication_Channel::routing_lost_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::routing_lost_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    };

    /* call handler */
    if (routing_lost_con) {
        routing_lost_con((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* routing busy */

void IP_Communication_Channel::async_send_routing_busy(const std::vector<uint8_t> data)
{
    m_data_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::routing_busy_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::routing_busy_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    };

    /* call handler */
    if (routing_busy_con) {
        routing_busy_con((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* routing system broadcast */

void IP_Communication_Channel::async_send_routing_system_broadcast(const std::vector<uint8_t> data)
{
    m_data_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::routing_system_broadcast_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::routing_system_broadcast_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    };

    /* call handler */
    if (routing_system_broadcast_con) {
        routing_system_broadcast_con((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* --- Remote Logging --- */

/* --- Remote Configuration and Diagnosis --- */

/* remote diagnostic request */

void IP_Communication_Channel::async_send_remote_diagnostic_request(const std::vector<uint8_t> data)
{
    m_data_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::remote_diagnostic_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::remote_diagnostic_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    };

    /* call handler */
    if (remote_diagnostic_Lcon) {
        remote_diagnostic_Lcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* remote diagnostic response */

void IP_Communication_Channel::async_send_remote_diagnostic_response(const std::vector<uint8_t> data)
{
    m_data_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::remote_diagnostic_response_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::remote_diagnostic_response_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    };

    /* call handler */
    if (remote_diagnostic_Rcon) {
        remote_diagnostic_Rcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* remote basic configuration request */

void IP_Communication_Channel::async_send_remote_basic_configuration_request(const std::vector<uint8_t> data)
{
    m_data_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::remote_basic_configuration_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::remote_basic_configuration_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    };

    /* call handler */
    if (remote_basic_configuration_con) {
        remote_basic_configuration_con((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* remote reset request */

void IP_Communication_Channel::async_send_remote_reset_request(const std::vector<uint8_t> data)
{
    m_data_socket.async_send(
        asio::buffer(data),
        std::bind(&IP_Communication_Channel::remote_reset_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Communication_Channel::remote_reset_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    };

    /* call handler */
    if (remote_reset_con) {
        remote_reset_con((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* --- Object Server --- */

/* --- Security --- */

}
