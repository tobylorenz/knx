// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <deque>
#include <functional>
#include <memory>

#include <asio.hpp>

#include <KNX/03/03/02/Status.h>
#include <KNX/03/08/01/Error_Codes.h>
#include <KNX/03/08/02/Communication_Channel_Id.h>
#include <KNX/03/08/02/Connection_Frame.h>
#include <KNX/03/08/02/Connection_Request_Information.h>
#include <KNX/03/08/02/Connection_Response_Data_Block.h>
#include <KNX/03/08/02/Device_Information_DIB.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/02/Supported_Service_Families_DIB.h>
#include <KNX/03/08/03/Device_Configuration_Ack_Frame.h>
#include <KNX/03/08/03/Device_Configuration_Request_Frame.h>
#include <KNX/03/08/04/Tunnelling_Ack_Frame.h>
#include <KNX/03/08/04/Tunnelling_Feature_Get_Frame.h>
#include <KNX/03/08/04/Tunnelling_Feature_Id.h>
#include <KNX/03/08/04/Tunnelling_Feature_Info_Frame.h>
#include <KNX/03/08/04/Tunnelling_Feature_Response_Frame.h>
#include <KNX/03/08/04/Tunnelling_Feature_Set_Frame.h>
#include <KNX/03/08/04/Tunnelling_Layer_Type_Code.h>
#include <KNX/03/08/04/Tunnelling_Request_Frame.h>
#include <KNX/03/08/07/Remote_Reset_Request_Frame.h>
#include <KNX/03/08/07/Selector.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Communication Channel (Control, Data)
 *
 * KNXnet/IP client uses this to connect to one of the KNXnet/IP server's service containers.
 *
 * @todo Use connection alive timer.
 * @todo To keep connection alive, send a connectionstate request in time (50% * CONNECTION_ALIVE_TIME).
 * @todo KNXnet/IP Secure
 */
class KNX_EXPORT IP_Communication_Channel
{
public:
    explicit IP_Communication_Channel(asio::io_context & io_context, const IP_Host_Protocol_Address_Information & remote_control_endpoint);
    virtual ~IP_Communication_Channel();

    /** remote control endpoint */
    const IP_Host_Protocol_Address_Information remote_control_endpoint;

    /* --- Core --- */

    /* description request */
    virtual void description_req();
    std::function<void(const Status status)> description_Lcon;
    std::function<void(const std::shared_ptr<Host_Protocol_Address_Information> control_endpoint)> description_ind;

    /* description response */
    virtual void description_res(const std::shared_ptr<Device_Information_DIB> device_hardware, const std::shared_ptr<Supported_Service_Families_DIB> supported_service_families, const std::vector<std::shared_ptr<Description_Information_Block>> other_device_information);
    std::function<void(const Status status)> description_Rcon;
    std::function<void(const std::shared_ptr<Device_Information_DIB> device_hardware, const std::shared_ptr<Supported_Service_Families_DIB> supported_service_families, const std::vector<std::shared_ptr<Description_Information_Block>> other_device_information)> description_Acon;

    /* connect request */
    virtual void connect_req(const std::shared_ptr<Connection_Request_Information> connection_request_information);
    std::function<void(const Status status)> connect_Lcon;
    std::function<void(const std::shared_ptr<Host_Protocol_Address_Information> control_endpoint, const std::shared_ptr<Host_Protocol_Address_Information> data_endpoint, const std::shared_ptr<Connection_Request_Information> connection_request_information)> connect_ind;

    /* connect response */
    virtual void connect_res(const Communication_Channel_Id communication_channel_id, const Error_Code status, const std::shared_ptr<Host_Protocol_Address_Information> data_endpoint, const std::shared_ptr<Connection_Response_Data_Block> connection_response_data_block);
    std::function<void(const Status status)> connect_Rcon;
    std::function<void(const Error_Code status, const std::shared_ptr<Host_Protocol_Address_Information> data_endpoint, const std::shared_ptr<Connection_Response_Data_Block> connection_response_data_block)> connect_Acon;

    /* connectionstate request */
    virtual void connectionstate_req();
    std::function<void(const Status status)> connectionstate_Lcon;
    std::function<void(const std::shared_ptr<Host_Protocol_Address_Information> control_endpoint)> connectionstate_ind;

    /* connectionstate response */
    virtual void connectionstate_res(const Error_Code status = Error_Code::E_NO_ERROR);
    std::function<void(const Status status)> connectionstate_Rcon;
    std::function<void(const Error_Code status)> connectionstate_Acon;

    /* disconnect request */
    virtual void disconnect_req();
    std::function<void(const Status status)> disconnect_Lcon;
    std::function<void(const std::shared_ptr<Host_Protocol_Address_Information> control_endpoint)> disconnect_ind;

    /* disconnect response */
    virtual void disconnect_res(const Error_Code status = Error_Code::E_NO_ERROR);
    std::function<void(const Status status)> disconnect_Rcon;
    std::function<void(const Error_Code status)> disconnect_Acon;

    /* --- Device Management --- */

    /* device configuration request */
    virtual void device_configuration_req(const std::vector<uint8_t> & cemi_frame_data);
    std::function<void(const Status status)> device_configuration_con;
    std::function<void(const std::vector<uint8_t> cemi_frame_data)> device_configuration_ind;

    /* device configuration ack */
    // ... handled internally

    /* --- Tunnelling --- */

    /* tunnelling request */
    virtual void tunnelling_req(const std::vector<uint8_t> cemi_frame_data);
    std::function<void(const Status status)> tunnelling_con;
    std::function<void(const std::vector<uint8_t> cemi_frame_data)> tunnelling_ind;

    /* tunnelling ack */
    // ... handled internally

    /* tunnelling feature get */
    virtual void tunnelling_feature_get_req(const Tunnelling_Feature_Id feature_identifier);
    std::function<void(const Status status)> tunnelling_feature_get_con;
    std::function<void(const Tunnelling_Feature_Id feature_identifier)> tunnelling_feature_get_ind;

    /* tunnelling feature response */
    virtual void tunnelling_feature_res(
        const Tunnelling_Feature_Id feature_identifier,
        const Error_Code return_code,
        const std::vector<uint8_t> feature_value);
    std::function<void(const Status status)> tunnelling_feature_response_con;
    std::function<void(const Tunnelling_Feature_Id feature_identifier, const Error_Code return_code, std::vector<uint8_t> feature_value)> tunnelling_feature_response_ind;

    /* tunnelling feature set */
    virtual void tunnelling_feature_set_req(
        const Tunnelling_Feature_Id feature_identifier,
        const std::vector<uint8_t> feature_value);
    std::function<void(const Status status)> tunnelling_feature_set_con;
    std::function<void(const Tunnelling_Feature_Id feature_identifier, const std::vector<uint8_t> feature_value)> tunnelling_feature_set_ind;

    /* tunnelling feature info */
    virtual void tunnelling_feature_info_req(
        const Tunnelling_Feature_Id feature_identifier,
        const std::vector<uint8_t> feature_value);
    std::function<void(const Status status)> tunnelling_feature_info_con;
    std::function<void(const Tunnelling_Feature_Id feature_identifier, const std::vector<uint8_t> feature_value)> tunnelling_feature_info_ind;

    /* --- Routing --- */

    /* routing indication */
    virtual void routing_indication_req(const std::vector<uint8_t> cemi_frame_data);
    std::function<void(const Status status)> routing_indication_con;
    std::function<void(const std::vector<uint8_t> cemi_frame_data)> routing_indication_ind;

    /* routing lost */
    virtual void routing_lost_req(const uint8_t device_state, const uint16_t number_of_lost_messages);
    std::function<void(const Status status)> routing_lost_con;
    std::function<void(const uint8_t device_state, const uint16_t number_of_lost_messages)> routing_lost_ind;

    /* routing busy */
    virtual void routing_busy_req(const uint8_t device_state, const uint16_t routing_busy_wait_time, const uint16_t routing_busy_control_field);
    std::function<void(const Status status)> routing_busy_con;
    std::function<void(const uint8_t device_state, const uint16_t routing_busy_wait_time, const uint16_t routing_busy_control_field)> routing_busy_ind;

    /* routing system broadcast */
    virtual void routing_system_broadcast_req();
    std::function<void(const Status status)> routing_system_broadcast_con;
    std::function<void()> routing_system_broadcast_ind;

    /* --- Remote Logging --- */

    /* --- Remote Configuration and Diagnosis --- */

    /* remote diagnostic request */
    virtual void remote_diagnostic_req(const std::shared_ptr<Host_Protocol_Address_Information> discovery_endpoint, const std::shared_ptr<Selector> selector);
    std::function<void(const Status status)> remote_diagnostic_Lcon;
    std::function<void(const std::shared_ptr<Host_Protocol_Address_Information> discovery_endpoint, const std::shared_ptr<Selector> selector)> remote_diagnostic_ind;

    /* remote diagnostic response */
    virtual void remote_diagnostic_res(const std::shared_ptr<Selector> selector, const std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks);
    std::function<void(const Status status)> remote_diagnostic_Rcon;
    std::function<void(const std::shared_ptr<Selector> selector, const std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks)> remote_diagnostic_Acon;

    /* remote basic configuration request */
    virtual void remote_basic_configuration_req(const std::shared_ptr<Host_Protocol_Address_Information> discovery_endpoint, const std::shared_ptr<Selector> selector, const std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks);
    std::function<void(const Status status)> remote_basic_configuration_con;
    std::function<void(const std::shared_ptr<Host_Protocol_Address_Information> discovery_endpoint, const std::shared_ptr<Selector> selector, const std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks)> remote_basic_configuration_ind;

    /* remote reset request */
    virtual void remote_reset_req(const std::shared_ptr<Selector> selector, const Reset_Command_Type_Code reset_command);
    std::function<void(const Status status)> remote_reset_con;
    std::function<void(const std::shared_ptr<Selector> selector, const Reset_Command_Type_Code reset_command)> remote_reset_ind;

    /* --- Object Server --- */

    /* --- Security --- */

protected:
    /** io context */
    asio::io_context & m_io_context;

    /** connection request information */
    std::shared_ptr<Connection_Request_Information> m_connection_request_information{};

    /** communication channel id */
    Communication_Channel_Id communication_channel_id{};

    /** remote data endpoint */
    IP_Host_Protocol_Address_Information m_remote_data_endpoint{};

    /** Connection State */
    enum Connection_State {
        /** no connection */
        Closed,

        /** connection open */
        Open_Idle,

        /** waiting for Ack */
        Open_Wait
    } state{Connection_State::Closed};

    /** Tx State Machine for Connection Frames */
    struct Tx {
        /** send sequence counter to server */
        uint8_t sequence_counter{};

        /** maximum repetition count */
        const uint8_t max_rep_count{3}; // @todo put it somewhere appropriately, also to adjust

        /** transmit queue */
        std::deque<std::shared_ptr<Connection_Frame>> queue{};

        /** repetition count */
        uint8_t rep_count{};
    } tx;

    /** Rx State Machine for Connection Frames */
    struct Rx {
        /** receive sequence counter for tunnelling_request_frame/_ack_frames from server */
        uint8_t sequence_counter{};
    } rx;

    /** local control socket */
    asio::ip::udp::socket m_control_socket;

    /** local data socket */
    asio::ip::udp::socket m_data_socket;

    /** local control endpoint */
    IP_Host_Protocol_Address_Information local_control_endpoint() const;

    /** local data endpoint */
    IP_Host_Protocol_Address_Information local_data_endpoint() const;

    virtual void async_control_connect(const asio::ip::udp::endpoint & remote_control_endpoint);
    virtual void control_connected(const std::error_code & error);

    virtual void async_data_connect();
    virtual void data_connected(const std::error_code & error);

    /* connection alive timer */
    asio::steady_timer connection_alive_timer; // CONNECTION_ALIVE_TIMEOUT
    virtual void connection_alive_timer_restart();
    virtual void connection_alive_timer_stop();
    virtual void connection_alive_timer_expired(const std::error_code & error);

    /* control response multiplexer */
    std::vector<uint8_t> m_control_response_data{};
    virtual void async_receive_control_response();
    virtual void control_response_received(const std::error_code & error, std::size_t bytes_transferred);

    /* data response multiplexer */
    std::vector<uint8_t> m_data_response_data{};
    virtual void async_receive_data_response();
    virtual void data_response_received(const std::error_code & error, std::size_t bytes_transferred);

    /* --- Core --- */

    /* description request */
    virtual void async_send_description_request(const std::vector<uint8_t> data);
    virtual void description_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* description response */
    virtual void async_send_description_response(const std::vector<uint8_t> data);
    virtual void description_response_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* connect request timer */
    asio::steady_timer connect_request_timer; // CONNECT_REQUEST_TIMEOUT
    virtual void connect_request_timer_restart();
    virtual void connect_request_timer_stop();
    virtual void connect_request_timer_expired(const std::error_code & error);

    /* connect request */
    virtual void async_send_connect_request(const std::vector<uint8_t> data);
    virtual void connect_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* connect response */
    virtual void async_send_connect_response(const std::vector<uint8_t> data);
    virtual void connect_response_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* connectionstate request timer */
    asio::steady_timer connectionstate_request_timer; // CONNECTIONSTATE_REQUEST_TIMEOUT
    virtual void connectionstate_request_timer_restart();
    virtual void connectionstate_request_timer_stop();
    virtual void connectionstate_request_timer_expired(const std::error_code & error);

    /* connectionstate request */
    virtual void async_send_connectionstate_request(const std::vector<uint8_t> data);
    virtual void connectionstate_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* connectionstate response */
    virtual void async_send_connectionstate_response(const std::vector<uint8_t> data);
    virtual void connectionstate_response_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* disconnect request */
    virtual void async_send_disconnect_request(const std::vector<uint8_t> data);
    virtual void disconnect_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* disconnect response */
    virtual void async_send_disconnect_response(const std::vector<uint8_t> data);
    virtual void disconnect_response_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* --- Device Management --- */

    /* device configuration request timer */
    asio::steady_timer device_configuration_request_timer; // DEVICE_CONFIGURATION_REQUEST_TIMEOUT
    virtual void device_configuration_request_timer_restart();
    virtual void device_configuration_request_timer_stop();
    virtual void device_configuration_request_timer_expired(const std::error_code & error);

    /* device configuration request */
    virtual void async_send_device_configuration_request();
    virtual void device_configuration_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* device configuration ack */
    virtual void device_configuration_ack(const Error_Code status, const uint8_t sequence_counter);
    virtual void async_send_device_configuration_ack();
    virtual void device_configuration_ack_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* --- Tunnelling --- */

    /* tunnelling request timer */
    asio::steady_timer tunnelling_request_timer; // TUNNELLING_REQUEST_TIMEOUT
    virtual void tunnelling_request_timer_restart();
    virtual void tunnelling_request_timer_stop();
    virtual void tunnelling_request_timer_expired(const std::error_code & error);

    /* tunnelling request */
    virtual void async_send_tunnelling_request();
    virtual void tunnelling_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* tunnelling ack */
    virtual void tunnelling_ack_req(const Error_Code status, const uint8_t sequence_counter);
    virtual void async_send_tunnelling_ack(std::shared_ptr<Tunnelling_Ack_Frame> tunnelling_ack_frame);
    virtual void tunnelling_ack_sent(const std::error_code & error, std::size_t bytes_transferredt);

    /* tunnelling feature get */
    virtual void async_send_tunnelling_feature_get();
    virtual void tunnelling_feature_get_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* tunnelling feature response */
    virtual void async_send_tunnelling_feature_response();
    virtual void tunnelling_feature_response_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* tunnelling feature set */
    virtual void async_send_tunnelling_feature_set();
    virtual void tunnelling_feature_set_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* tunnelling feature info */
    virtual void async_send_tunnelling_feature_info();
    virtual void tunnelling_feature_info_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* --- Routing --- */

    /* routing indication */
    virtual void async_send_routing_indication(const std::vector<uint8_t> data);
    virtual void routing_indication_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* routing lost */
    virtual void async_send_routing_lost(const std::vector<uint8_t> data);
    virtual void routing_lost_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* routing busy */
    virtual void async_send_routing_busy(const std::vector<uint8_t> data);
    virtual void routing_busy_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* routing system broadcast */
    virtual void async_send_routing_system_broadcast(const std::vector<uint8_t> data);
    virtual void routing_system_broadcast_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* --- Remote Logging --- */

    /* --- Remote Configuration and Diagnosis --- */

    /* remote diagnostic request */
    virtual void async_send_remote_diagnostic_request(const std::vector<uint8_t> data);
    virtual void remote_diagnostic_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* remote diagnostic response */
    virtual void async_send_remote_diagnostic_response(const std::vector<uint8_t> data);
    virtual void remote_diagnostic_response_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* remote basic configuration request */
    virtual void async_send_remote_basic_configuration_request(const std::vector<uint8_t> data);
    virtual void remote_basic_configuration_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* remote reset request */
    virtual void async_send_remote_reset_request(const std::vector<uint8_t> data);
    virtual void remote_reset_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* --- Object Server --- */

    /* --- Security --- */
};

}
