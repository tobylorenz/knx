// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/IP_Discovery_Channel.h>

#include <cassert>
#include <iostream>

#include <boost/current_function.hpp>

#include <KNX/03/08/02/Search_Request_Extended_Frame.h>
#include <KNX/03/08/02/Search_Request_Frame.h>
#include <KNX/03/08/02/Search_Response_Extended_Frame.h>
#include <KNX/03/08/02/Search_Response_Frame.h>

namespace KNX {

IP_Discovery_Channel::IP_Discovery_Channel(asio::io_context & io_context) :
    m_io_context(io_context),
    m_discovery_socket(io_context),
    search_timer(io_context)
{
    /* open discovery socket */
    asio::error_code error;
    m_discovery_socket.open(asio::ip::udp::v4(), error);
    switch (error.value()) {
    case 0: // success (socket opened)
        break;
    default:
        std::cerr << __func__ << ": on open: " << error.message().c_str() << std::endl;
        return;
    }

    /* bind local endpoint */
    m_discovery_socket.bind(asio::ip::udp::endpoint(), error);
    switch (error.value()) {
    case 0: // success (socket bind)
        break;
    default:
        std::cerr << __func__ << ": on bind: " << error.message().c_str() << std::endl;
        break;
    }

    /* prepare a buffer for up to 256 octets */
    m_discovery_response_data.resize(256);
}

IP_Discovery_Channel::~IP_Discovery_Channel()
{
    /* shutdown discovery socket */
    asio::error_code error;
    m_discovery_socket.shutdown(asio::ip::udp::socket::shutdown_both, error);
    switch (error.value()) {
    case 0: // success (socket shutdown)
        break;
    case asio::error::not_connected:
        break;
    default:
        std::cerr << __func__ << ": on shutdown: " << error.message().c_str() << std::endl;
        break;
    }

    /* close discovery socket */
    m_discovery_socket.close(error);
    switch (error.value()) {
    case 0: // success (socket closed)
        break;
    default:
        std::cerr << __func__ << ": on close: " << error.message().c_str() << std::endl;
        break;
    }
}

/* search */

void IP_Discovery_Channel::search_req(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::chrono::seconds search_timeout)
{
    /* define request */
    Search_Request_Frame search_request_frame;
    search_request_frame.discovery_endpoint = std::make_shared<IP_Host_Protocol_Address_Information>(local_discovery_endpoint());

    /* start initiators */
    async_receive_response();
    async_send_search_request(remote_discovery_endpoint, search_request_frame.toData());
    search_timer_restart();
}

void IP_Discovery_Channel::search_res(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, std::shared_ptr<Host_Protocol_Address_Information> control_endpoint, const std::shared_ptr<Device_Information_DIB> device_hardware, const std::shared_ptr<Supported_Service_Families_DIB> supported_service_families)
{
    /* define request */
    Search_Response_Frame search_response_frame;
    search_response_frame.control_endpoint = control_endpoint;
    search_response_frame.device_hardware = device_hardware;
    search_response_frame.supported_service_families = supported_service_families;

    /* start initiators */
    async_send_search_response(remote_discovery_endpoint, search_response_frame.toData());
}

/* search extended */

void IP_Discovery_Channel::search_extended_req(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::chrono::seconds search_timeout, const std::vector<std::shared_ptr<Search_Request_Parameter>> search_request_parameters)
{
    /* define request */
    Search_Request_Extended_Frame search_request_extended_frame;
    search_request_extended_frame.discovery_endpoint = std::make_shared<IP_Host_Protocol_Address_Information>(local_discovery_endpoint());
    search_request_extended_frame.search_request_parameters = search_request_parameters;

    /* start initiators */
    async_receive_response();
    async_send_search_request_extended(remote_discovery_endpoint, search_request_extended_frame.toData());
    search_timer_restart();
}

void IP_Discovery_Channel::search_extended_res(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, std::shared_ptr<Host_Protocol_Address_Information> control_endpoint, const std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks)
{
    /* define request */
    Search_Response_Extended_Frame search_response_extended_frame;
    search_response_extended_frame.control_endpoint = control_endpoint;
    search_response_extended_frame.description_information_blocks = description_information_blocks;

    /* start initiators */
    async_send_search_response_extended(remote_discovery_endpoint, search_response_extended_frame.toData());
}

/* protected */

IP_Host_Protocol_Address_Information IP_Discovery_Channel::local_discovery_endpoint() const
{
    IP_Host_Protocol_Address_Information hpai;
    hpai.host_protocol_code = Host_Protocol_Code::IPV4_UDP;

    asio::error_code error;
    asio::ip::udp::endpoint local_discovery_endpoint = m_discovery_socket.local_endpoint(error);
    switch (error.value()) {
    case 0: // success (local endpoint retrieved)
        break;
    default:
        std::cerr << __func__ << ": on local_endpoint: " << error.message().c_str() << std::endl;
        return hpai;
    }

    hpai.ip_address = local_discovery_endpoint.address().to_v4().to_bytes();
    hpai.ip_port_number = local_discovery_endpoint.port();
    return hpai;
}

/* response multiplexer */

void IP_Discovery_Channel::async_receive_response()
{
    m_discovery_socket.async_receive_from(
        asio::buffer(m_discovery_response_data),
        m_response_endpoint,
        std::bind(&IP_Discovery_Channel::response_received, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Discovery_Channel::response_received(const std::error_code & error, std::size_t bytes_transferred)
{
    switch (error.value()) {
    case 0: // success (buffer received)
        break;
    case asio::error::operation_aborted:
        // do nothing
        return;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        return;
    }

    /* determine frame type */
    Frame frame;
    frame.fromData(std::cbegin(m_discovery_response_data), std::cbegin(m_discovery_response_data) + bytes_transferred);
    if (frame.total_length != bytes_transferred) {
        std::cerr << __func__ << ": Unexpected Frame length " << frame.total_length << " != " << bytes_transferred << std::endl;
        return;
    }

    /* ip_address */
    IP_Host_Protocol_Address_Information remote_endpoint;
    remote_endpoint.host_protocol_code = Host_Protocol_Code::IPV4_UDP;
    remote_endpoint.ip_address = m_response_endpoint.address().to_v4().to_bytes();
    remote_endpoint.ip_port_number = m_response_endpoint.port();

    /* handle frame */
    switch (frame.service_type_identifier) {
    case Service_Type_Identifier::SEARCH_REQUEST:
        if (search_ind) {
            Search_Request_Frame search_request_frame;
            search_request_frame.fromData(std::cbegin(m_discovery_response_data), std::cbegin(m_discovery_response_data) + bytes_transferred);
            search_ind(remote_endpoint, search_request_frame.discovery_endpoint);
        }
        break;
    case Service_Type_Identifier::SEARCH_RESPONSE:
        if (search_Acon) {
            Search_Response_Frame search_response_frame;
            search_response_frame.fromData(std::cbegin(m_discovery_response_data), std::cbegin(m_discovery_response_data) + bytes_transferred);
            search_Acon(remote_endpoint, search_response_frame.control_endpoint, search_response_frame.device_hardware, search_response_frame.supported_service_families);
        }
        break;
    case Service_Type_Identifier::SEARCH_REQUEST_EXT:
        if (search_extended_ind) {
            Search_Request_Extended_Frame search_request_extended_frame;
            search_request_extended_frame.fromData(std::cbegin(m_discovery_response_data), std::cbegin(m_discovery_response_data) + bytes_transferred);
            search_extended_ind(remote_endpoint, search_request_extended_frame.discovery_endpoint, search_request_extended_frame.search_request_parameters);
        }
        break;
    case Service_Type_Identifier::SEARCH_RESPONSE_EXT:
        if (search_extended_Acon) {
            Search_Response_Extended_Frame search_response_extended_frame;
            search_response_extended_frame.fromData(std::cbegin(m_discovery_response_data), std::cbegin(m_discovery_response_data) + bytes_transferred);
            search_extended_Acon(remote_endpoint, search_response_extended_frame.control_endpoint, search_response_extended_frame.description_information_blocks);
        }
        break;
    default:
        // ignore these services and not send any answers
        std::cerr << __func__ << ": Unexpected Service_Type_Identifier 0x" << std::hex << static_cast<uint16_t>(frame.service_type_identifier) << std::endl;
    }

    /* start initiators */
    async_receive_response();
}

/* search timer */

void IP_Discovery_Channel::search_timer_restart(const std::chrono::seconds search_timeout)
{
    search_timer.expires_after(search_timeout); // cancel pending timers
    search_timer.async_wait(
        std::bind(&IP_Discovery_Channel::search_timer_expired, this, std::placeholders::_1));
}

void IP_Discovery_Channel::search_timer_stop()
{
    search_timer.cancel();
}

void IP_Discovery_Channel::search_timer_expired(const std::error_code & error)
{
    switch (error.value()) {
    case 0: // success (timer expired)
        break;
    case asio::error::operation_aborted: // timer cancelled
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* cancel pending socket operations */
    m_discovery_socket.cancel();
}

/* search request */

void IP_Discovery_Channel::async_send_search_request(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::vector<uint8_t> search_request_data)
{
    asio::ip::udp::endpoint asio_remote_discovery_endpoint;
    asio_remote_discovery_endpoint.address(asio::ip::address_v4(remote_discovery_endpoint.ip_address));
    asio_remote_discovery_endpoint.port(remote_discovery_endpoint.ip_port_number);

    m_discovery_socket.async_send_to(
        asio::buffer(search_request_data),
        asio_remote_discovery_endpoint,
        std::bind(&IP_Discovery_Channel::search_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Discovery_Channel::search_request_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (search_Lcon) {
        search_Lcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* search response */

void IP_Discovery_Channel::async_send_search_response(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::vector<uint8_t> search_response_data)
{
    asio::ip::udp::endpoint asio_remote_discovery_endpoint;
    asio_remote_discovery_endpoint.address(asio::ip::address_v4(remote_discovery_endpoint.ip_address));
    asio_remote_discovery_endpoint.port(remote_discovery_endpoint.ip_port_number);

    m_discovery_socket.async_send_to(
        asio::buffer(search_response_data),
        asio_remote_discovery_endpoint,
        std::bind(&IP_Discovery_Channel::search_response_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Discovery_Channel::search_response_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (search_Rcon) {
        search_Rcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* search request extended */

void IP_Discovery_Channel::async_send_search_request_extended(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::vector<uint8_t> search_request_extended_data)
{
    asio::ip::udp::endpoint asio_remote_discovery_endpoint;
    asio_remote_discovery_endpoint.address(asio::ip::address_v4(remote_discovery_endpoint.ip_address));
    asio_remote_discovery_endpoint.port(remote_discovery_endpoint.ip_port_number);

    m_discovery_socket.async_send_to(
        asio::buffer(search_request_extended_data),
        asio_remote_discovery_endpoint,
        std::bind(&IP_Discovery_Channel::search_request_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Discovery_Channel::search_request_extended_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (search_extended_Lcon) {
        search_extended_Lcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

/* search response extended */

void IP_Discovery_Channel::async_send_search_response_extended(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::vector<uint8_t> search_response_extended_data)
{
    asio::ip::udp::endpoint asio_remote_discovery_endpoint;
    asio_remote_discovery_endpoint.address(asio::ip::address_v4(remote_discovery_endpoint.ip_address));
    asio_remote_discovery_endpoint.port(remote_discovery_endpoint.ip_port_number);

    m_discovery_socket.async_send_to(
        asio::buffer(search_response_extended_data),
        asio_remote_discovery_endpoint,
        std::bind(&IP_Discovery_Channel::search_response_extended_sent, this, std::placeholders::_1, std::placeholders::_2));
}

void IP_Discovery_Channel::search_response_extended_sent(const std::error_code & error, std::size_t /*bytes_transferred*/)
{
    switch (error.value()) {
    case 0: // success (buffer sent)
        break;
    default:
        std::cerr << __func__ << ": " << error.message().c_str() << std::endl;
        break;
    }

    /* call handler */
    if (search_extended_Rcon) {
        search_extended_Rcon((error.value() == 0) ? Status::ok : Status::not_ok);
    }
}

}
