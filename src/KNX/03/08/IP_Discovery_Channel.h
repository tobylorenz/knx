// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <chrono>
#include <functional>
#include <memory>
#include <vector>

#include <asio.hpp>

#include <KNX/03/03/02/Status.h>
#include <KNX/03/08/01/Internet_Protocol_Constants.h>
#include <KNX/03/08/01/Timeout_Constants.h>
#include <KNX/03/08/02/Device_Information_DIB.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/02/Search_Request_Parameter.h>
#include <KNX/03/08/02/Supported_Service_Families_DIB.h>
#include <KNX/knx_export.h>

namespace KNX {

/**
 * Discovery Channel
 *
 * KNXnet/IP client uses this to discover KNXnet/IP servers and their service containers.
 */
class KNX_EXPORT IP_Discovery_Channel
{
public:
    explicit IP_Discovery_Channel(asio::io_context & io_context);
    virtual ~IP_Discovery_Channel();

    /* search */
    virtual void search_req(
        const IP_Host_Protocol_Address_Information remote_discovery_endpoint = IP_Host_Protocol_Address_Information(System_Setup_Multicast_Address, Port_Number),
        const std::chrono::seconds search_timeout = SEARCH_TIMEOUT);
    std::function<void(const Status status)> search_Lcon;
    std::function<void(
        const IP_Host_Protocol_Address_Information remote_discovery_endpoint,
        const std::shared_ptr<Host_Protocol_Address_Information> discovery_endpoint)> search_ind;
    virtual void search_res(
        const IP_Host_Protocol_Address_Information remote_discovery_endpoint,
        const std::shared_ptr<Host_Protocol_Address_Information> control_endpoint,
        const std::shared_ptr<Device_Information_DIB> device_hardware,
        const std::shared_ptr<Supported_Service_Families_DIB> supported_service_families);
    std::function<void(const Status status)> search_Rcon;
    std::function<void(
        const IP_Host_Protocol_Address_Information remote_discovery_endpoint,
        const std::shared_ptr<Host_Protocol_Address_Information> control_endpoint,
        const std::shared_ptr<Device_Information_DIB> device_hardware,
        const std::shared_ptr<Supported_Service_Families_DIB> supported_service_families)> search_Acon;

    /* search extended */
    virtual void search_extended_req(
        const IP_Host_Protocol_Address_Information remote_discovery_endpoint = IP_Host_Protocol_Address_Information(System_Setup_Multicast_Address, Port_Number),
        const std::chrono::seconds search_timeout = SEARCH_TIMEOUT,
        const std::vector<std::shared_ptr<Search_Request_Parameter>> search_request_parameters = {});
    std::function<void(const Status status)> search_extended_Lcon;
    std::function<void(
        const IP_Host_Protocol_Address_Information remote_discovery_endpoint,
        const std::shared_ptr<Host_Protocol_Address_Information> discovery_endpoint,
        const std::vector<std::shared_ptr<Search_Request_Parameter>> search_request_parameters)> search_extended_ind;
    virtual void search_extended_res(
        const IP_Host_Protocol_Address_Information remote_discovery_endpoint,
        const std::shared_ptr<Host_Protocol_Address_Information> control_endpoint,
        const std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks);
    std::function<void(const Status status)> search_extended_Rcon;
    std::function<void(
        const IP_Host_Protocol_Address_Information remote_discovery_endpoint,
        const std::shared_ptr<Host_Protocol_Address_Information> control_endpoint,
        const std::vector<std::shared_ptr<Description_Information_Block>> description_information_blocks)> search_extended_Acon;

protected:
    /** io context */
    asio::io_context & m_io_context;

    /** local discovery socket */
    asio::ip::udp::socket m_discovery_socket;

    /** local discovery endpoint */
    IP_Host_Protocol_Address_Information local_discovery_endpoint() const;

    /* response multiplexer */
    asio::ip::udp::endpoint m_response_endpoint{};
    std::vector<uint8_t> m_discovery_response_data{};
    void async_receive_response();
    void response_received(const std::error_code & error, std::size_t bytes_transferred);

    /* search timer */
    asio::steady_timer search_timer; // SEARCH_TIMEOUT
    void search_timer_restart(const std::chrono::seconds search_timeout = SEARCH_TIMEOUT);
    void search_timer_stop();
    void search_timer_expired(const std::error_code & error);

    /* search request */
    void async_send_search_request(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::vector<uint8_t> search_request_data);
    void search_request_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* search response */
    void async_send_search_response(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::vector<uint8_t> search_response_data);
    void search_response_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* search request extended */
    void async_send_search_request_extended(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::vector<uint8_t> search_request_extended_data);
    void search_request_extended_sent(const std::error_code & error, std::size_t bytes_transferred);

    /* search response extended */
    void async_send_search_response_extended(const IP_Host_Protocol_Address_Information remote_discovery_endpoint, const std::vector<uint8_t> search_response_extended_data);
    void search_response_extended_sent(const std::error_code & error, std::size_t bytes_transferred);
};

}
