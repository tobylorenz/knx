// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/03/08/IP_Server/IP_Server.h>

namespace KNX {

Server::Server(asio::io_context & io_context) :
    m_io_context(io_context)
{
}

}
