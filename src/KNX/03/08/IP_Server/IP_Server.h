// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <asio.hpp>

#include <KNX/03/08/IP_Server/IP_Service_Container.h>
#include <KNX/knx_export.h>

namespace KNX {

class KNX_EXPORT Server
{
public:
    explicit Server(asio::io_context & io_context);
    virtual ~Server() = default;

protected:
    /** io context */
    asio::io_context & m_io_context;

    /** Discovery endpoint */
    asio::ip::udp::endpoint m_discovery_endpoint;

    /** Service container (1..N) */
    std::vector<IP_Service_Container> m_service_container;
};

}
