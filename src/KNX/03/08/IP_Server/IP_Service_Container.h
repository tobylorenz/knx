// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <vector>

#include <asio.hpp>

#include <KNX/knx_export.h>

namespace KNX {

/** Service container (one for each KNX subnetwork) */
class KNX_EXPORT IP_Service_Container
{
public:
    IP_Service_Container(asio::io_context & io_context);
    virtual ~IP_Service_Container() = default;

protected:
    /** Control endpoint */
    asio::ip::udp::endpoint m_control_endpoint;

    /** Data endpoint(s) (1..N) */
    std::vector<asio::ip::udp::endpoint> m_data_endpoints;
};

}
