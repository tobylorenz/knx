// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 Overview */
#include <KNX/03/08/01_Overview.h>

/* 2 Core */
#include <KNX/03/08/02_Core.h>

/* 3 Device Management */
#include <KNX/03/08/03_Device_Management.h>

/* 4 Tunnelling */
#include <KNX/03/08/04_Tunnelling.h>

/* 5 Routing */
#include <KNX/03/08/05_Routing.h>

/* 6 Remote Logging */
#include <KNX/03/08/06_Remote_Logging.h>

/* 7 Remote Configuration and Diagnosis */
#include <KNX/03/08/07_Remote_Configuration_and_Diagnosis.h>

/* 8 Object Server */
#include <KNX/03/08/08_Object_Server.h>

/* 9 Security */
#include <KNX/03/08/09_Security.h>

/* 10 XML Data Encoding */
//#include <KNX/03/08/10_XML_Data_Encoding.h>
