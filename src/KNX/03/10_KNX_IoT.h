// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 KNX IoT Introduction */

/* 2 KNX IoT Constants */

/* 3 KNX Information Model */

/* 4 KNX IoT 3rd Party API */

/* 5 KNX IoT Point API */

/* 6 KNX IoT Router */
