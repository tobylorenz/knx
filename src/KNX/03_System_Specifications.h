// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 Architecture */
//#include <KNX/03/01_Architecture.h>

/* 2 Communication Medium */
#include <KNX/03/02_Communication_Media.h>

/* 3 Communication */
#include <KNX/03/03_Communication.h>

/* 4 Application Environment */
#include <KNX/03/04_Application_Environment.h>

/* 5 Management */
#include <KNX/03/05_Management.h>

/* 6 Standardised Interfaces */
#include <KNX/03/06_Standardised_Interfaces.h>

/* 7 Interworking */
#include <KNX/03/07_Interworking.h>

/* 8 KNXnet/IP */
#include <KNX/03/08_KNXnet_IP.h>
