// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>

namespace KNX {

/**
 * PL110 BCU1
 *
 * @ingroup KNX_09_04_01_04
 */
class KNX_EXPORT PL110_BCU1
{
};

}
