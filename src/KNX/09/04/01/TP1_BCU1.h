// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>

namespace KNX {

/**
 * TP1 BCU1
 *
 * @ingroup KNX_09_04_01_03
 */
class KNX_EXPORT TP1_BCU1
{
};

}
