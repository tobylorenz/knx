// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/03/02/02_Twisted_Pair_1.h>
#include <KNX/03/03/03_Network_Layer.h>
#include <KNX/03/03/04_Transport_Layer.h>
#include <KNX/03/03/07_Application_Layer.h>
#include <KNX/03/06/03/EMI.h>
#include <KNX/03/06/03/EMI/EMI_PEI.h>
#include <KNX/knx_export.h>
#include <KNX/types.h>

namespace KNX {

/**
 * TP1 BCU2
 *
 * @ingroup KNX_09_04_01_05
 */
class KNX_EXPORT TP1_BCU2 :
    public EMI_PEI
{
public:
    TP1_BCU2(asio::io_context & io_context, TP1_Bus_Simulation & tp1_bus_simulation);

    /** TP1 physical layer simulation */
    TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    TP1_Data_Link_Layer tp1_data_link_layer;

    /** PEI Data con */
    std::deque<std::vector<uint8_t>> pei_data_con{};

    /** PEI Data ind */
    std::deque<std::vector<uint8_t>> pei_data_ind{};

    /** memory minimum authorization level to read memory (0=max level) */
    std::map<Memory_Address, Level> memory_min_read_level{}; // if not set, defaults to minimum_level

    /** user memory */
    std::map<UserMemory_Address, Memory_Data> user_memory{};

    /** authorization keys */
    std::vector<Key> authorization_keys{}; // index is Level. default size: 16. levels: 0..15. minimum_level: 15.

    /** routing table */
    std::map<uint16_t, std::vector<uint8_t>> routing_table{};

    /** router memory */
    std::map<uint16_t, std::vector<uint8_t>> router_memory{};

protected:
    void local_emi_con(const std::vector<uint8_t> data);
    void local_emi_ind(const std::vector<uint8_t> data);

    void local_L_Service_Information_ind();

    void A_IndividualAddress_Write_ind_initiator();
    void A_IndividualAddress_Read_ind_initiator();
    void A_IndividualAddressSerialNumber_Read_ind_initiator();
    void A_IndividualAddressSerialNumber_Write_ind_initiator();
    void A_NetworkParameter_Read_Acon_initiator();
    void A_DeviceDescriptor_Read_ind_initiator();
    void A_DomainAddress_Write_ind_initiator();
    void A_DomainAddress_Read_ind_initiator();
    void A_DomainAddressSelective_Read_ind_initiator();
    void A_DomainAddressSerialNumber_Read_ind_initiator();
    void A_DomainAddressSerialNumber_Write_ind_initiator();
    void A_ADC_Read_ind_initiator();
    void A_Memory_Read_ind_initiator();
    void A_Memory_Write_ind_initiator();
    void A_MemoryBit_Write_ind_initiator();
    void A_UserMemory_Read_ind_initiator();
    void A_UserMemory_Write_ind_initiator();
    void A_UserMemoryBit_Write_ind_initiator();
    void A_UserManufacturerInfo_Read_ind_initiator();
    void A_Authorize_Request_ind_initiator();
    void A_Key_Write_ind_initiator();
    void A_Open_Routing_Table_ind_initiator();
    void A_Read_Routing_Table_ind_initiator();
    void A_Write_Routing_Table_ind_initiator();
    void A_Read_Router_Memory_ind_initiator();
    void A_Write_Router_Memory_ind_initiator();
};

}
