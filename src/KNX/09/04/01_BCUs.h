// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 3 TP1 BCU1 */
#include <KNX/09/04/01/TP1_BCU1.h>

/* 4 PL110 BCU1 */
#include <KNX/09/04/01/PL110_BCU1.h>

/* 5 TP1 BCU2 */
#include <KNX/09/04/01/TP1_BCU2.h>
