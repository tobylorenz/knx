// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>

namespace KNX {

/**
 * BIM M 111 (TP1)
 *
 * @ingroup KNX_09_04_02_03
 */
class KNX_EXPORT BIM_M_111
{
};

}
