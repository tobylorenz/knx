// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

/* 1 BCUs */
#include <KNX/09/04/01_BCUs.h>

/* 2 BIMs */
#include <KNX/09/04/02_BIMs.h>
