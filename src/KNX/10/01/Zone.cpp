// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/10/01/Zone.h>

namespace KNX {

/* 0x4: Geographical tags: Apartment/Floor 01..63.R.S */
/* 0x5: Geographical tags: Apartment/Floor 64..126.R.S */

uint7_t Zone::apartment_floor() const
{
    assert((extended_frame_format == Extended_Frame_Format::LTE0) || (extended_frame_format == Extended_Frame_Format::LTE1));

    switch (extended_frame_format) {
    case Extended_Frame_Format::LTE0:
        return 0 + (group_address >> 10) & 0x3f;
        break;
    case Extended_Frame_Format::LTE1:
        return 64 + (group_address >> 10) & 0x3f;
        break;
    default:
        assert(false);
    }
}

uint6_t Zone::room() const
{
    assert((extended_frame_format == Extended_Frame_Format::LTE0) || (extended_frame_format == Extended_Frame_Format::LTE1));

    return (group_address >> 4) & 0x3f;
}

uint4_t Zone::subzone() const
{
    assert((extended_frame_format == Extended_Frame_Format::LTE0) || (extended_frame_format == Extended_Frame_Format::LTE1));

    return group_address & 0x0f;
}

/* 0x6: range of Application specific tags */

uint4_t Zone::application_domain() const
{
    assert(extended_frame_format == Extended_Frame_Format::LTE2);

    return (group_address >> 12) & 0x0f;
}

uint12_t Zone::application_domain_dependent_tags() const
{
    assert(extended_frame_format == Extended_Frame_Format::LTE2);

    return group_address & 0x3ff;
}

/* 0x7: Unassigned (peripheral) tags & broadcast */

uint16_t Zone::unassigned_tag() const
{
    assert(extended_frame_format == Extended_Frame_Format::LTE3);

    return group_address;
}

}
