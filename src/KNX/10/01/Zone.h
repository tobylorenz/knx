// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <KNX/knx_export.h>
#include <KNX/03/03/02/Extended_Frame_Format.h>
#include <KNX/03/03/02/Group_Address.h>

namespace KNX {

/**
 * Zone
 *
 * @ingroup KNX_10_01_07_02
 */
class KNX_EXPORT Zone
{
public:
    Extended_Frame_Format extended_frame_format{};
    Group_Address group_address{};

    /* 0x4: Geographical tags: Apartment/Floor 01..63.R.S */
    /* 0x5: Geographical tags: Apartment/Floor 64..126.R.S */
    uint7_t apartment_floor() const; // 1..126 (0=wildcard)
    uint6_t room() const; // 1..63 (0=wildcard)
    uint4_t subzone() const; // 1..15 (0=wildcard)

    /* 0x6: range of Application specific tags */
    enum Application_Domain : uint8_t { // actually uint4_t
        HVAC = 0x0,
    };
    uint4_t application_domain() const;
    uint12_t application_domain_dependent_tags() const;

    /* 0x7: Unassigned (peripheral) tags & broadcast */
    uint16_t unassigned_tag() const;
};

}
