// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <stdexcept>
#include <sstream>

#include <KNX/knx_export.h>

namespace KNX {

/**
 * This exception is thrown when DPT::fromData is called with unexpected size
 * for this DPT, e.g. DPT-1 (1 bit) with two byte data.
 */
class KNX_EXPORT DataDoesntMatchDPTException : public std::range_error
{
public:
    /**
     * Constructor
     *
     * @param[in] link Block position within MDF file
     */
    explicit DataDoesntMatchDPTException(uint8_t dpt_size, uint16_t data_size) :
        std::range_error("Data size (" + std::to_string(data_size) + ") doesn't match expected DPT size (" + std::to_string(dpt_size) + ")") {
    }
};

}
