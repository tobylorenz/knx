// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include <KNX/Memory.h>

namespace KNX {

void Memory::set(const Address address, const std::vector<uint8_t> data)
{
    // current address, current data
    Address a{address};
    std::vector<uint8_t>::const_iterator d = std::cbegin(data);

    // search first block
    std::map<Address, std::vector<uint8_t>>::iterator m = std::begin(memory_data);
    if (m == std::end(memory_data)) {
        // create first memory block
        memory_data[a] = {0};
        m = std::begin(memory_data);
    } else {
        while (m != std::end(memory_data)) {
            if ((m->first <= a) && (a < m->first + m->second.size())) {
                // write into existing memory block
                break;
            } else if (a == m->first + m->second.size()) {
                // extend existing block and write into it
                m->second.push_back(0);
                // @todo join two blocks?
                break;
            }
            ++m;
        }
        // create new memory block
        if (m == std::end(memory_data)) {
            memory_data[a] = {0};
            m = std::prev(std::end(memory_data));
        }
    }

    // write data into memory
    while (d != std::cend(data)) {
        std::map<Address, std::vector<uint8_t>>::iterator next_m = std::next(m);
        if ((next_m != std::cend(memory_data)) && (next_m->first == a)) {
            // go to next memory block
            ++m;
        } else if (a == m->first + m->second.size()) {
            // extend existing block and write into it
            m->second.push_back(0);
        }

        m->second[a - m->first] = *d++;
        a++;
    }

    // combine blocks
    bool combined{true};
    while (combined) {
        combined = false;
        m = std::begin(memory_data);
        while (m != std::prev(std::end(memory_data))) {
            std::map<Address, std::vector<uint8_t>>::iterator next_m = std::next(m);
            if ((m->first + m->second.size()) == next_m->first) {
                // combine blocks
                m->second.insert(std::end(m->second), std::cbegin(next_m->second), std::cend(next_m->second));
                memory_data.erase(next_m->first);
                combined = true;
                break;
            }
            ++m;
        }
    }

    // inform observers
    for (const auto & func : updated) {
        func(address, data.size());
    }
}

std::vector<uint8_t> Memory::get(const Address address, const Size size) const
{
    std::vector<uint8_t> data{};

    std::map<Address, std::vector<uint8_t>>::const_iterator m = std::cbegin(memory_data);
    while (m != std::cend(memory_data)) {
        if ((m->first <= address) && (address < m->first + m->second.size())) {
            const Address get_end_address = address + size;
            const Address block_end_address = m->first + m->second.size();
            const Address actual_end_address = std::min(get_end_address, block_end_address);
            data.assign(std::cbegin(m->second) + (address - m->first), std::cbegin(m->second) + (actual_end_address - m->first));
            break;
        }
        ++m;
    }

    return data;
}

}
