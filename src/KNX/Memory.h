// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <cstdint>
#include <functional>
#include <map>
#include <vector>

namespace KNX {

// @todo fulfill requirements for a C++ Container
class Memory
{
public:
    /** address type */
    using Address = uint16_t;

    /** size type */
    using Size = uint16_t;

    Memory() = default;
    virtual ~Memory() = default;

    /** Memory */
    std::map<Address, std::vector<uint8_t>> memory_data{};

    /**
     * set memory
     *
     * @param[in] address address
     * @param[in] data data
     */
    void set(const Address address, const std::vector<uint8_t> data);

    /**
     * get memory
     *
     * @param[in] address
     * @param[in] size
     * @return data
     */
    std::vector<uint8_t> get(const Address address, const Size size) const;

    /**
     * memory has changed notification
     */
    std::vector<std::function<void(const Address address, const Size size)>> updated{};
};

}
