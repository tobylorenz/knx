// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <deque>
#include <functional>

#include <asio/io_context.hpp>

namespace KNX {

/** message bus */
template <typename ... Message>
class Message_Bus
{
public:
    /**
     * completion handler type
     *
     * completion handler is executed, if predicate is true
     */
    using CompletionHandler = std::function<void(const Message ... message)>;

    /**
     * predicate type
     *
     * returns true if predicate is true
     */
    using Predicate = std::function<bool(const Message ... message)>;

    Message_Bus(asio::io_context & io_context) :
        io_context(io_context)
    {
    }

    /**
     * notify all
     *
     * @param[in] message message
     */
    void notify(const Message ... message) {
        for (auto it = message_handlers.begin(); it != message_handlers.end();) {
            assert(it->predicate);
            if (it->predicate(message...)) {
                assert(it->completionHandler);
                auto completionHandler = it->completionHandler;
                io_context.post([completionHandler, message...]{
                    completionHandler(message...);
                });
                it = message_handlers.erase(it);
            } else {
                ++it;
            }
        }
    }

    /**
     * predicate that is always true.
     *
     * @param[in] message message
     *
     * @return always true
     */
    static bool true_predicate(const Message ... /*message*/) {
        return true;
    }

    /**
     * wait for a message and execute completion handler if predicate is true
     *
     * @param[in] completionHandler completion handler
     * @param[in] predicate predicate
     * @param[in] persist persist
     */
    void async_wait(CompletionHandler completionHandler, Predicate predicate = true_predicate) {
        assert(completionHandler);
        assert(predicate);
        message_handlers.push_back({completionHandler, predicate});
    }

    void cancel() {
        message_handlers.clear();
    }

private:
    asio::io_context & io_context;

    /** message handler */
    struct Message_Handler_Entry {
        /** completion handler */
        CompletionHandler completionHandler;

        /** predicate */
        Predicate predicate;
    };

    /** message handlers */
    std::deque<Message_Handler_Entry> message_handlers{};
};

}
