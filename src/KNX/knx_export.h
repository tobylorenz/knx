// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later


namespace KNX {

#ifndef KNX_EXPORT_H
#define KNX_EXPORT_H

#ifdef KNX_STATIC_DEFINE
#  define KNX_EXPORT
#  define KNX_NO_EXPORT
#else
#  ifndef KNX_EXPORT
#    ifdef KNX_EXPORTS
/* We are building this library */
#      define KNX_EXPORT __attribute__((visibility("default")))
#    else
/* We are using this library */
#      define KNX_EXPORT __attribute__((visibility("default")))
#    endif
#  endif

#  ifndef KNX_NO_EXPORT
#    define KNX_NO_EXPORT __attribute__((visibility("hidden")))
#  endif
#endif

#ifndef KNX_DEPRECATED
#  define KNX_DEPRECATED __attribute__ ((__deprecated__))
#endif

#ifndef KNX_DEPRECATED_EXPORT
#  define KNX_DEPRECATED_EXPORT KNX_EXPORT KNX_DEPRECATED
#endif

#ifndef KNX_DEPRECATED_NO_EXPORT
#  define KNX_DEPRECATED_NO_EXPORT KNX_NO_EXPORT KNX_DEPRECATED
#endif

#if 0 /* DEFINE_NO_DEPRECATED */
#  ifndef KNX_NO_DEPRECATED
#    define KNX_NO_DEPRECATED
#  endif
#endif

#endif /* KNX_EXPORT_H */

}
