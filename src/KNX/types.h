// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <cassert>
#include <cstdint>

#include <KNX/knx_export.h>

namespace KNX {

#if 1
template<typename basetype, basetype mask>
class KNX_EXPORT uintX_t
{
public:
    constexpr uintX_t() = default;

    constexpr /*explicit*/ uintX_t(const basetype value) :
        m_value(value) {
        assert((value & mask) == 0);
    }

    constexpr uintX_t & operator=(const basetype & value) {
        assert((value & mask) == 0);

        m_value = value;

        return *this;
    }

    template<typename type>
    constexpr uintX_t & operator=(const type value) {
        assert((value & mask) == 0);

        m_value = value;

        return *this;
    }

    // pre-increment
    basetype & operator++() {
        m_value++;
        m_value &= ~mask;

        return m_value;
    }

    // pre-decrement
    basetype & operator--() {
        m_value--;
        m_value &= ~mask;

        return m_value;
    }

    // post-increment
    basetype operator++(int) {
        m_value++;
        m_value &= ~mask;

        return m_value;
    }

    // post-decrement
    basetype operator--(int) {
        m_value--;
        m_value &= ~mask;

        return m_value;
    }

    operator basetype() const {
        return m_value;
    }

private:
    basetype m_value{};
};

//using uint1_t = uintX_t<uint8_t, 0xFE>;
using uint1_t = bool;
using uint2_t = uintX_t<uint8_t, 0xFC>;
using uint3_t = uintX_t<uint8_t, 0xF8>;
using uint4_t = uintX_t<uint8_t, 0xF0>;
using uint5_t = uintX_t<uint8_t, 0xE0>;
using uint6_t = uintX_t<uint8_t, 0xC0>;
using uint7_t = uintX_t<uint8_t, 0x80>;
//using uint8_t = uintX_t<uint8_t, 0x00>;

using uint9_t = uintX_t<uint16_t, 0xFE00>;
using uint10_t = uintX_t<uint16_t, 0xFC00>;
using uint11_t = uintX_t<uint16_t, 0xF800>;
using uint12_t = uintX_t<uint16_t, 0xF000>;
using uint13_t = uintX_t<uint16_t, 0xE000>;
using uint14_t = uintX_t<uint16_t, 0xC000>;
using uint15_t = uintX_t<uint16_t, 0x8000>;
//using uint16_t = uintX_t<uint16_t, 0x0000>;

using uint17_t = uintX_t<uint32_t, 0xFFFE0000>;
using uint18_t = uintX_t<uint32_t, 0xFFFC0000>;
using uint19_t = uintX_t<uint32_t, 0xFFF80000>;
using uint20_t = uintX_t<uint32_t, 0xFFF00000>;
using uint21_t = uintX_t<uint32_t, 0xFFE00000>;
using uint22_t = uintX_t<uint32_t, 0xFFC00000>;
using uint23_t = uintX_t<uint32_t, 0xFF800000>;
using uint24_t = uintX_t<uint32_t, 0xFF000000>;

using uint25_t = uintX_t<uint32_t, 0xFE000000>;
using uint26_t = uintX_t<uint32_t, 0xFC000000>;
using uint27_t = uintX_t<uint32_t, 0xF8000000>;
using uint28_t = uintX_t<uint32_t, 0xF0000000>;
using uint29_t = uintX_t<uint32_t, 0xE0000000>;
using uint30_t = uintX_t<uint32_t, 0xC0000000>;
using uint31_t = uintX_t<uint32_t, 0x80000000>;
//using uint32_t = uintX_t<uint32_t, 0x00000000>;

using uint33_t = uintX_t<uint64_t, 0xFFFFFFFE00000000>;
using uint34_t = uintX_t<uint64_t, 0xFFFFFFFC00000000>;
using uint35_t = uintX_t<uint64_t, 0xFFFFFFF800000000>;
using uint36_t = uintX_t<uint64_t, 0xFFFFFFF000000000>;
using uint37_t = uintX_t<uint64_t, 0xFFFFFFE000000000>;
using uint38_t = uintX_t<uint64_t, 0xFFFFFFC000000000>;
using uint39_t = uintX_t<uint64_t, 0xFFFFFF8000000000>;
using uint40_t = uintX_t<uint64_t, 0xFFFFFF0000000000>;

using uint41_t = uintX_t<uint64_t, 0xFFFFFE0000000000>;
using uint42_t = uintX_t<uint64_t, 0xFFFFFC0000000000>;
using uint43_t = uintX_t<uint64_t, 0xFFFFF80000000000>;
using uint44_t = uintX_t<uint64_t, 0xFFFFF00000000000>;
using uint45_t = uintX_t<uint64_t, 0xFFFFE00000000000>;
using uint46_t = uintX_t<uint64_t, 0xFFFFC00000000000>;
using uint47_t = uintX_t<uint64_t, 0xFFFF800000000000>;
using uint48_t = uintX_t<uint64_t, 0xFFFF000000000000>;

using uint49_t = uintX_t<uint64_t, 0xFFFE000000000000>;
using uint50_t = uintX_t<uint64_t, 0xFFFC000000000000>;
using uint51_t = uintX_t<uint64_t, 0xFFF8000000000000>;
using uint52_t = uintX_t<uint64_t, 0xFFF0000000000000>;
using uint53_t = uintX_t<uint64_t, 0xFFE0000000000000>;
using uint54_t = uintX_t<uint64_t, 0xFFC0000000000000>;
using uint55_t = uintX_t<uint64_t, 0xFF80000000000000>;
using uint56_t = uintX_t<uint64_t, 0xFF00000000000000>;

using uint57_t = uintX_t<uint64_t, 0xFE00000000000000>;
using uint58_t = uintX_t<uint64_t, 0xFC00000000000000>;
using uint59_t = uintX_t<uint64_t, 0xF800000000000000>;
using uint60_t = uintX_t<uint64_t, 0xF000000000000000>;
using uint61_t = uintX_t<uint64_t, 0xE000000000000000>;
using uint62_t = uintX_t<uint64_t, 0xC000000000000000>;
using uint63_t = uintX_t<uint64_t, 0x8000000000000000>;
//using uint64_t = uintX_t<uint64_t, 0x0000000000000000>;

#else

using uint1_t = uint8_t;
using uint2_t = uint8_t;
using uint3_t = uint8_t;
using uint4_t = uint8_t;
using uint5_t = uint8_t;
using uint6_t = uint8_t;
using uint7_t = uint8_t;
//using uint8_t = uint8_t;

using uint9_t = uint16_t;
using uint10_t = uint16_t;
using uint11_t = uint16_t;
using uint12_t = uint16_t;
using uint13_t = uint16_t;
using uint14_t = uint16_t;
using uint15_t = uint16_t;
//using uint16_t = uint16_t;

using uint17_t = uint32_t;
using uint18_t = uint32_t;
using uint19_t = uint32_t;
using uint20_t = uint32_t;
using uint21_t = uint32_t;
using uint22_t = uint32_t;
using uint23_t = uint32_t;
using uint24_t = uint32_t;

using uint25_t = uint32_t;
using uint26_t = uint32_t;
using uint27_t = uint32_t;
using uint28_t = uint32_t;
using uint29_t = uint32_t;
using uint30_t = uint32_t;
using uint31_t = uint32_t;
//using uint32_t = uint32_t;

using uint40_t = uint64_t;
using uint41_t = uint64_t;
using uint42_t = uint64_t;
using uint43_t = uint64_t;
using uint44_t = uint64_t;
using uint45_t = uint64_t;
using uint46_t = uint64_t;
using uint47_t = uint64_t;

using uint48_t = uint64_t;
using uint49_t = uint64_t;
using uint50_t = uint64_t;
using uint51_t = uint64_t;
using uint52_t = uint64_t;
using uint53_t = uint64_t;
using uint54_t = uint64_t;
using uint55_t = uint64_t;
using uint56_t = uint64_t;

using uint57_t = uint64_t;
using uint58_t = uint64_t;
using uint59_t = uint64_t;
using uint60_t = uint64_t;
using uint61_t = uint64_t;
using uint62_t = uint64_t;
using uint63_t = uint64_t;
//using uint64_t = uint64_t;

#endif

}
