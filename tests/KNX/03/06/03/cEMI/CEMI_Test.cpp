// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/03/03/N_Data_Group/N_Data_Group_PDU.h>
#include <KNX/03/03/04/T_Data_Group/T_Data_Group_PDU.h>
#include <KNX/03/03/07/A_GroupValue_Write/A_GroupValue_Write_PDU.h>
#include <KNX/03/06/03/cEMI.h>
#include <KNX/03/06/03/cEMI/Additional_Info/PL_medium_info.h>
#include <KNX/03/06/03/cEMI/Additional_Info/RF_medium_info.h>
#include <KNX/03/06/03/cEMI/CEMI_Message.h>
#include <KNX/03/06/03/cEMI/Data_Link_Layer/CEMI_LPDU.h>

class CEMI_Test : public ::testing::Test
{
    virtual ~CEMI_Test() = default;
};

/**
 * L_Data.req message without any additional information
 *
 * @ingroup KNX_03_06_03_04_01_04_03_10
 */
TEST(CEMI_Test, Example_1)
{
    std::vector<uint8_t> data {
        0x11, // Message Code
        0x00 // Additional Info Length
    }; // Service Information ...

    KNX::CEMI_Message message;
    message.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(message.message_code, KNX::CEMI_Message_Code::L_Data_req);
    ASSERT_EQ(message.additional_info_length, 0);
    ASSERT_EQ(message.additional_info_length_calculated(), 0);

    ASSERT_EQ(message.toData(), data);
}

/**
 * L_Data.con message, from a Powerline medium frame, with Domain Address
 * @ingroup KNX_03_06_03_04_01_04_03_10
 */
TEST(CEMI_Test, Example_2)
{
    std::vector<uint8_t> data {
        0x2E, // Message Code
        0x04, // Additional Info Length
        0x01, // Type ID
        0x02, // Len
        0x03, // information.Domain_Address
        0x04 // information.Address
    }; // Service Information ...

    KNX::CEMI_Message message;
    message.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(message.message_code, KNX::CEMI_Message_Code::L_Data_con);
    ASSERT_EQ(message.additional_info_length, 4);
    ASSERT_EQ(message.additional_info_length_calculated(), 4);

    ASSERT_EQ(message.additional_info.size(), 1);
    ASSERT_EQ(message.additional_info[0]->type_id, KNX::Additional_Info::TypeId::PL_medium_info);
    ASSERT_EQ(message.additional_info[0]->length, 2);
    std::shared_ptr<KNX::PL_medium_info> ai0 = std::dynamic_pointer_cast<KNX::PL_medium_info>(message.additional_info[0]);
    ASSERT_EQ(ai0->length_calculated(), 2);
    ASSERT_EQ(ai0->domain_address, 0x0304);

    ASSERT_EQ(message.toData(), data);
}

/**
 * L_Data.ind message, RF frame with RF Control Information and KNX Serial Number
 *
 * @ingroup KNX_03_06_03_04_01_04_03_10
 * @note Example shows AIL=9, Len=7 and misses the LFN byte
 */
TEST(CEMI_Test, Example_3)
{
    std::vector<uint8_t> data {
        0x29, // Message Code
        0x0A, // Additional Info Length
        0x02, // Type ID
        0x08, // Len
        0x07, // RF-Ctrl/RF-Info
        0x06, // SN6
        0x05, // SN5
        0x04, // SN4
        0x03, // SN3
        0x02, // SN2
        0x01, // SN1
        0x00 // LFN
    }; // Service Information ...

    KNX::CEMI_Message message;
    message.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(message.message_code, KNX::CEMI_Message_Code::L_Data_ind);
    ASSERT_EQ(message.additional_info_length, 10);
    ASSERT_EQ(message.additional_info_length_calculated(), 10);

    ASSERT_EQ(message.additional_info.size(), 1);
    ASSERT_EQ(message.additional_info[0]->type_id, KNX::Additional_Info::TypeId::RF_medium_info);
    ASSERT_EQ(message.additional_info[0]->length, 8);
    std::shared_ptr<KNX::RF_medium_info> ai0 = std::dynamic_pointer_cast<KNX::RF_medium_info>(message.additional_info[0]);
    ASSERT_EQ(ai0->length_calculated(), 8);
    ASSERT_EQ(ai0->rf_info, 0x07);
    std::array<uint8_t, 6> expected_serial_number_doa{0x06, 0x05, 0x04, 0x03, 0x02, 0x01};
    ASSERT_EQ(ai0->serial_number_doa, expected_serial_number_doa);
    ASSERT_EQ(ai0->link_frame_number, 0x00);

    ASSERT_EQ(message.toData(), data);
}

/**
 * L_Data.ind message, RF frame with RF Control Information and RF Domain Address
 *
 * @ingroup KNX_03_06_03_04_01_04_03_10
 * @note Example shows AIL=9, Len=7 and misses the LFN byte
 */
TEST(CEMI_Test, Example_4)
{
    std::vector<uint8_t> data {
        0x29, // Message Code
        0x0A, // Additional Info Length
        0x02, // Type ID
        0x08, // Len
        0x07, // RF-Ctrl/RF-Info
        0x06, // DoA6
        0x05, // DoA5
        0x04, // DoA4
        0x03, // DoA3
        0x02, // DoA2
        0x01, // DoA1
        0x00 // LFN
    }; // Service Information ...

    KNX::CEMI_Message message;
    message.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(message.message_code, KNX::CEMI_Message_Code::L_Data_ind);
    ASSERT_EQ(message.additional_info_length, 10);
    ASSERT_EQ(message.additional_info_length_calculated(), 10);

    ASSERT_EQ(message.additional_info.size(), 1);
    ASSERT_EQ(message.additional_info[0]->type_id, KNX::Additional_Info::TypeId::RF_medium_info);
    ASSERT_EQ(message.additional_info[0]->length, 8);
    std::shared_ptr<KNX::RF_medium_info> ai0 = std::dynamic_pointer_cast<KNX::RF_medium_info>(message.additional_info[0]);
    ASSERT_EQ(ai0->length_calculated(), 8);
    ASSERT_EQ(ai0->rf_info, 0x07);
    std::array<uint8_t, 6> expected_serial_number_doa{0x06, 0x05, 0x04, 0x03, 0x02, 0x01};
    ASSERT_EQ(ai0->serial_number_doa, expected_serial_number_doa);
    ASSERT_EQ(ai0->link_frame_number, 0x00);

    ASSERT_EQ(message.toData(), data);
}

/**
 * L_Busmon.ind
 *
 * @ingroup KNX_03_06_03_04_01_05_07_06
 */
TEST(CEMI_Test, Test_4_1_5_7_6)
{
    std::vector<uint8_t> data {
        0x2B,       // Message Code: L_Busmon.ind
        0x07,       // Additional Info Length: 7
        0x03, 0x01, 0xff, // Busmonitor Error Flags
        0x04, 0x02, 0xff, 0xff, // Timestamp
        0xff, 0xff  // Data
    };

    std::shared_ptr<KNX::CEMI_Message> msg = KNX::make_CEMI_Message(std::cbegin(data), std::cend(data));
    ASSERT_EQ(msg->message_code, KNX::CEMI_Message_Code::L_Busmon_ind);
    ASSERT_EQ(msg->additional_info_length, 7);
    ASSERT_EQ(msg->additional_info.size(), 2);
    std::shared_ptr<KNX::Busmonitor_Status_Info> ai1 = std::dynamic_pointer_cast<KNX::Busmonitor_Status_Info>(msg->additional_info[0]);
    ASSERT_EQ(ai1->frame_error, true);
    ASSERT_EQ(ai1->bit_error, true);
    ASSERT_EQ(ai1->parity_error, true);
    ASSERT_EQ(ai1->overflow, true);
    ASSERT_EQ(ai1->lost, true);
    ASSERT_EQ(ai1->sequence_number, 7);
    std::shared_ptr<KNX::Timestamp_relative> ai2 = std::dynamic_pointer_cast<KNX::Timestamp_relative>(msg->additional_info[1]);
    ASSERT_EQ(ai2->timestamp_relative, 0xffff);

    std::shared_ptr<KNX::CEMI_L_Busmon_ind> msg2 = std::dynamic_pointer_cast<KNX::CEMI_L_Busmon_ind>(msg);
    std::vector<uint8_t> expected_data {0xff, 0xff};
    ASSERT_EQ(msg2->data, expected_data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
