# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# tests
add_executable(KNX_Float_Test "")
target_sources(KNX_Float_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/KNX_Float_Test.cpp)
target_link_libraries(KNX_Float_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET KNX_Float_Test)
set_target_properties(KNX_Float_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_1_Test "")
target_sources(DPT_1_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_1_Test.cpp)
target_link_libraries(DPT_1_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_1_Test)
set_target_properties(DPT_1_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_2_Test "")
target_sources(DPT_2_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_2_Test.cpp)
target_link_libraries(DPT_2_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_2_Test)
set_target_properties(DPT_2_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_3_Test "")
target_sources(DPT_3_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_3_Test.cpp)
target_link_libraries(DPT_3_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_3_Test)
set_target_properties(DPT_3_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_4_Test "")
target_sources(DPT_4_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_4_Test.cpp)
target_link_libraries(DPT_4_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_4_Test)
set_target_properties(DPT_4_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_5_Test "")
target_sources(DPT_5_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_5_Test.cpp)
target_link_libraries(DPT_5_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_5_Test)
set_target_properties(DPT_5_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_6_Test "")
target_sources(DPT_6_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_6_Test.cpp)
target_link_libraries(DPT_6_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_6_Test)
set_target_properties(DPT_6_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_7_Test "")
target_sources(DPT_7_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_7_Test.cpp)
target_link_libraries(DPT_7_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_7_Test)
set_target_properties(DPT_7_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_8_Test "")
target_sources(DPT_8_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_8_Test.cpp)
target_link_libraries(DPT_8_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_8_Test)
set_target_properties(DPT_8_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_9_Test "")
target_sources(DPT_9_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_9_Test.cpp)
target_link_libraries(DPT_9_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_9_Test)
set_target_properties(DPT_9_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_10_Test "")
target_sources(DPT_10_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_10_Test.cpp)
target_link_libraries(DPT_10_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_10_Test)
set_target_properties(DPT_10_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_11_Test "")
target_sources(DPT_11_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_11_Test.cpp)
target_link_libraries(DPT_11_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_11_Test)
set_target_properties(DPT_11_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_12_Test "")
target_sources(DPT_12_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_12_Test.cpp)
target_link_libraries(DPT_12_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_12_Test)
set_target_properties(DPT_12_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_13_Test "")
target_sources(DPT_13_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_13_Test.cpp)
target_link_libraries(DPT_13_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_13_Test)
set_target_properties(DPT_13_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_14_Test "")
target_sources(DPT_14_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_14_Test.cpp)
target_link_libraries(DPT_14_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_14_Test)
set_target_properties(DPT_14_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_15_Test "")
target_sources(DPT_15_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_15_Test.cpp)
target_link_libraries(DPT_15_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_15_Test)
set_target_properties(DPT_15_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_16_Test "")
target_sources(DPT_16_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_16_Test.cpp)
target_link_libraries(DPT_16_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_16_Test)
set_target_properties(DPT_16_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_17_Test "")
target_sources(DPT_17_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_17_Test.cpp)
target_link_libraries(DPT_17_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_17_Test)
set_target_properties(DPT_17_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_18_Test "")
target_sources(DPT_18_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_18_Test.cpp)
target_link_libraries(DPT_18_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_18_Test)
set_target_properties(DPT_18_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_19_Test "")
target_sources(DPT_19_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_19_Test.cpp)
target_link_libraries(DPT_19_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_19_Test)
set_target_properties(DPT_19_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_20_Test "")
target_sources(DPT_20_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_20_Test.cpp)
target_link_libraries(DPT_20_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_20_Test)
set_target_properties(DPT_20_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_21_Test "")
target_sources(DPT_21_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_21_Test.cpp)
target_link_libraries(DPT_21_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_21_Test)
set_target_properties(DPT_21_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_22_Test "")
target_sources(DPT_22_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_22_Test.cpp)
target_link_libraries(DPT_22_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_22_Test)
set_target_properties(DPT_22_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_23_Test "")
target_sources(DPT_23_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_23_Test.cpp)
target_link_libraries(DPT_23_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_23_Test)
set_target_properties(DPT_23_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_24_Test "")
target_sources(DPT_24_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_24_Test.cpp)
target_link_libraries(DPT_24_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_24_Test)
set_target_properties(DPT_24_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_25_Test "")
target_sources(DPT_25_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_25_Test.cpp)
target_link_libraries(DPT_25_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_25_Test)
set_target_properties(DPT_25_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_26_Test "")
target_sources(DPT_26_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_26_Test.cpp)
target_link_libraries(DPT_26_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_26_Test)
set_target_properties(DPT_26_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_27_Test "")
target_sources(DPT_27_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_27_Test.cpp)
target_link_libraries(DPT_27_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_27_Test)
set_target_properties(DPT_27_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_28_Test "")
target_sources(DPT_28_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_28_Test.cpp)
target_link_libraries(DPT_28_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_28_Test)
set_target_properties(DPT_28_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_29_Test "")
target_sources(DPT_29_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_29_Test.cpp)
target_link_libraries(DPT_29_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_29_Test)
set_target_properties(DPT_29_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_30_Test "")
target_sources(DPT_30_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_30_Test.cpp)
target_link_libraries(DPT_30_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_30_Test)
set_target_properties(DPT_30_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_31_Test "")
target_sources(DPT_31_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_31_Test.cpp)
target_link_libraries(DPT_31_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_31_Test)
set_target_properties(DPT_31_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_200_Test "")
target_sources(DPT_200_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_200_Test.cpp)
target_link_libraries(DPT_200_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_200_Test)
set_target_properties(DPT_200_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_201_Test "")
target_sources(DPT_201_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_201_Test.cpp)
target_link_libraries(DPT_201_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_201_Test)
set_target_properties(DPT_201_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_202_Test "")
target_sources(DPT_202_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_202_Test.cpp)
target_link_libraries(DPT_202_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_202_Test)
set_target_properties(DPT_202_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_203_Test "")
target_sources(DPT_203_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_203_Test.cpp)
target_link_libraries(DPT_203_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_203_Test)
set_target_properties(DPT_203_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_204_Test "")
target_sources(DPT_204_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_204_Test.cpp)
target_link_libraries(DPT_204_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_204_Test)
set_target_properties(DPT_204_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_205_Test "")
target_sources(DPT_205_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_205_Test.cpp)
target_link_libraries(DPT_205_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_205_Test)
set_target_properties(DPT_205_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_206_Test "")
target_sources(DPT_206_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_206_Test.cpp)
target_link_libraries(DPT_206_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_206_Test)
set_target_properties(DPT_206_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_207_Test "")
target_sources(DPT_207_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_207_Test.cpp)
target_link_libraries(DPT_207_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_207_Test)
set_target_properties(DPT_207_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_209_Test "")
target_sources(DPT_209_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_209_Test.cpp)
target_link_libraries(DPT_209_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_209_Test)
set_target_properties(DPT_209_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_210_Test "")
target_sources(DPT_210_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_210_Test.cpp)
target_link_libraries(DPT_210_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_210_Test)
set_target_properties(DPT_210_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_211_Test "")
target_sources(DPT_211_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_211_Test.cpp)
target_link_libraries(DPT_211_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_211_Test)
set_target_properties(DPT_211_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_212_Test "")
target_sources(DPT_212_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_212_Test.cpp)
target_link_libraries(DPT_212_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_212_Test)
set_target_properties(DPT_212_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_213_Test "")
target_sources(DPT_213_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_213_Test.cpp)
target_link_libraries(DPT_213_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_213_Test)
set_target_properties(DPT_213_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_214_Test "")
target_sources(DPT_214_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_214_Test.cpp)
target_link_libraries(DPT_214_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_214_Test)
set_target_properties(DPT_214_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_215_Test "")
target_sources(DPT_215_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_215_Test.cpp)
target_link_libraries(DPT_215_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_215_Test)
set_target_properties(DPT_215_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_216_Test "")
target_sources(DPT_216_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_216_Test.cpp)
target_link_libraries(DPT_216_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_216_Test)
set_target_properties(DPT_216_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_217_Test "")
target_sources(DPT_217_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_217_Test.cpp)
target_link_libraries(DPT_217_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_217_Test)
set_target_properties(DPT_217_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_218_Test "")
target_sources(DPT_218_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_218_Test.cpp)
target_link_libraries(DPT_218_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_218_Test)
set_target_properties(DPT_218_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_219_Test "")
target_sources(DPT_219_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_219_Test.cpp)
target_link_libraries(DPT_219_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_219_Test)
set_target_properties(DPT_219_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_220_Test "")
target_sources(DPT_220_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_220_Test.cpp)
target_link_libraries(DPT_220_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_220_Test)
set_target_properties(DPT_220_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_221_Test "")
target_sources(DPT_221_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_221_Test.cpp)
target_link_libraries(DPT_221_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_221_Test)
set_target_properties(DPT_221_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_222_Test "")
target_sources(DPT_222_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_222_Test.cpp)
target_link_libraries(DPT_222_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_222_Test)
set_target_properties(DPT_222_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_223_Test "")
target_sources(DPT_223_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_223_Test.cpp)
target_link_libraries(DPT_223_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_223_Test)
set_target_properties(DPT_223_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_224_Test "")
target_sources(DPT_224_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_224_Test.cpp)
target_link_libraries(DPT_224_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_224_Test)
set_target_properties(DPT_224_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_225_Test "")
target_sources(DPT_225_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_225_Test.cpp)
target_link_libraries(DPT_225_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_225_Test)
set_target_properties(DPT_225_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_228_Test "")
target_sources(DPT_228_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_228_Test.cpp)
target_link_libraries(DPT_228_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_228_Test)
set_target_properties(DPT_228_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_229_Test "")
target_sources(DPT_229_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_229_Test.cpp)
target_link_libraries(DPT_229_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_229_Test)
set_target_properties(DPT_229_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_230_Test "")
target_sources(DPT_230_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_230_Test.cpp)
target_link_libraries(DPT_230_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_230_Test)
set_target_properties(DPT_230_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_231_Test "")
target_sources(DPT_231_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_231_Test.cpp)
target_link_libraries(DPT_231_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_231_Test)
set_target_properties(DPT_231_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_232_Test "")
target_sources(DPT_232_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_232_Test.cpp)
target_link_libraries(DPT_232_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_232_Test)
set_target_properties(DPT_232_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_234_Test "")
target_sources(DPT_234_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_234_Test.cpp)
target_link_libraries(DPT_234_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_234_Test)
set_target_properties(DPT_234_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_235_Test "")
target_sources(DPT_235_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_235_Test.cpp)
target_link_libraries(DPT_235_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_235_Test)
set_target_properties(DPT_235_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_236_Test "")
target_sources(DPT_236_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_236_Test.cpp)
target_link_libraries(DPT_236_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_236_Test)
set_target_properties(DPT_236_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_237_Test "")
target_sources(DPT_237_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_237_Test.cpp)
target_link_libraries(DPT_237_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_237_Test)
set_target_properties(DPT_237_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_238_Test "")
target_sources(DPT_238_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_238_Test.cpp)
target_link_libraries(DPT_238_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_238_Test)
set_target_properties(DPT_238_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_239_Test "")
target_sources(DPT_239_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_239_Test.cpp)
target_link_libraries(DPT_239_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_239_Test)
set_target_properties(DPT_239_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_240_Test "")
target_sources(DPT_240_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_240_Test.cpp)
target_link_libraries(DPT_240_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_240_Test)
set_target_properties(DPT_240_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_241_Test "")
target_sources(DPT_241_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_241_Test.cpp)
target_link_libraries(DPT_241_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_241_Test)
set_target_properties(DPT_241_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_242_Test "")
target_sources(DPT_242_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_242_Test.cpp)
target_link_libraries(DPT_242_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_242_Test)
set_target_properties(DPT_242_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_243_Test "")
target_sources(DPT_243_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_243_Test.cpp)
target_link_libraries(DPT_243_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_243_Test)
set_target_properties(DPT_243_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_244_Test "")
target_sources(DPT_244_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_244_Test.cpp)
target_link_libraries(DPT_244_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_244_Test)
set_target_properties(DPT_244_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_245_Test "")
target_sources(DPT_245_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_245_Test.cpp)
target_link_libraries(DPT_245_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_245_Test)
set_target_properties(DPT_245_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_246_Test "")
target_sources(DPT_246_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_246_Test.cpp)
target_link_libraries(DPT_246_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_246_Test)
set_target_properties(DPT_246_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_247_Test "")
target_sources(DPT_247_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_247_Test.cpp)
target_link_libraries(DPT_247_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_247_Test)
set_target_properties(DPT_247_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_248_Test "")
target_sources(DPT_248_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_248_Test.cpp)
target_link_libraries(DPT_248_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_248_Test)
set_target_properties(DPT_248_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_249_Test "")
target_sources(DPT_249_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_249_Test.cpp)
target_link_libraries(DPT_249_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_249_Test)
set_target_properties(DPT_249_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_250_Test "")
target_sources(DPT_250_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_250_Test.cpp)
target_link_libraries(DPT_250_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_250_Test)
set_target_properties(DPT_250_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_251_Test "")
target_sources(DPT_251_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_251_Test.cpp)
target_link_libraries(DPT_251_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_251_Test)
set_target_properties(DPT_251_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_252_Test "")
target_sources(DPT_252_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_252_Test.cpp)
target_link_libraries(DPT_252_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_252_Test)
set_target_properties(DPT_252_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_253_Test "")
target_sources(DPT_253_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_253_Test.cpp)
target_link_libraries(DPT_253_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_253_Test)
set_target_properties(DPT_253_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_254_Test "")
target_sources(DPT_254_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_254_Test.cpp)
target_link_libraries(DPT_254_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_254_Test)
set_target_properties(DPT_254_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_255_Test "")
target_sources(DPT_255_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_255_Test.cpp)
target_link_libraries(DPT_255_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_255_Test)
set_target_properties(DPT_255_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_256_Test "")
target_sources(DPT_256_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_256_Test.cpp)
target_link_libraries(DPT_256_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_256_Test)
set_target_properties(DPT_256_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_257_Test "")
target_sources(DPT_257_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_257_Test.cpp)
target_link_libraries(DPT_257_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_257_Test)
set_target_properties(DPT_257_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_265_Test "")
target_sources(DPT_265_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_265_Test.cpp)
target_link_libraries(DPT_265_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_265_Test)
set_target_properties(DPT_265_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_266_Test "")
target_sources(DPT_266_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_266_Test.cpp)
target_link_libraries(DPT_266_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_266_Test)
set_target_properties(DPT_266_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_267_Test "")
target_sources(DPT_267_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_267_Test.cpp)
target_link_libraries(DPT_267_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_267_Test)
set_target_properties(DPT_267_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_268_Test "")
target_sources(DPT_268_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_268_Test.cpp)
target_link_libraries(DPT_268_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_268_Test)
set_target_properties(DPT_268_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_269_Test "")
target_sources(DPT_269_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_269_Test.cpp)
target_link_libraries(DPT_269_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_269_Test)
set_target_properties(DPT_269_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_270_Test "")
target_sources(DPT_270_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_270_Test.cpp)
target_link_libraries(DPT_270_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_270_Test)
set_target_properties(DPT_270_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_271_Test "")
target_sources(DPT_271_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_271_Test.cpp)
target_link_libraries(DPT_271_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_271_Test)
set_target_properties(DPT_271_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_272_Test "")
target_sources(DPT_272_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_272_Test.cpp)
target_link_libraries(DPT_272_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_272_Test)
set_target_properties(DPT_272_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_273_Test "")
target_sources(DPT_273_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_273_Test.cpp)
target_link_libraries(DPT_273_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_273_Test)
set_target_properties(DPT_273_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_274_Test "")
target_sources(DPT_274_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_274_Test.cpp)
target_link_libraries(DPT_274_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_274_Test)
set_target_properties(DPT_274_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_275_Test "")
target_sources(DPT_275_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_275_Test.cpp)
target_link_libraries(DPT_275_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_275_Test)
set_target_properties(DPT_275_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_276_Test "")
target_sources(DPT_276_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_276_Test.cpp)
target_link_libraries(DPT_276_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_276_Test)
set_target_properties(DPT_276_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_277_Test "")
target_sources(DPT_277_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_277_Test.cpp)
target_link_libraries(DPT_277_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_277_Test)
set_target_properties(DPT_277_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_278_Test "")
target_sources(DPT_278_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_278_Test.cpp)
target_link_libraries(DPT_278_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_278_Test)
set_target_properties(DPT_278_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_279_Test "")
target_sources(DPT_279_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_279_Test.cpp)
target_link_libraries(DPT_279_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_279_Test)
set_target_properties(DPT_279_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_280_Test "")
target_sources(DPT_280_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_280_Test.cpp)
target_link_libraries(DPT_280_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_280_Test)
set_target_properties(DPT_280_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_281_Test "")
target_sources(DPT_281_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_281_Test.cpp)
target_link_libraries(DPT_281_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_281_Test)
set_target_properties(DPT_281_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_282_Test "")
target_sources(DPT_282_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_282_Test.cpp)
target_link_libraries(DPT_282_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_282_Test)
set_target_properties(DPT_282_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_283_Test "")
target_sources(DPT_283_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_283_Test.cpp)
target_link_libraries(DPT_283_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_283_Test)
set_target_properties(DPT_283_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)

add_executable(DPT_284_Test "")
target_sources(DPT_284_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/DPT_284_Test.cpp)
target_link_libraries(DPT_284_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET DPT_284_Test)
set_target_properties(DPT_284_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)
