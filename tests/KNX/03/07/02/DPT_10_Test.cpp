// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_10_Test : public ::testing::Test
{
    virtual ~DPT_10_Test() = default;
};

TEST(DPT_10_Test, DPT_10_001)
{
    KNX::DPT_10_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 10);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 0);
    ASSERT_EQ(dpt.hour, 0);
    ASSERT_EQ(dpt.minutes, 0);
    ASSERT_EQ(dpt.seconds, 0);
    ASSERT_EQ(dpt.text(), "no day, 00:00:00"); // @note ETS shows no day, 00:00:00 AM

    data = {0xF7, 0x3B, 0x3B};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 7);
    ASSERT_EQ(dpt.hour, 23);
    ASSERT_EQ(dpt.minutes, 59);
    ASSERT_EQ(dpt.seconds, 59);
    ASSERT_EQ(dpt.text(), "Sunday, 23:59:59"); // @note ETS shows Sunday, 11:59:59 PM

    /* days */

    data = {0x20, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 1);
    ASSERT_EQ(dpt.hour, 0);
    ASSERT_EQ(dpt.minutes, 0);
    ASSERT_EQ(dpt.seconds, 0);
    ASSERT_EQ(dpt.text(), "Monday, 00:00:00");

    data = {0xE0, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 7);
    ASSERT_EQ(dpt.hour, 0);
    ASSERT_EQ(dpt.minutes, 0);
    ASSERT_EQ(dpt.seconds, 0);
    ASSERT_EQ(dpt.text(), "Sunday, 00:00:00");

    /* hours */

    data = {0x17, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 0);
    ASSERT_EQ(dpt.hour, 23);
    ASSERT_EQ(dpt.minutes, 0);
    ASSERT_EQ(dpt.seconds, 0);
    ASSERT_EQ(dpt.text(), "no day, 23:00:00");

    data = {0x1F, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 0);
    ASSERT_EQ(dpt.hour, 31);
    ASSERT_EQ(dpt.minutes, 0);
    ASSERT_EQ(dpt.seconds, 0);
    ASSERT_EQ(dpt.text(), "no day, 31:00:00");

    /* minutes */

    data = {0x00, 0x3B, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 0);
    ASSERT_EQ(dpt.hour, 0);
    ASSERT_EQ(dpt.minutes, 59);
    ASSERT_EQ(dpt.seconds, 0);
    ASSERT_EQ(dpt.text(), "no day, 00:59:00");

    data = {0x00, 0xFF, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x00, 0x3F, 0x00};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 0);
    ASSERT_EQ(dpt.hour, 0);
    ASSERT_EQ(dpt.minutes, 63);
    ASSERT_EQ(dpt.seconds, 0);
    ASSERT_EQ(dpt.text(), "no day, 00:63:00");

    /* seconds */

    data = {0x00, 0x00, 0x3B};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 0);
    ASSERT_EQ(dpt.hour, 0);
    ASSERT_EQ(dpt.minutes, 0);
    ASSERT_EQ(dpt.seconds, 59);
    ASSERT_EQ(dpt.text(), "no day, 00:00:59");

    data = {0x00, 0x00, 0xFF};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x00, 0x00, 0x3F};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 0);
    ASSERT_EQ(dpt.hour, 0);
    ASSERT_EQ(dpt.minutes, 0);
    ASSERT_EQ(dpt.seconds, 63);
    ASSERT_EQ(dpt.text(), "no day, 00:00:63");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
