// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_11_Test : public ::testing::Test
{
    virtual ~DPT_11_Test() = default;
};

TEST(DPT_11_Test, DPT_11_001)
{
    KNX::DPT_11_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 11);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 0);
    ASSERT_EQ(dpt.month, 0);
    ASSERT_EQ(dpt.year, 0);
    ASSERT_EQ(dpt.text(), "00.00.2000");

    data = {0xff, 0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x1f, 0x0f, 0x7f};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 31);
    ASSERT_EQ(dpt.month, 15);
    ASSERT_EQ(dpt.year, 127);
    ASSERT_EQ(dpt.text(), "31.15.2027");

    /* year 2000 - 2089 */

    data = {0x01, 0x01, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 1);
    ASSERT_EQ(dpt.month, 1);
    ASSERT_EQ(dpt.year, 0);
    ASSERT_EQ(dpt.text(), "01.01.2000");

    data = {0x01, 0x01, 0x59};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 1);
    ASSERT_EQ(dpt.month, 1);
    ASSERT_EQ(dpt.year, 89);
    ASSERT_EQ(dpt.text(), "01.01.2089");

    /* year 1990 - 2027 */

    data = {0x01, 0x01, 0x5a};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 1);
    ASSERT_EQ(dpt.month, 1);
    ASSERT_EQ(dpt.year, 90);
    ASSERT_EQ(dpt.text(), "01.01.1990");

    data = {0x01, 0x01, 0x63};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 1);
    ASSERT_EQ(dpt.month, 1);
    ASSERT_EQ(dpt.year, 99);
    ASSERT_EQ(dpt.text(), "01.01.1999");

    data = {0x01, 0x01, 0x64};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 1);
    ASSERT_EQ(dpt.month, 1);
    ASSERT_EQ(dpt.year, 100);
    ASSERT_EQ(dpt.text(), "01.01.2000");

    data = {0x01, 0x01, 0x7f};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.day, 1);
    ASSERT_EQ(dpt.month, 1);
    ASSERT_EQ(dpt.year, 127);
    ASSERT_EQ(dpt.text(), "01.01.2027");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
