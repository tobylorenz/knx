// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_12_Test : public ::testing::Test
{
    virtual ~DPT_12_Test() = default;
};

TEST(DPT_12_Test, DPT_12_001)
{
    KNX::DPT_12_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 12);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 counter pulses");

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 counter pulses");

    data = {0x7f, 0xff, 0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 2147483647);
    ASSERT_EQ(dpt.text(), "2147483647 counter pulses");

    data = {0x80, 0xff, 0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 2164260863);
    ASSERT_EQ(dpt.text(), "2164260863 counter pulses");

    data = {0xff, 0xff, 0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 4294967295);
    ASSERT_EQ(dpt.text(), "4294967295 counter pulses");
}

TEST(DPT_12_Test, DPT_12_100)
{
    KNX::DPT_12_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 12);
    ASSERT_EQ(dpt.subnumber, 100);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 s");
}

TEST(DPT_12_Test, DPT_12_101)
{
    KNX::DPT_12_101 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 12);
    ASSERT_EQ(dpt.subnumber, 101);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 min");
}

TEST(DPT_12_Test, DPT_12_102)
{
    KNX::DPT_12_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 12);
    ASSERT_EQ(dpt.subnumber, 102);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 h");
}

TEST(DPT_12_Test, DPT_12_1200)
{
    KNX::DPT_12_1200 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 12);
    ASSERT_EQ(dpt.subnumber, 1200);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 litre");
}

TEST(DPT_12_Test, DPT_12_1201)
{
    KNX::DPT_12_1201 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 12);
    ASSERT_EQ(dpt.subnumber, 1201);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 m³");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
