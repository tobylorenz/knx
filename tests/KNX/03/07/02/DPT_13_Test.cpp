// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_13_Test : public ::testing::Test
{
    virtual ~DPT_13_Test() = default;
};

TEST(DPT_13_Test, DPT_13_001)
{
    KNX::DPT_13_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 counter pulses");

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 counter pulses");

    data = {0x7f, 0xff, 0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 2147483647);
    ASSERT_EQ(dpt.text(), "2147483647 counter pulses");

    data = {0x80, 0xff, 0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, -2130706433);
    ASSERT_EQ(dpt.text(), "-2130706433 counter pulses");

    data = {0xff, 0xff, 0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-1 counter pulses");
}

TEST(DPT_13_Test, DPT_13_002)
{
    KNX::DPT_13_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 2);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 m³/h"); // @note ETS shows nothing.
}

TEST(DPT_13_Test, DPT_13_010)
{
    KNX::DPT_13_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 10);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 Wh");
}

TEST(DPT_13_Test, DPT_13_011)
{
    KNX::DPT_13_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 11);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 VAh");
}

TEST(DPT_13_Test, DPT_13_012)
{
    KNX::DPT_13_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 12);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 VARh");
}

TEST(DPT_13_Test, DPT_13_013)
{
    KNX::DPT_13_013 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 13);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 kWh");
}

TEST(DPT_13_Test, DPT_13_014)
{
    KNX::DPT_13_014 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 14);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 kVAh");
}

TEST(DPT_13_Test, DPT_13_015)
{
    KNX::DPT_13_015 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 15);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 kVARh");
}

TEST(DPT_13_Test, DPT_13_016)
{
    KNX::DPT_13_016 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 16);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 MWh");
}

TEST(DPT_13_Test, DPT_13_100)
{
    KNX::DPT_13_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 100);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 s");
}

TEST(DPT_13_Test, DPT_13_1200)
{
    KNX::DPT_13_1200 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 1200);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 litre");
}

TEST(DPT_13_Test, DPT_13_1201)
{
    KNX::DPT_13_1201 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 13);
    ASSERT_EQ(dpt.subnumber, 1201);

    data = {0x00, 0x00, 0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 m³");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
