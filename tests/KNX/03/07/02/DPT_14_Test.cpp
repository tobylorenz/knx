// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_14_Test : public ::testing::Test
{
    virtual ~DPT_14_Test() = default;
};

TEST(DPT_14_Test, DPT_14_000)
{
    KNX::DPT_14_000 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 0);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 m/s²");
}

TEST(DPT_14_Test, DPT_14_001)
{
    KNX::DPT_14_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 rad/s²");
}

TEST(DPT_14_Test, DPT_14_002)
{
    KNX::DPT_14_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 2);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 J/mol");
}

TEST(DPT_14_Test, DPT_14_003)
{
    KNX::DPT_14_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 3);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 1/s");
}

TEST(DPT_14_Test, DPT_14_004)
{
    KNX::DPT_14_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 4);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 mol");
}

TEST(DPT_14_Test, DPT_14_005)
{
    KNX::DPT_14_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 5);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14");
}

TEST(DPT_14_Test, DPT_14_006)
{
    KNX::DPT_14_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 6);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 rad");
}

TEST(DPT_14_Test, DPT_14_007)
{
    KNX::DPT_14_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 7);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 °");
}

TEST(DPT_14_Test, DPT_14_008)
{
    KNX::DPT_14_008 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 8);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Js");
}

TEST(DPT_14_Test, DPT_14_009)
{
    KNX::DPT_14_009 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 9);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 rad/s");
}

TEST(DPT_14_Test, DPT_14_010)
{
    KNX::DPT_14_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 10);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 m²");
}

TEST(DPT_14_Test, DPT_14_011)
{
    KNX::DPT_14_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 11);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 F");
}

TEST(DPT_14_Test, DPT_14_012)
{
    KNX::DPT_14_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 12);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 C/m²");
}

TEST(DPT_14_Test, DPT_14_013)
{
    KNX::DPT_14_013 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 13);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 C/m³");
}

TEST(DPT_14_Test, DPT_14_014)
{
    KNX::DPT_14_014 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 14);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 m²/N");
}

TEST(DPT_14_Test, DPT_14_015)
{
    KNX::DPT_14_015 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 15);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 S");
}

TEST(DPT_14_Test, DPT_14_016)
{
    KNX::DPT_14_016 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 16);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 S/m");
}

TEST(DPT_14_Test, DPT_14_017)
{
    KNX::DPT_14_017 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 17);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 kg/m³");
}

TEST(DPT_14_Test, DPT_14_018)
{
    KNX::DPT_14_018 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 18);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 C");
}

TEST(DPT_14_Test, DPT_14_019)
{
    KNX::DPT_14_019 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 19);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 A");
}

TEST(DPT_14_Test, DPT_14_020)
{
    KNX::DPT_14_020 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 20);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 A/m²");
}

TEST(DPT_14_Test, DPT_14_021)
{
    KNX::DPT_14_021 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 21);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Cm");
}

TEST(DPT_14_Test, DPT_14_022)
{
    KNX::DPT_14_022 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 22);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 C/m²");
}

TEST(DPT_14_Test, DPT_14_023)
{
    KNX::DPT_14_023 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 23);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 V/m");
}

TEST(DPT_14_Test, DPT_14_024)
{
    KNX::DPT_14_024 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 24);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 C");
}

TEST(DPT_14_Test, DPT_14_025)
{
    KNX::DPT_14_025 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 25);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 C/m²");
}

TEST(DPT_14_Test, DPT_14_026)
{
    KNX::DPT_14_026 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 26);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 C/m²");
}

TEST(DPT_14_Test, DPT_14_027)
{
    KNX::DPT_14_027 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 27);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 V");
}

TEST(DPT_14_Test, DPT_14_028)
{
    KNX::DPT_14_028 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 28);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 V");
}

TEST(DPT_14_Test, DPT_14_029)
{
    KNX::DPT_14_029 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 29);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Am²");
}

TEST(DPT_14_Test, DPT_14_030)
{
    KNX::DPT_14_030 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 30);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 V");
}

TEST(DPT_14_Test, DPT_14_031)
{
    KNX::DPT_14_031 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 31);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 J");
}

TEST(DPT_14_Test, DPT_14_032)
{
    KNX::DPT_14_032 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 32);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 N");
}

TEST(DPT_14_Test, DPT_14_033)
{
    KNX::DPT_14_033 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 33);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Hz");
}

TEST(DPT_14_Test, DPT_14_034)
{
    KNX::DPT_14_034 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 34);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 rad/s");
}

TEST(DPT_14_Test, DPT_14_035)
{
    KNX::DPT_14_035 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 35);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 J/K");
}

TEST(DPT_14_Test, DPT_14_036)
{
    KNX::DPT_14_036 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 36);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 W");
}

TEST(DPT_14_Test, DPT_14_037)
{
    KNX::DPT_14_037 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 37);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 J");
}

TEST(DPT_14_Test, DPT_14_038)
{
    KNX::DPT_14_038 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 38);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Ω");
}

TEST(DPT_14_Test, DPT_14_039)
{
    KNX::DPT_14_039 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 39);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 m");
}

TEST(DPT_14_Test, DPT_14_040)
{
    KNX::DPT_14_040 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 40);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 J");
}

TEST(DPT_14_Test, DPT_14_041)
{
    KNX::DPT_14_041 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 41);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 cd/m²");
}

TEST(DPT_14_Test, DPT_14_042)
{
    KNX::DPT_14_042 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 42);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 lm");
}

TEST(DPT_14_Test, DPT_14_043)
{
    KNX::DPT_14_043 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 43);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 cd");
}

TEST(DPT_14_Test, DPT_14_044)
{
    KNX::DPT_14_044 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 44);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 A/m");
}

TEST(DPT_14_Test, DPT_14_045)
{
    KNX::DPT_14_045 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 45);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Wb");
}

TEST(DPT_14_Test, DPT_14_046)
{
    KNX::DPT_14_046 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 46);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 T");
}

TEST(DPT_14_Test, DPT_14_047)
{
    KNX::DPT_14_047 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 47);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Am²");
}

TEST(DPT_14_Test, DPT_14_048)
{
    KNX::DPT_14_048 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 48);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 T");
}

TEST(DPT_14_Test, DPT_14_049)
{
    KNX::DPT_14_049 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 49);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 A/m");
}

TEST(DPT_14_Test, DPT_14_050)
{
    KNX::DPT_14_050 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 50);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 A");
}

TEST(DPT_14_Test, DPT_14_051)
{
    KNX::DPT_14_051 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 51);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 kg");
}

TEST(DPT_14_Test, DPT_14_052)
{
    KNX::DPT_14_052 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 52);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 kg/s");
}

TEST(DPT_14_Test, DPT_14_053)
{
    KNX::DPT_14_053 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 53);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 N/s");
}

TEST(DPT_14_Test, DPT_14_054)
{
    KNX::DPT_14_054 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 54);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 rad");
}

TEST(DPT_14_Test, DPT_14_055)
{
    KNX::DPT_14_055 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 55);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 °");
}

TEST(DPT_14_Test, DPT_14_056)
{
    KNX::DPT_14_056 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 56);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 W");
}

TEST(DPT_14_Test, DPT_14_057)
{
    KNX::DPT_14_057 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 57);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 cos Φ");
}

TEST(DPT_14_Test, DPT_14_058)
{
    KNX::DPT_14_058 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 58);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Pa");
}

TEST(DPT_14_Test, DPT_14_059)
{
    KNX::DPT_14_059 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 59);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Ω");
}

TEST(DPT_14_Test, DPT_14_060)
{
    KNX::DPT_14_060 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 60);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Ω");
}

TEST(DPT_14_Test, DPT_14_061)
{
    KNX::DPT_14_061 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 61);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Ωm");
}

TEST(DPT_14_Test, DPT_14_062)
{
    KNX::DPT_14_062 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 62);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 H");
}

TEST(DPT_14_Test, DPT_14_063)
{
    KNX::DPT_14_063 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 63);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 sr");
}

TEST(DPT_14_Test, DPT_14_064)
{
    KNX::DPT_14_064 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 64);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 W/m²");
}

TEST(DPT_14_Test, DPT_14_065)
{
    KNX::DPT_14_065 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 65);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 m/s");
}

TEST(DPT_14_Test, DPT_14_066)
{
    KNX::DPT_14_066 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 66);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Pa");
}

TEST(DPT_14_Test, DPT_14_067)
{
    KNX::DPT_14_067 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 67);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 N/m");
}

TEST(DPT_14_Test, DPT_14_068)
{
    KNX::DPT_14_068 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 68);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 °C");
}

TEST(DPT_14_Test, DPT_14_069)
{
    KNX::DPT_14_069 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 69);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 K");
}

TEST(DPT_14_Test, DPT_14_070)
{
    KNX::DPT_14_070 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 70);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 K");
}

TEST(DPT_14_Test, DPT_14_071)
{
    KNX::DPT_14_071 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 71);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 J/K");
}

TEST(DPT_14_Test, DPT_14_072)
{
    KNX::DPT_14_072 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 72);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 W/mK");
}

TEST(DPT_14_Test, DPT_14_073)
{
    KNX::DPT_14_073 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 73);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 V/K");
}

TEST(DPT_14_Test, DPT_14_074)
{
    KNX::DPT_14_074 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 74);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 s");
}

TEST(DPT_14_Test, DPT_14_075)
{
    KNX::DPT_14_075 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 75);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 Nm");
}

TEST(DPT_14_Test, DPT_14_076)
{
    KNX::DPT_14_076 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 76);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 m³");
}

TEST(DPT_14_Test, DPT_14_077)
{
    KNX::DPT_14_077 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 77);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 m³/s");
}

TEST(DPT_14_Test, DPT_14_078)
{
    KNX::DPT_14_078 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 78);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 N");
}

TEST(DPT_14_Test, DPT_14_079)
{
    KNX::DPT_14_079 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 79);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 J");
}

TEST(DPT_14_Test, DPT_14_080)
{
    KNX::DPT_14_080 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 80);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 VA");
}

TEST(DPT_14_Test, DPT_14_1200)
{
    KNX::DPT_14_1200 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 1200);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 m³/h");
}

TEST(DPT_14_Test, DPT_14_1201)
{
    KNX::DPT_14_1201 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 14);
    ASSERT_EQ(dpt.subnumber, 1201);

    data = {0x40, 0x49, 0x0f, 0xdb};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_FLOAT_EQ(dpt.float_value, 3.14159274101);
    ASSERT_EQ(dpt.text(), "3.14 1/ls");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
