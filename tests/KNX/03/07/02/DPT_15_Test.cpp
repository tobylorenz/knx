// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_15_Test : public ::testing::Test
{
    virtual ~DPT_15_Test() = default;
};

TEST(DPT_15_Test, DPT_15_000)
{
    KNX::DPT_15_000 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 15);
    ASSERT_EQ(dpt.subnumber, 0);

    // @note ETS shows nothing.

    /** EXAMPLE 1 from specification */
    data = {0x12, 0x34, 0x56, 0x4D};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.digits[0], 1);
    ASSERT_EQ(dpt.digits[1], 2);
    ASSERT_EQ(dpt.digits[2], 3);
    ASSERT_EQ(dpt.digits[3], 4);
    ASSERT_EQ(dpt.digits[4], 5);
    ASSERT_EQ(dpt.digits[5], 6);
    ASSERT_EQ(dpt.e, false);
    ASSERT_EQ(dpt.p, true);
    ASSERT_EQ(dpt.d, false);
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.index, 13);
    ASSERT_EQ(dpt.text(), "13:123456 (-P--) = (Detection error: no error, Permission: accepted, Read direction: left to right, Encryption of access information: no)");

    /** EXAMPLE 2 from specification */
    data = {0x00, 0x67, 0x89, 0x0E};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.digits[0], 0);
    ASSERT_EQ(dpt.digits[1], 0);
    ASSERT_EQ(dpt.digits[2], 6);
    ASSERT_EQ(dpt.digits[3], 7);
    ASSERT_EQ(dpt.digits[4], 8);
    ASSERT_EQ(dpt.digits[5], 9);
    ASSERT_EQ(dpt.e, false);
    ASSERT_EQ(dpt.p, false);
    ASSERT_EQ(dpt.d, false);
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.index, 14);
    ASSERT_EQ(dpt.text(), "14:006789 (----) = (Detection error: no error, Permission: not accepted, Read direction: left to right, Encryption of access information: no)");

    data = {0x12, 0x34, 0x56, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.digits[0], 1);
    ASSERT_EQ(dpt.digits[1], 2);
    ASSERT_EQ(dpt.digits[2], 3);
    ASSERT_EQ(dpt.digits[3], 4);
    ASSERT_EQ(dpt.digits[4], 5);
    ASSERT_EQ(dpt.digits[5], 6);
    ASSERT_EQ(dpt.e, false);
    ASSERT_EQ(dpt.p, false);
    ASSERT_EQ(dpt.d, false);
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.index, 0);
    ASSERT_EQ(dpt.text(), "0:123456 (----) = (Detection error: no error, Permission: not accepted, Read direction: left to right, Encryption of access information: no)");

    data = {0x12, 0x34, 0x56, 0xFF};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.digits[0], 1);
    ASSERT_EQ(dpt.digits[1], 2);
    ASSERT_EQ(dpt.digits[2], 3);
    ASSERT_EQ(dpt.digits[3], 4);
    ASSERT_EQ(dpt.digits[4], 5);
    ASSERT_EQ(dpt.digits[5], 6);
    ASSERT_EQ(dpt.e, true);
    ASSERT_EQ(dpt.p, true);
    ASSERT_EQ(dpt.d, true);
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.index, 15);
    ASSERT_EQ(dpt.text(), "15:123456 (EPDC) = (Detection error: reading of access information code was not successful, Permission: accepted, Read direction: right to left, Encryption of access information: yes)");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
