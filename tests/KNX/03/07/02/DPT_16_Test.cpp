// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_16_Test : public ::testing::Test
{
    virtual ~DPT_16_Test() = default;
};

TEST(DPT_16_Test, DPT_16_000)
{
    KNX::DPT_16_000 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 16);
    ASSERT_EQ(dpt.subnumber, 0);

    data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "");
    ASSERT_EQ(dpt.text().length(), 0);

    data = {0x4B, 0x4E, 0x58, 0x20, 0x69, 0x73, 0x20, 0x4F, 0x4B, 0x00, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.characters[0], 'K');
    ASSERT_EQ(dpt.characters[1], 'N');
    ASSERT_EQ(dpt.characters[2], 'X');
    ASSERT_EQ(dpt.characters[3], ' ');
    ASSERT_EQ(dpt.characters[4], 'i');
    ASSERT_EQ(dpt.characters[5], 's');
    ASSERT_EQ(dpt.characters[6], ' ');
    ASSERT_EQ(dpt.characters[7], 'O');
    ASSERT_EQ(dpt.characters[8], 'K');
    ASSERT_EQ(dpt.characters[9], 0);
    ASSERT_EQ(dpt.characters[10], 0);
    ASSERT_EQ(dpt.characters[11], 0);
    ASSERT_EQ(dpt.characters[12], 0);
    ASSERT_EQ(dpt.characters[13], 0);
    ASSERT_EQ(dpt.text(), "KNX is OK");
    ASSERT_EQ(dpt.text().length(), 9);

    data = {0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "ABCDEFGHIJKLMN");
    ASSERT_EQ(dpt.text().length(), 14);
}

TEST(DPT_16_Test, DPT_16_001)
{
    KNX::DPT_16_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 16);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "");
    ASSERT_EQ(dpt.text().length(), 0);

    data = {0x4B, 0x4E, 0x58, 0x20, 0x69, 0x73, 0x20, 0x4F, 0x4B, 0x00, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.characters[0], 'K');
    ASSERT_EQ(dpt.characters[1], 'N');
    ASSERT_EQ(dpt.characters[2], 'X');
    ASSERT_EQ(dpt.characters[3], ' ');
    ASSERT_EQ(dpt.characters[4], 'i');
    ASSERT_EQ(dpt.characters[5], 's');
    ASSERT_EQ(dpt.characters[6], ' ');
    ASSERT_EQ(dpt.characters[7], 'O');
    ASSERT_EQ(dpt.characters[8], 'K');
    ASSERT_EQ(dpt.characters[9], 0);
    ASSERT_EQ(dpt.characters[10], 0);
    ASSERT_EQ(dpt.characters[11], 0);
    ASSERT_EQ(dpt.characters[12], 0);
    ASSERT_EQ(dpt.characters[13], 0);
    ASSERT_EQ(dpt.text(), "KNX is OK");
    ASSERT_EQ(dpt.text().length(), 9);

    data = {0x41, 0x42, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4A, 0x4B, 0x4C, 0x4D, 0x4E};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "ABCDEFGHIJKLMN");
    ASSERT_EQ(dpt.text().length(), 14);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
