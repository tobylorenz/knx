// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_17_Test : public ::testing::Test
{
    virtual ~DPT_17_Test() = default;
};

TEST(DPT_17_Test, DPT_17_001)
{
    KNX::DPT_17_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 17);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "1"); // @note ETS shows scene_number + 1

    data = {0x3f};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "64"); // @note ETS shows scene_number + 1

    data = {0x40};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x00};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "1"); // @note ETS shows scene_number + 1

    data = {0x7f};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x3f};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "64"); // @note ETS shows scene_number + 1

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x3f};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "64"); // @note ETS shows scene_number + 1
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
