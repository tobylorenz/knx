// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_18_Test : public ::testing::Test
{
    virtual ~DPT_18_Test() = default;
};

TEST(DPT_18_Test, DPT_18_001)
{
    KNX::DPT_18_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 18);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Activate 1");

    data = {0x3f};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Activate 64");

    data = {0x40};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x00};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Activate 1");

    data = {0x7f};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x3f};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Activate 64");

    data = {0x80};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Learn 1");

    data = {0xc0};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x80};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Learn 1");

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0xbf};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Learn 64");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
