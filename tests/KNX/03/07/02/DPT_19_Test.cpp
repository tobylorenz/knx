// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_19_Test : public ::testing::Test
{
    virtual ~DPT_19_Test() = default;
};

TEST(DPT_19_Test, DPT_19_001)
{
    KNX::DPT_19_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 19);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Any day, 00.00.1900, 00:00:00 (----------) = (Fault: Normal (no fault), Working Day: Bank Holiday (No working day), No WD: WD field valid, No Year: Year field valid, No Date: Month and Day of Month field valid, No Day of Week: Day of week field valid, No Time: Hour of day, Minutes and Seconds field valid, Standard Summer Time: Time = UT+X, Quality of clock: clock without ext. sync signal, Synchronisation source reliability: unreliable synchronisation source (mains, local quartz))"); // @note ETS shows "Any day, 1/1/1900, 12:00:00 AM ...

    /* year */

    data = {0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Any day, 00.00.2155, 00:00:00 (----------) = (Fault: Normal (no fault), Working Day: Bank Holiday (No working day), No WD: WD field valid, No Year: Year field valid, No Date: Month and Day of Month field valid, No Day of Week: Day of week field valid, No Time: Hour of day, Minutes and Seconds field valid, Standard Summer Time: Time = UT+X, Quality of clock: clock without ext. sync signal, Synchronisation source reliability: unreliable synchronisation source (mains, local quartz))");

    /* month */

    data = {0x00, 0x0c, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Any day, 00.12.1900, 00:00:00 (----------) = (Fault: Normal (no fault), Working Day: Bank Holiday (No working day), No WD: WD field valid, No Year: Year field valid, No Date: Month and Day of Month field valid, No Day of Week: Day of week field valid, No Time: Hour of day, Minutes and Seconds field valid, Standard Summer Time: Time = UT+X, Quality of clock: clock without ext. sync signal, Synchronisation source reliability: unreliable synchronisation source (mains, local quartz))");

    /* day of month */

    data = {0x00, 0x00, 0x1f, 0x00, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Any day, 31.00.1900, 00:00:00 (----------) = (Fault: Normal (no fault), Working Day: Bank Holiday (No working day), No WD: WD field valid, No Year: Year field valid, No Date: Month and Day of Month field valid, No Day of Week: Day of week field valid, No Time: Hour of day, Minutes and Seconds field valid, Standard Summer Time: Time = UT+X, Quality of clock: clock without ext. sync signal, Synchronisation source reliability: unreliable synchronisation source (mains, local quartz))");

    /* day of week */

    data = {0x00, 0x00, 0x00, 0xe0, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Sunday, 00.00.1900, 00:00:00 (----------) = (Fault: Normal (no fault), Working Day: Bank Holiday (No working day), No WD: WD field valid, No Year: Year field valid, No Date: Month and Day of Month field valid, No Day of Week: Day of week field valid, No Time: Hour of day, Minutes and Seconds field valid, Standard Summer Time: Time = UT+X, Quality of clock: clock without ext. sync signal, Synchronisation source reliability: unreliable synchronisation source (mains, local quartz))");

    /* hour of day */

    data = {0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Any day, 00.00.1900, 24:00:00 (----------) = (Fault: Normal (no fault), Working Day: Bank Holiday (No working day), No WD: WD field valid, No Year: Year field valid, No Date: Month and Day of Month field valid, No Day of Week: Day of week field valid, No Time: Hour of day, Minutes and Seconds field valid, Standard Summer Time: Time = UT+X, Quality of clock: clock without ext. sync signal, Synchronisation source reliability: unreliable synchronisation source (mains, local quartz))");

    /* minutes */

    data = {0x00, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Any day, 00.00.1900, 00:59:00 (----------) = (Fault: Normal (no fault), Working Day: Bank Holiday (No working day), No WD: WD field valid, No Year: Year field valid, No Date: Month and Day of Month field valid, No Day of Week: Day of week field valid, No Time: Hour of day, Minutes and Seconds field valid, Standard Summer Time: Time = UT+X, Quality of clock: clock without ext. sync signal, Synchronisation source reliability: unreliable synchronisation source (mains, local quartz))");

    /* seconds */

    data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x3b, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Any day, 00.00.1900, 00:00:59 (----------) = (Fault: Normal (no fault), Working Day: Bank Holiday (No working day), No WD: WD field valid, No Year: Year field valid, No Date: Month and Day of Month field valid, No Day of Week: Day of week field valid, No Time: Hour of day, Minutes and Seconds field valid, Standard Summer Time: Time = UT+X, Quality of clock: clock without ext. sync signal, Synchronisation source reliability: unreliable synchronisation source (mains, local quartz))");

    /* flags */

    data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    data = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0xC0};
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.text(), "Any day, 00.00.1900, 00:00:00 (FWVYDOTSQR) = (Fault: Fault, Working Day: Working day, No WD: WD field not valid, No Year: Year field not valid, No Date: Month and Day of Month field not valid, No Day of Week: Day of week field not valid, No Time: Hour of day, Minutes and Seconds field not valid, Standard Summer Time: Time = UT+X+1, Quality of clock: clock with ext. sync signal, Synchronisation source reliability: reliable synchronisation source (radio, Internet))");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
