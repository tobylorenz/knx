// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_1_Test : public ::testing::Test
{
    virtual ~DPT_1_Test() = default;
};

TEST(DPT_1_Test, DPT_1_001)
{
    KNX::DPT_1_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Off");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "On");
}

TEST(DPT_1_Test, DPT_1_002)
{
    KNX::DPT_1_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 2);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "False");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "True");
}

TEST(DPT_1_Test, DPT_1_003)
{
    KNX::DPT_1_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 3);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Disable");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Enable");
}

TEST(DPT_1_Test, DPT_1_004)
{
    KNX::DPT_1_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 4);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "No ramp");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Ramp");
}

TEST(DPT_1_Test, DPT_1_005)
{
    KNX::DPT_1_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 5);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "No alarm");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Alarm");
}

TEST(DPT_1_Test, DPT_1_006)
{
    KNX::DPT_1_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 6);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Low");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "High");
}

TEST(DPT_1_Test, DPT_1_007)
{
    KNX::DPT_1_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 7);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Decrease");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Increase");
}

TEST(DPT_1_Test, DPT_1_008)
{
    KNX::DPT_1_008 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 8);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Up");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Down");
}

TEST(DPT_1_Test, DPT_1_009)
{
    KNX::DPT_1_009 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 9);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Open");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Close");
}

TEST(DPT_1_Test, DPT_1_010)
{
    KNX::DPT_1_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 10);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Stop");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Start");
}

TEST(DPT_1_Test, DPT_1_011)
{
    KNX::DPT_1_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 11);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Inactive");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Active");
}

TEST(DPT_1_Test, DPT_1_012)
{
    KNX::DPT_1_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 12);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Not inverted");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Inverted");
}

TEST(DPT_1_Test, DPT_1_013)
{
    KNX::DPT_1_013 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 13);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Start/stop");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Cyclically");
}

TEST(DPT_1_Test, DPT_1_014)
{
    KNX::DPT_1_014 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 14);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Fixed");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Calculated");
}

TEST(DPT_1_Test, DPT_1_015)
{
    KNX::DPT_1_015 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 15);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "no action (dummy)");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "reset command (trigger)");
}

TEST(DPT_1_Test, DPT_1_016)
{
    KNX::DPT_1_016 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 16);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "no action (dummy)");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "acknowledge command (trigger), e.g. for alarming");
}

TEST(DPT_1_Test, DPT_1_017)
{
    KNX::DPT_1_017 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 17);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "trigger");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "trigger");
}

TEST(DPT_1_Test, DPT_1_018)
{
    KNX::DPT_1_018 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 18);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "not occupied");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "occupied");
}

TEST(DPT_1_Test, DPT_1_019)
{
    KNX::DPT_1_019 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 19);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "closed");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "open");
}

TEST(DPT_1_Test, DPT_1_021)
{
    KNX::DPT_1_021 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 21);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "logical function OR");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "logical function AND");
}

TEST(DPT_1_Test, DPT_1_022)
{
    KNX::DPT_1_022 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 22);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "scene A");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "scene B");
}

TEST(DPT_1_Test, DPT_1_023)
{
    KNX::DPT_1_023 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 23);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "only move Up/Down mode (shutter)");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "move Up/Down + StepStop mode (blind)");
}

TEST(DPT_1_Test, DPT_1_024)
{
    KNX::DPT_1_024 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 24);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Day"); // @note ETS shows nothing.

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Night"); // @note ETS shows nothing.
}

TEST(DPT_1_Test, DPT_1_100)
{
    KNX::DPT_1_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 100);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "cooling");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "heating");
}

TEST(DPT_1_Test, DPT_1_1200)
{
    KNX::DPT_1_1200 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 1200);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data)); // @note ETS shows nothing.
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Consumer");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Producer");
}

TEST(DPT_1_Test, DPT_1_1201)
{
    KNX::DPT_1_1201 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 1);
    ASSERT_EQ(dpt.subnumber, 1201);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data)); // @note ETS shows nothing.
    ASSERT_EQ(dpt.b, false);
    ASSERT_EQ(dpt.text(), "Positive");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.b, true);
    ASSERT_EQ(dpt.text(), "Negative");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
