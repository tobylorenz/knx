// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_201_Test : public ::testing::Test
{
    virtual ~DPT_201_Test() = default;
};

TEST(DPT_201_Test, DPT_201_100)
{
    KNX::DPT_201_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 201);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_201_100
}

TEST(DPT_201_Test, DPT_201_102)
{
    KNX::DPT_201_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 201);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_201_102
}

TEST(DPT_201_Test, DPT_201_104)
{
    KNX::DPT_201_104 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 201);
    ASSERT_EQ(dpt.subnumber, 104);

    // @todo DPT_201_104
}

TEST(DPT_201_Test, DPT_201_105)
{
    KNX::DPT_201_105 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 201);
    ASSERT_EQ(dpt.subnumber, 105);

    // @todo DPT_201_105
}

TEST(DPT_201_Test, DPT_201_107)
{
    KNX::DPT_201_107 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 201);
    ASSERT_EQ(dpt.subnumber, 107);

    // @todo DPT_201_107
}

TEST(DPT_201_Test, DPT_201_108)
{
    KNX::DPT_201_108 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 201);
    ASSERT_EQ(dpt.subnumber, 108);

    // @todo DPT_201_108
}

TEST(DPT_201_Test, DPT_201_109)
{
    KNX::DPT_201_109 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 201);
    ASSERT_EQ(dpt.subnumber, 109);

    // @todo DPT_201_109
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
