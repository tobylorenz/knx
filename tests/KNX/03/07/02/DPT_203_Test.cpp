// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_203_Test : public ::testing::Test
{
    virtual ~DPT_203_Test() = default;
};

TEST(DPT_203_Test, DPT_203_002)
{
    KNX::DPT_203_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 2);

    // @todo DPT_203_002
}

TEST(DPT_203_Test, DPT_203_003)
{
    KNX::DPT_203_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 3);

    // @todo DPT_203_003
}

TEST(DPT_203_Test, DPT_203_004)
{
    KNX::DPT_203_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 4);

    // @todo DPT_203_004
}

TEST(DPT_203_Test, DPT_203_005)
{
    KNX::DPT_203_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 5);

    // @todo DPT_203_005
}

TEST(DPT_203_Test, DPT_203_006)
{
    KNX::DPT_203_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 6);

    // @todo DPT_203_006
}

TEST(DPT_203_Test, DPT_203_007)
{
    KNX::DPT_203_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 7);

    // @todo DPT_203_007
}

TEST(DPT_203_Test, DPT_203_011)
{
    KNX::DPT_203_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 11);

    // @todo DPT_203_011
}

TEST(DPT_203_Test, DPT_203_012)
{
    KNX::DPT_203_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 12);

    // @todo DPT_203_012
}

TEST(DPT_203_Test, DPT_203_013)
{
    KNX::DPT_203_013 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 13);

    // @todo DPT_203_013
}

TEST(DPT_203_Test, DPT_203_014)
{
    KNX::DPT_203_014 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 14);

    // @todo DPT_203_014
}

TEST(DPT_203_Test, DPT_203_015)
{
    KNX::DPT_203_015 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 15);

    // @todo DPT_203_015
}

TEST(DPT_203_Test, DPT_203_017)
{
    KNX::DPT_203_017 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 17);

    // @todo DPT_203_017
}

TEST(DPT_203_Test, DPT_203_100)
{
    KNX::DPT_203_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_203_100
}

TEST(DPT_203_Test, DPT_203_101)
{
    KNX::DPT_203_101 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 101);

    // @todo DPT_203_101
}

TEST(DPT_203_Test, DPT_203_102)
{
    KNX::DPT_203_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_203_102
}

TEST(DPT_203_Test, DPT_203_104)
{
    KNX::DPT_203_104 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 203);
    ASSERT_EQ(dpt.subnumber, 104);

    // @todo DPT_203_104
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
