// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_205_Test : public ::testing::Test
{
    virtual ~DPT_205_Test() = default;
};

TEST(DPT_205_Test, DPT_205_002)
{
    KNX::DPT_205_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 2);

    // @todo DPT_205_002
}

TEST(DPT_205_Test, DPT_205_003)
{
    KNX::DPT_205_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 3);

    // @todo DPT_205_003
}

TEST(DPT_205_Test, DPT_205_004)
{
    KNX::DPT_205_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 4);

    // @todo DPT_205_004
}

TEST(DPT_205_Test, DPT_205_005)
{
    KNX::DPT_205_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 5);

    // @todo DPT_205_005
}

TEST(DPT_205_Test, DPT_205_006)
{
    KNX::DPT_205_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 6);

    // @todo DPT_205_006
}

TEST(DPT_205_Test, DPT_205_007)
{
    KNX::DPT_205_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 7);

    // @todo DPT_205_007
}

TEST(DPT_205_Test, DPT_205_017)
{
    KNX::DPT_205_017 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 17);

    // @todo DPT_205_017
}

TEST(DPT_205_Test, DPT_205_100)
{
    KNX::DPT_205_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_205_100
}

TEST(DPT_205_Test, DPT_205_101)
{
    KNX::DPT_205_101 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 101);

    // @todo DPT_205_101
}

TEST(DPT_205_Test, DPT_205_102)
{
    KNX::DPT_205_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_205_102
}

TEST(DPT_205_Test, DPT_205_103)
{
    KNX::DPT_205_103 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 205);
    ASSERT_EQ(dpt.subnumber, 103);

    // @todo DPT_205_103
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
