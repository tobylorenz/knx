// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_206_Test : public ::testing::Test
{
    virtual ~DPT_206_Test() = default;
};

TEST(DPT_206_Test, DPT_206_100)
{
    KNX::DPT_206_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 206);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_206_100
}

TEST(DPT_206_Test, DPT_206_102)
{
    KNX::DPT_206_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 206);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_206_102
}

TEST(DPT_206_Test, DPT_206_104)
{
    KNX::DPT_206_104 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 206);
    ASSERT_EQ(dpt.subnumber, 104);

    // @todo DPT_206_104
}

TEST(DPT_206_Test, DPT_206_105)
{
    KNX::DPT_206_105 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 206);
    ASSERT_EQ(dpt.subnumber, 105);

    // @todo DPT_206_105
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
