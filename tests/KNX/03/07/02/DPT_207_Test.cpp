// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_207_Test : public ::testing::Test
{
    virtual ~DPT_207_Test() = default;
};

TEST(DPT_207_Test, DPT_207_100)
{
    KNX::DPT_207_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 207);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_207_100
}

TEST(DPT_207_Test, DPT_207_101)
{
    KNX::DPT_207_101 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 207);
    ASSERT_EQ(dpt.subnumber, 101);

    // @todo DPT_207_101
}

TEST(DPT_207_Test, DPT_207_102)
{
    KNX::DPT_207_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 207);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_207_102
}

TEST(DPT_207_Test, DPT_207_104)
{
    KNX::DPT_207_104 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 207);
    ASSERT_EQ(dpt.subnumber, 104);

    // @todo DPT_207_104
}

TEST(DPT_207_Test, DPT_207_105)
{
    KNX::DPT_207_105 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 207);
    ASSERT_EQ(dpt.subnumber, 105);

    // @todo DPT_207_105
}

TEST(DPT_207_Test, DPT_207_600)
{
    KNX::DPT_207_600 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 207);
    ASSERT_EQ(dpt.subnumber, 600);

    // @todo DPT_207_600
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
