// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_209_Test : public ::testing::Test
{
    virtual ~DPT_209_Test() = default;
};

TEST(DPT_209_Test, DPT_209_100)
{
    DPT_209_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 209);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_209_100
}

TEST(DPT_209_Test, DPT_209_101)
{
    DPT_209_101 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 209);
    ASSERT_EQ(dpt.subnumber, 101);

    // @todo DPT_209_101
}

TEST(DPT_209_Test, DPT_209_102)
{
    DPT_209_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 209);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_209_102
}

TEST(DPT_209_Test, DPT_209_103)
{
    DPT_209_103 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 209);
    ASSERT_EQ(dpt.subnumber, 103);

    // @todo DPT_209_103
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
