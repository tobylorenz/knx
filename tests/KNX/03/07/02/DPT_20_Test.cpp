// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_20_Test : public ::testing::Test
{
    virtual ~DPT_20_Test() = default;
};

TEST(DPT_20_Test, DPT_20_001)
{
    KNX::DPT_20_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 0);
    ASSERT_EQ(dpt.text(), "autonomous");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 1);
    ASSERT_EQ(dpt.text(), "slave");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 2);
    ASSERT_EQ(dpt.text(), "master");

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 255);
    ASSERT_EQ(dpt.text(), "reserved");
}

TEST(DPT_20_Test, DPT_20_002)
{
    KNX::DPT_20_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 2);

    // @todo DPT_20_002
}

TEST(DPT_20_Test, DPT_20_003)
{
    KNX::DPT_20_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 3);

    // @todo DPT_20_003
}

TEST(DPT_20_Test, DPT_20_004)
{
    KNX::DPT_20_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 4);

    // @todo DPT_20_004
}

TEST(DPT_20_Test, DPT_20_005)
{
    KNX::DPT_20_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 5);

    // @todo DPT_20_005
}

TEST(DPT_20_Test, DPT_20_006)
{
    KNX::DPT_20_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 6);

    // @todo DPT_20_006
}

TEST(DPT_20_Test, DPT_20_007)
{
    KNX::DPT_20_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 7);

    // @todo DPT_20_007
}

TEST(DPT_20_Test, DPT_20_008)
{
    KNX::DPT_20_008 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 8);

    // @todo DPT_20_008
}

TEST(DPT_20_Test, DPT_20_011)
{
    KNX::DPT_20_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 11);

    // @todo DPT_20_011
}

TEST(DPT_20_Test, DPT_20_012)
{
    KNX::DPT_20_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 12);

    // @todo DPT_20_012
}

TEST(DPT_20_Test, DPT_20_013)
{
    KNX::DPT_20_013 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 13);

    // @todo DPT_20_013
}

TEST(DPT_20_Test, DPT_20_014)
{
    KNX::DPT_20_014 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 14);

    // @todo DPT_20_014
}

TEST(DPT_20_Test, DPT_20_017)
{
    KNX::DPT_20_017 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 17);

    // @todo DPT_20_017
}

TEST(DPT_20_Test, DPT_20_020)
{
    KNX::DPT_20_020 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 20);

    // @todo DPT_20_020
}

TEST(DPT_20_Test, DPT_20_021)
{
    KNX::DPT_20_021 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 21);

    // @todo DPT_20_021
}

TEST(DPT_20_Test, DPT_20_022)
{
    KNX::DPT_20_022 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 22);

    // @todo DPT_20_022
}

TEST(DPT_20_Test, DPT_20_100)
{
    KNX::DPT_20_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_20_100
}

TEST(DPT_20_Test, DPT_20_101)
{
    KNX::DPT_20_101 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 101);

    // @todo DPT_20_101
}

TEST(DPT_20_Test, DPT_20_102)
{
    KNX::DPT_20_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_20_102
}

TEST(DPT_20_Test, DPT_20_103)
{
    KNX::DPT_20_103 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 103);

    // @todo DPT_20_103
}

TEST(DPT_20_Test, DPT_20_104)
{
    KNX::DPT_20_104 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 104);

    // @todo DPT_20_104
}

TEST(DPT_20_Test, DPT_20_105)
{
    KNX::DPT_20_105 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 105);

    // @todo DPT_20_105
}

TEST(DPT_20_Test, DPT_20_106)
{
    KNX::DPT_20_106 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 106);

    // @todo DPT_20_106
}

TEST(DPT_20_Test, DPT_20_107)
{
    KNX::DPT_20_107 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 107);

    // @todo DPT_20_107
}

TEST(DPT_20_Test, DPT_20_108)
{
    KNX::DPT_20_108 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 108);

    // @todo DPT_20_108
}

TEST(DPT_20_Test, DPT_20_109)
{
    KNX::DPT_20_109 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 109);

    // @todo DPT_20_109
}

TEST(DPT_20_Test, DPT_20_110)
{
    KNX::DPT_20_110 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 110);

    // @todo DPT_20_110
}

TEST(DPT_20_Test, DPT_20_111)
{
    KNX::DPT_20_111 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 111);

    // @todo DPT_20_111
}

TEST(DPT_20_Test, DPT_20_112)
{
    KNX::DPT_20_112 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 112);

    // @todo DPT_20_112
}

TEST(DPT_20_Test, DPT_20_113)
{
    KNX::DPT_20_113 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 113);

    // @todo DPT_20_113
}

TEST(DPT_20_Test, DPT_20_114)
{
    KNX::DPT_20_114 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 114);

    // @todo DPT_20_114
}

TEST(DPT_20_Test, DPT_20_115)
{
    KNX::DPT_20_115 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 115);

    // @todo DPT_20_115
}

TEST(DPT_20_Test, DPT_20_116)
{
    KNX::DPT_20_116 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 116);

    // @todo DPT_20_116
}

TEST(DPT_20_Test, DPT_20_120)
{
    KNX::DPT_20_120 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 120);

    // @todo DPT_20_120
}

TEST(DPT_20_Test, DPT_20_121)
{
    KNX::DPT_20_121 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 121);

    // @todo DPT_20_121
}

TEST(DPT_20_Test, DPT_20_122)
{
    KNX::DPT_20_122 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 122);

    // @todo DPT_20_122
}

TEST(DPT_20_Test, DPT_20_600)
{
    KNX::DPT_20_600 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 600);

    // @todo DPT_20_600
}

TEST(DPT_20_Test, DPT_20_601)
{
    KNX::DPT_20_601 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 601);

    // @todo DPT_20_601
}

TEST(DPT_20_Test, DPT_20_602)
{
    KNX::DPT_20_602 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 602);

    // @todo DPT_20_602
}

TEST(DPT_20_Test, DPT_20_603)
{
    KNX::DPT_20_603 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 603);

    // @todo DPT_20_603
}

TEST(DPT_20_Test, DPT_20_604)
{
    KNX::DPT_20_604 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 604);

    // @todo DPT_20_604
}

TEST(DPT_20_Test, DPT_20_605)
{
    KNX::DPT_20_605 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 605);

    // @todo DPT_20_605
}

TEST(DPT_20_Test, DPT_20_606)
{
    KNX::DPT_20_606 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 606);

    // @todo DPT_20_606
}

TEST(DPT_20_Test, DPT_20_607)
{
    KNX::DPT_20_607 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 607);

    // @todo DPT_20_607
}

TEST(DPT_20_Test, DPT_20_608)
{
    KNX::DPT_20_608 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 608);

    // @todo DPT_20_608
}

TEST(DPT_20_Test, DPT_20_609)
{
    KNX::DPT_20_609 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 609);

    // @todo DPT_20_609
}

TEST(DPT_20_Test, DPT_20_610)
{
    KNX::DPT_20_610 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 610);

    // @todo DPT_20_610
}

TEST(DPT_20_Test, DPT_20_611)
{
    KNX::DPT_20_611 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 611);

    // @todo DPT_20_611
}

TEST(DPT_20_Test, DPT_20_612)
{
    KNX::DPT_20_612 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 612);

    // @todo DPT_20_612
}

TEST(DPT_20_Test, DPT_20_613)
{
    KNX::DPT_20_613 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 613);

    // @todo DPT_20_613
}

TEST(DPT_20_Test, DPT_20_801)
{
    KNX::DPT_20_801 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 801);

    // @todo DPT_20_801
}

TEST(DPT_20_Test, DPT_20_802)
{
    KNX::DPT_20_802 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 802);

    // @todo DPT_20_802
}

TEST(DPT_20_Test, DPT_20_803)
{
    KNX::DPT_20_803 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 803);

    // @todo DPT_20_803
}

TEST(DPT_20_Test, DPT_20_804)
{
    KNX::DPT_20_804 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 804);

    // @todo DPT_20_804
}

TEST(DPT_20_Test, DPT_20_1000)
{
    KNX::DPT_20_1000 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1000);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 0);
    ASSERT_EQ(dpt.text(), "Data Link Layer");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 1);
    ASSERT_EQ(dpt.text(), "Data Link Layer Busmonitor");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 2);
    ASSERT_EQ(dpt.text(), "Data Link Layer Raw Frames");

    data = {0x06};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 6);
    ASSERT_EQ(dpt.text(), "cEMI Transport Layer");

    data = {0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 254);
    ASSERT_EQ(dpt.text(), "reserved");

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.field1, 255);
    ASSERT_EQ(dpt.text(), "No Layer");
}

TEST(DPT_20_Test, DPT_20_1001)
{
    KNX::DPT_20_1001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1001);

    // @todo DPT_20_1001
}

TEST(DPT_20_Test, DPT_20_1002)
{
    KNX::DPT_20_1002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1002);

    // @todo DPT_20_1002
}

TEST(DPT_20_Test, DPT_20_1003)
{
    KNX::DPT_20_1003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1003);

    // @todo DPT_20_1003
}

TEST(DPT_20_Test, DPT_20_1004)
{
    KNX::DPT_20_1004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1004);

    // @todo DPT_20_1004
}

TEST(DPT_20_Test, DPT_20_1005)
{
    KNX::DPT_20_1005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1005);

    // @todo DPT_20_1005
}

TEST(DPT_20_Test, DPT_20_1200)
{
    KNX::DPT_20_1200 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1200);

    // @todo DPT_20_1200
}

TEST(DPT_20_Test, DPT_20_1202)
{
    KNX::DPT_20_1202 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1202);

    // @todo DPT_20_1202
}

TEST(DPT_20_Test, DPT_20_1203)
{
    KNX::DPT_20_1203 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1203);

    // @todo DPT_20_1203
}

TEST(DPT_20_Test, DPT_20_1204)
{
    KNX::DPT_20_1204 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1204);

    // @todo DPT_20_1204
}

TEST(DPT_20_Test, DPT_20_1205)
{
    KNX::DPT_20_1205 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1205);

    // @todo DPT_20_1205
}

TEST(DPT_20_Test, DPT_20_1206)
{
    KNX::DPT_20_1206 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1206);

    // @todo DPT_20_1206
}

TEST(DPT_20_Test, DPT_20_1207)
{
    KNX::DPT_20_1207 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1207);

    // @todo DPT_20_1207
}

TEST(DPT_20_Test, DPT_20_1208)
{
    KNX::DPT_20_1208 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1208);

    // @todo DPT_20_1208
}

TEST(DPT_20_Test, DPT_20_1209)
{
    KNX::DPT_20_1209 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 20);
    ASSERT_EQ(dpt.subnumber, 1209);

    // @todo DPT_20_1209
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
