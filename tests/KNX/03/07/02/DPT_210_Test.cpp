// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_210_Test : public ::testing::Test
{
    virtual ~DPT_210_Test() = default;
};

TEST(DPT_210_Test, DPT_210_100)
{
    KNX::DPT_210_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 210);
    ASSERT_EQ(dpt.subnumber, 100);

    data = {0x00, 0x00, 0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.value, 0);
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.attributes[4], false);
    ASSERT_EQ(dpt.attributes[5], false);
    ASSERT_EQ(dpt.attributes[6], false);
    ASSERT_EQ(dpt.attributes[7], false);
    ASSERT_EQ(dpt.attributes[8], false);
    ASSERT_EQ(dpt.attributes[9], false);
    ASSERT_EQ(dpt.attributes[10], false);
    ASSERT_EQ(dpt.attributes[11], false);
    // @todo ASSERT_EQ(dpt.text(), "reserved");

    data = {0xff, 0xff, 0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.value, -1);
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.attributes[4], true);
    ASSERT_EQ(dpt.attributes[5], true);
    ASSERT_EQ(dpt.attributes[6], true);
    ASSERT_EQ(dpt.attributes[7], true);
    ASSERT_EQ(dpt.attributes[8], true);
    ASSERT_EQ(dpt.attributes[9], true);
    ASSERT_EQ(dpt.attributes[10], true);
    ASSERT_EQ(dpt.attributes[11], true);
    // @todo ASSERT_EQ(dpt.text(), "reserved");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
