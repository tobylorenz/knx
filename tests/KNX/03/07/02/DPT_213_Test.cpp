// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_213_Test : public ::testing::Test
{
    virtual ~DPT_213_Test() = default;
};

TEST(DPT_213_Test, DPT_213_100)
{
    KNX::DPT_213_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 213);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_213_100
}

TEST(DPT_213_Test, DPT_213_101)
{
    KNX::DPT_213_101 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 213);
    ASSERT_EQ(dpt.subnumber, 101);

    // @todo DPT_213_101
}

TEST(DPT_213_Test, DPT_213_102)
{
    KNX::DPT_213_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 213);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_213_102
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
