// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_218_Test : public ::testing::Test
{
    virtual ~DPT_218_Test() = default;
};

TEST(DPT_218_Test, DPT_218_001)
{
    KNX::DPT_218_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 218);
    ASSERT_EQ(dpt.subnumber, 1);

    // @todo DPT_218_001
}

TEST(DPT_218_Test, DPT_218_002)
{
    KNX::DPT_218_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 218);
    ASSERT_EQ(dpt.subnumber, 2);

    // @todo DPT_218_002
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
