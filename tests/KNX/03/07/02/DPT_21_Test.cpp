// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_21_Test : public ::testing::Test
{
    virtual ~DPT_21_Test() = default;
};

TEST(DPT_21_Test, DPT_21_001)
{
    KNX::DPT_21_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.attributes[4], false);
    ASSERT_EQ(dpt.text(), "OutOfService: false, Fault: false, Overridden: false, InAlarm: false, AlarmUnAck: acknowledged"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.attributes[4], true);
    ASSERT_EQ(dpt.text(), "OutOfService: true, Fault: true, Overridden: true, InAlarm: true, AlarmUnAck: unacknowledged");
}

TEST(DPT_21_Test, DPT_21_002)
{
    KNX::DPT_21_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 2);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.text(), "UserStopped: false, OwnIA: false, VerifyMode: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.text(), "UserStopped: true, OwnIA: true, VerifyMode: true");
}

TEST(DPT_21_Test, DPT_21_100)
{
    KNX::DPT_21_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 100);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.attributes[4], false);
    ASSERT_EQ(dpt.attributes[5], false);
    ASSERT_EQ(dpt.attributes[6], false);
    ASSERT_EQ(dpt.attributes[7], false);
    ASSERT_EQ(dpt.text(), "ForceRequest: false, Protection: false, Oversupply: false, Overrun: false, DHWNorm: false, DHWLegio: false, RoomHConf: false, RoomHMax: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.attributes[4], true);
    ASSERT_EQ(dpt.attributes[5], true);
    ASSERT_EQ(dpt.attributes[6], true);
    ASSERT_EQ(dpt.attributes[7], true);
    ASSERT_EQ(dpt.text(), "ForceRequest: true, Protection: true, Oversupply: true, Overrun: true, DHWNorm: true, DHWLegio: true, RoomHConf: true, RoomHMax: true");
}

TEST(DPT_21_Test, DPT_21_101)
{
    KNX::DPT_21_101 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 101);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.text(), "ForceRequest: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.text(), "ForceRequest: true");
}

TEST(DPT_21_Test, DPT_21_102)
{
    KNX::DPT_21_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 102);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.attributes[4], false);
    ASSERT_EQ(dpt.attributes[5], false);
    ASSERT_EQ(dpt.attributes[6], false);
    ASSERT_EQ(dpt.attributes[7], false);
    ASSERT_EQ(dpt.text(), "Fault: false, StatusECO: false, TempFlowLimit: false, TempReturnLimit: false, StatusMorningBoost: false, StatusStartOptim: false, StatusStopOptim: false, SummerMode: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.attributes[4], true);
    ASSERT_EQ(dpt.attributes[5], true);
    ASSERT_EQ(dpt.attributes[6], true);
    ASSERT_EQ(dpt.attributes[7], true);
    ASSERT_EQ(dpt.text(), "Fault: true, StatusECO: true, TempFlowLimit: true, TempReturnLimit: true, StatusMorningBoost: true, StatusStartOptim: true, StatusStopOptim: true, SummerMode: true");
}

TEST(DPT_21_Test, DPT_21_103)
{
    KNX::DPT_21_103 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 103);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.text(), "Fault: ok, SDHWLoadActive: false, SolarLoadSufficient: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.text(), "Fault: fault, SDHWLoadActive: true, SolarLoadSufficient: true");
}

TEST(DPT_21_Test, DPT_21_104)
{
    KNX::DPT_21_104 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 104);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.text(), "Oil: false, Gas: false, SolidState: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.text(), "Oil: true, Gas: true, SolidState: true");
}

TEST(DPT_21_Test, DPT_21_105)
{
    KNX::DPT_21_105 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 105);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.text(), "Fault: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.text(), "Fault: true");
}

TEST(DPT_21_Test, DPT_21_106)
{
    KNX::DPT_21_106 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 106);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.text(), "Fault: false, FanActive: false, Heat: false, Cool: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.text(), "Fault: true, FanActive: true, Heat: true, Cool: true");
}

TEST(DPT_21_Test, DPT_21_107)
{
    KNX::DPT_21_107 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 107);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.attributes[4], false);
    ASSERT_EQ(dpt.text(), "Effective value of the window status: false, Effective value of the presence status: false, Effective value of the comfort pus button: false, Status of comfort prolongation User: false, Status of HVAC Mode User: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.attributes[4], true);
    ASSERT_EQ(dpt.text(), "Effective value of the window status: true, Effective value of the presence status: true, Effective value of the comfort pus button: true, Status of comfort prolongation User: true, Status of HVAC Mode User: true");
}

TEST(DPT_21_Test, DPT_21_601)
{
    KNX::DPT_21_601 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 601);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.attributes[4], false);
    ASSERT_EQ(dpt.attributes[5], false);
    ASSERT_EQ(dpt.attributes[6], false);
    ASSERT_EQ(dpt.text(), "LoadDetectionError: false, Undervoltage: false, Overcurrent: false, Underload: false, DefectiveLoad: false, LampFailure: false, Overheat: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.attributes[4], true);
    ASSERT_EQ(dpt.attributes[5], true);
    ASSERT_EQ(dpt.attributes[6], true);
    ASSERT_EQ(dpt.text(), "LoadDetectionError: true, Undervoltage: true, Overcurrent: true, Underload: true, DefectiveLoad: true, LampFailure: true, Overheat: true");
}

TEST(DPT_21_Test, DPT_21_1000)
{
    KNX::DPT_21_1000 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 1000);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.text(), "Asynchronous: false, BiBat Master: false, BiBat Slave: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.text(), "Asynchronous: true, BiBat Master: true, BiBat Slave: true");
}

TEST(DPT_21_Test, DPT_21_1001)
{
    KNX::DPT_21_1001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 1001);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.text(), "DoA: false, KNX SN: false, DoA and KNX SN: false"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.text(), "DoA: true, KNX SN: true, DoA and KNX SN: true");
}

TEST(DPT_21_Test, DPT_21_1002)
{
    KNX::DPT_21_1002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 1002);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.text(), "Security Failure: There is no Security Failure."); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.text(), "Security Failure: There is a Security Failure.");
}

TEST(DPT_21_Test, DPT_21_1010)
{
    KNX::DPT_21_1010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 1010);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.attributes[4], false);
    ASSERT_EQ(dpt.attributes[5], false);
    ASSERT_EQ(dpt.attributes[6], false);
    ASSERT_EQ(dpt.attributes[7], false);
    ASSERT_EQ(dpt.text(),
              "The visual effect of channel 1 is inactive. "
              "The visual effect of channel 2 is inactive. "
              "The visual effect of channel 3 is inactive. "
              "The visual effect of channel 4 is inactive. "
              "The visual effect of channel 5 is inactive. "
              "The visual effect of channel 6 is inactive. "
              "The visual effect of channel 7 is inactive. "
              "The visual effect of channel 8 is inactive."); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.attributes[4], true);
    ASSERT_EQ(dpt.attributes[5], true);
    ASSERT_EQ(dpt.attributes[6], true);
    ASSERT_EQ(dpt.attributes[7], true);
    ASSERT_EQ(dpt.text(),
              "The visual effect of channel 1 is active. "
              "The visual effect of channel 2 is active. "
              "The visual effect of channel 3 is active. "
              "The visual effect of channel 4 is active. "
              "The visual effect of channel 5 is active. "
              "The visual effect of channel 6 is active. "
              "The visual effect of channel 7 is active. "
              "The visual effect of channel 8 is active.");
}

TEST(DPT_21_Test, DPT_21_1200)
{
    KNX::DPT_21_1200 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 1200);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.attributes[4], false);
    ASSERT_EQ(dpt.attributes[5], false);
    ASSERT_EQ(dpt.attributes[6], false);
    ASSERT_EQ(dpt.attributes[7], false);
    ASSERT_EQ(dpt.text(),
              "Status of Virtual Dry Contact 1: Virtual contact open, "
              "Status of Virtual Dry Contact 2: Virtual contact open, "
              "Status of Virtual Dry Contact 3: Virtual contact open, "
              "Status of Virtual Dry Contact 4: Virtual contact open, "
              "Status of Virtual Dry Contact 5: Virtual contact open, "
              "Status of Virtual Dry Contact 6: Virtual contact open, "
              "Status of Virtual Dry Contact 7: Virtual contact open, "
              "Status of Virtual Dry Contact 8: Virtual contact open"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.attributes[4], true);
    ASSERT_EQ(dpt.attributes[5], true);
    ASSERT_EQ(dpt.attributes[6], true);
    ASSERT_EQ(dpt.attributes[7], true);
    ASSERT_EQ(dpt.text(),
              "Status of Virtual Dry Contact 1: Virtual contact closed, "
              "Status of Virtual Dry Contact 2: Virtual contact closed, "
              "Status of Virtual Dry Contact 3: Virtual contact closed, "
              "Status of Virtual Dry Contact 4: Virtual contact closed, "
              "Status of Virtual Dry Contact 5: Virtual contact closed, "
              "Status of Virtual Dry Contact 6: Virtual contact closed, "
              "Status of Virtual Dry Contact 7: Virtual contact closed, "
              "Status of Virtual Dry Contact 8: Virtual contact closed"); // @note ETS shows nothing.
}

TEST(DPT_21_Test, DPT_21_1201)
{
    KNX::DPT_21_1201 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 21);
    ASSERT_EQ(dpt.subnumber, 1201);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], false);
    ASSERT_EQ(dpt.attributes[1], false);
    ASSERT_EQ(dpt.attributes[2], false);
    ASSERT_EQ(dpt.attributes[3], false);
    ASSERT_EQ(dpt.attributes[4], false);
    ASSERT_EQ(dpt.attributes[5], false);
    ASSERT_EQ(dpt.attributes[6], false);
    ASSERT_EQ(dpt.attributes[7], false);
    ASSERT_EQ(dpt.text(),
              "Status of Phase 1: Phase absent, "
              "Status of Phase 2: Phase absent, "
              "Status of Phase 3: Phase absent, "
              "Status of Phase 4: Phase absent, "
              "Status of Phase 5: Phase absent, "
              "Status of Phase 6: Phase absent, "
              "Status of Phase 7: Phase absent, "
              "Status of Phase 8: Phase absent"); // @note ETS shows nothing.

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.attributes[0], true);
    ASSERT_EQ(dpt.attributes[1], true);
    ASSERT_EQ(dpt.attributes[2], true);
    ASSERT_EQ(dpt.attributes[3], true);
    ASSERT_EQ(dpt.attributes[4], true);
    ASSERT_EQ(dpt.attributes[5], true);
    ASSERT_EQ(dpt.attributes[6], true);
    ASSERT_EQ(dpt.attributes[7], true);
    ASSERT_EQ(dpt.text(),
              "Status of Phase 1: Phase present, "
              "Status of Phase 2: Phase present, "
              "Status of Phase 3: Phase present, "
              "Status of Phase 4: Phase present, "
              "Status of Phase 5: Phase present, "
              "Status of Phase 6: Phase present, "
              "Status of Phase 7: Phase present, "
              "Status of Phase 8: Phase present"); // @note ETS shows nothing.
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
