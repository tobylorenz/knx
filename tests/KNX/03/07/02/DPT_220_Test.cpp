// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_220_Test : public ::testing::Test
{
    virtual ~DPT_220_Test() = default;
};

TEST(DPT_220_Test, DPT_220_100)
{
    KNX::DPT_220_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 220);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_220_100
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
