// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_225_Test : public ::testing::Test
{
    virtual ~DPT_225_Test() = default;
};

TEST(DPT_225_Test, DPT_225_001)
{
    KNX::DPT_225_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 225);
    ASSERT_EQ(dpt.subnumber, 1);

    // @todo DPT_225_001
}

TEST(DPT_225_Test, DPT_225_002)
{
    KNX::DPT_225_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 225);
    ASSERT_EQ(dpt.subnumber, 2);

    // @todo DPT_225_002
}

TEST(DPT_225_Test, DPT_225_003)
{
    KNX::DPT_225_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 225);
    ASSERT_EQ(dpt.subnumber, 3);

    // @todo DPT_225_003
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
