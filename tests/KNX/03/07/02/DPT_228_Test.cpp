// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_228_Test : public ::testing::Test
{
    virtual ~DPT_228_Test() = default;
};

TEST(DPT_228_Test, DPT_228_1000)
{
    KNX::DPT_228_1000 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 228);
    ASSERT_EQ(dpt.subnumber, 1000);

    // @todo DPT_228_1000
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
