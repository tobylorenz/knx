// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_22_Test : public ::testing::Test
{
    virtual ~DPT_22_Test() = default;
};

TEST(DPT_22_Test, DPT_22_100)
{
    KNX::DPT_22_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 22);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_22_100
}

TEST(DPT_22_Test, DPT_22_101)
{
    KNX::DPT_22_101 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 22);
    ASSERT_EQ(dpt.subnumber, 101);

    // @todo DPT_22_101
}

TEST(DPT_22_Test, DPT_22_102)
{
    KNX::DPT_22_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 22);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_22_102
}

TEST(DPT_22_Test, DPT_22_103)
{
    KNX::DPT_22_103 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 22);
    ASSERT_EQ(dpt.subnumber, 103);

    // @todo DPT_22_103
}

TEST(DPT_22_Test, DPT_22_1000)
{
    KNX::DPT_22_1000 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 22);
    ASSERT_EQ(dpt.subnumber, 1000);

    // @todo DPT_22_1000
}

TEST(DPT_22_Test, DPT_22_1010)
{
    KNX::DPT_22_1010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 22);
    ASSERT_EQ(dpt.subnumber, 1010);

    // @todo DPT_22_1010
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
