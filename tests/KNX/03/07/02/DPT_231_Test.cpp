// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_231_Test : public ::testing::Test
{
    virtual ~DPT_231_Test() = default;
};

TEST(DPT_231_Test, DPT_231_001)
{
    KNX::DPT_231_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 231);
    ASSERT_EQ(dpt.subnumber, 1);

    // @todo DPT_231_001
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
