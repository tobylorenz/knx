// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_235_Test : public ::testing::Test
{
    virtual ~DPT_235_Test() = default;
};

TEST(DPT_235_Test, DPT_235_001)
{
    KNX::DPT_235_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 235);
    ASSERT_EQ(dpt.subnumber, 1);

    // @todo DPT_235_001
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
