// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_23_Test : public ::testing::Test
{
    virtual ~DPT_23_Test() = default;
};

TEST(DPT_23_Test, DPT_23_001)
{
    KNX::DPT_23_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 23);
    ASSERT_EQ(dpt.subnumber, 1);

    // @todo DPT_23_001
}

TEST(DPT_23_Test, DPT_23_002)
{
    KNX::DPT_23_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 23);
    ASSERT_EQ(dpt.subnumber, 2);

    // @todo DPT_23_002
}

TEST(DPT_23_Test, DPT_23_003)
{
    KNX::DPT_23_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 23);
    ASSERT_EQ(dpt.subnumber, 3);

    // @todo DPT_23_003
}

TEST(DPT_23_Test, DPT_23_102)
{
    KNX::DPT_23_102 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 23);
    ASSERT_EQ(dpt.subnumber, 102);

    // @todo DPT_23_102
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
