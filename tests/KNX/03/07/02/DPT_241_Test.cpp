// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_241_Test : public ::testing::Test
{
    virtual ~DPT_241_Test() = default;
};

TEST(DPT_241_Test, DPT_241_800)
{
    KNX::DPT_241_800 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 241);
    ASSERT_EQ(dpt.subnumber, 800);

    // @todo DPT_241_800
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
