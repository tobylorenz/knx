// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_250_Test : public ::testing::Test
{
    virtual ~DPT_250_Test() = default;
};

TEST(DPT_250_Test, DPT_250_600)
{
    KNX::DPT_250_600 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 250);
    ASSERT_EQ(dpt.subnumber, 600);

    // @todo DPT_250_600
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
