// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_251_Test : public ::testing::Test
{
    virtual ~DPT_251_Test() = default;
};

TEST(DPT_251_Test, DPT_251_600)
{
    KNX::DPT_251_600 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 251);
    ASSERT_EQ(dpt.subnumber, 600);

    // @todo DPT_251_600
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
