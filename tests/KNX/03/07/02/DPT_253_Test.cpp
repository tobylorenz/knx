// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_253_Test : public ::testing::Test
{
    virtual ~DPT_253_Test() = default;
};

TEST(DPT_253_Test, DPT_253_600)
{
    KNX::DPT_253_600 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 253);
    ASSERT_EQ(dpt.subnumber, 600);

    // @todo DPT_253_600
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
