// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_254_Test : public ::testing::Test
{
    virtual ~DPT_254_Test() = default;
};

TEST(DPT_254_Test, DPT_254_600)
{
    KNX::DPT_254_600 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 254);
    ASSERT_EQ(dpt.subnumber, 600);

    // @todo DPT_254_600
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
