// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_255_Test : public ::testing::Test
{
    virtual ~DPT_255_Test() = default;
};

TEST(DPT_255_Test, DPT_255_001)
{
    KNX::DPT_255_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 255);
    ASSERT_EQ(dpt.subnumber, 1);

    // @todo DPT_255_001
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
