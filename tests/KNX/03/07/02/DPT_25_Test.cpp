// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_25_Test : public ::testing::Test
{
    virtual ~DPT_25_Test() = default;
};

TEST(DPT_25_Test, DPT_25_1000)
{
    KNX::DPT_25_1000 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 25);
    ASSERT_EQ(dpt.subnumber, 1000);

    // @todo DPT_25_1000
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
