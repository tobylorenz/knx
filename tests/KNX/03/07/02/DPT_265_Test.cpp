// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_265_Test : public ::testing::Test
{
    virtual ~DPT_265_Test() = default;
};

TEST(DPT_265_Test, DPT_265_001)
{
    KNX::DPT_265_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 265);
    ASSERT_EQ(dpt.subnumber, 1);

    // @todo DPT_265_001
}

TEST(DPT_265_Test, DPT_265_005)
{
    KNX::DPT_265_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 265);
    ASSERT_EQ(dpt.subnumber, 5);

    // @todo DPT_265_005
}

TEST(DPT_265_Test, DPT_265_009)
{
    KNX::DPT_265_009 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 265);
    ASSERT_EQ(dpt.subnumber, 9);

    // @todo DPT_265_009
}

TEST(DPT_265_Test, DPT_265_011)
{
    KNX::DPT_265_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 265);
    ASSERT_EQ(dpt.subnumber, 11);

    // @todo DPT_265_011
}

TEST(DPT_265_Test, DPT_265_012)
{
    KNX::DPT_265_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 265);
    ASSERT_EQ(dpt.subnumber, 12);

    // @todo DPT_265_012
}

TEST(DPT_265_Test, DPT_265_1200)
{
    KNX::DPT_265_1200 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 265);
    ASSERT_EQ(dpt.subnumber, 1200);

    // @todo DPT_265_1200
}

TEST(DPT_265_Test, DPT_265_1201)
{
    KNX::DPT_265_1201 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 265);
    ASSERT_EQ(dpt.subnumber, 1201);

    // @todo DPT_265_1201
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
