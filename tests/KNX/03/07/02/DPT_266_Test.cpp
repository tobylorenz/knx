// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_266_Test : public ::testing::Test
{
    virtual ~DPT_266_Test() = default;
};

TEST(DPT_266_Test, DPT_266_027)
{
    KNX::DPT_266_027 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 266);
    ASSERT_EQ(dpt.subnumber, 27);

    // @todo DPT_266_027
}

TEST(DPT_266_Test, DPT_266_056)
{
    KNX::DPT_266_056 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 266);
    ASSERT_EQ(dpt.subnumber, 56);

    // @todo DPT_266_056
}

TEST(DPT_266_Test, DPT_266_080)
{
    KNX::DPT_266_080 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 266);
    ASSERT_EQ(dpt.subnumber, 80);

    // @todo DPT_266_080
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
