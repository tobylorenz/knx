// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_268_Test : public ::testing::Test
{
    virtual ~DPT_268_Test() = default;
};

TEST(DPT_268_Test, DPT_268_1203)
{
    KNX::DPT_268_1203 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 268);
    ASSERT_EQ(dpt.subnumber, 1203);

    // @todo DPT_268_1203
}

TEST(DPT_268_Test, DPT_268_1204)
{
    KNX::DPT_268_1204 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 268);
    ASSERT_EQ(dpt.subnumber, 1204);

    // @todo DPT_268_1204
}

TEST(DPT_268_Test, DPT_268_1205)
{
    KNX::DPT_268_1205 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 268);
    ASSERT_EQ(dpt.subnumber, 1205);

    // @todo DPT_268_1205
}

TEST(DPT_268_Test, DPT_268_1206)
{
    KNX::DPT_268_1206 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 268);
    ASSERT_EQ(dpt.subnumber, 1206);

    // @todo DPT_268_1206
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
