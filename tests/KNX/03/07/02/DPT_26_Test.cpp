// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_26_Test : public ::testing::Test
{
    virtual ~DPT_26_Test() = default;
};

TEST(DPT_26_Test, DPT_26_001)
{
    KNX::DPT_26_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 26);
    ASSERT_EQ(dpt.subnumber, 1);

    // @todo DPT_26_001
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
