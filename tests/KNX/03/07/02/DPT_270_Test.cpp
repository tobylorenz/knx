// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_270_Test : public ::testing::Test
{
    virtual ~DPT_270_Test() = default;
};

TEST(DPT_270_Test, DPT_270_1200)
{
    KNX::DPT_270_1200 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 270);
    ASSERT_EQ(dpt.subnumber, 1200);

    // @todo DPT_270_1200
}

TEST(DPT_270_Test, DPT_270_1201)
{
    KNX::DPT_270_1201 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 270);
    ASSERT_EQ(dpt.subnumber, 1201);

    // @todo DPT_270_1201
}

TEST(DPT_270_Test, DPT_270_1202)
{
    KNX::DPT_270_1202 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 270);
    ASSERT_EQ(dpt.subnumber, 1202);

    // @todo DPT_270_1202
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
