// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_273_Test : public ::testing::Test
{
    virtual ~DPT_273_Test() = default;
};

TEST(DPT_273_Test, DPT_273_001)
{
    KNX::DPT_273_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 273);
    ASSERT_EQ(dpt.subnumber, 1);

    // @todo DPT_273_001
}

TEST(DPT_273_Test, DPT_273_002)
{
    KNX::DPT_273_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 273);
    ASSERT_EQ(dpt.subnumber, 2);

    // @todo DPT_273_002
}

TEST(DPT_273_Test, DPT_273_003)
{
    KNX::DPT_273_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 273);
    ASSERT_EQ(dpt.subnumber, 3);

    // @todo DPT_273_003
}

TEST(DPT_273_Test, DPT_273_004)
{
    KNX::DPT_273_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 273);
    ASSERT_EQ(dpt.subnumber, 4);

    // @todo DPT_273_004
}

TEST(DPT_273_Test, DPT_273_005)
{
    KNX::DPT_273_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 273);
    ASSERT_EQ(dpt.subnumber, 5);

    // @todo DPT_273_005
}

TEST(DPT_273_Test, DPT_273_006)
{
    KNX::DPT_273_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 273);
    ASSERT_EQ(dpt.subnumber, 6);

    // @todo DPT_273_006
}

TEST(DPT_273_Test, DPT_273_007)
{
    KNX::DPT_273_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 273);
    ASSERT_EQ(dpt.subnumber, 7);

    // @todo DPT_273_007
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
