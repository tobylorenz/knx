// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_277_Test : public ::testing::Test
{
    virtual ~DPT_277_Test() = default;
};

TEST(DPT_277_Test, DPT_277_1200)
{
    KNX::DPT_277_1200 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 277);
    ASSERT_EQ(dpt.subnumber, 1200);

    // @todo DPT_277_1200
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
