// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_28_Test : public ::testing::Test
{
    virtual ~DPT_28_Test() = default;
};

TEST(DPT_28_Test, DPT_28_001)
{
    KNX::DPT_28_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 28);
    ASSERT_EQ(dpt.subnumber, 1);

    data = { 0x54, 0x6F, 0x4D, 0x69, 0x4C, 0x6F };
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.str, "ToMiLo");
    ASSERT_EQ(dpt.text(), "ToMiLo");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
