// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_29_Test : public ::testing::Test
{
    virtual ~DPT_29_Test() = default;
};

TEST(DPT_29_Test, DPT_29_010)
{
    KNX::DPT_29_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 29);
    ASSERT_EQ(dpt.subnumber, 10);

    // @todo DPT_29_010
}

TEST(DPT_29_Test, DPT_29_011)
{
    KNX::DPT_29_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 29);
    ASSERT_EQ(dpt.subnumber, 11);

    // @todo DPT_29_011
}

TEST(DPT_29_Test, DPT_29_012)
{
    KNX::DPT_29_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 29);
    ASSERT_EQ(dpt.subnumber, 12);

    // @todo DPT_29_012
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
