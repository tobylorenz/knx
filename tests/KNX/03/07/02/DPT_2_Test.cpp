// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_2_Test : public ::testing::Test
{
    virtual ~DPT_2_Test() = default;
};

TEST(DPT_2_Test, DPT_2_001)
{
    KNX::DPT_2_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, Off");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, On");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, Off");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, On");
}

TEST(DPT_2_Test, DPT_2_002)
{
    KNX::DPT_2_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 2);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, False");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, True");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, False");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, True");
}

TEST(DPT_2_Test, DPT_2_003)
{
    KNX::DPT_2_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 3);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, Disable");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, Enable");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, Disable");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, Enable");
}

TEST(DPT_2_Test, DPT_2_004)
{
    KNX::DPT_2_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 4);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, No ramp");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, Ramp");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, No ramp");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, Ramp");
}

TEST(DPT_2_Test, DPT_2_005)
{
    KNX::DPT_2_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 5);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, No alarm");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, Alarm");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, No alarm");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, Alarm");
}

TEST(DPT_2_Test, DPT_2_006)
{
    KNX::DPT_2_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 6);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, Low");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, High");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, Low");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, High");
}

TEST(DPT_2_Test, DPT_2_007)
{
    KNX::DPT_2_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 7);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, Decrease");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, Increase");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, Decrease");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, Increase");
}

TEST(DPT_2_Test, DPT_2_008)
{
    KNX::DPT_2_008 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 8);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, Up");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, Down");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, Up");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, Down");
}

TEST(DPT_2_Test, DPT_2_009)
{
    KNX::DPT_2_009 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 9);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, Open");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, Close");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, Open");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, Close");
}

TEST(DPT_2_Test, DPT_2_010)
{
    KNX::DPT_2_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 10);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, Stop");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, Start");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, Stop");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, Start");
}

TEST(DPT_2_Test, DPT_2_011)
{
    KNX::DPT_2_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 11);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, Inactive");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, Active");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, Inactive");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, Active");
}

TEST(DPT_2_Test, DPT_2_012)
{
    KNX::DPT_2_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 2);
    ASSERT_EQ(dpt.subnumber, 12);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "no control, Not inverted");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "no control, Inverted");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, false);
    ASSERT_EQ(dpt.text(), "control, Not inverted");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.v, true);
    ASSERT_EQ(dpt.text(), "control, Inverted");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
