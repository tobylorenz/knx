// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_30_Test : public ::testing::Test
{
    virtual ~DPT_30_Test() = default;
};

TEST(DPT_30_Test, DPT_30_1010)
{
    KNX::DPT_30_1010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 30);
    ASSERT_EQ(dpt.subnumber, 1010);

    // @todo DPT_30_1010
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
