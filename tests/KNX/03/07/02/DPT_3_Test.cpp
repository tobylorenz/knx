// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_3_Test : public ::testing::Test
{
    virtual ~DPT_3_Test() = default;
};

TEST(DPT_3_Test, DPT_3_007)
{
    KNX::DPT_3_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 3);
    ASSERT_EQ(dpt.subnumber, 7);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 0);
    ASSERT_EQ(dpt.text(), "Decrease, Break");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 1);
    ASSERT_EQ(dpt.text(), "Decrease, 100 %");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 2);
    ASSERT_EQ(dpt.text(), "Decrease, 50 %");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 3);
    ASSERT_EQ(dpt.text(), "Decrease, 25 %");

    data = {0x04};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 4);
    ASSERT_EQ(dpt.text(), "Decrease, 12 %");

    data = {0x05};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 5);
    ASSERT_EQ(dpt.text(), "Decrease, 6 %");

    data = {0x06};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 6);
    ASSERT_EQ(dpt.text(), "Decrease, 3 %");

    data = {0x07};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 7);
    ASSERT_EQ(dpt.text(), "Decrease, 1 %");

    data = {0x08};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 0);
    ASSERT_EQ(dpt.text(), "Increase, Break");

    data = {0x09};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 1);
    ASSERT_EQ(dpt.text(), "Increase, 100 %");

    data = {0x0a};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 2);
    ASSERT_EQ(dpt.text(), "Increase, 50 %");

    data = {0x0b};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 3);
    ASSERT_EQ(dpt.text(), "Increase, 25 %");

    data = {0x0c};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 4);
    ASSERT_EQ(dpt.text(), "Increase, 12 %");

    data = {0x0d};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 5);
    ASSERT_EQ(dpt.text(), "Increase, 6 %");

    data = {0x0e};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 6);
    ASSERT_EQ(dpt.text(), "Increase, 3 %");

    data = {0x0f};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 7);
    ASSERT_EQ(dpt.text(), "Increase, 1 %");
}

TEST(DPT_3_Test, DPT_3_008)
{
    KNX::DPT_3_008 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 3);
    ASSERT_EQ(dpt.subnumber, 8);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 0);
    ASSERT_EQ(dpt.text(), "Up, Break");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 1);
    ASSERT_EQ(dpt.text(), "Up, 100 %");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 2);
    ASSERT_EQ(dpt.text(), "Up, 50 %");

    data = {0x03};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 3);
    ASSERT_EQ(dpt.text(), "Up, 25 %");

    data = {0x04};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 4);
    ASSERT_EQ(dpt.text(), "Up, 12 %");

    data = {0x05};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 5);
    ASSERT_EQ(dpt.text(), "Up, 6 %");

    data = {0x06};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 6);
    ASSERT_EQ(dpt.text(), "Up, 3 %");

    data = {0x07};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, false);
    ASSERT_EQ(dpt.step_code, 7);
    ASSERT_EQ(dpt.text(), "Up, 1 %");

    data = {0x08};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 0);
    ASSERT_EQ(dpt.text(), "Down, Break");

    data = {0x09};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 1);
    ASSERT_EQ(dpt.text(), "Down, 100 %");

    data = {0x0a};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 2);
    ASSERT_EQ(dpt.text(), "Down, 50 %");

    data = {0x0b};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 3);
    ASSERT_EQ(dpt.text(), "Down, 25 %");

    data = {0x0c};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 4);
    ASSERT_EQ(dpt.text(), "Down, 12 %");

    data = {0x0d};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 5);
    ASSERT_EQ(dpt.text(), "Down, 6 %");

    data = {0x0e};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 6);
    ASSERT_EQ(dpt.text(), "Down, 3 %");

    data = {0x0f};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.c, true);
    ASSERT_EQ(dpt.step_code, 7);
    ASSERT_EQ(dpt.text(), "Down, 1 %");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
