// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_4_Test : public ::testing::Test
{
    virtual ~DPT_4_Test() = default;
};

TEST(DPT_4_Test, DPT_4_001)
{
    KNX::DPT_4_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 4);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x61};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.character, 'a');
    ASSERT_EQ(dpt.text(), "a");

    data = {0xE4};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.character, 0xE4);
    ASSERT_EQ(dpt.text(), "ä");
}

TEST(DPT_4_Test, DPT_4_002)
{
    KNX::DPT_4_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 4);
    ASSERT_EQ(dpt.subnumber, 2);

    data = {0x61};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.character, 'a');
    ASSERT_EQ(dpt.text(), "a");

    data = {0xE4};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.character, 0xE4);
    ASSERT_EQ(dpt.text(), "ä");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
