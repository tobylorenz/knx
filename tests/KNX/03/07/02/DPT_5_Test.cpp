// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_5_Test : public ::testing::Test
{
    virtual ~DPT_5_Test() = default;
};

TEST(DPT_5_Test, DPT_5_001)
{
    KNX::DPT_5_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 5);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0.0 %"); // actually "0 %"

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "0.4 %"); // actually "0 %"

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 2);
    ASSERT_EQ(dpt.text(), "0.8 %"); // actually "1 %"

    data = {0x63};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 99);
    ASSERT_EQ(dpt.text(), "38.8 %"); // actually "39 %"

    data = {0x64};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 100);
    ASSERT_EQ(dpt.text(), "39.2 %"); // actually "39 %"

    data = {0x65};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 101);
    ASSERT_EQ(dpt.text(), "39.6 %"); // actually "40 %"

    data = {0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 254);
    ASSERT_EQ(dpt.text(), "99.6 %"); // actually "100 %"

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 255);
    ASSERT_EQ(dpt.text(), "100.0 %"); // actually "100 %"
}

TEST(DPT_5_Test, DPT_5_003)
{
    KNX::DPT_5_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 5);
    ASSERT_EQ(dpt.subnumber, 3);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0.0 °"); // actually "0 °"

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1.4 °"); // actually "1 °"

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 2);
    ASSERT_EQ(dpt.text(), "2.8 °"); // actually "3 °"

    data = {0x63};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 99);
    ASSERT_EQ(dpt.text(), "139.8 °"); // actually "140 °"

    data = {0x64};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 100);
    ASSERT_EQ(dpt.text(), "141.2 °"); // actually "141 °"

    data = {0x65};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 101);
    ASSERT_EQ(dpt.text(), "142.6 °"); // actually "143 °"

    data = {0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 254);
    ASSERT_EQ(dpt.text(), "358.6 °"); // actually "359 °"

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 255);
    ASSERT_EQ(dpt.text(), "360.0 °"); // actually "360 °"
}

TEST(DPT_5_Test, DPT_5_004)
{
    KNX::DPT_5_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 5);
    ASSERT_EQ(dpt.subnumber, 4);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 %");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 %");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 2);
    ASSERT_EQ(dpt.text(), "2 %");

    data = {0x63};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 99);
    ASSERT_EQ(dpt.text(), "99 %");

    data = {0x64};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 100);
    ASSERT_EQ(dpt.text(), "100 %");

    data = {0x65};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 101);
    ASSERT_EQ(dpt.text(), "101 %");

    data = {0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 254);
    ASSERT_EQ(dpt.text(), "254 %");

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 255);
    ASSERT_EQ(dpt.text(), "255 %");
}

TEST(DPT_5_Test, DPT_5_005)
{
    KNX::DPT_5_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 5);
    ASSERT_EQ(dpt.subnumber, 5);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 2);
    ASSERT_EQ(dpt.text(), "2");

    data = {0x63};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 99);
    ASSERT_EQ(dpt.text(), "99");

    data = {0x64};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 100);
    ASSERT_EQ(dpt.text(), "100");

    data = {0x65};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 101);
    ASSERT_EQ(dpt.text(), "101");

    data = {0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 254);
    ASSERT_EQ(dpt.text(), "254");

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 255);
    ASSERT_EQ(dpt.text(), "255");
}

TEST(DPT_5_Test, DPT_5_006)
{
    KNX::DPT_5_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 5);
    ASSERT_EQ(dpt.subnumber, 6);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "no tariff available");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "tariff 1");

    data = {0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 254);
    ASSERT_EQ(dpt.text(), "tariff 254");

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 255);
    ASSERT_EQ(dpt.text(), "reserved");

    // @note ETS shows the following obviously wrong output.
#if 0
    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "0.996078431372549");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 2);
    ASSERT_EQ(dpt.text(), "1.9921568627451");

    data = {0x63};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 99);
    ASSERT_EQ(dpt.text(), "98.6117647058824");

    data = {0x64};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 100);
    ASSERT_EQ(dpt.text(), "99.6078431372549");

    data = {0x65};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 101);
    ASSERT_EQ(dpt.text(), "100.603921568627");

    data = {0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 254);
    ASSERT_EQ(dpt.text(), "253.003921568627");

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 255);
    ASSERT_EQ(dpt.text(), "254");
#endif
}

TEST(DPT_5_Test, DPT_5_010)
{
    KNX::DPT_5_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 5);
    ASSERT_EQ(dpt.subnumber, 10);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 counter pulses"); // @note ETS shows "0"

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 counter pulses"); // @note ETS shows "1"

    data = {0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 254);
    ASSERT_EQ(dpt.text(), "254 counter pulses"); // @note ETS shows "254"

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 255);
    ASSERT_EQ(dpt.text(), "255 counter pulses"); // @note ETS shows "255"
}

TEST(DPT_5_Test, DPT_5_100)
{
    KNX::DPT_5_100 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 5);
    ASSERT_EQ(dpt.subnumber, 100);

    // @todo DPT_5_100
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
