// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_6_Test : public ::testing::Test
{
    virtual ~DPT_6_Test() = default;
};

TEST(DPT_6_Test, DPT_6_001)
{
    KNX::DPT_6_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 6);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 %");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 %");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 2);
    ASSERT_EQ(dpt.text(), "2 %");

    data = {0x7f};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 127);
    ASSERT_EQ(dpt.text(), "127 %");

    data = {0x80};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, -128);
    ASSERT_EQ(dpt.text(), "-128 %");

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-1 %");
}

TEST(DPT_6_Test, DPT_6_010)
{
    KNX::DPT_6_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 6);
    ASSERT_EQ(dpt.subnumber, 10);

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 counter pulses");

    data = {0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 counter pulses");

    data = {0x02};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 2);
    ASSERT_EQ(dpt.text(), "2 counter pulses");

    data = {0x7f};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 127);
    ASSERT_EQ(dpt.text(), "127 counter pulses");

    data = {0x80};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -128);
    ASSERT_EQ(dpt.text(), "-128 counter pulses");

    data = {0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-1 counter pulses");
}

TEST(DPT_6_Test, DPT_6_020)
{
    KNX::DPT_6_020 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 6);
    ASSERT_EQ(dpt.subnumber, 20);

    // @note ETS shows nothing.

    data = {0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.text(), "A set, B set, C set, D set, E set");

    data = {0x09};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.text(), "A set, B set, C set, D set, E clear, mode 0 is active");

    data = {0x12};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.text(), "A set, B set, C set, D clear, E set, mode 1 is active");

    data = {0x24};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.text(), "A set, B set, C clear, D set, E set, mode 2 is active");

    data = {0x40};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.text(), "A set, B clear, C set, D set, E set");

    data = {0x80};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.text(), "A clear, B set, C set, D set, E set");

    data = {0xf8};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.text(), "A clear, B clear, C clear, D clear, E clear");

    data = {0x07};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.text(), "A set, B set, C set, D set, E set");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
