// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_7_Test : public ::testing::Test
{
    virtual ~DPT_7_Test() = default;
};

TEST(DPT_7_Test, DPT_7_001)
{
    KNX::DPT_7_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 pulses");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 pulses");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534 pulses");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535 pulses");
}

TEST(DPT_7_Test, DPT_7_002)
{
    KNX::DPT_7_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 2);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 ms");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 ms");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534 ms");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535 ms");
}

TEST(DPT_7_Test, DPT_7_003)
{
    KNX::DPT_7_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 3);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 ms");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "10 ms");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "655340 ms");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "655350 ms");
}

TEST(DPT_7_Test, DPT_7_004)
{
    KNX::DPT_7_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 4);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 ms");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "100 ms");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "6553400 ms");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "6553500 ms");
}

TEST(DPT_7_Test, DPT_7_005)
{
    KNX::DPT_7_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 5);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 s");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 s");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534 s");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535 s");
}

TEST(DPT_7_Test, DPT_7_006)
{
    KNX::DPT_7_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 6);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 min");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 min");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534 min");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535 min");
}

TEST(DPT_7_Test, DPT_7_007)
{
    KNX::DPT_7_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 7);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 h");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 h");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534 h");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535 h");
}

TEST(DPT_7_Test, DPT_7_010)
{
    KNX::DPT_7_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 10);

    // @note ETS shows nothing.

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535");
}

TEST(DPT_7_Test, DPT_7_011)
{
    KNX::DPT_7_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 11);

    // @note ETS shows nothing.

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 mm");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 mm");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534 mm");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535 mm");
}

TEST(DPT_7_Test, DPT_7_012)
{
    KNX::DPT_7_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 12);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 mA");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 mA");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534 mA");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535 mA");
}

TEST(DPT_7_Test, DPT_7_013)
{
    KNX::DPT_7_013 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 13);

    // @note ETS shows nothing.

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 lux");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 lux");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534 lux");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535 lux");
}

TEST(DPT_7_Test, DPT_7_600)
{
    KNX::DPT_7_600 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 7);
    ASSERT_EQ(dpt.subnumber, 600);

    // @note ETS shows nothing.

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 0);
    ASSERT_EQ(dpt.text(), "0 K");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 1);
    ASSERT_EQ(dpt.text(), "1 K");

    data = {0xff, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65534);
    ASSERT_EQ(dpt.text(), "65534 K");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.unsigned_value, 65535);
    ASSERT_EQ(dpt.text(), "65535 K");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
