// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_8_Test : public ::testing::Test
{
    virtual ~DPT_8_Test() = default;
};

TEST(DPT_8_Test, DPT_8_001)
{
    KNX::DPT_8_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 1);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 pulses");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 pulses");

    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, 32767);
    ASSERT_EQ(dpt.text(), "32767 pulses");

    data = {0x80, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, -32768);
    ASSERT_EQ(dpt.text(), "-32768 pulses");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-1 pulses");
}

TEST(DPT_8_Test, DPT_8_002)
{
    KNX::DPT_8_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 2);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 ms");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 ms");

    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 32767);
    ASSERT_EQ(dpt.text(), "32767 ms");

    data = {0x80, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -32768);
    ASSERT_EQ(dpt.text(), "-32768 ms");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-1 ms");
}

TEST(DPT_8_Test, DPT_8_003)
{
    KNX::DPT_8_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 3);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 ms");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "10 ms");

    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 32767);
    ASSERT_EQ(dpt.text(), "327670 ms");

    data = {0x80, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -32768);
    ASSERT_EQ(dpt.text(), "-327680 ms");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-10 ms");
}

TEST(DPT_8_Test, DPT_8_004)
{
    KNX::DPT_8_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 4);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 ms");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "100 ms");

    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 32767);
    ASSERT_EQ(dpt.text(), "3276700 ms");

    data = {0x80, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -32768);
    ASSERT_EQ(dpt.text(), "-3276800 ms");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-100 ms");
}

TEST(DPT_8_Test, DPT_8_005)
{
    KNX::DPT_8_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 5);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 s");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 s");

    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 32767);
    ASSERT_EQ(dpt.text(), "32767 s");

    data = {0x80, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -32768);
    ASSERT_EQ(dpt.text(), "-32768 s");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-1 s");
}

TEST(DPT_8_Test, DPT_8_006)
{
    KNX::DPT_8_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 6);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 min");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 min");

    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 32767);
    ASSERT_EQ(dpt.text(), "32767 min");

    data = {0x80, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -32768);
    ASSERT_EQ(dpt.text(), "-32768 min");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-1 min");
}

TEST(DPT_8_Test, DPT_8_007)
{
    KNX::DPT_8_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 7);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 h");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 h");

    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 32767);
    ASSERT_EQ(dpt.text(), "32767 h");

    data = {0x80, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -32768);
    ASSERT_EQ(dpt.text(), "-32768 h");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-1 h");
}

TEST(DPT_8_Test, DPT_8_010)
{
    KNX::DPT_8_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 10);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0.00 %"); // @note ETS shows "0%"

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "0.01 %"); // @note ETS shows "0.0099999..."

    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 32767);
    ASSERT_EQ(dpt.text(), "327.67 %"); // @note ETS shows "327.6699999..."

    data = {0x80, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -32768);
    ASSERT_EQ(dpt.text(), "-327.68 %"); // @note ETS shows "-327.6799999..."

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-0.01 %"); // @note ETS shows "-0.0099999..."
}

TEST(DPT_8_Test, DPT_8_011)
{
    KNX::DPT_8_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 11);

    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 0);
    ASSERT_EQ(dpt.text(), "0 °");

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 1);
    ASSERT_EQ(dpt.text(), "1 °");

    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, 32767);
    ASSERT_EQ(dpt.text(), "32767 °");

    data = {0x80, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -32768);
    ASSERT_EQ(dpt.text(), "-32768 °");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(dpt.signed_value, -1);
    ASSERT_EQ(dpt.text(), "-1 °");
}

TEST(DPT_8_Test, DPT_8_012)
{
    KNX::DPT_8_012 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 8);
    ASSERT_EQ(dpt.subnumber, 12);

    // @todo DPT_8_012
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
