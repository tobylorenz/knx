// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/07/02_Datapoint_Types.h>

class DPT_9_Test : public ::testing::Test
{
    virtual ~DPT_9_Test() = default;
};

TEST(DPT_9_Test, DPT_9_001)
{
    KNX::DPT_9_001 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 1);

    // zero
    data = {0x00, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_DOUBLE_EQ(dpt.float_value, 0.0);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "0.00 °C"); // @note ETS shows "0 °C"

    data = {0x00, 0x01};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_DOUBLE_EQ(dpt.float_value, 0.01);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "0.01 °C");

    data = {0x00, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_DOUBLE_EQ(dpt.float_value, 2.55);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "2.55 °C");

    // maximum - 1
    data = {0x7f, 0xfe};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_DOUBLE_EQ(dpt.float_value, 670433.28);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "670433.28 °C");

    // maximum (670760.96), invalid
    data = {0x7f, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_DOUBLE_EQ(dpt.float_value, 670760.96);
    ASSERT_EQ(dpt.float_value.invalid(), true);
    ASSERT_EQ(dpt.text(), "670760.96 °C");

    // minimum (-671088.64)
    data = {0xf8, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_DOUBLE_EQ(dpt.float_value, -671088.64);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-671088.64 °C");

    data = {0xff, 0x00};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_DOUBLE_EQ(dpt.float_value, -83886.08);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-83886.08 °C");

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, dpt.toData());
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 °C");
}

TEST(DPT_9_Test, DPT_9_002)
{
    KNX::DPT_9_002 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 2);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 K");
}

TEST(DPT_9_Test, DPT_9_003)
{
    KNX::DPT_9_003 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 3);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 K/h");
}

TEST(DPT_9_Test, DPT_9_004)
{
    KNX::DPT_9_004 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 4);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 Lux");
}

TEST(DPT_9_Test, DPT_9_005)
{
    KNX::DPT_9_005 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 5);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 m/s");
}

TEST(DPT_9_Test, DPT_9_006)
{
    KNX::DPT_9_006 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 6);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 Pa");
}

TEST(DPT_9_Test, DPT_9_007)
{
    KNX::DPT_9_007 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 7);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 %");
}

TEST(DPT_9_Test, DPT_9_008)
{
    KNX::DPT_9_008 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 8);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 ppm");
}

TEST(DPT_9_Test, DPT_9_009)
{
    KNX::DPT_9_009 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 9);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 m³/h");
}

TEST(DPT_9_Test, DPT_9_010)
{
    KNX::DPT_9_010 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 10);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 s");
}

TEST(DPT_9_Test, DPT_9_011)
{
    KNX::DPT_9_011 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 11);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 ms");
}

TEST(DPT_9_Test, DPT_9_020)
{
    KNX::DPT_9_020 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 20);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 mV");
}

TEST(DPT_9_Test, DPT_9_021)
{
    KNX::DPT_9_021 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 21);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 mA");
}

TEST(DPT_9_Test, DPT_9_022)
{
    KNX::DPT_9_022 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 22);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 W/m²");
}

TEST(DPT_9_Test, DPT_9_023)
{
    KNX::DPT_9_023 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 23);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 K/%");
}

TEST(DPT_9_Test, DPT_9_024)
{
    KNX::DPT_9_024 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 24);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 kW");
}

TEST(DPT_9_Test, DPT_9_025)
{
    KNX::DPT_9_025 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 25);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 l/h");
}

TEST(DPT_9_Test, DPT_9_026)
{
    KNX::DPT_9_026 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 26);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 l/m²");
}

TEST(DPT_9_Test, DPT_9_027)
{
    KNX::DPT_9_027 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 27);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 °F");
}

TEST(DPT_9_Test, DPT_9_028)
{
    KNX::DPT_9_028 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 28);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 km/h");
}

TEST(DPT_9_Test, DPT_9_029)
{
    KNX::DPT_9_029 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 29);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 g/m³");
}

TEST(DPT_9_Test, DPT_9_030)
{
    KNX::DPT_9_030 dpt;
    std::vector<uint8_t> data;
    ASSERT_EQ(dpt.main_number, 9);
    ASSERT_EQ(dpt.subnumber, 30);

    data = {0xff, 0xff};
    dpt.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(dpt.float_value, -327.68);
    ASSERT_EQ(dpt.float_value.invalid(), false);
    ASSERT_EQ(dpt.text(), "-327.68 µg/m³");
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
