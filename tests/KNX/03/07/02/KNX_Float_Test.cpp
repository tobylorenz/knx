// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <cmath>

#include <KNX/03/07/02/KNX_Float.h>

class KNX_Float_Test : public ::testing::Test
{
    virtual ~KNX_Float_Test() = default;
};

class KNX_Float_Protected_Access : public KNX::KNX_Float
{
public:
    using KNX_Float::operator=;
    using KNX_Float::operator double;
    using KNX_Float::sign;
    using KNX_Float::exponent;
    using KNX_Float::mantissa;
};

TEST(KNX_Float_Test, Float_Test_Pack)
{
    KNX_Float_Protected_Access knx_float;
    std::vector<uint8_t> data;

    // from/to check
    data = {0xAA, 0xAA};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.sign, 1);
    ASSERT_EQ(knx_float.exponent, 0x5);
    ASSERT_EQ(knx_float.mantissa, 0x2AA);

    // from/to check
    data = {0x55, 0x55};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0xa);
    ASSERT_EQ(knx_float.mantissa, 0x555);
}

TEST(KNX_Float_Test, Float_Test_Get)
{
    KNX_Float_Protected_Access knx_float;
    std::vector<uint8_t> data;

    // zero
    data = {0x00, 0x00};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(knx_float, 0.00);
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0);
    ASSERT_EQ(knx_float.mantissa, 0);

    // one
    data = {0x00, 0x01};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(knx_float, 0.01);
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0);
    ASSERT_EQ(knx_float.mantissa, 1);

    data = {0x00, 0xff};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(knx_float, 2.55);
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0);
    ASSERT_EQ(knx_float.mantissa, 255);

    // maximum - 1
    data = {0x7f, 0xfe};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(knx_float, 670433.28);
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0xf);
    ASSERT_EQ(knx_float.mantissa, 0x7fe);

    // maximum / invalid
    data = {0x7f, 0xff};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(knx_float, 670760.96);
    ASSERT_EQ(knx_float.invalid(), true);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0xf);
    ASSERT_EQ(knx_float.mantissa, 0x7ff);

    // minimum
    data = {0xf8, 0x00};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(knx_float, -671088.64);
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 1);
    ASSERT_EQ(knx_float.exponent, 0xf);
    ASSERT_EQ(knx_float.mantissa, 0x000);

    data = {0xff, 0x00};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(knx_float, -83886.08);
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 1);
    ASSERT_EQ(knx_float.exponent, 0xf);
    ASSERT_EQ(knx_float.mantissa, 0x700);

    data = {0xff, 0xff};
    knx_float.fromData(std::cbegin(data), std::cend(data));
    ASSERT_DOUBLE_EQ(knx_float, -327.68);
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 1);
    ASSERT_EQ(knx_float.exponent, 0xf);
    ASSERT_EQ(knx_float.mantissa, 0x7ff);
}

TEST(KNX_Float_Test, Float_Test_Set)
{
    KNX_Float_Protected_Access knx_float;
    std::vector<uint8_t> data;

    // zero
    data = {0x00, 0x00};
    knx_float = 0.00;
    ASSERT_DOUBLE_EQ(knx_float, 0.00);
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0);
    ASSERT_EQ(knx_float.mantissa, 0);

    // one
    data = {0x00, 0x01};
    knx_float = 0.01;
    ASSERT_DOUBLE_EQ(knx_float, 0.01);
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0);
    ASSERT_EQ(knx_float.mantissa, 1);

    data = {0x00, 0xff};
    knx_float = 2.55;
    ASSERT_DOUBLE_EQ(knx_float, 2.55);
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0);
    ASSERT_EQ(knx_float.mantissa, 255);

    // maximum - 1
    data = {0x7f, 0xfe};
    knx_float = 670433.28;
    ASSERT_DOUBLE_EQ(knx_float, 670433.28);
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0xf);
    ASSERT_EQ(knx_float.mantissa, 0x7fe);

    // maximum / invalid
    data = {0x7f, 0xff};
    knx_float = 670760.96;
    ASSERT_DOUBLE_EQ(knx_float, 670760.96);
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.invalid(), true);
    ASSERT_EQ(knx_float.sign, 0);
    ASSERT_EQ(knx_float.exponent, 0xf);
    ASSERT_EQ(knx_float.mantissa, 0x7ff);

    data = {0xa0, 0x00};
    knx_float = -327.68;
    ASSERT_DOUBLE_EQ(knx_float, -327.68);
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 1);
    ASSERT_EQ(knx_float.exponent, 0x4);
    ASSERT_EQ(knx_float.mantissa, 0x000);

    data = {0xe0, 0x00};
    knx_float = -83886.08;
    ASSERT_DOUBLE_EQ(knx_float, -83886.08);
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 1);
    ASSERT_EQ(knx_float.exponent, 0xc);
    ASSERT_EQ(knx_float.mantissa, 0x000);

    // minimum
    data = {0xf8, 0x00};
    knx_float = -671088.64;
    ASSERT_DOUBLE_EQ(knx_float, -671088.64);
    ASSERT_EQ(data, knx_float.toData());
    ASSERT_EQ(knx_float.invalid(), false);
    ASSERT_EQ(knx_float.sign, 1);
    ASSERT_EQ(knx_float.exponent, 0xf);
    ASSERT_EQ(knx_float.mantissa, 0x000);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
