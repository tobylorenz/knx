// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/Connect_Request_Frame.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/04/Tunnelling_Connection_Request_Information.h>

class Connection_Request_Test : public ::testing::Test
{
    virtual ~Connection_Request_Test() = default;
};

/**
 * @ingroup KNX_03_08_02_08_08_05
 */
TEST(Connection_Request_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, // service type identifier 0x205h
        0x05, // "
        0x00, // total length, 26 octets
        0x1A, // "
        0x08, // structure length
        0x01, // host protocol code, e.g. 01h, for UDP over IPv4
        192, // IP address of control endpoint,
        168, // e.g. 192.168.200.12
        200, // "
        12, // "
        0xC3, // port number of control endpoint,
        0xB4, // e.g. 50100
        0x08, // structure length
        0x01, // host protocol code, e.g. 01h, for UDP over IPv4
        192, // IP address of data endpoint,
        168, // e.g. 192.168.200.20
        200, // "
        20, // "
        0xC3, // port number of control endpoint,
        0xB4, // e.g. 50100
        0x04, // structure length of CRD for TUNNELING_CONNECTION
        0x04, // connection type code, e.g. 04h, TUNNEL_CONNECTION
        0x02, // KNX layer, e.g. TUNNEL_LINKLAYER
        0x00}; // reserved

    KNX::Connect_Request_Frame connect_request;
    connect_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(connect_request.header_length, 0x06);
    ASSERT_EQ(connect_request.protocol_version, 0x10);
    ASSERT_EQ(connect_request.service_type_identifier, KNX::Service_Type_Identifier::CONNECT_REQUEST);
    ASSERT_EQ(connect_request.total_length, 26);
    ASSERT_EQ(connect_request.total_length_calculated(), 26);

    ASSERT_TRUE(connect_request.control_endpoint);
    ASSERT_EQ(connect_request.control_endpoint->structure_length, 0x08);
    ASSERT_EQ(connect_request.control_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(connect_request.control_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_control_ip_address{{192, 168, 200, 12}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(connect_request.control_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_control_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 50100);

    ASSERT_TRUE(connect_request.data_endpoint);
    ASSERT_EQ(connect_request.data_endpoint->structure_length, 0x08);
    ASSERT_EQ(connect_request.data_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(connect_request.data_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_data_ip_address{{192, 168, 200, 20}};
    ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(connect_request.data_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_data_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 50100);

    ASSERT_TRUE(connect_request.connection_request_information);
    ASSERT_EQ(connect_request.connection_request_information->structure_length, 0x04);
    ASSERT_EQ(connect_request.connection_request_information->structure_length_calculated(), 0x04);
    ASSERT_EQ(connect_request.connection_request_information->connection_type_code, KNX::Connection_Type_Code::TUNNEL_CONNECTION);
    std::shared_ptr<KNX::Tunnelling_Connection_Request_Information> tcri = std::dynamic_pointer_cast<KNX::Tunnelling_Connection_Request_Information>(connect_request.connection_request_information);
    ASSERT_TRUE(tcri);
    ASSERT_EQ(tcri->layer, KNX::Tunnelling_Layer_Type_Code::TUNNEL_LINKLAYER);

    ASSERT_EQ(connect_request.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
