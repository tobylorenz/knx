// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/Connect_Response_Frame.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/04/Tunnelling_Connection_Response_Data_Block.h>

class Connect_Response_Test : public ::testing::Test
{
    virtual ~Connect_Response_Test() = default;
};

/**
 * @ingroup KNX_03_08_02_08_08_06
 */
TEST(Connect_Response_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, // service type identifier 0x206h
        0x06, // "
        0x00, // total length, 20 octets
        0x14, // "
        0x15, // communication channel ID, e.g. 21
        0x00, // status code (NO_ERROR)
        0x08, // structure length
        0x01, // host protocol code, e.g. 01h, for UDP over IPv4
        192, // IP address of data endpoint,
        168, // e.g. 192.168.200.20
        200, // "
        20, // "
        0xC3, // port number of data endpoint,
        0xB4, // e.g. 50100
        0x04, // structure length of CRD for TUNNELING_CONNECTION
        0x04, // connection type code, e.g. 04h, TUNNEL_CONNECTION
        0x11, // Individual Address, e.g. 01.01.10,
        0x0A}; // used for TUNNELING_CONNECTION

    KNX::Connect_Response_Frame connect_response;
    connect_response.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(connect_response.header_length, 0x06);
    ASSERT_EQ(connect_response.protocol_version, 0x10);
    ASSERT_EQ(connect_response.service_type_identifier, KNX::Service_Type_Identifier::CONNECT_RESPONSE);
    ASSERT_EQ(connect_response.total_length, 20);
    ASSERT_EQ(connect_response.total_length_calculated(), 20);
    ASSERT_EQ(connect_response.communication_channel_id, 21);
    ASSERT_EQ(connect_response.status, KNX::Error_Code::E_NO_ERROR);

    ASSERT_TRUE(connect_response.data_endpoint);
    ASSERT_EQ(connect_response.data_endpoint->structure_length, 0x08);
    ASSERT_EQ(connect_response.data_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(connect_response.data_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_ip_address{{192, 168, 200, 20}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(connect_response.data_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 50100);

    ASSERT_EQ(connect_response.connection_response_data_block->structure_length, 0x04);
    ASSERT_EQ(connect_response.connection_response_data_block->structure_length_calculated(), 0x04);
    ASSERT_EQ(connect_response.connection_response_data_block->connection_type_code, KNX::Connection_Type_Code::TUNNEL_CONNECTION);
    std::shared_ptr<KNX::Tunnelling_Connection_Response_Data_Block> rcrdb = std::dynamic_pointer_cast<KNX::Tunnelling_Connection_Response_Data_Block>(connect_response.connection_response_data_block);
    ASSERT_EQ(rcrdb->individual_address, 0x110A);

    ASSERT_EQ(connect_response.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
