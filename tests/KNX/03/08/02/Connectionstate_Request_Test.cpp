// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/Connectionstate_Request_Frame.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>

class Connectionstate_Request_Test : public ::testing::Test
{
    virtual ~Connectionstate_Request_Test() = default;
};

/**
 * @ingroup KNX_03_08_02_08_08_07
 */
TEST(Connectionstate_Request_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, // service type identifier 0x207h
        0x07, // "
        0x00, // total length, 16 octets
        0x10, // "
        0x15, // communication channel ID, e.g. 21
        0x00, // reserved
        0x08, // structure length
        0x01, // host protocol code, e.g. 01h, for UDP over IPv4
        192, // IP address of control endpoint,
        168, // e.g. 192.168.200.12
        200, // "
        12, // "
        0xC3, // port number of control endpoint,
        0xB4}; // e.g. 50100

    KNX::Connectionstate_Request_Frame connectionstate_request;
    connectionstate_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(connectionstate_request.header_length, 0x06);
    ASSERT_EQ(connectionstate_request.protocol_version, 0x10);
    ASSERT_EQ(connectionstate_request.service_type_identifier, KNX::Service_Type_Identifier::CONNECTIONSTATE_REQUEST);
    ASSERT_EQ(connectionstate_request.total_length, 16);
    ASSERT_EQ(connectionstate_request.total_length_calculated(), 16);
    ASSERT_EQ(connectionstate_request.communication_channel_id, 21);

    ASSERT_TRUE(connectionstate_request.control_endpoint);
    ASSERT_EQ(connectionstate_request.control_endpoint->structure_length, 0x08);
    ASSERT_EQ(connectionstate_request.control_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(connectionstate_request.control_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_ip_address{{192, 168, 200, 12}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(connectionstate_request.control_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 50100);

    ASSERT_EQ(connectionstate_request.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
