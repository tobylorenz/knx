// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/Description_Request_Frame.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>

class Description_Request_Test : public ::testing::Test
{
    virtual ~Description_Request_Test() = default;
};

/**
 * @ingroup KNX_03_08_02_08_08_03
 */
TEST(Description_Request_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, // service type identifier 0x203h
        0x03, // "
        0x00, // total length, 14 octets
        0x0E, // "
        0x08, // structure length
        0x01, // host protocol code, e.g. 01h, for UDP over IPV4
        192, // IP address of control endpoint,
        168, // e.g. 192.168.200.12
        200, // "
        12, // "
        0xC3, // port number of control endpoint,
        0xB4}; // e.g. 50100

    KNX::Description_Request_Frame description_request;
    description_request.fromData(std::cbegin(data), std::cend(data));
    ASSERT_EQ(description_request.header_length, 0x06);
    ASSERT_EQ(description_request.protocol_version, 0x10);
    ASSERT_EQ(description_request.service_type_identifier, KNX::Service_Type_Identifier::DESCRIPTION_REQUEST);
    ASSERT_EQ(description_request.total_length, 14);
    ASSERT_EQ(description_request.total_length_calculated(), 14);

    ASSERT_TRUE(description_request.control_endpoint);
    ASSERT_EQ(description_request.control_endpoint->structure_length, 0x08);
    ASSERT_EQ(description_request.control_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(description_request.control_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_ip_address{{192, 168, 200, 12}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(description_request.control_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 50100);

    ASSERT_EQ(description_request.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
