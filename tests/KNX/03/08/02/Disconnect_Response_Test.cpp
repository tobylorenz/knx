// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/Disconnect_Response_Frame.h>

class Disconnect_Response_Test : public ::testing::Test
{
    virtual ~Disconnect_Response_Test() = default;
};

/**
 * @ingroup KNX_03_08_02_08_08_10
 */
TEST(Disconnect_Response_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, // service type identifier 0x20Ah
        0x0A, // "
        0x00, // total length, 8 octets
        0x08, // "
        0x15, // communication channel ID, e.g. 21
        0x00}; // status code (NO_ERROR)

    KNX::Disconnect_Response_Frame disconnect_response;
    disconnect_response.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(disconnect_response.header_length, 0x06);
    ASSERT_EQ(disconnect_response.protocol_version, 0x10);
    ASSERT_EQ(disconnect_response.service_type_identifier, KNX::Service_Type_Identifier::DISCONNECT_RESPONSE);
    ASSERT_EQ(disconnect_response.total_length, 8);
    ASSERT_EQ(disconnect_response.total_length_calculated(), 8);
    ASSERT_EQ(disconnect_response.communication_channel_id, 21);
    ASSERT_EQ(disconnect_response.status, KNX::Error_Code::E_NO_ERROR);

    ASSERT_EQ(disconnect_response.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
