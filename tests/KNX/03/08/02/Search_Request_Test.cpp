// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/02/Search_Request_Frame.h>

class Search_Request_Test : public ::testing::Test
{
    virtual ~Search_Request_Test() = default;
};

/**
 * @ingroup KNX_03_08_02_08_08_01
 */
TEST(Search_Request_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, // service type identifier 0x201h
        0x01, // "
        0x00, // total length, 14 octets
        0x0E, // "
        0x08, // structure length
        0x01, // host protocol code, e.g. 01h, for UDP over IPV4
        192, // IP address of control endpoint,
        168, // e.g. 192.168.200.12
        200, // "
        12, // "
        0x0E, // port number of control endpoint, 3671
        0x57}; // "

    KNX::Search_Request_Frame search_request;
    search_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(search_request.header_length, 0x06);
    ASSERT_EQ(search_request.protocol_version, 0x10);
    ASSERT_EQ(search_request.service_type_identifier, KNX::Service_Type_Identifier::SEARCH_REQUEST);
    ASSERT_EQ(search_request.total_length, 14);
    ASSERT_EQ(search_request.total_length_calculated(), 14);

    ASSERT_TRUE(search_request.discovery_endpoint);
    ASSERT_EQ(search_request.discovery_endpoint->structure_length, 0x08);
    ASSERT_EQ(search_request.discovery_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(search_request.discovery_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_ip_address{{192, 168, 200, 12}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(search_request.discovery_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 3671);

    ASSERT_EQ(search_request.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
