// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/02/Search_Response_Frame.h>

class Search_Response_Test : public ::testing::Test
{
    virtual ~Search_Response_Test() = default;
};

/**
 * @ingroup KNX_03_08_02_08_08_02
 */
TEST(Search_Response_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, // service type identifier 0202h
        0x02, // "
        0x00, // total length, 78 octets
        0x4E, // "
        0x08, // structure length (HPAI)
        0x01, // host protocol code, e.g. 01h, for UDP over IPV4
        192, // IP address of control endpoint,
        168, // e.g. 192.168.200.12
        200, // "
        12, // "
        0xC3, // port number of control endpoint,
        0xB4, // e.g. 50100
        0x36, // structure length (DIB hardware)
        0x01, // description type code, 01h = DEVICE_INFO
        0x02, // KNX medium, 02h = TP1 (KNX TP)
        0x01, // Device Status, 01h = programming mode
        0x11, // KNX Individual Address, e.g. 1.1.0
        0x00, // "
        0x00, // Project-Installation ID, e.g. 0011h
        0x11, // "
        0x00, // KNX device KNX Serial Number,
        0x01, // "
        0x11, // "
        0x11, // "
        0x11, // "
        0x11, // "
        224, // device routing multicast address
        0, // e.g. 224.0.23.12
        23, // "
        12, // "
        0x45, // KNXnet/IP MAC address
        0x49, // "
        0x42, // "
        0x6E, // "
        0x65, // "
        0x74, // "
        'M', // Device Friendly Name, e.g. "MYHOME"
        'Y', // "
        'H', // "
        'O', // "
        'M', // "
        'E', // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x00, // "
        0x0A, // structure length (DIB supported service families)
        0x02, // description type code, 02h = SUPP_SVC_FAMILIES
        0x02, // service family, e.g. 02h = KNXnet/IP Core
        0x01, // service family version, e.g. 01h
        0x03, // service family, e.g. 03h = KNXnet/Device Mgmt
        0x01, // service family version, e.g. 01h
        0x04, // service family, e.g. 04h = KNXnet/IP Tunnelling
        0x01, // service family version, e.g. 01h
        0x05, // service family, e.g. 05h = KNXnet/IP Routing
        0x01}; // service family version, e.g. 01h

    KNX::Search_Response_Frame search_response;
    search_response.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(search_response.header_length, 0x06);
    ASSERT_EQ(search_response.protocol_version, 0x10);
    ASSERT_EQ(search_response.service_type_identifier, KNX::Service_Type_Identifier::SEARCH_RESPONSE);
    ASSERT_EQ(search_response.total_length, 78);
    ASSERT_EQ(search_response.total_length_calculated(), 78);

    ASSERT_TRUE(search_response.control_endpoint);
    ASSERT_EQ(search_response.control_endpoint->structure_length, 0x08);
    ASSERT_EQ(search_response.control_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(search_response.control_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_ip_address{{192, 168, 200, 12}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(search_response.control_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 50100);

    ASSERT_TRUE(search_response.device_hardware);
    ASSERT_EQ(search_response.device_hardware->structure_length, 0x36);
    ASSERT_EQ(search_response.device_hardware->structure_length_calculated(), 0x36);
    ASSERT_EQ(search_response.device_hardware->description_type_code, KNX::Description_Type_Code::DEVICE_INFO);
    ASSERT_EQ(search_response.device_hardware->medium, KNX::Medium_Code::TP1);
    ASSERT_EQ(search_response.device_hardware->device_status, 0x01);
    ASSERT_EQ(search_response.device_hardware->individual_address, 0x1100);
    ASSERT_EQ(search_response.device_hardware->project_installation_identifier, 0x0011);
    std::array<uint8_t, 6> expected_serial_number{{0x00, 0x01, 0x11, 0x11, 0x11, 0x11}};
    ASSERT_EQ(search_response.device_hardware->device_serial_number.serial_number, expected_serial_number);
    std::array<uint8_t, 4> expected_device_routing_multicast_address{{224, 0, 23, 12}};
    ASSERT_EQ(search_response.device_hardware->device_routing_multicast_address, expected_device_routing_multicast_address);
    std::array<uint8_t, 6> expected_device_mac_address{{0x45, 0x49, 0x42, 0x6E, 0x65, 0x74}};
    ASSERT_EQ(search_response.device_hardware->device_mac_address, expected_device_mac_address);
    std::array<char, 30> expected_device_friendly_name{{'M', 'Y', 'H', 'O', 'M', 'E', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
    ASSERT_EQ(search_response.device_hardware->device_friendly_name, expected_device_friendly_name);

    ASSERT_TRUE(search_response.supported_service_families);
    ASSERT_EQ(search_response.supported_service_families->structure_length, 0x0A);
    ASSERT_EQ(search_response.supported_service_families->structure_length_calculated(), 0x0A);
    ASSERT_EQ(search_response.supported_service_families->description_type_code, KNX::Description_Type_Code::SUPP_SVC_FAMILIES);
    std::map<uint8_t, uint8_t> service_families = search_response.supported_service_families->service_families;
    ASSERT_EQ(service_families.size(), 4);
    ASSERT_EQ(service_families[0x02], 0x01);
    ASSERT_EQ(service_families[0x03], 0x01);
    ASSERT_EQ(service_families[0x04], 0x01);
    ASSERT_EQ(service_families[0x05], 0x01);

    ASSERT_EQ(search_response.toData(), data);
}

/**
 * MDT KNX IP Router
 */
TEST(Search_Response_Test, MDT_KNX_IP_Router)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, 0x02, // service type identifier 0202h
        0x00, 0x50, // total length, 80 octets
        0x08, // structure length (HPAI)
        0x01, // host protocol code, 01h, for UDP over IPV4
        10, 2, 1, 35, // IP address of control endpoint,
        0x0E, 0x57, // port number of control endpoint, 3671
        0x36, // structure length (DIB hardware)
        0x01, // description type code, 01h = DEVICE_INFO
        0x02, // KNX medium, 02h = TP1 (KNX TP)
        0x00, // Device Status
        0x11, 0x00, // KNX Individual Address, 1.1.0
        0x00, 0x00, // Project-Installation ID, 0000h
        0x00, 0x83, 0x70, 0x40, 0x03, 0x18, // KNX device KNX Serial Number,
        224, 0, 23, 12,// device routing multicast address
        0xCC, 0x1B, 0xE0, 0x80, 0x3F, 0xB8, // KNXnet/IP MAC address
        'M', 'D', 'T', ' ', 'K', 'N', 'X', ' ', 'I', 'P', ' ', 'R', 'o', 'u', 't', 'e', 'r', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // Device Friendly Name
        0x0C, // structure length (DIB supported service families)
        0x02, // description type code, 02h = SUPP_SVC_FAMILIES
        0x02, 0x02, // service family, 02h = KNXnet/IP Core
        0x03, 0x02, // service family, 03h = KNXnet/Device Mgmt
        0x04, 0x02, // service family, 04h = KNXnet/IP Tunnelling
        0x05, 0x02, // service family, 05h = KNXnet/IP Routing
        0x07, 0x02}; // service family

    KNX::Search_Response_Frame search_response;
    search_response.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(search_response.header_length, 0x06);
    ASSERT_EQ(search_response.protocol_version, 0x10);
    ASSERT_EQ(search_response.service_type_identifier, KNX::Service_Type_Identifier::SEARCH_RESPONSE);
    ASSERT_EQ(search_response.total_length, 80);
    ASSERT_EQ(search_response.total_length_calculated(), 80);

    ASSERT_TRUE(search_response.control_endpoint);
    ASSERT_EQ(search_response.control_endpoint->structure_length, 0x08);
    ASSERT_EQ(search_response.control_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(search_response.control_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_ip_address{{10, 2, 1, 35}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(search_response.control_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 3671);

    ASSERT_TRUE(search_response.device_hardware);
    ASSERT_EQ(search_response.device_hardware->structure_length, 0x36);
    ASSERT_EQ(search_response.device_hardware->structure_length_calculated(), 0x36);
    ASSERT_EQ(search_response.device_hardware->description_type_code, KNX::Description_Type_Code::DEVICE_INFO);
    ASSERT_EQ(search_response.device_hardware->medium, KNX::Medium_Code::TP1);
    ASSERT_EQ(search_response.device_hardware->device_status, 0x00);
    ASSERT_EQ(search_response.device_hardware->individual_address, 0x1100);
    ASSERT_EQ(search_response.device_hardware->project_installation_identifier, 0x0000);
    std::array<uint8_t, 6> expected_serial_number{{0x00, 0x83, 0x70, 0x40, 0x03, 0x18}};
    ASSERT_EQ(search_response.device_hardware->device_serial_number.serial_number, expected_serial_number);
    std::array<uint8_t, 4> expected_device_routing_multicast_address{{224, 0, 23, 12}};
    ASSERT_EQ(search_response.device_hardware->device_routing_multicast_address, expected_device_routing_multicast_address);
    std::array<uint8_t, 6> expected_device_mac_address{{0xcc, 0x1b, 0xe0, 0x80, 0x3f, 0xb8}};
    ASSERT_EQ(search_response.device_hardware->device_mac_address, expected_device_mac_address);
    std::array<char, 30> expected_device_friendly_name{{'M', 'D', 'T', ' ', 'K', 'N', 'X', ' ', 'I', 'P', ' ', 'R', 'o', 'u', 't', 'e', 'r', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
    ASSERT_EQ(search_response.device_hardware->device_friendly_name, expected_device_friendly_name);

    ASSERT_TRUE(search_response.supported_service_families);
    ASSERT_EQ(search_response.supported_service_families->structure_length, 0x0C);
    ASSERT_EQ(search_response.supported_service_families->structure_length_calculated(), 0x0C);
    ASSERT_EQ(search_response.supported_service_families->description_type_code, KNX::Description_Type_Code::SUPP_SVC_FAMILIES);
    std::map<uint8_t, uint8_t> service_families = search_response.supported_service_families->service_families;
    ASSERT_EQ(service_families.size(), 5);
    ASSERT_EQ(service_families[0x02], 0x02);
    ASSERT_EQ(service_families[0x03], 0x02);
    ASSERT_EQ(service_families[0x04], 0x02);
    ASSERT_EQ(service_families[0x05], 0x02);
    ASSERT_EQ(service_families[0x07], 0x02);

    ASSERT_EQ(search_response.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
