// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/03/Device_Configuration_Ack_Frame.h>

class Device_Configuration_Ack_Test : public ::testing::Test
{
    virtual ~Device_Configuration_Ack_Test() = default;
};

/**
 * @ingroup KNX_03_08_03_05_02
 */
TEST(Device_Configuration_Ack_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x03, // service type identifier 0311h
        0x11, // "
        0x00, // total length, 10 octets
        0x0A, // "
        0x04, // structure length of connection header
        0x15, // communication channel ID, e.g. 21
        0x00, // sequence counter
        0x00}; // reserved

    KNX::Device_Configuration_Ack_Frame device_configuration_ack;
    device_configuration_ack.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(device_configuration_ack.header_length, 0x06);
    ASSERT_EQ(device_configuration_ack.protocol_version, 0x10);
    ASSERT_EQ(device_configuration_ack.service_type_identifier, KNX::Service_Type_Identifier::DEVICE_CONFIGURATION_ACK);
    ASSERT_EQ(device_configuration_ack.total_length, 10);
    ASSERT_EQ(device_configuration_ack.total_length_calculated(), 10);
    ASSERT_EQ(device_configuration_ack.structure_length, 0x04);
    ASSERT_EQ(device_configuration_ack.structure_length_calculated(), 0x04);
    ASSERT_EQ(device_configuration_ack.communication_channel_id, 21);
    ASSERT_EQ(device_configuration_ack.sequence_counter, 0);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
