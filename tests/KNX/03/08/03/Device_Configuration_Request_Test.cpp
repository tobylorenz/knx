// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/03/Device_Configuration_Request_Frame.h>

class Device_Configuration_Request_Test : public ::testing::Test
{
    virtual ~Device_Configuration_Request_Test() = default;
};

/**
 * @ingroup KNX_03_08_03_05_01
 */
TEST(Device_Configuration_Request_Test, Example)
{
    constexpr uint8_t l = 4;
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x03, // service type identifier 0310h
        0x10, // "
        0x00, // total length, nn octets
        l + 10, // "
        0x04, // structure length of connection header
        0x15, // communication channel ID, e.g. 21
        0x00, // sequence counter
        0x00, // reserved
        0xFC, // Message Code, e.g. M_PropRead.req
        0x00, // Service Information
        0x00, // "
        0x00}; // "
    data.resize(l + 10);

    KNX::Device_Configuration_Request_Frame device_configuration_request;
    device_configuration_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(device_configuration_request.header_length, 0x06);
    ASSERT_EQ(device_configuration_request.protocol_version, 0x10);
    ASSERT_EQ(device_configuration_request.service_type_identifier, KNX::Service_Type_Identifier::DEVICE_CONFIGURATION_REQUEST);
    ASSERT_EQ(device_configuration_request.total_length, l + 10);
    ASSERT_EQ(device_configuration_request.total_length_calculated(), l + 10);
    ASSERT_EQ(device_configuration_request.structure_length, 0x04);
    ASSERT_EQ(device_configuration_request.structure_length_calculated(), 0x04);
    ASSERT_EQ(device_configuration_request.communication_channel_id, 21);
    ASSERT_EQ(device_configuration_request.sequence_counter, 0);

    std::vector<uint8_t> expected_cemi_frame_data{0xFC, 0x00, 0x00, 0x00};
    ASSERT_EQ(device_configuration_request.cemi_frame_data, expected_cemi_frame_data);

    ASSERT_EQ(device_configuration_request.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
