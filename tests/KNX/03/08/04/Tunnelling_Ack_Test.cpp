// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/04/Tunnelling_Ack_Frame.h>

class Tunnelling_Ack_Test : public ::testing::Test
{
    virtual ~Tunnelling_Ack_Test() = default;
};

/**
 * @ingroup KNX_03_08_04_05_02
 */
TEST(Tunnelling_Ack_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x04, // service type identifier 0421h
        0x21, // "
        0x00, // total length, 10 octets
        0x0A, // "
        0x04, // structure length of connection header
        0x15, // communication channel ID, e.g. 21
        0x00, // sequence counter
        0x00}; // status, e.g. 00h (NO_ERROR)

    KNX::Tunnelling_Ack_Frame tunnelling_ack;
    tunnelling_ack.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(tunnelling_ack.header_length, 0x06);
    ASSERT_EQ(tunnelling_ack.protocol_version, 0x10);
    ASSERT_EQ(tunnelling_ack.service_type_identifier, KNX::Service_Type_Identifier::TUNNELING_ACK);
    ASSERT_EQ(tunnelling_ack.total_length, 10);
    ASSERT_EQ(tunnelling_ack.total_length_calculated(), 10);
    ASSERT_EQ(tunnelling_ack.structure_length, 0x04);
    ASSERT_EQ(tunnelling_ack.structure_length_calculated(), 0x04);
    ASSERT_EQ(tunnelling_ack.communication_channel_id, 21);
    ASSERT_EQ(tunnelling_ack.sequence_counter, 0);
    ASSERT_EQ(tunnelling_ack.status, static_cast<uint8_t>(KNX::Error_Code::E_NO_ERROR));

    ASSERT_EQ(tunnelling_ack.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
