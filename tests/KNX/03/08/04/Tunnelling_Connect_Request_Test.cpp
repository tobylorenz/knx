// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/Connect_Request_Frame.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/04/Tunnelling_Connection_Request_Information.h>

class Tunnelling_Connection_Request_Test : public ::testing::Test
{
    virtual ~Tunnelling_Connection_Request_Test() = default;
};

/**
 * MDT KNX IP Router
 */
TEST(Tunnelling_Connection_Request_Test, MDT_KNX_IP_Router)
{
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> control_hpai = std::make_shared<KNX::IP_Host_Protocol_Address_Information>();
    control_hpai->ip_address = {10, 2, 1, 35};
    control_hpai->ip_port_number = 44473;
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> data_hpai = std::make_shared<KNX::IP_Host_Protocol_Address_Information>();
    data_hpai->ip_address = {10, 2, 1, 153};
    data_hpai->ip_port_number = 33673;
    std::shared_ptr<KNX::Tunnelling_Connection_Request_Information> cri = std::make_shared<KNX::Tunnelling_Connection_Request_Information>();
    cri->layer = KNX::Tunnelling_Layer_Type_Code::TUNNEL_BUSMONITOR;
    KNX::Connect_Request_Frame connect_request_frame;
    connect_request_frame.control_endpoint = std::move(control_hpai);
    connect_request_frame.data_endpoint = std::move(data_hpai);
    connect_request_frame.connection_request_information = std::move(cri);

    std::vector<uint8_t> expected_data {
        0x06, // header size
        0x10, // protocol version
        0x02, 0x05, // service type identifier 0x205h
        0x00, 0x1A, // total length, 26 octets
        0x08, // structure length
        0x01, // host protocol code, 01h, for UDP over IPv4
        10, 2, 1, 35, // IP address of control endpoint,
        0xad, 0xB9, // port number of control endpoint,
        0x08, // structure length
        0x01, // host protocol code, 01h, for UDP over IPv4
        10, 2, 1, 153, // IP address of data endpoint,
        0x83, 0x89, // port number of control endpoint,
        0x04, // structure length of CRD for TUNNELING_CONNECTION
        0x04, // connection type code, 04h, TUNNEL_CONNECTION
        0x80, // KNX layer, TUNNEL_BUSMONITOR
        0x00}; // reserved

    ASSERT_EQ(connect_request_frame.toData(), expected_data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
