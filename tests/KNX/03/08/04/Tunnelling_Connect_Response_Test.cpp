// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/Connect_Response_Frame.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/04/Tunnelling_Connection_Response_Data_Block.h>

class Tunnelling_Connect_Response_Test : public ::testing::Test
{
    virtual ~Tunnelling_Connect_Response_Test() = default;
};

/**
 * MDT KNX IP Router (E_TUNNELING_LAYER)
 */
TEST(Tunnelling_Connect_Response_Test, MDT_KNX_IP_Router_E_TUNNELING_LAYER)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, 0x06, // service type identifier 0x206h
        0x00, 0x08, // total length, 8 octets
        0x00, // communication channel ID, 0
        0x29, // status code (E_TUNNELING_LAYER)
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; // padding

    KNX::Connect_Response_Frame connect_response;
    connect_response.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(connect_response.header_length, 0x06);
    ASSERT_EQ(connect_response.protocol_version, 0x10);
    ASSERT_EQ(connect_response.service_type_identifier, KNX::Service_Type_Identifier::CONNECT_RESPONSE);
    ASSERT_EQ(connect_response.total_length, 8);
    ASSERT_EQ(connect_response.total_length_calculated(), 8);
    ASSERT_EQ(connect_response.communication_channel_id, 0);
    ASSERT_EQ(connect_response.status, KNX::Error_Code::E_TUNNELING_LAYER);
}

/**
 * MDT KNX IP Router
 */
TEST(Tunnelling_Connect_Response_Test, MDT_KNX_IP_Router)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x02, 0x06, // service type identifier 0x206h
        0x00, 0x14, // total length, 20 octets
        0x11, // communication channel ID, 0x11
        0x00, // status code (E_NO_ERROR)
        0x08, // structure length
        0x01, // host protocol
        0x0A, 0x02, 0x01, 0x23, // IP address
        0x0E, 0x57, // IP port
        0x04, // structure length
        0x04, // connection type: tunnelling connection
        0xFF, // individual address
        0xF2};

    KNX::Connect_Response_Frame connect_response;
    connect_response.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(connect_response.header_length, 0x06);
    ASSERT_EQ(connect_response.protocol_version, 0x10);
    ASSERT_EQ(connect_response.service_type_identifier, KNX::Service_Type_Identifier::CONNECT_RESPONSE);
    ASSERT_EQ(connect_response.total_length, 20);
    ASSERT_EQ(connect_response.total_length_calculated(), 20);
    ASSERT_EQ(connect_response.communication_channel_id, 17);
    ASSERT_EQ(connect_response.status, KNX::Error_Code::E_NO_ERROR);

    ASSERT_TRUE(connect_response.data_endpoint);
    ASSERT_EQ(connect_response.data_endpoint->structure_length, 8);
    ASSERT_EQ(connect_response.data_endpoint->structure_length_calculated(), 8);
    ASSERT_EQ(connect_response.data_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(connect_response.data_endpoint);
    std::array<uint8_t, 4> expected_ip_address{{10, 2, 1, 35}};
    ASSERT_EQ(ihpai->ip_address, expected_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 3671);

    ASSERT_TRUE(connect_response.connection_response_data_block);
    ASSERT_EQ(connect_response.connection_response_data_block->structure_length, 4);
    ASSERT_EQ(connect_response.connection_response_data_block->structure_length_calculated(), 4);
    ASSERT_EQ(connect_response.connection_response_data_block->connection_type_code, KNX::Connection_Type_Code::TUNNEL_CONNECTION);
    std::shared_ptr<KNX::Tunnelling_Connection_Response_Data_Block> tcrdb = std::dynamic_pointer_cast<KNX::Tunnelling_Connection_Response_Data_Block>(connect_response.connection_response_data_block);
    ASSERT_EQ(tcrdb->individual_address, 0xfff2);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
