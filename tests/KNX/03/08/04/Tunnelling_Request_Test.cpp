// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/04/Tunnelling_Request_Frame.h>

class Tunnelling_Request : public ::testing::Test
{
    virtual ~Tunnelling_Request() = default;
};

/**
 * @ingroup KNX_03_08_04_05_01
 */
TEST(Tunnelling_Request, Example)
{
    constexpr uint8_t l = 6; // not cEMI frame header, but only Service Information bytes
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x04, // service type identifier 0420h
        0x20, // "
        0x00, // total length, L+12 octets
        l + 12, // "
        0x04, // structure length of connection header // @note Spec shows 0x06 here
        0x15, // communication channel ID, e.g. 21
        0x00, // sequence counter
        0x00, // reserved
        0x11, // message code (e.g. L_Data.req message)
        0x00, // additional information (none)
        0x00, // Service Information (L bytes)
        0x00, // "
        0x00}; // "
    data.resize(l + 12);

    KNX::Tunnelling_Request_Frame tunnelling_request;
    tunnelling_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(tunnelling_request.header_length, 0x06);
    ASSERT_EQ(tunnelling_request.protocol_version, 0x10);
    ASSERT_EQ(tunnelling_request.service_type_identifier, KNX::Service_Type_Identifier::TUNNELING_REQUEST);
    ASSERT_EQ(tunnelling_request.total_length, l + 12);
    ASSERT_EQ(tunnelling_request.total_length_calculated(), l + 12);
    ASSERT_EQ(tunnelling_request.structure_length, 0x04);
    ASSERT_EQ(tunnelling_request.structure_length_calculated(), 0x04);
    ASSERT_EQ(tunnelling_request.communication_channel_id, 21);
    ASSERT_EQ(tunnelling_request.sequence_counter, 0);

    std::vector<uint8_t> expected_cemi_frame_data{0x11, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    ASSERT_EQ(tunnelling_request.cemi_frame_data, expected_cemi_frame_data);

    ASSERT_EQ(tunnelling_request.toData(), data);
}

/**
 * MDT KNX IP Router
 */
TEST(Tunnelling_Request, MDT_KNX_IP_Router)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x04, // service type identifier 0420h
        0x20, // "
        0x00, // total length, 23 octets
        0x17, // "
        0x04, // structure length of connection header
        0x11, // communication channel ID, e.g. 0x11
        0x00, // sequence counter
        0x00, // reserved
        0x29, // message code (e.g. L_Data.ind message)
        0x00, // additional information (none)
        0xBC, // Service Information (L bytes)
        0xE0, // "
        0x00, // "
        0x00, // "
        0x18, // "
        0x01, // "
        0x03, // "
        0x00, // "
        0x80, // "
        0x0E, // "
        0xF9}; // "

    KNX::Tunnelling_Request_Frame tunnelling_request;
    tunnelling_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(tunnelling_request.header_length, 0x06);
    ASSERT_EQ(tunnelling_request.protocol_version, 0x10);
    ASSERT_EQ(tunnelling_request.service_type_identifier, KNX::Service_Type_Identifier::TUNNELING_REQUEST);
    ASSERT_EQ(tunnelling_request.total_length, 23);
    ASSERT_EQ(tunnelling_request.total_length_calculated(), 23);
    ASSERT_EQ(tunnelling_request.structure_length, 0x04);
    ASSERT_EQ(tunnelling_request.structure_length_calculated(), 0x04);
    ASSERT_EQ(tunnelling_request.communication_channel_id, 0x11);
    ASSERT_EQ(tunnelling_request.sequence_counter, 0);

    std::vector<uint8_t> expected_cemi_frame_data{0x29, 0x00, 0xBC, 0xE0, 0x00, 0x00, 0x18, 0x01, 0x03, 0x00, 0x80, 0x0E, 0xF9};
    ASSERT_EQ(tunnelling_request.cemi_frame_data, expected_cemi_frame_data);

    ASSERT_EQ(tunnelling_request.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
