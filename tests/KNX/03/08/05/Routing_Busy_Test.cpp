// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/05/Routing_Busy_Frame.h>

class Routing_Busy_Test : public ::testing::Test
{
    virtual ~Routing_Busy_Test() = default;
};

/**
 * @ingroup KNX_03_08_05_06_03
 */
TEST(Routing_Busy_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x05, // service type identifier 0532h
        0x32, // "
        0x00, // total length, 0Ch octets
        0x0C, // "
        0x04, // structure length
        0x00, // device state
        0x00, // number of milliseconds to wait,
        0x64, // e.g. 100 ms
        0x00, // routing busy control value
        0x00}; // (default value: 0000h)

    KNX::Routing_Busy_Frame routing_busy;
    routing_busy.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(routing_busy.header_length, 0x06);
    ASSERT_EQ(routing_busy.protocol_version, 0x10);
    ASSERT_EQ(routing_busy.service_type_identifier, KNX::Service_Type_Identifier::ROUTING_BUSY);
    ASSERT_EQ(routing_busy.total_length, 12);
    ASSERT_EQ(routing_busy.total_length_calculated(), 12);
    ASSERT_EQ(routing_busy.structure_length, 0x04);
    ASSERT_EQ(routing_busy.structure_length_calculated(), 0x04);
    ASSERT_EQ(routing_busy.device_state, 0);
    ASSERT_EQ(routing_busy.routing_busy_wait_time, 100);
    ASSERT_EQ(routing_busy.routing_busy_control_field, 0);

    ASSERT_EQ(routing_busy.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
