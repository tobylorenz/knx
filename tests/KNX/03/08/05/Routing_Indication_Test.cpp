// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/05/Routing_Indication_Frame.h>

class Routing_Indication_Test : public ::testing::Test
{
    virtual ~Routing_Indication_Test() = default;
};

/**
 * @ingroup KNX_03_08_05_06_01
 */
TEST(Routing_Indication_Test, Example)
{
    constexpr uint8_t l = 3; // not cEMI frame header, but only Service Information bytes
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x05, // service type identifier 0530h
        0x30, // "
        0x00,  // total length, L+08h octets
        l + 8, // "
        0x11, // message code (e.g. L_Data.req message)
        0x00, // additional information (none)
        0x00, // Service Information (L bytes)
        0x00, // ""
        0x00}; // ""
    data.resize(l + 8);

    KNX::Routing_Indication_Frame routing_indication;
    routing_indication.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(routing_indication.header_length, 0x06);
    ASSERT_EQ(routing_indication.protocol_version, 0x10);
    ASSERT_EQ(routing_indication.service_type_identifier, KNX::Service_Type_Identifier::ROUTING_INDICATION);
    ASSERT_EQ(routing_indication.total_length, l + 8);
    ASSERT_EQ(routing_indication.total_length_calculated(), l + 8);

    std::vector<uint8_t> expected_cemi_frame_data{0x11, 0x00, 0x00, 0x00, 0x00};
    ASSERT_EQ(routing_indication.cemi_frame_data, expected_cemi_frame_data);

    ASSERT_EQ(routing_indication.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
