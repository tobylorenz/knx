// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/05/Routing_Lost_Message_Frame.h>

class Routing_Lost_Message_Test : public ::testing::Test
{
    virtual ~Routing_Lost_Message_Test() = default;
};

/**
 * @ingroup KNX_03_08_05_06_02
 */
TEST(Routing_Lost_Message_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x05, // service type identifier 0531h
        0x31, // "
        0x00, // total length, 0Ah octets
        0x0A, // "
        0x04, // structure length
        0x00, // device state
        0x00, // number of lost messages,
        0x05}; // e.g. 5

    KNX::Routing_Lost_Message_Frame routing_lost_message;
    routing_lost_message.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(routing_lost_message.header_length, 0x06);
    ASSERT_EQ(routing_lost_message.protocol_version, 0x10);
    ASSERT_EQ(routing_lost_message.service_type_identifier, KNX::Service_Type_Identifier::ROUTING_LOST_MESSAGE);
    ASSERT_EQ(routing_lost_message.total_length, 0x0A);
    ASSERT_EQ(routing_lost_message.total_length_calculated(), 0x0A);
    ASSERT_EQ(routing_lost_message.structure_length, 0x04);
    ASSERT_EQ(routing_lost_message.structure_length_calculated(), 0x04);
    ASSERT_EQ(routing_lost_message.device_state, 0x00);
    ASSERT_EQ(routing_lost_message.number_of_lost_messages, 5);

    ASSERT_EQ(routing_lost_message.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
