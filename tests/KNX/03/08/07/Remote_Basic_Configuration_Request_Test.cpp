// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/IP_Config_DIB.h>
#include <KNX/03/08/02/IP_Current_Config_DIB.h>
#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/02/Addresses_DIB.h>
#include <KNX/03/08/07/Remote_Basic_Configuration_Request_Frame.h>

class Remote_Basic_Configuration_Request_Test : public ::testing::Test
{
    virtual ~Remote_Basic_Configuration_Request_Test() = default;
};

/**
 * @ingroup KNX_03_08_07_05_03
 */
TEST(Remote_Basic_Configuration_Request_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x07, // service type identifier 0742h
        0x42, // "
        0x00, // total length, 32 octets
        0x20, // "
        0x08, // structure length of HPAI
        0x01, // host protocol code, e.g. 01h, for UDP over IPv4
        0xE0, // IP multicast address
        0x00, // e.g. 224.0.23.12
        0x17, // (System Routing Multicast Address)
        0x0C, // "
        0x0E, // port number of control endpoint, 3671
        0x57, // "
        0x02, // structure length of SELECTOR
        0x01, // Programming Mode Selector
        0x10, // structure length of DIB IP Config
        0x03, // Description Type Code
        0xC0, // IP address
        0xA8, // e.g. 192.168.3.12
        0x03, // "
        0x0C, // "
        0xFF, // subnet mask
        0xFF, // e.g. 255.255.255.0
        0xFF, // "
        0x00, // "
        0xC0, // default gateway IP address
        0xA8, // e.g. 192.168.3.1
        0x03, // "
        0x01, // "
        0x00, // IP capabilities (not writeable -> 00h)
        0x01}; // IP assignment method (e.g. manually)

    KNX::Remote_Basic_Configuration_Request_Frame remote_basic_configuration_request;
    remote_basic_configuration_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(remote_basic_configuration_request.header_length, 0x06);
    ASSERT_EQ(remote_basic_configuration_request.protocol_version, 0x10);
    ASSERT_EQ(remote_basic_configuration_request.service_type_identifier, KNX::Service_Type_Identifier::REMOTE_BASIC_CONFIGURATION_REQUEST);
    ASSERT_EQ(remote_basic_configuration_request.total_length, 32);

    ASSERT_TRUE(remote_basic_configuration_request.discovery_endpoint);
    ASSERT_EQ(remote_basic_configuration_request.discovery_endpoint->structure_length, 0x08);
    ASSERT_EQ(remote_basic_configuration_request.discovery_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(remote_basic_configuration_request.discovery_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_ip_multicast_address{{224, 0, 23, 12}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(remote_basic_configuration_request.discovery_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_ip_multicast_address);
    ASSERT_EQ(ihpai->ip_port_number, 3671);

    ASSERT_TRUE(remote_basic_configuration_request.selector);
    ASSERT_EQ(remote_basic_configuration_request.selector->structure_length, 0x02);
    ASSERT_EQ(remote_basic_configuration_request.selector->structure_length_calculated(), 0x02);
    ASSERT_EQ(remote_basic_configuration_request.selector->selector_type_code, KNX::Selector_Type_Code::PrgMode);
    std::shared_ptr<KNX::PrgMode_Selector> pms = std::dynamic_pointer_cast<KNX::PrgMode_Selector>(remote_basic_configuration_request.selector);
    ASSERT_TRUE(pms);

    ASSERT_EQ(remote_basic_configuration_request.description_information_blocks.size(), 1);

    ASSERT_TRUE(remote_basic_configuration_request.description_information_blocks[0]);
    ASSERT_EQ(remote_basic_configuration_request.description_information_blocks[0]->description_type_code, KNX::Description_Type_Code::IP_CONFIG);
    std::shared_ptr<KNX::IP_Config_DIB> dib0 = std::dynamic_pointer_cast<KNX::IP_Config_DIB>(remote_basic_configuration_request.description_information_blocks[0]);
    ASSERT_TRUE(dib0);
    ASSERT_EQ(dib0->structure_length, 0x10);
    ASSERT_EQ(dib0->structure_length_calculated(), 0x10);
    ASSERT_EQ(dib0->description_type_code, KNX::Description_Type_Code::IP_CONFIG);
    std::array<uint8_t, 4> expected_ip_address{{192, 168, 3, 12}};
    ASSERT_EQ(dib0->ip_address, expected_ip_address);
    std::array<uint8_t, 4> expected_subnet_mask{{255, 255, 255, 0}};
    ASSERT_EQ(dib0->subnet_mask, expected_subnet_mask);
    std::array<uint8_t, 4> expected_default_gateway{{192, 168, 3, 1}};
    ASSERT_EQ(dib0->default_gateway, expected_default_gateway);
    ASSERT_EQ(dib0->ip_capabilities, static_cast<KNX::IP_Capabilities>(0x00));
    ASSERT_EQ(dib0->ip_assignment_method, static_cast<KNX::IP_Assignment_Method>(0x01));

    ASSERT_EQ(remote_basic_configuration_request.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
