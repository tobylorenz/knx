// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/07/Remote_Diagnostic_Request_Frame.h>

class Remote_Diagnostic_Request_Test : public ::testing::Test
{
    virtual ~Remote_Diagnostic_Request_Test() = default;
};

/**
 * @ingroup KNX_03_08_07_05_01
 */
TEST(Remote_Diagnostic_Request_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x07, // service type identifier 0740h
        0x40, // "
        0x00, // total length, 16 octets
        0x10, // "
        0x08, // structure length of HPAI
        0x01, // host protocol code, e.g. 01h, for UDP over IPv4
        0xE0, // IP multicast address
        0x00, // e.g. 224.0.23.12
        0x17, // (System Routing Multicast Address)
        0x0C, // "
        0x0E, // port number of control endpoint, 3671
        0x57, // "
        0x02, // structure length of SELECTOR
        0x01}; // Programming Mode Selector

    KNX::Remote_Diagnostic_Request_Frame remote_diagnostic_request;
    remote_diagnostic_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(remote_diagnostic_request.header_length, 0x06);
    ASSERT_EQ(remote_diagnostic_request.protocol_version, 0x10);
    ASSERT_EQ(remote_diagnostic_request.service_type_identifier, KNX::Service_Type_Identifier::REMOTE_DIAGNOSTIC_REQUEST);
    ASSERT_EQ(remote_diagnostic_request.total_length, 16);
    ASSERT_EQ(remote_diagnostic_request.total_length_calculated(), 16);

    ASSERT_TRUE(remote_diagnostic_request.discovery_endpoint);
    ASSERT_EQ(remote_diagnostic_request.discovery_endpoint->structure_length, 0x08);
    ASSERT_EQ(remote_diagnostic_request.discovery_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(remote_diagnostic_request.discovery_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_ip_address{{224, 0, 23, 12}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(remote_diagnostic_request.discovery_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 3671);

    ASSERT_TRUE(remote_diagnostic_request.selector);
    ASSERT_EQ(remote_diagnostic_request.selector->structure_length, 0x02);
    ASSERT_EQ(remote_diagnostic_request.selector->structure_length_calculated(), 0x02);
    ASSERT_EQ(remote_diagnostic_request.selector->selector_type_code, KNX::Selector_Type_Code::PrgMode);
    std::shared_ptr<KNX::PrgMode_Selector> pms = std::dynamic_pointer_cast<KNX::PrgMode_Selector>(remote_diagnostic_request.selector);
    ASSERT_TRUE(pms);

    ASSERT_EQ(remote_diagnostic_request.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
