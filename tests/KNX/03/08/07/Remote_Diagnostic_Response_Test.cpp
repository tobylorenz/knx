// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/IP_Config_DIB.h>
#include <KNX/03/08/02/IP_Current_Config_DIB.h>
#include <KNX/03/08/02/Addresses_DIB.h>
#include <KNX/03/08/07/Remote_Diagnostic_Response_Frame.h>

class Remote_Diagnostic_Response_Test : public ::testing::Test
{
    virtual ~Remote_Diagnostic_Response_Test() = default;
};

/**
 * @ingroup KNX_03_08_07_05_02
 */
TEST(Remote_Diagnostic_Response_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x07, // service type identifier 0741h
        0x41, // "
        0x00, // total length, 58 octets
        0x3A, // "
        0x02, // structure length of SELECTOR
        0x01, // Programming Mode Selector
        0x10, // structure length of DIB IP Config
        0x03, // Description Type Code
        0xC0, // IP address
        0xA8, // e.g. 192.168.2.12
        0x02, // "
        0x0C, // "
        0xFF, // subnet mask
        0xFF, // e.g. 255.255.255.0
        0xFF, // "
        0x00, // "
        0xC0, // default gateway IP address
        0xA8, // e.g. 192.168.2.1
        0x02, // "
        0x01, // "
        0x02, // IP capabilities (e.g. DHCP)
        0x01, // IP assignment method (e.g. manually)
        0x14, // structure length of DIB IP Current Config
        0x04, // Description Type Code
        0xC0, // IP address
        0xA8, // e.g. 192.168.2.12
        0x02, // "
        0x0C, // "
        0xFF, // subnet mask
        0xFF, // e.g. 255.255.255.0
        0xFF, // "
        0x00, // "
        0xC0, // default gateway IP address
        0xA8, // e.g. 192.168.2.1
        0x02, // "
        0x01, // "
        0xC0, // DHCP server IP address
        0xA8, // e.g. 192.168.2.1
        0x02, // "
        0x01, // "
        0x04, // Current IP assignment method (e.g. DHCP)
        0x00, // reserved
        0x0E, // structure length of DIB KNX Addresses
        0x05, // Description Type Code
        0x11, // KNX individual address (e.g. 1.1.0)
        0x00, // "
        0x11, // Additional individual address (e.g. 1.1.255)
        0xFF, // "
        0x11, // Additional individual address (e.g. 1.1.254)
        0xFE, // "
        0x11, // Additional individual address (e.g. 1.1.200)
        0xC8, // "
        0x11, // Additional individual address (e.g. 1.1.199)
        0xC7, // "
        0x11, // Additional individual address (e.g. 1.1.150)
        0x96}; // "

    KNX::Remote_Diagnostic_Response_Frame remote_diagnostic_response;
    remote_diagnostic_response.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(remote_diagnostic_response.header_length, 0x06);
    ASSERT_EQ(remote_diagnostic_response.protocol_version, 0x10);
    ASSERT_EQ(remote_diagnostic_response.service_type_identifier, KNX::Service_Type_Identifier::REMOTE_DIAGNOSTIC_RESPONSE);
    ASSERT_EQ(remote_diagnostic_response.total_length, 58);

    ASSERT_TRUE(remote_diagnostic_response.selector);
    ASSERT_EQ(remote_diagnostic_response.selector->structure_length, 0x02);
    ASSERT_EQ(remote_diagnostic_response.selector->structure_length_calculated(), 0x02);
    ASSERT_EQ(remote_diagnostic_response.selector->selector_type_code, KNX::Selector_Type_Code::PrgMode);
    std::shared_ptr<KNX::PrgMode_Selector> pms = std::dynamic_pointer_cast<KNX::PrgMode_Selector>(remote_diagnostic_response.selector);
    ASSERT_TRUE(pms);

    ASSERT_EQ(remote_diagnostic_response.description_information_blocks.size(), 3);

    ASSERT_TRUE(remote_diagnostic_response.description_information_blocks[0]);
    ASSERT_EQ(remote_diagnostic_response.description_information_blocks[0]->description_type_code, KNX::Description_Type_Code::IP_CONFIG);
    std::shared_ptr<KNX::IP_Config_DIB> dib0 = std::dynamic_pointer_cast<KNX::IP_Config_DIB>(remote_diagnostic_response.description_information_blocks[0]);
    ASSERT_TRUE(dib0);
    ASSERT_EQ(dib0->structure_length, 0x10);
    ASSERT_EQ(dib0->structure_length_calculated(), 0x10);
    ASSERT_EQ(dib0->description_type_code, KNX::Description_Type_Code::IP_CONFIG);
    std::array<uint8_t, 4> expected_ip_address{{192, 168, 2, 12}};
    ASSERT_EQ(dib0->ip_address, expected_ip_address);
    std::array<uint8_t, 4> expected_subnet_mask{{255, 255, 255, 0}};
    ASSERT_EQ(dib0->subnet_mask, expected_subnet_mask);
    std::array<uint8_t, 4> expected_default_gateway{{192, 168, 2, 1}};
    ASSERT_EQ(dib0->default_gateway, expected_default_gateway);
    ASSERT_EQ(dib0->ip_capabilities, static_cast<KNX::IP_Capabilities>(0x02));
    ASSERT_EQ(dib0->ip_assignment_method, static_cast<KNX::IP_Assignment_Method>(0x01));

    ASSERT_TRUE(remote_diagnostic_response.description_information_blocks[1]);
    ASSERT_EQ(remote_diagnostic_response.description_information_blocks[1]->description_type_code, KNX::Description_Type_Code::IP_CUR_CONFIG);
    std::shared_ptr<KNX::IP_Current_Config_DIB> dib1 = std::dynamic_pointer_cast<KNX::IP_Current_Config_DIB>(remote_diagnostic_response.description_information_blocks[1]);
    ASSERT_TRUE(dib1);
    ASSERT_EQ(dib1->structure_length, 0x14);
    ASSERT_EQ(dib1->structure_length_calculated(), 0x14);
    ASSERT_EQ(dib1->description_type_code, KNX::Description_Type_Code::IP_CUR_CONFIG);
    ASSERT_EQ(dib1->current_ip_address, expected_ip_address);
    ASSERT_EQ(dib1->current_subnet_mask, expected_subnet_mask);
    ASSERT_EQ(dib1->current_default_gateway, expected_default_gateway);
    ASSERT_EQ(dib1->current_ip_assignment_method, static_cast<KNX::IP_Assignment_Method>(0x04));

    ASSERT_TRUE(remote_diagnostic_response.description_information_blocks[2]);
    ASSERT_EQ(remote_diagnostic_response.description_information_blocks[2]->description_type_code, KNX::Description_Type_Code::KNX_ADDRESSES);
    std::shared_ptr<KNX::Addresses_DIB> dib2 = std::dynamic_pointer_cast<KNX::Addresses_DIB>(remote_diagnostic_response.description_information_blocks[2]);
    ASSERT_TRUE(dib2);
    ASSERT_EQ(dib2->structure_length, 0x0E);
    ASSERT_EQ(dib2->structure_length_calculated(), 0x0E);
    ASSERT_EQ(dib2->description_type_code, KNX::Description_Type_Code::KNX_ADDRESSES);
    ASSERT_EQ(dib2->individual_address, 0x1100);
    ASSERT_EQ(dib2->additional_individual_addresses.size(), 5);
    ASSERT_EQ(dib2->additional_individual_addresses[0], 0x11FF);
    ASSERT_EQ(dib2->additional_individual_addresses[1], 0x11FE);
    ASSERT_EQ(dib2->additional_individual_addresses[2], 0x11C8);
    ASSERT_EQ(dib2->additional_individual_addresses[3], 0x11C7);
    ASSERT_EQ(dib2->additional_individual_addresses[4], 0x1196);

    ASSERT_EQ(remote_diagnostic_response.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
