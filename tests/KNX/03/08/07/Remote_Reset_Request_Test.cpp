// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/07/Remote_Reset_Request_Frame.h>

class Remote_Reset_Request_Test : public ::testing::Test
{
    virtual ~Remote_Reset_Request_Test() = default;
};

/**
 * @ingroup KNX_03_08_07_05_04
 */
TEST(Remote_Reset_Request_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x10, // protocol version
        0x07, // service type identifier 0743h
        0x43, // "
        0x00, // total length, 10 octets
        0x0A, // "
        0x02, // structure length of SELECTOR
        0x01, // Programming Mode Selector
        0x01, // restart
        0x00}; // reserved

    KNX::Remote_Reset_Request_Frame remote_reset_request;
    remote_reset_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(remote_reset_request.header_length, 0x06);
    ASSERT_EQ(remote_reset_request.protocol_version, 0x10);
    ASSERT_EQ(remote_reset_request.service_type_identifier, KNX::Service_Type_Identifier::REMOTE_RESET_REQUEST);
    ASSERT_EQ(remote_reset_request.total_length, 10);

    ASSERT_TRUE(remote_reset_request.selector);
    ASSERT_EQ(remote_reset_request.selector->structure_length, 0x02);
    ASSERT_EQ(remote_reset_request.selector->structure_length_calculated(), 0x02);
    ASSERT_EQ(remote_reset_request.selector->selector_type_code, KNX::Selector_Type_Code::PrgMode);
    std::shared_ptr<KNX::PrgMode_Selector> pms = std::dynamic_pointer_cast<KNX::PrgMode_Selector>(remote_reset_request.selector);
    ASSERT_TRUE(pms);

    ASSERT_EQ(remote_reset_request.reset_command, KNX::Reset_Command_Type_Code::Restart);

    ASSERT_EQ(remote_reset_request.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
