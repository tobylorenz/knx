# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# tests
add_executable(Secure_Channel_Authorize_Test "")
target_sources(Secure_Channel_Authorize_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/Secure_Channel_Authorize_Test.cpp)
target_link_libraries(Secure_Channel_Authorize_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET Secure_Channel_Authorize_Test)
set_target_properties(Secure_Channel_Authorize_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON)

add_executable(Secure_Channel_Request_Test "")
target_sources(Secure_Channel_Request_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/Secure_Channel_Request_Test.cpp)
target_link_libraries(Secure_Channel_Request_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET Secure_Channel_Request_Test)
set_target_properties(Secure_Channel_Request_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON)

add_executable(Secure_Channel_Response_Test "")
target_sources(Secure_Channel_Response_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/Secure_Channel_Response_Test.cpp)
target_link_libraries(Secure_Channel_Response_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET Secure_Channel_Response_Test)
set_target_properties(Secure_Channel_Response_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON)

add_executable(Secure_Channel_Status_Test "")
target_sources(Secure_Channel_Status_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/Secure_Channel_Status_Test.cpp)
target_link_libraries(Secure_Channel_Status_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET Secure_Channel_Status_Test)
set_target_properties(Secure_Channel_Status_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON)

add_executable(Secure_Group_Sync_Request_Test "")
target_sources(Secure_Group_Sync_Request_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/Secure_Group_Sync_Request_Test.cpp)
target_link_libraries(Secure_Group_Sync_Request_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET Secure_Group_Sync_Request_Test)
set_target_properties(Secure_Group_Sync_Request_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON)

add_executable(Secure_Group_Sync_Response_Test "")
target_sources(Secure_Group_Sync_Response_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/Secure_Group_Sync_Response_Test.cpp)
target_link_libraries(Secure_Group_Sync_Response_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET Secure_Group_Sync_Response_Test)
set_target_properties(Secure_Group_Sync_Response_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON)

add_executable(Secure_Wrapper_Test "")
target_sources(Secure_Wrapper_Test
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/Secure_Wrapper_Test.cpp)
target_link_libraries(Secure_Wrapper_Test
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET Secure_Wrapper_Test)
set_target_properties(Secure_Wrapper_Test PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 20
    CXX_STANDARD_REQUIRED ON)
