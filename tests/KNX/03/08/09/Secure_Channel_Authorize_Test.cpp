// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/09/Secure_Channel_Authorize_Frame.h>

class Secure_Channel_Authorize_Test : public ::testing::Test
{
    virtual ~Secure_Channel_Authorize_Test() = default;
};

/**
 * @ingroup KNX_AN159_A_03_01
 */
TEST(Secure_Channel_Authorize_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x13, // protocol version
        0xAA, // service_type_identifier AA03h
        0x03, // SECURE_CHANNEL_AUTHORIZE
        0x00, // total length, 24 octets
        0x18, // "
        0x00, // Authorization Context
        0x01, // e.g. 01h management client full access
        0x00}; // Message Authentication Code
    data.resize(24);

    KNX::Secure_Channel_Authorize_Frame secure_channel_authorize;
    secure_channel_authorize.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(secure_channel_authorize.header_length, 0x06);
    ASSERT_EQ(secure_channel_authorize.protocol_version, KNX::KNXNETIP_VERSION_13);
    ASSERT_EQ(secure_channel_authorize.service_type_identifier, KNX::Service_Type_Identifier::SECURE_CHANNEL_AUTHORIZE);
    ASSERT_EQ(secure_channel_authorize.total_length, 24);
    ASSERT_EQ(secure_channel_authorize.total_length_calculated(), 24);
    ASSERT_EQ(secure_channel_authorize.authorization_context, 0x01);
    std::array<uint8_t, 16> expected_message_authentication_code{};
    ASSERT_EQ(secure_channel_authorize.message_authentication_code, expected_message_authentication_code);

    ASSERT_EQ(secure_channel_authorize.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
