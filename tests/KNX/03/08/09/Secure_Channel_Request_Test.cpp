// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/02/IP_Host_Protocol_Address_Information.h>
#include <KNX/03/08/09/Secure_Channel_Request_Frame.h>

class Secure_Channel_Request_Test : public ::testing::Test
{
    virtual ~Secure_Channel_Request_Test() = default;
};

/**
 * @ingroup KNX_AN159_A_01_01
 */
TEST(Secure_Channel_Request_Test, Unencrypted_Binary_Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x13, // protocol version
        0xAA, // service type identifier AA01h
        0x01, // SECURE_CHANNEL_REQUEST
        0x00, // total length, 50 octets
        0x32, // "
        0x08, // structure length
        0x01, // host protocol code, e.g. 01h for UDP over IPv4
        0xC0, // IPv4 address of client's control endpoint,
        0xA8, // e.g. 192.168.200.12
        0xC8, // "
        0x0C, // "
        0xC3, // UDP port number of client's control endpoint,
        0xB4, // e.g. 50100
        0x00}; // Diffie-Hellman Client Public Value X
    data.resize(50);

    KNX::Secure_Channel_Request_Frame secure_channel_request;
    secure_channel_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(secure_channel_request.header_length, 0x06);
    ASSERT_EQ(secure_channel_request.protocol_version, KNX::KNXNETIP_VERSION_13);
    ASSERT_EQ(secure_channel_request.service_type_identifier, KNX::Service_Type_Identifier::SECURE_CHANNEL_REQUEST);
    ASSERT_EQ(secure_channel_request.total_length, 50);
    ASSERT_EQ(secure_channel_request.total_length_calculated(), 50);

    ASSERT_TRUE(secure_channel_request.control_endpoint);
    ASSERT_EQ(secure_channel_request.control_endpoint->structure_length, 0x08);
    ASSERT_EQ(secure_channel_request.control_endpoint->structure_length_calculated(), 0x08);
    ASSERT_EQ(secure_channel_request.control_endpoint->host_protocol_code, KNX::Host_Protocol_Code::IPV4_UDP);
    std::array<uint8_t, 4> expected_ip_address{{192, 168, 200, 12}};
    std::shared_ptr<KNX::IP_Host_Protocol_Address_Information> ihpai = std::dynamic_pointer_cast<KNX::IP_Host_Protocol_Address_Information>(secure_channel_request.control_endpoint);
    ASSERT_EQ(ihpai->ip_address, expected_ip_address);
    ASSERT_EQ(ihpai->ip_port_number, 50100);

    std::array<uint8_t, 36> expected_client_public_value{};
    ASSERT_EQ(secure_channel_request.client_public_value, expected_client_public_value);

    ASSERT_EQ(secure_channel_request.toData(), data);
}

/**
 * @ingroup KNX_AN159_A_01_02
 */
TEST(Secure_Channel_Request_Test, Encrypted_Binary_Example)
{
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
