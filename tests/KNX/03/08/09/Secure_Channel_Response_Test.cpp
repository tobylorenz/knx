// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/09/Secure_Channel_Response_Frame.h>

class Secure_Channel_Response_Test : public ::testing::Test
{
    virtual ~Secure_Channel_Response_Test() = default;
};

/**
 * @ingroup KNX_AN159_A_02_01
 */
TEST(Secure_Channel_Response_Test, Unencrypted_Binary_Example_Success)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x13, // protocol version
        0xAA, // service type identifier AA02h
        0x02, // SECURE_CHANNEL_RESPONSE
        0x00, // total length, 60 octets
        0x3C, // "
        0x00, // Secure Channel Index, e.g. 01h
        0x01, // "
        0x00, // Diffie-Hellman Server Public Value Y
        // ...
        0x00}; // Message Authentication Code
    data.resize(60);

    KNX::Secure_Channel_Response_Frame secure_channel_response;
    secure_channel_response.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(secure_channel_response.header_length, 0x06);
    ASSERT_EQ(secure_channel_response.protocol_version, KNX::KNXNETIP_VERSION_13);
    ASSERT_EQ(secure_channel_response.service_type_identifier, KNX::Service_Type_Identifier::SECURE_CHANNEL_RESPONSE);
    ASSERT_EQ(secure_channel_response.total_length, 60);
    ASSERT_EQ(secure_channel_response.total_length_calculated(), 60);
    ASSERT_EQ(secure_channel_response.secure_channel_index, 0x01);
    std::array<uint8_t, 36> expected_server_public_value{};
    ASSERT_EQ(secure_channel_response.server_public_value, expected_server_public_value);
    std::array<uint8_t, 16> expected_message_authentication_code{};
    ASSERT_EQ(secure_channel_response.message_authentication_code, expected_message_authentication_code);

    ASSERT_EQ(secure_channel_response.toData(), data);
}

/**
 * @ingroup KNX_AN159_A_02_02
 */
TEST(Secure_Channel_Response_Test, Unencrypted_Binary_Example_Error)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x13, // protocol version
        0xAA, // service type identifier AA02h
        0x02, // SECURE_CHANNEL_RESPONSE
        0x00, // total length, 8 octets
        0x08, // "
        0x00, // Secure Channel Index, 00h (error)
        0x00}; // "

    KNX::Secure_Channel_Response_Frame secure_channel_response;
    secure_channel_response.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(secure_channel_response.header_length, 0x06);
    ASSERT_EQ(secure_channel_response.protocol_version, KNX::KNXNETIP_VERSION_13);
    ASSERT_EQ(secure_channel_response.service_type_identifier, KNX::Service_Type_Identifier::SECURE_CHANNEL_RESPONSE);
    ASSERT_EQ(secure_channel_response.total_length, 8);
    ASSERT_EQ(secure_channel_response.total_length_calculated(), 8);
    ASSERT_EQ(secure_channel_response.secure_channel_index, 0x00);

    ASSERT_EQ(secure_channel_response.toData(), data);
}

/**
 * @ingroup KNX_AN159_A_02_03
 */
TEST(Secure_Channel_Response_Test, Encrypted_Binary_Example_Success)
{
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
