// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/09/Secure_Channel_Status_Frame.h>

class Secure_Channel_Status_Test : public ::testing::Test
{
    virtual ~Secure_Channel_Status_Test() = default;
};

/**
 * @ingroup KNX_AN159_A_03_03
 */
TEST(Secure_Channel_Status_Test, Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x13, // protocol version
        0xAA, // service_type_identifier AA04h
        0x04, // SECURE_CHANNEL_STATUS
        0x00, // total length, 8 octets
        0x08, // "
        0x00, // status code 00h STATUS_AUTHORIZATION_SUCCESS
        0x00}; // Reserved

    KNX::Secure_Channel_Status_Frame secure_channel_status;
    secure_channel_status.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(secure_channel_status.header_length, 0x06);
    ASSERT_EQ(secure_channel_status.protocol_version, KNX::KNXNETIP_VERSION_13);
    ASSERT_EQ(secure_channel_status.service_type_identifier, KNX::Service_Type_Identifier::SECURE_CHANNEL_STATUS);
    ASSERT_EQ(secure_channel_status.total_length, 8);
    ASSERT_EQ(secure_channel_status.total_length_calculated(), 8);
    ASSERT_EQ(secure_channel_status.status, KNX::Secure_Channel_Status_Frame::Status::STATUS_AUTHORIZATION_SUCCESS);

    ASSERT_EQ(secure_channel_status.toData(), data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
