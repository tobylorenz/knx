// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/09/Secure_Group_Sync_Request_Frame.h>

class Secure_Group_Sync_Request_Test : public ::testing::Test
{
    virtual ~Secure_Group_Sync_Request_Test() = default;
};

/**
 * @ingroup KNX_AN159_A_04_01
 */
TEST(Secure_Group_Sync_Request_Test, Unencrypted_Binary_Example)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x13, // protocol version
        0xAA, // service type identifier AA06h
        0x06, // SECURE_GROUP_SYNC_REQUEST
        0x00, // total length, 28 octets
        0x1C, // "
        0x00, // time stamp
        0x00, // e.g. 1
        0x00, // "
        0x00, // "
        0x00, // "
        0x01, // "
        0x00}; // Message Authentication Code
    data.resize(28);

    KNX::Secure_Group_Sync_Request_Frame secure_group_sync_request;
    secure_group_sync_request.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(secure_group_sync_request.header_length, 0x06);
    ASSERT_EQ(secure_group_sync_request.protocol_version, KNX::KNXNETIP_VERSION_13);
    ASSERT_EQ(secure_group_sync_request.service_type_identifier, KNX::Service_Type_Identifier::SECURE_GROUP_SYNC_REQUEST);
    ASSERT_EQ(secure_group_sync_request.total_length, 28);
    ASSERT_EQ(secure_group_sync_request.total_length_calculated(), 28);
    std::array<uint8_t, 6> expected_time_stamp{{0, 0, 0, 0, 0, 1}};
    ASSERT_EQ(secure_group_sync_request.time_stamp, expected_time_stamp);
    std::array<uint8_t, 16> expected_message_authentication_code{};
    ASSERT_EQ(secure_group_sync_request.message_authentication_code, expected_message_authentication_code);

    ASSERT_EQ(secure_group_sync_request.toData(), data);
}

/**
 * @ingroup KNX_AN159_A_04_02
 */
TEST(Secure_Group_Sync_Request_Test, Encrypted_Binary_Example)
{
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
