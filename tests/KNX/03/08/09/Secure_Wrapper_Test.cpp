// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/08/09/Secure_Wrapper_Frame.h>

class Secure_Wrapper_Test : public ::testing::Test
{
    virtual ~Secure_Wrapper_Test() = default;
};

/**
 * @ingroup KNX_AN159_A_03_01
 */
TEST(Secure_Wrapper_Test, Unencrypted_Secure_Channel_Authorize_Frame)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x13, // protocol version
        0xAA, // service type identifier AA00h
        0x00, // SECURE_WRAPPER
        0x00, // total length, 56 octets
        0x38, // "
        0x00, // Secure Channel Index
        0x01, // e.g. 01h management client full access
        0x00, // Secure Unicast Sequence Identifier
        0x00, // e.g. 01h for first telegram sent
        0x00, // to newly created secure channel
        0x00, // "
        0x00, // "
        0x01, // "
        0x00, // Security Domain Mask
        0x00, // 0000h for unicast connections
        0x06, // header size
        0x13, // protocol version
        0xAA, // service_type_identifier AA03h
        0x03, // SECURE_CHANNEL_AUTHORIZE
        0x00, // total (inner) length, 24 octets
        0x18, // "
        0x00, // Authorization Context
        0x01, // e.g. 01h management client full access
        0x00, // Message Authentication Code
        // ...
        0x00}; // Message Authentication Code
    data.resize(56);

    KNX::Secure_Wrapper_Frame secure_wrapper;
    secure_wrapper.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(secure_wrapper.header_length, 0x06);
    ASSERT_EQ(secure_wrapper.protocol_version, KNX::KNXNETIP_VERSION_13);
    ASSERT_EQ(secure_wrapper.service_type_identifier, KNX::Service_Type_Identifier::SECURE_WRAPPER);
    ASSERT_EQ(secure_wrapper.total_length, 56);
    ASSERT_EQ(secure_wrapper.total_length_calculated(), 56);
    ASSERT_EQ(secure_wrapper.secure_channel_index, 1);
    std::array<uint8_t, 6> expected_sequence_identifier_group_counter{{0, 0, 0, 0, 0, 1}};
    ASSERT_EQ(secure_wrapper.sequence_identifier_group_counter, expected_sequence_identifier_group_counter);
    ASSERT_EQ(secure_wrapper.security_domain_mask, 0x0000);
    std::vector<uint8_t> expected_original_frame{{0x06, 0x13, 0xAA, 0x03, 0x00, 0x18, 0x00, 0x01, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};
    ASSERT_EQ(secure_wrapper.original_frame, expected_original_frame);
    std::array<uint8_t, 16> expected_message_authentication_code{};
    ASSERT_EQ(secure_wrapper.message_authentication_code, expected_message_authentication_code);

    ASSERT_EQ(secure_wrapper.toData(), data);
}

/**
 * @ingroup KNX_AN159_A_03_02
 */
TEST(Secure_Wrapper_Test, Encrypted_Secure_Channel_Authorize_Frame)
{
}

/**
 * @ingroup KNX_AN159_A_03_03
 */
TEST(Secure_Wrapper_Test, Unencrypted_Secure_Channel_Status_Frame)
{
    std::vector<uint8_t> data {
        0x06, // header size
        0x13, // protocol version
        0xAA, // service type identifier AA00h
        0x00, // SECURE_WRAPPER
        0x00, // total length, 40 octets
        0x28, // "
        0x00, // Secure Channel Index
        0x01, // "
        0x00, // Secure Unicast Sequence Identifier
        0x00, // e.g. 01h for first telegram sent
        0x00, // to newly created secure channel
        0x00, // "
        0x00, // "
        0x01, // "
        0x00, // Security Domain Mask
        0x00, // 0000h for unicast connections
        0x06, // header size
        0x13, // protocol version
        0xAA, // service_type_identifier AA04h
        0x04, // SECURE_CHANNEL_STATUS
        0x00, // total (inner) length, 8 octets
        0x08, // "
        0x00, // status code 00h STATUS_AUTHORIZATION_SUCCESS
        0x00, // Reserved
        0x00}; // Message Authentication Code
    data.resize(40);

    KNX::Secure_Wrapper_Frame secure_wrapper;
    secure_wrapper.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(secure_wrapper.header_length, 0x06);
    ASSERT_EQ(secure_wrapper.protocol_version, KNX::KNXNETIP_VERSION_13);
    ASSERT_EQ(secure_wrapper.service_type_identifier, KNX::Service_Type_Identifier::SECURE_WRAPPER);
    ASSERT_EQ(secure_wrapper.total_length, 40);
    ASSERT_EQ(secure_wrapper.total_length_calculated(), 40);
    ASSERT_EQ(secure_wrapper.secure_channel_index, 1);
    std::array<uint8_t, 6> expected_sequence_identifier_group_counter{{0, 0, 0, 0, 0, 1}};
    ASSERT_EQ(secure_wrapper.sequence_identifier_group_counter, expected_sequence_identifier_group_counter);
    ASSERT_EQ(secure_wrapper.security_domain_mask, 0x0000);
    std::vector<uint8_t> expected_original_frame{{0x06, 0x13, 0xAA, 0x04, 0x00, 0x08, 0x00, 0x00}};
    ASSERT_EQ(secure_wrapper.original_frame, expected_original_frame);
    std::array<uint8_t, 16> expected_message_authentication_code{};
    ASSERT_EQ(secure_wrapper.message_authentication_code, expected_message_authentication_code);

    ASSERT_EQ(secure_wrapper.toData(), data);
}

/**
 * @ingroup KNX_AN159_A_03_04
 */
TEST(Secure_Wrapper_Test, Encrypted_Secure_Channel_Status_Frame)
{
}

/**
 * @ingroup KNX_AN159_A03_05
 */
TEST(Secure_Wrapper_Test, Unencrypted_Routing_Indication_Frame)
{
    constexpr uint16_t l = 8;
    std::vector<uint8_t> data {
        0x06, // header size
        0x13, // protocol version
        0xAA, // service type identifier AA00h
        0x00, // SECURE_WRAPPER
        0x00, // total length, L+40 octets
        l + 40, // "
        0x00, // Secure Channel Index
        0x00, // 0000h for multicast messages
        0x00, // Group Counter/Time Stamp
        0x00, // e.g. 1316565324
        0x4E, // "
        0x79, // "
        0x31, // "
        0x4C, // "
        0x00, // Security Domain Mask
        0x01, // sending to security group 0 by default
        0x06, // header size
        0x10, // protocol version
        0x05, // service_type_identifier 0530h
        0x30, // ROUTING_INDICATION
        0x00, // total (inner) length, L+8 octets
        0x08, // "
        0x11, // message code (e.g. L_Data.req message)
        0x00, // additional information (none)
        0x00, // Service Information (L bytes)
        0x00}; // Message Authentication Code
    data.resize(l + 40);

    KNX::Secure_Wrapper_Frame secure_wrapper;
    secure_wrapper.fromData(std::cbegin(data), std::cend(data));

    ASSERT_EQ(secure_wrapper.header_length, 0x06);
    ASSERT_EQ(secure_wrapper.protocol_version, KNX::KNXNETIP_VERSION_13);
    ASSERT_EQ(secure_wrapper.service_type_identifier, KNX::Service_Type_Identifier::SECURE_WRAPPER);
    ASSERT_EQ(secure_wrapper.total_length, l + 40);
    ASSERT_EQ(secure_wrapper.total_length_calculated(), l + 40);
    ASSERT_EQ(secure_wrapper.secure_channel_index, 0x0000);
    std::array<uint8_t, 6> expected_sequence_identifier_group_counter{{0x00, 0x00, 0x4e, 0x79, 0x31, 0x4c}};
    ASSERT_EQ(secure_wrapper.sequence_identifier_group_counter, expected_sequence_identifier_group_counter);
    ASSERT_EQ(secure_wrapper.security_domain_mask, 0x0001);
    std::vector<uint8_t> expected_original_frame{0x06, 0x10, 0x05, 0x30, 0x00, 0x08, 0x11, 0x00};
    expected_original_frame.resize(l + 8);
    ASSERT_EQ(secure_wrapper.original_frame, expected_original_frame);
    std::array<uint8_t, 16> expected_message_authentication_code{};
    ASSERT_EQ(secure_wrapper.message_authentication_code, expected_message_authentication_code);

    ASSERT_EQ(secure_wrapper.toData(), data);
}

/**
 * @ingroup KNX_AN159_A_03_05
 */
TEST(Secure_Wrapper_Test, Encrypted_Routing_Indication_Frame)
{
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
