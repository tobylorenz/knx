# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# sub directories
add_subdirectory(06)
add_subdirectory(07)
add_subdirectory(08)
