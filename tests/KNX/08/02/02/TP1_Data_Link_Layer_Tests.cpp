// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/09/04/01/TP1_BCU2.h>

class TP1_Link_Layer_Tests : public ::testing::Test
{
    virtual ~TP1_Link_Layer_Tests() = default;
};

/** BCU that can be used as Bus Device Under Test (BDUT) */
class BCU :
    public KNX::TP1_BCU2
{
public:
    explicit BCU(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        KNX::TP1_BCU2(io_context, tp1_bus_simulation) {
    }
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class BCU_Plain_Data
{
public:
    explicit BCU_Plain_Data(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context)
    {
        tp1_physical_layer.connect(tp1_bus_simulation);
        tp1_data_link_layer.group_address_table = group_address_table;
        tp1_data_link_layer.L_Busmon_ind = std::bind(&BCU_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/**
 * Value and Invalid Control field - Receive
 *
 * @ingroup KNX_08_02_02_10_02_01_01
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_1_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1001));
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = 0x1003;
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP 0
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_6;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Send Group Value Read telegrams via Interface supporting sending of Raw Frame to
     * the BDUT with valid and invalid control fields to a supported group address - observe a
     * waiting time of 1 seconds between telegrams to check BDUT reaction
     */
    for (uint16_t control_field = 0; control_field < 256; control_field++) {
        switch (control_field) {
        case 0x10:
        case 0x14:
        case 0x18:
        case 0x1C:
        case 0x30:
        case 0x34:
        case 0x38:
        case 0x3C: {
            // L_Data_Extended
            tp1_data = { 0, 0xE0, 0xAF, 0xFE, 0x10, 0x01, 0x01, 0x00, 0x00, 0 }; // A_GroupValue_Read
            tp1_data[0] = control_field;
            bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
            io_context.poll();
            ASSERT_EQ(*log++, tp1_data);

            // L_Ack
            tp1_data = { 0xCC }; // L_Ack
            ASSERT_EQ(*log++, tp1_data);

            // A_GroupValue_Response
            const uint8_t answer_control_field = 0xB0 | (control_field & 0x0c);
            tp1_data = { answer_control_field, 0x10, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
            ASSERT_EQ(*log++, tp1_data);
        }
        break;
        case 0x90:
        case 0x94:
        case 0x98:
        case 0x9C:
        case 0xB0:
        case 0xB4:
        case 0xB8:
        case 0xBC: {
            // L_Data_Standard
            tp1_data = { 0, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
            tp1_data[0] = control_field;
            bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
            io_context.poll();
            ASSERT_EQ(*log++, tp1_data);

            // L_Ack
            tp1_data = { 0xCC }; // L_Ack
            ASSERT_EQ(*log++, tp1_data);

            // A_GroupValue_Response
            const uint8_t answer_control_field = 0xB0 | (control_field & 0x0c);
            tp1_data = { answer_control_field, 0x10, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
            ASSERT_EQ(*log++, tp1_data);
        }
        break;
        case 0xF0: {
            // L_Poll_Data
            // @todo L_Poll_Data test
        }
        break;
        case 0x00:
        case 0x04:
        case 0x08:
        case 0x0C:
        case 0x40:
        case 0x44:
        case 0x48:
        case 0x4C:
        case 0x80:
        case 0x84:
        case 0x88:
        case 0x8C:
        case 0xC0:
        case 0xC4:
        case 0xC8:
        case 0xCC: {
            // Acknowledgement frame
            // @todo Acknowledgement frame test
        }
        break;
        case 0x20:
        case 0x24:
        case 0x28:
        case 0x2C:
        case 0x50:
        case 0x54:
        case 0x58:
        case 0x5C:
        case 0x60:
        case 0x64:
        case 0x68:
        case 0x6C:
        case 0x70:
        case 0x74:
        case 0x78:
        case 0x7C:
        case 0xA0:
        case 0xA4:
        case 0xA8:
        case 0xAC:
        case 0xD0:
        case 0xD4:
        case 0xD8:
        case 0xDC:
        case 0xE0:
        case 0xE4:
        case 0xE8:
        case 0xEC:
        case 0xF4:
        case 0xF8:
        case 0xFC: {
            // unknown
            // @todo unknown test
        }
        break;
        default: {
            // invalid
            // @todo invalid test
        }
        break;
        }
    }
}

/**
 * Priorities: Send
 *
 * @ingroup KNX_08_02_02_10_02_01_02
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_1_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Set PEI of BDUT to Transport-Layer (remote) */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* Stimulate BDUT to send telegrams with all priorities */
    for (uint8_t priority : {
                0x0C, 0x04, 0x08, 0x00
            }) {
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), priority, 0x00, 0x00, 0xAF, 0xFE }; // T_Connect.req
        bdut.emi_req(pei_data);
        io_context.poll();
        tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x80, 0 }; // T_Connect
        tp1_data[0] |= priority;
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_req), priority, 0x00, 0x00, 0xAF, 0xFE }; // T_Disconnect.req
        bdut.emi_req(pei_data);
        io_context.poll();
        tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x81, 0 }; // T_Disconnect
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);
    }
}

/**
 * Repetition flag : send
 *
 * @ingroup KNX_08_02_02_10_02_01_03
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_1_3)
{
    // See test 10.2.6.2
}

/**
 * Repetition flag : receive
 *
 * @ingroup KNX_08_02_02_10_02_02_01
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_1_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Send a frame to the BDUT, in which the repetition flag is not set, followed by the same
     * frame with repetition flag set. Check whether the BDUT writes the value when receiving
     * the first frame and does not again write the value when receiving the second frame. */
    // @todo Test_10_2_1_4
}

/**
 * Source address
 *
 * @ingroup KNX_08_02_02_10_02_02_02
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimulate BDUT to send a frame.
    // OUT BC 1001 AFFE 60 80 :T-Connect(Addr=AFFE)
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x0C, 0x00, 0x00, 0xAF, 0xFE }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Individual Address
 *
 * @ingroup KNX_08_02_02_10_02_03_01
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Use EITT to send the following frame addressed to the BDUT
    // IN BC AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Unused Individual Address
 *
 * @ingroup KNX_08_02_02_10_02_03_02
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_3_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Use EITT to send the following frame addressed to BDUT
    // IN BC AFFE 1002 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(log, std::cend(busmoni.log)); // No L_Ack
}

/**
 * Used Group Address
 *
 * @ingroup KNX_08_02_02_10_02_03_03
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_3_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP 0
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_6;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Use EITT to send the following frames addrssed to BDUT:
    // IN BC AFFE 1002 E1 00 81
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 0000 E3 00 C0 12 34 :SetPhysAddr(Addr=1234)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE3, 0x00, 0xC0, 0x12, 0x34, 0 }; // A_IndividualAddress_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Unused Group Address
 *
 * @ingroup KNX_08_02_02_10_02_03_04
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_3_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Ensure that the group address 2222h is not contained in the BDUT's address table.
    // Use EITT to send the following frame addressed to the BDUT:
    // IN BC AFFE 2222 E1 00 81
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x22, 0x22, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(log, std::cend(busmoni.log)); // No L_Ack
}

/**
 * Send telegrams
 *
 * @ingroup KNX_08_02_02_10_02_03_05
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_3_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimulate BDUT to send a group telegram, a broadcast telegram and an individual
    // addressed telegram. Check telegrams with EITT.
    // OUT BC 1001 1002 E1 00 81
    bdut.application_layer.A_GroupValue_Write_req(KNX::Ack_Request::dont_care, KNX::ASAP_Group(1), KNX::Priority::low, KNX::Network_Layer_Parameter, {0x01}, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x01, 0x10, 0x02, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 0000 E3 00 C0 12 34 :SetPhysAddr(Addr=1234)
    bdut.application_layer.A_IndividualAddress_Write_req(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, KNX::Individual_Address(0x1234), [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE3, 0x00, 0xC0, 0x12, 0x34, 0 }; // A_IndividualAddress_Write
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 80 :T-Connect(Addr=AFFE)
    bdut.application_layer.A_Connect_req({KNX::Individual_Address(0xAFFE), true}, KNX::Priority::system, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * BDUT is a Router
 *
 * @ingroup KNX_08_02_02_10_02_03_06
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_3_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP 0
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_6;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Send an individual addressed telegram to the BDUT.
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a group addressed telegram to the BDUT.
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a broadcast telegram to the BDUT.
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE3, 0x00, 0xC0, 0x12, 0x34, 0 }; // A_IndividualAddress_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @todo check routing
}

/**
 * Info Length: Send
 *
 * @ingroup KNX_08_02_02_10_02_04_01
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimulate BDUT to send a ...
    // a) T-Disconnect
    bdut.application_layer.A_Connect_req({KNX::Individual_Address(0xAFFE), false}, KNX::Priority::low, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    bdut.application_layer.A_Disconnect_req(KNX::Priority::low, {KNX::Individual_Address(0xAFFE), false}, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x81, 0 }; // T_Disconnect // @note Priority changed from Low to System. Following TP spec.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // b) A memory response with maximum info length (check with EITT).
    // OUT BC 1001 AFFE 60 81 (T-Disconnect)
    bdut.application_layer.A_Connect_req({KNX::Individual_Address(0xAFFE), false}, KNX::Priority::low, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 AFFE 6F 42 52 01 16 01 02 03 04 05 06 07 08 09 00A 0B OC (Memory Response, Count =12, Addr=0116, Data : 01 02 03 04 05 06 07 08 09 0A 0B OC)
    bdut.application_layer.A_Memory_Read_res(KNX::Ack_Request::dont_care, KNX::Priority::low, KNX::Network_Layer_Parameter, {KNX::Individual_Address(0xAFFE), true}, 0x12, 0x0116, {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C}, [](const KNX::Status a_status) -> void {
        // do nothing
    });
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6F, 0x42, 0x42, 0x01, 0x16, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0 }; // T_Memory_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Info Length: Receive
 *
 * @ingroup KNX_08_02_02_10_02_04_02
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_4_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Send frames from EITT to BDUT with different info lengths:
    // IN BC AFFE 1001 60 80 (T-Connect)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x42, 0x52, 0x01, 0x16, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0 }; // T_Memory_Read

    // IN BC AFFE 1001 6F 42 52 01 16 01 02 03 04 05 06 07 08 09 00A 0B OC (Memory Response, Count =12, Addr=0116, Data : 01 02 03 04 05 06 07 08 09 0A 0B OC):
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Incorrect Info Length: Receive (Optional)
 *
 * @ingroup KNX_08_02_02_10_02_04_03
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_4_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    //    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Send frames from EITT to BDUT with information length not corresponding to number
    // of sent data
    // IN BC AFFE 1002 EF 00 81
    //    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xEF, 0x00, 0x81, 0 };
    //    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    //    io_context.poll();
    //    ASSERT_EQ(*log++, tp1_data);
    //    ASSERT_EQ(log, std::cend(busmoni.log)); // No L_Ack
    // IN BC AFFE 1002 E2 00 80 00 00 00 00 00 00 00 00 00 00 00 00 00 00 :
    //    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    //    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    //    io_context.poll();
    //    ASSERT_EQ(*log++, tp1_data);
    //    ASSERT_EQ(log, std::cend(busmoni.log)); // No L_Ack

    // @todo Test_10_2_4_3
}

/**
 * Checksum: Send
 *
 * @ingroup KNX_08_02_02_10_02_05_01
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_5_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimulate BDUT to send a frame. Use the BCU Terminal connected to EITT to check
    // the correct calculation of the checksum.
    // OUT BC 1001 1002 E1 00 81
    bdut.application_layer.A_GroupValue_Write_req(KNX::Ack_Request::dont_care, KNX::ASAP_Group(1), KNX::Priority::low, KNX::Network_Layer_Parameter, {0x01}, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x01, 0x10, 0x02, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Checksum: Receive
 *
 * @ingroup KNX_08_02_02_10_02_05_02
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_5_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP 0
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_6;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Use Waveform Generator to send a frame addressed to the BDUT with incorrect
    // checksum.
    // OUT BC 1001 1002 E1 00 81
    tp1_data = { 0xBC, 0x10, 0x01, 0x10, 0x02, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);

    // @todo NACK
}

/**
 * Sending of Ack, Nack, Busy
 *
 * @ingroup KNX_08_02_02_10_02_06_01
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_6_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    //    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Send frame to BDUT and check correct generation of acknowledge frame. Check in
    // PICS under which conditions which a Busy signal is transmitted (e.g. in case of BCU2
    // device: program EEPROM and send group telegram addressed to BDUT)
    // @todo Busy
}

/**
 * Repetition
 *
 * @ingroup KNX_08_02_02_10_02_06_02
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_6_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    //    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimulate BDUT to send telegram, which triggers via the sent line of the BDUT the
    // Waveform Generator to send ...
    // a) a NACK
    // b) Busy
    // c) no frame
    // d) overlapping NACK and Busy
    // @todo Test_10_2_6_2
}

/**
 * Receive ACK
 *
 * @ingroup KNX_08_02_02_10_02_06_03
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_6_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimulate BDUT to send a frame to which another device responds with ACK.
    bdut.application_layer.A_Connect_req({KNX::Individual_Address(0xAFFE), false}, KNX::Priority::low, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Sending legal Length
 *
 * @ingroup KNX_08_02_02_10_02_07_01
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_7_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    //    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimulate BDUT to send master frames to non-existing polling group.
    // OUT F0 1001 1004 00
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x00);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x00, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x01);
    //    io_context.poll();
    // OUT F0 1001 1004 01 FE
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x01, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x02);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x02, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x03);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x03, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x04);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x04, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x05);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x05, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x06);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x06, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x07);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x07, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x08);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x08, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x09);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x09, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x0A);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x0A, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x0B);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x0B, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x0C);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x0C, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x0D);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x0D, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x0E);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x0E, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x0F);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x0F, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    // @todo Test_10_2_7_1
}

/**
 * Slave sends FEh
 *
 * @ingroup KNX_08_02_02_10_02_07_02
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_7_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    //    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimulate BDUT to send polling request on existing polling group with number of
    // expected poll data = 1. A slave sends the value FEh in the first slot.
    //    bdut.tp1_data_link_layer.L_Poll_Data_req(0x1004, 0x01);
    //    io_context.poll();
    //    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x01, 0xFE, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    // @todo Test_10_2_7_2
}

/**
 * Other polling group
 *
 * @ingroup KNX_08_02_02_10_02_08_01
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_8_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Generate master frames with polling group not supported by the BDUT.
    // IN F0 1001 1005 00
    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x05, 0x00, 0 };
    // IN F0 1001 1005 01 FE
    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x05, 0x01, 0xFE, 0 };
    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x05, 0x0F, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    // @todo Test_10_2_8_1
}

/**
 * Length = 0
 *
 * @ingroup KNX_08_02_02_10_02_08_02
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_8_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Generate master frames with number of slots = 0.
    // IN F0 1001 1004 00
    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x00, 0 };
    // @todo Test_10_2_8_2
}

/**
 * Length > 0
 *
 * @ingroup KNX_08_02_02_10_02_08_03
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_8_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Generate master frames with number of slots = 6.
    // IN F0 1001 1004 06
    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x06, 0 };
    // @todo Test_10_2_8_3
}

/**
 * Length > 15
 *
 * @ingroup KNX_08_02_02_10_02_08_04
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_8_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Generate master frames with number of slots = 16.
    // IN F0 1001 1004 11
    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x11, 0 };
    // @todo Test_10_2_8_4
}

/**
 * Legal Length
 *
 * @ingroup KNX_08_02_02_10_02_08_05
 */
TEST(TP1_Link_Layer_Tests, Test_10_2_8_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 Bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002));
    bdut.interface_objects->device()->polling_group_settings()->polling_group_address = KNX::Group_Address(0x1003);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Generate master frames with number of slots = 1 ... 15. BDUT answers in the slot for
    // which it was configured (e.g. in underneath example configured for slot number 0).
    // IN F0 1001 1004 01
    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x01, 0 };
    // OUT FF
    tp1_data = { 0xFF, 0 };
    // IN F0 1001 1004 02
    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x02, 0 };
    // OUT FF FE
    tp1_data = { 0xFF, 0xFE, 0 };
    // IN F0 1001 1004 0F
    tp1_data = { 0xF0, 0x10, 0x01, 0x10, 0x04, 0x0F, 0 };
    // OUT FF FE FE FE FE FE FE FE FE FE FE FE FE FE FE
    tp1_data = { 0xFF, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0 };
    // @todo Test_10_2_8_5
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
