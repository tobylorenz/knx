// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/09/04/01/TP1_BCU2.h>

class Network_Layer_Tests : public ::testing::Test
{
    virtual ~Network_Layer_Tests() = default;
};

/** BCU that can be used as Bus Device Under Test (BDUT) */
class BCU :
    public KNX::TP1_BCU2
{
public:
    explicit BCU(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        KNX::TP1_BCU2(io_context, tp1_bus_simulation) {
    }
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class BCU_Plain_Data
{
public:
    explicit BCU_Plain_Data(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context)
    {
        tp1_physical_layer.connect(tp1_bus_simulation);

        tp1_data_link_layer.group_address_table = group_address_table;
        tp1_data_link_layer.L_Busmon_ind = std::bind(&BCU_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/**
 * Group oriented communication
 *
 * L_Data / N_Data_Group / T_Data_Group / A_GroupValue_Read
 * from IA = 0xAFFE
 * to GA = 0x1001
 *
 * L_Data / N_Data_Group / T_Data_Group / A_GroupValue_Response
 * from IA = 0x1001
 * to GA = 0xAFFE
 *
 * @ingroup KNX_08_03_03_03_01
 */
TEST(Network_Layer_Tests, Test_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // A Group object (GO) shall be present in the BDUT that is read- and transmit enabled. This GO shall be
    // associated to a group address (1001h is used here as an example).
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1001)); // Group_Address -> TSAP 1
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1); // 1 bit
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;

    // Send telegrams with Routing Count 6, 5, 4, 3, 2, 1, 0 to the BDUT.
    for (const KNX::Hop_Count hop_count : {
                6, 5, 4, 3, 2, 1, 0
            }) {
        // IN BC AFFE 1001 81 00 00 :Group Value Read
        // IN BC AFFE 1001 91 00 00 :Group Value Read
        // IN BC AFFE 1001 A1 00 00 :Group Value Read
        // IN BC AFFE 1001 B1 00 00 :Group Value Read
        // IN BC AFFE 1001 C1 00 00 :Group Value Read
        // IN BC AFFE 1001 D1 00 00 :Group Value Read
        // IN BC AFFE 1001 E1 00 00 :Group Value Read
        tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        // Acceptance: The BDUT shall answer with Routing Count 6 in all cases.
        // OUT BC 1001 1001 E2 00 40 00 :Value Response
        tp1_data = { 0xBC, 0x10, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x00 to 0xE1, 0x00, 0x40, as less than 6 bits
        ASSERT_EQ(*log++, tp1_data);
    }

    // Test Group Communication Routing Count 7

    // IN BC AFFE 1001 F1 00 00 :Group Value Read
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xF1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT shall answer with Routing Count 7 (the BDUT may optionally answer with routing counter 6)
    // OUT BC 1001 1001 F2 00 40 00 :Value Response
    tp1_data = { 0xBC, 0x10, 0x01, 0x10, 0x01, 0xF1, 0x00, 0x40, 0 }; // A_GroupValue_Response // @note changed from 0xF2, 0x00, 0x40, 0x00 to 0xF1, 0x00, 0x40, as less than 6 bits
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device oriented communication - connected
 *
 * L_Data / N_Data_Individual / T_Data_Connected / A_DeviceDescriptor_Read
 * from IA = 0xAFFE
 * to IA = 0x1001
 *
 * L_Data / N_Data_Individual / T_Data_Connected / A_DeviceDescriptor_Response
 * from IA = 0x1001
 * to IA = 0xAFFE
 *
 * @ingroup KNX_08_03_03_03_02
 */
TEST(Network_Layer_Tests, Test_3_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    const KNX::Individual_Address bdut_address{0x1001};
    bdut.interface_objects->device()->device_descriptor()->device_descriptor = 0x0020;
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;

    BCU_Plain_Data bcu(io_context, tp1_bus);
    const KNX::Individual_Address bcu_address{0xAFFE};
    bcu.group_address_table->individual_address = bcu_address;

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Send telegrams with Routing Count 6, 5, 4, 3, 2, 1, 0 to the BDUT.
    uint8_t seq_no = 0;
    for (const KNX::Hop_Count hop_count : {
                6, 5, 4, 3, 2, 1, 0
            }) {
        // IN BC AFFE 1001 60 80 :T-Connect(Addr=1001)
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu_address), KNX::Connection_State::Closed);
        tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 }; // T_Connect
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu_address), KNX::Connection_State::Open_Idle);

        // IN BC AFFE 1001 61 43 00 :MaskVersionRead()
        tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        tp1_data[6] = (tp1_data[6] & 0xc3) | (seq_no << 2);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);
        io_context.poll();
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu_address), KNX::Connection_State::Open_Wait);

        // Acceptance: The BDUT shall answer with Routing Count 6 in all cases.
        // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
        tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 }; // T_Ack
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        // OUT BC 1001 AFFE 63 43 40 00 20 :MaskVersionResponse(Type=00, Version=20)
        tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x43, 0x40, 0x00, 0x20, 0 }; // A_DeviceDescriptor_Response
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
        tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 }; // T_Ack
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        // IN BC AFFE 1001 60 81 :T-Disconnect
        tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);
    }

    // Routing count 7

    // IN BC AFFE 1001 70 80 :T-Connect(Addr=01.00.001)
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu_address), KNX::Connection_State::Closed);
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x70, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu_address), KNX::Connection_State::Open_Idle);

    // IN BC AFFE 1001 71 43 00 :MaskVersionRead()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x71, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    io_context.poll();
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu_address), KNX::Connection_State::Open_Wait);

    // Acceptance: The BDUT shall answer with Routing Count 7 (optionally routing counter 6)
    // OUT B0 1001 AFFE 70 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x70, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 AFFE 73 43 40 00 20 :MaskVersionResponse(Type=00, Version=20)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x43, 0x40, 0x00, 0x20, 0 }; // A_DeviceDescriptor_Response // @note Although the test spec says hop_count=7 (optional 6), the TP layer spec says it's always Network Layer Parameter (6).
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 70 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x70, 0xC2, 0 }; // T_Ack
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 70 81 :T-Disconnect
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x70, 0x81, 0 }; // T_Disconnect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device oriented communication - connectionless
 *
 * L_Data / N_Data_Individual / T_Data_Individual / A_PropertyValue_Read
 * from IA = 0xAFFE
 * to IA = 0x1001
 *
 * L_Data / N_Data_Individual / T_Data_Individual / A_PropertyValue_Response
 * from IA = 0x1001
 * to IA = 0xAFFE
 *
 * @ingroup KNX_08_03_03_03_03
 */
TEST(Network_Layer_Tests, Test_3_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Send telegrams with Routing Count 6, 5, 4, 3, 2, 1, 0 to the BDUT.
    for (const KNX::Hop_Count hop_count : {
                6, 5, 4, 3, 2, 1, 0
            }) {
        // IN BC AFFE 1001 65 03 D5 00 01 10 01 :PropertyRead(Obj=00, Prop=01, Count=1, Start=001)
        tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        // Acceptance: The BDUT shall answer with Routing Count 6 in all cases
        // OUT BC 1001 AFFE 67 03 D6 00 01 10 01 00 00 :PropertyResponse(Obj=00, Prop=01, Count=1, Start=001, Data=00 00 )
        tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x01, 0x10, 0x01, 0x00, 0x00, 0 }; // A_PropertyValue_Response
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);
    }

    // Routing count 7

    // IN BC AFFE 1001 75 03 D5 00 01 10 01 :PropertyRead(Obj=00, Prop=01, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x75, 0x03, 0xD5, 0x00, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT shall answer with Routing Count 7 (optionally routing counter 6)
    // OUT BC 1001 AFFE 77 03 D6 00 01 10 01 00 00 :PropertyResponse(Obj=00, Prop=01, Count=1, Start=001, Data=00 00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x77, 0x03, 0xD6, 0x00, 0x01, 0x10, 0x01, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Broadcast communication
 *
 * L_Data / N_Data_Broadcast / T_Data_Broadcast / A_IndividualAddress_Read
 * from IA = 0xAFFE
 * to GA = 0x0000
 *
 * L_Data / N_Data_Broadcast / T_Data_Broadcast / A_IndividualAddress_Response
 * from IA = 0x1001
 * to GA = 0x0000
 *
 * @ingroup KNX_08_03_03_03_04
 */
TEST(Network_Layer_Tests, Test_3_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Send telegrams with Routing Count 6, 5, 4, 3, 2, 1, 0 to the BDUT.
    for (const KNX::Hop_Count hop_count : {
                6, 5, 4, 3, 2, 1, 0
            }) {
        // IN BC AFFE 0000 E1 01 00 :ReadPhysAddr()
        tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        // Acceptance: The BDUT shall answer with Routing Count 6 in all cases.
        // OUT BC 1001 0000 E1 01 40 :ReadPhysAddrResponse(Addr=01.00.001)
        tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 }; // A_IndividualAddress_Response
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);
    }

    // Routing count 7

    // IN BC AFFE 0000 F1 01 00 :ReadPhysAddr()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xF1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT shall answer with Routing Count 7 (optionally routing counter 6)
    // OUT BC 1001 0000 F1 01 40 :ReadPhysAddrResponse(Addr=01.00.001)
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xF1, 0x01, 0x40, 0 }; // A_IndividualAddress_Response
    ASSERT_EQ(*log++, tp1_data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
