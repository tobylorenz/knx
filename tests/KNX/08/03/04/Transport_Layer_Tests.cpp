// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <iostream>

#include <KNX/09/04/01/TP1_BCU2.h>

class Transport_Layer_Tests : public ::testing::Test
{
    virtual ~Transport_Layer_Tests() = default;
};

/** BCU that can be used as Bus Device Under Test (BDUT) */
class BCU :
    public KNX::TP1_BCU2
{
public:
    explicit BCU(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        KNX::TP1_BCU2(io_context, tp1_bus_simulation) {
    }
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class BCU_Plain_Data
{
public:
    explicit BCU_Plain_Data(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context) {
        tp1_physical_layer.connect(tp1_bus_simulation);

        tp1_data_link_layer.group_address_table = group_address_table;
        tp1_data_link_layer.L_Busmon_ind = std::bind(&BCU_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/**
 * Telegram Sequence 1: Procedure with initial state ‘CLOSED’ (all styles)
 *
 * @ingroup KNX_08_03_04_02_02_01_01
 */
TEST(Transport_Layer_Tests, Test_2_2_1_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* The BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);
}

/**
 * Telegram sequence 2: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * style 1)
 *
 * @ingroup KNX_08_03_04_02_02_01_02
 */
TEST(Transport_Layer_Tests, Test_2_2_1_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. The remote device sends a T_CONNECT.req to BDUT */
    /* (4) IN 00:00:01.0 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that BDUT sends a T_DISCONNECT.ind to the user. */
    /* (5) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* 6. Check that a telegram is sent addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (6) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* Now the BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 3: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * style 1)
 *
 * @ingroup KNX_08_03_04_02_02_01_03
 */
TEST(Transport_Layer_Tests, Test_2_2_1_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 : */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Send a T_DATA_CONNECTED.req to remote BCU. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    /* 6. The remote device sends a T_CONNECT.req to BDUT */
    /* (6) IN 00:00:04.0 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 7. Check that BDUT sends a T_DISCONNECT.ind to the user. */
    /* (7) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* 8. Check that a telegram is sent addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (8) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* Check that the BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 4: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * styles 1/3)
 *
 * @ingroup KNX_08_03_04_02_02_02_01
 */
TEST(Transport_Layer_Tests, Test_2_2_2_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT confirms the T_CONNECT */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    bdut.transport_layer.client_accepts_connection = false; // BDUT only accepts one connection in parallel.

    /* 4. Connect from a remote device during an existing connection (remote BCU <> third BCU). */
    /* (4) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that a telegram is sent addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (5) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    /* Note : Style 2 - BDUT sends no Disconnect on the bus */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);
}

/**
 * Telegram sequence 5: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * styles 1/3)
 *
 * @ingroup KNX_08_03_04_02_02_02_02
 */
TEST(Transport_Layer_Tests, Test_2_2_2_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. Set the BCU in Link-Layer. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT confirms the T_CONNECT */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    /* 4. BDUT sends a T_DATA_CONNECTED.req to the third BCU. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    bdut.transport_layer.client_accepts_connection = false; // BDUT only accepts one connection in parallel.

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Wait);

    /* 5. Connect from a remote device during an existing connection (remote BCU <> third BCU). */
    /* (5) IN 00:00:04.0 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 6. Check that a telegram is sent addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (6) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    /* Note : Style 2 - BDUT sends no Disconnect on the bus */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Telegram Sequence 6: Procedure with initial state ‘CLOSED’ (without PEI, all
 * styles)
 *
 * @ingroup KNX_08_03_04_02_02_03_01
 */
TEST(Transport_Layer_Tests, Test_2_2_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* The BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);

    /* 2. The remote device sends a T_DISCONNECT.req to BDUT */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram Sequence 7: Procedure with initial state ‘OPEN_IDLE’ (without PEI, all
 * styles)
 *
 * @ingroup KNX_08_03_04_02_02_03_02
 */
TEST(Transport_Layer_Tests, Test_2_2_3_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 : */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. The remote device sends a T_DISCONNECT.req to BDUT */
    /* (4) IN 00:00:00.2 B0 A001 A000 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that the BDUT sends a T_DISCONNECT_ind */
    /* (5) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram Sequence 8: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * all styles)
 *
 * @ingroup KNX_08_03_04_02_02_03_03
 */
TEST(Transport_Layer_Tests, Test_2_2_3_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 : */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Send a T_DATA_CONNECTED.req to remote BCU. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    /* 6. The remote device sends a T_DISCONNECT.req to BDUT */
    /* (6) IN 00:00:04.0 B0 A001 A000 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 7. Check that the BDUT sends a T_DISCONNECT_ind */
    /* (7) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Now the BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 9: Procedure with initial state ‘OPEN_IDLE’ (without PEI, all
 * styles)
 *
 * @ingroup KNX_08_03_04_02_02_04_01
 */
TEST(Transport_Layer_Tests, Test_2_2_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. Activate the Link-Layer. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. BDUT receives a T_CONNECT.con */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    /* 4. The BDUT receives a T_DISCONNECT.req from remote BCU (<> connected BCU = third BCU) */
    /* (4) IN 00:00:02.0 B0 A001 A000 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* No reaction. */

    /* The BDUT persists in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);
}

/**
 * Telegram sequence 10: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * all styles)
 *
 * @ingroup KNX_08_03_04_02_02_04_02
 */
TEST(Transport_Layer_Tests, Test_2_2_4_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. Activate the Link-Layer. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. BDUT receives a T_CONNECT.con */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    /* 4. BDUT sends a T_DATA_CONNECTED.req to the third BCU. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Wait);

    /* 5. The BDUT receives a T_DISCONNECT.req from remote BCU (<> connected BCU = third BCU) */
    /* (5) IN 00:00:04.0 B0 A001 A000 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* No reaction. */

    /* The BDUT persists in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Wait);
}

/**
 * Telegram sequence 11: Connect from the local user to an existing device
 * (styles 1/2)
 *
 * @ingroup KNX_08_03_04_02_02_05
 */
TEST(Transport_Layer_Tests, Test_2_2_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to an existing device (remote BCU). */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 01 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x01 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 3. Check that a telegram is sent addressed to remote BCU with NSDU=T_CONNECT.req */
    /* (3) OUT 00:00:00.0 B0 A000 A001 60 80 :T-Connect(Addr=10.00.001) */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 4. Check that the BDUT sends a T_CONNECT_con when the IAK=OK from the existing device (remote BCU). */
    /* (4) OUT 00:00:00.0 86 B0 A0 01 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    /* Note : Style 3 - On A12 BDUT changes to CONNECTING and on A13 to OPEN_IDLE. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);
}

/**
 * Telegram sequence 12: Connect from the local user to an non existing device
 * (style 1)
 *
 * @ingroup KNX_08_03_04_02_02_06
 */
TEST(Transport_Layer_Tests, Test_2_2_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* The BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_state(KNX::Individual_Address(0xA005)), KNX::Connection_State::Closed);

    /* 2. Send a T_CONNECT to a non existing device (Address A005h). */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 05 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x05 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(KNX::TSAP_Connected(0xA005)), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(KNX::TSAP_Connected(0xA005)), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(KNX::TSAP_Connected(0xA005)), KNX::Connection_State::Open_Idle);

    /* 3. Check that a telegram is sent addressed to non existing BCU with NSDU=T_CONNECT.req */
    /* (3) OUT 00:00:00.0 B0 A000 A005 60 80 :T-Connect(Addr=10.00.005) */
    /* (only visible by third PC with EITT in Busmonitor mode) */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x05, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // let connection timer expire
    bdut.transport_layer.test_expire_connection_timeout_timer(KNX::TSAP_Connected(0xA005));
    io_context.poll();

    /* 4. Check that a T_DISCONNECT_ind is sent, when receiving no IAK. */
    /* (4) OUT 00:00:00.0 87 00 A0 00 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x05, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'CLOSED'. */
    /* Note : Style 3 - On A12 BDUT changes to CONNECTING and on A5 to CLOSED - */
    /* Style 2: DUT sends no T.Disconnect.ind and remains in OPEN_IDLE" */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(KNX::TSAP_Connected(0xA005)), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(KNX::TSAP_Connected(0xA005)), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(KNX::TSAP_Connected(0xA005)), KNX::Connection_State::Closed);
}

/**
 * Telegram Sequence 13: Procedure with initial state ‘OPEN_IDLE’ (styles 1/3)
 *
 * @ingroup KNX_08_03_04_02_02_07_01
 */
TEST(Transport_Layer_Tests, Test_2_2_7_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to remote BCU. */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 01 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x01 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 3. Check that a telegram is sending addressed to remote BCU with NSDU=T_CONNECT.req */
    /* (3) OUT 00:00:00.0 B0 A000 A001 60 80 :T-Connect(Addr=10.00.001) */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 4. Check that the BDUT conirms the T_CONNECT when the IAK=OK from the existing device (remote BCU). */
    /* (4) OUT 00:00:00.0 86 B0 A0 01 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Connection established. State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 5. Connect during an existing connection. */
    /* (5) IN 00:00:00.2 43 00 00 00 A0 01 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x01 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 6. Check that a telegram is sent addressed to BCU with NSDU=T_DISCONNECT.req */
    /* (6) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 7. Check that a T_DISCONNECT_ind is sent */
    /* (7) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT goes in State 'CLOSED'. */
    /* Note : Style 2 - BDUT remains in OPEN_IDLE, BDUT sends no T_Disconnect.ind and no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram Sequence 14: Procedure with initial state ‘OPEN_WAIT’ (styles 1/3)
 *
 * @ingroup KNX_08_03_04_02_02_07_02
 */
TEST(Transport_Layer_Tests, Test_2_2_7_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to remote BCU. */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 01 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x01 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 3. Send telegram addressed to remote BCU with NSDU=T_CONNECT.req */
    /* (3) OUT 00:00:00.0 B0 A000 A001 60 80 :T-Connect(Addr=10.00.001) */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 4. Check that the BDUT sends a T_CONNECT_con when the IAK=OK from the existing device (remote BCU). */
    /* (4) OUT 00:00:00.0 86 B0 A0 01 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Connection established. State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 5. Send a T_DATA_CONNECTED.req to remote BCU. */
    /* (5) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 6. Send telegram addressed to remote BCU with NSDU=T_DATA_CONNECTED.req */
    /* (6) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    /* 7. Connect during an existing connection. */
    /* (7) IN 00:00:00.2 43 00 00 00 A0 01 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x01 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 8. Check that a telegram is sent addressed to BCU with NSDU=T_DISCONNECT.req */
    /* (8) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 9. Check that a T_DISCONNECT_ind is sent */
    /* (9) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Now the BDUT is in State 'CLOSED'. */
    /* Note : Style 2 - BDUT remains in OPEN_WAIT, BDUT sends no T_Disconnect.ind and no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 15: Procedure with initial state ‘OPEN_IDLE’ (Styles 1/3)
 *
 * @ingroup KNX_08_03_04_02_02_08_01
 */
TEST(Transport_Layer_Tests, Test_2_2_8_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT.req to remote BCU (client). */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 01 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x01 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 3. Send telegram addressed to remote BCU with NSDU=T_CONNECT.req */
    /* (3) OUT 00:00:00.0 B0 A000 A001 60 80 :T-Connect(Addr=10.00.001) */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 4. Check that the BDUT sends a T_CONNECT_con when the IAK=OK from the existing device (remote BCU). */
    /* (4) OUT 00:00:00.0 86 B0 A0 01 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Connection established. State 'OPEN_IDLE' (client). */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 5. T_DISCONNECT.req from local user (BDUT). */
    /* (5) IN 00:00:00.2 44 00 00 00 00 00 :T_DISCONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_req), 0x00, 0x00, 0x00, 0xA0, 0x01 }; // T_Disconnect.req // @note destination changed from 0x0000 to 0xA001
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 6. Check that the client sends an T_DISCONNECT_con to the local user. */
    /* (6) OUT 00:00:00.0 88 00 00 00 00 00 :T_DISCONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_con), 0x00, 0xA0, 0x00, 0xA0, 0x01 }; // T_Disconnect.con // @note source changed form 0x0000 to 0xA000. dest changed from 0x0000 to 0xA001.
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* 7. Check that a telegram is sent addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (7) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* BDUT is in State 'CLOSED' – */
    /* Note : Style 2 - DUT remains in OPEN_IDLE and sends T_Disconnect.ind" */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 16: Procedure with initial state ‘OPEN_WAIT’ (Styles 1/3)
 *
 * @ingroup KNX_08_03_04_02_02_08_02
 */
TEST(Transport_Layer_Tests, Test_2_2_8_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H.*/
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT.req to remote BCU (client). */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 01 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x01 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 3. Send telegram addressed to remote BCU with NSDU=T_CONNECT.req */
    /* (3) OUT 00:00:00.0 B0 A000 A001 60 80 :T-Connect(Addr=10.00.001) */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 4. Check that the BDUT sends a T_CONNECT_con when the IAK=OK from the existing device (remote BCU). */
    /* (4) OUT 00:00:00.0 86 B0 A0 01 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Connection established. State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 5. Send T_DATA_CONNECTED.req to remote BCU (client). */
    /* (5) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 6. A telegram is sent to remote BCU with NSDU=T_DATA_CONNECTED.req */
    /* (6) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    /* 7. T_DISCONNECT.req from the local user (BDUT). */
    /* (7) IN 00:00:00.2 44 00 00 00 00 00 :T_DISCONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_req), 0x00, 0x00, 0x00, 0xA0, 0x01 }; // T_Disconnect.req // @note destination changed from 0x0000 to 0xA001
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 8. Check that the client sends an T_DISCONNECT_con to the local user. */
    /* (8) OUT 00:00:00.0 88 00 00 00 00 00 :T_DISCONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_con), 0x00, 0xA0, 0x00, 0xA0, 0x01 }; // T_Disconnect.con // @note source changed from 0x0000 to 0xA000. dest changed from 0x0000 to 0xA001.
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* 9. Check that a telegram is sent addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (9) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* BDUT is in State 'CLOSED'. */
    /* Note: : Style 2: DUT remains in OPEN_WAIT, DUT sends Disconnect on the bus + disconnect.ind after leaving OPEN_WAIT */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 17: Disconnect from the local user without an existing
 * connection (styles 1/3)
 *
 * @ingroup KNX_08_03_04_02_02_09
 */
TEST(Transport_Layer_Tests, Test_2_2_9)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    //std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* State 'CLOSE'(client). */
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);

    /* 2. T_DISCONNECT.req from local user (BDUT). */
    /* (2) IN 00:00:00.2 44 00 00 00 00 00 :T_DISCONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_req), 0x00, 0x00, 0x00, 0x00, 0x00 }; // T_Disconnect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 3. Check that the client sends an T_DISCONNECT_con to the local user. */
    /* (3) OUT 00:00:00.0 88 00 00 00 00 00 :T_DISCONNECT.con */
    /* Note : Style 2 - BDUT sends no T_Disconnect.con. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_con), 0x00, 0xA0, 0x00, 0x00, 0x00 }; // T_Disconnect.con // @note Src changed from 0x0000 to 0xA000.
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * Telegram sequence 18: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * all styles)
 *
 * @ingroup KNX_08_03_04_02_02_10_01
 */
TEST(Transport_Layer_Tests, Test_2_2_10_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in state 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* The connection timeout timer has started. */
    /* 4.+5. After the time interval of 6s (connection timeout timer is released) the BDUT will close the connection by sending a T_DISCONNECT.ind and a telegram with NSDU=T_DISCONNECT_REQ. */
    bdut.transport_layer.test_expire_connection_timeout_timer(bcu1_address);
    io_context.poll();

    /* (4) OUT 00:00:06.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* (5) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Telegram sequence 19: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * all styles)
 *
 * @ingroup KNX_08_03_04_02_02_11_01
 */
TEST(Transport_Layer_Tests, Test_2_2_11_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in state 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 5. Send T_DATA_CONNECTED.req to remote BCU (client). */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 6. A telegram is sent to remote BCU with NSDU=T_DATA_CONNECTED.req */
    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in state 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1) {
        /* ------------------------------------------------------------------------ */
        /* The acknowledgement timeout timer has started. */

        bdut.transport_layer.test_expire_acknowledgement_timeout_timer(bcu1_address);
        io_context.poll();

        /* (6) OUT 00:00:03.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        bdut.transport_layer.test_expire_acknowledgement_timeout_timer(bcu1_address);
        io_context.poll();

        /* (7) OUT 00:00:03.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        bdut.transport_layer.test_expire_acknowledgement_timeout_timer(bcu1_address);
        io_context.poll();

        /* (8) OUT 00:00:03.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        bdut.transport_layer.test_expire_acknowledgement_timeout_timer(bcu1_address);
        io_context.poll();

        /* If the repeat counter = 3 (maximum of T_DATA_CONNECTED.req repetitions), the acknowledgement timeout timer stops and the user gets a T_DISCONNECT.ind. */
        /* (9) OUT 00:00:00.2 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
        ASSERT_EQ(*pei_data_ind++, pei_data);

        /* (10) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT is in state 'CLOSED'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
    }

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1_Rationalised) {
        /* ------------------------------------------------------------------------- */
        /* Or alternatively for step (6) to step (10) for Style 1 rationalised */

        /* The DUT is in state 'OPEN_WAIT'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

        /* The connection timeout timer has started. If no T_ACK is received, the BDUT closes the connection after 6 sec. */
        /* (6) OUT 00:00:06.0 87 00 00 00 00 00:T_DISCONNECT.ind */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0x00, 0x00, 0x00, 0x00 }; // T_Disconnect.ind
        ASSERT_EQ(*pei_data_ind++, pei_data);

        /* (7) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The DUT is in state 'CLOSED'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
    }
}

/**
 * Telegram sequence 20a: Reception of a correct N_Data_Individual –
 * Procedure with initial state ‘OPEN_IDLE’ (without PEI, all styles)
 *
 * @ingroup KNX_08_03_04_02_03_01
 */
TEST(Transport_Layer_Tests, Test_2_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT receives a NSDU=T_DATA_CONNECTED_REQ (MaskVersionRead). */
    /* (4) IN 00:00:00.2 B0 A001 A000 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that the BDUT is sent a T_DATA_CONNECTED.ind and a telegram with NSDU=T_ACK. */
    /* (5) OUT 00:00:00.0 89 B0 A0 01 A0 00 61 43 00 :T_DATA_CONNECTED.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x43, 0x00 }; // T_Data_Connected.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* (6) OUT 00:00:00.0 B0 A000 A001 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 23);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);
}

/**
 * Telegram sequence 20b: Reception of a correct N_Data_Individual –
 * Procedure with initial state ‘OPEN_WAIT’ (without PEI, all styles)
 *
 * @ingroup KNX_08_03_04_02_03_02
 */
TEST(Transport_Layer_Tests, Test_2_3_2)
{
    std::cerr << "Test 2.3.2" << std::endl;
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->device()->device_descriptor()->device_descriptor = 0x0020;
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION */
    const KNX::Individual_Address bdut_address{0x1001};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xAFFE};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* Initial state: Send T_Connect and MaskVersionRead to BDUT to establish OPEN_WAIT as the initial state for the sequence. */
    /* (1) IN start B0 AFFE 1001 60 80 :T-Connect(Addr=1001) */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* BDUT is in State OPEN_IDLE */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* Now the BDUT receives a MaskVersionRead */
    /* (2) IN 00:00:02.0 B0 AFFE 1001 61 43 00 :DeviceDescriptorRead(DescType=00) */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* (3) OUT 00:00:00.0 B0 1001 AFFE 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* BDUT is in State OPEN_IDLE */
    //    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    //    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    //    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* (4) OUT 00:00:00.0 B0 1001 AFFE 63 43 40 ?? ?? :DeviceDescriptorResponse(DescType=00, Descriptor=?? ?? ) */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x43, 0x40, 0x00, 0x20, 0 }; // A_DeviceDescriptor_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* BDUT is in State OPEN_WAIT */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22); // T_Data_Connected.con
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0); // do nothing
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    /* (5) IN 00:00:03.5 B0 AFFE 1001 63 46 01 01 FE :MemoryRead(Count=01, Addr=01FE - may be any memory location) */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x01, 0x01, 0xFE, 0 }; // A_Memory_Read
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* (6) OUT 00:00:00.0 B0 1001 AFFE 60 C6 :T-Ack(Seq=1) */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /**
     * @note
     * In style 1, if a T_Data_Connected.ind is received, before the previous
     * transmission is acknowledged, the connection gets closed.
     */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
    return; // @todo Test_2_3_2

#if 0
    /* BDUT persists in state OPEN_WAIT */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    // acknowledgement timeout
    bdut.transport_layer.test_expire_acknowledgement_timeout_timer(bcu1_address);

    /* (7) OUT 00:00:00.2 B0 1001 AFFE 63 43 40 ?? ?? :DeviceDescriptorResponse(DescType=00, Descriptor=?? ?? ) */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x43, 0x40, 0x00, 0x20, 0 }; // A_DeviceDescriptor_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* (8) IN 00:00:06.5 B0 AFFE 1001 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* BDUT is now in OPEN_IDLE and sends the stored Memory-Response */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* (9) OUT 00:00:00.0 B0 1001 AFFE 64 46 41 01 FE ?? :MemoryResponse(Count=01, Addr=01FE, Data=?? ) */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x46, 0x41, 0x01, 0xFE, 0x00, 0 }; // A_Memory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* (10) OUT 00:00:03.5 B0 1001 AFFE 64 46 41 01 FE ?? :MemoryResponse(Count=01, Addr=01FE, Data=?? ) */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x46, 0x41, 0x01, 0xFE, 0x00, 0 }; // A_Memory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* (11) OUT 00:00:03.5 B0 1001 AFFE 64 46 41 01 FE ?? :MemoryResponse(Count=01, Addr=01FE, Data=?? ) */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x46, 0x41, 0x01, 0xFE, 0x00, 0 }; // A_Memory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* (12) OUT 00:00:03.5 B0 1001 AFFE 64 46 41 01 FE ?? :MemoryResponse(Count=01, Addr=01FE, Data=?? ) */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x46, 0x41, 0x01, 0xFE, 0x00, 0 }; // A_Memory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* (13) OUT 00:00:03.5 B0 1001 AFFE 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
#endif
}

/**
 * Telegram sequence 21: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * all styles)
 *
 * @ingroup KNX_08_03_04_02_03_03_01
 */
TEST(Transport_Layer_Tests, Test_2_3_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION */
    const KNX::Individual_Address bdut_address{0x1001};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xAFFE};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* Initial state: A T_Connect is sent to establish the initial OPEN_IDLE state */
    /* IN B0 AFFE 1001 60 80 : T_Connect (Addr=1001) */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* BDUT is now in State Open_Idle – now the BDUT receives a MaskVersionRead */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* IN B0 AFFE 1001 61 7F 00 : MaskVersionRead() */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x7F, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* OUT B0 1001 AFFE 60 FE : T_Ack (Seq=F) */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xFE, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* BDUT is in State Open_Idle for a few seconds */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 23);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    bdut.transport_layer.test_expire_connection_timeout_timer(bcu1_address);
    io_context.poll();

    /* OUT B0 1001 AFFE 60 81 : T_Disconnect */
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* End State : closed */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 22: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * all styles)
 *
 * @ingroup KNX_08_03_04_02_03_03_02
 */
TEST(Transport_Layer_Tests, Test_2_3_3_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Send T_DATA_CONNECTED.req to remote BCU. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 5. A telegram is sent to remote BCU with NSDU=T_DATA_CONNECTED.req */
    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in state 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    /* 6. Now the BDUT receives a NSDU=T_DATA_CONNECTED_REQ (MaskVersionRead). */
    /* (6) IN 00:00:00.2 B0 A001 A000 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 7. Check that the BDUT sends a T_DATA_CONNECTED.ind and */
    /* (7) OUT 00:00:00.0 89 B0 A0 01 A0 00 61 43 00 :T_DATA_CONNECTED.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x43, 0x00 }; // T_Data_Connected.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* 8. a telegram with NSDU=T_DATA_ACK. */
    /* (8) OUT 00:00:00.0 B0 A000 A001 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 9. The BDUT receives a repeated NSDU=T_DATA_CONNECTED_REQ (MaskVersionRead). */
    /* (9) IN 00:00:00.2 B0 A001 A000 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 10. Check that the BDUT sends a telegram with NSDU=T_DATA_ACK. */
    /* (10) OUT 00:00:00.0 B0 A000 A001 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in state 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 23);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);
}

/**
 * Telegram sequence 23: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * all styles)
 *
 * @ingroup KNX_08_03_04_02_03_04_01
 */
TEST(Transport_Layer_Tests, Test_2_3_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1) {
        /* ---------------------------------------------------------------- */
        /* The BDUT is in State 'OPEN_IDLE'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

        /* 4. The BDUT receives a MaskVersionRead with a wrong sequence number. */
        /* (4) IN 00:00:00.2 B0 A001 A000 61 57 00 :MaskVersionRead() */
        tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x57, 0x00, 0 }; // A_DeviceDescriptor_Read
        bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* 5. Check that the BDUT sends a T_NAK. (Seq=5) */
        /* (5) OUT 00:00:00.0 B0 A000 A001 60 D7 :T-Nack(Seq=5) */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0xD7, 0 }; // T_Nak
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT persists in State 'OPEN_IDLE'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 24);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);
    }

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1_Rationalised) {
        /* ----------------------------------------------------------------- */
        /* Alternatively for Style 1 rationalised */

        /* The DUT is in State 'OPEN_IDLE'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

        /* 4. The DUT receives a MaskVersionRead with a wrong sequence number. */
        /* (4) IN 00:00:00.2 B0 A001 A000 61 57 00 :MaskVersionRead() */
        tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x57, 0x00, 0 }; // A_DeviceDescriptor_Read
        bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* 5. Check that the DUT sends a T_Disconnect on the bus and a T_Disconnect.ind locally. */
        /* (5) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* (6) OUT 00:00:00.0 87 00 00 00 00 00 :T_DISCONNECT.ind */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0x00, 0x00, 0x00, 0x00 }; // T_Disconnect.ind
        ASSERT_EQ(*pei_data_ind++, pei_data);

        /* The DUT changes to "CLOSED". */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
    }
}

/**
 * Telegram sequence 24: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * all styles)
 *
 * @ingroup KNX_08_03_04_02_03_04_02
 */
TEST(Transport_Layer_Tests, Test_2_3_4_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Send T_DATA_CONNECTED.req to remote BCU. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 5. A telegram is sent to remote BCU with NSDU=T_DATA_CONNECTED.req */
    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1) {
        /* ------------------------------------------------------------------------ */
        /* The BDUT is in state 'OPEN_WAIT'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

        /* 6. The BDUT receives a MaskVersionRead with a wrong sequence number. */
        /* (6) IN 00:00:00.2 B0 A001 A000 61 57 00 :MaskVersionRead() */
        tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x57, 0x00, 0 }; // A_DeviceDescriptor_Read
        bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* 7. Check that the BDUT sends a T_NAK. (Seq=5) */
        /* (7) OUT 00:00:00.0 B0 A000 A001 60 D7 :T-Nack(Seq=5) */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0xD7, 0 }; // T_Nak
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT is still in state 'OPEN_WAIT'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 24);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);
    }

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1_Rationalised) {
        /* ------------------------------------------------------------------------ */
        /* Alternatively for Style 1 rationalised */
        /* The DUT is in state 'OPEN_WAIT'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

        /* 6. The DUT receives a MaskVersionRead with a wrong sequence number. */
        /* (6) IN 00:00:00.2 B0 A001 A000 61 57 00 :MaskVersionRead() */
        tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x57, 0x00, 0 }; // A_DeviceDescriptor_Read
        bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* 7. Check that the DUT sends a T_Disconnect on the Bus and a T_Disconnect.ind locally. */
        /* (7) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* (8) OUT 00:00:00.0 87 00 00 00 00 00 :T_DISCONNECT.ind */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0x00, 0x00, 0x00, 0x00 }; // T_Disconnect.ind
        ASSERT_EQ(*pei_data_ind++, pei_data);

        /* The DUT changes to "CLOSED". */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
    }
}

/**
 * Telegram sequence 25: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * style 1)
 *
 * @ingroup KNX_08_03_04_02_03_05_01
 */
TEST(Transport_Layer_Tests, Test_2_3_5_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. Activate the Link-Layer. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. BDUT receives a T_CONNECT.con */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    /* 4. The BDUT receives a MaskVersionRead from remote BCU (<> connected BCU = third BCU) */
    /* (4) IN 00:00:00.2 B0 A001 A000 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT_REQ. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    /* Note: Style 2/3 - BDUT sends no Disconnect on the bus. */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);
}

/**
 * Telegram sequence 26: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * style 1)
 *
 * @ingroup KNX_08_03_04_02_03_05_02
 */
TEST(Transport_Layer_Tests, Test_2_3_5_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. Activate the Link-Layer. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. BDUT receives a T_CONNECT.con */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    /* 4. BDUT sends a T_DATA_CONNECTED.req to the third BCU. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Wait);

    /* 5. The BDUT receives a MaskVersionRead from remote BCU (<> connected BCU = third BCU) */
    /* (5) IN 00:00:04.0 B0 A001 A000 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT_REQ. */
    /* (6) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    /* Note: Style 2/3 - BDUT sends no Disconnect on the bus. */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Wait);
}

/**
 * Telegram sequence 27: Procedure with initial state ‘OPEN_IDLE’ (style 1/3)
 *
 * @ingroup KNX_08_03_04_02_04_01_01
 */
TEST(Transport_Layer_Tests, Test_2_4_1_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT gets a T_DATA_CONNECTED.req. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 5. Check that the BDUT sends a telegram with NSDU=T_DATA_CONNECTED.req. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    /* 6. Now the BDUT receives a T_ACK. */
    /* (6) IN 00:00:00.2 B0 A001 A000 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 7. Check that the BDUT goes to 'OPEN_IDLE' and sends a T_DATA_CONNECTED.con */
    /* (7) OUT 00:00:00.0 8E B0 A0 00 00 00 61 43 00 :T_DATA_CONNECTED.con */
    /* Note : Style 2 - BDUT sends no T_Data_Connected.con. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 8);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 8);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_con), 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00 }; // T_Data_Connected.con // @note destination address changed from 0x0000 to 0xA001
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * Telegram sequence 28: Procedure with initial state ‘CLOSED’ to check event 15
 * (style 1)
 *
 * @ingroup KNX_08_03_04_02_04_01_02
 */
TEST(Transport_Layer_Tests, Test_2_4_1_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    //std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* The BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);

    /* 2. Now the BDUT gets a T_DATA_CONNECTED.req */
    /* (2) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 3. Check that a T_DISCONNECT_ind is sent. */
    /* (3) OUT 00:00:00.0 87 00 A0 00 A0 00 :T_DISCONNECT.ind */
    /* Note: Style 2/3 - BDUT sends no T_Disconnect.ind. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0x00, 0x00, 0xA0, 0x00 }; // T_Disconnect.ind // @note Source changed from 0xA000 to 0x0000
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT persists in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 29: Procedure with initial state ‘OPEN_WAIT to check event
 * 15 (style 1)
 *
 * @ingroup KNX_08_03_04_02_04_01_03
 */
TEST(Transport_Layer_Tests, Test_2_4_1_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT gets a T_DATA_CONNECTED.req. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 5. The BDUT sends a telegram with NSDU=T_DATA_CONNECTED.req. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1) {
        /* --------------------------------------------------------------------------- */
        /* The BDUT is in State 'OPEN_WAIT'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

        /* 6. Now the BDUT gets a T_DATA_CONNECTED.req again. */
        /* (6) IN 00:00:07.0 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
        bdut.emi_req(pei_data);
        io_context.poll();

        /* 7. Check that a telegram is sent addressed to BCU with NSDU=T_DISCONNECT.req */
        /* (7) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* 8. Check that a T_DISCONNECT_ind is sent */
        /* (8) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
        ASSERT_EQ(*pei_data_ind++, pei_data);

        /* The BDUT is in State 'CLOSED'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
    }

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1_Rationalised) {
        /* -------------------------------------------------------------------------- */
        /* Alternatively for Style 1 rationalised */

        /* The DUT is in State 'OPEN_WAIT'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

        /* 6. The DUT receives a second T_DATA.requ. */
        /* (6) IN 00:00:03.0 41 B0 00 00 00 00 01 03 00 :T_DATA.req */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
        bdut.emi_req(pei_data);

        /* 7. No reaction to the second T_Data.req, until BDUT receives a T_ACK. */
        /* (7) IN 00:00:02.0 B0 A001 A000 60 C2 :T-Ack(Seq=0) */
        tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC2, 0 }; // T_Ack
        bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* (8) OUT 00:00:00.0 8E B0 A0 00 AF 00 61 43 00 :T_DATA.conf */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_con), 0xB0, 0xA0, 0x00, 0xAF, 0x00, 0x61, 0x43, 0x00 }; // T_Data_Connected.con
        ASSERT_EQ(*pei_data_con++, pei_data);

        /* (9) OUT 00:00:00.0 B0 A000 A001 61 47 00 :MaskVersionRead() */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x47, 0x00, 0 }; // A_DeviceDescriptor_Read
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* 8. BDUT is in OPEN_WAIT, timeout after 6s. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

        /* (10) OUT 00:00:05.8 B0 A000 A001 60 81 :T-Disconnect */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* (11) OUT 00:00:00.0 87 00 00 00 00 00 :T_DISCONNECT.ind */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0x00, 0x00, 0x00, 0x00 }; // T_Disconnect.ind
        ASSERT_EQ(*pei_data_ind++, pei_data);

        /* The DUT is in State 'CLOSED'. */
        /* Note: Style 2/3 - BDUT remains in OPEN_WAIT, BDUT sends T_Data_Connected on the bus after leaving OPEN_WAIT. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
    }
}

/**
 * Telegram sequence 30: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * styles 1/2)
 *
 * @ingroup KNX_08_03_04_02_04_02_01
 */
TEST(Transport_Layer_Tests, Test_2_4_2_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT receives a T_ACK. */
    /* (4) IN 00:00:00.2 B0 A001 A000 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that a telegram is sent addressed to BCU with NSDU=T_DISCONNECT.req */
    /* (5) OUT 00:00:00.2 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 6. Check that a T_DISCONNECT_ind is sent */
    /* (6) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'CLOSED'. */
    /* Note: Style 3 - BDUT remains in OPEN_IDLE, BDUT sends no T_Disconnect.ind and no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 31: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * styles 1/2)
 *
 * @ingroup KNX_08_03_04_02_04_03_01
 */
TEST(Transport_Layer_Tests, Test_2_4_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT receives a T_ACK with wrong sequence number. */
    /* (4) IN 00:00:00.2 B0 A001 A000 60 D6 :T-Ack(Seq=5) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xD6, 0 }; // T_Ack
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT.req. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 6. Check that a T_DISCONNECT_ind is sent */
    /* (6) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT goes into state ‘CLOSED'. */
    /* Note: Style 3 - BDUT remains in OPEN_IDLE, BDUT sends no T_Disconnect.ind and no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 32: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * styles 1/3)
 *
 * @ingroup KNX_08_03_04_02_04_03_02
 */
TEST(Transport_Layer_Tests, Test_2_4_3_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT gets a T_DATA_CONNECTED.req. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 5. The BDUT sends a telegram with NSDU=T_DATA_CONNECTED.req. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    /* 6. Now the BDUT receives a T_ACK with wrong sequence number. */
    /* (6) IN 00:00:00.2 B0 A001 A000 60 D6 :T-Ack(Seq=5) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xD6, 0 }; // T_Ack
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 7. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT.req. */
    /* (7) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 8. Check that a T_DISCONNECT_ind is sent */
    /* (8) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    /* Note: Style 2 - BDUT remains in OPEN_WAIT and sends no T_Disconnect.ind and no Disconnect on the bus. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * Telegram sequence 33: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * style 1)
 *
 * @ingroup KNX_08_03_04_02_04_04_01
 */
TEST(Transport_Layer_Tests, Test_2_4_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. Activate the Link-Layer. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. BDUT receives a T_CONNECT.con */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    /* 4. The BDUT receives a T_ACK from remote BCU (<> connected BCU = third BCU) */
    /* (4) IN 00:00:00.2 B0 A001 A000 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT.req. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'OPEN_IDLE'. */
    /* Note: Style 2/3 - BDUT sends no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);
}

/**
 * Telegram sequence 34: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * style 1)
 *
 * @ingroup KNX_08_03_04_02_04_04_02
 */
TEST(Transport_Layer_Tests, Test_2_4_4_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. Activate the Link-Layer. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. BDUT receives a T_CONNECT.con */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    /* 4. BDUT sends a T_DATA_CONNECTED.req to the third BCU. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Wait);

    /* 5. The BDUT receives a T_ACK from remote BCU (<> connected BCU = third BCU) */
    /* (5) IN 00:00:04.0 B0 A001 A000 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 6. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT.req. */
    /* (6) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'OPEN_WAIT'. */
    /* Note: Style 2/3 - BDUT sends no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Wait);
}

/**
 * Telegram Sequence 35: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * style 1/2)
 *
 * @ingroup KNX_08_03_04_02_04_05_01
 */
TEST(Transport_Layer_Tests, Test_2_4_5_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT receives a T_NACK with wrong sequence number. */
    /* (4) IN 00:00:00.2 B0 A001 A000 60 D7 :T-Nack(Seq=5) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xD7, 0 }; // T_Nak
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that the BDUT sends a T_DISCONNECT.ind */
    /* (5) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* 6. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT.req. */
    /* (6) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT takes the State 'CLOSED'. */
    /* Note: Style 3 - BDUT remains in OPEN_IDLE, BDUT sends no T_Disconnect.ind and no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram Sequence 36: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * style 1/3)
 *
 * @ingroup KNX_08_03_04_02_04_05_02
 */
TEST(Transport_Layer_Tests, Test_2_4_5_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT gets a T_DATA_CONNECTED.req. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 5. The BDUT sends a telegram with NSDU=T_DATA_CONNECTED.req. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    /* 6. Now the BDUT receives a T_NACK with wrong sequence number. */
    /* (6) IN 00:00:00.2 B0 A001 A000 60 D7 :T-Nack(Seq=5) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xD7, 0 }; // T_Nak
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 7. Check that the BDUT sends a T_DISCONNECT.ind */
    /* (7) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* 8. Send telegram addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (8) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* Now the BDUT is in State 'CLOSED'. */
    /* Note: Style 2 - BDUT remains in OPEN_WAIT, BDUT sends no T_Disconnect.ind and no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram Sequence 37: Reception of T_NAK_PDU with correct sequence
 * number - Procedure with initial state ‘OPEN_IDLE’ (without PEI, style 1/3)
 *
 * @ingroup KNX_08_03_04_02_04_06
 */
TEST(Transport_Layer_Tests, Test_2_4_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT receives a T_NACK with correct sequence number. */
    /* (4) IN 00:00:00.2 B0 A001 A000 60 C3 :T-Nack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC3, 0 }; // T_Nak
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that the BDUT sends a T_DISCONNECT.ind */
    /* (5) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* 6. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT.req. */
    /* (6) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT takes the State 'CLOSED'. */
    /* Note: style 2 - BDUT remains in OPEN_IDLE, BDUT sends no T_Disconnect.ind and no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram Sequence 38: Reception of T_NAK_PDU and maximum number
 * of repetitions is not reached - Procedure with initial state ‘OPEN_WAIT’
 * (without PEI, all styles)
 *
 * @ingroup KNX_08_03_04_02_04_07
 */
TEST(Transport_Layer_Tests, Test_2_4_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT gets a T_DATA_CONNECTED.req. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 5. The BDUT sends a telegram with NSDU=T_DATA_CONNECTED.req. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1) {
        /* ------------------------------------------------------------------------- */
        /* The BDUT is in State 'OPEN_WAIT'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

        /* 6. Now the BDUT receives a T_NACK. */
        /* (6) IN 00:00:00.2 B0 A001 A000 60 C3 :T-Nack(Seq=0) */
        tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC3, 0 }; // T_Nak
        bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* 7. The BDUT sends a telegram with NSDU=T_DATA_CONNECTED.req (stored message). */
        /* (7) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT persists in State 'OPEN_WAIT'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);
    }

    if (bdut.transport_layer.connection_style(bcu1_address) == KNX::Transport_Layer::Connection_Style::Style_1_Rationalised) {
        /* ------------------------------------------------------------------------- */
        /* Alternatively for Style 1 rationalised */
        /* The DUT is in State 'OPEN_WAIT'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

        /* 4. Now the DUT receives a T_NACK with the correct sequence number. */
        /* (6) IN 00:00:00.2 B0 A001 A000 60 C3 :T-Nack(Seq=0) */
        tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC3, 0 }; // T_Nak
        bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* 5. Check that the DUT sends a T_DISCONNECT.ind */
        /* (7) OUT 00:00:00.0 87 00 00 00 00 00 :T_DISCONNECT.ind */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0x00, 0x00, 0x00, 0x00 }; // T_Disconnect.ind
        ASSERT_EQ(*pei_data_ind++, pei_data);

        /* 6. Check that the DUT sends a telegram with NSDU=T_DISCONNECT.req. */
        /* (8) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
        tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect.req
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The DUT goes to 'CLOSED'. */
        ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 2);
        ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 5);
        ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
    }
}

/**
 * Telegram Sequence 39: Reception of T_NAK_PDU and maximum number
 * of repetitions is reached - Procedure with initial state ‘OPEN_WAIT’
 * (without PEI, all styles – optional style 1 rationalised)
 *
 * @ingroup KNX_08_03_04_02_04_08
 */
TEST(Transport_Layer_Tests, Test_2_4_8)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Send T_CONNECT to BDUT. */
    /* (2) IN 00:00:00.2 B0 A001 A000 60 80 :T-Connect(Addr=10.00.000) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a T_CONNECT_ind to remote BCU */
    /* (3) OUT 00:00:00.0 85 B0 A0 01 A0 00 :T_CONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xA0, 0x01, 0xA0, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 1);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Idle);

    /* 4. Now the BDUT gets a T_DATA_CONNECTED.req. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* 5. The BDUT sends a telegram with NSDU=T_DATA_CONNECTED.req. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is still in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Open_Wait);

    bdut.transport_layer.test_expire_acknowledgement_timeout_timer(bcu1_address);
    io_context.poll();

    /* (6) OUT 00:00:03.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    bdut.transport_layer.test_expire_acknowledgement_timeout_timer(bcu1_address);
    io_context.poll();

    /* (7) OUT 00:00:03.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    bdut.transport_layer.test_expire_acknowledgement_timeout_timer(bcu1_address);
    io_context.poll();

    /* (8) OUT 00:00:03.0 B0 A000 A001 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 6. Now the BDUT receives a T_NACK. */
    /* (9) IN 00:00:10.0 B0 A001 A000 60 C3 :T-Nack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC3, 0 }; // T_Nak
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 7. Check that the BDUT sends a T_DISCONNECT.ind */
    /* (10) OUT 00:00:00.0 87 00 A0 01 A0 00 :T_DISCONNECT.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xA0, 0x01, 0xA0, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* 8. Send telegram addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (11) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

/**
 * Telegram sequence 40: Procedure with initial state ‘OPEN_IDLE’ (without PEI,
 * style 1)
 *
 * @ingroup KNX_08_03_04_02_04_09_01
 */
TEST(Transport_Layer_Tests, Test_2_4_9_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. Activate the Link-Layer. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. BDUT receives a T_CONNECT.con */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    /* 4. The BDUT receives a T_NACK from remote BCU (<> connected BCU = third BCU) */
    /* (4) IN 00:00:00.2 B0 A001 A000 60 C3 :T-Nack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC3, 0 }; // T_Nak
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that a telegram is sent addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (5) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'OPEN_IDLE'. */
    /* Note: Style 2/3 - BDUT sends no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);
}

/**
 * Telegram sequence 41: Procedure with initial state ‘OPEN_WAIT’ (without PEI,
 * style 1)
 *
 * @ingroup KNX_08_03_04_02_04_09_02
 */
TEST(Transport_Layer_Tests, Test_2_4_9_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* PREPARATION: Install a third BCU with physical address A007H. Activate the Link-Layer. */
    const KNX::Individual_Address bcu2_address{0xA007};
    bcu2.group_address_table->individual_address = bcu2_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* 2. Connect BDUT with third BCU */
    /* (2) IN 00:00:00.2 43 00 00 00 A0 07 :T_CONNECT.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xA0, 0x07 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x60, 0x80, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. BDUT receives a T_CONNECT.con */
    /* (3) OUT 00:00:00.0 86 B0 A0 07 A0 00 :T_CONNECT.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xA0, 0x07, 0xA0, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* The BDUT is in State 'OPEN_IDLE'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 19);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 13);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Idle);

    /* 4. BDUT sends a T_DATA_CONNECTED.req to the third BCU. */
    /* (4) IN 00:00:00.2 41 B0 00 00 00 00 01 03 00 :T_DATA_CONNECTED.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0xB0, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x07, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT is in State 'OPEN_WAIT'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Wait);

    /* 5. The BDUT receives a T_NACK from remote BCU (<> connected BCU = third BCU) */
    /* (5) IN 00:00:04.0 B0 A001 A000 60 C3 :T-Nack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC3, 0 }; // T_Nak
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 6. Check that a telegram is sent addressed to remote BCU with NSDU=T_DISCONNECT.req */
    /* (6) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'OPEN_WAIT'. */
    /* Note : Style 2/3 - BDUT sends no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu2_address), 22);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu2_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu2_address), KNX::Connection_State::Open_Wait);
}

/**
 * Telegram Sequence 42: Procedure with initial state ‘CLOSED’ (without
 * PEI, style 1)
 *
 * @ingroup KNX_08_03_04_02_05_01
 */
TEST(Transport_Layer_Tests, Test_2_5_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    /* remote BCU No 1 */
    BCU_Plain_Data bcu1(io_context, tp1_bus);

    /* BCU No 2 (3rd BCU) */
    //BCU_Plain_Data bcu2(io_context, tp1_bus);

    /* BCU No 3 to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION: Set physical address of BDUT to A000H. The remote BCU is set to A001H. */
    const KNX::Individual_Address bdut_address{0xA000};
    bdut.interface_objects->group_address_table()->individual_address = bdut_address;
    const KNX::Individual_Address bcu1_address{0xA001};
    bcu1.group_address_table->individual_address = bcu1_address;

    /* 1. Set PEI of BDUT to Transport-Layer (remote) */
    /* (1) IN start A9 00 12 34 48 88 0A :PEI_SWITCH.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);

    /* The BDUT is in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);

    /* 2. Now the BDUT receives a NSDU=T_DATA_CONNECTED_REQ (MaskVersionRead). */
    /* (2) IN 00:00:00.2 B0 A001 A000 61 43 00 :MaskVersionRead() */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 3. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT_REQ. */
    /* (3) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);

    /* 4. Now the BDUT receives a T_ACK. */
    /* (4) IN 00:00:00.2 B0 A001 A000 60 C2 :T-Ack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 5. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT_REQ. */
    /* (5) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'CLOSED'. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);

    /* 6. Now the BDUT receives a T_NAK. */
    /* (6) IN 00:00:00.2 B0 A001 A000 60 C3 :T-Nack(Seq=0) */
    tp1_data = { 0xB0, 0xA0, 0x01, 0xA0, 0x00, 0x60, 0xC3, 0 }; // T_Nak
    bcu1.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 7. Check that the BDUT sends a telegram with NSDU=T_DISCONNECT_REQ. */
    /* (7) OUT 00:00:00.0 B0 A000 A001 60 81 :T-Disconnect */
    tp1_data = { 0xB0, 0xA0, 0x00, 0xA0, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT persists in State 'CLOSED'. */
    /* Note: Style 2/3 - BDUT sends no Disconnect on the bus. */
    ASSERT_EQ(bdut.transport_layer.connection_last_event(bcu1_address), 21);
    ASSERT_EQ(bdut.transport_layer.connection_last_action(bcu1_address), 0);
    ASSERT_EQ(bdut.transport_layer.connection_state(bcu1_address), KNX::Connection_State::Closed);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
