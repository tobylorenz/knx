// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "KNX/03/05/01/Interface_Object_Type.h"
#include "gtest/gtest.h"

#include <KNX/09/04/01/TP1_BCU2.h>

class Application_Layer_Tests : public ::testing::Test
{
    virtual ~Application_Layer_Tests() = default;
};

/** BCU that can be used as Bus Device Under Test (BDUT) */
class BCU :
    public KNX::TP1_BCU2
{
public:
    explicit BCU(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        KNX::TP1_BCU2(io_context, tp1_bus_simulation)
    {
        A_Restart_ind_initiator();
    }

    /** counter of received A_Restart_ind */
    uint8_t A_Restart_ind_count{};

protected:
    void A_Restart_ind_initiator() {
        application_layer.A_Restart_ind([this](const KNX::Restart_Erase_Code /*erase_code*/, const KNX::Restart_Channel_Number /*channel_number*/, const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/) -> void
        {
            ++A_Restart_ind_count;

            A_Restart_ind_initiator();
        });
    }
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class BCU_Plain_Data
{
public:
    explicit BCU_Plain_Data(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context)
    {
        tp1_physical_layer.connect(tp1_bus_simulation);

        tp1_data_link_layer.group_address_table = group_address_table;
        tp1_data_link_layer.L_Busmon_ind = std::bind(&BCU_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/* 1.4 Group objects (GO) */

/* 1.4.1 Tests with accessible configuration and communication flags */

/**
 * BDUT sends A_GroupValue_Read
 *
 * @ingroup KNX_08_03_07_01_04_01_01
 */
TEST(Application_Layer_Tests, Test_1_4_1_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION */
    // group objects
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.application_interface_layer.group_object_server.group_object_table;
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1); // 1 bit
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(1); // 4 bit -> communication flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_2_value = std::make_shared<std::vector<uint8_t>>(1); // -> configuration flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_3_value = std::make_shared<std::vector<uint8_t>>(1); // -> GO0 value without modification of config/comm flags
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].type = KNX::Group_Object_Type::Unsigned_Integer_4;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].data = group_object_2_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].type = KNX::Group_Object_Type::Octet_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].data = group_object_3_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].type = KNX::Group_Object_Type::Octet_1;
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0)), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(1)), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(2)), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(3)), 3);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(KNX::TSAP_Group(0)), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(KNX::TSAP_Group(1)), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(KNX::TSAP_Group(2)), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(KNX::TSAP_Group(3)), 3);
    // address table
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1000)); // Group_Address -> TSAP 1
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1001)); // Group_Address -> TSAP 2
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002)); // Group_Address -> TSAP 3
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1003)); // Group_Address -> TSAP 4
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(1)), KNX::Group_Address(0x1000));
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(2)), KNX::Group_Address(0x1001));
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(3)), KNX::Group_Address(0x1002));
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(4)), KNX::Group_Address(0x1003));
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::TSAP_Group(0x1000)), 1);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::TSAP_Group(0x1001)), 2);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::TSAP_Group(0x1002)), 3);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::TSAP_Group(0x1003)), 4);
    // association table
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(2), KNX::ASAP_Group(1)); // TSAP 2 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(3), KNX::ASAP_Group(2)); // TSAP 3 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(4), KNX::ASAP_Group(3)); // TSAP 4 -> ASAP
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(1)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(2)), 3);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(3)), 4);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(1)), 0);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(2)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(3)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(4)), 3);

    // Preparation: reset object data and flags

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1002 E2 00 80 DF :set Configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1003 E2 00 80 00 :clear data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x00, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // IN BC AFFE 1003 E2 00 80 01 :clear data to other value than default value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x01, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // Generate read request with different priorities

    // Low priority

    // IN BC AFFE 1001 E1 00 87 :set read request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x87, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // Acceptance: BDUT sends A_GroupValue_Read with low priority and Comm. flags are set accordingly

    // OUT BC 1101 1000 E1 00 00 : BDUT sends Value Read request
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 44 :Comm.-flags = idle/OK, read
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x44, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Normal priority

    // IN BC AFFE 1003 E2 00 80 00 :clear data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x00, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // IN BC AFFE 1002 E2 00 80 DD :set priority to normal
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDD, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1001 E1 00 87 :set read request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x87, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // Acceptance: BDUT sends A_GroupValue_Read with normal priority

    // OUT B4 1101 1000 E1 00 00 : BDUT sends Value Read request
    tp1_data = { 0xB4, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Urgent priority

    // IN BC AFFE 1002 E2 00 80 DE :set priority to urgent
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDE, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 87 :set read request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x87, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // Acceptance: BDUT sends A_GroupValue_Read with urgent priority

    // OUT B8 1101 1000 E1 00 00 : BDUT sends Value Read request
    tp1_data = { 0xB8, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // System priority

    // IN BC AFFE 1002 E2 00 80 DC :set priority to system
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDC, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 87 :set read request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x87, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // Acceptance: BDUT sends A_GroupValue_Read with system priority

    // OUT B0 1101 1000 E1 00 00 : BDUT sends Value Read request
    tp1_data = { 0xB0, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Check function of Configuration flags

    // Disable "communication"

    // IN BC AFFE 1002 E2 00 80 DB :disable communication in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDB, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1001 E1 00 87 :set read request in communication flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x87, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Acceptance: BDUT does not send an A_GroupValue_Read and Comm. flags are set accordingly.

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 45 :Comm.-flags = BCU 2 : idle/error, read - BCU 1 : status of flags as set
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x47, 0 }; // A_GroupValue_Response (BCU1)
    //tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x45, 0 }; // A_GroupValue_Response (BCU2)
    ASSERT_EQ(*log++, tp1_data);

    // Disable "read"

    // IN BC AFFE 1002 E2 00 80 D7 :disable read in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xD7, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1001 E1 00 87 :set read request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x87, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // Acceptance: BDUT sends A_GroupValue_Read and Comm. flags are set accordingly.

    // OUT BC 1101 1000 E1 00 00 : BDUT sends Value Read
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 44 :Comm.-flags = idle/OK, read
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x44, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Disable "write"

    // IN BC AFFE 1002 E2 00 80 CF :disable write enable in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xCF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1001 E1 00 87 :set read request in communication flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x87, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // Acceptance: BDUT sends A_GroupValue_Read and Comm. flags are set accordingly.

    // OUT BC 1101 1000 E1 00 00 : BDUT sends Value Read
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 44 :Comm.-flags = idle/OK, read
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x44, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Disable "transmission"

    // IN BC AFFE 1002 E2 00 80 9F :disable transmission in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x9F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1001 E1 00 87 :set read request in communication flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x87, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Acceptance: BDUT does not send an A_GroupValue_Read and Comm. flags are set accordingly.

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 45 : Comm.-flags = BCU 2 : idle/error, read - BCU 1 : status of flags as set
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x47, 0 }; // A_GroupValue_Response (BCU1)
    //tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x45, 0 }; // A_GroupValue_Response (BCU2)
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Disable "read response update"

    // IN BC AFFE 1002 E2 00 80 5F :disable read response update in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x5F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 87 :set read request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x87, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // Acceptance: BDUT sends A_GroupValue_Read and Comm. flags are set accordingly.

    // OUT BC 1101 1000 E1 00 00 : BDUT sends Value Read
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 44 :Comm.-flags = idle/OK, read
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x44, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack
}

/**
 * BDUT receives A_GroupValue_Read
 *
 * @ingroup KNX_08_03_07_01_04_01_02
 */
TEST(Application_Layer_Tests, Test_1_4_1_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION */
    // group objects
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.application_interface_layer.group_object_server.group_object_table;
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1); // 1 bit
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(1); // 4 bit -> communication flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_2_value = std::make_shared<std::vector<uint8_t>>(1); // -> configuration flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_3_value = std::make_shared<std::vector<uint8_t>>(1); // -> GO0 value without modification of config/comm flags
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].type = KNX::Group_Object_Type::Unsigned_Integer_4;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].data = group_object_2_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].type = KNX::Group_Object_Type::Octet_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].data = group_object_3_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].type = KNX::Group_Object_Type::Octet_1;
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0)), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(1)), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(2)), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(3)), 3);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(KNX::TSAP_Group(0)), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(KNX::TSAP_Group(1)), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(KNX::TSAP_Group(2)), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(KNX::TSAP_Group(3)), 3);
    // address table
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1000)); // Group_Address -> TSAP 1
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1001)); // Group_Address -> TSAP 2
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002)); // Group_Address -> TSAP 3
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1003)); // Group_Address -> TSAP 4
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(1)), KNX::Group_Address(0x1000));
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(2)), KNX::Group_Address(0x1001));
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(3)), KNX::Group_Address(0x1002));
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(4)), KNX::Group_Address(0x1003));
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1000)), 1);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1001)), 2);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1002)), 3);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1003)), 4);
    // association table
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(2), KNX::ASAP_Group(1)); // TSAP 2 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(3), KNX::ASAP_Group(2)); // TSAP 3 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(4), KNX::ASAP_Group(3)); // TSAP 4 -> ASAP
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(1)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(2)), 3);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(3)), 4);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(1)), 0);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(2)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(3)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(4)), 3);

    // Preparation: reset object data and flags.

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1002 E2 00 80 DF :set Configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1003 E2 00 80 00 :clear data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x00, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // IN BC AFFE 1003 E2 00 80 01 :set object to other data than default
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x01, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // BDUT receives read requests with different priorities

    // Low priority

    // IN BC AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_GroupValue_Response with correct data.

    // OUT BC 1101 1000 E1 00 41 :==> generated valueResponse according to priority set in Value Read (optionally with priority set in object)
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Normal priority

    // IN B4 AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xB4, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B4 1101 1000 E1 00 41 :==> generated valueResponse according to priority set in Value Read (optionally with priority set in object)
    tp1_data = { 0xB4, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Urgent priority

    // IN B8 AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xB8, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B8 1101 1000 E1 00 41 :==> generated valueResponse according to priority set in Value Read (optionally with priority set in object)
    tp1_data = { 0xB8, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // System priority

    // IN B0 AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1101 1000 E1 00 41 :==> generated valueResponse according to priority set in Value Read
    tp1_data = { 0xB0, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack
    // (optionally with priority set in object)

    // BDUT receives read requests with different routing counters

    // Acceptance: generate response with correct routing counter setting

    // IN BC AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1000 E1 00 41 :==> generated valueResponse according to set routing counter
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1000 81 00 00 :read object value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0x81, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1000 E1 00 41 :==> generated valueResponse according to set routing counter
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1000 F1 00 00 :read object value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xF1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1000 F1 00 41 :==> generated valueResponse with routing counter 7 (identical to
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xF1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // routing counter in ValueRead) or optionally with 6

    // Check function of Configuration flags

    // Disable "communication"

    // Acceptance: no response generated
    ASSERT_EQ(log, std::cend(busmoni.log));

    // IN BC AFFE 1002 E2 00 80 DB :disable communication in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDB, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Disable "read"

    // Acceptance: no response generated
    ASSERT_EQ(log, std::cend(busmoni.log));

    // IN BC AFFE 1002 E2 00 80 D7 :disable read in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xD7, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Disable "write"

    // Acceptance: response generated

    // IN BC AFFE 1002 E2 00 80 CF :disable write in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xCF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1000 E1 00 41 :==> generated valueResponse
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Disable "transmission"

    // Acceptance: response generated

    // IN BC AFFE 1002 E2 00 80 9F :disable transmission in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x9F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1000 E1 00 41 :==> generated valueResponse
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Disable "read response update"

    // Acceptance: response generated

    // IN BC AFFE 1002 E2 00 80 5F :disable read response update in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x5F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 00 :read object value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1000 E1 00 41 :==> generated valueResponse
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack
}

/**
 * BDUT sends A_GroupValue_Write
 *
 * @ingroup KNX_08_03_07_01_04_01_03
 */
TEST(Application_Layer_Tests, Test_1_4_1_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION */
    // group objects
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.application_interface_layer.group_object_server.group_object_table;
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1); // 1 bit
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(1); // 4 bit -> communication flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_2_value = std::make_shared<std::vector<uint8_t>>(1); // -> configuration flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_3_value = std::make_shared<std::vector<uint8_t>>(1); // -> GO0 value without modification of config/comm flags
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].type = KNX::Group_Object_Type::Unsigned_Integer_4;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].data = group_object_2_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].type = KNX::Group_Object_Type::Octet_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].data = group_object_3_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].type = KNX::Group_Object_Type::Octet_1;
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0)), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(1)), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(2)), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(3)), 3);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(0), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(1), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(2), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(3), 3);
    // address table
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1000)); // Group_Address -> TSAP 1
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1001)); // Group_Address -> TSAP 2
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002)); // Group_Address -> TSAP 3
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1003)); // Group_Address -> TSAP 4
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(1)), 0x1000);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(2)), 0x1001);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(3)), 0x1002);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(4)), 0x1003);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1000)), 1);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1001)), 2);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1002)), 3);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1003)), 4);
    // association table
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(2), KNX::ASAP_Group(1)); // TSAP 2 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(3), KNX::ASAP_Group(2)); // TSAP 3 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(4), KNX::ASAP_Group(3)); // TSAP 4 -> ASAP
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(1)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(2)), 3);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(3)), 4);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(1)), 0);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(2)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(3)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(4)), 3);

    // Preparation: reset object data and flags

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1002 E2 00 80 DF :set Configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1003 E2 00 80 00 :clear data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x00, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // IN BC AFFE 1003 E2 00 80 01 :set object to value other than default
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x01, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // Stimulate BDUT to send A_GroupValue_Write with different priorities

    // Low priority

    // IN BC AFFE 1001 E1 00 83 :set transmit request in communication flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x83, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // Acceptance: BDUT sends message with correct data and Comm. flags are set accordingly.

    // OUT BC 1101 1000 E1 00 81 :--> generated valueWrite
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 40 :Comm.-flags = idle/OK
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Normal priority

    // IN BC AFFE 1002 E2 00 80 DD :set priority to normal
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDD, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 83 :set transmit request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x83, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // OUT B4 1101 1000 E1 00 81 :--> generated valueWrite
    tp1_data = { 0xB4, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Urgent priority

    // IN BC AFFE 1002 E2 00 80 DE :set priority to urgent
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDE, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 83 :set transmit request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x83, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // OUT B8 1101 1000 E1 00 81 :--> generated valueWrite
    tp1_data = { 0xB8, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // System priority

    // IN BC AFFE 1002 E2 00 80 DC :set priority to system
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDC, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 83 :set transmit request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x83, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // OUT B0 1101 1000 E1 00 81 :--> generated valueWrite
    tp1_data = { 0xB0, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Check function of Configuration flags

    // Disable "communication"

    // Acceptance: ==> no telegram generated, check Comm. flags

    // IN BC AFFE 1002 E2 00 80 DB :disable communication in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDB, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 83 :set transmit request in communication flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x83, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 41 :Comm.-flags = idle/error (BCU 2), transmit request (BCU1)
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x43, 0 }; // A_GroupValue_Response (BCU1)
    //tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response (BCU2)
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :reset Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "read"

    // Acceptance: ==> generate telegram, check Comm. flags

    // IN BC AFFE 1002 E2 00 80 D7 :disable read in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xD7, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 83 :set transmit request in communication flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x83, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // OUT BC 1101 1000 E1 00 81 :--> generated valueWrite
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 40 :Comm.-flags = idle/OK (BCU2), according to value set (BCU1)
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x43, 0 }; // A_GroupValue_Response (BCU1)
    //tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response (BCU2)
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Disable "write"

    // Acceptance: ==> generate telegram, check Comm. flags

    // IN BC AFFE 1002 E2 00 80 CF :disable write in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xCF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 83 :set transmit request in communication flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x83, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // OUT BC 1101 1000 E1 00 81 :--> generated valueWrite
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 40 :Comm.-flags = idle/OK
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Disable "transmission"

    // Acceptance: ==> no telegram generated, check Comm. flags

    // IN BC AFFE 1002 E2 00 80 9F :disable transmission in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x9F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 83 :set transmit request in communication flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x83, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 41 :Comm.-flags = idle/error (BCU 2), transmit request (BCU1)
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x43, 0 }; // A_GroupValue_Response (BCU1)
    //tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response (BCU2)
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :reset Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "read response update"

    // Acceptance: ==> generate telegram, check Comm. flags

    // IN BC AFFE 1002 E2 00 80 5F :disable read response update
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x5F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1001 E1 00 83 :set transmit request
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x83, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    bdut.application_interface_layer.group_object_server.process_requests();
    io_context.poll();

    // OUT BC 1101 1000 E1 00 81 :--> generated valueWrite
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 40 :Comm.-flags = idle/OK (BCU2), according to value set (BCU1)
    //tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x43, 0 }; // A_GroupValue_Response (BCU1)
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response (BCU2)
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack
}

/**
 * BDUT receives A_GroupValue_Write
 *
 * @ingroup KNX_08_03_07_01_04_01_04
 */
TEST(Application_Layer_Tests, Test_1_4_1_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION */
    // group objects
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.application_interface_layer.group_object_server.group_object_table;
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1); // 1 bit
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(1); // 4 bit -> communication flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_2_value = std::make_shared<std::vector<uint8_t>>(1); // -> configuration flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_3_value = std::make_shared<std::vector<uint8_t>>(1); // -> GO0 value without modification of config/comm flags
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].type = KNX::Group_Object_Type::Unsigned_Integer_4;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].data = group_object_2_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].type = KNX::Group_Object_Type::Octet_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].data = group_object_3_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].type = KNX::Group_Object_Type::Octet_1;
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0)), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(1)), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(2)), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(3)), 3);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(0), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(1), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(2), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(3), 3);
    // address table
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1000)); // Group_Address -> TSAP 1
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1001)); // Group_Address -> TSAP 2
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002)); // Group_Address -> TSAP 3
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1003)); // Group_Address -> TSAP 4
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(1)), 0x1000);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(2)), 0x1001);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(3)), 0x1002);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(4)), 0x1003);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1000)), 1);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1001)), 2);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1002)), 3);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1003)), 4);
    // association table
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(2), KNX::ASAP_Group(1)); // TSAP 2 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(3), KNX::ASAP_Group(2)); // TSAP 3 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(4), KNX::ASAP_Group(3)); // TSAP 4 -> ASAP
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(1)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(2)), 3);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(3)), 4);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(1)), 0);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(2)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(3)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(4)), 3);

    // Preparation: reset object data and flags

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1002 E2 00 80 DF :set Configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1003 E2 00 80 00 :clear data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x00, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // IN BC AFFE 1003 E2 00 80 01 :set object to value other than default
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x01, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // BDUT receives telegram

    // IN BC AFFE 1000 E1 00 80 :==> Value Write sent by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: Comm. flags are set accordingly

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 48 :Comm.-flags = update flag, BIM M112: Update flag = bit 4
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x48, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 00 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x00 to 0xE1, 0x00, 0x40
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Check function of Configuration flags

    // Disable "communication"
    // Acceptance ==> Update flag not set.

    // IN BC AFFE 1002 E2 00 80 DB :disable comm in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDB, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 81 :==> Value Write sent by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 40 :Comm.-flags = update flag not set
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 00 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x00 to 0xE1, 0x00, 0x40
    ASSERT_EQ(*log++, tp1_data);
    // NO L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "read"
    // Acceptance ==> Update flag set.

    // IN BC AFFE 1002 E2 00 80 D7 :disable read in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xD7, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 81 :==> Value Write sent by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 48 :Comm.-flags = update flag, BIM M112: Update flag = bit 4
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x48, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 01 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x01 to 0xE1, 0x00, 0x41
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "write"
    // Acceptance ==> Update flag not set.

    // IN BC AFFE 1002 E2 00 80 CF :disable write in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xCF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 80 :==> Value Write sent by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 40 :Comm.-flags = update flag not set
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 01 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x01 to 0xE2, 0x00, 0x41
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "transmission"
    // Acceptance ==> Update flag set.

    // IN BC AFFE 1002 E2 00 80 9F :disable transmission in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x9F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 80 :==> Value Write from EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 48 :Comm.-flags = update flag, BIM M112: Update flag = bit 4
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x48, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 00 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x00 to 0xE1, 0x00, 0x40
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "read response update"
    // Acceptance ==> Update flag set.

    // IN BC AFFE 1002 E2 00 80 5F :disable read response update
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x5F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 81 :==> Value Write sent by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 48 :Comm.-flags = update flag, BIM M112: Update flag = bit 4
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x48, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 01 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x01 to 0xE2, 0x00, 0x41
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. Flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // BDUT receives invalid data length (optional)
    // Purpose of the test is to check whether the group objects implemented in BDUT reject a value
    // write/response addressed to them, of which the indicated info length does not match their own supported
    // field types. E.g. group object with value field type 3 bytes receives on its attributed group address a value
    // write with info length 5. This does not apply to frames with value field type less than 7 bits, as these all
    // have the same info length and can therefore not be distinguished by the addressed group object.
    // For this particular test, GO0 and GO3 shall have the value field type of BYTE3 instead of UINT1:
    // Preparation
    group_object_0_value->resize(3);
    group_object_3_value->resize(3);

    // IN BC AFFE 1002 E2 00 80 DF :set Configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1003 E4 00 80 00 00 00 :clear object data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE4, 0x00, 0x80, 0x00, 0x00, 0x00, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // IN BC AFFE 1003 E4 00 80 CC CC CC :set object to value other than default value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE4, 0x00, 0x80, 0xCC, 0xCC, 0xCC, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // Test

    // IN BC AFFE 1000 E5 00 80 FF FF FF FF :set object to value larger than size of group object
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE5, 0x00, 0x80, 0xFF, 0xFF, 0xFF, 0xFF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : the object value is not updated

    // IN BC AFFE 1003 E1 00 00 :send Value Read to group object
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E4 00 40 CC CC CC :value of group object not updated
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE4, 0x00, 0x40, 0xCC, 0xCC, 0xCC, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1000 E3 00 80 FF FF :set object to value smaller than size of group object
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE3, 0x00, 0x80, 0xFF, 0xFF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : the object value is not updated

    // IN BC AFFE 1003 E1 00 00 :send Value Read to group object
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E4 00 40 CC CC CC :value of group object not updated
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE4, 0x00, 0x40, 0xCC, 0xCC, 0xCC, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // @todo The tests shall be performed for all supported value field types.
}

/**
 * BDUT receives A_GroupValue_Response
 *
 * @ingroup KNX_08_03_07_01_04_01_05
 */
TEST(Application_Layer_Tests, Test_1_4_1_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION */
    // group objects
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.application_interface_layer.group_object_server.group_object_table;
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1); // 1 bit
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(1); // 4 bit -> communication flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_2_value = std::make_shared<std::vector<uint8_t>>(1); // -> configuration flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_3_value = std::make_shared<std::vector<uint8_t>>(1); // -> GO0 value without modification of config/comm flags
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].type = KNX::Group_Object_Type::Unsigned_Integer_4;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].data = group_object_2_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].type = KNX::Group_Object_Type::Octet_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].data = group_object_3_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].type = KNX::Group_Object_Type::Octet_1;
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0)), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(1)), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(2)), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(3)), 3);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(0), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(1), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(2), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(3), 3);
    // address table
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1000)); // Group_Address -> TSAP 1
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1001)); // Group_Address -> TSAP 2
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002)); // Group_Address -> TSAP 3
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1003)); // Group_Address -> TSAP 4
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(1)), 0x1000);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(2)), 0x1001);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(3)), 0x1002);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(4)), 0x1003);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1000)), 1);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1001)), 2);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1002)), 3);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1003)), 4);
    // association table
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(2), KNX::ASAP_Group(1)); // TSAP 2 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(3), KNX::ASAP_Group(2)); // TSAP 3 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(4), KNX::ASAP_Group(3)); // TSAP 4 -> ASAP
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(1)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(2)), 3);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(3)), 4);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(1)), 0);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(2)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(3)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(4)), 3);

    // Preparation: reset object data and flags

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // IN BC AFFE 1002 E2 00 80 DF :set Configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1003 E2 00 80 00 :clear data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x00, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // IN BC AFFE 1003 E2 00 80 01 :set object to value other than default
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE2, 0x00, 0x80, 0x01, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // Disable "communication"
    // Acceptance ==> Update flag not set.

    // IN BC AFFE 1002 E2 00 80 DB :disable comm in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDB, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 40 :==> ValueResponse by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 40 :Comm.-flags = update flag not set
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 01 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x01 to 0xE1, 0x00, 0x41, 0x00
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "read"
    // Acceptance ==> Update flag set.

    // IN BC AFFE 1002 E2 00 80 D7 :disable read in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xD7, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 40 :==> ValueResponse by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 48 :Comm.-flags = update flag, BIM M112: Update flag = bit 4
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x48, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 00 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x00 to 0xE1, 0x00, 0x40
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "write"
    // Acceptance ==> Update flag not set.

    // IN BC AFFE 1002 E2 00 80 CF :disable write in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xCF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 41 :==> ValueResponse by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 40 :Comm.-flags = update flag not set
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 00 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response // @note Changed from 0xE2, 0x00, 0x40, 0x00 to 0xE1, 0x00, 0x40
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "transmission"
    // Acceptance ==> Update flag set.

    // IN BC AFFE 1002 E2 00 80 9F :disable transmission in configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x9F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 41 :==> ValueResponse by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 48 :Comm.-flags = update flag, BIM M112: Update flag = bit 4
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x48, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1003 E2 00 40 01 :Value Response of BDUT
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x01 to 0xE1, 0x00, 0x41
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];

    // Disable "read response update" (if possible)
    // Acceptance ==> Update flag not set

    // IN BC AFFE 1002 E2 00 80 5F :disable read response update
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0x5F, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1000 E1 00 40 :==> ValueResponse by EITT to BDUT
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC AFFE 1001 E1 00 00 :read communication-flags
    *group_object_1_value = {group_object_table->communication_flags[KNX::ASAP_Group(0)]};
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 1001 E1 00 40: Update flag not set (BCU2), update flag set (BCU1)
    //tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x41, 0 }; // A_GroupValue_Response (BCU1)
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x01, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response (BCU2)
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1003 E1 00 00 :Value read of object value
    *group_object_3_value = *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // The group object value remains unchanged for devices supporting deactivation of the update flag and vice
    // versa. The underneath frame shows the reaction in the case where the device does not support
    // deactivation of the update flag.

    // OUT BC 1101 1003 E2 00 40 00 :Value Response of BDUT
    //tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x40, 0 }; // A_GroupValue_Response // @note changed from 0xE2, 0x00, 0x40, 0x00 to 0xE1, 0x00, 0x40
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE1, 0x00, 0x41, 0 }; // update_enable=false supported
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // IN BC AFFE 1001 E1 00 80 :clear Comm. Flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->communication_flags[KNX::ASAP_Group(0)] = (*group_object_1_value)[0];
}

/**
 * BDUT receives invalid APCI
 *
 * @ingroup KNX_08_03_07_01_04_01_06
 */
TEST(Application_Layer_Tests, Test_1_4_1_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    //std::vector<uint8_t> pei_data;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* PREPARATION */
    // group objects
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.application_interface_layer.group_object_server.group_object_table;
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1); // 1 bit
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(1); // 4 bit -> communication flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_2_value = std::make_shared<std::vector<uint8_t>>(1); // -> configuration flags of GO0
    std::shared_ptr<std::vector<uint8_t>> group_object_3_value = std::make_shared<std::vector<uint8_t>>(1); // -> GO0 value without modification of config/comm flags
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].type = KNX::Group_Object_Type::Unsigned_Integer_4;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].data = group_object_2_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].type = KNX::Group_Object_Type::Octet_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].data = group_object_3_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].type = KNX::Group_Object_Type::Octet_1;
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0)), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(1)), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(2)), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(3)), 3);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(0), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(1), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(2), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(3), 3);
    // address table
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1000)); // Group_Address -> TSAP 1
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1001)); // Group_Address -> TSAP 2
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1002)); // Group_Address -> TSAP 3
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x1003)); // Group_Address -> TSAP 4
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(1)), 0x1000);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(2)), 0x1001);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(3)), 0x1002);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(4)), 0x1003);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1000)), 1);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1001)), 2);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1002)), 3);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x1003)), 4);
    // association table
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0)); // TSAP 1 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(2), KNX::ASAP_Group(1)); // TSAP 2 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(3), KNX::ASAP_Group(2)); // TSAP 3 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(4), KNX::ASAP_Group(3)); // TSAP 4 -> ASAP
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(1)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(2)), 3);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(3)), 4);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(1)), 0);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(2)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(3)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(4)), 3);

    // For this particular test, GO0 and GO3 shall have the value field type of BYTE3 instead of UINT1:
    group_object_0_value->resize(3);
    group_object_3_value->resize(3);

    // Preparation

    // IN BC AFFE 1002 E2 00 80 DF :set Configuration flags
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x02, 0xE2, 0x00, 0x80, 0xDF, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config = (*group_object_2_value)[0];

    // IN BC AFFE 1003 E4 00 80 00 00 00 :clear object data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE4, 0x00, 0x80, 0x00, 0x00, 0x00, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // IN BC AFFE 1003 E4 00 80 CC CC CC :set object to value other than default value
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE4, 0x00, 0x80, 0xCC, 0xCC, 0xCC, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // Test 1 (optional) – Checking acceptance of Value Read with values higher than 00

    // IN BC AFFE 1000 E1 00 3F :send Value Read with higher values than 00
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE1, 0x00, 0x3F, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : no value response may be sent

    return; // @todo following is not covered in TP1 layer yet.

    // Test 2 – Checking acceptance of frames with unsupported APCI’s or APCI’s not valid for group
    // communication

    // IN BC AFFE 1000 E4 0x xx FF FF FF : send frame with invalid APCI (where ‘x xx’ is APCI other
    // than Value Read, Value Write or Value Response)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0xE4, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : no value response may be sent and value of object is not updated

    // IN BC AFFE 1003 E1 00 00 :send Value Read to group object
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    *group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = *group_object_3_value;

    // OUT BC 1101 1003 E4 00 40 CC CC CC :value of group object not updated
    tp1_data = { 0xBC, 0x11, 0x01, 0x10, 0x03, 0xE4, 0x00, 0x40, 0xCC, 0xCC, 0xCC, 0 }; // A_GroupValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 1.6 System Interface Objects */

/* 1.6.3 Device Object */

/**
 * Device Object - PID_OBJECT_TYPE (1)
 *
 * @ingroup KNX_08_03_07_01_06_03_01
 */
TEST(Application_Layer_Tests, Test_1_6_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=1; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x01, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=1; PropIndex=0; property data type=PDT_Unsigned_Int; max. nr. of elements=1; write access=disabled; read access=enabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=1; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=1; Count =1; Start=001; Property Data=0=Object Type ID for "Device Object")
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x01, 0x10, 0x01, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_OBJECT_NAME (2)
 *
 * @ingroup KNX_08_03_07_01_06_03_02
 */
TEST(Application_Layer_Tests, Test_1_6_3_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=2; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x02, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=2; PropIndex=0; property data type=PDT_Unsigned_Char[]; max. nr. of elements=10; write access=disabled; read access=enabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x02, 0x00, 0x02, 0x00, 0x0A, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli:send A_PropertyValue_Read to BDUT to read number of valid array elements
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=2; Count =1; Start=000)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x02, 0x10, 0x00, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with number of valid array elements
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=2; Count =1; Start=000; Property Data=number of valid array elements)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x02, 0x10, 0x00, 0x00, 0x0A, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT to read property value (object name)
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=2; Count =number of valid array elements; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x02, 0xA0, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=2; Count =number of valid array elements; Start=001; Property Data=object name)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6F, 0x03, 0xD6, 0x00, 0x02, 0xA0, 0x01, 'D', 'e', 'v', 'i', 'c', 'e', ' ', 'O', 'b', 'j', 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_FIRMWARE_REVISION (9)
 *
 * @ingroup KNX_08_03_07_01_06_03_03
 */
TEST(Application_Layer_Tests, Test_1_6_3_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=9d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x09, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=9d; PropIndex=0; property data type=PDT_Unsigned_Char; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The firmware revision property always has read-only access. Its value is fixed.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x09, 0x00, 0x02, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=9d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x09, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=9d; Count =1; Start=001; Property Data=BDUT´s firmware revision)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x09, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_SERIAL_NUMBER (11)
 *
 * @ingroup KNX_08_03_07_01_06_03_04
 */
TEST(Application_Layer_Tests, Test_1_6_3_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=11d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x0B, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=11d; PropIndex=0; property data type=PDT_Generic_06; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The serial number property always has read-only access. Its value is fixed.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x0B, 0x00, 0x16, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=11d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x0B, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=11d; Count =1; Start=001; Property Data=BDUT´s Serial Number)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6B, 0x03, 0xD6, 0x00, 0x0B, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Check received data with the SN information from manufacturer, which is accompanied
    // with BDUT, e.g. printed on BDUT.
}

/**
 * Device Object - PID_MANUFACTURER_ID (12)
 *
 * @ingroup KNX_08_03_07_01_06_03_05
 */
TEST(Application_Layer_Tests, Test_1_6_3_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=12d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x0C, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=12d; PropIndex=0; property data type=PDT_Unsigned_Int; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The manufacturer ID property always has read-only access.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x0C, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=12d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x0C, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=12d; Count =1; Start=001; Property Data=KNX member code of BDUT´s manufacturer)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x0C, 0x10, 0x01, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_DEVICE_CONTROL (14)
 *
 * @ingroup KNX_08_03_07_01_06_03_06
 */
TEST(Application_Layer_Tests, Test_1_6_3_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=14d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x0E, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=14d; PropIndex=0; property data type=PDT_Generic_01; max. nr. of elements=1; write access=enabled; read access=enabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x0E, 0x00, 0xB3, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response // @note PDT_GENERIC_01 changed to PDT_BITSET8
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=14d; Count =1; Start=001)
    // Note: Current devices with implementation independent resources support only bit 1 (frame
    // received with own IA as source address) of service information. All other bits are set to 0.
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x0E, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=14d; Count =1; Start=001; Property Data=00h)
    // Note: initial value after start-up = 0
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x0E, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Use a telegram generator to send a (valid) frame to BDUT with BDUT´s IA as source address:
    // Test frame (IN): Send A_PropertyValue_Read to BDUT:
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=14d; Count =1; Start=001)
    tp1_data = { 0xBC, 0x11, 0x01, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x0E, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @note BDUT sends A_Service_Information here to indicate individual address duplication
    tp1_data = { 0xB0, 0x11, 0x01, 0x00, 0x00, 0xE4, 0x03, 0xDF, 0x02, 0x00, 0x00, 0 }; // A_ServiceInformation_Indication
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=14d; Count =1; Start=001; Property Data=0000’0010b)
    tp1_data = { 0xBC, 0x11, 0x01, 0x11, 0x01, 0x66, 0x03, 0xD6, 0x00, 0x0E, 0x10, 0x01, 0x02, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Stimuli: Reset value of service information:
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=14d; Count =1; Start=001; data = 00h)
    tp1_data = { 0xBC, 0x11, 0x01, 0x11, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x0E, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=14d; Count =1; Start=001; Property Data=00h)
    tp1_data = { 0xBC, 0x11, 0x01, 0x11, 0x01, 0x66, 0x03, 0xD6, 0x00, 0x0E, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack
}

/**
 * Device Object - PID_ORDER_INFO (15)
 *
 * @ingroup KNX_08_03_07_01_06_03_07
 */
TEST(Application_Layer_Tests, Test_1_6_3_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=15d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x0F, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=15; PropIndex=0; property data type=PDT_Generic_10; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The order information property always has read-only access. Its value is fixed.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x0F, 0x00, 0x1A, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=15d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x0F, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=15d; Count =1; Start=001; Property Data=BDUT´s Order Info)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6F, 0x03, 0xD6, 0x00, 0x0F, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_VERSION (25)
 *
 * @ingroup KNX_08_03_07_01_06_03_08
 */
TEST(Application_Layer_Tests, Test_1_6_3_8)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=25d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x19, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=25d; PropIndex=0; property data type=PDT_Unsigned_Int; max. nr. of elements=1; write access=disabled; read access=enabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x19, 0x00, 0x30, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response // @note PDT_UNSIGNED_INT changed to PDT_VERSION
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=25d; Count =1; Start=001)
    // Note: The object version property always has read-only access.
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x19, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=25d; Count =1;
    // Start=001; Property Data=version as declared by BDUT´s manufacturer)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x19, 0x10, 0x01, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_ROUTING_COUNT (51)
 *
 * @ingroup KNX_08_03_07_01_06_03_09
 */
TEST(Application_Layer_Tests, Test_1_6_3_9)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Purpose: Check if BDUT sends property description with correct data
    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=51d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x33, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=51d; PropIndex=0; property data type=PDT_Unsigned_Char; max. nr. of elements=1; write access=enabled; read access=enabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x33, 0x00, 0x82, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=51d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x33, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=51d; Count =1; Start=001; Property Data=06h)
    // Note: default value is 6 according the KNX System Specification
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x33, 0x10, 0x01, 0x60, 0 }; // A_PropertyValue_Response // @note Routing Count changed from 0x06 to 0x60.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Set the default routing count to an other value (07h):
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=51d; Count =1; Start=001; data = 07h)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x33, 0x10, 0x01, 0x70, 0 }; // A_PropertyValue_Write // @note Routing Count changed from 0x07 to 0x70.
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=51d; Count =1; Start=001; Property Data=00h)
    // Remark: BDUT sends with new hop count in NPCI-field.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x76, 0x03, 0xD6, 0x00, 0x33, 0x10, 0x01, 0x70, 0 }; // A_PropertyValue_Response // @note Routing Count changed from 0x07 to 0x70.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Stimulate BDUT to send a frame to check for its hop count:
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=51d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x33, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=51d; Count =1; Start=001; Property Data=07h)
    // Note: hop count in the frame’s hop count field now must be set to 7
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x76, 0x03, 0xD6, 0x00, 0x33, 0x10, 0x01, 0x70, 0 }; // A_PropertyValue_Response // @note Routing Count changed from 0x07 to 0x70.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Set the routing count to another value (00h):
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=51d; Count =1; Start=001; data = 00h)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x33, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=51d; Count =1; Start=001; Property Data=00h)
    // Remark: BDUT sends with new hop count in NPCI-field.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x06, 0x03, 0xD6, 0x00, 0x33, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Stimulate BDUT to send a frame to check for its hop count:
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=51d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x33, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=51d; Count =1; Start=001; Property Data=00h)
    // Note: hop count in the frame’s hop count field now must be set to 0
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x06, 0x03, 0xD6, 0x00, 0x33, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Reset default routing count to value (06h):
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=51d; Count =1; Start=001; data = 06h)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x33, 0x10, 0x01, 0x60, 0 }; // A_PropertyValue_Write // @note Routing Count changed from 0x06 to 0x60.
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=51d; Count =1; Start=001; Property Data=06h)
    // Remark: BDUT sends with new hop count in NPCI-field.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x33, 0x10, 0x01, 0x60, 0 }; // A_PropertyValue_Response // @note Routing Count changed from 0x06 to 0x60.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_MAX_RETRY_COUNT (52)
 *
 * @ingroup KNX_08_03_07_01_06_03_10
 */
TEST(Application_Layer_Tests, Test_1_6_3_10)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Purpose: Check if BDUT sends property description with correct data
    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=52d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x34, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=52d; PropIndex=0; property data type=PDT_Unsigned_Char; max. nr. of elements=1; write access=enabled; read access=enabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x34, 0x00, 0x91, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response // @note PDT_UNSIGNED_CHAR changed to PDT_GENERIC_01
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Note: Default Value is 33h (3 for Busy retransmit limit, 3 for Nack retransmit limit)
    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=52d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x34, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=52d; Count =1; Start=001; Property Data=33h)
    // Note: default value is 33h according the KNX System Specification
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x34, 0x10, 0x01, 0x33, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Optional: Check BDUT´s behaviour (number of repetitions) with test according to
    // Volume 9 Part 3 "Basic and System Components – Couplers".

    // Stimuli: Set the Max_Retry_Count to another value (12h):
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=52d; Count =1; Start=001; data = 12h)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x34, 0x10, 0x01, 0x12, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=52d; Count =1; Start=001; Property Data=12h)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x34, 0x10, 0x01, 0x12, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Optional: Check BDUT´s behaviour (number of repetitions) with test according to
    // Volume 9 Part 3 "Basic and System Components – Couplers".

    // Stimuli: Set the Max_Retry_Count to another value (01h):
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=52d; Count =1; Start=001; data = 01h)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x34, 0x10, 0x01, 0x01, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=52d; Count =1; Start=001; Property Data=01h)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x34, 0x10, 0x01, 0x01, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Optional: Check BDUT´s behaviour (number of repetitions) with test according to
    // Volume 9 Part 3 "Basic and System Components – Couplers".

    // Stimuli: Reset the Max_Retry_Count to the default value (33h):
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=52d; Count =1; Start=001; data = 33h)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x34, 0x10, 0x01, 0x33, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=52d; Count =1; Start=001; Property Data=33h)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x34, 0x10, 0x01, 0x33, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_PROGMODE (54)
 *
 * @ingroup KNX_08_03_07_01_06_03_11
 */
TEST(Application_Layer_Tests, Test_1_6_3_11)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=54d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x36, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=54d; PropIndex=0; property data type=PDT_Unsigned_Char; max. nr. of elements=1; write access=enabled; read access=enabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x36, 0x00, 0xB3, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response // @note PDT_UNSIGNED_CHAR changed to PDT_BITSET8
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_PRODUCT_ID (55)
 *
 * @ingroup KNX_08_03_07_01_06_03_12
 */
TEST(Application_Layer_Tests, Test_1_6_3_12)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=55d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x37, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=55d; PropIndex=0; property data type=PDT_Generic_10; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The Product Information property always has read-only access.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x37, 0x00, 0x1A, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=55d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x37, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=55d; Count =1; Start=001; Property Data=product information as declared by BDUT´s manufacturer)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6F, 0x03, 0xD6, 0x00, 0x37, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_MAX_APDULENGTH (56)
 *
 * @ingroup KNX_08_03_07_01_06_03_13
 */
TEST(Application_Layer_Tests, Test_1_6_3_13)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=56d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x38, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=56d; PropIndex=0; property data type=PDT_Unsigned_Int; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The Max_APDU_Length property is mandatory for all BDUT with Max APDU-Length≠15. It always has read-only access.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x38, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=56d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x38, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=56d; Count =1; Start=001; Property Data=Max. APDU-Length)
    // Note: TP-Uart devices have a max APDU-Length of 55 (octets).
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x38, 0x10, 0x01, 0x00, 0x37, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_SUBNET_ADDRESS (57)
 *
 * @ingroup KNX_08_03_07_01_06_03_14
 */
TEST(Application_Layer_Tests, Test_1_6_3_14)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    bdut.interface_objects->device()->subnetwork_address()->subnetwork_address = 0x11;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=57d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x39, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=57d; PropIndex=0; property data type=PDT_Unsigned_Char; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The SNA property always has read-only access.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x39, 0x00, 0x02, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=57d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x39, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=57d; Count =1; Start=001; Property Data=BDUT´s SNA)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x39, 0x10, 0x01, 0x11, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_DEVICE_ADDRESS (58)
 *
 * @ingroup KNX_08_03_07_01_06_03_15
 */
TEST(Application_Layer_Tests, Test_1_6_3_15)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    bdut.interface_objects->device()->device_address()->device_address = 0x01;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=58d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x3A, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=58d; PropIndex=0; property data type=PDT_Unsigned_Char; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The device address property always has read-only access.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x3A, 0x00, 0x02, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=58d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x3A, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=58d; Count =1; Start=001; Property Data=BDUT´s device address)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x3A, 0x10, 0x01, 0x01, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_DPSU_TYPE (67)
 *
 * @ingroup KNX_08_03_07_01_06_03_16
 */
TEST(Application_Layer_Tests, Test_1_6_3_16)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=67d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x43, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=67d; PropIndex=0; property data type=PDT_Unsigned_Int; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The Distributed Power Supply Type property always has read-only access.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x43, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=67d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x43, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=67d; Count =1; Start=001; Property Data= DPSU-Type as declared by BDUT´s manufacturer)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x43, 0x10, 0x01, 0x00, 0x0A, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object - PID_DPSU_STATUS (68)
 *
 * @ingroup KNX_08_03_07_01_06_03_17
 */
TEST(Application_Layer_Tests, Test_1_6_3_17)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=68d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x44, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=68d; PropIndex=0; property data type=PDT_Binary_Information; max. nr. of elements=1; write access=disabled; read access=enabled)
    // Note: The Distributed Power Supply Status property always has read-only access.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x44, 0x00, 0x32, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=68d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x44, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=68d; Count =1; Start=001; Property Data= current DPSU-Status)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x44, 0x10, 0x01, 0x01, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Change BDUT’s DPSU status by means of local HMI or by sending a value to
    // property 69 (PID_DPSU_ENABLE, 1.6.3.18)
    bdut.interface_objects->device()->psu_status()->psu_status.b = false;
    // Then send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=68d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x44, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=68d; Count =1; Start=001; Property Data= changed DPSU-Status)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x44, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Change back BDUT’s DPSU status to its initial value by means of local HMI or
    // by sending the corresponding value to property 69 (PID_DPSU_ENABLE, 1.6.3.18).
    bdut.interface_objects->device()->psu_status()->psu_status.b = true;
}

/**
 * Device Object - PID_DPSU_ENABLE (69)
 *
 * @ingroup KNX_08_03_07_01_06_03_18
 */
TEST(Application_Layer_Tests, Test_1_6_3_18)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=69d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x45, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=69d; PropIndex=0; property data type=PDT_Enum8; max. nr. of elements=1; write access=enabled; read access=enabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x45, 0x00, 0xB5, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=69d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x45, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=69d; Count =1; Start=001; Property Data= current DPSU_Enable value)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x45, 0x10, 0x01, 0x02, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Change BDUT’s DPSU_Enable value by sending A_PropertyValue_Write
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=69d; Count =1; Start=001, data=other than default/ex-factory value)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x45, 0x10, 0x01, 0x01, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=69d; Count =1; Start=001; Property Data= changed DPSU_Enable value)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x45, 0x10, 0x01, 0x01, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Change back BDUT’s DPSU_Enable to its initial value
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=69d; Count =1; Start=001, data= default value)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x45, 0x10, 0x01, 0x02, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x45, 0x10, 0x01, 0x02, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 1.6.4 Address Table Object */

/**
 * Address Table Object - PID_OBJECT_TYPE (1)
 *
 * @ingroup KNX_08_03_07_01_06_04_01
 */
TEST(Application_Layer_Tests, Test_1_6_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The test sequence as described in clause 1.6.3.1 applies, with adapted local Object Index and Object Type
    // ID=1 (Object Type ID for "Address Table Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x01, 0x01, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x01, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x01, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x01, 0x01, 0x10, 0x01, 0x00, 0x01, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Address Table Object - PID_OBJECT_NAME (2)
 *
 * @ingroup KNX_08_03_07_01_06_04_02
 */
TEST(Application_Layer_Tests, Test_1_6_4_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The test sequence as described in clause 1.6.3.2 applies, with adapted local Object Index and Object Type
    // ID=1 (Object Type ID for "Address Table Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x01, 0x02, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x01, 0x02, 0x00, 0x02, 0x00, 0x04, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x01, 0x02, 0x10, 0x00, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x01, 0x02, 0x10, 0x00, 0x00, 0x04, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x01, 0x02, 0x40, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x69, 0x03, 0xD6, 0x01, 0x02, 0x40, 0x01, 'G', 'r', 'A', 'T', 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Address Table Object - PID_LOAD_STATE_CONTROL (5)
 *
 * @ingroup KNX_08_03_07_01_06_04_03
 */
TEST(Application_Layer_Tests, Test_1_6_4_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The load control property shall be tested according the test sequences described in clause 4.2
    // (management server testing of Load State Machines).
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x01, 0x05, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x01, 0x05, 0x00, 0x80, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x01, 0x05, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x01, 0x05, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Address Table Object - PID_TABLE (23)
 *
 * @ingroup KNX_08_03_07_01_06_04_04
 */
TEST(Application_Layer_Tests, Test_1_6_4_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Writing (and reading) of the address table (Standard Mode Group Address Table) shall be tested
    // according the test sequences described in clause 4.2 (management server testing of Load State Machines).
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x01, 0x17, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x01, 0x17, 0x00, 0x10, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @note value is implementation specific

    //    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x01, 0x17, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    //    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    //    io_context.poll();
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    //    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x01, 0x17, 0x10, 0x01, 0x00, 0x02, 0 }; // A_PropertyValue_Response
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);
}

/* 1.6.5 Association Table Object */

/**
 * Association Table Object - PID_OBJECT_TYPE (1)
 *
 * @ingroup KNX_08_03_07_01_06_05_01
 */
TEST(Application_Layer_Tests, Test_1_6_5_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The test sequence as described in clause 1.6.3.1 applies, with adapted local Object Index and Object Type
    // ID=2 (Object Type ID for "Association Table Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x02, 0x01, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x02, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x02, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x02, 0x01, 0x10, 0x01, 0x00, 0x02, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Association Table Object - PID_OBJECT_NAME (2)
 *
 * @ingroup KNX_08_03_07_01_06_05_02
 */
TEST(Application_Layer_Tests, Test_1_6_5_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The test sequence as described in clause 1.6.3.2 applies, with adapted local Object Index and Object Type
    // ID=2 (Object Type ID for "Association Table Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x02, 0x02, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x02, 0x02, 0x00, 0x02, 0x00, 0x05, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x02, 0x02, 0x10, 0x00, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x02, 0x02, 0x10, 0x00, 0x00, 0x05, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x02, 0x02, 0x50, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6A, 0x03, 0xD6, 0x02, 0x02, 0x50, 0x01, 'G', 'r', 'O', 'A', 'T', 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Association Table Object - PID_LOAD_STATE_CONTROL (5)
 *
 * @ingroup KNX_08_03_07_01_06_05_03
 */
TEST(Application_Layer_Tests, Test_1_6_5_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The load control property shall be tested according the test sequences described in clause 4.2
    // (management server testing of Load State Machines).
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x02, 0x05, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x02, 0x05, 0x00, 0x80, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x02, 0x05, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Association Table Object - PID_TABLE (23)
 *
 * @ingroup KNX_08_03_07_01_06_05_04
 */
TEST(Application_Layer_Tests, Test_1_6_5_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Writing (and reading) of the association table (associations for standard mode group addresses) shall be
    // tested according the test sequences described in clause 4.2 (management server testing of Load State
    // Machines).
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x02, 0x17, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x02, 0x17, 0x00, 0x10, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @note value is implementation specific

    //    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x02, 0x17, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    //    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    //    io_context.poll();
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    //    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x02, 0x17, 0x10, 0x01, 0x00, 0x02, 0 }; // A_PropertyValue_Response
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);
}

/* 1.6.6 Application Program Object */

/**
 * Application Program Object - PID_OBJECT_TYPE (1)
 *
 * @ingroup KNX_08_03_07_01_06_06_01
 */
TEST(Application_Layer_Tests, Test_1_6_6_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The test sequence as described in clause 1.6.3.1 applies, with adapted local Object Index and Object Type
    // ID=3 (Object Type ID for "Application Program Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x03, 0x01, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x03, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x03, 0x01, 0x10, 0x01, 0x00, 0x03, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Application Program Object - PID_OBJECT_NAME (2)
 *
 * @ingroup KNX_08_03_07_01_06_06_02
 */
TEST(Application_Layer_Tests, Test_1_6_6_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The test sequence as described in clause 1.6.3.2 applies, with adapted local Object Index and Object Type
    // ID=3 (Object Type ID for "Application Program Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x03, 0x02, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x03, 0x02, 0x00, 0x02, 0x00, 0x07, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x02, 0x10, 0x00, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x03, 0x02, 0x10, 0x00, 0x00, 0x07, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x02, 0x70, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6C, 0x03, 0xD6, 0x03, 0x02, 0x70, 0x01, 'A', 'p', 'p', 'l', 'P', 'r', 'g', 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Application Program Object - PID_LOAD_STATE_CONTROL (5)
 *
 * @ingroup KNX_08_03_07_01_06_06_03
 */
TEST(Application_Layer_Tests, Test_1_6_6_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The load control property shall be tested according the test sequences described in clause 4.2
    // (management server testing of Load State Machines).
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x03, 0x05, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x03, 0x05, 0x00, 0x80, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x05, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Application Program Object - PID_RUN_CONTROL (6)
 *
 * @ingroup KNX_08_03_07_01_06_06_04
 */
TEST(Application_Layer_Tests, Test_1_6_6_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The run control property shall be tested according the test sequences described in clause 4.3 (management
    // server testing of Run State Machine).
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x03, 0x06, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x03, 0x06, 0x00, 0x80, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Application Program Object - PID_APPLICATION_VERSION (13)
 *
 * @ingroup KNX_08_03_07_01_06_06_05
 */
TEST(Application_Layer_Tests, Test_1_6_6_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=Index of Application Program Object;
    // PropID=13d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x03, 0x0D, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex= Index of Application Program Object;
    // PropID=13d; PropIndex=0; property data type=PDT_Generic_05; max. nr. of elements=1; write access;
    // read access=enabled)
    // Note: write access (yes/no) is implementation dependent. BDUT´s manufacturer shall declare in
    // the PICS/PIXIT whether the property is write-enabled or not.
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x03, 0x0D, 0x00, 0x95, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=Index of Application Program Object; PropID=13d;
    // Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x0D, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=Index of Application Program Object;
    // PropID=13d; Count =1; Start=001; Property Data=current application version information)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6A, 0x03, 0xD6, 0x03, 0x0D, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Set the application version to another (valid) value, if applicable:
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=Index of Application Program Object;
    // PropID=13d; Count =1; Start=001; data = valid value, as declared by manufacturer)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x6A, 0x03, 0xD7, 0x03, 0x0D, 0x10, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=Index of Application Program Object;
    // PropID=13d; Count =1; Start=001; Property Data=as in Write service)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6A, 0x03, 0xD6, 0x03, 0x0D, 0x10, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=Index of Application Program Object; PropID=13d;
    // Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x0D, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=Index of Application Program Object;
    // PropID=13d; Count =1; Start=001; Property Data=value as written before)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6A, 0x03, 0xD6, 0x03, 0x0D, 0x10, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Try to write an illegal application version to BDUT, if applicable
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=Index of Application Program Object;
    // PropID=13d; Count =1; Start=001; data = ID for an invalid "application version"
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x6A, 0x03, 0xD7, 0x03, 0x0D, 0x10, 0x01, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT shall reject the write request, i.e. sends a negative A_PropertyValue_Response
    // (with no data)
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=Index of Application Program Object;
    // PropID=13d; Count =0; Start=001; no Data)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x03, 0x0D, 0x00, 0x01, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Optional: Check BDUT´s behaviour (number of repetitions) with test according to Volume 9
    // Part 3 "Basic and System Components – Couplers".

    // Stimuli: Reset the application version to the default value as declared by BDUT´s manufacturer
    // Test frame (IN): A_PropertyValue_Write (ObjIndex=Index of Application Program Object;
    // PropID=13d; Count =1; Start=001; data = ID for the default application version)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x6A, 0x03, 0xD7, 0x03, 0x0D, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=Index of Application Program Object;
    // PropID=13d; Count =1; Start=001; Property Data=ID for the default application version)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6A, 0x03, 0xD6, 0x03, 0x0D, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 1.6.7 Group Object Table Object */

/**
 * Group Object Table Object - PID_OBJECT_TYPE (1)
 *
 * @ingroup KNX_08_03_07_01_06_07_01
 */
TEST(Application_Layer_Tests, Test_1_6_7_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The test sequence as described in clause 1.6.3.1 applies, with adapted local Object Index and Object Type
    // ID=9 (Object Type ID for "Group Object Table Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x04, 0x01, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x04, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x04, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x04, 0x01, 0x10, 0x01, 0x00, 0x09, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Group Object Table Object - PID_OBJECT_NAME (2)
 *
 * @ingroup KNX_08_03_07_01_06_07_02
 */
TEST(Application_Layer_Tests, Test_1_6_7_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The test sequence as described in clause 1.6.3.2 applies, with adapted local Object Index and Object Type
    // ID=9 (Object Type ID for "Group Object Table Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x04, 0x02, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x04, 0x02, 0x00, 0x02, 0x00, 0x06, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x04, 0x02, 0x10, 0x00, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x04, 0x02, 0x10, 0x00, 0x00, 0x06, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x04, 0x02, 0x60, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6B, 0x03, 0xD6, 0x04, 0x02, 0x60, 0x01, 'G', 'r', 'O', 'b', 'j', 'T', 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Group Object Table Object - PID_LOAD_STATE_CONTROL (5)
 *
 * @ingroup KNX_08_03_07_01_06_07_03
 */
TEST(Application_Layer_Tests, Test_1_6_7_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The load control property shall be tested according the test sequences described in clause 4.2
    // (management server testing of Load State Machines).
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x04, 0x05, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x04, 0x05, 0x00, 0x80, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x04, 0x05, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x04, 0x05, 0x10, 0x01, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Group Object Table Object - PID_GRPOBJTABLE (51)
 *
 * @ingroup KNX_08_03_07_01_06_07_04
 */
TEST(Application_Layer_Tests, Test_1_6_7_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Writing (and reading) of the group object table shall be tested according the tests described in clause 4.2
    // (management server testing of Load State Machines).
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x04, 0x33, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x04, 0x33, 0x00, 0x16, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x04, 0x33, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6B, 0x03, 0xD6, 0x04, 0x33, 0x10, 0x01, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Group Object Table Object - PID_EXT_GRPOBJREFERENCE (52)
 *
 * @ingroup KNX_08_03_07_01_06_07_05
 */
TEST(Application_Layer_Tests, Test_1_6_7_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Writing (and reading) of the (extended) group object reference table shall be tested according the tests
    // described in clause 4.2 (management server testing of Load State Machines).
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x04, 0x34, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x04, 0x34, 0x00, 0x18, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x04, 0x34, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6D, 0x03, 0xD6, 0x04, 0x34, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 2 Network Management Server Tests */

/**
 * 2.3 Testing of A_IndividualAddress_Read-Service - Server Test
 *
 * @ingroup KNX_08_03_07_02_03
 */
TEST(Application_Layer_Tests, Test_2_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Prior to starting the test, set individual address of the BDUT to a fix value (e.g. 1001H) */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* For step 1 to 2 : Switch off programming LED on BDUT */
    bdut.interface_objects->device()->programming_mode()->prog_mode = false;

    /* 2.3.1 Try to read Address with LED off */

    // IN BC 10.15.254 00/0000 E1 01 00 :A_IndividualAddress_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: no response may be sent
    ASSERT_EQ(log, std::cend(busmoni.log));

    /* 2.3.2 Send Response to BDUT with LED off */

    // IN BC 10.15.254 00/0000 E1 01 40 :A_IndividualAddress_Response(Addr=AFFE)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: no response may be sent
    ASSERT_EQ(log, std::cend(busmoni.log));

    // For step 3 to 4 : Switch on programming LED on BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    /* 2.3.3 Read Address with LED on */

    // IN BC 10.15.254 00/0000 E1 01 00 :A_IndividualAddress_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 00/0000 E1 01 40 :A_IndividualAddress_Response(Addr=1001)
    // Acceptance: the BDUT sends an A_IndividualAddress_Response-PDU
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.3.4 Send Response to BDUT with LED on */

    // IN BC 10.15.254 00/0000 E1 01 40 :A_IndividualAddress_Response(Addr=AFFE)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: no response may be sent
    ASSERT_EQ(log, std::cend(busmoni.log));
}

/**
 * 2.4 Testing of A_IndividualAddress_Write-Service - Server Test
 *
 * @ingroup KNX_08_03_07_02_04
 */
TEST(Application_Layer_Tests, Test_2_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Prior to starting the test, set individual address of the BDUT to a fix value (e.g. 1001H) */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* For step 1 to 2 : Switch off programming LED on BDUT */
    bdut.interface_objects->device()->programming_mode()->prog_mode = false;

    /* 2.4.1 Try to set Address with LED off */

    // IN BC 10.15.254 00/0000 E3 00 C0 12 03 :A_IndividualAddress_Write(Addr=1203)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE3, 0x00, 0xC0, 0x12, 0x03, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance:
    // No reaction of the BDUT - BDUT keeps individual address as downloaded prior to
    // starting the test. This can be checked by (switch on programming LED first !!):
    ASSERT_EQ(log, std::cend(busmoni.log));
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    // IN BC 10.15.254 00/0000 E1 01 00 :A_IndividualAddress_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 00/0000 E1 01 40 :A_IndividualAddress_Response(Addr=1001)
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // For test step 2 : Switch on programming LED on BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    /* 2.4.2 Set Address with LED on */

    // IN BC 10.15.254 00/0000 E3 00 C0 12 03 :A_IndividualAddress_Write(Addr=1203)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE3, 0x00, 0xC0, 0x12, 0x03, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance:
    //  The BDUT now has the individual address 1203H. This can be checked by:

    // IN BC 10.15.254 00/0000 E1 01 00 :A_IndividualAddress_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.02.003 00/0000 E1 01 40 :A_IndividualAddress_Response(Addr=1203)
    tp1_data = { 0xBC, 0x12, 0x03, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.5 Testing of A_DeviceDescriptor_Read-Service - Server Test
 *
 * @ingroup KNX_08_03_07_02_05
 */
TEST(Application_Layer_Tests, Test_2_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->device()->device_descriptor()->device_descriptor = 0x0300;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Prior to starting the test, set the individual address of the BDUT to a fix value (e.g. 1001H). */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* 2.5.1 Read Device Descriptor, connection-oriented */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 61 43 00 :A_DeviceDescriptor_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : The BDUT sends a telegram with an A_DeviceDescriptor_Response-PDU, containing the
    // type and version or answers with the lowest Device Descriptor it supports
    // OUT BC 01.00.001 AFFE 63 43 40 ?? ?? :A_DeviceDescriptor_Response(Type=??, Version=??)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x43, 0x40, 0x03, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.5.2 Read Device Descriptor, connectionless (when supported) */

    // IN BC 10.15.254 01.00.001 61 03 00 :A_DeviceDescriptor_Read(Type 0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x03, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : The BDUT sends a telegram with an A_DeviceDescriptor_Response-PDU, containing the
    // type and version or the lowest Device Descriptor it supports
    // OUT BC 01.00.001 AFFE 63 03 40 03 00: BDUT sends 0300 as DD Type 0 response example represents a device with ‘mask version’ 0300h
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x03, 0x40, 0x03, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.5.3 Read DD Type2, connection-oriented */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 61 43 02 :A_DeviceDescriptor_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x02, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    io_context.poll();
    // Acceptance : when The BDUT supports DD2, it shall send a telegram with an
    // A_DeviceDescriptor_Response-PDU, containing the correct DD2 information. When the BDUT does not
    // support DD2, it shall answer with the error code
    // OUT BC 01.00.001 10.15.254 6F 43 42 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? :A_DeviceDescriptor_Response(DD Type 2 – see supplement 15 Easy common parts – paragraph 1.3.3.2.2)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6F, 0x43, 0x42, 0x00, 0xFD, 0x90, 0x01, 0x10, 0x3F, 0x1F, 0xF4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.5.4 Read DD Type2 , connectionless (if supported) */

    // IN BC 10.15.254 01.00.001 61 03 02 :A_DeviceDescriptor_Read(Type 2)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x03, 0x02, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : when The BDUT supports DD2, it shall send a telegram with an
    // A_DeviceDescriptor_Response-PDU, containing the correct DD2 information. When the BDUT does not
    // support DD2, it shall answer with the error code
    // OUT BC 1101 AFFE 6F 03 42 00 FD 90 01 10 3F 1F F4 00 00 00 00 00 00: response example represents an LTE- Mode device or in case of error (DD Type 2 not supported):
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6F, 0x03, 0x42, 0x00, 0xFD, 0x90, 0x01, 0x10, 0x3F, 0x1F, 0xF4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src Addr changed form 0x1101 to 0x1001.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1101 AFFE 6F 03 7F: "DD Type = 3Fh"
    //    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6F, 0x03, 0x7F, 0 }; // @note Src Addr changed form 0x1101 to 0x1001.
    //    ASSERT_EQ(*log++, tp1_data);
    // Note: For Easy-LTE mode devices, LT_Base is fixed to 3Fh and the 1st channel code is fixed to 1FF4h
    // within DD Type 2. Please refer to KNX System Specification, Supplement B "Ctrl-Mode Tests", for
    // more details.

    /* 2.5.5 Read illegal DD Types, connection-oriented */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 61 43 0x :A_DeviceDescriptor_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : when the BDUT does not support the read DD, it shall answer with the error code
    // OUT BC 01.00.001 10.15.254 61 43 7F : A_DeviceDescriptor_Response(negative response)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x61, 0x43, 0x7F, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.5.6 Read illegal DD Types, connectionless (if supported) */

    // IN BC 10.15.254 01.00.001 61 03 0x :A_DeviceDescriptor_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x03, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : The BDUT sends a telegram with a negative A_DeviceDescriptor_Response-PDU,
    // OUT BC 01.00.001 10.15.254 61 03 7F :A_DeviceDescriptor_Response(negative response)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x61, 0x03, 0x7F, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.6 Testing of A_Memory_Read-Service - Server Test
 *
 * @ingroup KNX_08_03_07_02_06
 */
TEST(Application_Layer_Tests, Test_2_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Assumed Memory Model:
    // Address 200H to 300H: accessible memory area
    // Downloaded data from 200H onwards: 11 22 33 44 55 66 77 88 99 AA BB CC DD EE FF 11 22 and so forth
    // From address 300H onwards: protected memory area
    bdut.memory[0x200] = {
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00
    };

    /* 2.6.1 Legal Length - accessible Memory Area (10 bytes from 200H) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 63 42 0A 02 00 :A_Memory_Read(Count=0A, Addr=0200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x42, 0x0A, 0x02, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends an A_Memory_Response-PDU with the required data
    // OUT BC 01.00.001 10.15.254 6D 42 4A 02 00 11 22 33 44 55 66 77 88 99 AA :A_Memory_Response(Count=0A, Addr=0200, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6D, 0x42, 0x4A, 0x02, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.6.2 Legal Length - protected Memory Area (10 bytes from 300H) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 63 42 0A 03 00 :A_Memory_Read(Count=0A, Addr=0300)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x42, 0x0A, 0x03, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends an A_Memory_Response-PDU with length byte set to zero and no data
    // OUT BC 01.00.001 10.15.254 63 42 40 03 00 :A_Memory_Response (Count=00, Addr=0300, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x42, 0x40, 0x03, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.6.3 Legal Length - partly protected Memory Area (2 Bytes from 2FFH) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 63 42 02 02 FF :A_Memory_Read-PDU (Count=02, Addr=02FF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x42, 0x02, 0x02, 0xFF, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends an A_Memory_Response-PDU with length byte set to zero and no data
    // OUT BC 01.00.001 10.15.254 63 42 40 02 FF :A_Memory_Response-PDU (Count=00, Addr=02FF, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x42, 0x40, 0x02, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.6.4 Illegal Length - accessible Memory Area (13 bytes from 200H) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 63 42 0D 02 00 :A_Memory_Read(Count=0D, Addr=0200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x42, 0x0D, 0x02, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends an A_Memory_Response-PDU with length byte set to zero and no data
    // OUT BC 01.00.001 10.15.254 63 42 40 02 00 :A_Memory_Response-PDU (Count=00, Addr=0200, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x42, 0x40, 0x02, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.7 Testing of A_Memory_Write-Service - Server Test
 *
 * @ingroup KNX_08_03_07_02_07
 */
TEST(Application_Layer_Tests, Test_2_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Assumed Memory Model:
    // Address 200H to 300H : accessible memory area
    // Downloaded data from 200H onwards: FF EE DD CC BB AA 99 88 77 66 55 44 33 22 11 00 FF EE and so forth
    // From address 300H onwards: protected memory area
    bdut.memory[0x200] = {
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00,
        0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0x22, 0x11, 0x00
    };

    /* 2.7.1 Legal Length - accessible Memory - no Verify (10 Bytes from 200H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6D 42 8A 02 00 11 22 33 44 55 66 77 88 99 AA :A_Memory_Write(Count=0A, Addr=0200, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6D, 0x42, 0x8A, 0x02, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: After reading the written memory, the same data is returned by the BDUT as written.
    // IN BC 10.15.254 01.00.001 63 46 0A 02 00 :A_Memory_Read-PDU (Count=0A, Addr=0200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x0A, 0x02, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 6D 42 4A 02 00 11 22 33 44 55 66 77 88 99 AA :A_Memory_Response(Count=0A, Addr=0200, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6D, 0x42, 0x4A, 0x02, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.7.2 Legal length - partly protected Memory - no Verify (2 Bytes from 2FFH) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 65 42 82 02 FF 12 34 :A_Memory_Write (Count=02, Addr=02FF, Data= 12 34)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x42, 0x82, 0x02, 0xFF, 0x12, 0x34, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: after reading the affected accessible memory area, a response shall be generated showing that data has not been modified.
    // IN BC 10.15.254 01.00.001 63 46 01 02 FF :A_Memory_Read(Count=01, Addr=02FF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x01, 0x02, 0xFF, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 64 42 41 02 FF 00 :A_Memory_Response (Count=01, Addr=02FF, Data= 00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0x41, 0x02, 0xFF, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.7.3 Illegal Length - accessible Memory - no Verify (13 bytes from 0210H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6F 42 8D 02 10 FF FF FF FF FF FF FF FF FF FF FF FF :A_Memory_Write(Count=0D, Addr=0210, Data= FF FF FF FF FF FF FF FF FF FF FF FF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x42, 0x8D, 0x02, 0x10, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: after reading the affected accessible memory area, a response shall be generated showing that data has not been modified.
    // IN BC 10.15.254 01.00.001 63 46 0A 02 10 :A_Memory_Read(Count=0A, Addr=0110)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x0A, 0x02, 0x10, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 6C 42 4A 02 10 FF EE DD CC BB AA 99 88 77 66 :A_Memory_Response(Count=0A, Addr=0210, Data= FF EE DD CC BB AA 99 88 77 66)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6D, 0x42, 0x4A, 0x02, 0x10, 0xFF, 0xEE, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0 }; // @note Length changed from 0x6C to 0x6D
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.7.4 Legal Length - accessible Memory – Verify (Activation Method Manufacturer dependent, e.g. Property write - 10 Bytes from 0220H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6D 42 8A 02 20 11 22 33 44 55 66 77 88 99 AA :A_Memory_Write(Count=0A, Addr=0220, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6D, 0x42, 0x8A, 0x02, 0x20, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends an A_Memory_Write-PDU with the data written
    // OUT BC 01.00.001 10.15.254 6D 42 4A 02 20 11 22 33 44 55 66 77 88 99 AA :A_Memory_Response(Count=0A, Addr=0220, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6D, 0x42, 0x4A, 0x02, 0x20, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.7.5 Legal Length - protected Memory – Verify (10 bytes from 300H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6D 42 8A 03 00 11 22 33 44 55 66 77 88 99 AA :A_Memory_Write(Count=0A, Addr=0300, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6D, 0x42, 0x8A, 0x03, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends an A_Memory_Response with the length set to 0 and no data.
    // OUT BC 01.00.001 10.15.254 63 42 40 03 00 :A_Memory_Response (Count=00, Addr=0300, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x42, 0x40, 0x03, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.7.6 Legal Length - partly protected Memory – Verify (2 bytes from 02FF) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 65 42 82 02 FF 12 34 :A_Memory_Write(Count=02, Addr=02FF, Data= 12 34)
    tp1_data = { 0xBc, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x42, 0x82, 0x02, 0xFF, 0x12, 0x34, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : the BDUT sends an A_Memory_Response with the length set to 0 and no data
    // OUT BC 01.00.001 10.15.254 63 42 40 02 FF :A_Memory_Response (Count=00, Addr=02FF, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x42, 0x40, 0x02, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.7.7 Illegal Length - accessible Memory – Verify (13 bytes from 200H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6F 42 8D 02 00 12 34 56 78 9A BC DE F0 12 34 56 78:A_Memory_Write(Count=0D, Addr=0200, Data= 12 34 56 78 9A BC DE F0 12 34 56 78)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x42, 0x8D, 0x02, 0x00, 0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0, 0x12, 0x34, 0x56, 0x78, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends an A_Memory_Response with the length set to 0 and no data
    // OUT BC 01.00.001 10.15.254 63 42 40 02 00 :A_Memory_Response (Count=00, Addr=0200, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x42, 0x40, 0x02, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.8 Testing of A_ADC_Read-Service - Server Test
 *
 * @ingroup KNX_08_03_07_02_08
 */
TEST(Application_Layer_Tests, Test_2_8)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* 2.8.1 Correct Channel Number (Channel 0 and 1 count) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 62 41 80 01 :A_ADC_Read-PDU(Channel=0, Count=01)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x62, 0x41, 0x80, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends an A_ADC_Response-PDU with the correct data
    // OUT BC 01.00.001 10.15.254 64 41 C0 01 ?? ?? :A_ADC_Response (Channel=0, Count=01, ????)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x41, 0xC0, 0x01, 0x12, 0x34, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFe, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.8.2 Incorrect Channel Number (Channel 8 and 1 count) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 62 41 88 01 :A_ADC_Read(Channel=08, Count=01)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x62, 0x41, 0x88, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends an A_ADC_Response-PDU with the count set to zero.
    // OUT BC 01.00.001 10.15.254 64 41 C8 00 00 00 :A_ADC_Response (Channel=08, Count=00, 0000)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x41, 0xC8, 0x00, 0x00, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.9 Testing of A_Restart-Service - Server Test
 *
 * @ingroup KNX_08_03_07_02_09
 */
TEST(Application_Layer_Tests, Test_2_9)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* 2.9.1 Connection-oriented Communication Mode */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // out

    // IN BC 10.15.254 01.00.001 61 43 80 :A_Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: Compare BDUT’s reaction to what the manufacturer has declared in the supplied PIXIT forms for Management.
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 }; // T-Ack(Seq=0)
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.A_Restart_ind_count, 1);

    /* 2.9.2 Connectionless Communication Mod */

    // IN BC AFFE 1101 61 03 80 :send A_Restart to BDUT:
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x03, 0x80, 0 }; // @note Dest changed from 0x1101 to 0x1001.
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: Compare BDUT’s reaction to what the manufacturer has declared in the supplied PIXIT forms for management.
    ASSERT_EQ(bdut.A_Restart_ind_count, 2);
}

/**
 * 2.10 Testing of A_MemoryBit_Write-Service - Server Test
 *
 * @ingroup KNX_08_03_07_02_10
 */
TEST(Application_Layer_Tests, Test_2_10)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Assumed Memory Model:
    // 200H to 2FFH : accessible memory area: entire memory area filled with 0FH
    // 300H to 3FFH : protected memory area
    bdut.memory[0x200] = {
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F,
        0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F
    };

    /* 2.10.1 Legal Length - accessible Memory - no Verify (5 bytes from 200H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6E 43 D0 05 02 00 33 33 33 33 33 55 55 55 55 55 :A_MemoryBit_Write(Count=05, Addr=0200, Data= 33 33 33 33 33 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x43, 0xD0, 0x05, 0x02, 0x00, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: After reading the concerned memory area, the BDUT sends a response showing that the memory has been manipulated.
    // IN BC 10.15.254 01.00.001 63 46 05 02 00 :A_Memory_Read(Count=05, Addr=0200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x05, 0x02, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 68 42 45 02 00 56 56 56 56 56 :A_Memory_Response(Count=05, Addr=0200, Data= 56 56 56 56 56)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x68, 0x42, 0x45, 0x02, 0x00, 0x56, 0x56, 0x56, 0x56, 0x56, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.10.2 Legal Length - partly protected Memory - no Verify (2 bytes from 02FF) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 68 43 D0 02 02 FF 33 33 55 55 :A_MemoryBit_Write(Count=02, Addr=02FF, Data= 33 33 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x68, 0x43, 0xD0, 0x02, 0x02, 0xFF, 0x33, 0x33, 0x55, 0x55, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: after reading the last byte of the accessible memory area, the BDUT sends a response showing that the memory has not been manipulated.
    // IN BC 10.15.254 01.00.001 63 46 01 02 FF :A_Memory_Read(Count=01, Addr=02FF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x01, 0x02, 0xFF, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 64 42 41 02 FF 0F :A_Memory_Response(Count=01, Addr=02FF, Data= 0F)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0x41, 0x02, 0xFF, 0x0F, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.10.3 Illegal Length - accessible Memory - no Verify (6 bytes from 0210H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6F 43 D0 06 02 10 33 33 33 33 33 33 55 55 55 55 55 :A_MemoryBit_Write(Count=06, Addr=0210, Data= 33 33 33 33 33 33 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD0, 0x06, 0x02, 0x10, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: after reading the concerned memory area, the BDUT sends a response showing that the memory has not been manipulated.
    // IN BC 10.15.254 01.00.001 63 46 06 02 10 :A_Memory_Read(Count=06, Addr=0210)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x06, 0x02, 0x10, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 69 42 46 02 10 0F 0F 0F 0F 0F 0F :A_Memory_Response(Count=06, Addr=0110, Data= 0F 0F 0F 0F 0F 0F)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x69, 0x42, 0x46, 0x02, 0x10, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C6 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 }; // @note Sequence counter changed from 0xC6 to 0xC2.
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.10.4 Legal Length - accessible Memory – Verify (5 bytes from 0220H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6E 43 D0 05 02 20 33 33 33 33 33 55 55 55 55 55 :A_MemoryBit_Write(Count=05, Addr=0220, Data= 33 33 33 33 33 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x43, 0xD0, 0x05, 0x02, 0x20, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends a response showing that the memory has been manipulated.
    // OUT BC 01.00.001 10.15.254 68 42 45 02 20 56 56 56 56 56 :A_Memory_Response(Count=05, Addr=0220, Data= 56 56 56 56 56)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x68, 0x42, 0x45, 0x02, 0x20, 0x56, 0x56, 0x56, 0x56, 0x56, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.10.5 Legal Length - protected Memory – Verify (5 bytes from 0300H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6E 43 D0 05 03 00 33 33 33 33 33 55 55 55 55 55 :A_MemoryBit_Write(Count=05, Addr=0300, Data= 33 33 33 33 33 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x43, 0xD0, 0x05, 0x03, 0x00, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends a response with the count set to zero and no data
    // OUT BC 01.00.001 10.15.254 63 42 40 0300 :A_Memory_Response (Count=00, Addr=0300, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x42, 0x40, 0x03, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.10.6 Legal Length - partly protected Memory – Verify (2 bytes from 02FF) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 68 43 D0 02 02 FF 33 33 55 55 :A_MemoryBit_Write(Count=02, Addr=02FF, Data= 33 33 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x68, 0x43, 0xD0, 0x02, 0x02, 0xFF, 0x33, 0x33, 0x55, 0x55, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: the BDUT sends a response with the count set to zero and no data
    // OUT BC 01.00.001 10.15.254 63 42 40 02 FF :A_Memory_Response (Count=00, Addr=02FF, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x42, 0x40, 0x02, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.10.7 Illegal Length - accessible Memory – Verify (6 bytes from 0230H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6F 43 D0 06 02 30 33 33 33 33 33 33 55 55 55 55 55 55 :A_MemoryBit_Write(Count=06, Addr=0230, Data= 33 33 33 33 33 33 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD0, 0x06, 0x02, 0x30, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 63 42 40 02 30 :A_Memory_Response(Count=00, Addr=0230, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x42, 0x40, 0x02, 0x30, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.11 Testing of A_Authorize_Request-Service: Server Test
 *
 * @ingroup KNX_08_03_07_02_11
 */
TEST(Application_Layer_Tests, Test_2_11)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    bdut.memory[0x200] = { 0xFF };
    bdut.memory[0x300] = { 0xAA };
    bdut.memory_min_read_level[0x200] = 1; // 1 gives access, 3 blocks access
    bdut.memory_min_read_level[0x300] = 2; // 2 gives access
    bdut.authorization_keys[1] = { 0x12, 0x34, 0x56, 0x78 };
    bdut.authorization_keys[2] = { 0xFF, 0xFF, 0xFF, 0xFF };
    bdut.authorization_keys[3] = { 0x87, 0x65, 0x43, 0x21 };

    /* 2.11.1 Connection-oriented Communication Mode */

    /* 2.11.1.1 Authorization with Legal Key */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 43 D1 00 12 34 56 78 :A_Authorize_Request(12345678)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0x12, 0x34, 0x56, 0x78, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance a: Authorize Response for level 1 is returned.
    // OUT BC 01.00.001 10.15.254 62 43 D2 01 :A_Authorize_Response(01)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance b: Memory read for level 1 block succeeds.
    // IN BC 10.15.254 01.00.001 63 46 01 02 00 :A_Memory_Read(Count=01, Addr=0200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x01, 0x02, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 64 46 41 02 00 FF :A_Memory_Response (Count=01, Addr=0200, Data= FF)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x46, 0x41, 0x02, 0x00, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.11.1.2 Authorization with Illegal Key */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 43 D1 00 87 65 43 21 :A_Authorize_Request(87654321)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0x87, 0x65, 0x43, 0x21, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance a: no authorization is returned (e.g. 3)
    // OUT BC 01.00.001 10.15.254 62 43 D2 03 :A_Authorize_Response(03)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance b: Memory read for level 1 block fails
    // IN BC 10.15.254 01.00.001 63 46 01 02 00 :A_Memory_Read(Count=01, Addr=0200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x01, 0x02, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 63 46 40 02 00 :A_Memory_Response (Count=00, Addr=0200, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x63, 0x46, 0x40, 0x02, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.11.1.3 Reaction to Authorize Response */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 62 43 D2 01 :A_Authorize_Response(01)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x62, 0x43, 0xD2, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : no reaction of the BDUT
    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.11.1.4 Authorization with default Key */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 43 D1 00 FF FF FF FF :A_Authorize_Request(FFFFFFFF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xFF, 0xFF, 0xFF, 0xFF, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance a: authorization to level 2 is given
    // OUT BC 01.00.001 10.15.254 62 43 D2 02 :A_Authorize_Response(02)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance b: Memory read for level 2 succeeds
    // IN BC 10.15.254 01.00.001 63 46 01 03 00 :A_Memory_Read(Count=01, Addr=0300)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x46, 0x01, 0x03, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 64 46 41 03 00 AA :A_Memory_Response (Count=01, Addr=0300, Data= AA)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x46, 0x41, 0x03, 0x00, 0xAA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.11.2 Connectionless Communication Mode */
}

/**
 * 2.12 Testing of A_Key_Write-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_12
 */
TEST(Application_Layer_Tests, Test_2_12)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    bdut.authorization_keys[1] = { 0x11, 0x11, 0x11, 0x11 };

    /* 2.12.1 Connection-oriented Communication Mode */

    /* 2.12.1.1 Authorize at Level 1 - set Key for Illegal Level */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 43 D1 00 11 11 11 11 :A_Authorize_Request(11111111)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0x11, 0x11, 0x11, 0x11, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 62 43 D2 01 :A_Authorize_Response(01)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 47 D3 16 12 34 56 78 :A_Key_Write(16, 12345678)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x47, 0xD3, 0x16, 0x12, 0x34, 0x56, 0x78, 0 }; // @note be aware that level=0x16=22
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: Rejection value is returned.
    // OUT BC 01.00.001 10.15.254 62 47 D4 FF :A_Key_Response(FF)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x47, 0xD4, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.12.1.2 Authorize at higher Level - set Key for lower Level */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 43 D1 00 11 11 11 11 :A_Authorize_Request(11111111)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0x11, 0x11, 0x11, 0x11, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 62 43 D2 01 :A_Authorize_Response(01)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 47 D3 02 22 22 22 22 :A_Key_Write(02, 22222222)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x47, 0xD3, 0x02, 0x22, 0x22, 0x22, 0x22, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance a: Access level 2 is set.
    // OUT BC 01.00.001 10.15.254 62 47 D4 02 :A_Key_Response(02)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x47, 0xD4, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance b: Authorization with new key at new level succeeds.
    // IN B0 10.15.254 01.00.001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 4B D1 00 22 22 22 22 :A_Authorize_Request(22222222)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x4B, 0xD1, 0x00, 0x22, 0x22, 0x22, 0x22, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 62 4B D2 02 :A_Authorize_Response(02)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x4B, 0xD2, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFe, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.12.1.3 Authorize and set Key at same Level */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 43 D1 00 22 22 22 22 :A_Authorize_Request(22222222)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0x22, 0x22, 0x22, 0x22, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 62 43 D2 02 :A_Authorize_Response(02)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 47 D3 02 12 12 12 12 :A_Key_Write(02, 12121212)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x47, 0xD3, 0x02, 0x12, 0x12, 0x12, 0x12, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance a: access level 2 is reset
    // OUT BC 01.00.001 10.15.254 62 47 D4 02 :A_Key_Response(02)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x47, 0xD4, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance b: authorization with new key at same level succeeds
    // IN B0 10.15.254 01.00.001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 4B D1 00 12 12 12 12 :A_Authorize_Request(12121212)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x4B, 0xD1, 0x00, 0x12, 0x12, 0x12, 0x12, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 62 4B D2 02 :A_Authorize_Response(02)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x4B, 0xD2, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.12.1.4 Authorize at lower Level – set Key for higher Level */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 43 D1 00 12 12 12 12 :A_Authorize_Request(12121212)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0x12, 0x12, 0x12, 0x12, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 62 43 D2 02 :A_Authorize_Response(02)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 47 D3 01 33 33 33 33 :A_Key_Write(01, 33333333)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x47, 0xD3, 0x01, 0x33, 0x33, 0x33, 0x33, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: rejection value is returned
    // OUT BC 01.00.001 10.15.254 62 47 D4 FF :A_Key_Response(FF)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x47, 0xD4, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 }; // @note Seq changed from 0 (0xC2) to 1 (0xC6)
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.12.2 Connectionless Communication Mode */
}

/**
 * 2.13 Testing of A_PropertyValue_Read-Service : Server-Test
 *
 * @ingroup KNX_08_03_07_02_13
 */
TEST(Application_Layer_Tests, Test_2_13)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* 2.13.1 Property Read with correct parameters */

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=1; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x01, 0x10, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=1; Count =1; Start=001; Property Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x01, 0x10, 0x01, 0x00, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.13.2 Property Read of array element 0 (current number of valid array elements) */

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=1; Count =1; Start=000)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x01, 0x10, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with current number of valid elements
    // Test frame (OUT): A_PropertyValue_Response with ObjIndex=0; PropID=1; Count =1; Start=000; current number of valid array elements
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x01, 0x10, 0x00, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.13.3 Property Read with illegal Object Index */

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read using a non-existing object index, PropID=1; count=1, start=1
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0xFF, 0x01, 0x10, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=1; Count =0; Start=001; no Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0xFF, 0x01, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.13.4 Property Read with illegal Property ID */

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read using a non-existing Property ID within an existing object (existing object index), count=1, start=1
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0xFF, 0x10, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=as set in Read; Count =0; Start=001; no Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0xFF, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.13.5 Property Read with illegal Start Index */

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read using an existing Property ID within an existing object (existing object index), count=1, with an illegal start index
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x01, 0x1F, 0xFF, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=as set in Read; Count =0; Start=as set in Read; no Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0x01, 0x0F, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.13.6 Property Read with illegal Count */

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read using an existing Property ID within an existing object (existing object index), start=001, with an illegal number of elements (count)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x01, 0xF0, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=as set in Read; Count =0; Start=001; no Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0x01, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.14 Testing of A_PropertyValue_Write-Service : Server-Test
 *
 * @ingroup KNX_08_03_07_02_14
 */
TEST(Application_Layer_Tests, Test_2_14)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* 2.14.1 Property Write with correct parameters */

    // Stimuli: send A_PropertyValue_Write to BDUT
    // Test frame (IN): A_PropertyValue_Write using a property with write access within an existing object (existing object index); Count=1; Start=001; valid data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x36, 0x10, 0x01, 0x81, 0 }; // object=OT_DEVICE, property=PID_PROGMODE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Write; PropID=as set in Write; Count =1; Start=001; written data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x36, 0x10, 0x01, 0x81, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.14.2 Property Write to array element 0 (current number of valid array elements) */

    // Stimuli: send A_PropertyValue_Write to BDUT
    // Test frame (IN): A_PropertyValue_Write (ObjIndex & PropID of a property with write-access enabled; Count =1; Start=000; data= 0)
    bdut.interface_objects->device()->interface_object_name()->description.write_enable = true;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x67, 0x03, 0xD7, 0x00, 0x02, 0x10, 0x00, 0x00, 0x00, 0 }; // object=OT_DEVICE, property=PID_OBJECT_NAME
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex & PropID as in write request; Count =1; Start=000; current number of valid array elements=0)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x02, 0x10, 0x00, 0x00, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Note: test is applicable only for a BDUT that support properties with more than 1 array element and with write-access enabled.

    // Stimuli: send A_PropertyValue_Read to BDUT to a now "empty" property
    // Test frame (IN): A_PropertyValue_Read (ObjIndex & PropID of written property; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x02, 0x10, 0x01, 0 }; // object=OT_DEVICE, property=PID_OBJECT_NAME
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data.
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=as set in Read; Count =0; Start=001; no Data).
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0x02, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.14.3 Property Write to array element with array index > current valid nr. of elements */

    // Stimuli: send A_PropertyValue_Write to BDUT
    // Test frame (IN): A_PropertyValue_Write, using a property with write access within an existing object (existing object index); Count=1; Start=index>current valid nr. of elements but with index < max. nr. of elements; valid data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x02, 0x10, 0x02, 0xFF, 0 }; // object=OT_DEVICE, property=PID_OBJECT_NAME
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Write; PropID=as set in Write; Count =1; Start=as set in write; written data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x02, 0x10, 0x02, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (same property as written with property write; Count =1; Start=000)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x02, 0x10, 0x00, 0 }; // object=OT_DEVICE, property=PID_OBJECT_NAME
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with adapted number of valid elements
    // Test frame (OUT): A_PropertyValue_Response with ObjIndex=as set in read; PropID=as set in read; Count =1; Start=000; current (adapted) number of valid array elements
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x02, 0x10, 0x00, 0x00, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.14.4 Property Write with illegal Object Index */

    // Stimuli: send A_PropertyValue_Write to BDUT
    // Test frame (IN): A_PropertyValue_Write using a non-existing object index, PropID=1; count=1, start=1; valid data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x03, 0xD7, 0xFF, 0x01, 0x10, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=1; Count =0; Start=001; no Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0xFF, 0x01, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.14.5 Property Write with illegal Property ID */

    // Stimuli: send A_PropertyValue_Write to BDUT
    // Test frame (IN): A_PropertyValue_Write using a non-existing Property ID within an existing object (existing object index), count=1, start=1; valid data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x03, 0xD7, 0x00, 0xFF, 0x10, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=as set in Read; Count =0; Start=001; no Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0xFF, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.14.6 Property Write with illegal Start Index */

    // Stimuli: send A_PropertyValue_Write to BDUT
    // Test frame (IN): A_PropertyValue_Write using an existing Property ID within an existing object (existing object index), count=1, with an illegal start index; valid data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x02, 0x1F, 0xFF, 0 }; // object=OT_DEVICE, property=PID_OBJECT_NAME
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=as set in Read; Count =0; Start=as set in Read; no Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0x02, 0x0F, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.14.7 Property Write with illegal Count */

    // Stimuli: send A_PropertyValue_Write to BDUT
    // Test frame (IN): A_PropertyValue_Write using an existing Property ID within an existing object (existing object index), start=001, with an illegal number of elements (count); valid data
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x67, 0x03, 0xD7, 0x00, 0x36, 0x20, 0x01, 0x00, 0x00, 0 }; // object=OT_DEVICE, property=PID_PROGMODE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=as set in Read; Count =0; Start=001; no Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0x36, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.14.8 Property Write to write-protected Value */

    // Stimuli: send A_PropertyValue_Write to BDUT
    // Test frame (IN): A_PropertyValue_Write using an existing Property ID within an existing object (existing object index), count=1, start=001; valid data; use a write protected/read-only property
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x03, 0xD7, 0x00, 0x01, 0x10, 0x01, 0xFF, 0 }; // object=OT_DEVICE, property=PID_OBJECT_TYPE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with count set to 0 and no data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=as set in Read; PropID=as set in Read; Count =0; Start=001; no Data)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0x01, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.15 Testing of A_PropertyDescription_Read-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_15
 */
TEST(Application_Layer_Tests, Test_2_15)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* 2.15.1.1 A_PropertyDescription_Read-Service with correct parameters */

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=1; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=1; PropIndex=0; property data type; max. nr. of elements; write access; read access)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=0; index of an existing Property)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=ID of property with asked index; PropIndex=as set in Read; property data type; max. nr. of elements; write access; read access)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.15.1.2 A_PropertyDescription_Read-Service with illegal object index */

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (non existing object index; PropID=1; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with the complete data field, but MaxCount set to zero (all data set to zero: property data type field, MaxCount, write-access & read access)
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=as set in read; PropID=as set in read; PropIndex=as set in read; property data type=0; MaxCount=0; write access=0; read access=0)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.15.1.3 A_PropertyDescription_Read-Service with illegal Property ID */

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex of existing object; invalid PropID; PropIndex=don’t care)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x03, 0xD8, 0x00, 0xFF, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with the complete data field, but MaxCount set to zero (all data set to zero: property data type field, MaxCount, write-access & read access)
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=as set in read; PropID=as set in read; PropIndex=as set in read; property data type=0; MaxCount=0; write access=0; read access=0)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.15.1.4 A_PropertyDescription_Read-Service with illegal Property Index */

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex of existing object; PropID = 0; PropIndex of a not existing property)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x00, 0xFF, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with data field set to zero
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=as set in read; PropID=as set in read; PropIndex=as set in read; property data type=0; MaxCount=0; write access=0; read access=0)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.16 Testing of A_IndAddressSerialNumber_Write-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_16
 */
TEST(Application_Layer_Tests, Test_2_16)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The Serial number of the device must be known. Example: the Serial number of device is 303030303030H
    bdut.interface_objects->device()->serial_number()->serial_number = { 0x30, 0x30, 0x30, 0x30, 0x30, 0x30 };

    /* 2.16.1 Set Individual Address via correct Serial Number */

    // IN BC 10.15.254 0000 ED 03 DE 30 30 30 30 30 30 10 02 00 00 00 00 :A_IndAddressSerialNumber_Write(Sno=303030303030, PhysAddr=1002)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xED, 0x03, 0xDE, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x10, 0x02, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT now has the individual address 1002H. This can be checked via an IndAddressRead in programming mode. For verification switch ON programming LED of BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    // IN BC 10.15.254 0000 E1 01 00 :A_IndAddress_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.002 0000 E1 01 40 :A_IndAddress_Response(Addr=01.00.002)
    tp1_data = { 0xBC, 0x10, 0x02, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Now switch OFF programming LED of BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = false;

    /* 2.16.2 Set Individual Address to other Value via same Serial Number */

    // IN BC 10.15.254 0000 ED 03 DE 30 30 30 30 30 30 10 01 00 00 00 00 :A_IndAddressSerialNumber_Write(Sno=303030303030, PhysAddr=1001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xED, 0x03, 0xDE, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT now has the individual address 1001H. This can be checked via a IndividualAddressRead in programming mode. For verification switch ON programming LED of BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    // IN BC 10.15.254 0000 E1 01 00 :A_IndAddress_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 0000 E1 01 40 :A_IndAddress_Response(Addr=01.00.001)
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Now switch OFF programming LED of BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = false;

    /* 2.16.3 Set Individual Address to other Value via incorrect Serial Number */

    // IN BC 10.15.254 0000 ED 03 DE 30 30 30 30 30 29 10 03 00 00 00 00 :A_IndAddressSerialNumber_Write(Sno=303030303029, PhysAddr=1003)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xED, 0x03, 0xDE, 0x30, 0x30, 0x30, 0x30, 0x30, 0x29, 0x10, 0x03, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT still has the individual address 1001H. This can be checked via a IndividualAddressRead in programming mode.
    // For verification switch ON programming LED of BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    // IN BC 10.15.254 0000 E1 01 00 :A_IndAddress_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 0000 E1 01 40 :A_IndAddress_Response(Addr=01.00.001)
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // switch OFF programming LED of BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = false;
}

/**
 * 2.17 Testing of A_IndAddressSerialNumber_Read-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_17
 */
TEST(Application_Layer_Tests, Test_2_17)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The Serial number of the device must be known. Example: the Serial number of device is 303030303030H
    bdut.interface_objects->device()->serial_number()->serial_number = { 0x30, 0x30, 0x30, 0x30, 0x30, 0x30 };

    /* 2.17.1 Try to read Individual Address via incorrect Serial Number */

    // IN BC 10.15.254 00/0000 E7 03 DC 30 30 30 30 30 29 :A_IndAddressSerialNumber_Read(Sno=303030303029)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE7, 0x03, 0xDC, 0x30, 0x30, 0x30, 0x30, 0x30, 0x29, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: No response may be sent.
    ASSERT_EQ(log, std::cend(busmoni.log));

    /* 2.17.2 Send Response to BDUT via incorrect Serial Number */

    // IN BC 10.15.254 00/0000 EB 03 DD 30 30 30 30 30 29 00 00 00 00 :A_IndAddressSerialNumber_Response(Sno=303030303029)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xEB, 0x03, 0xDD, 0x30, 0x30, 0x30, 0x30, 0x30, 0x29, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: No response may be sent.
    ASSERT_EQ(log, std::cend(busmoni.log));

    /* 2.17.3 Read Individual Address via correct Serial Number */

    // IN BC 10.15.254 00/0000 E7 03 DC 30 30 30 30 30 30 :A_IndAddressSerialNumber_Read(Sno=303030303030)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE7, 0x03, 0xDC, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends an A_IndividualAddressSerialNumber_Response-PDU.
    // OUT BC 01.01.001 00/0000 EB 03 DD 30 30 30 30 30 30 00 00 00 00 :A_IndAddressSerialNumber_Response(Sno=303030303030)
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xEB, 0x03, 0xDD, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.17.4 Send Response to BDUT via correct Serial Number */

    // IN BC 10.15.254 00/0000 EB 03 DD 30 30 30 30 30 30 00 00 00 00 :A_IndAddressSerialNumber_Response(Sno=303030303030)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xEB, 0x03, 0xDD, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: no response may be sent
    ASSERT_EQ(log, std::cend(busmoni.log));
}

/**
 * 2.18 Testing of A_NetworkParameter_Read - Server Tests
 *
 * @ingroup KNX_08_03_07_02_18
 */
TEST(Application_Layer_Tests, Test_2_18)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: send A_NetworkParameter_Read to BDUT
    // Test frame (IN): A_NetworkParameter_Read (Object_Type; PID; Test_Info)
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE5, 0x03, 0xDA, 0x00, 0x00, 0x0B, 0x01, 0 }; // NM_Read_SerialNumber_By_ProgrammingMode (object_type=OT_DEVICE, pid=PID_SERIAL_NUMBER, operand=01h)
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_NetworkParameter_Response with correct data and standard hop count
    // Test frame (OUT): A_NetworkParameter_Response (Object_Type; PID; Test_Info; Test_Result)
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xEB, 0x03, 0xDB, 0x00, 0x00, 0x0B, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.19 Testing of A_NetworkParameter_Write - Server Tests
 *
 * @ingroup KNX_08_03_07_02_19
 */
TEST(Application_Layer_Tests, Test_2_19)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->device()->subnetwork_address()->subnetwork_address = 0x11;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* 2.19.1 General Test Case with correct service parameters */

    // Stimuli: send A_NetworkParameter_Write to BDUT
    // Test frame (IN): A_NetworkParameter_Write (Object_Type; PID; Value)

    // Note: manufacturers must declare in the PICS/PIXIT for management server services, which network
    // parameters are supported A_NetworkParameter_Write-Service

    // Acceptance: BDUT´s behaviour according manufacture’s declaration about implemented features

    /* 2.19.2 Example: Subnet Address Update (PID_Subnet_Addr) */

    // Stimuli: Use a Telegram Generator to send an A_NetworkParameter_Write (SNA Update) to BDUT
    // Test frame (IN): A_NetworkParameter_Write (Device Object; PID_Subnet_Addr; new SNA), with hop count = 0
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE5, 0x03, 0xE4, 0x00, 0x00, 0x39, 0x11, 0 }; // OT_DEVICE, PID_SUBNET_ADDR
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @note only the Property is changed, not the Individual Address in the Group Address Table.

    // Acceptance: BDUT has updated its Subnet Address:
    // Test frame (IN): send Property Read to PID_Subnet_Addr (DO)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x39, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Test frame (OUT): Property Response, value of SNA updated
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x39, 0x10, 0x01, 0x11, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.20 Illegal APCI in point to point communication mode
 *
 * @ingroup KNX_08_03_07_02_20
 */
TEST(Application_Layer_Tests, Test_2_20)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    //std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: Use a telegram generator (e.g. EITT) to send frames with incorrect APCI in point to point
    // connectionless communication mode to the BDUT.
    std::vector<std::vector<uint8_t>> tp1_datas {
        //    0  1 2  3 4  5  6  7  8  9
        // IN BC AFFE 1101 63 00 00 00 00 // APCI = 0x000 = A_GroupValue_Read
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x00, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 00 00 00 // APCI = 0x100 = A_IndividualAddress_Read
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x00, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 00 00 00 // APCI = 0x200 = A_Memory_Read
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x00, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 01 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x01, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 01 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x01, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 01 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x01, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 02 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x02, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 02 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x02, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 02 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x02, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 04 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x04, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 04 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x04, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 04 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x04, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 08 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x08, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 08 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x08, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 08 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x08, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 10 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x10, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 10 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x10, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 10 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x10, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 20 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x20, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 20 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x20, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 20 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x20, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 40 00 00 // APCI = 0x040 = A_GroupValue_Response
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x40, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 40 00 00 // APCI = 0x140 = A_IndividualAddress_Response
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x40, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 40 00 00 // APCI = 0x240 = A_Memory_Response
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x40, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 80 00 00 // APCI = 0x080 = A_GroupValue_Write
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x80, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 80 00 00 // APCI = 0x180 = A_ADC_Read
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x80, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 80 00 00 // APCI = 0x280 = A_Memory_Write
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x80, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 08 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x08, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 08 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x08, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 08 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x08, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 11 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x11, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 11 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x11, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 11 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x11, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 22 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x22, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 22 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x22, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 22 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x22, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 44 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x44, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 44 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x44, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 44 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x44, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 00 88 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x00, 0x88, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 01 88 00 00 // APCI = 0x180 = A_ADC_Read
        //{ 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x01, 0x88, 0x00, 0x00, 0 },
        // IN BC AFFE 1101 63 02 88 00 00
        { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x63, 0x02, 0x88, 0x00, 0x00, 0 },
        // ... ....................
    };
    for (const std::vector<uint8_t> & tp1_data : tp1_datas) {
        // Acceptance: BDUT does not accept the frames (sends no reaction onto the bus).
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        // @note I interpret the acceptance as such: Frame is correct, hence L_Ack. But there is no service, so no ACon.
        std::vector<uint8_t> tp1_data_ack = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data_ack);
        //ASSERT_EQ(log, std::cend(busmoni.log)); // No L_Ack
    }
}

/**
 * 2.21 M_LC_TAB_MEM_ENABLE -Server Test
 *
 * @ingroup KNX_08_03_07_02_21
 */
TEST(Application_Layer_Tests, Test_2_21)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    //std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    //std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // See test sequences in clause 2.19 and in clause 2.20.
    // @note Likely 2.22 M_LC_TAB_MEM_READ and 2.23 M_LC_TAB_MEM_WRITE is meant.
}

/**
 * 2.22 M_LC_TAB_MEM_READ -Server Test
 *
 * @ingroup KNX_08_03_07_02_22
 */
TEST(Application_Layer_Tests, Test_2_22)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1100);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Test Setup: Load memory area with default value (by means of LC_TAB _MEM_Write service)
    // Assumed memory Model of line coupler Mask 09xxh (external RAM):
    // Address 200H to FFFH : accessible memory area
    // Downloaded data from 200H onwards : 11 22 33 44 55 66 77 88 99 AA BB CC DD EE FF 11 22 and so forth
    // From address 2200H onwards : protected memory area
    bdut.routing_table[0x200] = {
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11,
        0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22,
        0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33,
        0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44,
        0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55,
        0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66,
        0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
        0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
        0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99,
        0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA,
        0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB,
        0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC,
        0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD,
        0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE,
        0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11
    };

    /* 2.22.1 Legal Length - accessible Memory Area (10 bytes from 0200H) */

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 }; // A_Open_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 64 47 C1 0A 02 00 :LcTabMemRead(Count=0A, Addr=0200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x64, 0x47, 0xC1, 0x0A, 0x02, 0x00, 0 }; // A_Read_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a Response PDU with the required data.
    // OUT BC 01.01.000 10.15.254 6E 43 C2 0A 02 00 11 22 33 44 55 66 77 88 99 AA :LcTabMemResponse(Count=0A, Addr=0200, Data=11 22 33 44 55 66 77 88 99 AA )
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x6E, 0x43, 0xC2, 0x0A, 0x02, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.22.2 Legal Length - protected Memory Area (10 bytes from 2200H) */

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 }; // A_Open_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 64 47 C1 0A 22 00 :LcTabMemRead(Count=0A, Addr=2200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x64, 0x47, 0xC1, 0x0A, 0x22, 0x00, 0 }; // A_Read_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a Response PDU with length byte set to zero and no data.
    // OUT BC 01.01.000 10.15.254 64 43 C2 00 22 00 :LcTabMemResponse(Count=00, Addr=2200, Data=)
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x64, 0x43, 0xC2, 0x00, 0x22, 0x00, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.22.3 Legal Length - partly protected Memory Area (2 Bytes from FFFH) */

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 64 47 C1 02 21 FF :LcTabMemRead(Count=02, Addr=21FF)
    tp1_data = { 0xBc, 0xAF, 0xFE, 0x11, 0x00, 0x64, 0x47, 0xC1, 0x02, 0x21, 0xFf, 0 }; // A_Read_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a Response PDU with length byte set to zero and no data.
    // OUT BC 01.01.000 10.15.254 64 43 C2 00 21 FF :LcTabMemResponse(Count=00, Addr=21FF, Data=)
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x64, 0x43, 0xC2, 0x00, 0x21, 0xFF, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.22.4 Illegal Length - accessible Memory Area (12 bytes from 200H) */

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 64 47 C1 0C 02 00 :LcTabMemRead(Count=0C, Addr=0200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x64, 0x47, 0xC1, 0x0C, 0x02, 0x00, 0 }; // A_Read_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a Response PDU with length byte set to zero and no data.
    // OUT BC 01.01.000 10.15.254 64 43 C2 00 02 00 :LcTabMemResponse(Count=00, Addr=0200, Data=)
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x64, 0x43, 0xC2, 0x00, 0x02, 0x00, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.23 M_LC_TAB_MEM_WRITE -Server Test
 *
 * @ingroup KNX_08_03_07_02_23
 */
TEST(Application_Layer_Tests, Test_2_23)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1100);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Test Setup : Load memory area with default value (by means of M_LC_Tab_Mem_Write-Service)
    bdut.routing_table[0x0200] = {
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11,
        0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22,
        0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33,
        0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44,
        0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55,
        0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66,
        0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77,
        0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
        0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99,
        0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA,
        0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB,
        0xCC, 0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC,
        0xDD, 0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD,
        0xEE, 0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE,
        0xFF, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF,
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11
    };
    bdut.routing_table[0x21FF] = { 0xFF };

    /* 2.23.1 Assumed memory Model see 2.22. Step 1 : Legal Length - accessible Memory - no Verify (10 Bytes from 200H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 }; // A_Open_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 6E 47 C3 0A 02 00 11 22 33 44 55 66 77 88 99 AA :LcTabMemWrite(Count=0A, Addr=0200, Data=11 22 33 44 55 66 77 88 99 AA )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x6E, 0x47, 0xC3, 0x0A, 0x02, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 }; // A_Write_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: After reading the written memory, the same data is returned by the BDUT as written)
    // IN BC 10.15.254 01.01.000 64 4B C1 0A 02 00 :LcTabMemRead(Count=0A, Addr=0200)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x64, 0x4B, 0xC1, 0x0A, 0x02, 0x00, 0 }; // A_Read_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.01.000 10.15.254 6E 43 C2 0A 02 00 11 22 33 44 55 66 77 88 99 AA :LcTabMemResponse(Count=0A, Addr=0200, Data=11 22 33 44 55 66 77 88 99 AA )
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x6E, 0x43, 0xC2, 0x0A, 0x02, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.23.2 Step 2 : Legal Length - partly protected Memory - no Verify (2 Bytes from 21FFH) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 }; // A_Open_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 66 47 C3 02 21 FF 11 22 :LcTabMemWrite(Count=02, Addr=21FF, Data=11 22 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x66, 0x47, 0xC3, 0x02, 0x21, 0xFF, 0x11, 0x22, 0 }; // A_Write_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: after reading the affected accessible memory area, a response shall be generated showing that data has not been modified
    // IN BC 10.15.254 01.01.000 64 4B C1 01 21 FF :LcTabMemRead(Count=01, Addr=0FFF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x64, 0x4B, 0xC1, 0x01, 0x21, 0xFF, 0 }; // A_Read_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.01.000 10.15.254 65 43 C2 01 21 FF FF :LcTabMemResponse(Count=01, Addr=0FFF, Data=FF )
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x65, 0x43, 0xC2, 0x01, 0x21, 0xFF, 0xFF, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.23.3 Step 3 : Illegal Length - accessible Memory - no Verify (12 bytes from 200H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 }; // A_Open_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 6F 47 C3 0C 02 00 01 02 03 04 05 06 07 08 09 0A 0B :LcTabMemWrite(Count=0C, Addr=0200, Data=01 02 03 04 05 06 07 08 09 0A 0B )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x6F, 0x47, 0xC3, 0x0C, 0x02, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0 }; // A_Write_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 64 4B C1 0A 02 00 :LcTabMemRead(Count=0A, Addr=0000)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x64, 0x4B, 0xC1, 0x0A, 0x02, 0x00, 0 }; // A_Read_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : after reading the affected accessible memory area, a response shall be generated showing that data has not been modified
    // OUT BC 01.01.000 10.15.254 6E 43 C2 0A 02 00 11 22 33 44 55 66 77 88 99 AA :LcTabMemResponse(Count=0A, Addr=0200, Data=11 22 33 44 55 66 77 88 99 AA )
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x6E, 0x43, 0xC2, 0x0A, 0x02, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.23.4 Step 4 : Legal Length - accessible Memory - Verify (Activation Method Manufacturer dependent, e.g. property write - 10 Bytes from 200H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 6F 47 C3 0A 02 00 01 02 03 04 05 06 07 08 09 AA :LcTabMemWrite(Count=0A, Addr=0200, Data=01 02 03 04 05 06 07 08 09 AA )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x6E, 0x47, 0xC3, 0x0A, 0x02, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xAA, 0 }; // A_Write_Routing_Table_Req // @note Length changed from 0x6F to 0x6F
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a Memory Response with the data written.
    // OUT BC 01.01.000 10.15.254 6E 43 C2 0A 02 00 01 02 03 04 05 06 07 08 09 AA :LcTabMemResponse(Count=0A, Addr=0200, Data=01 02 03 04 05 06 07 08 09 AA )
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x6E, 0x43, 0xC2, 0x0A, 0x02, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0xAA, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.23.5 Step 5 : Legal Length - protected Memory – Verify (10 bytes from 1000H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 6E 47 C3 0A 22 00 00 11 22 33 44 55 66 77 88 99 AA: LcTabMemWrite(Count=0A, Addr=2200, Data=00 11 22 33 44 55 66 77 88 99 AA )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x6F, 0x47, 0xC3, 0x0A, 0x22, 0x00, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 }; // A_Write_Routing_Table_Req // @note Length changed from 0x6E to 0x6F
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a Memory Response with the length set to 0 and no data.
    // OUT BC 01.01.000 10.15.254 64 43 C2 00 22 00 :LcTabMemResponse(Count=00, Addr=2200, Data=)
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x64, 0x43, 0xC2, 0x00, 0x22, 0x00, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.23.6 Step 6 : Legal Length - partly protected Memory – Verify (2 bytes from 21FF) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // @todo std::deque corrupts on the next lines, so we just clear it here. Weird.
    busmoni.log.clear();
    log = std::cbegin(busmoni.log);

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 66 47 C3 02 21 FF 11 22 :LcTabMemWrite(Count=02, Addr=21FF, Data=11 22 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x66, 0x47, 0xC3, 0x02, 0x21, 0xFF, 0x11, 0x22, 0 }; // A_Write_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a Memory Response with the length set to 0 and no data.
    // OUT BC 01.01.000 10.15.254 64 43 C2 00 21 FF :LcTabMemResponse(Count=00, Addr=21FF, Data=)
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x64, 0x43, 0xC2, 0x00, 0x21, 0xFF, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.23.7 Step 7 : Illegal Length - accessible Memory – Verify (12 bytes from 200H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.01.000 60 80 :T-Connect(Addr=1100)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 61 43 C0 :M_LC_Tab_Mem_Enable()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x61, 0x43, 0xC0, 0 }; // A_Open_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.01.000 6F 47 C3 0C 02 00 DD CC BB AA 99 88 77 66 55 44 33 :LcTabMemWrite(Count=0C, Addr=0200, Data=DD CC BB AA 99 88 77 66 55 44 33 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x00, 0x6F, 0x47, 0xC3, 0x0C, 0x02, 0x00, 0xDD, 0xCC, 0xBB, 0xAA, 0x99, 0x88, 0x77, 0x66, 0x55, 0x44, 0x33, 0 }; // A_Write_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.000 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x11, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a Memory Response with the length set to 0 and no data.
    // OUT BC 01.01.000 10.15.254 64 43 C2 00 02 00 :LcTabMemResponse(Count=00, Addr=0200, Data=)
    tp1_data = { 0xBC, 0x11, 0x00, 0xAF, 0xFE, 0x64, 0x43, 0xC2, 0x00, 0x02, 0x00, 0 }; // A_Read_Routing_Table_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.01.000 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x11, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.24 M_LC_SLAVE_READ/Write -Server Test
 *
 * @ingroup KNX_08_03_07_02_24
 */
TEST(Application_Layer_Tests, Test_2_24)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0xFF00);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    bdut.router_memory[0x0119] = { 0x01, 0x02 };

    /* 2.24.1 A_Read_Router_Memory legal length */

    // IN B0 10.15.254 15.15.000 60 80 T-Connect(Addr=FF00)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 61 43 C0 RoutingTableOpen()
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x61, 0x43, 0xC0, 0 }; // A_Open_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 15.15.000 10.15.254 60 C2 T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 66 47 CA 02 01 19 01 02 :RouterMemoryWrite(Count=02, Addr=0119, Data=01 02 )
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x66, 0x47, 0xCA, 0x02, 0x01, 0x19, 0x01, 0x02, 0 }; // A_Write_Router_Memory_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 15.15.000 10.15.254 60 C6 T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 64 4B C8 02 01 19 :RouterMemoryRead(Count=02, Addr=0119)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x64, 0x4B, 0xC8, 0x02, 0x01, 0x19, 0 }; // A_Read_Router_Memory_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 15.15.000 10.15.254 60 CA T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends Response with correct length byte and data
    // OUT B0 15.15.000 10.15.254 66 43 C9 02 01 19 01 02 :RouterMemoryResponse(Count=02, Addr=0119, Data=01 02 )
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x66, 0x43, 0xC9, 0x02, 0x01, 0x19, 0x01, 0x02, 0 }; // A_Read_Router_Memory_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 60 C2 T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 60 81 T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.24.2 Router Memory Read illegal length */

    // IN B0 10.15.254 15.15.000 60 80 T-Connect(Addr=FF00)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFf, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 61 43 C0 RoutingTableOpen()
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x61, 0x43, 0xC0, 0 }; // A_Open_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 15.15.000 10.15.254 60 C2 T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 66 47 CA 02 01 19 01 02 :RouterMemoryWrite(Count=02, Addr=0119, Data=01 02 )
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x66, 0x47, 0xCA, 0x020, 0x01, 0x19, 0x01, 0x02, 0 }; // A_Write_Router_Memory_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 15.15.000 10.15.254 60 C6 T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 64 4B C8 0C 01 19 :RouterMemoryRead(Count=0C, Addr=0119)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x64, 0x4B, 0xC8, 0x0C, 0x01, 0x19, 0 }; // A_Read_Router_Memory_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 15.15.000 10.15.254 60 CA T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : BDUT sends Response with length byte set to zero and no data
    // OUT B0 15.15.000 10.15.254 64 43 C9 00 01 19 :RouterMemoryResponse(Count=00, Addr=0119, Data=)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x64, 0x43, 0xC9, 0x00, 0x01, 0x19, 0 }; // A_Read_Router_Memory_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 60 C2 T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 60 81 T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.24.3 Router Memory Write illegal length */

    // IN B0 10.15.254 15.15.000 60 80 T-Connect(Addr=FF00)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 61 43 C0 RoutingTableOpen()
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x61, 0x43, 0xC0, 0 }; // A_Open_Routing_Table_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 15.15.000 10.15.254 60 C2 T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 66 47 CA 0C 01 19 22 33 :RouterMemoryWrite(Count=0C, Addr=0119, Data=22 33 )
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x66, 0x47, 0xCA, 0x0C, 0x01, 0x19, 0x22, 0x33, 0 }; // A_Write_Router_Memory_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 15.15.000 10.15.254 60 C6 T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 64 4B C8 02 01 19 :RouterMemoryRead(Count=02, Addr=0119)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x64, 0x4B, 0xC8, 0x02, 0x01, 0x19, 0 }; // A_Read_Router_Memory_Req
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 15.15.000 10.15.254 60 CA T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a Response, no data were changed
    // OUT B0 15.15.000 10.15.254 66 43 C9 02 01 19 01 02 :RouterMemoryResponse(Count=02, Addr=0119, Data=01 02 )
    tp1_data = { 0xB0, 0xFF, 0x00, 0xAF, 0xFE, 0x66, 0x43, 0xC9, 0x02, 0x01, 0x19, 0x01, 0x02, 0 }; // A_Read_Router_Memory_Res
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 60 C2 T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 15.15.000 60 81 T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0xFF, 0x00, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.25 A_ServiceInformation_Indication_Write-Service
 *
 * @ingroup KNX_08_03_07_02_25
 */
TEST(Application_Layer_Tests, Test_2_25)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.authorization_keys[0] = { 0xAA, 0xAA, 0xAA, 0xAA };

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* third BCU */
    BCU bcu3(io_context, tp1_bus);
    bcu3.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0xA007);

    // Prepare device to enable the sending of the ServiceIndicationWrite_Service

    // Connect to BDUT
    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access the service control property of device object - Authorise
    // response for level 0 is returned
    // IN BC 10.15.254 01.00.001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 }; // A_Authorize_Request
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 }; // A_Authorize_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a property value write to device object, PID_SERVICE_CONTROL to enable the "receive own indaddr srvinfo"

    // BDUT returns the property value after writing
    // IN BC 10.00.001 01.00.001 67 47 D7 00 08 10 01 00 02 :PropertyWrite(Obj=00, Prop=08, Count=1, Start=001, Data=00 02 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x67, 0x47, 0xD7, 0x00, 0x08, 0x10, 0x01, 0x00, 0x02, 0 }; // A_PropertyValue_Write // @note source changed from 0xA001 to 0xAFFE.
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 67 47 D6 00 08 10 01 00 02 :PropertyResponse(Obj=00, Prop=08, Count=1, Start=001, Data=00 02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x67, 0x47, 0xD6, 0x00, 0x08, 0x10, 0x01, 0x00, 0x02, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Disconnect from BDUT
    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Now set individual address of BDUT to A007 (the same as third BCU)

    // Switch on Programming LED of DUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    // IN BC 10.15.254 00/0000 E3 00 C0 A0 07 :A_IndAddress_Write(Addr=A007)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE3, 0x00, 0xC0, 0xA0, 0x07, 0 }; // A_IndividualAddress_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Switch off Programming LED of DUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = false;

    ASSERT_EQ(bdut.interface_objects->group_address_table()->individual_address, 0xA007);

    // Start of the actual test

    // Send a ValueWrite with same source address as BDUT (via third BCU) - Third BCU runs on application
    // layer, so no L_Data.con can be detected, merely the ValueWrite.ind on the remote BCU

    // IN 11 0C A0 07 10 01 E1 00 81 :L_DATA.requ(0C, S=A007, D=1001, E1, 00, Data=81)
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_req), 0x0C, 0xA0, 0x07, 0x10, 0x01, 0xE1, 0x00, 0x81 }; // L_Data.req
    bcu3.emi_req(pei_data);
    io_context.poll();

    // OUT BC 10.00.007 02/0001 E1 00 81 :EIS1 switching (switch on)
    tp1_data = { 0xBC, 0xA0, 0x07, 0x10, 0x01, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    ASSERT_EQ(bdut.interface_objects->device()->device_control()->device_control.individual_address_duplication(), true);

    // Acceptance: The BDUT sends a ServiceInformationIndicationWrite service

    // OUT B0 10.00.007 00/0000 E4 03 DF 02 00 00 :ServiceInfo(Info=020000)
    tp1_data = { 0xB0, 0xA0, 0x07, 0x00, 0x00, 0xE4, 0x03, 0xDF, 0x02, 0x00, 0x00, 0 }; // A_ServiceInformation_Indication
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.26 Testing of A_DomainAddress_Write-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_26
 */
TEST(Application_Layer_Tests, Test_2_26)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Tests are applicable only for BDUT on open media with 2-octet domain address.
    // Prior to starting the test, set the Domain Address of the RS232 to 0002H and its individual address to 07.15.254
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);
    // The BDUT shall have the Domain Address 0002H and the individual address 01.00.001.
    bdut.interface_objects->device()->domain_address()->domain_address = 0x0002;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    // For step 1 : Switch off programming LED on BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = false;

    /* 2.26.1 Try to set Domain Address with LED off */

    // IN B0 07.15.254 00/0000 E3 03 E0 00 05 :A_DomainAddress_Write (Domain Address=0005)
    tp1_data = { 0xB0, 0x7F, 0xFE, 0x00, 0x00, 0xE3, 0x03, 0xE0, 0x00, 0x05, 0 }; // A_DomainAddress_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: No reaction of the BDUT - BDUT keeps its Domain Address as downloaded prior to starting the test. This can be checked by:
    ASSERT_EQ(log, std::cend(busmoni.log));

    // @note switch on programming LED to get answer to A_DomainAddress_Read
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    // IN B0 07.15.254 00/0000 E1 03 E1 :A_DomainAddress_Read()
    tp1_data = { 0xB0, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 00/0000 E3 03 E2 00 02 :A_DomainAddress_Response(Domain Address=0002)
    tp1_data = { 0xA0, 0x10, 0x01, 0x00, 0x00, 0xE3, 0x03, 0xE2, 0x00, 0x02, 0 }; // A_DomainAddress_Response // @note Control changed from 0xB0 to 0xA0.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // For test step 2 : Switch on programming LED on BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    /* 2.26.2 Try to set Address with LED on */

    // IN B0 07.15.254 00/0000 E3 03 E0 00 05 :A_DomainAddress_Write (Domain Address=0005)
    tp1_data = { 0xB0, 0x7F, 0xFE, 0x00, 0x00, 0xE3, 0x03, 0xE0, 0x00, 0x05, 0 }; // A_DomainAddress_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT has the new Domain Address.

    // IN B0 07.15.254 00/0000 E1 03 E1 :A_DomainAddress_Read()
    tp1_data = { 0xB0, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 00/0000 E3 03 E2 00 05 :A_DomainAddress_Response (Domain Address=0005)
    tp1_data = { 0xA0, 0x10, 0x01, 0x00, 0x00, 0xE3, 0x03, 0xE2, 0x00, 0x05, 0 }; // A_DomainAddress_Response // @note Control changed from 0xB0 to 0xA0.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.27 Testing of A_DomainAddress_Read-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_27
 */
TEST(Application_Layer_Tests, Test_2_27)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Tests are applicable only for BDUT on open media with 2-octet domain address.
    // Prior to starting the test, set the Domain Address of the RS232 to 0002H and its individual address to 07.15.254
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);
    // The BDUT shall have the Domain Address 0005H and the individual address 01.00.001.
    bdut.interface_objects->device()->domain_address()->domain_address = 0x0005;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    // For step 1 to 2 : Switch off programming LED on BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = false;

    /* 2.27.1 Try to read Domain Address with LED off */

    // IN B0 07.15.254 00/0000 E1 03 E1 :A_DomainAddress_Read()
    tp1_data = { 0xB0, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: No response may be sent.
    ASSERT_EQ(log, std::cend(busmoni.log));

    /* 2.27.2 Send Response to BDUT with LED off */

    // IN B0 07.15.254 00/0000 E3 03 E2 00 05 :A_DomainAddress_Response (Domain Address=0005)
    tp1_data = { 0xB0, 0x7F, 0xFE, 0x00, 0x00, 0xE3, 0x03, 0xE2, 0x00, 0x05, 0 }; // A_DomainAddress_Response
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance : no response may be sent
    ASSERT_EQ(log, std::cend(busmoni.log));

    // For steps 3 to 4 : Switch on programming LED on BDUT
    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    /* 2.27.3 Read Domain Address with LED on */

    // IN B0 07.15.254 00/0000 E1 03 E1 :A_DomainAddress_Read()
    tp1_data = { 0xB0, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends the correct Domain Address.
    // OUT B0 01.01.001 00/0000 E3 03 E2 00 05 :A_DomainAddress_Response(Domain Address=0005)
    tp1_data = { 0xA0, 0x10, 0x01, 0x00, 0x00, 0xE3, 0x03, 0xE2, 0x00, 0x05, 0 }; // A_DomainAddress_Response // @note Control changed from 0xB0 to 0xA0. Src changed from 0x1101 to 0x1001.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.27.4 Send Response with LED on */

    // IN B0 07.15.254 00/0000 E3 03 E2 00 05 :A_DomainAddress_Response(Domain Address=0005)
    tp1_data = { 0xB0, 0x7F, 0xFE, 0x00, 0x00, 0xE3, 0x03, 0xE2, 0x00, 0x05, 0 }; // A_DomainAddress_Response
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: No response may be sent.
    ASSERT_EQ(log, std::cend(busmoni.log));
}

/**
 * 2.28 Testing of A_DomainAddressSelective_Read-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_28
 */
TEST(Application_Layer_Tests, Test_2_28)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Tests are applicable only for BDUT on open media with 2-octets domain address.
    // Prior to starting the test, set the Domain Address of the local device to 0002H and its individual address to 07.15.254
    bcu.group_address_table->individual_address = KNX::Individual_Address(0x7FFE);
    // The BDUT shall have the Domain Address 0005H and the individual address 01.00.001.
    bdut.interface_objects->device()->domain_address()->domain_address = 0x0005;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    // IN B0 07.15.254 00/0000 E6 03 E3 00 05 7F FE 03 :A_DomainAddressSelective_Read(Domain Address=0005, StartAddr=7FFE, Range=03)
    tp1_data = { 0xB0, 0x7F, 0xFE, 0x00, 0x00, 0xE6, 0x03, 0xE3, 0x00, 0x05, 0x7F, 0xFE, 0x03, 0 }; // A_DomainAddressSelective_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT sends a telegram with SystemID 0005H.
    // OUT B0 01.00.001 00/0000 E3 03 E2 00 05 :A_DomainAddress_Response (Domain Address=0005)
    tp1_data = { 0xA0, 0x10, 0x01, 0x00, 0x00, 0xE3, 0x03, 0xE2, 0x00, 0x05, 0 }; // A_DomainAddress_Response // @note Control changed from 0xB0 to 0xA0.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.29 Testing of A_UserMemory_Read-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_29
 */
TEST(Application_Layer_Tests, Test_2_29)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Assumed Memory Model :
    // 7FF0H to 7FFFH : accessible memory area filled with the following data : 11 22 33 44 55 66 77 88 99 AA BB CC DD EE FF and so forth
    // 8000H to 8FFFH : protected memory area
    bdut.user_memory[0x7FF0] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11 };

    /* 2.29.1 Legal Length - accessible Memory (10 bytes from 7FF0) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 64 42 C0 0A 7F F0 :A_UserMemory_Read (Count=0A, Addr=07FF0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x42, 0xC0, 0x0A, 0x7F, 0xF0, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT returns the correct data.
    // OUT BC 01.00.001 10.15.254 6E 42 C1 0A 7F F0 11 22 33 44 55 66 77 88 99 AA :A_UserMemory_Response(Count=0A, Addr=07FF0, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6E, 0x42, 0xC1, 0x0A, 0x7F, 0xF0, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.29.2 Legal Length - protected Memory (10 bytes from 8000H) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 64 42 C0 0A 80 00 :A_UserMemory_Read (Count=0A, Addr=08000)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x42, 0xC0, 0x0A, 0x80, 0x00, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT answers with an A_UserMemory_Response-PDU with no data and count set to zero.
    // OUT BC 01.00.001 10.15.254 64 42 C1 00 08 00 :A_UserMemory_Response (Count=00, Addr=00800, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC1, 0x00, 0x80, 0x00, 0 }; // A_UserMemory_Response // @note memory_address changed from 0x00800 to 0x08000.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.29.3 Legal length - partly protected Memory (2 bytes from 7FFF) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFe, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 64 42 C0 02 7F FF :A_UserMemory_Read (Count=02, Addr=07FFF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x42, 0xC0, 0x02, 0x7F, 0xFF, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT answers with an A_UserMemory_Response-PDU with no data and count set to zero.
    // OUT BC 01.00.001 10.15.254 64 42 C1 00 7F FF :A_UserMemory_Response (Count=00, Addr=07FFF, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC1, 0x00, 0x7F, 0xFF, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x00, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.29.4 Illegal Length - accessible Memory (13 bytes from 07FF0) */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFe, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 64 42 C0 0D 7F F0 :A_UserMemory_Read (Count=0D, Addr=07FF0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x42, 0xC0, 0x0D, 0x7F, 0xF0, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT answers with an A_UserMemory_Response-PDU with no data and count set to zero.
    // OUT BC 01.00.001 10.15.254 64 42 C1 00 7F F0 :A_UserMemory_Response (Count=00, Addr=07FF0, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC1, 0x00, 0x7F, 0xF0, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFe, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.30 Testing of A_UserMemory_Write-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_30
 */
TEST(Application_Layer_Tests, Test_2_30)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Assumed Memory Model:
    // 7FF0H to 7FFFH : accessible memory area filled with 11 22 33 44 55 66 77 88 99 AA BB CC DD EE FF and so forth
    // 8000H to 8FFFH : protected memory area
    bdut.user_memory[0x7FF0] = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x11 };

    /* 2.30.1 Legal length - accessible Memory - no Verify (10 bytes from 7FF0) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6E 42 C2 0A 7F F0 11 22 33 44 55 66 77 88 99 AA :A_UserMemory_Write(Count=0A, Addr=07FF0, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x42, 0xC2, 0x0A, 0x7F, 0xF0, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 }; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: After reading the written memory, the same data is returned by the BDUT as written)
    // IN BC 10.15.254 01.00.001 64 46 C0 0A 7F F0 :A_UserMemory_Read (Count=0A, Addr=07FF0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x46, 0xC0, 0x0A, 0x7F, 0xF0, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 6E 42 C1 0A 7F F0 11 22 33 44 55 66 77 88 99 AA :A_UserMemory_Response(Count=0A, Addr=07FF0, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6E, 0x42, 0xC1, 0x0A, 0x7F, 0xF0, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFe, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.30.2 Legal Length - partly protected Memory - no Verify (2 bytes from 7FFF) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 42 C2 02 7F FF 12 34 :A_UserMemory_Write(Count=02, Addr=07FFF, Data= 12 34)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x42, 0xC2, 0x02, 0x7F, 0xFF, 0x12, 0x34, 0 }; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: After reading the affected accessible memory area, a response shall be generated showing that data has not been modified.
    // IN BC 10.15.254 01.00.001 64 46 C0 01 7F FF :A_UserMemory_Read (Count=01, Addr=07FFF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x46, 0xC0, 0x01, 0x7F, 0xFF, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 65 42 C1 01 7F FF FF :A_UserMemory_Response(Count=01, Addr=07FFF, Data= FF)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x42, 0xC1, 0x01, 0x7F, 0xFF, 0x11, 0 }; // A_UserMemory_Response // @note Data at 0x7FFF is 0x11
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0};
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.30.3 Illegal Length - accessible Memory - no Verify (12 Bytes from 7FF0) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6F 42 C2 0C 7F F0 FF FF FF FF FF FF FF FF FF FF FF :A_UserMemory_Write(Count=0C, Addr=07FF0, Data= FF FF FF FF FF FF FF FF FF FF FF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x42, 0xC2, 0x0C, 0x7F, 0xF0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0 }; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: After reading the affected accessible memory area, a response shall be generated showing that data has not been modified.
    // IN BC 10.15.254 01.00.001 64 46 C0 0A 7F F0 :A_UserMemory_Read (Count=0A, Addr=07FF0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x46, 0xC0, 0x0A, 0x7F, 0xF0, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 6E 42 C1 0A 7F F0 11 22 33 44 55 66 77 88 99 AA :A_UserMemory_Response(Count=0A, Addr=07FF0, Data= 11 22 33 44 55 66 77 88 99 AA)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6E, 0x42, 0xC1, 0x0A, 0x7F, 0xF0, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.30.4 Legal Length - accessible Memory – Verify (10 bytes from 7FF0) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6E 42 C2 0A 7F F0 00 11 22 33 44 55 66 77 88 99 :A_UserMemory_Write(Count=0A, Addr=07FF0, Data= 00 11 22 33 44 55 66 77 88 99)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x42, 0xC2, 0x0A, 0x7F, 0xF0, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0 }; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT replies with a Response containing the same data as written.
    // OUT BC 01.00.001 10.15.254 6E 42 C1 0A 7F F0 00 11 22 33 44 55 66 77 88 99 :A_UserMemory_Response(Count=0A, Addr=07FF0, Data= 00 11 22 33 44 55 66 77 88 99)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6E, 0x42, 0xC1, 0x0A, 0x7F, 0xF0, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0}; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.30.5 Legal Length - protected Memory – Verify (10 bytes from 8000H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6E 42 C2 0A 80 00 00 11 22 33 44 55 66 77 88 99 :A_UserMemory_Write(Count=0A, Addr=08000, Data= 00 11 22 33 44 55 66 77 88 99)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x42, 0xC2, 0x0A, 0x80, 0x00, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0 }; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT replies with an A_UserMemory_Response-PDU with count set to zero and no data.
    // OUT BC 01.00.001 10.15.254 64 42 C1 00 80 00 :A_UserMemory_Response (Count=00, Addr=08000, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC1, 0x00, 0x80, 0x00, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.30.6 Legal Length - partly protected Memory – Verify (2 bytes from 7FFF) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 66 42 C2 02 7F FF 12 34 :A_UserMemory_Write(Count=02, Addr=07FFF, Data= 12 34)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x42, 0xC2, 0x02, 0x7F, 0xFF, 0x12, 0x34, 0 }; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT replies with an A_UserMemory_Response-PDU with count set to zero and no data.
    // OUT BC 01.00.001 10.15.254 64 42 C1 00 7F FF :A_UserMemory_Response (Count=00, Addr=07FFF, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC1, 0x00, 0x7F, 0xFF, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0  };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.30.7 Illegal Length - accessible Memory – Verify (12 bytes from 7FF0) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6F 42 C2 0C 7F F0 FF FF FF FF FF FF FF FF FF FF FF :A_UserMemory_Write(Count=0C, Addr=07FF0, Data= FF FF FF FF FF FF FF FF FF FF FF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x42, 0xC2, 0x0C, 0x7F, 0xF0, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0 }; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT replies with an A_UserMemory_Response-PDU with count set to zero and no data.
    // OUT BC 01.00.001 10.15.254 64 42 C1 00 7F F0 :A_UserMemory_Response (Count=00, Addr=07FF0, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC1, 0x00, 0x7F, 0xF0, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.31 Testing of A_UserMemoryBit_Write-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_31
 */
TEST(Application_Layer_Tests, Test_2_31)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Assumed Memory Model:
    // 7FF0H to 7FFFH : accessible memory area filled with OFH
    // 8000H to 8FFFH : protected memory area
    bdut.user_memory[0x7FF0] = { 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F };

    /* 2.31.1 Legal Length - accessible Memory - no Verify (5 bytes from 7FF0) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6E 42 C4 05 7F F0 33 33 33 33 33 55 55 55 55 55 :A_UserMemoryBit_Write(Count=05, Addr=07FF0, Data= 33 33 33 33 33 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x42, 0xC4, 0x05, 0x7F, 0xF0, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0 }; // A_UserMemoryBit_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: After reading the concerned memory area, the BDUT replies with a Response showing that the data has been manipulated.
    // IN BC 10.15.254 01.00.001 64 46 C0 05 7F F0 :A_UserMemory_Read (Count=05, Addr=07FF0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x46, 0xC0, 0x05, 0x7F, 0xF0, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 69 42 C1 05 7F F0 56 56 56 56 56 :A_UserMemory_Response(Count=05, Addr=07FF0, Data= 56 56 56 56 56)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x69, 0x42, 0xC1, 0x05, 0x7F, 0xF0, 0x56, 0x56, 0x56, 0x56, 0x56, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.31.2 Legal Length - partly protected Memory - no Verify (2 bytes from 7FFF) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 68 42 C4 02 7F FF 33 33 55 55 :A_UserMemoryBit_Write(Count=02, Addr=07FFF, Data= 33 33 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x68, 0x42, 0xC4, 0x02, 0x7F, 0xFF, 0x33, 0x33, 0x55, 0x55, 0 }; // A_UserMemoryBit_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 64 46 C0 01 7F FF :A_UserMemory_Read (Count=01, Addr=07FFF)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x46, 0xC0, 0x01, 0x7F, 0xFF, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 }; // @note control changed from 0xBC to 0xB0
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: After reading the first byte of the concerned memory area, the BDUT replies with a Response showing that the data has not been manipulated.
    // OUT BC 01.00.001 10.15.254 65 42 C1 01 7F FF 0F :A_UserMemory_Response(Count=01, Addr=07FFF, Data= 0F)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x42, 0xC1, 0x01, 0x7F, 0xFF, 0x0F, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.31.3 Illegal Length - accessible Memory - no Verify (6 bytes from 7FF0) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(false);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6C 42 C2 08 7F F0 0F 0F 0F 0F 0F 0F 0F 0F :A_UserMemory_Write(Count=08, Addr=07FF0, Data= 0F 0F 0F 0F 0F 0F 0F 0F) - reinstalling previous values
    tp1_data = { 0xBC, 0xAF, 0xFe, 0x10, 0x01, 0x6C, 0x42, 0xC2, 0x08, 0x7F, 0xF0, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0 }; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6C 46 C2 08 7F F9 0F 0F 0F 0F 0F 0F 0F 0F :A_UserMemory_Write(Count=08, Addr=07FF9, Data= 0F 0F 0F 0F 0F 0F 0F 0F) - reinstalling previous values
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6C, 0x46, 0xC2, 0x08, 0x7F, 0xF9, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0}; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6F 4A C4 06 7F F0 33 33 33 33 33 33 55 55 55 55 55 55: A_UserMemoryBit_Write(Count=06, Addr=07FF0, Data= 33 33 33 33 33 33 33 55 55 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x4A, 0xC4, 0x06, 0x7F, 0xF0, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0 }; // A_UserMemoryBit_Write // @note data reduced to 5 bytes to fit TP1 standard frame
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: After reading concerned memory area, the BDUT replies with an A_UserMemory_Response-PDU showing that the data has not been manipulated
    // IN BC 10.15.254 01.00.001 64 4E C0 06 7F F0 :A_UserMemory_Read (Count=06, Addr=07FF0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x64, 0x4E, 0xC0, 0x06, 0x7F, 0xF0, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 01.00.001 10.15.254 6A 42 C1 06 7F F0 0F 0F 0F 0F 0F 0F :A_UserMemory_Response(Count=06, Addr=07FF0, Data= 0F 0F 0F 0F 0F 0F)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6A, 0x42, 0xC1, 0x06, 0x7F, 0xF0, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0x0F, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.31.4 Legal Length - accessible Memory – Verify (5 bytes from 7FF0) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6E 42 C4 05 7F F0 33 33 33 33 33 55 55 55 55 55 :A_UserMemoryBit_Write(Count=05, Addr=07FF0, Data= 33 33 33 33 33 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x42, 0xC4, 0x05, 0x7F, 0xF0, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0 }; // A_UserMemoryBit_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT replies with an A_UserMemory_Response-PDU showing that the data has been manipulated
    // OUT BC 01.00.001 10.15.254 69 42 C1 05 7F F0 56 56 56 56 56 :A_UserMemory_Response(Count=05, Addr=07FF0, Data= 56 56 56 56 56)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x69, 0x42, 0xC1, 0x05, 0x7F, 0xF0, 0x56, 0x56, 0x56, 0x56, 0x56, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.31.5 Legal Length - protected Memory – Verify (5 bytes from 8000H) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6E 42 C4 05 80 00 33 33 33 33 33 55 55 55 55 55 :A_UserMemoryBit_Write(Count=05, Addr=08000, Data= 33 33 33 33 33 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x42, 0xC4, 0x05, 0x80, 0x00, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0 }; // A_UserMemoryBit_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT replies with an A_UserMemory_Response-PDU with a count set to zero and no data.
    // OUT BC 01.00.001 10.15.254 64 42 C1 00 80 00 :A_UserMemory_Response (Count=00, Addr=08000, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC1, 0x00, 0x80, 0x00, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.31.6 Legal Length - partly protected Memory – Verify (2 bytes from 7FFF) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 68 42 C4 02 7F FF 33 33 55 55 :A_UserMemoryBit_Write(Count=02, Addr=07FFF, Data= 33 33 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x68, 0x42, 0xC4, 0x02, 0x7F, 0xFF, 0x33, 0x33, 0x55, 0x55, 0 }; // A_UserMemoryBit_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT replies with an A_UserMemory_Response-PDU with a count set to zero and no data.
    // OUT BC 01.00.001 10.15.254 64 42 C1 00 7F FF :A_UserMemory_Response (Count=00, Addr=07FFF, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC1, 0x00, 0x7F, 0xFF, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* 2.31.7 Illegal Length - accessible Memory – Verify (6 bytes from 7FF0) */
    bdut.interface_objects->device()->device_control()->device_control.set_verify_mode_on(true);

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 6F 42 C4 06 7F F0 33 33 33 33 33 33 55 55 55 55 55 55 :A_UserMemoryBit_Write(Count=06, Addr=07FF0, Data= 33 33 33 33 33 33 33 55 55 55 55 55 55)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6E, 0x42, 0xC4, 0x06, 0x7F, 0xF0, 0x33, 0x33, 0x33, 0x33, 0x33, 0x55, 0x55, 0x55, 0x55, 0x55, 0}; // A_UserMemoryBit_Write // @note data reduced to 5 bytes to fit TP1 standard frame
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.00.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: The BDUT replies with an A_UserMemory_Response-PDU with a count set to zero and no data.
    // OUT BC 01.00.001 10.15.254 64 42 C1 00 7F F0 :A_UserMemory_Response (Count=00, Addr=07FF0, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC1, 0x00, 0x7F, 0xF0, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 2.32 Testing of A_UserManufacturerInfo_Read-Service : Server Test
 *
 * @ingroup KNX_08_03_07_02_32
 */
TEST(Application_Layer_Tests, Test_2_32)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* 2.32.1 Read Management Type */

    // IN B0 10.15.254 01.00.001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 61 42 C5 :A_UserManufacturerInfo_Read()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x42, 0xC5, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 01.01.001 10.15.254 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 }; // @note Src Addr changed from 0x1101 to 0x1001
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends a response containing manufacturer's code and type number according manufacturer's declarations
    // OUT BC 01.01.001 10.15.254 64 42 C6 12 12 34 :A_UserManufacturerInfo_Response (Code=12, Type Number=1234)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x64, 0x42, 0xC6, 0x12, 0x12, 0x34, 0 }; // @note Src Addr changed from 0x1101 to 0x1001
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC 10.15.254 01.00.001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 10.15.254 01.00.001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 3 Network Management Client */

/* 3.2 Testing of AL-Services */

/**
 * 3.2.1 A_NetworkParameter_Read - Client Tests
 *
 * @ingroup KNX_08_03_07_03_02_01
 */
TEST(Application_Layer_Tests, Test_3_2_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: Stimulate BDUT according to manufacturer’s information in datasheet to send an
    // A_NetworkParameter_Read for Subnet Address
    const KNX::ASAP_Individual asap{KNX::Individual_Address(0), false};
    const KNX::Parameter_Type parameter_type{KNX::OT_DEVICE, KNX::Device_Object::PID_SUBNET_ADDR};
    bdut.application_layer.A_NetworkParameter_Read_req(asap, KNX::Comm_Mode::Broadcast, KNX::Network_Layer_Parameter, parameter_type, KNX::Priority::low, {0x00}, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();

    // Acceptance: BDUT sends SNA Read frame:
    // Test frame (OUT): A_NetworkParameter_Read (DO; PID_Subnet_Addr, Test_Info=00h), with hop count = 0
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE5, 0x03, 0xDA, 0x00, 0x00, 0x39, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 3.2.2 A_NetworkParameter_Response - Client Tests
 *
 * @ingroup KNX_08_03_07_03_02_02
 */
TEST(Application_Layer_Tests, Test_3_2_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->device()->subnetwork_address()->subnetwork_address = 0x10;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* 3.2.2.2 Example: Response to a subnet Address Read */

    // Stimuli: Stimulate BDUT according to manufacturer’s information in datasheet to send an
    // A_NetworkParameter_Read for Subnet Address and use a Telegram Generator to send an SNA Response
    // before BDUT´s response timeout has elapsed.
    const KNX::ASAP_Individual asap{KNX::Individual_Address(0), false};
    const KNX::Parameter_Type parameter_type{KNX::OT_DEVICE, KNX::Device_Object::PID_SUBNET_ADDR};
    bdut.application_layer.A_NetworkParameter_Read_req(asap, KNX::Comm_Mode::Broadcast, KNX::Network_Layer_Parameter, parameter_type, KNX::Priority::low, {0x00}, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();

    // Test frame (OUT): A_NetworkParameter_Read (DO; PID_Subnet_Addr, Test_Info=00h), with hop count = 0
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE5, 0x03, 0xDA, 0x00, 0x00, 0x39, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Test frame (IN): A_NetworkParameter_Response (DO; PID_Subnet_Addr, Test_Info=00h; Test_Result = new SNA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x03, 0xDB, 0x00, 0x00, 0x39, 0x00, 0xFF, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    ASSERT_EQ(bdut.interface_objects->device()->subnetwork_address()->subnetwork_address, 0xFF);

    // Acceptance:
    // BDUT has updated its Subnet Address:

    // Test frame (IN): send Property Read to PID_Subnet_Addr (DO)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x39, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Test frame (OUT): Property Response, value of SNA updated
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x39, 0x10, 0x01, 0xFF, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Use a Telegram Generator to send an SNA Response to BDUT, without prior SNA Read from
    // BDUT elapsed.

    // Test frame (IN): A_NetworkParameter_Response (DO; PID_Subnet_Addr, Test_Info=00h; Test_Result = new SNA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x03, 0xDB, 0x00, 0x00, 0x39, 0x00, 0x10, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance:
    // BDUT has updated its Subnet Address:

    // Test frame (IN): send Property Read to PID_Subnet_Addr (DO)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x39, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Test frame (OUT): Property Response, value of SNA updated
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x39, 0x10, 0x01, 0x10, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 3.2.3 A_NetworkParameter_Write - Client Tests
 *
 * @ingroup KNX_08_03_07_03_02_03
 */
TEST(Application_Layer_Tests, Test_3_2_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: Stimulate BDUT according to manufacturer’s information in datasheet to send an
    // A_NetworkParameter_Write frame
    const KNX::ASAP_Individual asap{KNX::Individual_Address(0), false};
    const KNX::Parameter_Type parameter_type{KNX::OT_DEVICE, KNX::Device_Object::PID_SUBNET_ADDR};
    bdut.application_layer.A_NetworkParameter_Write_req(asap, KNX::Comm_Mode::Broadcast, KNX::Network_Layer_Parameter, parameter_type, KNX::Priority::low, {0x00}, [](const KNX::Status a_status){
        // Lcon
    });
    io_context.poll();

    // Acceptance: BDUT´s sends network parameter read frame with correct service parameters
    // and hop count

    // Test frame (OUT): A_NetworkParameter_Write (service parameters), with correct hop count
    // (hop count is depending on network parameter to be written!)
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE5, 0x03, 0xE4, 0x00, 0x00, 0x39, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 4 Testing of Device Management Services */

/**
 *  4.1 Switch Programming Mode
 *
 * @ingroup KNX_08_03_07_04_01
 */
TEST(Application_Layer_Tests, Test_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->device()->subnetwork_address()->subnetwork_address = 0x10;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: Property Value Write to BDUT (Device Object, PID_ProgMode) with value ON:

    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=54d; Count =1; Start=001; data = 01h)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x36, 0x03, 0xD7, 0x00, 0x36, 0x10, 0x01, 0x81, 0 }; // @note data changed from 0x01 to 0x81, to set correct parity
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    ASSERT_EQ(bdut.interface_objects->device()->programming_mode()->prog_mode, true);

    // Acceptance: BDUT sends A_PropertyValue_Response with programming mode set to ON (in data field). BDUT´s Individual Address can be read.

    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=54d; Count =1; Start=001; data = 01h)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x36, 0x10, 0x01, 0x81, 0 }; // @note data changed from 0x01 to 0x81, to set correct parity
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Test frame (IN): A_IndividualAddress_Read
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Test frame (OUT): A_IndividualAddress_Response, BDUT sends its Individual Address
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 }; // A_IndividualAddress_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Stimuli: Set programming mode to OFF:

    // Test frame (IN): A_PropertyValue_Write (ObjIndex=0; PropID=54d; Count =1; Start=001; data = 00h)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x36, 0x03, 0xD7, 0x00, 0x36, 0x10, 0x01, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    ASSERT_EQ(bdut.interface_objects->device()->programming_mode()->prog_mode, false);

    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=54d; Count =1; Start=001; data = 00h)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x03, 0xD6, 0x00, 0x36, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * 4.2 Testing of Load State Machine
 *
 * @ingroup KNX_08_03_07_04_02
 */
TEST(Application_Layer_Tests, Test_4_2)
{
    // @note Test Suite Supplement G “Load Controls” – Testing for Device Model 0300
}

/**
 * 4.3 Testing of Run State Machine
 *
 * @ingroup KNX_08_03_07_04_03
 */
TEST(Application_Layer_Tests, Test_4_3)
{
    // @note Test Suite Supplement I “Run State Machines”
}

/* 5 Device Individualisation */

/**
 * 5.1 Programming Mode & Localisation via Programmiong Mode
 *
 * @ingroup KNX_08_03_07_05_01
 */
TEST(Application_Layer_Tests, Test_5_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->device()->subnetwork_address()->subnetwork_address = 0x10;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Try to read Address with Programming Mode off

    // Test frame (IN): A_IndividualAddress_Read
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // no reaction

    // Read Address with Programming Mode on

    bdut.interface_objects->device()->programming_mode()->prog_mode = true;

    // Test frame (IN): A_IndividualAddress_Read
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Test frame (OUT): A_IndividualAddress_Response, BDUT sends its Individual Address
    tp1_data = { 0xBC, 0x10, 0x01, 0x00, 0x00, 0xE1, 0x01, 0x40, 0 }; // A_IndividualAddress_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @note Check of programming mode is covered sufficiently with the tests described in clause 4.1 in this
    // document. The tests in clause 4.1use the management service for switching of BDUT´s programming
    // mode,
}

/**
 * 5.2 Serial Number
 *
 * @ingroup KNX_08_03_07_05_02
 */
TEST(Application_Layer_Tests, Test_5_2)
{
    // @note Check of device individualisation by serial number is covered sufficiently with the tests according clause
    // clause 2.16 in this document.
}

/**
 * 5.3 Domain Address Assignment
 *
 * @ingroup KNX_08_03_07_05_03
 */
TEST(Application_Layer_Tests, Test_5_3)
{
    // To be completed
}

/**
 * 5.4 Local Assignment of Individual Address
 *
 * @ingroup KNX_08_03_07_05_04
 */
TEST(Application_Layer_Tests, Test_5_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->device()->subnetwork_address()->subnetwork_address = 0x10;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    // std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Stimuli: Change BDUT´s Individual Address via BDUT´s HMI
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    // Acceptance: BDUT changes its Individual Address
    ASSERT_EQ(bdut.interface_objects->group_address_table()->individual_address, 0x1101);
}

/**
 * 5.5 Distributed Address Assignment (DAA)
 *
 * @ingroup KNX_08_03_07_05_05
 */
TEST(Application_Layer_Tests, Test_5_5)
{
    // to be completed
}

/**
 * 5.6 Subnet Address Assignment
 *
 * @ingroup KNX_08_03_07_05_06
 */
TEST(Application_Layer_Tests, Test_5_6)
{
    // @note Check of Subnet Address Assignment is covered sufficiently with the tests described clause 2.18 and
    // 3.2.1/3.2.2 in this document.
}

/**
 * 6 Verification of implemented Interface Object and Properties
 *
 * @ingroup KNX_08_03_07_06
 */
TEST(Application_Layer_Tests, Test_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);
    bdut.interface_objects->device()->subnetwork_address()->subnetwork_address = 0x10;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    // std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // For those mandatory properties that can be written, random tests shall be performed to check whether new
    // values are accepted by the BDUT.

    // It shall moreover be checked that the Interface Object index start with 0 and are consecutively numbered.

    // @todo Test_6

    // TP1_BUC2 has implemented
    // - 0 OT_DEVICE
    //   - 1 PID_OBJECT_TYPE
    //   - 2 PID_OBJECT_NAME
    //   - 8 PID_SERVICE_CONTROL (write_enable)
    //   - 9 PID_FIRMWARE_REVISION
    //   - 11 PID_SERIAL_NUMBER
    //   - 12 PID_MANUFACTURER_ID
    //   - 14 PID_DEVICE_CONTROL (write_enable)
    //   - 15 PID_ORDER_INFO
    //   - 25 PID_VERSION
    //   - 51 PID_ROUTING_COUNT (write_enable)
    //   - 52 PID_MAX_RETRY_COUNT (write_enable)
    //   - 54 PID_PROGMODE (write_enable)
    //   - 55 PID_PRODUCT_ID
    //   - 56 PID_MAX_APDU_LENGTH
    //   - 67 PID_PSU_TYPE
    //   - 68 PID_PSU_STATUS
    //   - 69 PID_PSU_ENABLE (write_enable)
    //   - 70 PID_DOMAIN_ADDRESS (write_enable)
    //   - 83 PID_DEVICE_DESCRIPTOR
    // - 1 OT_ADDRESS_TABLE
    //   - 1 PID_OBJECT_TYPE
    //   - 2 PID_OBJECT_NAME
    //   - 5 PID_LOAD_STATE_CONTROL (write_enable)
    //   - 23 PID_TABLE
    // - 2 OT_ASSOCIATION_TABLE
    //   - 1 PID_OBJECT_TYPE
    //   - 2 PID_OBJECT_NAME
    //   - 5 PID_LOAD_STATE_CONTROL (write_enable)
    //   - 23 PID_TABLE
    // - 3 Application Program
    //   - 1 PID_OBJECT_TYPE
    //   - 2 PID_OBJECT_NAME
    //   - 5 PID_LOAD_STATE_CONTROL (write_enable)
    //   - 6 PID_RUN_STATE_CONTROL (write_enable)
    //   - 13 PID_PROGRAM_VERSION
    // - 9 OT_GROUP_OBJECT_TABLE
    //   - 1 PID_OBJECT_TYPE
    //   - 2 PID_OBJECT_NAME
    //   - 5 PID_LOAD_STATE_CONTROL (write_enable)
    //   - 51 PID_GRP_OBJTABLE
    //   - 52 PID_EXT_GRPOBJREFERENCE
}

/* 7 Test of Address Table */

/* 7.1 General */

/**
 * 7.2 Server Tests
 *
 * @ingroup KNX_08_03_07_07_02
 */
TEST(Application_Layer_Tests, Test_7_2)
{
    // See Link Layer Tests
}

/**
 * 7.3 Client Tests
 *
 * @ingroup KNX_08_03_07_07_03
 */
TEST(Application_Layer_Tests, Test_7_3)
{
    // Make download of Address Table with client and record the frames with EITT
    // Compare the trace of EITT with the relevant management procedures in the Resource document
    // Read out Server device and check the contents of the Group Address Table with the reference. Check sorting of the addresses.

    // @todo Test_7_3
}

/* 8 Test of structure of Association Table */

/* 8.1 General */

/**
 * 8.2 Server Tests
 *
 * @ingroup KNX_08_03_07_08_02
 */
TEST(Application_Layer_Tests, Test_8_2)
{
    // 8.2.1 Receiving telegrams

    // Test 1 – 1 to 1 relation – Stimulate the test object by sending a telegram on the group address
    // linked to it. Check reaction via the application.

    // Test 2 – 1 to n relation - as Test 1 Stimulate all test objects by sending a telegram on the group
    // address linked to it. Check reaction via the application.

    // Test 3 –n to 1 relation - as Test 1 - Stimulate the test object by sending a telegram on the
    // group addresses linked to it. Check reaction via the application.

    // Test 4 –n to n relation (optional) - as Test 1 - check the exact reaction of all linked objects

    // 8.2.2 Sending telegrams

    // Test 1 – 1 to 1 relation – Stimulate the application to send one telegram on the sending group
    // address.

    // Test 2 – 1 to n relation - as Test 1 – additional reaction on all linked group objects.

    // Test 3 –n to 1 relation - as Test 1 – additionally no telegram on any other linked group
    // address.

    // @todo Test_8_2
}

/**
 * 8.3 Client Tests
 *
 * @ingroup KNX_08_03_07_08_03
 */
TEST(Application_Layer_Tests, Test_8_3)
{
    // Make download of Association Table with client and record the frames with EITT
    // Compare the trace of EITT with the relevant management procedures in the Resource document
    // Read out Server device and check the contents of the Association Table with the reference.

    // @todo Test_8_3
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
