// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/02/02_Twisted_Pair_1.h>
#include <KNX/03/03/03_Network_Layer.h>
#include <KNX/03/03/04_Transport_Layer.h>
#include <KNX/03/03/07_Application_Layer.h>
#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/06/03/cEMI.h>
#include <KNX/03/06/03/cEMI/CEMI_Server.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

class Network_Layer_Tests : public ::testing::Test
{
    virtual ~Network_Layer_Tests() = default;
};

/** BCU that can be used as Bus Device Under Test (BDUT) */
class BCU
{
public:
    BCU(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        interface_objects(std::make_shared<KNX::Interface_Objects>()),
        current_level(std::make_shared<KNX::Level>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context),
        cemi_server()
    {
        // interface objects
        std::shared_ptr<KNX::Device_Object> device_object = interface_objects->device();
        (void) device_object->interface_object_type();
        (void) device_object->device_control();
        (void) device_object->routing_count();
        std::shared_ptr<KNX::Group_Address_Table> group_address_table = interface_objects->group_address_table();
        (void) group_address_table->interface_object_type();
        group_address_table->table()->description.write_enable = true;
        group_address_table->table()->description.property_datatype = KNX::PDT_GENERIC_02; // group address
        group_address_table->table()->description.max_nr_of_elem = 0x02; // two group addresses sufficient for the tests
        std::shared_ptr<KNX::Router_Object> router_object = interface_objects->router();
        (void) router_object->routing_table_control();
        std::shared_ptr<KNX::CEMI_Server_Object> cemi_server_object = interface_objects->cemi_server();
        (void) cemi_server_object->interface_object_type();
        (void) cemi_server_object->medium_type();
        cemi_server_object->communication_mode()->communication_mode.field1 = 0x00; // Data Link Layer
        cemi_server_object->communication_mode()->description.write_enable = true;
        (void) cemi_server_object->link_layer_transparency_control();

        // current level
        *current_level = 15;

        // TP1 Physical Layer simulation
        tp1_physical_layer.connect(tp1_bus_simulation);

        // TP1 Data Link Layer
        tp1_data_link_layer.device_object = device_object;
        tp1_data_link_layer.group_address_table = group_address_table;

        // cEMI Server
        cemi_server.connect(tp1_data_link_layer);
        cemi_server.cemi_con = std::bind(&BCU::local_cemi_con, this, std::placeholders::_1);
        cemi_server.cemi_ind = std::bind(&BCU::local_cemi_ind, this, std::placeholders::_1);

        // cEMI Management Server
        cemi_server.management_server.interface_objects = interface_objects;
        cemi_server.management_server.current_level = current_level;
    }

    /** resources */
    std::shared_ptr<KNX::Interface_Objects> interface_objects{};

    /** current level */
    std::shared_ptr<KNX::Level> current_level{};

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** cEMI Server */
    KNX::CEMI_Server cemi_server;

    /** PEI Data con */
    std::deque<std::vector<uint8_t>> pei_data_con{};

    /** PEI Data ind */
    std::deque<std::vector<uint8_t>> pei_data_ind{};

protected:
    void local_cemi_con(const std::vector<uint8_t> data) {
        pei_data_con.push_back(data);
    }

    void local_cemi_ind(const std::vector<uint8_t> data) {
        pei_data_ind.push_back(data);
    }
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class BCU_Plain_Data
{
public:
    explicit BCU_Plain_Data(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        device_object(std::make_shared<KNX::Device_Object>()),
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context)
    {
        tp1_physical_layer.connect(tp1_bus_simulation);

        tp1_data_link_layer.device_object = device_object;
        tp1_data_link_layer.group_address_table = group_address_table;
        tp1_data_link_layer.L_Busmon_ind = std::bind(&BCU_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** device object */
    std::shared_ptr<KNX::Device_Object> device_object;

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/* 10 Testing of cEMI */

/* 10.1 Testing of Busmonitor and Raw Services */

/**
 * L_Busmon
 *
 * @see KNX_08_06_03_02_02
 *
 * @ingroup KNX_08_06_03_10_01_01
 */
TEST(CEMI_Tests, Test_10_1_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;
    bdut.tp1_data_link_layer.group_address_table->individual_address = 0x1000;

    BCU_Plain_Data bcu1(io_context, tp1_bus);
    bcu1.group_address_table->individual_address = KNX::Individual_Address(0x1001);
    bcu1.group_address_table->group_addresses.insert(KNX::Group_Address(0x2000));

    BCU_Plain_Data bcu2(io_context, tp1_bus);
    bcu2.group_address_table->individual_address = KNX::Individual_Address(0x1002);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Set the BDUT in busmonitor-mode. */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01, 0x01 }; // M_PropWrite.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();
    ASSERT_EQ(bdut.cemi_server.management_server.interface_objects->cemi_server()->communication_mode()->communication_mode.field1, 0x01); // Data Link Layer Busmonitor
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    // @todo This should be set automatically according to cemi_server->communication_mode.
    bdut.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    ASSERT_EQ(bdut.tp1_data_link_layer.mode, KNX::Data_Link_Layer::Mode::Busmonitor);

    /* Step 1 (indirect) */

    /* Send a frame on the bus that is not acknowledged. */
    tp1_data = { 0xBC, 0x10, 0x02, 0x20, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu2.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT sends the L_Busmon.ind specified below */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Busmon_ind), 0x07, 0x03, 0x01, 0x00, 0x04, 0x02, 0x00, 0x00, 0xBC, 0x10, 0x02, 0x20, 0x01, 0xE1, 0x00, 0x00, 0 }; // L_Busmon.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 2 (indirect) */

    /* Send a frame on the bus that is acknowledged. */
    tp1_data = { 0xBC, 0x10, 0x02, 0x20, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu2.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the two L_Busmon.ind specified below */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Busmon_ind), 0x07, 0x03, 0x01, 0x00, 0x04, 0x02, 0x00, 0x00, 0xBC, 0x10, 0x02, 0x20, 0x00, 0xE1, 0x00, 0x00, 0 }; // L_Busmon.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Busmon_ind), 0x07, 0x03, 0x01, 0x00, 0x04, 0x02, 0x00, 0x00, 0xCC }; // L_Busmon.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * L_Raw
 *
 * @see KNX_08_06_03_02_03
 *
 * @ingroup KNX_08_06_03_10_01_02
 */
TEST(CEMI_Tests, Test_10_1_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;
    bdut.tp1_data_link_layer.group_address_table->individual_address = 0xAFFE;

    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Set the BDUT back to Link Layer-mode. */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::M_PropWrite_req), 0x00, 0x08, 0x01, 0x34, 0x10, 0x01, 0x00 }; // M_PropWrite.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();
    //ASSERT_EQ(bdut.tp1_data_link_layer.mode, KNX::Data_Link_Layer::Mode::Normal);
    ASSERT_EQ(bdut.cemi_server.management_server.interface_objects->cemi_server()->communication_mode()->communication_mode.field1, 0x00); // Data Link Layer
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::M_PropWrite_con), 0x00, 0x08, 0x01, 0x34, 0x10, 0x01 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 1 (direct) */

    /* Send an L_Raw.req message to the BDUT */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Raw_req), 0x00, 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0x60, 0xC2, 0 }; // L_Raw.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the corresponding frame on the bus */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT sends the following L_Raw.con, after the frame is sent completely on the bus */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Raw_con), 0x00, 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0x60, 0xC2, 0 }; // L_Raw.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (indirect) */

    // @todo We have to switch DLL to Busmonitor to receive L_Raw.ind.
    bdut.tp1_data_link_layer.mode = KNX::TP1_Data_Link_Layer::Mode::Busmonitor;
    // @note We have to switch communication_mode from Data Link Layer (0) to Data Link Layer Raw Frames (2) to receive L_Raw.ind.
    bdut.cemi_server.management_server.interface_objects->cemi_server()->communication_mode()->communication_mode.field1 = 0x02; // Data Link Layer Raw Frames

    /* Send a frame on the bus. */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT sends the following L_Raw.ind when it sees a frame on the bus */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Raw_ind), 0x00, 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0x60, 0xC2, 0 }; // L_Raw.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * Additional Information
 *
 * @ingroup KNX_08_06_03_10_01_03
 */
TEST(CEMI_Tests, Test_10_1_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    // @todo Test_10_1_3

    /* Test Step 1 (indirect) */

    /* Set BDUT to the communication mode (Busmonitor Mode or Raw Mode) needed for the test (direct).
     * Then send frames (indirect) on the bus that are reported by BDUT on its cEMI interface. */
    bdut.cemi_server.management_server.interface_objects->cemi_server()->communication_mode()->communication_mode.field1 = 0x01; // Data Link Layer Busmonitor
    //bdut.cemi_server.management_server.interface_objects->cemi_server()->communication_mode()->communication_mode.field1 = 0x02; // Data Link Layer Raw Frames

    /* The BDUT sends cEMI frames (with L_Busmon.ind or L_Raw.ind, according the current communication
     * mode) with the expected additional information. */

    /* Test Step 2 (direct) */

    /* Send L_Raw.req messages to the BDUT with the additional information type to be tested. */

    /* The BDUT sends the frames to the bus as expected, e.g. with time delay. */

    /* Test Step 3 (direct) */

    /* Send L_Raw.req messages to the BDUT with unsupported additional information type(s) */

    /* The BDUT ignores the received additional information and sends the frames to the bus independent of the
     * (unsupported) additional information given in the cEMI frame. */
}

/* 10.2 Testing of Link Layer services */

/**
 * L_Data
 *
 * @see KNX_08_06_03_03_01_02
 *
 * @ingroup KNX_08_06_03_10_02_01
 */
TEST(CEMI_Tests, Test_10_2_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Link Layer as working layer for the BDUT. */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01, 0x00 }; // M_PropWrite.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();
    ASSERT_EQ(bdut.cemi_server.management_server.interface_objects->cemi_server()->communication_mode()->communication_mode.field1, 0x00); // Data Link Layer
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Install a device in the test set-up acknowledging only the Group Address FFFFh. */
    BCU_Plain_Data bcu2(io_context, tp1_bus);
    bcu2.group_address_table->group_addresses.insert(KNX::Group_Address(0xFFFF));

    /* Load the BDUT with the following address table: 02h, 1000h, FFFEh. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xFFFE));

    /* Step 1 (direct) */

    /* The BDUT receives a L_Data.req, e.g. Group address type - APCI=A_GroupValue_Read */
    pei_data = { 0x11, 0x00, 0xBC, 0xE0, 0x00, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // L_Data.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus corresponding to the L_Data.req */
    /* The "Source Address" shall be the individual address of the BDUT. */
    tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_Data.con when it receives an IACK on the bus */
    pei_data = { 0x2E, 0x00, 0xBC, 0xE0, 0x10, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // L_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* The BDUT receives an L_Data.req, e.g. Group address type - APCI=ValueWrite */
    pei_data = { 0x11, 0x00, 0xBC, 0xE0, 0x00, 0x00, 0xFF, 0xFF, 0x0F, 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E }; // L_Data.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT transmits the data packet on the KNX bus corresponding to the L_Data.req */
    /* The "Source Address" shall be the individual address of the BDUT. */
    tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xEF, 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an according L_Data.con service. */
    pei_data = { 0x2E, 0x00, 0xBC, 0xE0, 0x10, 0x00, 0xFF, 0xFF, 0x0F, 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E }; // L_Data.con // @todo 0xEF changed to 0x0F
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (direct) */

    /* The BDUT receives a L_Data.req, e.g. Group address type - APCI=A_GroupValue_Read - System
     * priority */
    pei_data = { 0x11, 0x00, 0x90, 0xE0, 0x00, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // L_Data.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT transmits the data packet on the KNX bus corresponding to the L_Data.req */
    tp1_data = { 0xB0, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an according L_Data.con service */
    pei_data = { 0x2E, 0x00, 0xB0, 0xE0, 0x10, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // L_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 4 (direct) */

    /* The BDUT receives a L_Data.req, e.g. Group address type - APCI=A_GroupValue_Read, with Repeat-
     * Flag set to "repetitions" */
    pei_data = { 0x11, 0x00, 0xBC, 0xE0, 0x00, 0x00, 0x12, 0x34, 0x01, 0x00, 0x00 }; // L_Data.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT transmits the data packet on the KNX bus corresponding to the L_Data.req, with repetitions */
    tp1_data = { 0xBC, 0x10, 0x00, 0x12, 0x34, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT sends the following L_Data.con */
    pei_data = { 0x2E, 0x00, 0xBC, 0xE0, 0x10, 0x00, 0x12, 0x34, 0x01, 0x00, 0x00 }; // L_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 5 (direct) */

    /* The BDUT receives a L_Data.req, e.g. Group address type -
     * APCI=A_GroupPropValue_InfoReportValue_Read, with Repeat-Flag set to "no repetitions" */
    pei_data = { 0x11, 0x00, 0x14, 0xE4, 0x00, 0x00, 0x04, 0x11, 0x07, 0x07, 0xEB, 0x00, 0x64, 0x01, 0x33, 0x02, 0x00 }; // L_Data.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT transmits the data packet on the KNX bus corresponding to the L_Data.req (without
     * repetitions) */
    tp1_data = { 0x34, 0xE4, 0x10, 0x00, 0x04, 0x11, 0x07, 0x07, 0xEB, 0x00, 0x64, 0x01, 0x33, 0x02, 0x00, 0 }; // A_GroupPropValue_InfoReportValue_Read
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT sends the following L_Data.con */
    pei_data = { 0x2E, 0x00, 0x34, 0xE4, 0x10, 0x00, 0x04, 0x11, 0x07, 0x07, 0xEB, 0x00, 0x64, 0x01, 0x33, 0x02, 0x00 }; // L_Data.con // @todo 0x15 changed to 0x34
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 6 (direct) */

    /* Configure a bus device, so that it generates "BUSY" signals for all received data packets on the KNX bus. */

    /* The BDUT receives an L_Data.req, e.g. Group address type - APCI=A_GroupValue_Read, with Repeat-
     * Flag set to "repetitions" */
    pei_data = { 0x11, 0x00, 0xBC, 0xE0, 0x00, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // L_Data.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read // @note 0x01 changed to 0xE1
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_Data.con (and a corresponding frame on the bus plus repetitions) */
    pei_data = { 0x2E, 0x00, 0xBC, 0xE0, 0x10, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // @todo 0xBD changed to 0xBC due to missing repetitions
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 7 (direct) */

    /* Configure a bus device, so that it generates "BUSY" signals for all received data packets on the KNX bus. */

    /* The BDUT receives an L_Data.req, e.g. Group address type - APCI=A_GroupPropValue_InfoReport,
     * with Repeat-Flag set to "no repetitions" */
    pei_data = { 0x11, 0x00, 0x14, 0xE4, 0x00, 0x00, 0x04, 0x11, 0x07, 0x07, 0xEB, 0x00, 0x64, 0x01, 0x33, 0x02, 0x00 }; // L_Data.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    tp1_data = { 0x34, 0xE4, 0x10, 0x00, 0x04, 0x11, 0x07, 0x07, 0xEB, 0x00, 0x64, 0x01, 0x33, 0x02, 0x00, 0 }; // A_GroupPropValue_InfoReport // @todo 0x15 changed to 0x34
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT sends the following L_Data.con (and an according frame on the bus, without repetitions) */
    pei_data = { 0x2E, 0x00, 0x34, 0xE4, 0x10, 0x00, 0x04, 0x11, 0x07, 0x07, 0xEB, 0x00, 0x64, 0x01, 0x33, 0x02, 0x00 }; // L_Data.con // @todo 0x11 changed to 0x2E, 0x15 changed to 0x34
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 8 (indirect) */

    /* The BDUT receives a data packet, which is acknowledged by BDUT with an IACK */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data.ind */
    pei_data = { 0x29, 0x00, 0xBC, 0xE0, 0xAF, 0xFE, 0xFF, 0xFE, 0x01, 0x00, 0x80 }; // L_Data.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 9 (indirect) */

    /* The BDUT receives a repeated data packet with system priority (which is acknowledged by an IACK) */
    tp1_data = { 0x90, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data.ind */
    pei_data = { 0x29, 0x00, 0xB0, 0xE0, 0xAF, 0xFE, 0xFF, 0xFE, 0x01, 0x00, 0x80 }; // L_Data.ind // @note 0x90 changed to 0xB0.
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 10 (indirect) */

    /* The BDUT receives a data packet with the maximum value length (which is acknowledged by an IACK) */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xEF, 0x00,
                 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0
               }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data.ind */
    pei_data = { 0x29, 0x00, 0xBC, 0xE0, 0xAF, 0xFE, 0xFF, 0xFE, 0x0F, 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E }; // L_Data.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT receives a data packet with the maximum value length (which is acknowledged by an IACK) */
    tp1_data = { 0x3C, 0xE0, 0xAF, 0xFE, 0xFF, 0xFE, 0x37, 0x00,
                 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10,
                 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20,
                 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x28, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30,
                 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0
               }; // @note 0x37 added to extend length=0x37=55
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data.ind */
    pei_data = { 0x29, 0x00, 0x3C, 0xE0, 0xAF, 0xFE, 0xFF, 0xFE, 0x37, 0x00, // @note 0x2fff changed to 0xaffe
                 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10,
                 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F, 0x20,
                 0x21, 0x22, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28, 0x28, 0x2A, 0x2B, 0x2C, 0x2D, 0x2E, 0x2F, 0x30,
                 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37
               };
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 11 (indirect) */

    /* The following data packet is sent on the KNX bus (example for a Group Addressed frame) */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT does not transmit an L_Data.ind */
    ASSERT_EQ(pei_data_ind, bdut.pei_data_ind.cend());
}

/**
 * L_PollData
 *
 * @see KNX_08_06_03_03_02_02
 *
 * @ingroup KNX_08_06_03_10_02_02
 */
TEST(CEMI_Tests, Test_10_2_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Select the Link Layer as working layer for the BDUT. */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01, 0x00 }; // M_PropWrite.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();
    ASSERT_EQ(bdut.cemi_server.management_server.interface_objects->cemi_server()->communication_mode()->communication_mode.field1, 0x00); // Data Link Layer
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Load the BDUT with the following address table: 01h,1000h */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Install a device in the test set-up with polling group FFFEh and answer slot 02h, Data=01h. */
    BCU poll_slave(io_context, tp1_bus);
    poll_slave.interface_objects->device()->polling_group_settings()->polling_group_address = 0xFFFE;
    poll_slave.interface_objects->device()->polling_group_settings()->polling_slot_number = 0x02;
    poll_slave.tp1_data_link_layer.poll_data = 0x01;

    /* Step 1 (direct) */

    /* The BDUT receives a L_Poll_Data.req, number of slots requested = 05h */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Poll_Data_req),  0x00, 0xF0, 0x00, 0x00, 0x00, 0xFF, 0xFE, 0x05 };
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus and fills all unanswered slots with FEh */
    /* The "Source Address" shall be the individual address of the BDUT. */
    tp1_data = { 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0, 0xFE, 0xFE, 0x01, 0xFE, 0xFE };
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_Poll_Data.con */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Poll_Data_con), 0x00, 0xF0, 0x00, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0xFE, 0xFE, 0x01, 0xFE, 0xFE }; // @note hop_count changed from 0x00 to ox60
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* The BDUT receives an L_Poll_Data.req, number of slots requested = 0Fh */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Poll_Data_req), 0x00, 0xF0, 0x00, 0x00, 0x00, 0xFF, 0xFE, 0x0F };
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus and fills all unanswered slots with FEh */
    /* The "Source Address" shall be the individual address of the BDUT. */
    tp1_data = { 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x0F, 0, 0xFE, 0xFE, 0x01, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE };
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_Poll_Data.con */
    pei_data = { static_cast<uint8_t>(KNX::CEMI_Message_Code::L_Poll_Data_con), 0x00, 0xF0, 0x00, 0x10, 0x00, 0xFF, 0xFE, 0x0F, 0xFE, 0xFE, 0x01, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE };
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * Additional Information
 *
 * @ingroup KNX_08_06_03_10_02_03
 */
TEST(CEMI_Tests, Test_10_2_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x02FF);

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Test Step 1 (indirect) */

    /* Set BDUT to Link Layer communication mode (direct). Then send frames (indirect) on the bus that are
     * reported by BDUT on its cEMI interface. */
    bdut.interface_objects->cemi_server()->communication_mode()->communication_mode.field1 = 0x00; // Data Link Layer

    /* The BDUT sends cEMI frames with the expected additional information. */

    /* Test Step 2 (direct) */

    /* Send Link Layer messages to the BDUT with the additional information type to be tested. */

    /* The BDUT sends the frames to the bus as expected, e.g. with time delay. */

    /* Test Step 3 (direct) */

    /* Send link layer messages to the BDUT with unsupported additional information type(s) */
    pei_data = { 0x11, 0x04, 0x01, 0x02, 0x00, 0x01, 0xBC, 0xE0, 0x00, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // L_Data.req + PL_medium_information
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    pei_data = { 0x11, 0x06, 0x05, 0x04, 0x00, 0x00, 0xFF, 0xFF, 0xBC, 0xE0, 0x00, 0x00, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // L_Data.req + Time_Delay_until_sendinr
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT ignores the received additional information and sends the frames to the bus independent of the
     * (unsupported) additional information given in the cEMI frame. The BDUT sends back an L_Data
     * confirmation without the additional information types given in the L_Data request */
    pei_data = { 0x2E, 0x00, 0xBC, 0xE0, 0x02, 0xFF, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // L_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    pei_data = { 0x2E, 0x00, 0xBC, 0xE0, 0x02, 0xFF, 0xFF, 0xFF, 0x01, 0x00, 0x00 }; // L_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/* 10.3 Testing of Network Layer Services */

/* 10.4 Testing of Transport Layer Services */

/* 10.5 Testing of Application Layer Services */

/* 10.6 Testing of Local Device Management Services */

/**
 * M_PropRead
 *
 * @ingroup KNX_08_06_03_10_06_01
 */
TEST(CEMI_Tests, Test_10_6_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Step 1: Property Read with correct parameters */

    /* The BDUT receives an M_PropRead.req, e.g. to Object Type Property of cEMI server Object */
    pei_data = { 0xFC, 0x00, 0x08, 0x01, 0x01, 0x10, 0x01 }; // M_PropRead.req(IOT=8, OI=1, PID=1, NoE=1, SIx=1)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_PropRead.con */
    pei_data = { 0xFB, 0x00, 0x08, 0x01, 0x01, 0x10, 0x01, 0x00, 0x08 }; // M_PropRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Repeat the test with other (correct) parameters:
     * - Test with other Object Type
     * - Test with Property ID ≠ 1
     * - Test with Count ≠ 1 (if applicable)
     * - Test with Start Index ≠ 001 (if applicable)
     * - Combinations of them
     */

    /* Step 2: Property Read of array element 0 (current number of valid array elements) */

    /* The BDUT receives an M_PropRead.req, e.g. to Object Type Property of cEMI server Object */
    pei_data = { 0xFC, 0x00, 0x08, 0x01, 0x01, 0x10, 0x00 }; // M_PropRead.req(IOT=8, OI=1, PID=1, NoE=1, SIx=0)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_PropRead.con */
    pei_data = { 0xFB, 0x00, 0x08, 0x01, 0x01, 0x10, 0x00, 0x00, 0x01 }; // M_PropRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Repeat the test for (at least one) other existing properties, e.g. for the Group Address and association table
     * properties. */

    /* Step 3: Property Read with illegal Object Type */

    /* The BDUT receives an M_PropRead.req, e.g. to Object Type Property of an inexistent Interface Object */
    pei_data = { 0xFC, 0x00, 0x63, 0x01, 0x01, 0x10, 0x01 }; // M_PropRead.req(IOT=99, OI=1, PID=1, NoE=1, SIx=1)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_PropRead.con */
    pei_data = { 0xFB, 0x00, 0x63, 0x01, 0x01, 0x00, 0x01, 0x07 }; // M_PropRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 4: Property Read with illegal Object Instance */

    /* The BDUT receives an M_PropRead.req, e.g. to Object Type Property of an inexistent Object Instance */
    pei_data = { 0xFC, 0x00, 0x08, 0x02, 0x01, 0x10, 0x01 }; // M_PropRead.req(IOT=8, OI=2, PID=1, NoE=1, SIx=1)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_PropRead.con */
    pei_data = { 0xFB, 0x00, 0x08, 0x02, 0x01, 0x00, 0x01, 0x07 }; // M_PropRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 5: Property Read with illegal Property ID */

    /* The BDUT receives an M_PropRead.req, e.g. to an inexistent Property within cEMI server Object */
    pei_data = { 0xFC, 0x00, 0x08, 0x01, 0x32, 0x10, 0x01 }; // M_PropRead.req(IOT=8, OI=1, PID=50, NoE=1, SIx=1)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_PropRead.con */
    pei_data = { 0xFB, 0x00, 0x08, 0x01, 0x32, 0x00, 0x01, 0x07 }; // M_PropRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 6: Property Read with illegal Start Index */

    /* The BDUT receives an M_PropRead.req, e.g. to index 2 of the object type Property (cEMI server
     * Object) */
    pei_data = { 0xFC, 0x00, 0x08, 0x01, 0x01, 0x10, 0x02 }; // M_PropRead.req(IOT=8, OI=1, PID=1, NoE=1, SIx=2)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_PropRead.con */
    pei_data = { 0xFB, 0x00, 0x08, 0x01, 0x01, 0x00, 0x02, 0x09 }; // M_PropRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 7: Property Read with illegal Count */

    /* The BDUT receives an M_PropRead.req, e.g. 2 elements of the object type Property (cEMI server
     * Object) */
    pei_data = { 0xFC, 0x00, 0x08, 0x01, 0x01, 0x20, 0x01 }; // M_PropRead.req(IOT=8, OI=1, PID=1, NoE=2, SIx=1)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_PropRead.con */
    pei_data = { 0xFB, 0x00, 0x08, 0x01, 0x01, 0x00, 0x01, 0x09 }; // M_PropRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Repeat this test step with an existing property, with start index = 0 and count ≠ 1. */
}

/**
 * M_PropWrite
 *
 * @ingroup KNX_08_06_03_10_06_02
 */
TEST(CEMI_Tests, Test_10_6_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;
    ASSERT_EQ(bdut.interface_objects->cemi_server()->communication_mode()->communication_mode.field1, 0x00); // Data Link Layer

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Step 1: Property Write with correct parameters */

    /* The BDUT receives an M_PropWrite.req, e.g. a switch to busmonitor mode */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01, 0x01 }; // M_PropWrite.req(OIT=8, OI=1, PID=52, NoE=1, SIx=1, Data=0x01)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();
    ASSERT_EQ(bdut.interface_objects->cemi_server()->communication_mode()->communication_mode.field1, 0x01); // Data Link Layer Busmonitor

    /* The BDUT sends the following M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Read back the property value with M_PropRead Service to check if the property Value was written. */

    /* Repeat the test with other (correct) parameters:
     * - Test with other Object Type (if applicable)
     * - Test with other Property ID (if applicable)
     * - Test with Count ≠ 1 (if applicable)
     * - Test with Start Index ≠ 001 (if applicable)
     * - Combinations of them */

    /* Step 2: Property Write to array element 0 (current number of valid array elements) */

    /* The BDUT receives an M_PropWrite.req, e.g. set Group Address table length to 0 */
    pei_data = { 0xF6, 0x00, 0x01, 0x01, 0x17, 0x10, 0x00, 0x00, 0x00 }; // M_PropWrite.req(IOT=1, OI=1, PID=23, NoE=1, SIx=0, Data=0x0000)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x01, 0x01, 0x17, 0x10, 0x00 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Read back the property with M_PropRead Service to check if the property value was written. */

    /* Repeat the test for (at least one) other existing array-structured properties with write-access enabled. */

    /* Step 3: Property Write to array element with array index > current valid nr. of elements */

    /* The BDUT receives an M_PropWrite.req, e.g. write Group Addresses to the (now empty) Group Address
     * table property */
    pei_data = { 0xF6, 0x00, 0x01, 0x01, 0x17, 0x20, 0x01, 0x10, 0x00, 0xFF, 0xFE }; // M_PropWrite.req(IOT=1, OI=1, PID=23, NoE=2, SIx=1, Data=0x1000FFFE)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x01, 0x01, 0x17, 0x20, 0x01 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Read back the number of current valid array-elements of the written property with M_PropRead Service
     * to check if the current valid number of array-elements was updated to the expected value (2 in the
     * example above) */

    /* Stimuli */
    pei_data = { 0xFC, 0x00, 0x01, 0x01, 0x17, 0x10, 0x00 }; // M_PropRead.req(IOT=1, OI=1, PID=23, NoE=1, SIx=0)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* Expected Response */
    pei_data = { 0xFB, 0x00, 0x01, 0x01, 0x17, 0x10, 0x00, 0x00, 0x02 }; // M_PropRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* If applicable, repeat the test for (at least one) other existing array-structured properties with write-access
     * enabled. */

    /* Step 4: Property Write with illegal Object Type */

    /* The BDUT receives an M_PropWrite.req to a property of a non-existent interface object, e.g. try to write
     * to PID_Table of an inexistent Interface Object (usually, write access is enabled for this property within
     * objects that have this property) */
    pei_data = { 0xF6, 0x00, 0x63, 0x01, 0x17, 0x10, 0x01, 0x10, 0x00 }; // M_PropWrite.req(IOT=99, OI=1, PID=23, NoE=1, SIx=1, Data=0x1000)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following negative M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x63, 0x01, 0x17, 0x00, 0x01, 0x07 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 5: Property Read with illegal Object Instance */

    /* The BDUT receives an M_PropWrite.req to a property of a non-existent instance of an existent interface
     * object, e.g. try to switch to busmonitor mode by writing to PID_CommMode of 2nd instance of the cEMI
     * server object */
    pei_data = { 0xF6, 0x00, 0x08, 0x02, 0x34, 0x10, 0x01, 0x01 }; // M_PropWrite.req(IOT=8, OI=2, PID=52, NoE=1, SIx=1, Data=0x01)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following negative M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x08, 0x02, 0x34, 0x00, 0x01, 0x07 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 6: Property Write with illegal Property ID */

    /* The BDUT receives an M_PropWrite.req to non-existent property within an existent interface object, e.g.
     * try to write to Property 50 of the cEMI server object */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x32, 0x10, 0x01, 0x00 }; // M_PropWrite.req(IOT=8, OI=1, PID=50, NoE=1, SIx=1, Data=0x00)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following negative M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x32, 0x00, 0x01, 0x07 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 7: Property Write with illegal Start Index */

    /* The BDUT receives an M_PropWrite.req to an existent property but with array-start index out of range,
     * e.g. try to write to PID_CommMode within cEMI server object */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x34, 0x10, 0x02, 0x00 }; // M_PropWrite.req(IOT=8, OI=1, PID=52, NoE=1, SIx=2, Data=0x00)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following negative M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x34, 0x00, 0x02, 0x09 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 8: Property Write with illegal Count */

    /* The BDUT receives an M_PropWrite.req to an existent property but with an illegal count (number of
     * array elements to write), e.g. try to write 2 elements to PID_CommMode within cEMI server object */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x34, 0x20, 0x01, 0x01, 0x02 }; // M_PropWrite.req(IOT=8, OI=1, PID=52, NoE=2, SIx=1, Data=0x0102)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following negative M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x34, 0x00, 0x01, 0x09 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Repeat this test step with an existing property, with start index = 0 and count ≠ 1, if applicable. */

    /* Example: Try to write 2 elements to Group Address table starting from array-index 0 */
    pei_data = { 0xF6, 0x00, 0x01, 0x01, 0x17, 0x20, 0x00, 0x00, 0x01, 0x10, 0x00 }; // M_PropWrite.req(IOT=1, OI=1, PID=23, NoE=2, SIx=0, Data=0x00011000)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following negative M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x01, 0x01, 0x17, 0x00, 0x00, 0x09 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 9: Property Write to a read-only Property */

    /* The BDUT receives an M_PropWrite.req to a read-only property, e.g. try to write to PID_MediaType
     * within cEMI server object */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x33, 0x10, 0x01, 0x00 }; // M_PropWrite.req(IOT=8, OI=1, PID=51, NoE=1, SIx=1, Data=0x00)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following negative M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x33, 0x00, 0x01, 0x05 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 10: Property Write with a Value out of allowed range */

    /* The BDUT receives an M_PropWrite.req, e.g. try to switch to an invalid communication mode */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01, 0x1F }; // M_PropWrite.req(IOT=8, OI=1, PID=52, NoE=1, SIx=1, Data=0x1F)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following negative M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x34, 0x00, 0x01, 0x01 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 11: Property Write with a Data Type Conflict */

    /* The BDUT receives an M_PropWrite.req, e.g. try to switch to busmonitor mode with a wrong property
     * data type (wrong length) */
    pei_data = { 0xF6, 0x00, 0x08, 0x01, 0x34, 0x10, 0x01, 0x00, 0x01 }; // M_PropWrite.req(IOT=8, OI=1, PID=52, NoE=1, SIx=1, Data=0x0001)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following negative M_PropWrite.con */
    pei_data = { 0xF5, 0x00, 0x08, 0x01, 0x34, 0x00, 0x01, 0x08 }; // M_PropWrite.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Additional Test Steps: Error Codes */

    /* The BDUT receives an M_PropWrite.req that causes an error with the error code to be tested */

    /* The BDUT sends the negative M_PropWrite.con with the expected error code */
}

/**
 * M_PropInfo
 *
 * @ingroup KNX_08_06_03_10_06_03
 */
TEST(CEMI_Tests, Test_10_6_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    // @todo Test_10_6_3

    /* Stimulate BDUT to send an M_PropInfo.ind */

    /* The BDUT sends the correct M_PropInfo.ind */
    //pei_data = { 0xF7, ... }; // M_PropInfo.ind

    /* If applicable, repeat the test for (at least one) other properties. */
}

/**
 * M_Reset
 *
 * @ingroup KNX_08_06_03_10_06_04
 */
TEST(CEMI_Tests, Test_10_6_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Step 1: Reaction to an M_Reset.req */

    /* The BDUT receives an M_Reset.req */
    pei_data = { 0xF1 }; // M_Reset.req
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The reaction to an M_Reset.ind is cEMI server specific. The cEMI server device shall react to an
     * M_Reset.ind as stated by the product supplier in the product documentation (PICS/PIXIT). */

    /* If applicable, the BDUT sends an M_Reset.ind */
    pei_data = { 0xF0 }; // M_Reset.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 2: behaviour after a power-on */

    /* Disconnect BDUT’s power supplying connection and reconnect */

    /* The behaviour after a supply power-on is cEMI server specific. The cEMI server device shall react to a
     * supply power on as stated by the product supplier in the product documentation (PICS/PIXIT). */

    /* If applicable, the BDUT sends an M_Reset.ind */
    //pei_data = { 0xF0 }; // M_Reset.ind
    //ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * M_FuncPropCommand
 *
 * @ingroup KNX_08_06_03_10_06_05
 */
TEST(CEMI_Tests, Test_10_6_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Step 1: FunctionPropertyCommand with correct parameters */

    /* The BDUT receives an M_FuncPropCommand.req, e.g. clear routing table */
    pei_data = { 0xF8, 0x00, 0x06, 0x01, 0x38, 0x00, 0x01 }; // M_FuncPropCommand.req(IOT=6, OI=1, PID=56, Data=0x0001)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_FuncPropStateResponse.con */
    pei_data = { 0xFA, 0x00, 0x06, 0x01, 0x38, 0x00, 0x01 }; // M_FuncPropStateRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2: FunctionPropertyCommand with incorrect data */

    /* The BDUT receives an M_FuncPropCommand.req, e.g. with faulty data */
    pei_data = { 0xF8, 0x00, 0x06, 0x01, 0x38, 0x00, 0x08 }; // M_FuncPropCommand.req(IOT=6, OI=1, PID=56, Data=0x0008)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_FuncPropStateResponse.con */
    pei_data = { 0xFA, 0x00, 0x06, 0x01, 0x38, 0xFF, 0x08 }; // M_FuncPropStateRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3: FunctionPropertyCommand with illegal parameters */

    /* The BDUT receives an M_FuncPropCommand.req to a property with property data type ≠
     * PDT_Function */
    pei_data = { 0xF8, 0x00, 0x06, 0x00, 0x01, 0x00, 0x01 }; // M_FuncPropCommand.req(IOT=6, OI=0, PID=1, Data=0x0001)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_FuncPropStateResponse.con */
    pei_data = { 0xFA, 0x00, 0x06, 0x00, 0x01 }; // M_FuncPropStateRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Repeat the test with other illegal parameters:
     * - Test with illegal Object Type (an inexistent interface object)
     * - Test with illegal Object Instance (e.g. instance >1 if this does not exist in the BDUT)
     * - Test with illegal Property ID */
}

/**
 * M_FuncPropStateRead
 *
 * @ingroup KNX_08_06_03_10_06_06
 */
TEST(CEMI_Tests, Test_10_6_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Step 1: FunctionPropertyStateRead with correct parameters */

    /* The BDUT receives an M_FuncPropStateRead.req, e.g. test if routing table is cleared */
    pei_data = { 0xF9, 0x00, 0x06, 0x01, 0x38, 0x00, 0x01 }; // M_FuncPropStateRead.req(IOT=6, OI=1, PID=56, Data=0x0001)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_FuncPropStateResponse.con */
    pei_data = { 0xFA, 0x00, 0x06, 0x01, 0x38, 0x00, 0x01 }; // M_FuncPropStateRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2: FunctionPropertyStateRead with incorrect data */

    /* The BDUT receives an M_FuncPropStateRead.req e.g. with faulty data */
    pei_data = { 0xF9, 0x00, 0x06, 0x01, 0x38, 0x00, 0x08 }; // M_FuncPropStateRead.req(IOT=6, OI=1, PID=56, Data=0x0008)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_FuncPropStateResponse.con */
    pei_data = { 0xFA, 0x00, 0x06, 0x01, 0x38, 0xFF, 0x08 }; // M_FuncPropStateRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3: FunctionPropertyStateRead with illegal parameters */

    /* The BDUT receives a M_FuncPropStateRead.req to a property with property data type ≠ PDT_Function */
    pei_data = { 0xF9, 0x00, 0x06, 0x00, 0x01, 0x00, 0x01 }; // M_FuncPropStateRead.req(IOT=6, OI=0, PID=1, Data=0x0001)
    bdut.cemi_server.cemi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the following M_FuncPropStateResponse.con */
    pei_data = { 0xFA, 0x00, 0x06, 0x00, 0x01 }; // M_FuncPropStateRead.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Repeat the test with other illegal parameters:
     * - Test with illegal Object Type (an inexistent interface object)
     * - Test with illegal Object Instance (e.g. instance >1 if this does not exist in the BDUT)
     * - Test with illegal Property ID */
}

/* 10.7 Testing of cEMI Server's Interface Objects */

/**
 * Device Object
 *
 * @ingroup KNX_08_06_03_10_07_02
 */
TEST(CEMI_Tests, Test_10_7_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    // @todo Test_10_7_2

    /* Tests shall cover:
     * - Reading from all implemented (read-only) properties.
     * - Writing to (and reading back from) all implemented properties with write-access enabled.
     * - At least once each negative test (try to read from a non-existent property, try to write to a read-only
     *   property and try to write to a non-existent property). */

    /* The information, which properties are implemented in the BDUT shall be taken from the cEMI server
     * device profile, respectively from the manufacturer’s product documentation. */
}

/**
 * cEMI Service Object
 *
 * @ingroup KNX_08_06_03_10_07_03
 */
TEST(CEMI_Tests, Test_10_7_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    // @todo Test_10_7_3

    /* Tests shall cover:
     * - Reading from all implemented properties
     * - Writing to and reading back from all implemented properties with write-access enabled.
     * - At least once each negative test (try to read from a non-existent property, try to write to a read-only
     *   property and try to write to a non-existent property). */

    /* The information, which properties are implemented in the BDUT shall be taken from the cEMI server
     * device profile, respectively from the manufacturer’s product documentation. */
}

/**
 * Other Interface Objects
 *
 * @ingroup KNX_08_06_03_10_07_04
 */
TEST(CEMI_Tests, Test_10_7_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    //std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    // @todo Test_10_7_4

    /* In order to limit the (certification) test efforts, sample tests are sufficient here.
     * A reasonable set of properties shall be tested. The sample tests shall cover:
     * - Reading from a sample set of implemented properties
     * - Write to and read back from a sample set of properties with write-access enabled. */
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
