// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/09/04/01/TP1_BCU2.h>

class Network_Layer_Tests : public ::testing::Test
{
    virtual ~Network_Layer_Tests() = default;
};

/** BCU that can be used as Bus Device Under Test (BDUT) */
class BCU :
    public KNX::TP1_BCU2
{
public:
    explicit BCU(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        KNX::TP1_BCU2(io_context, tp1_bus_simulation) {
    }
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class BCU_Plain_Data
{
public:
    explicit BCU_Plain_Data(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        device_object(std::make_shared<KNX::Device_Object>()),
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context)
    {
        tp1_physical_layer.connect(tp1_bus_simulation);

        tp1_data_link_layer.device_object = device_object;
        tp1_data_link_layer.group_address_table = group_address_table;
        tp1_data_link_layer.L_Busmon_ind = std::bind(&BCU_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** device object */
    std::shared_ptr<KNX::Device_Object> device_object;

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/* 2 Testing of Busmonitor services */

/**
 * L_Busmon
 *
 * @ingroup KNX_08_06_03_02_02
 */
TEST(EMI_Tests, Test_2_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    BCU_Plain_Data bcu1(io_context, tp1_bus);
    bcu1.group_address_table->individual_address = KNX::Individual_Address(0x1001);
    bcu1.group_address_table->group_addresses.insert(KNX::Group_Address(0x2000));

    BCU_Plain_Data bcu2(io_context, tp1_bus);
    bcu2.group_address_table->individual_address = KNX::Individual_Address(0x1002);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Set the BDUT in busmonitor-mode. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x90, 0x18, 0x34, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();
    ASSERT_EQ(bdut.data_link_layer->mode, KNX::Data_Link_Layer::Mode::Busmonitor);

    /* Step 1 (indirect) */

    /* Send a frame on the bus that is not acknowledged. */
    tp1_data = { 0xBC, 0x10, 0x02, 0x20, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu2.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT sends the L_Busmon.ind specified below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Busmon_ind), 0x00, 0x00, 0x00, 0xBC, 0x10, 0x02, 0x20, 0x01, 0xE1, 0x00, 0x00, 0 }; // L_Busmon.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 2 (indirect) */

    /* Send a frame on the bus that is acknowledged. */
    tp1_data = { 0xBC, 0x10, 0x02, 0x20, 0x00, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu2.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the two L_Busmon.ind specified below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Busmon_ind), 0x00, 0x00, 0x00, 0xBC, 0x10, 0x02, 0x20, 0x00, 0xE1, 0x00, 0x00, 0 }; // L_Busmon.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Busmon_ind), 0x00, 0x00, 0x00, 0xCC }; // L_Busmon.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * L_PlainData
 *
 * @ingroup KNX_08_06_03_02_03
 */
TEST(EMI_Tests, Test_2_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0xAFFE);

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Step 1 (direct) */

    /* Send a L_PlainData.req message to the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Plain_Data_req), 0x00, 0x00, 0x00, 0x00, 0x00, 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0x60, 0xC2, 0 }; // L_Plain_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the corresponding frame on the bus. */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 3 Testing of Link Layer services */

/**
 * L_Data
 *
 * @ingroup KNX_08_06_03_03_01
 */
TEST(EMI_Tests, Test_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Link Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x18, 0x34, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 02h, 1000h, FFFEh */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xFFFE));

    /* Install a device in the test set-up acknowledging only the Group Address FFFFh. */
    BCU_Plain_Data bcu2(io_context, tp1_bus);
    bcu2.group_address_table->group_addresses.insert(KNX::Group_Address(0xFFFF));

    /* Step 1 (direct) */

    /* The BDUT receives a L_Data.req, e.g. Group address type - APCI=A_GroupValue_Read */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_req), 0x0C, 0x00, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00 }; // L_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus corresponding to the L_Data.req */
    tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_Data.con when it receives an IACK on the bus. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_con), 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00 }; // L_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* The BDUT receives a L_Data.request, e.g. Group address type - APCI=ValueWrite */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_req), 0x0C, 0x00, 0x00, 0xFF, 0xFF, 0xEF, 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E }; // L_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT transmits the data packet on the KNX bus corresponding to the L_Data.req */
    tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xEF, 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an according L_Data.con service. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_con), 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xEF, 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E }; // L_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (direct) */

    /* The BDUT receives a L_Data.req, e.g. Group Address type - APCI=A_GroupValue_Read - System priority */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_req), 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00 }; // L_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT transmits the data packet on the KNX bus corresponding to the L_Data.req */
    tp1_data = { 0xB0, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an according L_Data.con service */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_con), 0xB0, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00 }; // L_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 4 (direct) */

    /* The BDUT receives a L_Data.req, e.g. Group address type - APCI=A_GroupValue_Read */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_req), 0x0C, 0x00, 0x00, 0x00, 0x01, 0xE1, 0x00, 0x00 }; // L_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x00, 0x00, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_Data.con (and an according frame on the bus plus repetitions) */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_con), 0xBC, 0x10, 0x00, 0x00, 0x01, 0xE1, 0x00, 0x00 }; // L_Data.con // @note Control changed from 0x9D (repeat=repeated and confirm=error) to 0xBC
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 5 (direct) */

    /* The BDUT receives a L_Data.req, e.g. Group address type - APCI=A_GroupValue_Read */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_req), 0x0C, 0x00, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00 }; // L_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_Data.con (and an according frame on the bus plus repeitions) */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_con), 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00 }; // L_Data.con // @note Control changed from 0x9D (repeat=repeated and confirm=error) to 0xBC
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 6 (indirect) */

    /* The BDUT receives a data packet which is acknowledged by an IACK */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_ind), 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80 }; // L_Data.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 7 (indirect) */

    /* The BDUT receives a repeated data packet with system priority (which is acknowledged by an IACK) */
    tp1_data = { 0x90, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_ind), 0xB0, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80 }; // L_Data.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 8 (indirect) */

    /* The BDUT receives a data packet with the maximum value length (which is acknowledged by an IACK) */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xEF, 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_ind), 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xEF, 0x00, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E }; // L_Data.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 9 (indirect) */

    /* The following data packet is sent on the KNX bus */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x01, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT does not transmit a L_Data.ind */
    ASSERT_EQ(pei_data_ind, bdut.pei_data_ind.cend());
}

/**
 * L_PollData
 *
 * @ingroup KNX_08_06_03_03_02
 */
TEST(EMI_Tests, Test_3_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Link Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x18, 0x34, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h, 1000h */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Install a device in the test set-up with polling group FFFEh and answer slot 02h, Data=01h. */
    BCU poll_slave(io_context, tp1_bus);
    poll_slave.interface_objects->device()->polling_group_settings()->polling_group_address = 0xFFFE;
    poll_slave.interface_objects->device()->polling_group_settings()->polling_slot_number = 0x02;
    poll_slave.tp1_data_link_layer.poll_data = 0x01;

    /* Step 1 (direct) */

    /* The BDUT receives a L_PollData.req, number of slots requested = 05h */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Poll_Data_req), 0xF0, 0x00, 0x00, 0xFF, 0xFE, 0x05 }; // L_Poll_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus and fills all unanswered slots with FEh */
    tp1_data = { 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0, 0xFE, 0xFE, 0x01, 0xFE, 0xFE };
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_PollData.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Poll_Data_con), 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0xFE, 0xFE, 0x01, 0xFE, 0xFE }; // L_Poll_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* The BDUT receives a L_PollData.req, number of slots requested = 0Fh */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Poll_Data_req), 0xF0, 0x00, 0x00, 0xFF, 0xFE, 0x0F }; // L_Poll_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus and fills all unanswered slots with FEh */
    tp1_data = { 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x0F, 0, 0xFE, 0xFE, 0x01, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE };
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_PollData.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Poll_Data_con), 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x0F, 0xFE, 0xFE, 0x01, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE }; // L_Poll_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/* 4 Testing of Network Layer Services */

/**
 * N_Data_Individual
 *
 * @ingroup KNX_08_06_03_04_01
 */
TEST(EMI_Tests, Test_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Network-Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x88, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Set the routing counter of the BDUT to the value 6. */
    bdut.interface_objects->router()->hop_count()->hop_count = 6;

    /* Load the BDUT with the following address table: 01h, 1000h */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Step 1 (direct) */

    for (const KNX::Hop_Count hop_count : {
                6, 5, 4, 3, 2, 1, 0
            }) {
        /* Send local N_Data_Individual.req services with routing counter = 6, 5, 4, 3, 2, 1, 0 to the BDUT, e.g. APCI=A_ADC_Read */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Individual_req), 0x0C, 0x00, 0x00, 0xAF, 0xFE, 0x02, 0x41, 0x80, 0x00 }; // N_Data_Individual.req
        pei_data[6] = (pei_data[6] & 0x8f) | (hop_count << 4);
        bdut.emi_req(pei_data);
        io_context.poll();

        /* The BDUT sends corresponding data packets on the KNX bus with routing counter 6 */
        tp1_data = { 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x62, 0x41, 0x80, 0x00, 0 }; // A_ADC_Read
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT also sends N_Data_Individual.con services with routing counter = 6 */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Individual_con), 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x62, 0x41, 0x80, 0x00 }; // N_Data_Individual.con
        ASSERT_EQ(*pei_data_con++, pei_data);
    };

    /* Step 2 (direct) */

    /* Send a local N_Data_Individual.req service to the BDUT, e.g. APCI=A_ADC_Read, routing counter=7 */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Individual_req), 0x0C, 0x00, 0x00, 0xAF, 0xFE, 0x72, 0x41, 0x80, 0x00 }; // N_Data_Individual.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends a corresponding data packets on the KNX bus */
    tp1_data = { 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x72, 0x41, 0x80, 0x00, 0 }; // A_ADC_Read
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an N_Data_Individual.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Individual_con), 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x72, 0x41, 0x80, 0x00 }; // N_Data_Individual.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (indirect) */

    for (const KNX::Hop_Count hop_count : {
                7, 6, 5, 4, 3, 2, 1, 0
            }) {
        /* Send frames to BDUT via the bus with routing counters 7, 6, 5, 4, 3, 2, 1, 0, e.g. APCI=A_ADC_Read */
        tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0x62, 0x41, 0x80, 0x00, 0 }; // A_ADC_Read
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();

        /* The BDUT sends N_Data_Individual.ind services with the routing counter identical to the routing counter in the received frames */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Individual_ind), 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0x62, 0x41, 0x80, 0x00 }; // N_Data_Individual.ind // @note bc changed from 0x0C to 0xBC
        pei_data[6] = (pei_data[6] & 0x8f) | (hop_count << 4);
        ASSERT_EQ(*pei_data_ind++, pei_data);
    }
}

/**
 * N_Data_Group
 *
 * @ingroup KNX_08_06_03_04_02
 */
TEST(EMI_Tests, Test_4_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Network-Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x88, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Set the routing counter of the BDUT to the value 6. */
    bdut.interface_objects->router()->hop_count()->hop_count = 6;

    /* Load the BDUT with the following address table: 02h, 1000h, FFFEh */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xFFFE));

    /* Step 1 (direct) */

    for (const KNX::Hop_Count hop_count : {
                6, 5, 4, 3, 2, 1, 0
            }) {
        /* Send N_Data_Group.req services with routing counters 6, 5, 4, 3, 2, 1, 0 to the BDUT, e.g. APCI=A_GroupValue_Read */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Group_req), 0x0C, 0x00, 0x00, 0xFF, 0xFE, 0x01, 0x00, 0x00 }; // N_Data_Group.req
        pei_data[6] = (pei_data[6] & 0x8f) | (hop_count << 4);
        bdut.emi_req(pei_data);
        io_context.poll();

        /* The BDUT sends corresponding frames on the KNX bus with routing counter = 6 */
        tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFE, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read // @note APCI changed from A_GroupValue_Write (0x080) to A_GroupValue_Read (0x000)
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT also sends N_Data_Group.con services with routing counter = 6 */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Group_con), 0xBC, 0x10, 0x00, 0xFF, 0xFE, 0xE1, 0x00, 0x00 }; // N_Data_Group.con
        ASSERT_EQ(*pei_data_con++, pei_data);
    }

    /* Step 2 (direct) */

    /* Send an N_Data_Group.req service to the BDUT, e.g. APCI=A_GroupValue_Read, routing counter=7 */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Group_req), 0x0C, 0x00, 0x00, 0xFF, 0xFE, 0x71, 0x00, 0x00 }; // N_Data_Group.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends a corresponding frame on the KNX */
    tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFE, 0xF1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends a N_Data_Group.con with routing counter = 7 */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Group_con), 0xBC, 0x10, 0x00, 0xFF, 0xFE, 0xF1, 0x00, 0x00 }; // N_Data_Group.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (indirect) */

    for (const KNX::Hop_Count hop_count : {
                7, 6, 5, 4, 3, 2, 1, 0
            }) {
        /* Send frames to BDUT via the KNX bus with routing counters 7, 6, 5, 4, 3, 2, 1, 0, e.g. APCI=A_GroupValue_Read */
        tp1_data = { 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();

        /* The BDUT sends N_Data_Group.ind services containing the same routing counter as in the received frames */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Group_ind), 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x00 }; // N_Data_Group.ind
        pei_data[6] = (pei_data[6] & 0x8f) | (hop_count << 4);
        ASSERT_EQ(*pei_data_ind++, pei_data);
    }
}

/**
 * N_Data_Broadcast
 *
 * @ingroup KNX_08_06_03_04_03
 */
TEST(EMI_Tests, Test_4_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Network-Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x88, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Set the routing counter of the BDUT to the value 6. */
    bdut.interface_objects->router()->hop_count()->hop_count = 6;

    /* Load the BDUT with the following address table: 01h, 1000h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Step 1 (direct) */

    for (const KNX::Hop_Count hop_count : {
                6, 5, 4, 3, 2, 1, 0
            }) {
        /* Send N_Data_Broadcast.req services to the BDUT with routing counters 6, 5, 4, 3, 2, 1, 0, e.g. APCI=A_IndividualAddress_Read */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_req), 0x0C, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00 }; // N_Data_Broadcast.req
        pei_data[6] = (pei_data[6] & 0x8f) | (hop_count << 4);
        bdut.emi_req(pei_data);
        io_context.poll();

        /* The BDUT sends corresponding frames on the KNX with routing counter 6 */
        tp1_data = { 0xBC, 0x10, 0x00, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT also sends N_Data_Broadcast.con services with routing counter 6. */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_con), 0xBC, 0x10, 0x00, 0x00, 0x00, 0xE1, 0x01, 0x00 }; // N_Data_Broadcast.con
        ASSERT_EQ(*pei_data_con++, pei_data);
    }

    /* Step 2 (direct) */

    /* Send an N_Data_Broadcast.req service to the BDUT, e.g. APCI=A_IndividualAddress_Read, routing counter=7 */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_req), 0x0C, 0x00, 0x00, 0x00, 0x00, 0x71, 0x01, 0x00 }; // N_Data_Broadcast.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends a corresponding frame on the KNX bus with routing counter 7 */
    tp1_data = { 0xBC, 0x10, 0x00, 0x00, 0x00, 0xF1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an N_Data_Broadcast.con with routing counter 7 */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_con), 0xBC, 0x10, 0x00, 0x00, 0x00, 0xF1, 0x01, 0x00 }; // N_Data_Broadcast.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (direct) */

    for (const KNX::Hop_Count hop_count : {
                7, 6, 5, 4, 3, 2, 1, 0
            }) {
        /* Send broadcast frames on the KNX bus with routing counter 7, 6, 5, 4, 3, 2, 1, 0, e.g. APCI=A_IndividualAddress_Read */
        tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT sends N_Data_Broadcast.ind services with the same routing counters */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_ind), 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0xE1, 0x01, 0x00 }; // N_Data_Broadcast.ind
        pei_data[6] = (pei_data[6] & 0x8f) | (hop_count << 4);
        ASSERT_EQ(*pei_data_ind++, pei_data);
    }
}

/**
 * N_PollData
 *
 * @ingroup KNX_08_06_03_04_04
 */
TEST(EMI_Tests, Test_4_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Network-Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x88, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h, 1000h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Install a device in the test set-up with polling group FFFEh and answer slot 02h, Data=01h. */
    BCU poll_slave(io_context, tp1_bus);
    poll_slave.interface_objects->device()->polling_group_settings()->polling_group_address = 0xFFFE;
    poll_slave.interface_objects->device()->polling_group_settings()->polling_slot_number = 0x02;
    poll_slave.tp1_data_link_layer.poll_data = 0x01;

    /* Step 1 (direct) */

    /* The BDUT receives a N_PollData.req, number of slots requested = 05h */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Poll_Data_req), 0xF0, 0x00, 0x00, 0xFF, 0xFE, 0x05 }; // N_Poll_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus and fills all unanswered slots with FEh */
    tp1_data = { 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0, 0xFE, 0xFE, 0x01, 0xFE, 0xFE };
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following N_PollData.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Poll_Data_con), 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0xFE, 0xFE, 0x01, 0xFE, 0xFE }; // N_Poll_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* The BDUT receives a N_PollData.req, number of slots requested = 0Fh */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Poll_Data_req), 0xF0, 0x00, 0x00, 0xFF, 0xFE, 0x0F }; // N_Poll_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus and fills all unanswered slots with FEh */
    tp1_data = { 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x0F, 0, 0xFE, 0xFE, 0x01, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE };
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following N_PollData.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Poll_Data_con), 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x0F, 0xFE, 0xFE, 0x01, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE, 0xFE }; // N_Poll_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/* 5 Testing of Transport Layer services */

/**
 * T_Connect
 *
 * @ingroup KNX_08_06_03_05_01
 */
TEST(EMI_Tests, Test_5_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Transport-Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Set the routing counter of the BDUT to the value 6. */
    bdut.interface_objects->router()->hop_count()->hop_count = 6;

    /* Load the BDUT with the following address table: 01h,1000h */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Install a device on the bus with individual address = AFFEh (sends IACKs). */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* Step 1 (direct, IACK sent) */

    /* The BDUT receives a T_Connect.req. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xAF, 0xFE }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the TPDU Control Data Open on the KNX */
    tp1_data = { 0xB0, 0x10, 0x00, 0xAF, 0xFE, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends a T_Connect.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // T_Connect.con // @note destination changed from 0xAF00 to 0x1000
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct, no IACK sent) */

    /* The BDUT receives a T_Connect.req. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0x00, 0x01 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the TPDU Control Data Open on the KNX (and repeats it three times) */
    tp1_data = { 0xB0, 0x10, 0x00, 0x00, 0x01, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // @todo T_Connect is not repeated. Use timeout instead.
    bdut.transport_layer.test_expire_connection_timeout_timer(KNX::TSAP_Connected(0x0001));

    /* The BDUT also sends a T_Disconnect.ind */
    io_context.poll();
    tp1_data = { 0xB0, 0x10, 0x00, 0x00, 0x01, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0x00, 0x01, 0x10, 0x00 }; // T_Disconnect.ind // @note Source changed from 0x0010 to 0x0001. Dest changed from 0x0000 to 0x1000.
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 3 (indirect) */

    // Set BDUT back to closed
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.transport_layer.connection_state(KNX::TSAP_Connected(0xAFFE)), KNX::Connection_State::Closed);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xAF, 0xFE, 0x10, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT receives a frame with TPDU Control Data Open */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct T_Connect.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * T_Disconnect
 *
 * @ingroup KNX_08_06_03_05_02
 */
TEST(EMI_Tests, Test_5_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Transport-Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Set the routing counter of the BDUT to the value 6. */
    bdut.interface_objects->router()->hop_count()->hop_count = 6;

    /* Load the BDUT with the following address table: 01h,1000h */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Install a device on the bus with individual address = 0001h (sends IACKs) */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE); // @note Following sequences show that 0xAFFE is used.

    /* Step 1 (direct) */

    /* After establishing the transport connection ... */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0xAF, 0xFE }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0x10, 0x00, 0xAF, 0xFE, 0x60, 0x80, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);
    ASSERT_EQ(bdut.transport_layer.connection_state(KNX::TSAP_Connected(0xAFFE)), KNX::Connection_State::Open_Idle);

    /* ... send a T_Disconnect.req to the BDUT */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_req), 0x00, 0x00, 0x00, 0xAF, 0xFE }; // T_Disconnect.req // @note destination changed from 0x0000 to 0xAFFE
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX with TPCI=close */
    tp1_data = { 0xB0, 0x10, 0x00, 0xAF, 0xFE, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends a T_Disconnect.con message. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_con), 0x00, 0x10, 0x00, 0xAF, 0xFE }; // T_Disconnect.con // @note source changed from 0x0000 to 0x1000. destination changed from 0x0000 to 0xAFFE.
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (indirect) */

    /* Open a connection from device 0001h ... */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* ... and send the following data packet on the KNX bus */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct T_Disconnect.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xAF, 0xFE, 0x10, 0x00 }; // T_Disconnect.ind // @note destination changed from 0xAF00 to 0x1000.
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 3 (indirect) */

    /* Open a connection from device 0001h */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* ... and send the following data packet on the KNX bus */
    tp1_data = { 0xB0, 0x00, 0x02, 0x10, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT shows no reaction */
    ASSERT_EQ(pei_data_con, std::cend(bdut.pei_data_con));
    ASSERT_EQ(pei_data_ind, std::cend(bdut.pei_data_ind));

    /* Step 4 (indirect) */

    /* Send the following data packet on the KNX bus when no connection is open */
    tp1_data = { 0xB0, 0x00, 0x01, 0x10, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT shows no reaction */
    ASSERT_EQ(pei_data_con, std::cend(bdut.pei_data_con));
    ASSERT_EQ(pei_data_ind, std::cend(bdut.pei_data_ind));

    /* Step 5 (direct) */

    // Set BDUT back to closed
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.transport_layer.connection_state(KNX::TSAP_Connected(0xAFFE)), KNX::Connection_State::Closed);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xAF, 0xFE, 0x10, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Open a connection ... */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* ... and wait for 6 seconds. */
    bdut.transport_layer.test_expire_connection_timeout_timer(KNX::TSAP_Connected(0xAFFE));
    io_context.poll();

    /* The BDUT sends the correct T_Disconnect.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0xAF, 0xFE, 0x10, 0x00 }; // T_Disconnect.ind // @note source changed from 0x0000 to 0xAFFE. destination changed from 0x0000 to 0x1000.
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* The BDUT also sends the corresponding frame on the bus */
    tp1_data = { 0xB0, 0x10, 0x00, 0xAF, 0xFE, 0x60, 0x81, 0 }; // T_Disconnect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * T_Data_Connected
 *
 * @ingroup KNX_08_06_03_05_03
 */
TEST(EMI_Tests, Test_5_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Transport-Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h,1000h */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Install a device on the bus with individual address = 0001h (sends IACKs, T_ACKs). */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0x0001);

    /* Step 1 (direct) */

    /* After establishing the transport connection ... */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_req), 0x00, 0x00, 0x00, 0x00, 0x01 }; // T_Connect.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0x10, 0x00, 0x00, 0x01, 0x60, 0x080, 0 }; // T_Connect
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_con), 0xB0, 0x00, 0x01, 0x10, 0x00 }; // T_Connect.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* ... the BDUT receives T_Data_Connected.req services. */
    /* The BDUT sends the TPDUs Numbered Data Packet on the KNX-Bus in the following sequence */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB0, 0x10, 0x00, 0x00, 0x01, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x00, 0x01, 0x10, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0x04, 0x00, 0x00, 0x00, 0x00, 0x04, 0x02, 0x81, 0x00, 0xC0, 0x80 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB4, 0x10, 0x00, 0x00, 0x01, 0x64, 0x46, 0x81, 0x00, 0xC0, 0x80, 0 }; // A_Memory_Write
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x00, 0x01, 0x10, 0x00, 0x60, 0xC6, 0 }; // T_Ack
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0x08, 0x00, 0x00, 0x00, 0x00, 0x09, 0x02, 0x86, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xB8, 0x10, 0x00, 0x00, 0x01, 0x69, 0x4A, 0x86, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0 }; // A_Memory_Write
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x00, 0x01, 0x10, 0x00, 0x60, 0xCA, 0 }; // T_Ack
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0x0C, 0x00, 0x00, 0x00, 0x00, 0x0F, 0x02, 0x8B, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();
    tp1_data = { 0xBC, 0x10, 0x00, 0x00, 0x01, 0x6F, 0x4E, 0x8B, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0 }; // A_Memory_Write
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x00, 0x01, 0x10, 0x00, 0x60, 0xCE, 0 }; // T_Ack
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends T_Data_Connected.con services, when T_ACK frames are received on the bus (not after IACK!) */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_con), 0xB0, 0x10, 0x00, 0x00, 0x01, 0x61, 0x43, 0x00 }; // T_Data_Connected.con // @note destination changed from 0x0000 to 0x0001; control changed from 0x03 to 0x43
    ASSERT_EQ(*pei_data_con++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_con), 0xB4, 0x10, 0x00, 0x00, 0x01, 0x64, 0x42, 0x81, 0x00, 0xC0, 0x80 }; // T_Data_Connected.con // @note control changed from 0x02 to 0x42
    ASSERT_EQ(*pei_data_con++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_con), 0xB8, 0x10, 0x00, 0x00, 0x01, 0x69, 0x42, 0x86, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85 }; // T_Data_Connected.con // @note control changed from 0x02 to 0x42
    ASSERT_EQ(*pei_data_con++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_con), 0xBC, 0x10, 0x00, 0x00, 0x01, 0x6F, 0x42, 0x8B, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B }; // T_Data_Connected.con // @note control changed from 0x02 to 0x42
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    // Set BDUT back to closed
    tp1_data = { 0xB0, 0x00, 0x01, 0x10, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.transport_layer.connection_state(KNX::TSAP_Connected(0x0001)), KNX::Connection_State::Closed);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0x00, 0x01, 0x10, 0x00 }; // T_Disconnect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* With no connection open the BDUT receives a T_Data_Connected.req service */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_req), 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00 }; // T_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends a T_Disconnect.ind or shows no reaction. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Disconnect_ind), 0x00, 0x00, 0x00, 0x10, 0x00 }; // T_Disconnect.ind // @note Dest changed from 0x0000 to 0x0x1000
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 3 (indirect) */

    /* After establishing the transport connection, ... */
    ASSERT_EQ(bdut.transport_layer.connection_state(KNX::TSAP_Connected(0x0001)), KNX::Connection_State::Closed);
    tp1_data = { 0xB0, 0x00, 0x01, 0x10, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0x00, 0x01, 0x10, 0x00 }; // T_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
    ASSERT_EQ(bdut.transport_layer.connection_state(KNX::TSAP_Connected(0x0001)), KNX::Connection_State::Open_Idle);

    /* ... the following frames with TPDU Numbered Data Packet shall be sent to the BDUT via a certified RS232 */
    tp1_data = { 0xB0, 0x00, 0x01, 0x10, 0x00, 0x61, 0x43, 0x00, 0 }; // A_DeviceDescriptor_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x10, 0x00, 0x00, 0x01, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xB4, 0x00, 0x01, 0x10, 0x00, 0x64, 0x46, 0x81, 0x00, 0xC0, 0x80, 0 }; // A_Memory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x10, 0x00, 0x00, 0x01, 0x60, 0xC6, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xB8, 0x00, 0x01, 0x10, 0x00, 0x69, 0x4A, 0x86, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0 }; // A_Memory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x10, 0x00, 0x00, 0x01, 0x60, 0xCA, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x00, 0x01, 0x10, 0x00, 0x6F, 0x4E, 0x8B, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0 }; // A_Memory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x10, 0x00, 0x00, 0x01, 0x60, 0xCE, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct T_Data_Connected.ind services in the following sequence */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_ind), 0xB0, 0x00, 0x01, 0x10, 0x00, 0x61, 0x43, 0x00 }; // T_Data_Connected.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_ind), 0xB4, 0x00, 0x01, 0x10, 0x00, 0x64, 0x42, 0x81, 0x00, 0xC0, 0x80 }; // T_Data_Connected.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_ind), 0xB8, 0x00, 0x01, 0x10, 0x00, 0x69, 0x42, 0x86, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85 }; // T_Data_Connected.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Connected_ind), 0xBC, 0x00, 0x01, 0x10, 0x00, 0x6F, 0x42, 0x8B, 0x00, 0xC0, 0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B }; // T_Data_Connected.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * T_Data_Group
 *
 * @ingroup KNX_08_06_03_05_04
 */
TEST(EMI_Tests, Test_5_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Transport-Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Set the routing counter of the BDUT to the value 6. */
    bdut.interface_objects->router()->hop_count()->hop_count = 6;

    /* Load the following address table into the BDUT: 02h,1000h, FFFFh. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xFFFF));

    /* Install a device on the bus with individual address = 0001h (sends IACKs). */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0x0001);

    /* Step 1 (direct) */

    /* Send a T_Data_Group.req to the BDUT (e.g. APCI=A_GroupValue_Read) */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Group_req), 0x0C, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x00 }; // T_Data_Group.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends a frame on the KNX bus including the Value Read information */
    tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends a T_Data_Group.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Group_con), 0xBC, 0x10, 0x00, 0x00, 0x01, 0xE1, 0x00, 0x00 }; // T_Data_Group.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* Send a T_Data_Group.req to the BDUT (e.g. APCI=A_GroupValue_Read) */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Group_req), 0x0C, 0x00, 0x00, 0x00, 0x03, 0x01, 0x00, 0x00 }; // T_Data_Group.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends sends a T_Data_Group.con, but no frame on the KNX. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Group_con), 0xBD, 0x10, 0x00, 0x00, 0x03, 0x81, 0x00, 0x00 }; // T_Data_Group.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (indirect) */

    /* The Server transmits a frame on the KNX including e.g. APCI=A_GroupValue_Read */
    tp1_data = { 0xBC, 0x00, 0x01, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();

    /* The BDUT sends a T_Data_Group.ind service to the PEI */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Group_ind), 0xBC, 0x00, 0x01, 0x00, 0x01, 0xE1, 0x00, 0x00 }; // T_Data_Group.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * T_Data_Broadcast
 *
 * @ingroup KNX_08_06_03_05_05
 */
TEST(EMI_Tests, Test_5_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Transport Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h, 1000h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Step 1 (direct) */

    /* The BDUT receives a T_Data_Broadcast.req, e.g. APCI=A_IndividualAddress_Read */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Broadcast_req), 0x0C, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00 }; // T_Data_Broadcast.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus corresponding to the T_Data_Broadcast.req */
    tp1_data = { 0xBC, 0x10, 0x00, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an T_Data_Broadcast.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Broadcast_con), 0xBC, 0x10, 0x00, 0x00, 0x00, 0xE1, 0x01, 0x00 }; // T_Data_Broadcast.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (indirect) */

    /* The BDUT receives a frame on the KNX bus, e.g. APCI= A_IndividualAddress_Read */
    tp1_data = { 0xBC, 0x00, 0x01, 0x00, 0x00, 0xE1, 0x01, 0x00, 0 }; // A_IndividualAddress_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();

    /* The BDUT sends a T_Data_Broadcast.ind service */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Broadcast_ind), 0xBC, 0x00, 0x01, 0x00, 0x00, 0xE1, 0x01, 0x00 }; // T_Data_Broadcast.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * T_Data_Individual
 *
 * @ingroup KNX_08_06_03_05_06
 */
TEST(EMI_Tests, Test_5_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Transport Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h, 1000h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Step 1 (direct) */

    /* The BDUT receives a T_Data_Individual.req, e.g. APCI=A_PropertyValue_Read */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Individual_req), 0x0C, 0x00, 0x00, 0xAF, 0xFE, 0x05, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00 }; // T_Data_Individual.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus */
    tp1_data = { 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Read
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends a T_Data_Individual.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Individual_con), 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00 }; // T_Data_Individual.con // @note hop_count_type changed from 0x05 to 0x65
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (indirect) */
    /* The BDUT receives a frame, e.g. APCI=A_PropertyValue_Read */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();

    /* The BDUT sends a T_Data_Individual.ind service */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Individual_ind), 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00 }; // T_Data_Individual.ind // @note hop_count_type changed from 0x05 to 0x65
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * T_Data_PollData
 *
 * @ingroup KNX_08_06_03_05_07
 */
TEST(EMI_Tests, Test_5_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Transport Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h, 1000h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Install a device in the test set-up with polling group FFFEh and answer slot 02h, Data=01h. */
    BCU poll_slave(io_context, tp1_bus);
    poll_slave.interface_objects->device()->polling_group_settings()->polling_group_address = 0xFFFE;
    poll_slave.interface_objects->device()->polling_group_settings()->polling_slot_number = 0x02;
    poll_slave.tp1_data_link_layer.poll_data = 0x01;

    /* Step 1 (direct) */

    /* The BDUT receives a T_PollData.req, number of slots requested = 05h */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Poll_Data_req), 0xF0, 0x00, 0x00, 0xFF, 0xFE, 0x05 }; // T_Poll_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus and fills all unanswered slots with FEh */
    tp1_data = { 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0, 0xFE, 0xFE, 0x01, 0xFE, 0xFE };
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following T_PollData.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Poll_Data_con), 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0xFE, 0xFE, 0x01, 0xFE, 0xFE }; // T_Poll_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/* 6 Testing of Application Layer Services */

/**
 * M_Connect
 *
 * @ingroup KNX_08_06_03_06_01
 */
TEST(EMI_Tests, Test_6_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Select the Management as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x56, 0x78, 0x8A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h, 1000h */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Step 1 (indirect) */

    /* The BDUT receives a frame with TPDU Control Data Open */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();

    /* The BDUT sends the correct M_Connect.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_Connect_ind), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // M_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * M_Disconnect
 *
 * @ingroup KNX_08_06_03_06_02
 */
TEST(EMI_Tests, Test_6_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Select the Management as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x56, 0x78, 0x8A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h,1000h */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Step 1 (indirect) */

    // setup a connection first
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_Connect_ind), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // M_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);


    /* The following data packet is sent on the KNX bus */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x81, 0 }; // T_Disconnect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();

    /* The BDUT sends the correct M_Disconnect.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_Disconnect_ind), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // M_Disconnect.ind // @note control changed from 0x00 to 0xB0. dest changed from 0xAF00 to 0x1000
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * M_User_Data_Connected
 *
 * @ingroup KNX_08_06_03_06_03
 */
TEST(EMI_Tests, Test_6_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Management as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x56, 0x78, 0x8A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h,1000h */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Install a device on the bus with individual address = 0001h (sends IACKs). */
    BCU_Plain_Data bcu2(io_context, tp1_bus);
    bcu2.group_address_table->individual_address = 0x0001;

    /* Ensure that the read or written memory area of the BDUT is not read or write protected. */

    /* Set up the connection. */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_Connect_ind), 0xB0, 0xAF, 0xFE, 0x10, 0x00 }; // M_Connect.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* Step 1 (indirect) */

    /* Generate a frame which contains the following UsrDataRead-PDU */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x64, 0x42, 0xC0, 0x01, 0x00, 0x00, 0 }; // A_UserMemory_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x10, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT generates a M_User_Data_Connected.ind as follows */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_User_Data_Connected_ind), 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x64, 0x02, 0xC0, 0x01, 0x00, 0x00 }; // M_User_Data_Connected.ind // @note Control changed from 0x0C to 0xB0. Dest changed from 0x0000 to 0x1000. hop_count_type changed from 0 to 6.
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 2 (direct) */

    /* Send a M_User_Data_Connected.req to the BDUT with the following contents */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_User_Data_Connected_req), 0x0C, 0x00, 0x00, 0xAF, 0xFE, 0x05, 0x02, 0xC1, 0x01, 0x00, 0x00, 0x00 }; // M_User_Data_Connected.req // @note Dest changed from 0x0000 to 0xAFFE.
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT shall generate a frame containing the following APCI */
    tp1_data = { 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x65, 0x42, 0xC1, 0x01, 0x00, 0x00, 0x00, 0 }; // A_UserMemory_Response // @note Control changed from 0xB0 to 0xBC.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* Send T_Ack */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends a M_User_Data_Connected.con message */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_User_Data_Connected_con), 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x05, 0x02, 0xC1, 0x01, 0x00, 0x00, 0x00 }; // M_User_Data_Connected.con // @note Control changed from 0x0C to 0xBC. Dest changed from 0x0000 to 0xAFFE.
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (indirect) */

    /* Send the following frame to the BDUT */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0x6F, 0x46, 0xC2, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_UserMemory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x10, 0x00, 0xAF, 0xFE, 0x60, 0xC6, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT generates a M_User_Data_Connected.ind as follows */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_User_Data_Connected_ind), 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0x6F, 0x02, 0xC2, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; // M_User_Data_Connected.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 4 (direct) */

    /* Send a M_User_Data_Connected.req to the BDUT with the folowing contents */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_User_Data_Connected_req), 0x0C, 0x00, 0x00, 0xAF, 0xFE, 0x0F, 0x02, 0xC1, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; // M_User_Data_Connected.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT shall generate a frame containing the following UsrDataResponse-PDU */
    tp1_data = { 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x6F, 0x46, 0xC1, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_UserMemory_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* Send T_Ack */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0xC6, 0 }; // T_Ack
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends a M_User_Data_Connected.con message */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_User_Data_Connected_con), 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x0F, 0x02, 0xC1, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }; // M_User_Data_Connected.con // @note Control changed from 0x0C to 0xBC. Dest changed from 0x0000 to 0xAFFE.
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * A_Data_Group
 *
 * @ingroup KNX_08_06_03_06_04
 */
TEST(EMI_Tests, Test_6_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Application Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x56, 0x78, 0x8A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the following address table into the BDUT: 02h,1000h, FFFFh */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xFFFF));

    /* Load the following association table into the BDUT: 01h, 01h, 00h. */
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(0x01), KNX::ASAP_Group(0x00));

    /* Install a device on the bus with individual address = AFFEh (sends IACKs). */
    BCU_Plain_Data bcu2(io_context, tp1_bus);
    bcu2.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* Step 1 (direct) */

    /* Send an A_Data_Group.req to the BDUT, e.g. APCI=A_GroupValue_Read */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::A_Data_Group_req), 0x0C, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x00 }; // A_Data_Group.req // @note dest changed from 0 to 1.
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends a frame on the KNX bus including the Value Read information */
    tp1_data = { 0xBC, 0x10, 0x00, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends a A_Data_Group.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::A_Data_Group_con), 0xBC, 0x10, 0x00, 0x00, 0x01, 0xE1, 0x00, 0x00 }; // A_Data_Group.con // @note source changed from 0x0000 to 0x1000. dest changed from 0 to 1. Hop_count_type changed from 0x00 to 0xE1.
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (indirect) */

    /* The Server transmits a frame on the KNX bus including e.g. APCI=A_GroupValue_Read */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0xFF, 0xFF, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends an A_Data_Group.ind service */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::A_Data_Group_ind), 0xBC, 0xAF, 0xFE, 0x00, 0x01, 0xE1, 0x00, 0x00 }; // A_Data_Group.ind // @note source changed from 0x0000 to 0xAFFE. Dest changed from 0 to 1.
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * M_User_Data_Individual
 *
 * @ingroup KNX_08_06_03_06_05
 */
TEST(EMI_Tests, Test_6_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Management as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x56, 0x78, 0x8A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h, 1000h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Step 1 (direct) */

    /* The BDUT receives a M_User_Data_Individual.req, e.g. APCI=A_PropertyValue_Read */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_User_Data_Individual_req), 0x0C, 0x00, 0x00, 0x00, 0x01, 0x05, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00 }; // M_User_Data_Individual.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus */
    tp1_data = { 0xBC, 0x10, 0x00, 0x00, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Read
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends a M_User_Data_Individual.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_User_Data_Individual_con), 0xBC, 0x10, 0x00, 0x00, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00 }; // M_User_Data_Individual.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (indirect) */

    /* The BDUT receives a frame, e.g. APCI=A_PropertyValue_Read */
    tp1_data = { 0xBC, 0x00, 0x01, 0x10, 0x00, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends a M_User_Data_Individual.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_User_Data_Individual_ind), 0xBC, 0x00, 0x01, 0x10, 0x00, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00 }; // M_User_Data_Individual.ind // @note Control changed from 0x0C to 0xBC.
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * A_PollData
 *
 * @ingroup KNX_08_06_03_06_06
 */
TEST(EMI_Tests, Test_6_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Application Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x56, 0x78, 0x8A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h, 1000h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    /* Install a device in the test set-up with polling group FFFEh and answer slot 02h, Data=01h. */
    BCU poll_slave(io_context, tp1_bus);
    poll_slave.interface_objects->device()->polling_group_settings()->polling_group_address = 0xFFFE;
    poll_slave.interface_objects->device()->polling_group_settings()->polling_slot_number = 0x02;
    poll_slave.tp1_data_link_layer.poll_data = 0x01;

    /* Step 1 (direct) */

    /* The BDUT receives an A_PollData.req, number of slots requested = 05h */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::A_Poll_Data_req), 0xF0, 0x00, 0x00, 0xFF, 0xFE, 0x05 }; // A_Poll_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet on the KNX bus and fills all unanswered slots with FEh */
    tp1_data = { 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0, 0xFE, 0xFE, 0x01, 0xFE, 0xFE };
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following A_PollData.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::A_Poll_Data_con), 0xF0, 0x10, 0x00, 0xFF, 0xFE, 0x05, 0xFE, 0xFE, 0x01, 0xFE, 0xFE }; // A_Poll_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * M_InterfaceObj_Data
 *
 * @ingroup KNX_08_06_03_06_07
 */
TEST(EMI_Tests, Test_6_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Step 1 (direct) */

    /* The BDUT receives an M_InterfaceObj_Data.req service (e.g. APCI=A_PropertyValue_Response) */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_InterfaceObj_Data_req), 0x0C, 0xAF, 0xFE, 0x01, 0x00, 0x05, 0x03, 0xD6, 0x00, 0x00, 0x00, 0x00 }; // M_InterfaceObj_Data.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends a frame on the EHBES bus. */
    tp1_data = { 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an M_InterfaceObj_Data.con service to the user */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_InterfaceObj_Data_con), 0x0C, 0xAF, 0xFE, 0x01, 0x00, 0x05, 0x03, 0xD6, 0x00, 0x00, 0x00, 0x00 }; // M_InterfaceObj_Data.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* Establish a transport layer connection from another bus device to the BDUT. */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT receives an M_InterfaceObj_Data.req service (e.g. APCI=A_PropertyValue_Response) */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_InterfaceObj_Data_req), 0x0C, 0xAF, 0xFE, 0x00, 0x00, 0x05, 0x03, 0xD6, 0x00, 0x00, 0x00, 0x00 }; // M_InterfaceObj_Data.req // @note address changed from 0x0000 to 0xAFFE
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends a frame on the EHBES bus. */
    tp1_data = { 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x65, 0x43, 0xD6, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* Send T_Ack */
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x00, 0x60, 0xC2, 0 }; // T_Ack
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an M_InterfaceObj_Data.con service to the user */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_InterfaceObj_Data_con), 0x0C, 0xAF, 0xFE, 0x00, 0x00, 0x05, 0x03, 0xD6, 0x00, 0x00, 0x00, 0x00 }; // M_InterfaceObj_Data.con // @note Dest changed from 0x0000 to 0xAFFE.
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (indirect) */

    /* The BDUT receives a data frame on the bus. (e.g. APCI=A_PropertyValue_Read) with the connection type flag set to connection less communication */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Read // @note APCI changed from 0x2D6 to 0x2D5
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following EMI message at the PEI */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_InterfaceObj_Data_ind), 0xBC, 0xAF, 0xFE, 0x01, 0x00, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00 }; // M_InterfaceObj_Data.ind // @note Control changed from 0x0C to 0xBC. hop_count_type changed from 0x05 to 0x65. APCI changed from 0x2D6 to 0x2D5.
    ASSERT_EQ(*pei_data_ind++, pei_data);

    tp1_data = { 0xBC, 0x10, 0x00, 0xAF, 0xFE, 0x65, 0x03, 0xD6, 0x00, 0x00, 0x00, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* Step 4 (indirect) */

    /* The BDUT receives a data frame on the bus (e.g. APCI=A_PropertyValue_Read) with the connection type flag set to connection less communication */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x00, 0x65, 0x43, 0xD5, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Read // @note APCI changed from 0x2D6 to 0x2D5
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xB0, 0x10, 0x00, 0xAF, 0xFE, 0x60, 0xC2, 0 }; // T_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following EMI message at the PEI */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::M_InterfaceObj_Data_ind), 0xBC, 0xAF, 0xFE, 0x00, 0x00, 0x65, 0x03, 0xD5, 0x00, 0x00, 0x00, 0x00 }; // M_InterfaceObj_Data.ind // @note Control changed from 0x0C to 0xBC. hop_count_type changed from 0x05 to 0x65. APCI changed from 0x2D6 to 0x2D5
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/* 7 Testing of System Broadcast services */

/**
 * L_Data_SystemBroadcast
 *
 * @ingroup KNX_08_06_03_07_01
 */
TEST(EMI_Tests, Test_7_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0x1101);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Link Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x18, 0x34, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Set the Individual Address of the BDUT to 1101h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* Step 1 (direct) */

    /* The BDUT receives a L_Data_SystemBroadcast.req, e.g. destination addess = broadcast - APCI=A_DomainAddress_Read */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_SystemBroadcast_req), 0x0C, 0x00, 0x00, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // L_SystemBroadcast.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packet (and its repetitions) on the KNX bus corresponding to the L_Data_SystemBroadcast.req with the domain address = 0000h. */
    tp1_data = { 0xAC, 0x11, 0x01, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read // @note Control changed from 0xBC to 0xAC.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the following L_Data_SystemBroadcast.con. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_SystemBroadcast_con), 0xAC, 0x11, 0x01, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // L_SystemBroadcast.con // @note Control changed from 0x9C to 0xAC.
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* The BDUT receives a L_Data_SystemBroadcast.req, e.g. destination address = broadcast, APCI=A_DomainAddress_Read, System priority */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_SystemBroadcast_req), 0x00, 0x00, 0x00, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // L_SystemBroadcast.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT transmits the data packet (and its repetitions) on the KNX bus corresponding to the L_Data_SystemBroadcast.req */
    tp1_data = { 0xA0, 0x11, 0x01, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read // @note Control changed from 0xB0 to 0xA0.
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an according L_Data_SystemBroadcast.con service */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_SystemBroadcast_con), 0xA0, 0x11, 0x01, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // L_SystemBroadcast.con // @note Control changed from 0x90 to 0xA0.
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (indirect) */

    /* The BDUT receives a data packet, which is acknowledged by an IACK (only applicable to PL devices with IACK capability) */
    tp1_data = { 0xAC, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read // @note Control changed from 0xBC to 0xAC // @note SA changed from 0x1101 to 0x7FFE to avoid individual address duplication
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data_SystemBroadcast.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_SystemBroadcast_ind), 0xAC, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // L_Data_SystemBroadcast.ind // @note Control changed from 0xBC to 0xAC // @note SA changed from 0x1101 to 0x7FFE to avoid individual address duplication
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 4 (indirect) */

    /* The BDUT receives a repeated data packet with system priority (which is acknowledged by an IACK - only applicable to PL devices with IACK capability) */
    tp1_data = { 0xA0, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read // @note Control changed from 0x90 to 0xA0
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data_SystemBroadcast.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_SystemBroadcast_ind), 0xA0, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // L_SystemBroadcast.ind // @note Control changed from 0x90 to 0xA0
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * N_Data_SystemBroadcast
 *
 * @ingroup KNX_08_06_03_07_02
 */
TEST(EMI_Tests, Test_7_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0x7FFE);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Network-Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x88, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Set the routing counter of the BDUT to the value 6. */
    bdut.interface_objects->router()->hop_count()->hop_count = 6;

    /* Load the BDUT with the following address table: 01h, 1101h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* Step 1 (direct) */

    for (const KNX::Hop_Count hop_count : {
                6, 5, 4, 3, 2, 1, 0
            }) {
        /* Send N_Data_SystemBroadcast.req services to the BDUT with routing counters 6, 5, 4, 3, 2, 1, 0, e.g. APCI=A_IndividualAddress_Read */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_req), 0x0C, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0xE1 }; // N_Data_Broadcast.req
        pei_data[6] = (pei_data[6] & 0x8f) | (hop_count << 4);
        bdut.emi_req(pei_data);
        io_context.poll();

        /* The BDUT sends corresponding frames on the KNX with routing counter 6 */
        tp1_data = { 0xBC, 0x11, 0x01, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_IndividualAddress_Read
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT also sends N_Data_SystemBroadcast.con services with routing counter 6 */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_con), 0xBC, 0x11, 0x01, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // N_Data_Broadcast.con // @note Control changed from 0x9C to 0xBC.
        ASSERT_EQ(*pei_data_con++, pei_data);
    }

    /* Step 2 (direct) */

    /* Send an N_Data_SystemBroadcast.req service to the BDUT, e.g. APCI=A_DomainAddress_Read, routing counter =7 */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_req), 0x0C, 0x00, 0x00, 0x00, 0x00, 0x71, 0x03, 0xE1 }; // N_Data_Broadcast.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the corresponding frames on the KNX bus with routing counter 7 */
    tp1_data = { 0xBC, 0x11, 0x01, 0x00, 0x00, 0xF1, 0x03, 0xE1, 0 }; // A_IndividualAddress_Read
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an N_Data_SystemBroadcast.con with routing counter 7 */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_con), 0xBC, 0x11, 0x01, 0x00, 0x00, 0xF1, 0x03, 0xE1 }; // N_Data_Broadcast.con // @note Control changed from 0x9C to 0xBC.
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (indirect) */

    for (const KNX::Hop_Count hop_count : {
                7, 6, 5, 4, 3, 2, 1, 0
            }) {
        /* Send system-broadcast frames on the KNX bus with routing counter 7, 6, 5, 4, 3, 2, 1, 0, e.g. APCI=A_DomainAddress_Read */
        tp1_data = { 0xBC, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_IndividualAddress_Read
        tp1_data[5] = (tp1_data[5] & 0x8f) | (hop_count << 4);
        bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
        io_context.poll();
        ASSERT_EQ(*log++, tp1_data);
        tp1_data = { 0xCC }; // L_Ack
        ASSERT_EQ(*log++, tp1_data);

        /* The BDUT sends N_Data_Broadcast.ind services with the same routing counters */
        pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::N_Data_Broadcast_ind), 0xBC, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // N_Data_Broadcast.ind
        pei_data[6] = (pei_data[6] & 0x8f) | (hop_count << 4);
        ASSERT_EQ(*pei_data_ind++, pei_data);
    }
}

/**
 * T_Data_SystemBroadcast
 *
 * @ingroup KNX_08_06_03_07_03
 */
TEST(EMI_Tests, Test_7_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Select the Transport Layer as working layer for the BDUT. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Load the BDUT with the following address table: 01h, 1101h. */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* Step 1 (direct) */

    /* The BDUT receives a T_Data_SystemBroadcast.req, e.g. APCI=A_DomainAddress_Read */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_SystemBroadcast_req), 0x0C, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0xE1 }; // T_Data_SystemBroadcast.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT sends the data packets on the KNX bus corresponding to the T_Data_SystemBroadcast.req */
    tp1_data = { 0xAC, 0x11, 0x01, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read // @note Control changed from 0xBC to 0xAC.
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT also sends an T_Data_SystemBroadcast.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_SystemBroadcast_con), 0xAC, 0x11, 0x01, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // T_Data_SystemBroadcast.con // @note Control changed from 0x9C to 0xAC
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (indirect) */

    /* The BDUT receives a frame on the KNX bus, e.g. APCI=A_DomainAddress_Read */
    tp1_data = { 0xAC, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0 }; // A_DomainAddress_Read // @note Control changed from 0xBC to 0xAC
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();

    /* The BDUT sends a T_Data_SystemBroadcast.ind service */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_SystemBroadcast_ind), 0xAC, 0x7F, 0xFE, 0x00, 0x00, 0xE1, 0x03, 0xE1 }; // T_Data_SystemBroadcast.ind // @note Control changed from 0xBC to 0xAC
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/* 8 Testing of User services */

/**
 * U_ValueRead
 *
 * @ingroup KNX_08_06_03_08_01
 */
TEST(EMI_Tests, Test_8_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Load an application program with a Group Object table as described below */
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.application_interface_layer.group_object_server.group_object_table;
    bdut.application_interface_layer.group_object_server.group_object_table = group_object_table;
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_2_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_3_value = std::make_shared<std::vector<uint8_t>>(14);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].type = KNX::Group_Object_Type::Unsigned_Integer_7;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].data = group_object_2_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].type = KNX::Group_Object_Type::Octet_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].data = group_object_3_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].type = KNX::Group_Object_Type::Octet_14;

    /* Ensure that the application program is not running */

    /* Step 1 (direct) */

    /* Set the RAM-flags of Group Object 0, as listed below */
    /* Update = 1, Data Request = 0, Transmission Status = 00 */
    group_object_table->communication_flags[KNX::ASAP_Group(0)].update = true;
    group_object_table->communication_flags[KNX::ASAP_Group(0)].data_request = false;
    group_object_table->communication_flags[KNX::ASAP_Group(0)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok;

    /* Set the value of Group Object 0 to 1 */
    *group_object_0_value = { 0x01 };

    /* A client sends the U_ValueRead.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Read_req), 0x00 }; // U_Value_Read.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with a U_ValueRead.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Read_con), 0x00, 0x08, 0x01 }; // U_Value_Read.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* Set the RAM-flags of Group Object 1, as listed below */
    /* Update = 0, Data Request = 1, Transmission Status = 00 */
    group_object_table->communication_flags[KNX::ASAP_Group(1)].update = false;
    group_object_table->communication_flags[KNX::ASAP_Group(1)].data_request = true;
    group_object_table->communication_flags[KNX::ASAP_Group(1)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok;

    /* Set the value of Group Object 1 to 7FH. */
    *group_object_1_value = { 0x7F };

    /* A client sends the U_ValueRead.request below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Read_req), 0x01 }; // U_Value_Read.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with a U_ValueRead.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Read_con), 0x01, 0x04, 0x7F }; // U_Value_Read.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (direct) */

    /* Set the RAM-flags of Group Object 2, as listed below */
    /* Update = 0, Data Request = 0, Transmission Status = 01 */
    group_object_table->communication_flags[KNX::ASAP_Group(2)].update = false;
    group_object_table->communication_flags[KNX::ASAP_Group(2)].data_request = false;
    group_object_table->communication_flags[KNX::ASAP_Group(2)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_error;

    /* Set the value of Group Object 2 to 80H. */
    *group_object_2_value = { 0x80 };

    /* A client sends a U_ValueRead.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Read_req), 0x02 }; // U_Value_Read.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with a U_ValueRead.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Read_con), 0x02, 0x01, 0x80 }; // U_Value_Read.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 4 (direct) */

    /* Set the RAM-flags of Group Object 3, as listed below */
    /* Update = 0, Data Request = 0, Transmission Status = 00 */
    group_object_table->communication_flags[KNX::ASAP_Group(3)].update = false;
    group_object_table->communication_flags[KNX::ASAP_Group(3)].data_request = false;
    group_object_table->communication_flags[KNX::ASAP_Group(3)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok;

    /* Set the value of Group Object 3 to 81H 82H 83H 84H 85H 86H 87H 88H 89H 8AH 8BH 8CH 8DH 8EH. */
    *group_object_3_value = { 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E };

    /* A client sends a U_ValueRead.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Read_req), 0x03 }; // U_Value_Read.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with a U_ValueRead.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Read_con), 0x03, 0x00, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E }; // U_Value_Read.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * U_ValueWrite
 *
 * @ingroup KNX_08_06_03_08_02
 */
TEST(EMI_Tests, Test_8_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);

    BCU bdut(io_context, tp1_bus);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Load an application program with a Group Object table as described below */
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.application_interface_layer.group_object_server.group_object_table;
    bdut.application_interface_layer.group_object_server.group_object_table = group_object_table;
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_2_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_3_value = std::make_shared<std::vector<uint8_t>>(14);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].type = KNX::Group_Object_Type::Unsigned_Integer_7;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].data = group_object_2_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(2)].type = KNX::Group_Object_Type::Octet_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].data = group_object_3_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(3)].type = KNX::Group_Object_Type::Octet_14;

    /* Make sure the application program is not running. */

    /* Step 1 (direct) */

    /* Set the Group Object 0 in the BDUT, as listed below */
    /* Update-Flag = 0, Data Request-Flag = 0, Transmission Status = 00 */
    group_object_table->communication_flags[KNX::ASAP_Group(0)].update = false;
    group_object_table->communication_flags[KNX::ASAP_Group(0)].data_request = false;
    group_object_table->communication_flags[KNX::ASAP_Group(0)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok;
    /* Value = 0 */
    *group_object_0_value = { 0x00 };

    /* A client sends a U_ValueWrite.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Write_req), 0x00, 0xCF, 0x01 }; // U_Value_Write.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* In the BDUT the Group Object 0 is set as specified below */
    /* Update-Flag = 1, Data Request-Flag = 0, Transmission Status = 00 */
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(0)].update, true);
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(0)].data_request, false);
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(0)].transmission_status, KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok);
    /* Value = 01H */
    std::vector<uint8_t> expected_value { 0x01 };
    ASSERT_EQ(*group_object_0_value, expected_value);

    /* Step 2 (direct) */

    /* Set the Group Object 1 in the BDUT, as listed below */
    /* Update-Flag = 0, Data Request-Flag = 0, Transmission Status = 00 */
    group_object_table->communication_flags[KNX::ASAP_Group(1)].update = false;
    group_object_table->communication_flags[KNX::ASAP_Group(1)].data_request = false;
    group_object_table->communication_flags[KNX::ASAP_Group(1)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok;
    /* Value = 0 */
    *group_object_1_value = { 0x00 };

    /* A client sends a U_ValueWrite.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Write_req), 0x01, 0xAF, 0x7F }; // U_Value_Write.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* In the BDUT the Group Object 1 is set as specified below */
    /* Update-Flag = 0, Data Request-Flag = 1, Transmission Status = 00 */
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(1)].update, false);
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(1)].data_request, true);
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(1)].transmission_status, KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok);
    /* Value = 7FH */
    expected_value = { 0x7F };
    ASSERT_EQ(*group_object_1_value, expected_value);

    /* Step 3 (direct) */

    /* Set the Group Object 2 in the BDUT, as listed below */
    /* Update-Flag = 0, Data Request-Flag = 0, Transmission Status = 00 */
    group_object_table->communication_flags[KNX::ASAP_Group(2)].update = false;
    group_object_table->communication_flags[KNX::ASAP_Group(2)].data_request = false;
    group_object_table->communication_flags[KNX::ASAP_Group(2)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok;
    /* Value = 0 */
    *group_object_2_value = { 0x00 };

    /* A client sends a U_ValueWrite.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Write_req), 0x02, 0x9D, 0xFF }; // U_Value_Write.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* In the BDUT the Group Object 2 is set as specified below */
    /* Update-Flag = 0, Data Request-Flag = 0, Transmission Status = 01 */
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(2)].update, false);
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(2)].data_request, false);
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(2)].transmission_status, KNX::Group_Object_Communication_Flags::Transmission_Status::idle_error);
    /* Value = FFH */
    expected_value = { 0xFF };
    ASSERT_EQ(*group_object_2_value, expected_value);

    /* Step 4 (direct) */

    /* Set the Group Object 3 in the BDUT, as listed below */
    /* Update-Flag = 0, Data Request-Flag = 0, Transmission Status = 00 */
    group_object_table->communication_flags[KNX::ASAP_Group(3)].update = false;
    group_object_table->communication_flags[KNX::ASAP_Group(3)].data_request = false;
    group_object_table->communication_flags[KNX::ASAP_Group(3)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok;
    /* Value = 0..0H */
    *group_object_3_value = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    /* A client sends a U_ValueWrite.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Value_Write_req), 0x03, 0x8F, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E }; // U_Value_Write.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* In the BDUT the Group Object 3 is set as specified below */
    /* Update-Flag = 0, Data Request-Flag = 0, Transmission Status = 00 */
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(3)].update, false);
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(3)].data_request, false);
    ASSERT_EQ(group_object_table->communication_flags[KNX::ASAP_Group(3)].transmission_status, KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok);
    /* Value = 81H 82H 83H 84H 85H 86H 87H 88H 89H 8AH 8BH 8CH 8DH 8EH */
    expected_value = { 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E };
    ASSERT_EQ(*group_object_3_value, expected_value);
}

/**
 * U_FlagsRead
 *
 * @ingroup KNX_08_06_03_08_03
 */
TEST(EMI_Tests, Test_8_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Load an application program with a Group Object table as described below */
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.application_interface_layer.group_object_server.group_object_table;
    bdut.application_interface_layer.group_object_server.group_object_table = group_object_table;
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(2);
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.transmit_enable = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.segment_selector_type = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.write_enable = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.read_enable = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.communication_enable = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].config.transmission_priority = KNX::Priority::system;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(1)].type = KNX::Group_Object_Type::Octet_2;

    /* Make sure the application program is not running. */

    /* Step 1 (direct) */

    /* Set the RAM-flags of Group Object 0, as listed below */
    /* Update = 1, Data Request = 1, Transmission Status = 00 */
    group_object_table->communication_flags[KNX::ASAP_Group(0)].update = true;
    group_object_table->communication_flags[KNX::ASAP_Group(0)].data_request = true;
    group_object_table->communication_flags[KNX::ASAP_Group(0)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_ok;

    /* A client sends an A_FlagsRead.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Flags_Read_req), 0x00 }; // U_Flags_Read.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with the A_FlagsRead.confirm specified below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Flags_Read_con), 0x00, 0x0C, 0xDF, 0x00 }; // U_Flags_Read.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* Set the RAM-flags of Group Object 1, as listed below */
    /* Update = 0, Data Request = 0, Transmission Status = 01 */
    group_object_table->communication_flags[KNX::ASAP_Group(1)].update = false;
    group_object_table->communication_flags[KNX::ASAP_Group(1)].data_request = false;
    group_object_table->communication_flags[KNX::ASAP_Group(1)].transmission_status = KNX::Group_Object_Communication_Flags::Transmission_Status::idle_error;

    /* A client sends an A_FlagsRead.req */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Flags_Read_req), 0x01 }; // U_Flags_Read.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with an A_FlagsRead.con */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Flags_Read_con), 0x01, 0x01, 0xA0, 0x08 }; // U_Flags_Read.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * U_Event
 *
 * @ingroup KNX_08_06_03_08_04
 */
TEST(EMI_Tests, Test_8_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Load an application program with the following Group Object table into the BDUT */
    std::shared_ptr<KNX::Group_Object_Table> group_object_table = bdut.interface_objects->group_object_table();
    std::shared_ptr<std::vector<uint8_t>> group_object_0_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_1_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_2_value = std::make_shared<std::vector<uint8_t>>(1);
    std::shared_ptr<std::vector<uint8_t>> group_object_3_value = std::make_shared<std::vector<uint8_t>>(14);
    std::shared_ptr<std::vector<uint8_t>> group_object_4_value = std::make_shared<std::vector<uint8_t>>();
    std::shared_ptr<std::vector<uint8_t>> group_object_5_value = std::make_shared<std::vector<uint8_t>>();
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0100)].data = group_object_0_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0100)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0100)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0100)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0100)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0100)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0100)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0100)].type = KNX::Group_Object_Type::Unsigned_Integer_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0200)].data = group_object_1_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0200)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0200)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0200)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0200)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0200)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0200)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0200)].type = KNX::Group_Object_Type::Unsigned_Integer_7;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0301)].data = group_object_2_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0301)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0301)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0301)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0301)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0301)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0301)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0301)].type = KNX::Group_Object_Type::Octet_1;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0302)].data = group_object_3_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0302)].config.transmit_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0302)].config.segment_selector_type = false;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0302)].config.write_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0302)].config.read_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0302)].config.communication_enable = true;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0302)].config.transmission_priority = KNX::Priority::low;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0302)].type = KNX::Group_Object_Type::Octet_14;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0403)].data = group_object_4_value;
    group_object_table->group_object_descriptors[KNX::ASAP_Group(0x0500)].data = group_object_5_value;
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0x0100)), 0);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0x0200)), 1);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0x0301)), 2);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0x0302)), 3);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0x0403)), 4);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->group_object_number(KNX::ASAP_Group(0x0500)), 5);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(0), 0x0100);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(1), 0x0200);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(2), 0x0301);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(3), 0x0302);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(4), 0x0403);
    ASSERT_EQ(bdut.application_interface_layer.group_object_server.group_object_table->asap(5), 0x0500);

    /* Setup the address table as follows */
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x0001)); // Group_Address -> TSAP 1
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x0002)); // Group_Address -> TSAP 2
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x0003)); // Group_Address -> TSAP 3
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x0004)); // Group_Address -> TSAP 4
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0x0005)); // Group_Address -> TSAP 5
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(1)), 0x0001);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(2)), 0x0002);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(3)), 0x0003);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(4)), 0x0004);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(5)), 0x0005);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x0001)), 1);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x0002)), 2);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x0003)), 3);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x0004)), 4);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->tsap(KNX::Group_Address(0x0005)), 5);

    /* Setup the association table as follows */
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(0x0100)); // TSAP 1 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(2), KNX::ASAP_Group(0x0200)); // TSAP 2 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(3), KNX::ASAP_Group(0x0301)); // TSAP 3 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(4), KNX::ASAP_Group(0x0302)); // TSAP 4 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(5), KNX::ASAP_Group(0x0403)); // TSAP 5 -> ASAP
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(6), KNX::ASAP_Group(0x0500)); // TSAP 6 -> ASAP
    // following tests are in addition to test specification
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0x0100)), 1);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0x0200)), 2);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0x0301)), 3);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0x0302)), 4);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0x0403)), 5);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->tsap(KNX::ASAP_Group(0x0500)), 6);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(1)), 0x0100);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(2)), 0x0200);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(3)), 0x0301);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(4)), 0x0302);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(5)), 0x0403);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(6)), 0x0500);

    /* Enable U_Event message generation (BCU1 / BCU2: set bit 6 in ConfigDes) */
    bdut.application_interface_layer.group_object_server.U_Event_ind_message_generation = true;

    /* Make sure that the application program is not running */

    /* Step 1 (indirect) */

    /* Send a group frame with Group Address 0004h to the BDUT. */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x04, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();

    /* BDUT generates a U_Event.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Event_ind), 0x04 }; // U_Event.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 2 (indirect) */

    /* Send a group frame with Group Address 0003h to BDUT. */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x03, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();

    /* The BDUT sends a U_Event.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Event_ind), 0x03 }; // U_Event.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 3 (indirect) */

    /* Send group messages with the Group Addresses 0001h, 0002h, 0005h. Generate the group messages in the order given by the Group Addresses. */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x01, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x02, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x00, 0x05, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();

    /* The following U_Event.ind messages shall be generated by BDUT in the given order. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Event_ind), 0x01 }; // U_Event.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Event_ind), 0x02 }; // U_Event.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::U_Event_ind), 0x05 }; // U_Event.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * U_UserData
 *
 * @ingroup KNX_08_06_03_08_05
 */
TEST(EMI_Tests, Test_8_5)
{
}

/* 9 Testing of other default EMI services */

/**
 * PC_GetValue
 *
 * @ingroup KNX_08_06_03_09_01
 */
TEST(EMI_Tests, Test_9_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Disable any application program (e.g. in BCU1/BCU2 by setting the octet 10DH = 00H). */

    /* From a starting address SA set the data to the following values: 11H, 22H, 33H, 44H, 55H, 66H, 77H, 88H, 99H, AAH, BBH, CCH, DDH, EEH, FFH */
    bdut.memory[0x0120] = {
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF
    };

    /* Step 1 (direct) */

    /* A client sends the PC_GetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_req), 0x01, 0x01, 0x20 }; // PC_Get_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with the PC_GetValue.con specified below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_con), 0x01, 0x01, 0x20, 0x11 }; // PC_Get_Value.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* A client sends the PC_GetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_req), 0x0F, 0x01, 0x20 }; // PC_Get_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with the PC_GetValue.con specified below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_con), 0x0F, 0x01, 0x20, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF }; // PC_Get_Value.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * PC_SetValue
 *
 * @ingroup KNX_08_06_03_09_02
 */
TEST(EMI_Tests, Test_9_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Disable any application program (e.g. in BCU1/BCU2 by setting the octet 10DH = 00H). */

    /* Set the data starting at starting address1 (SA1, UsrRAM) and at starting address2 (SA2, UsrEEPROM) to the value 00H. For BCU1/BCU2 the starting addresses shall be: SA1=0CEH and SA2=120H. For CU 0701 the starting addresses shall be: SA1=0700h and SA2=4100h. */
    bdut.memory[0x0120] = {
        0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF
    };

    /* Step 1 (direct) */

    /* A client sends the PC_SetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Set_Value_req), 0x01, 0x01, 0x20, 0x12 }; // PC_Set_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Then the client sends the PC_GetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_req), 0x01, 0x01, 0x20 }; // PC_Get_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with the PC_GetValue.con specified below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_con), 0x01, 0x01, 0x20, 0x12 }; // PC_Get_Value.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 2 (direct) */

    /* A client sends the PC_SetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Set_Value_req), 0x0F, 0x01, 0x20, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF }; // PC_Set_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Then the client sends the PC_GetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_req), 0x0F, 0x01, 0x20 }; // PC_Get_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with the PC_GetValue.con specified below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_con), 0x0F, 0x01, 0x20, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF }; // PC_Get_Value.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 3 (direct) */

    /* A client sends the PC_SetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Set_Value_req), 0x01, 0x01, 0x20, 0x12 }; // PC_Set_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Then the client sends the PC_GetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_req), 0x01, 0x01, 0x20 }; // PC_Get_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with the PC_GetValue.con specified below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_con), 0x01, 0x01, 0x20, 0x12 }; // PC_Get_Value.con
    ASSERT_EQ(*pei_data_con++, pei_data);

    /* Step 4 (direct) */

    /* A client sends the PC_SetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Set_Value_req), 0x0F, 0x01, 0x20, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF }; // PC_Set_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Then the client sends the PC_GetValue.req below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_req), 0x0F, 0x01, 0x20 }; // PC_Get_Value.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with the PC_GetValue.con specified below */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PC_Get_Value_con), 0x0F, 0x01, 0x20, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF }; // PC_Get_Value.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * PEI_Identify
 *
 * @ingroup KNX_08_06_03_09_03
 */
TEST(EMI_Tests, Test_9_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_con = std::cbegin(bdut.pei_data_con);
    std::vector<uint8_t> pei_data;
    bdut.interface_objects->device()->serial_number()->serial_number = { 0x11, 0x22, 0x33, 0x44, 0x55, 0x66 };
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Step 1 (direct) */

    /* An external user sends a PEI_Identify.req message */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Identify_req) }; // PEI_Identify.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* The BDUT responds with a PEI_Identify.con message */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Identify_con), 0x10, 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66 }; // PEI_Identify.con
    ASSERT_EQ(*pei_data_con++, pei_data);
}

/**
 * PEI_Switch
 *
 * @ingroup KNX_08_06_03_09_04
 */
TEST(EMI_Tests, Test_9_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1000);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xFFFE));

    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    /* Step 1 (direct) */

    /* An external user sends a PEI_Switch.req message */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x18, 0x34, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Check whether the BDUT has switched to link layer by sending a frame on the bus addressed to the BDUT. */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct L_Data.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Data_ind), 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80 }; // L_Data.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 2 (direct) */

    /* An external user sends a PEI_Switch.req message. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x90, 0x18, 0x34, 0x56, 0x78, 0x0A }; // PEI_Switch.req
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Check whether the BDUT has switched to link layer in busmonitor mode by sending a frame on the bus addressed to the BDUT. */
    tp1_data = { 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    /* The BDUT sends the correct L_Busmon.ind */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::L_Busmon_ind), 0x00, 0x00, 0x00, 0xBC, 0xAF, 0xFE, 0xFF, 0xFE, 0xE1, 0x00, 0x80, 0 }; // L_Busmon.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 3 (direct) */

    /* An external user sends a PEI_Switch.req message. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x48, 0x88, 0x0A }; // PEI_Switch.req // @note Changed NL from PEI (0x18) to NL (0x12)
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Check whether the BDUT has switched to TLC and TLG by sending group and individual addressed frames on the bus addressed to the BDUT. */
    tp1_data = { 0xB0, 0x00, 0x01, 0x10, 0x00, 0x60, 0x80, 0 }; // T_Connect
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xBC, 0x00, 0x01, 0xFF, 0xFE, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read // @note Dest changed from 0xFFFF to 0xFFFE.
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends the correct T_Connect.ind and T_Data_Group.ind */
    //pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x80 }; // T_Connect.ind
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Connect_ind), 0xB0, 0x00, 0x01, 0x10, 0x00 }; // @note Control, Source and Dest changed from 0x0000.
    ASSERT_EQ(*pei_data_ind++, pei_data);
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::T_Data_Group_ind), 0xBC, 0x00, 0x01, 0x00, 0x01, 0xE1, 0x00, 0x00 }; // T_Data_Group.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    /* Step 4 (direct) */

    /* An external user sends a PEI_Switch.req message. */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::PEI_Switch_req), 0x00, 0x12, 0x34, 0x56, 0x78, 0x8A }; // PEI_Switch.req // @note Changed NL from PEI (0x18) to NL (0x12)
    bdut.emi_req(pei_data);
    io_context.poll();

    /* Check whether the BDUT has switched to AL by sending a frame on the bus addressed to the BDUT. */
    tp1_data = { 0xBC, 0x00, 0x01, 0xFF, 0xFE, 0xE1, 0x00, 0x00, 0 }; // A_GroupValue_Read // @note Dest changed from 0xFFFF to 0xFFFE.
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    /* The BDUT sends an A_Data_Group.ind service */
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::A_Data_Group_ind), 0xBC, 0x00, 0x01, 0x00, 0x01, 0xE1, 0x00, 0x00 }; // A_Data_Group.ind // @note Dest changed from 0x0000 to 0x0001 (1st group address).
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

/**
 * TM_Timer
 *
 * @ingroup KNX_08_06_03_09_05
 */
TEST(EMI_Tests, Test_9_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    KNX::TP1_Bus_Simulation tp1_bus(io_context);

    BCU bdut(io_context, tp1_bus);
    std::deque<std::vector<uint8_t>>::const_iterator pei_data_ind = std::cbegin(bdut.pei_data_ind);
    std::vector<uint8_t> pei_data;

    BCU_Plain_Data bcu(io_context, tp1_bus);

    /* Step 1 (direct) */

    /* Start a timer the option "message generation" and the addressed module = "PEI". */

    /* BDUT generates a TM_Timer.ind */
    bdut.TM_Timer_ind(1, 1, 2);
    bdut.TM_Timer_ind(2, 3, 4);

    // Format style 1 (Control Unit)
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::TM_Timer_ind), 0x02 }; // TM_Timer.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);

    // Format style 2 (BCU 2)
    pei_data = { static_cast<uint8_t>(KNX::EMI_Message_Code::TM_Timer_ind), 0x03, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00 }; // TM_Timer.ind
    ASSERT_EQ(*pei_data_ind++, pei_data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
