# SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
#
# SPDX-License-Identifier: GPL-3.0-or-later

# tests
add_executable(Interworking_Tests "")
target_sources(Interworking_Tests
    PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/Interworking_Tests.cpp)
target_link_libraries(Interworking_Tests
    PRIVATE GTest::GTest KNX)
gtest_add_tests(TARGET Interworking_Tests)
set_target_properties(Interworking_Tests PROPERTIES
    CXX_EXTENSIONS OFF
    CXX_STANDARD 14
    CXX_STANDARD_REQUIRED ON)
