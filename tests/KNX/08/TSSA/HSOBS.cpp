// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "HSOBS.h"

namespace KNX {

HSOBS::HSOBS()
{
}

HSOBS::~HSOBS()
{
    sequence_file.close();
    session_file.close();
}

void HSOBS::load_session_file(const std::string filename)
{
    session_file.open(filename, std::fstream::in);
}

void HSOBS::load_sequence_file(const std::string filename)
{
    sequence_file.open(filename, std::fstream::in);
}

void HSOBS::run_test()
{

}

}
