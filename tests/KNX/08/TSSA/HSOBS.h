// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#pragma once

#include <fstream>
#include <string>

#include <KNX/knx_export.h>

namespace KNX {

/**
 * HSOBS V4.3 Emulation
 *
 * Frame observation and generation software
 *
 * @ingroup KNX_08_TSSA
 */
class KNX_EXPORT HSOBS
{
public:
    explicit HSOBS();
    virtual ~HSOBS();

    /** load session file (*.SES) */
    void load_session_file(const std::string filename);

    /** load sequence file (*.SEQ) */
    void load_sequence_file(const std::string filename);

    void run_test();

private:
    std::fstream session_file{};
    std::fstream sequence_file{};
};

}
