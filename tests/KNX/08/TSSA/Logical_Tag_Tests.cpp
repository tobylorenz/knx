// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/03/02/01_Twisted_Pair_0.h>
#include <KNX/03/02/04_Powerline_132.h>
#include "PL132_BCU2.h"
#include "TP0_BCU2.h"

class Logical_Tag_Tests : public ::testing::Test
{
    virtual ~Logical_Tag_Tests() = default;
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class TP0_Plain_Data
{
public:
    explicit TP0_Plain_Data(asio::io_context & io_context, KNX::TP0_Bus_Simulation & tp0_bus_simulation) :
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp0_physical_layer(io_context),
        tp0_data_link_layer(tp0_physical_layer, io_context)
    {
        tp0_physical_layer.connect(tp0_bus_simulation);

        tp0_data_link_layer.group_address_table = group_address_table;
        tp0_data_link_layer.L_Busmon_ind = std::bind(&TP0_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP0_Physical_Layer_Simulation tp0_physical_layer;

    /** TP1 data link layer */
    KNX::TP0_Data_Link_Layer tp0_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

class PL132_Plain_Data
{
public:
    explicit PL132_Plain_Data(asio::io_context & io_context, KNX::PL132_Bus_Simulation & pl132_bus_simulation) :
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        pl132_physical_layer(io_context),
        pl132_data_link_layer(pl132_physical_layer, io_context)
    {
        pl132_physical_layer.connect(pl132_bus_simulation);

        pl132_data_link_layer.group_address_table = group_address_table;
        pl132_data_link_layer.L_Busmon_ind = std::bind(&PL132_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::PL132_Physical_Layer_Simulation pl132_physical_layer;

    /** TP1 data link layer */
    KNX::PL132_Data_Link_Layer pl132_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/* 2 E-Mode LT Reflex testing */

/**
 * 2.1 CEMO/LTR/AD
 *
 * @ingroup KNX_08_TSSA_02_01
 * @todo Test_2_1
 */
TEST(Logical_Tag_Tests, Test_2_1)
{
    // CEMOAD1.SEQ

    // TP0
    // # LT Reflex
    // # 2BP-2LED application, x/y = 8/4
    // # Test 1
    // # Modification of LED status, addressed to Group Address 9F01h
    // # IN, LED on (Service A-GroupValue-Write)
    // 3000,i,CD 91 9F 01 01 45 00 81 0A 66
    // # Expected : LED switches on
    // # IN, verify the previous write (Service A-GroupValue-read sent by another device)
    // 1000,i,CD 91 9F 01 01 B7 00 00 BB 2E
    // # OUT, Expected Service A-GroupValue-Response: CD 91 9F 01 01 FF 00 41 0B 77
    // # LED off
    // 6000,i,CD 91 9F 01 01 45 00 80 0A 65
    // # Expected : LED switches off

    // CEMOAD2.SEQ

    // TP0
    // # LT Reflex, connection-code = 01
    // # 2BP-2LED application, x/y = 16/1
    // # IN, LED on (Service A-GroupValue-Write)
    // 3000,i,CD 91 BC 01 01 45 00 81 0A 66
    // # Expected : no action
    // # IN, (Service A-GroupValue-read sent by another device)
    // 1000,i,CD 91 BC 01 01 B7 00 00 BB 2E
    // # OUT, Expected : no response
    // # 2BP-2LED application, x/y = 16/2
    // # IN, LED on (Service A-GroupValue-Write)
    // 3000,i,CD 91 BD 01 01 45 00 81 0A 66
    // # Expected : no action
    // # IN, (Service A-GroupValue-read sent by another device)
    // 1000,i,CD 91 BD 01 01 B7 00 00 BB 2E
    // # OUT, Expected : no response
    // # x/y = 16/3
    // # IN, LED on (Service A-GroupValue-Write)
    // 3000,i,CD 91 BE 01 01 45 00 81 0A 66
    // # Expected : LED switches on
    // # IN, verify the previous write (Service A-GroupValue-read sent by another device)
    // 1000,i,CD 91 BE 01 01 B7 00 00 BB 2E
    // # OUT, Expected Service A-GroupValue-Response: CD 91 9F 01 01 FF 00 41 0B 77
    // # LED off
    // 6000,i,CD 91 BE 01 01 45 00 80 0A 65
    // # Expected : LED switches off
    // # x/y = 16/4
    // # IN, LED on (Service A-GroupValue-Write)
    // 3000,i,CD 91 BF 01 01 45 00 81 0A 66
    // # Expected : no action
    // # IN, (Service A-GroupValue-read sent by another device)
    // 1000,i,CD 91 BF 01 01 B7 00 00 BB 2E
    // # OUT, Expected : no response

}

/**
 * 2.2 CEMO/LTR/CH
 *
 * @ingroup KNX_08_TSSA_02_02
 * @todo Test_2_2
 */
TEST(Logical_Tag_Tests, Test_2_2)
{
    // CEMOCH1.SEQ

    // TP0
    // # LT reflex
    // # Channel numbers 1 and 2 (GA = 9E 01 and 9F01)
    // # verify services A-groupvalue-read, A-groupvalue-write
    // # IN, Value read request, channel 1
    // 1000,i,CD 91 9E 01 01 45 00 00 0A 65
    // # OUT, Expected Service A-GroupValue-Response: CD 91 9E 01 01 FF 00 41 (or 40) 0B 77
    // # IN, Value read request, channel 2
    // 1000,i,CD 91 9F 01 01 45 00 00 0A 65
    // # OUT, Expected Service A-GroupValue-Response: CD 91 9F 01 01 FF 00 41 (or 40) 0B 77
    // # IN, Group write ON, channel 1
    // 5000,i,CD 91 9E 01 01 45 00 81 0A 65
    // # OUT, Expected LED switches on
    // # IN, Group write OFF, channel 1
    // 3000,i,CD 91 9E 01 01 FF 00 80 0A 65
    // # Expected LED switches off
    // # IN, Group write ON, channel 2
    // 5000,i,CD 91 9F 01 01 43 00 81 0A 65
    // # Expected LED switches on
    // # IN, Group write OFF, channel 2
    // 3000,i,CD 91 9F 01 01 43 00 80 0A 65
    // # Expected LED switches off
    // # IN, Descriptor type 2, Individual Address of the BDUT
    // 1000,i,CB 91 01 FF 01 45 03 02 0A 65
    // # OUT, Expected service A- DeviceDescriptor-Response:
    // #      CB 9F 01 45 01 FF 03 42 00 64 22 02 4C 01 00 AE FF 00 00 00 00 00 0A 65

}

/**
 * 2.3 CEMO/LTR/SN
 *
 * @ingroup KNX_08_TSSA_02_03
 * @todo Test_2_3
 */
TEST(Logical_Tag_Tests, Test_2_3)
{
    // CEMOSN1.SEQ

    // TP0
    // # Individual address : 01 FF in reflex mode
    // # IN, Test of service A_NetworkParameter_Write SNA “88” from 01 45 to all 00 00
    // 12000,i,CD 95 00 00 01 45 03 E4 00 00 39 88 0A 66
    // # Make the product send a frame to check that it uses the SNA value “88”. Press BP
    // # OUT, Expected A-GroupValue-Write: CD 91 9F 01 88 FF 00 81 0B 77 (reflex mode)
    // # Test of service A_NetworkParameter_Write SNA “01” from 01 45 to all 00 00
    // 1000,i,CD 95 00 00 01 45 03 E4 00 00 39 01 0A 66
}

/**
 * 2.4 CEMO/LTR/DI
 *
 * @ingroup KNX_08_TSSA_02_04
 * @todo Test_2_4
 */
TEST(Logical_Tag_Tests, Test_2_4)
{
    // CEMODI1.SEQ

    // TP0
    // # Device Identification for LT reflex
    // # DeviceDescriptor-Read service addressed to Individual Address 01 FF
    // # IN, Descriptor type 0 = Mask version read request
    // 1000,i,CB 91 01 FF 01 45 03 00 0A 65
    // # OUT, Expected A- DeviceDescriptor-Response: 3012 is the mask for “TP0-BIM”
    // #      CB 93 01 45 01 FF 03 40 30 12 0B 77
    // # IN, Descriptor type 2 = Identification
    // 1000,i,CB 91 01 FF 01 45 03 02 0A 65
    // # OUT, Expected service A- DeviceDescriptor-Response:
    // #      CB 9F 01 45 01 FF 03 42 00 64 22 02 4C 01 00 AE FF 00 00 00 00 00 0A 65
    // # IN, Descriptor type 1
    // 1000,i,CB 91 01 FF 01 45 03 01 0A 65
    // # OUT, Expected “error”: CB 91 01 45 01 FF 03 3F 0A 65 for the rest of the test
    // # Descriptor type 3
    // 1000,i,CB 91 01 FF 01 45 03 03 0A 65
    // # Descriptor type 4
    // 1000,i,CB 91 01 FF 01 45 03 04 0A 65
    // # Descriptor type 5
    // 1000,i,CB 91 01 FF 01 45 03 05 0A 65
    // # Descriptor type 6
    // 1000,i,CB 91 01 FF 01 45 03 06 0A 65
    // # Descriptor type 7
    // 1000,i,CB 91 01 FF 01 45 03 07 0A 65
    // # Descriptor type 8
    // 1000,i,CB 91 01 FF 01 45 03 08 0A 65
    // # Descriptor type 33
    // 1000,i,CB 91 01 FF 01 45 03 33 0A 65

    // CEMODI2.SEQ

    // TP0
    // # Device Identification for LT reflex
    // # DeviceDescriptor-Read service addressed to Group Address 9F 01
    // # IN, Descriptor type 0 = Mask version read request
    // 1000,i,CD 91 9F 01 01 45 03 00 0A 65
    // # OUT, NO ANSWER expected for the whole test
    // # Descriptor type 2 = Identification
    // 1000,i,CD 91 9F 01 01 45 03 02 0A 65
    // # Descriptor type 1
    // 1000,i,CD 91 9F 01 01 45 03 01 0A 65
    // # No answer expected
    // # Descriptor type 3
    // 1000,i,CD 91 9F 01 01 45 03 03 0A 65
    // # Descriptor type 4
    // 1000,i,CD 91 9F 01 01 45 03 04 0A 65
    // # Descriptor type 5
    // 1000,i,CD 91 9F 01 01 45 03 05 0A 65
    // # Descriptor type 6
    // 1000,i,CD 91 9F 01 01 45 03 06 0A 65
    // # Descriptor type 7
    // 1000,i,CD 91 9F 01 01 45 03 07 0A 65
    // # Descriptor type 8
    // 1000,i,CD 91 9F 01 01 45 03 08 0A 65
    // # Descriptor type 33
    // 1000,i,CD 91 9F 01 01 45 03 33 0A 65

    // CEMODI5.SEQ

    // TP0
    // # Connection mode, for LT reflex
    // # IN, Connection layer 4
    // 1000,i,CB 91 01 FF 01 45 60 80 0A 65
    // # IN, Descriptor type 0 = Mask version read request and TPCI “Numbered Data Packet”
    // 1000,i,CB 91 01 FF 01 45 43 00 0A 65
    // # OUT, Expected A- DeviceDescriptor-Response: CB 93 01 45 01 FF 43 40 30 12 0B 77
    // # IN, Descriptor type 2
    // 1000,i,CB 91 01 FF 01 45 47 02 0A 65
    // # OUT, Expected service A- DeviceDescriptor-Response:
    // #      CB 9F 01 45 01 FF 47 42 00 64 22 02 4C 01 00 AE FF 00 00 00 00 00 0A 65
    // #
    // # Test with Group Address 9F 01
    // # IN, Descriptor type 0 = Mask version read request
    // 1000,i,CD 91 9F 01 01 45 4B 00 0A 65
    // # OUT, No answer expected
    // # IN, Descriptor type 2
    // 1000,i,CD 91 9F 01 01 45 4F 02 0A 65
    // # OUT, No answer expected
    // # IN, Disconnection layer 4
    // 1000,i,CB 91 01 FF 01 45 60 81 0A 65
}

/**
 * 2.5 CEMO/LTR/GAcheck
 *
 * @ingroup KNX_08_TSSA_02_05
 * @todo Test_2_5
 */
TEST(Logical_Tag_Tests, Test_2_5)
{
    // CEMOGA1.SEQ

    // TP0
    // #GA check for LT reflex
    // # IN, Request to all if the GA 9F 01 is used by anybody
    // 1000,i,CD 97 00 00 01 BC 03 DA 00 01 17 01 9F 01 0A 65
    // # OUT, Expected service A_NetworkScan_Response:
    // #      CB 97 01 BC 01 FF 03 DB 00 01 17 01 9F 01 0A 65
    // # IN, Request to the product (Individual Address 01 FF) if it uses the GA 9F 01
    // 1000,i,CB 97 01 FF 01 BC 03 DA 00 01 17 01 9F 01 0A 65
    // # OUT, Expected service A_NetworkScan_Response:
    // #      CB 97 01 BC 01 FF 03 DB 00 01 17 01 9F 01 0A 65
    // #
    // # IN, Request to all if the GA 66 77 is used by anybody (but nobody uses it)
    // 1000,i,CD 97 00 00 01 BC 03 DA 00 01 17 01 66 77 0A 65
    // # OUT, No answer expected
    // # IN, Request to the product (address 01 54) if it uses the GA 66 77
    // 1000,i,CB 97 01 54 01 BC 03 DA 00 01 17 01 66 77 0A 65
    // # OUT, No answer expected

}

/**
 * 2.6 CEMO/LTR/WN
 *
 * @ingroup KNX_08_TSSA_02_06
 * @todo Test_2_6
 */
TEST(Logical_Tag_Tests, Test_2_6)
{
    // CEMOWN1.SEQ

    // TP0
    // # Wildcard service
    // # Group 9F01 write ON (group according to logical tag of the BDUT)
    // 5000,i,CD 91 9F 01 01 FF 00 81 0A 65
    // # Expected LED switches on
    // # Group 9F01 write OFF
    // 3000,i,CD 91 9F 01 01 FF 00 80 0A 65
    // # Expected LED switches off
    // #
    // # Group write ON with wildcard (BE01 tested)
    // 5000,i,CD 91 BE 01 01 FF 00 81 0A 65
    // # Expected LED switches on
    // # Group write OFF with wildcard
    // 3000,i,CD 91 BE 01 01 FF 00 80 0A 65
    // # Expected LED switches off

    // CEMOWN2.SEQ

    // TP0
    // # Wildcard service (9F 01 and BE 01)
    // # Group write ON (adapt the Group Address to the product under test)
    // 5000,i,CD 91 9F 01 01 45 00 81 0A 65
    // # No answer expected
    // # Group write OFF
    // 3000,i,CD 91 9F 01 01 45 00 80 0A 65
    // # No answer expected
    // #
    // # Group write ON with wildcard
    // 5000,i,CD 91 BE 01 01 45 00 81 0A 65
    // # Expected LED switches on
    // # Group write OFF with wildcard
    // 3000,i,CD 91 BE 01 01 45 00 80 0A 65
    // # Expected LED switches off
    // # Expected service A-GroupValue-Write after pressing a BP:
    // #      CD 91 BE 02 BE 01 00 81 0B 77

    // CEMOWN3.SEQ

    // TP0
    // # Wildcard service (Group Address BE 01 according to logical tag, and 9F01 tested)
    // # Group 9F01 write ON
    // 5000,i,CD 91 9F 01 01 FF 00 81 0A 65
    // # Expected LED switches on
    // # Group 9F01 write OFF
    // 3000,i,CD 91 9F 01 01 FF 00 80 0A 65
    // # Expected LED switches off
    // #
    // # Group write ON with wildcard
    // 5000,i,CD 91 BE 01 01 FF 00 81 0A 65
    // # Expected LED switches on
    // # Group write OFF with wildcard
    // 3000,i,CD 91 BE 01 01 FF 00 80 0A 65
    // # Expected LED switches off
}

/**
 * 2.7 CEMO/LTR/LM (mandatory if supported)
 *
 * @ingroup KNX_08_TSSA_02_07
 * @todo Test_2_7
 */
TEST(Logical_Tag_Tests, Test_2_7)
{
    // CEMOLM1.SEQ

    // TP0
    // # Link Management A_Link_Read
    // # [Group Object n° and start-index n° are tried in sequence]
    // # (responses depend on the application :
    // #  n° of objects, n° of GA attached per Group Object)
    // #
    // # IN, Link read request start-index to check
    // 1000,i,CB 93 01 FF 01 89 03 E5 00 01 0A 65
    // # OUT, Expected A_Link_Response CB 95 01 89 01 FF 03 E6 00 01 (or 11) .. .. 0A 65
    // # or OUT, If no association with the Group Object expected A_Link_Response:
    // #      CB 93 01 89 01 FF 03 E6 00 00 0A 65 (start-index at 0)
    // # Responses depend upon the pixit of the BDUT
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 01 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 02 01 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 00 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 01 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 02 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 03 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 04 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 05 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 06 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 07 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 08 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 09 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 0A 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 0B 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 0C 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 0D 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 0E 0A 65
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 0F 0A 65

    // CEMOLM2.SEQ

    // TP0
    // # A_Link_Write service for LT supervised
    // # For LT reflex Individual Address is 01 FF
    // #
    // # IN, Write "C024" in the product to the object 1
    // 1000,i,D9 95 01 FF 10 89 03 E7 01 01 C0 24 08 B3
    // # OUT, Expected service A_Link_Response : D9 95 10 89 01 FF 03 E6 01 01 C0 24 08 B3
    // # IN, verify it answers to this new Group Address (ON, then OFF)
    // 1000,i,DD 91 C0 24 10 89 00 80 0B 81
    // # Expected LED to switch on
    // 1000,i,DD 91 C0 24 10 89 00 81 0B 80
    // # Expected LED to switch on
    // # IN, Delete C024 from the product
    // 1000,i,D9 95 01 FF 10 89 03 E7 01 03 C0 24 08 B3
    // # OUT, Expected service A_Link_Response : D9 93 10 89 01 FF 03 E6 01 00 08 B3
    // # IN, Verify that it does not react (on)
    // 1000,i,DD 91 C0 24 10 89 00 80 0B 81
    // # OUT, No answer expected, No expected action on the LED
    // # IN, Verify that it does not react (off)
    // 1000,i,DD 91 C0 24 10 89 00 81 0B 80
    // # OUT, No answer expected, No expected action on the LED
    // # IN, Verify that another group if present (here C016) is still working (ON)
    // 1000,i,DD 91 C0 16 10 89 00 80 0B 81
    // # Expected LED switches off
    // # IN, Verify that another group if present (here C016) is still working (OFF)
    // 1000,i,DD 91 C0 16 10 89 00 81 0B 80
    // # Expected LED switches on

    // CEMOLM3.SEQ

    // TP0
    // # A_Link_Write service and s flag for sending address
    // # For LT reflex Individual Address is 01 FF
    // #
    // # IN, Write GAs (at least 10) in the product
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 24 08 B3
    // # OUT, Expected service A_Link_Response : D9 95 02 89 01 FF 03 E6 01 01 C0 24 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 25 08 B3
    // # Expected service A_Link_Response : D9 97 02 89 01 FF 03 E6 01 01 C0 24 C0 25 08 B3
    // # IN, Write first GA with s flag set to 1
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 01 C0 26 08 B3
    // # OUT, Expected : D9 99 02 89 01 FF 03 E6 01 31 C0 24 C0 25 C0 26 08 B3
    // # IN, Write another GA with s flag set to 1
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 01 C0 27 08 B3
    // # Verify that the “sending” group is now the last one:
    // # OUT, Expected : D9 9B 02 89 01 FF 03 E6 01 41 C0 24 C0 25 C0 26 C0 27 08 B3
    // # IN, other GAs
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 28 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 29 08 B3
    // # IN, Write a wrong frame containing two GA
    // 1000,i,D9 97 01 FF 02 89 03 E7 01 00 C0 30 C0 31 08 B3
    // # OUT, Expected : this frame is ignored
    // # IN, Write other GA (s flag to 0)
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 32 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 33 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 34 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 35 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 36 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 37 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 38 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 01 00 C0 39 08 B3
    // # 13 GA are already written (Maybe it was not possible to put all in the BDUT)
    // # IN, Verify that it answers to many of the new GA
    // # Expected LED switches off then on
    // 1000,i,DD 91 C0 24 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 24 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 25 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 25 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 26 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 26 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 28 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 28 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 33 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 33 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 39 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 39 01 78 00 81 0B 80
    // #
    // # The test may be divided in two parts. Here begins the second part.
    // # IN, Try to read the Group Addresses at index 1
    // 3000,i,D9 93 01 FF 02 89 03 E5 00 01 08 B3
    // # OUT, Expected service A_Link_Response :
    // #      D9 9B 02 89 01 FF 03 E6 01 41 C0 24 C0 25 C0 26 C0 27 08 B3
    // # IN, Try to read the GA at index 4, service A_Link_Read
    // 3000,i,D9 93 01 FF 02 89 03 E5 00 04 08 B3
    // # OUT, Expected service A_Link_Response : D9 95 02 89 01 FF 03 E6 01 44 C0 27 08 B3
    // # IN, Delete the GA C024 from the product
    // 1000,i,D9 95 01 FF 02 89 03 E7 00 02 C0 24 08 B3
    // # OUT, Expected : D9 9B 02 89 01 FF 03 E6 01 31 C0 25 C0 26 C0 27 C0 28 08 B3
    // # IN, Delete the GA C028 from the product
    // 1000,i,D9 95 01 FF 02 89 03 E7 00 02 C0 28 08 B3
    // # OUT, Expected : D9 99 02 89 01 FF 03 E6 01 31 C0 25 C0 26 C0 27 08 B3
    // # IN, Delete other GA
    // 1000,i,D9 95 01 FF 02 89 03 E7 00 02 C0 25 08 B3
    // # OUT, Expected : D9 97 02 89 01 FF 03 E6 01 21 C0 26 C0 27 08 B3
    // # IN, Delete other GA
    // 1000,i,D9 95 01 FF 02 89 03 E7 00 02 C0 26 08 B3
    // # OUT, Expected : D9 95 02 89 01 FF 03 E6 01 11 C0 27 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 00 02 C0 29 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 00 02 C0 30 08 B3
    // 1000,i,D9 95 01 FF 02 89 03 E7 00 02 C0 31 08 B3
    // # Verify that it is deleted (no answers, no actions expected)
    // 1000,i,DD 91 C0 24 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 24 01 78 00 81 0B 80
    // # Verify that other groups are still working (LED switches OFF, then ON)
    // 1000,i,DD 91 C0 27 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 27 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 32 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 32 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 39 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 39 01 78 00 81 0B 80

    // CEMOLM4.SEQ

    // TP0
    // # Link Management Write after a A-Memory-Write
    // # LT reflex
    // # L4 Ack are not said expected, but they may be present in responses (TPCI=C2)
    // #
    // # IN, Link_read request start-index
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 01 0A 65
    // # OUT, Expected : CB 95 01 89 01 FF 03 E6 01 11 C0 27 08 B3
    // # IN, Connection
    // 1000,i,CB 90 01 FF 01 89 80 B2 64
    // # IN, A-Memory-Read of 4 bytes, from address 0139h
    // 3000,i,CB 93 01 FF 01 89 02 04 01 39 0A 65
    // # OUT, Expected A_Memory_Response: CB 97 01 89 01 FF 02 44 01 39 AA BB CC DD 08 B3
    // # IN, A-Memory-Write of 2 bytes (55 and AA)
    // 1000,i,CB 95 01 FF 01 89 02 82 01 39 55 AA 0A 65
    // # IN, Disconnection
    // 1000,i,CB 90 01 FF 01 89 81 B2 64
    // #
    // # IN, Link_write
    // 1000,i,CB 95 01 FF 01 CC 03 E7 01 01 C0 24 0A 65
    // # OUT, No answer expected
    // # IN, Link_read request start-index
    // 1000,i,CB 93 01 FF 01 89 03 E5 01 01 0A 65
    // # OUT, No answer expected
    // # Try to push the button and see the new GA on/off (it will not work)
    // # IN, Verify that it is deleted (no answers, no actions expected [on])
    // 500,i,CD 91 C0 24 01 CC 00 81 0A 65
    // # OUT, No answer expected
    // # IN, Verify that it is deleted (no answers, no actions expected [off])
    // 500,i,CD 91 C0 24 01 CC 00 80 0A 65
    // # OUT, No answer expected
}

/**
 * 2.8 CEMO/LTR/NS
 *
 * @ingroup KNX_08_TSSA_02_08
 * @todo Test_2_8
 */
TEST(Logical_Tag_Tests, Test_2_8)
{
    // CEMONS1.SEQ

    // TP0
    // # LT reflex
    // # Reduced interface object with A_NetworkParameter_Read
    // # IN, Serial number read request (request coming from 01 AA to 01 FF) (APCI = 03 DA)
    // 1000,i,CB 95 01 FF 01 AA 03 DA 00 00 0B 00 0A 65
    // # OUT, As this service is optional, maybe no response
    // # or OUT, Expected service A_NetworkParameter_Response:
    // #      CB 95 01 AA 01 FF 03 DB 00 00 0B (then serial number on 6 bytes) 0A 65
    // # A_PropertyValue_Read request (on serial number)
    // # IN
    // 1000,i,CB 95 01 FF 01 AA 03 D5 00 0B 60 01 0A 65
    // # OUT, Expected response is :
    // #
    //  CB 9B 01 AA 01 FF 03 D6 00 0B 60 01 (then serial number on 6 bytes) 0A 65
    // #

    // CEMONS2.SEQ

    // TP0
    // # LT reflex
    // # Reduced interface object with A_NetworkParameter_Read
    // # IN, Object Type read request (request coming from 01 AA to 01 FF) (APCI = 03 DA)
    // 1000,i,CB 95 01 FF 01 AA 03 DA 00 00 01 00 0A 65
    // # OUT, As this service is optional, maybe no response
    // # or OUT, Expected service A_NetworkParameter_Response:
    // #      CB 95 01 AA 01 FF 03 DB 00 00 01 (then object type on 2 bytes) 0A 65
    // # A_PropertyValue_Read request of object type
    // # IN (APCI = 03 D5)
    // 1000,i,CB 95 01 FF 01 AA 03 D5 00 01 20 01 0A 65
    // # OUT, Expected response is :
    // #      CB 9B 01 AA 01 FF 03 D6 00 01 20 01 (then object type on 2 bytes) 0A 65

    // CEMONS3.SEQ

    // TP0
    // # LT reflex
    // # Reduced interface object is supported, test of A_PropertyDescription_Read
    // # IN, read request (request coming from 01 AA to 01 FF) (APCI = 03 D8)
    // 1000,i,CB 95 01 FF 01 AA 03 D8 00 0B 01 0A 65
    // # OUT, no response expected
}

/**
 * 2.9 CEMO/LTR/FOCI (mandatory if supported)
 *
 * @ingroup KNX_08_TSSA_02_09
 * @todo Test_2_9
 */
TEST(Logical_Tag_Tests, Test_2_9)
{
    // CEMOFO1.SEQ

    // TP0
    // # FOCIs
    // # First Foci tried = F0 01
    // 1000,i,CD 95 F0 01 01 5A 64 00 80 12 34 56 0A 65
    // # Expected answer depends on the FOCI, see also pixit
    // # First Foci tried = F0 01, new value
    // 1000,i,CD 95 F0 01 01 5A 64 00 80 00 00 00 0A 65
    // # Expected answer depends on the FOCI, see also pixit
    // # Second Foci tried = F4 03
    // 1000,i,CD 91 F4 03 01 5A 60 80 0A 65
    // # No response expected from this unknown FOCI
}

/**
 * 2.10 CEMO/LTR/PARA
 *
 * @ingroup KNX_08_TSSA_02_10
 * @todo Test_2_10
 */
TEST(Logical_Tag_Tests, Test_2_10)
{
    // CEMOPA1.SEQ

    // TP0
    // # LT reflex
    // #
    // # Parameters, preliminary = read size of blocks
    // # IN, PropertyDescriptor size read request on property “Channel parameter” channel 1
    // 1000,i,CB 94 01 FF 01 AA 03 D8 00 65 00 0A 65
    // # OUT, Expected A_PropertyDescription_Response:
    // #      CB 97 01 AA 01 FF 03 D9 00 65 00 03 02 00 0A 65
    // # IN, PropertyDescriptor size read request on property “Channel parameter” channel 2
    // 1000,i,CB 94 01 FF 01 AA 03 D8 00 66 00 0A 65
    // # OUT, Expected A_PropertyDescription_Response:
    // #      CB 97 01 AA 01 FF 03 D9 00 66 00 03 04 00 0A 65
    // #
    // # Parameters, first part = read
    // # IN, Device Object read request on property “Channel parameter” channel 1
    // 1000,i,CB 95 01 FF 01 AA 03 D5 00 65 20 01 0A 65
    // # OUT, Expected:CB 97 01 AA 01 FF 03 D6 00 65 20 01 (+ the block “11 12”) 0A 65
    // # IN, Device Object read request on property “Channel parameter” channel 2
    // 1000,i,CB 95 01 FF 01 AA 03 D5 00 66 40 01 0A 65
    // # OUT, Expected:CB 99 01 AA 01 FF 03 D6 00 65 40 01 (+ the block “21 22 23 24”) 0A 65
    // #
    // # Second part = Write
    // # IN, Device Object write property “Channel parameter” channel 1, 2 bytes
    // 1000,i,CB 97 01 FF 01 AA 03 D7 00 65 20 01 22 22 0A 65
    // # OUT, Expected:CB 97 01 AA 01 FF 03 D6 00 65 20 01 22 22 0A 65
    // # IN, Device Object write property “Channel parameter” channel 2, 4 bytes
    // 1000,i,CB 99 01 FF 01 AA 03 D7 00 66 40 01 33 33 44 44 0A 65
    // # OUT, Expected:CB 99 01 AA 01 FF 03 D6 00 65 40 01 33 33 44 44 0A 65
    // #
    // # Third part = Partial Write
    // # IN, Device Object write property “Channel parameter” channel 2, 3 bytes
    // 1000,i,CB 98 01 FF 01 AA 03 D7 00 66 30 01 55 66 77 0A 65
    // # OUT, Expected:CB 99 01 AA 01 FF 03 D6 00 65 40 01 55 66 77 44 0A 65
}

/* 3 E-Mode LT Supervised testing */

/**
 * 3.1 CEMO/LTS/SNPM
 *
 * @ingroup KNX_08_TSSA_03_01
 * @todo Test_3_1
 */
TEST(Logical_Tag_Tests, Test_3_1)
{
}

/**
 * 3.2 CEMO/LTS/SN
 *
 * @ingroup KNX_08_TSSA_03_02
 * @todo Test_3_2
 */
TEST(Logical_Tag_Tests, Test_3_2)
{
    // CEMOSN2.SEQ

    // TP0
    // # Individual address : 01 78 in supervised mode
    // # IN, Test of service A_NetworkParameter_Write SNA “88” from 01 45 to all 00 00
    // 12000,i,CD 95 00 00 01 45 03 E4 00 00 39 88 0A 66
    // # Make the product send a frame to check that it uses the SNA value “88”.
    // Press BP
    // # OUT, Expected A-GroupValue-Write: CD 91 9F 01 88 78 00 81 0B 77
    // # Test of service A_NetworkParameter_Write SNA “01” from 01 45 to all 00 00
    // 1000,i,CD 95 00 00 01 45 03 E4 00 00 39 01 0A 66
}

/**
 * 3.3 CEMO/LTS/DI
 *
 * @ingroup KNX_08_TSSA_03_03
 * @todo Test_3_3
 */
TEST(Logical_Tag_Tests, Test_3_3)
{
    // CEMODI3.SEQ

    // TP0
    // # Device Identification for LT supervised
    // # DeviceDescriptor-Read service addressed to Individual Address 01 78 (x/y=6/7)
    // # IN, Descriptor type 0 = Mask version read request
    // 1000,i,CB 91 01 78 01 45 03 00 0A 65
    // # OUT, Expected A- DeviceDescriptor-Response: 3012 is the mask for “TP0-BIM”
    // #      CB 93 01 45 01 78 03 40 30 12 0B 77
    // # IN, Descriptor type 2 = Identification
    // 1000,i,CB 91 01 78 01 45 03 02 0A 65
    // # OUT, Expected service A- DeviceDescriptor-Response:
    // #      CB 9F 01 45 01 78 03 42 00 64 22 02 4C 01 00 AE FF 00 00 00 00 00 0A 65
    // # IN, Descriptor type 1
    // 1000,i,CB 91 01 78 01 45 03 01 0A 65
    // # OUT, Expected “error”: CB 91 01 45 01 78 03 3F 0A 65 for the rest of the test
    // # Descriptor type 3
    // 1000,i,CB 91 01 78 01 45 03 03 0A 65
    // # Descriptor type 4
    // 1000,i,CB 91 01 78 01 45 03 04 0A 65
    // # Descriptor type 5
    // 1000,i,CB 91 01 78 01 45 03 05 0A 65
    // # Descriptor type 6
    // 1000,i,CB 91 01 78 01 45 03 06 0A 65
    // # Descriptor type 7
    // 1000,i,CB 91 01 78 01 45 03 07 0A 65
    // # Descriptor type 8
    // 1000,i,CB 91 01 78 01 45 03 08 0A 65
    // # Descriptor type 33
    // 1000,i,CB 91 01 78 01 45 03 33 0A 65

    // CEMODI4.SEQ

    // TP0
    // # Device Identification for LT supervised
    // # DeviceDescriptor-Read service addressed to Group Address 9F 01
    // # IN, Descriptor type 0 = Mask version read request
    // 1000,i,CD 91 9F 01 01 45 03 00 0A 65
    // # OUT, NO ANSWER expected for this whole test
    // # Descriptor type 2 = Identification
    // 1000,i,CD 91 9F 01 01 45 03 02 0A 65
    // # Descriptor type 1
    // 1000,i,CD 91 9F 01 01 45 03 01 0A 65
    // # Descriptor type 3
    // 1000,i,CD 91 9F 01 01 45 03 03 0A 65
    // # Descriptor type 4
    // 1000,i,CD 91 9F 01 01 45 03 04 0A 65
    // # Descriptor type 5
    // 1000,i,CD 91 9F 01 01 45 03 05 0A 65
    // # Descriptor type 6
    // 1000,i,CD 91 9F 01 01 45 03 06 0A 65
    // # Descriptor type 7
    // 1000,i,CD 91 9F 01 01 45 03 07 0A 65
    // # Descriptor type 8
    // 1000,i,CD 91 9F 01 01 45 03 08 0A 65
    // # Descriptor type 33
    // 1000,i,CD 91 9F 01 01 45 03 33 0A 65
    // # Descriptor type 2 = Identification verification
    // 1000,i,CD 91 9F 01 01 45 03 02 0A 65

    // CEMODI6.SEQ

    // TP0
    // # Connection mode, for LT supervised Individual Address 01 78
    // # IN, Connection layer 4
    // 1000,i,CB 91 01 78 01 45 60 80 0A 65
    // # IN, Descriptor type 0 = Mask version read request
    // 1000,i,CB 91 01 78 01 45 43 00 0A 65
    // # OUT, Expected A- DeviceDescriptor-Response: CB 93 01 45 01 78 43 40 30 12 0B 77
    // # IN, Descriptor type 2
    // 1000,i,CB 91 01 78 01 45 47 02 0A 65
    // # OUT, Expected service A- DeviceDescriptor-Response:
    // #      CB 9F 01 45 01 78 47 42 00 64 22 02 4C 01 00 AE FF 00 00 00 00 00 0A 65
    // #
    // # Test with Group Address 9F 01
    // # IN, Descriptor type 0 = Mask version read request
    // 1000,i,CD 91 9F 01 01 45 4B 00 0A 65
    // # OUT, No answer expected
    // # IN, Descriptor type 2
    // 1000,i,CD 91 9F 01 01 45 4F 02 0A 65
    // # OUT, No answer expected
    // # IN, Disconnection layer 4
    // 1000,i,CB 91 01 78 01 45 60 81 0A 65
}

/**
 * 3.4 CEMO/LTS/GAcheck
 *
 * @ingroup KNX_08_TSSA_03_04
 */
TEST(Logical_Tag_Tests, Test_3_4)
{
    // CEMOGA2.SEQ

    // TP0
    // #GA check for LT supervised
    // # IN, Request to all if the GA 9F 01 is used by anybody
    // 1000,i,CD 97 00 00 01 BC 03 DA 00 01 17 01 9F 01 0A 65
    // # OUT, Expected service A_NetworkScan_Response:
    // #      CB 97 01 BC 01 78 03 DB 00 01 17 01 9F 01 0A 65
    // # IN, Request to the product (Individual Address 01 78) if it uses the GA 87
    // 65
    // 1000,i,CB 97 01 78 01 BC 03 DA 00 01 17 01 9F 01 0A 65
    // # OUT, Expected service A_NetworkScan_Response:
    // #      CB 97 01 BC 01 78 03 DB 00 01 17 01 9F 01 0A 65
    // #
    // # IN, Request to all if the GA 66 77 is used by anybody (but nobody uses it)
    // 1000,i,CD 97 00 00 01 BC 03 DA 00 01 17 01 66 77 0A 65
    // # OUT, No answer expected
    // # IN, Request to the product (address 01 78) if it uses the GA 66 77
    // 1000,i,CB 97 01 78 01 BC 03 DA 00 01 17 01 66 77 0A 65
    // # OUT, No answer expected
    //
    //
    //
    // TPDU of the response:
    // CASE B2: 03 E6 00 12 C2 31
    // CASE C: 03 E6 01 15 C2 44 C2 45 C2 46 C2 47 C2 48 C2 49
    // CASE D: 03 E6 02 00
    // CASE E: 03 E6 03 1F C2 6F C2 70 C2 71 C2 72 C2 74 C2 75
}

/**
 * 3.5 CEMO/LTS/LM
 *
 * @ingroup KNX_08_TSSA_03_05
 * @todo Test_3_5
 */
TEST(Logical_Tag_Tests, Test_3_5)
{
    // CEMOLMD1.SEQ

    // TP0
    // # Link Management A_Link_Read
    // # For LT supervised, x/y = 8/9
    // #Case A
    // 1000,i,CB 93 01 78 01 89 03 E5 04 01 0A 65
    // # expected response:
    // #      CB 95 01 89 01 78 03 E6 04 00 0A 65
    // #Case B1
    // 1000,i,CB 93 01 78 01 89 03 E5 00 01 0A 65
    // # expected response:
    // #      CB 95 01 89 01 78 03 E6 00 11 C2 30 C2 31 0A 65
    // #Case B2
    // 1000,i,CB 93 01 78 01 89 03 E5 00 02 0A 65
    // # expected response:
    // #      CB 95 01 89 01 78 03 E6 00 12 C2 31 0A 65
    // #Case C
    // 1000,i,CB 93 01 78 01 89 03 E5 01 05 0A 65
    // # expected response:
    // #      CB 95 01 89 01 78 03 E6 01 15 C2 44 C2 45 C2 46 C2 47 C2 48 C2 49 0A 65
    // #Case D
    // 1000,i,CB 93 01 78 01 89 03 E5 02 03 0A 65
    // # expected response:
    // #      CB 95 01 89 01 78 03 E6 02 00 0A 65
    // #Case E
    // 1000,i,CB 93 01 78 01 89 03 E5 03 0F 0A 65
    // # expected response:
    // #      CB 95 01 89 01 78 03 E6 03 1F C2 6F C2 70 C2 71 C2 72 C2 74 C2 75 0A 65
    // The following can also be tried (combinations of start index)
    // # (not described in the text of the suite of tests)
    // # [Group Object n° and start-index n° are tried in sequence]
    // # expected response If no association with the Group Object A_Link_Response:
    // #      CB 93 01 89 01 78 03 E6 00 00 0A 65 (start-index at 0)
    // # Responses depend upon the pixit of the BDUT
    // 1000,i,CB 93 01 78 01 89 03 E5 01 00 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 01 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 02 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 03 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 04 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 05 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 06 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 07 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 08 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 09 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 0A 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 0B 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 0C 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 0D 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 0E 0A 65
    // 1000,i,CB 93 01 78 01 89 03 E5 01 0F 0A 65

    // CEMOLMD2.SEQ

    // TP0
    // # A_Link_Write service for LT supervised. This sequence only checks the service
    // # It does not check the given GA themselvesin the BDUT.
    // # For LT supervised, x/y = 8/9
    // #
    // #Case A
    // 1000,i,CB 95 01 78 01 89 03 E7 01 01 C2 40 0A 65
    // # expected response:
    // #      CB 95 01 89 01 78 03 E6 01 11 C2 40 0A 65
    // 1000,i,CB 95 01 78 01 89 03 E7 01 00 C2 41 0A 65
    // # expected response:
    // #      CB 97 01 89 01 78 03 E6 01 11 C2 40 C2 41 0A 65
    // 1000,i,CB 95 01 78 01 89 03 E7 01 00 C2 42 0A 65
    // # expected response:
    // #      CB 99 01 89 01 78 03 E6 01 11 C2 40 C2 41 C2 42 0A 65
    // 1000,i,CB 95 01 78 01 89 03 E7 01 00 C2 43 0A 65
    // # expected response:
    // #      CB 9B 01 89 01 78 03 E6 01 11 C2 40 C2 41 C2 42 C2 43 0A 65
    // 1000,i,CB 95 01 78 01 89 03 E7 01 00 C2 44 0A 65
    // # expected response:
    // #      CB 9D 01 89 01 78 03 E6 01 11 C2 40 C2 41 C2 42 C2 43 C2 44 0A 65
    // 1000,i,CB 95 01 78 01 89 03 E7 01 00 C2 45 0A 65
    // # expected response:
    // #      CB 9F 01 89 01 78 03 E6 01 11 C2 40 C2 41 C2 42 C2 43 C2 44 C2 45 0A 65
    // 1000,i,CB 95 01 78 01 89 03 E7 01 00 C2 46 0A 65
    // # expected response:
    // #      CB 9F 01 89 01 78 03 E6 01 13 C2 41 C2 42 C2 43 C2 44 C2 45 C2 46 0A 65
    // #Case B
    // 1000,i,CB 95 01 78 01 89 03 E7 02 00 C2 41 0A 65
    // # expected response (C2 41 added to GO2 which does not exist)
    // #      CB 93 01 89 01 78 03 E6 02 00 0A 65
    // #Case C
    // 1000,i,CB 95 01 78 01 89 03 E7 00 01 C2 36 0A 65
    // # expected response: (C2 36 is added with s flag to 1)
    // #      CB 99 01 89 01 78 03 E6 00 31 C2 34 C2 35 C2 36 0A 65
    // #Case D
    // 1000,i,CB 95 01 78 01 89 03 E7 00 02 C2 35 0A 65
    // # expected response: (C2 35 is deleted)
    // #      CB 97 01 89 01 78 03 E6 00 21 C2 34 C2 36 0A 65
    // The following can also be tried
    // # (not described in the text of the suite of tests)
    // # IN, Write "C024" in the product to the object 1
    // 1000,i,D9 95 01 78 10 89 03 E7 01 01 C0 24 08 B3
    // # OUT, Expected service A_Link_Response : D9 95 10 89 01 78 03 E6 01 01 C0 24 08 B3
    // # IN, verify it answers to this new Group Address (ON, then OFF)
    // 1000,i,DD 91 C0 24 10 89 00 80 0B 81
    // # Expected LED to switch on
    // 1000,i,DD 91 C0 24 10 89 00 81 0B 80
    // # Expected LED to switch on
    // # IN, Delete C024 from the product
    // 1000,i,D9 95 01 78 10 89 03 E7 01 03 C0 24 08 B3
    // # OUT, Expected service A_Link_Response : D9 93 10 89 01 78 03 E6 01 00 08 B3
    // # IN, Verify that it does not react (on)
    // 1000,i,DD 91 C0 24 10 89 00 80 0B 81
    // # OUT, No answer expected, No expected action on the LED
    // # IN, Verify that it does not react (off)
    // 1000,i,DD 91 C0 24 10 89 00 81 0B 80
    // # OUT, No answer expected, No expected action on the LED
    // # IN, Verify that another group if present (here C016) is still working (ON)
    // 1000,i,DD 91 C0 16 10 89 00 80 0B 81
    // # Expected LED switches off
    // # IN, Verify that another group if present (here C016) is still working (OFF)
    // 1000,i,DD 91 C0 16 10 89 00 81 0B 80
    // # Expected LED switches on

    // CEMOLMD3.SEQ

    // TP0
    // # A_Link_Write service and s flag for sending address
    // # For LT supervised, x/y = 8/9
    // #
    // # (not described in the text of the suite of tests)
    // # IN, Write GAs (at least 10) in the product
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 24 08 B3
    // # OUT, Expected service A_Link_Response : D9 95 02 89 01 78 03 E6 01 01 C0 24 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 25 08 B3
    // # Expected service A_Link_Response : D9 97 02 89 01 78 03 E6 01 01 C0 24 C0 25 08 B3
    // # IN, Write first GA with s flag set to 1
    // 1000,i,D9 95 01 78 02 89 03 E7 01 01 C0 26 08 B3
    // # OUT, Expected : D9 99 02 89 01 78 03 E6 01 31 C0 24 C0 25 C0 26 08 B3
    // # IN, Write another GA with s flag set to 1
    // 1000,i,D9 95 01 78 02 89 03 E7 01 01 C0 27 08 B3
    // # Verify that the “sending” group is now the last one:
    // # OUT, Expected : D9 9B 02 89 01 78 03 E6 01 41 C0 24 C0 25 C0 26 C0 27 08 B3
    // # IN, other GAs
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 28 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 29 08 B3
    // # IN, Write a wrong frame containing two GA
    // 1000,i,D9 97 01 78 02 89 03 E7 01 00 C0 30 C0 31 08 B3
    // # OUT, Expected : this frame is ignored
    // # IN, Write other GA (s flag to 0)
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 32 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 33 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 34 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 35 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 36 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 37 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 38 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 01 00 C0 39 08 B3
    // # 13 GA are already written (Maybe it was not possible to put all in the BDUT)
    // # IN, Verify that it answers to many of the new GA
    // # Expected LED switches off then on
    // 1000,i,DD 91 C0 24 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 24 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 25 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 25 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 26 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 26 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 28 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 28 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 33 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 33 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 39 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 39 01 78 00 81 0B 80
    // #
    // # The test may be divided in two parts. Here begins the second part.
    // # IN, Try to read the Group Addresses at index 1
    // 3000,i,D9 93 01 78 02 89 03 E5 00 01 08 B3
    // # OUT, Expected service A_Link_Response :
    // #      D9 9B 02 89 01 78 03 E6 01 41 C0 24 C0 25 C0 26 C0 27 08 B3
    // # IN, Try to read the GA at index 4, service A_Link_Read
    // 3000,i,D9 93 01 78 02 89 03 E5 00 04 08 B3
    // # OUT, Expected service A_Link_Response : D9 95 02 89 01 78 03 E6 01 44 C0 27 08 B3
    // # IN, Delete the GA C024 from the product
    // 1000,i,D9 95 01 78 02 89 03 E7 00 02 C0 24 08 B3
    // # OUT, Expected : D9 9B 02 89 01 78 03 E6 01 31 C0 25 C0 26 C0 27 C0 28 08 B3
    // # IN, Delete the GA C028 from the product
    // 1000,i,D9 95 01 78 02 89 03 E7 00 02 C0 28 08 B3
    // # OUT, Expected : D9 99 02 89 01 78 03 E6 01 31 C0 25 C0 26 C0 27 08 B3
    // # IN, Delete other GA
    // 1000,i,D9 95 01 78 02 89 03 E7 00 02 C0 25 08 B3
    // # OUT, Expected : D9 97 02 89 01 78 03 E6 01 21 C0 26 C0 27 08 B3
    // # IN, Delete other GA
    // 1000,i,D9 95 01 78 02 89 03 E7 00 02 C0 26 08 B3
    // # OUT, Expected : D9 95 02 89 01 78 03 E6 01 11 C0 27 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 00 02 C0 29 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 00 02 C0 30 08 B3
    // 1000,i,D9 95 01 78 02 89 03 E7 00 02 C0 31 08 B3
    // # Verify that it is deleted (no answers, no actions expected)
    // 1000,i,DD 91 C0 24 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 24 01 78 00 81 0B 80
    // # Verify that other groups are still working (LED switches OFF, then ON)
    // 1000,i,DD 91 C0 27 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 27 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 32 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 32 01 78 00 81 0B 80
    // 1000,i,DD 91 C0 39 01 78 00 80 0B 81
    // 1000,i,DD 91 C0 39 01 78 00 81 0B 80

    // CEMOLMD4.SEQ

    // TP0
    // # Link Management Write after a A-Memory-Write
    // # For LT supervised, x/y = 8/9
    // # L4 Ack are not said expected, but they may be present in responses (TPCI=C2)
    // #
    // # IN, Link_read request start-index
    // 1000,i,CB 93 01 78 01 89 03 E5 01 01 0A 65
    // # OUT, Expected : CB 95 01 89 01 78 03 E6 01 11 C0 27 08 B3
    // # IN, Connection
    // 1000,i,CB 90 01 78 01 89 80 B2 64
    // # IN, A-Memory-Read of 4 bytes, from address 0139h and TPCI “Numbered Data Packet”
    // 3000,i,CB 93 01 78 01 89 42 04 01 39 0A 65
    // # OUT, Expected A_Memory_Response: CB 97 01 89 01 78 42 44 01 39 AA BB CC DD 08 B3
    // # IN, A-Memory-Write of 2 bytes (55 and AA)
    // 1000,i,CB 95 01 78 01 89 46 82 01 39 55 AA 0A 65
    // # IN, Disconnection
    // 1000,i,CB 90 01 78 01 89 81 B2 64
    // #
    // # IN, Link_write
    // 1000,i,CB 95 01 78 01 CC 03 E7 01 01 C0 24 0A 65
    // # OUT, No answer expected
    // # IN, Link_read request start-index
    // 1000,i,CB 93 01 78 01 89 03 E5 01 01 0A 65
    // # OUT, No answer expected
    // # Try to push the button and see the new GA on/off (it will not work)
    // # IN, Verify that it is deleted (no answers, no actions expected [on])
    // 500,i,CD 91 C0 24 01 CC 00 81 0A 65
    // # OUT, No answer expected
    // # IN, Verify that it is deleted (no answers, no actions expected [off])
    // 500,i,CD 91 C0 24 01 CC 00 80 0A 65
    // # OUT, No answer expected
}

/**
 * 3.6 CEMO/LTS/NS
 *
 * @ingroup KNX_08_TSSA_03_06
 * @todo Test_3_6
 */
TEST(Logical_Tag_Tests, Test_3_6)
{
    // CEMONS4.SEQ

    // TP0
    // # For LT supervised keep Individual Address at 01 78, and x/y = 8/9
    // # Reduced interface object with A_NetworkParameter_Read
    // # IN, Serial number read request (request coming from 01 AA to 01 78) (APCI = 03 DA)
    // 1000,i,CB 95 01 78 01 AA 03 DA 00 00 0B 00 0A 65
    // # OUT, As this service is optional, maybe no response
    // # or OUT, Expected service A_NetworkParameter_Response:
    // #      CB 95 01 AA 01 78 03 DB 00 00 0B (then serial number on 6 bytes) 0A 65
    // # A_PropertyValue_Read request (on serial number)
    // # IN
    // 1000,i,CB 95 01 78 01 AA 03 D5 00 0B 60 01 0A 65
    // # OUT, Expected response is :
    // #      CB 9B 01 AA 01 78 03 D6 00 0B 60 01 (then serial number on 6 bytes) 0A 65
    // #

    // CEMONS5.SEQ

    // TP0
    // # For LT supervised keep Individual Address at 01 78, and x/y = 8/9
    // # Reduced interface object with A_NetworkParameter_Read
    // # IN, Object Type read request (request coming from 01 AA to 01 78) (APCI = 03 DA)
    // 1000,i,CB 95 01 78 01 AA 03 DA 00 00 01 00 0A 65
    // # OUT, As this service is optional, maybe no response
    // # or OUT, Expected service A_NetworkParameter_Response:
    // #      CB 95 01 AA 01 78 03 DB 00 00 01 (then object type on 2 bytes) 0A 65
    // # A_PropertyValue_Read request of object type
    // # IN (APCI = 03 D5)
    // 1000,i,CB 95 01 78 01 AA 03 D5 00 01 20 01 0A 65
    // # OUT, Expected response is :
    // #      CB 9B 01 AA 01 78 03 D6 00 01 20 01 (then object type on 2 bytes) 0A 65

    // CEMONS6.SEQ

    // TP0
    // # For LT supervised keep Individual Address at 01 78, and x/y = 8/9
    // # Reduced interface object is supported, test of A_PropertyDescription_Read
    // # IN, read request (request coming from 01 AA to 01 78) (APCI = 03 D8)
    // 1000,i,CB 95 01 78 01 AA 03 D8 00 0B 01 0A 65
    // # OUT, no response expected
}

/**
 * 3.7 CEMO/LTS/FOCI (mandatory if supported)
 *
 * @ingroup KNX_08_TSSA_03_07
 * @todo Test_3_7
 */
TEST(Logical_Tag_Tests, Test_3_7)
{
    // CEMOFO2.SEQ

    // TP0
    // # FOCIs
    // # First Foci tried = F0 01
    // 1000,i,CD 95 F0 01 01 5A 64 00 80 12 34 56 0A 65
    // # Expected answer depends on the FOCI, see also pixit
    // # First Foci tried = F0 01, new value
    // 1000,i,CD 95 F0 01 01 5A 64 00 80 00 00 00 0A 65
    // # Expected answer depends on the FOCI, see also pixit
    // # Second Foci tried = F4 03
    // 1000,i,CD 91 F4 03 01 5A 60 80 0A 65
    // # No response expected from this unknown FOCI
}

/**
 * 3.8 CEMO/LTS/ECH-PARA
 *
 * @ingroup KNX_08_TSSA_03_08
 * @todo Test_3_8
 */
TEST(Logical_Tag_Tests, Test_3_8)
{
    // CEMOPA2.SEQ

    // TP0
    // # For LT supervised, x/y = 8/9
    // #
    // # Parameters, preliminary = read size of blocks
    // # IN, PropertyDescriptor size read request on property “Channel parameter” channel 1
    // 1000,i,CB 94 01 78 01 AA 03 D8 00 65 00 0A 65
    // # OUT, Expected A_PropertyDescription_Response:
    // #      CB 97 01 AA 01 78 03 D9 00 65 00 03 02 00 0A 65
    // # IN, PropertyDescriptor size read request on property “Channel parameter” channel 2
    // 1000,i,CB 94 01 78 01 AA 03 D8 00 66 00 0A 65
    // # OUT, Expected A_PropertyDescription_Response:
    // #      CB 97 01 AA 01 78 03 D9 00 66 00 03 04 00 0A 65
    // #
    // # Parameters, first part = read
    // # IN, Device Object read request on property “Channel parameter” channel 1
    // 1000,i,CB 95 01 78 01 AA 03 D5 00 65 20 01 0A 65
    // # OUT, Expected:CB 97 01 AA 01 78 03 D6 00 65 20 01 (+ the block “11 12”) 0A 65
    // # IN, Device Object read request on property “Channel parameter” channel 2
    // 1000,i,CB 95 01 78 01 AA 03 D5 00 66 20 01 0A 65
    // # OUT, Expected:CB 99 01 AA 01 78 03 D6 00 65 40 01 (+ the block “21 22 23 24”) 0A 65
    // #
    // # Second part = Write
    // # IN, Device Object write property “Channel parameter” channel 1, 2 bytes
    // 1000,i,CB 97 01 78 01 AA 03 D7 00 65 20 01 22 22 0A 65
    // # OUT, Expected:CB 97 01 AA 01 78 03 D6 00 65 20 01 22 22 0A 65
    // # IN, Device Object write property “Channel parameter” channel 2, 4 bytes
    // 1000,i,CB 99 01 78 01 AA 03 D7 00 66 40 01 33 33 44 44 0A 65
    // # OUT, Expected:CB 99 01 AA 01 78 03 D6 00 65 40 01 33 33 44 44 0A 65
    // #
    // # Third part = Partial Write
    // # IN, Device Object write property “Channel parameter” channel 2, 3 bytes
    // 1000,i,CB 98 01 78 01 AA 03 D7 00 66 30 01 55 66 77 0A 65
    // # OUT, Expected:CB 99 01 AA 01 78 03 D6 00 65 40 01 55 66 77 44 0A 65
}

/* 4 E-Mode LT supervisor validation */

/**
 * 4.1 CEMO/LTSS/DO
 *
 * @ingroup KNX_08_TSSA_04_01
 */
TEST(Logical_Tag_Tests, Test_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* PL132 physical layer simulation */
    KNX::PL132_Bus_Simulation pl132_bus(io_context);
    std::vector<uint8_t> pl132_data;

    /* BDUT Supervisor */
    PL132_Plain_Data supervisor(io_context, pl132_bus); // @todo this should be PL132_BCU2, supporting NM_* methods.
    supervisor.group_address_table->individual_address = KNX::Individual_Address(0x0378);

    /* Frame Observer */
    PL132_Plain_Data observer(io_context, pl132_bus);
    observer.pl132_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(observer.log);

    // CEMODOA1.SEQ

    // Case A (Creation process - positive): install an auxiliary device with domain address 1234h and Individual Address 1.0.5
    KNX::PL132_BCU2 aux_device_1(io_context, pl132_bus);
    aux_device_1.interface_objects->device()->domain_address()->domain_address = KNX::Domain_Address_2(0x1234);
    aux_device_1.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(1, 0, 5);

    // CASE A: select at the Supervisor a domain address to be checked (e.g. 1234).
    // Start the DoA Creation Process.
    // Check that the Supervisor initiates a A_DomainAddress_SelectiveRead for the to be checked domain address, indicating an Individual Address from which onward the selective read shall be initiated as well as the check range (e.g. 1.0.0 – range 10).
    // Check that the Supervisor sends this message via broadcast (on DoA 0000h) and that it continues its search on DoA 1235h after it receives a response on DoA 1234.

    //# Individual address of supervisor is 03 78 in supervised mode
    //# Case A : creation with response
    //#    00 00 EF 03 78 00 00 66 03 E3 12 34 03 00 FF 0A 66
    pl132_data = { 0x00, 0x00, 0xEF, 0x03, 0x78, 0x00, 0x00, 0xE6, 0x03, 0xE3, 0x12, 0x34, 0x03, 0x00, 0xFF, 0x0A, 0x66 }; // A_DomainAddressSelective_Read // @note 0x66 (individual) changed to 0xE6 (group)
    supervisor.pl132_data_link_layer.L_Plain_Data_req(0, pl132_data);
    io_context.poll();
    ASSERT_EQ(*log++, pl132_data);

    //# expected response :
    //1000,i,00 00 EF 03 69 00 00 63 03 E2 12 34 0A 66
    pl132_data = { 0x00, 0x00, 0xEF, 0x10, 0x05, 0x00, 0x00, 0xE3, 0x03, 0xE2, 0x12, 0x34, 0x0A, 0x66 }; // A_DomainAddress_Response // @note src changed from 0x0369 to 0x1005. 0x63 (individual) changed to 0xE3 (group)
    ASSERT_EQ(*log++, pl132_data);

    // Case B (Creation process – negative): remove the above auxiliary device from the network
    aux_device_1.pl132_physical_layer.disconnect();

    // CASE B: select at the Supervisor a domain address to be checked (e.g. 1234).
    // Start the DoA Creation Process.
    // Check that the Supervisor initiates a A_DomainAddress_SelectiveRead for the to be checked domain address, indicating an Individual Address from which onward the selective read shall be initiated as well as the check range (e.g. 1.0.0 – range 10).
    // Check that the Supervisor sends this message via broadcast (on DoA 0000h) and that it accepts the selected DoA as a free DoA after the time-out (medium dependant).

    //# Case B : creation without response
    //#    00 00 EF 03 78 00 00 66 03 E3 12 34 03 00 FF 0A 66
    pl132_data = { 0x00, 0x00, 0xEF, 0x03, 0x78, 0x00, 0x00, 0xE6, 0x03, 0xE3, 0x12, 0x34, 0x03, 0x00, 0xFF, 0x0A, 0x66 }; // A_DomainAddressSelective_Read // @note 0x66 (individual) changed to 0xE6 (group)
    supervisor.pl132_data_link_layer.L_Plain_Data_req(0, pl132_data);
    io_context.poll();
    ASSERT_EQ(*log++, pl132_data);

    ASSERT_EQ(log, std::cend(observer.log));

    // Case C (Acquisition process – positive): reinstall auxiliary device 1
    aux_device_1.pl132_physical_layer.connect(pl132_bus);

    // CASE C: select at the Supervisor a domain address to be written (e.g. 1235).
    // Start the DoA Acquisition process.
    // Check that the Supervisor initiates an A_DomainAddress_Write for the set domain address.
    // Check that the Supervisor sends this message via broadcast (on Destination Address and DoA 0000h) and repeats the A_DomainAddrWrite periodically for a maximum time smaller than 10 minutes (check in the PIXIT proforma of the Supervisor).

    //# Case C : distribution
    //#    00 00 EF 03 78 00 00 63 03 E0 12 35 0A 66
    pl132_data = { 0x00, 0x00, 0xEF, 0x03, 0x78, 0x00, 0x00, 0xE3, 0x03, 0xE0, 0x12, 0x35, 0x0A, 0x66 }; // A_DomainAddress_Write // @note 0x63 (individual) changed to 0xE3 (group)
    supervisor.pl132_data_link_layer.L_Plain_Data_req(0, pl132_data);
    io_context.poll();
    ASSERT_EQ(*log++, pl132_data);
    ASSERT_EQ(log, std::cend(observer.log));

    // Case D (Distribution process – positive): install auxiliary device 2, which is able to request a domain address with the Supervisor
    KNX::PL132_BCU2 aux_device_2(io_context, pl132_bus);
    aux_device_2.interface_objects->device()->domain_address()->domain_address = KNX::Domain_Address_2(0x1234);
    aux_device_2.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x03FF);

    // CASE D: Initiate at the auxiliary device the A_DomainAddrRead service.
    // Select at the Supervisor a domain address (e.g. 1235) to be written in the auxiliary device requesting a new domain address.
    // Check that the Supervisor initiates an A_DomainAddress_Response for the to be set domain address.
    // Check that the Supervisor sends this message via broadcast (on Destination Address and DoA 0000h).

    //# Case D : query from auxiliary device
    //# IN, A_DomainAddress_read
    //    2000,i,00 00 EF 03 FF 00 00 61 03 E1 0A 66
    pl132_data = { 0x00, 0x00, 0xEF, 0x03, 0xFF, 0x00, 0x00, 0xE1, 0x03, 0xE1, 0x0A, 0x66 }; // A_DomainAddress_Read // @note 0x61 (individual) changed to 0xE1 (group)
    aux_device_2.pl132_data_link_layer.L_Plain_Data_req(0, pl132_data);
    io_context.poll();
    ASSERT_EQ(*log++, pl132_data);

    //# expected response from supervisor:
    //#      00 00 EF 03 78 00 00 63 03 E2 12 35 0A 66
    pl132_data = { 0x00, 0x00, 0xEF, 0x03, 0x78, 0x00, 0x00, 0xE3, 0x03, 0xE2, 0x12, 0x35, 0x0A, 0x66 }; // A_DomainAddress_Response // @note 0x63 (individual) changed to 0xE3 (group)
    supervisor.pl132_data_link_layer.L_Plain_Data_req(0, pl132_data);
    io_context.poll();
    ASSERT_EQ(*log++, pl132_data);
}

/**
 * 4.2 CEMO/LTSS/SN (mandatory if supported)
 *
 * @ingroup KNX_08_TSSA_04_02
 * @todo Test_4_2
 */
TEST(Logical_Tag_Tests, Test_4_2)
{
    std::vector<uint8_t> tp0_data;

    // The auxiliary device shall have the default SNA address.

    // Stimulate the supervisor to send an A_Network_Parameter_Write service (IO
    // Type 0, Prop ID 57d, value of new SNA 88h). Check whether the routing counter
    // is set to 0 in the A_Network_Parameter_Write service and the service is sent
    // broadcast

    // CEMOSNS1.SEQ

    //# Individual address : 01 FF in reflex mode
    //# IN, Test of service A_NetworkParameter_Write SNA "88" from 01 45 to all 00 00
    //    12000,i,CD 95 00 00 01 45 03 E4 00 00 39 88 0A 66
    tp0_data = { 0xCD, 0x95, 0x00, 0x00, 0x01, 0x45, 0x03, 0xE4, 0x00, 0x00, 0x39, 0x88, 0x0A, 0x66 }; // A_NetworkParameter_Write

    //# Make the product send a frame to check that it uses the SNA value "88". Press BP
    //# OUT, Expected A-GroupValue-Write: CD 91 9F 01 88 FF 00 81 0B 77 (reflex mode)
    //# Test of service A_NetworkParameter_Write SNA "01" from 01 45 to all 00 00
    //    1000,i,CD 95 00 00 01 45 03 E4 00 00 39 01 0A 66
    tp0_data = { 0xCD, 0x95, 0x00, 0x00, 0x01, 0x45, 0x03, 0xE4, 0x00, 0x00, 0x39, 0x01, 0x0A, 0x66 }; // A_NetworkParameter_Write

    // CEMOSNS2.SEQ

    //# Individual address : 01 78 in supervised mode
    //# IN, Test of service A_NetworkParameter_Write SNA "88" from 01 45 to all 00 00
    //    12000,i,CD 95 00 00 01 45 03 E4 00 00 39 88 0A 66
    tp0_data = { 0xCD, 0x95, 0x00, 0x00, 0x01, 0x45, 0x03, 0xE4, 0x00, 0x00, 0x39, 0x88, 0x0A, 0x66 }; // A_NetworkParameter_Write

    //# Make the product send a frame to check that it uses the SNA value "88".
    //    Press BP
    //# OUT, Expected A-GroupValue-Write: CD 91 9F 01 88 78 00 81 0B 77
    //# Test of service A_NetworkParameter_Write SNA "01" from 01 45 to all 00 00
    //    1000,i,CD 95 00 00 01 45 03 E4 00 00 39 01 0A 66
    tp0_data = { 0xCD, 0x95, 0x00, 0x00, 0x01, 0x45, 0x03, 0xE4, 0x00, 0x00, 0x39, 0x01, 0x0A, 0x66 }; // A_NetworkParameter_Write
}

/**
 * 4.3 CEMO/LTSS/IA
 *
 * @ingroup KNX_08_TSSA_04_03
 * @todo Test_4_3
 */
TEST(Logical_Tag_Tests, Test_4_3)
{
    std::vector<uint8_t> tp0_data;

    // CEMOIAS1.SEQ

    //# NM_IndividualAddressWrite procedure
    //# Connection mode, Individual Address of supervisor 01 78
    //# OUT, Connection layer 4 from supervisor
    //#    CB 90 01 FF 01 78 80 0A 65
    tp0_data = { 0xCB, 0x90, 0x01, 0xFF, 0x01, 0x78, 0x80, 0x0A, 0x65 }; // A_Connect

    //# OUT, Descriptor type 0 = Mask version read request from supervisor
    //#    CB 91 01 FF 01 78 07 00 0A 65
    tp0_data = { 0xCB, 0x91, 0x01, 0xFF, 0x01, 0x78, 0x07, 0x00, 0x0A, 0x65 }; // A_DeviceDescriptor_Read

    //# IN, Expected A- DeviceDescriptor-Response from auxiliary device
    //    1000,i,CB 93 01 78 01 FF 07 40 30 12 0B 77
    tp0_data = { 0xCB, 0x93, 0x01, 0x78, 0x01, 0xFF, 0x07, 0x40, 0x30, 0x12, 0x0B, 0x77 }; // A_DeviceDescriptor_Response

    //# OUT, Disconnection layer 4 from supervisor
    //#    CB 90 01 FF 01 78 81 0A 65
    tp0_data = { 0xCB, 0x90, 0x01, 0xFF, 0x01, 0x78, 0x81, 0x0A, 0x65 }; // A_Disconnect

    //#Supervisor : sending of service A_PhysicalAddress_Read
    //#    CB 91 01 FF 01 78 01 00 0A 65
    tp0_data = { 0xCB, 0x91, 0x01, 0xFF, 0x01, 0x78, 0x01, 0x00, 0x0A, 0x65 }; // A_IndividualAddress_Read

    //# expected response
    //    1000,i,CB 93 01 78 01 FF 01 40 01 FF 0A 65
    tp0_data = { 0xCB, 0x93, 0x01, 0x78, 0x01, 0xFF, 0x01, 0x40, 0x01, 0xFF, 0x0A, 0x65 }; // A_IndividualAddress_Response

    //#Supervisor : sending of service A_PhysicalAddress_Write
    //#    CB 93 01 FF 01 78 00 C0 01 51 A 65
    tp0_data = { 0xCB, 0x93, 0x01, 0xFF, 0x01, 0x78, 0x00, 0xC0, 0x01, 0x51, 0x0A, 0x65 }; // A_IndividualAddress_Write

    //# OUT, Connection layer 4 from supervisor
    //#    CB 90 01 51 01 78 80 0A 65
    tp0_data = { 0xCB, 0x90, 0x01, 0x51, 0x01, 0x78, 0x80, 0x0A, 0x65 }; // A_Connect

    //# OUT, Descriptor type 0 = Mask version read request from supervisor
    //#    CB 92 01 51 01 78 07 00 00 0A 65
    tp0_data = { 0xCB, 0x92, 0x01, 0x51, 0x01, 0x78, 0x07, 0x00, 0x00, 0x0A, 0x65 }; // A_DeviceDescriptor_Read

    //# IN, Expected A- DeviceDescriptor-Response from auxiliary device
    //    1000,i,CB 93 01 78 01 51 07 40 30 12 0B 77
    tp0_data = { 0xCB, 0x93, 0x01, 0x78, 0x01, 0x51, 0x07, 0x40, 0x30, 0x12, 0x0B, 0x77 }; // A_DeviceDescriptor_Response

    //# OUT, A_Restart
    //#    CB 91 01 51 01 78 0B 80 0B 77
    tp0_data = { 0xCB, 0x91, 0x01, 0x51, 0x01, 0x78, 0x0B, 0x80, 0x0B, 0x77 }; // A_Restart

    //# OUT, Disconnection layer 4 from supervisor
    //    1000,i,CB 90 01 51 01 78 81 0A 65
    tp0_data = { 0xCB, 0x90, 0x01, 0x51, 0x01, 0x78, 0x81, 0x0A, 0x65 }; // A_Disconnect

    // CEMOIAS2.SEQ

    //# NM_IndividualAddressSerialNumberWrite procedure
    //# Individual Address of supervisor 01 78
    //#Supervisor : sending of service A_IndividualAddressSerialNumber_Write
    //#    CB 9D 01 FF 01 78 03 DE 30 40 50 60 70 80 01 51 00 00 00 00 0A 65
    tp0_data = { 0xCB, 0x9D, 0x01, 0xFF, 0x01, 0x78, 0x03, 0xDE, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x01, 0x51, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x65 }; // A_IndividualAddressSerialNumber_Write

    //#Supervisor : sending of service A_IndividualAddressSerialNumber_Read
    //#    CD 97 00 00 01 78 03 DC 30 40 50 60 70 80 0A 65
    tp0_data = { 0xCD, 0x97, 0x00, 0x00, 0x01, 0x78, 0x03, 0xDC, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x0A, 0x65 }; // A_IndividualAddressSerialNumber_Read

    //# expected response
    //    1000,i,CB 93 01 78 01 51 03 DD 30 40 50 60 70 80 00 00 00 00 0A 65
    tp0_data = { 0xCB, 0x93, 0x01, 0x78, 0x01, 0x51, 0x03, 0xDD, 0x30, 0x40, 0x50, 0x60, 0x70, 0x80, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x65 }; // A_IndividualAddressSerialNumber_Response
}

/**
 * 4.4 CEMO/LTSS/DI
 *
 * @ingroup KNX_08_TSSA_04_04
 * @todo Test_4_4
 */
TEST(Logical_Tag_Tests, Test_4_4)
{
    std::vector<uint8_t> tp0_data;

    // CEMODIS1.SEQ

    //# Device Identification done by LT supervisor
    //# Case A only, connectionless
    //# DeviceDescriptor-Read service addressed to correct device
    //# by supervisor at the Individual Address 01 78 (x/y=6/7)
    //# OUT, Descriptor type 0 = Mask version read request
    //#    CB 91 01 45 01 78 03 00 0A 65
    tp0_data = { 0xCB, 0x91, 0x01, 0x45, 0x01, 0x78, 0x03, 0x00, 0x0A, 0x65 }; // A_DeviceDescriptor_Read

    //# IN, Expected A- DeviceDescriptor-Response:
    //    1000,i,CB 93 01 78 01 45 03 40 30 12 0B 77
    tp0_data = { 0xCB, 0x93, 0x01, 0x78, 0x01, 0x45, 0x03, 0x40, 0x30, 0x12, 0x0B, 0x77 }; // A_DeviceDescriptor_Response

    //# OUT, Descriptor type 2 = Identification
    //#    CB 91 01 45 01 78 03 02 0A 65
    tp0_data = { 0xCB, 0x91, 0x01, 0x45, 0x01, 0x78, 0x03, 0x02, 0x0A, 0x65 }; // A_DeviceDescriptor_Read

    //# IN, Expected service A- DeviceDescriptor-Response:
    //    1000,i,CB 9F 01 78 01 45 03 42 00 64 22 02 4C 01 00 AE FF 00 00 00 00 00 0A 65
    tp0_data = { 0xCB, 0x9F, 0x01, 0x78, 0x01, 0x45, 0x03, 0x42, 0x00, 0x64, 0x22, 0x02, 0x4C, 0x01, 0x00, 0xAE, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x65 }; // A_DeviceDescriptor_Response

    // CEMODIS2.SEQ

    //# Device Identification done by LT supervisor
    //# Case A only, connection oriented
    //# DeviceDescriptor-Read service addressed to correct device
    //# by supervisor at the Individual Address 01 78 (x/y=6/7)
    //# OUT, Connection layer 4 from supervisor
    //#    CB 90 01 45 01 78 80 0A 65
    tp0_data = { 0xCB, 0x90, 0x01, 0x45, 0x01, 0x78, 0x80, 0x0A, 0x65 }; // A_Connect

    //# OUT, Descriptor type 0 = Mask version read request from supervisor
    //#    CB 91 01 45 01 78 07 00 0A 65
    tp0_data = { 0xCB, 0x91, 0x01, 0x45, 0x01, 0x78, 0x07, 0x00, 0x0A, 0x65 }; // A_DeviceDescriptor_Read

    //# IN, Expected A- DeviceDescriptor-Response from auxiliary device
    //    1000,i,CB 93 01 78 01 45 07 40 30 12 0B 77
    tp0_data = { 0xCB, 0x93, 0x01, 0x78, 0x01, 0x45, 0x07, 0x40, 0x30, 0x12, 0x0B, 0x77 }; // A_DeviceDescriptor_Response

    //# OUT, Descriptor type 2
    //#    CB 91 01 45 01 78 03 02 0A 66
    tp0_data = { 0xCB, 0x91, 0x01, 0x45, 0x01, 0x78, 0x03, 0x02, 0x0A, 0x66 }; // A_DeviceDescriptor_Read

    //# IN, Expected response from auxiliary device
    //    1000,i,CB 9F 01 78 01 45 03 42 00 64 22 02 4C 01 00 AE FF 00 00 00 00 00 0A 66
    tp0_data = { 0xCB, 0x9F, 0x01, 0x78, 0x01, 0x45, 0x03, 0x42, 0x00, 0x64, 0x22, 0x02, 0x4C, 0x01, 0x00, 0xAE, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x66 }; // A_DeviceDescriptor_Response

    //# OUT, Disconnection layer 4 from supervisor
    //#    CB 90 01 45 01 78 81 0A 65
    tp0_data = { 0xCB, 0x90, 0x01, 0x45, 0x01, 0x78, 0x81, 0x0A, 0x65 }; // A_Disconnect
}

/**
 * 4.5 CEMO/LTSS/GA
 *
 * @ingroup KNX_08_TSSA_04_05
 * @todo Test_4_5
 */
TEST(Logical_Tag_Tests, Test_4_5)
{
    std::vector<uint8_t> tp0_data;

    // CEMOGAS1.SEQ

    //#Case A
    //#GA check from supervisor for LT supervised correct device
    //# OUT, Request to all if the GA C1 23 is used by anybody (from supervisor)[case 1]
    //#    CD 97 00 00 01 78 03 DA 00 01 17 01 C1 23 0A 65
    tp0_data = { 0xCD, 0x97, 0x00, 0x00, 0x01, 0x78, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x01, 0xC1, 0x23, 0x0A, 0x65 }; // A_NetworkParameter_Read

    //# OUT, Expected service A_NetworkParameter_Response:
    //#      CB 97 01 78 01 BC 03 DB 00 01 17 01 C1 23 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0x78, 0x01, 0xBC, 0x03, 0xDB, 0x00, 0x01, 0x17, 0x01, 0xC1, 0x23, 0x0A, 0x65 }; // A_NetworkParameter_Response

    //# or OUT, Request (from supervisor) to one product (Individual Address 01 BC)[case 2]
    //#      if it uses the GA C1 23
    //#    CB 97 01 BC 01 78 03 DA 00 01 17 01 C1 23 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0xBC, 0x01, 0x78, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x01, 0xC1, 0x23, 0x0A, 0x65 }; // A_NetworkParameter_Read

    //# OUT, Expected service A_NetworkParameter_Response:
    //#      CB 97 01 78 01 BC 03 DB 00 01 17 01 C1 23 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0x78, 0x01, 0xBC, 0x03, 0xDB, 0x00, 0x01, 0x17, 0x01, 0xC1, 0x23, 0x0A, 0x65 }; // A_NetworkParameter_Response

    //#
    //# OUT, Request (from supervisor) to all if the GA 66 77 is used by anybody
    //#      (but nobody uses it)
    //#    CD 97 00 00 01 78 03 DA 00 01 17 01 66 77 0A 65
    tp0_data = { 0xCD, 0x97, 0x00, 0x00, 0x01, 0x78, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x01, 0x66, 0x77, 0x0A, 0x65 }; // A_NetworkParameter_Read

    //#      No answer expected
    //# or OUT, Request (from supervisor) to the product (address 01 BC)
    //#      if it uses the GA 66 77
    //#    CB 97 01 BC 01 78 03 DA 00 01 17 01 66 77 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0xBC, 0x01, 0x78, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x01, 0x66, 0x77, 0x0A, 0x65 }; // A_NetworkParameter_Read

    //#      No answer expected
    //    Case B [optional] (range equal to 7F)
    //# OUT, Request to all if any GA from C1 23 to C1 A3 are used by anybody [case 1]
    //#    CD 97 00 00 01 78 03 DA 00 01 17 7F C1 23 0A 65
    tp0_data = { 0xCD, 0x97, 0x00, 0x00, 0x01, 0x78, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x7F, 0xC1, 0x23, 0x0A, 0x65 }; // A_NetworkParameter_Read
}

/**
 * 4.6 CEMO/LTSS/LM
 *
 * @ingroup KNX_08_TSSA_04_06
 * @todo Test_4_6
 */
TEST(Logical_Tag_Tests, Test_4_6)
{
    std::vector<uint8_t> tp0_data;

    // CEMOLMS1.SEQ

    //# Link Management A_Link_Read
    //# [Group Object n° are tried in sequence]
    //# (Responses depend on the application :
    //#     n° of objects, n° of GA attached per Group Object)
    //# For LT supervised correct device, x/y = 8/9
    //#
    //# Case A
    //# OUT, Link read request start-index to check, from supervisor
    //#    CB 93 01 AA 01 78 03 E5 00 01 0A 65
    tp0_data = { 0xCB, 0x93, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xE5, 0x00, 0x01, 0x0A, 0x65 }; // A_Link_Read

    //# IN, Expected A_Link_Response from correct device
    //    1000,i,CB 95 01 78 01 AA 03 E6 00 11 C2 40 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xE6, 0x00, 0x11, 0xC2, 0x40, 0x0A, 0x65 }; // A_Link_Response

    //# or (case F) IN, if no association with the Group Object expected A_Link_Response:
    //        1000,i,CB 93 01 78 01 AA 03 E6 00 00 0A 65 (start-index at 0)
    tp0_data = { 0xCB, 0x93, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xE6, 0x00, 0x00, 0x0A, 0x65 }; // A_Link_Response

    //# OUT, Link read request, from supervisor
    //#    CB 93 01 AA 01 78 03 E5 FF 01 0A 65
    tp0_data = { 0xCB, 0x93, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xE5, 0xFF, 0x01, 0x0A, 0x65 }; // A_Link_Read

    //# IN, Expected A_Link_Response from correct device
    //        1000,i,CB 95 01 78 01 AA 03 E6 FF 11 C2 41 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xE6, 0xFF, 0x11, 0xC2, 0x41, 0x0A, 0x65 }; // A_Link_Response

    //# Case B
    //# OUT, Link read request, from supervisor
    //#    CB 93 01 AA 01 78 03 E5 01 01 0A 65
    tp0_data = { 0xCB, 0x93, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xE5, 0x01, 0x01, 0x0A, 0x65 }; // A_Link_Read

    //# IN, Expected A_Link_Response from correct device
    //        1000,i,CB 9F 01 78 01 AA 03 E6 01 41 C2 50 C2 51 C2 52 C2 53 C2 54 C2 55 0A 65
    tp0_data = { 0xCB, 0x9F, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xE6, 0x01, 0x41, 0xC2, 0x50, 0xC2, 0x51, 0xC2, 0x52, 0xC2, 0x53, 0xC2, 0x54, 0xC2, 0x55, 0x0A, 0x65 }; // A_Link_Response

    //# Case C
    //# OUT, Link read request, from supervisor
    //#    CB 93 01 AA 01 78 03 E5 01 07 0A 65
    tp0_data = { 0xCB, 0x93, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xE5, 0x01, 0x07, 0x0A, 0x65 }; // A_Link_Read

    //# IN, Expected A_Link_Response from correct device
    //        1000,i,CB 9B 01 78 01 AA 03 E6 01 47 C2 56 C2 57 C2 58 C2 59 0A 65
    tp0_data = { 0xCB, 0x9B, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xE6, 0x01, 0x47, 0xC2, 0x56, 0xC2, 0x57, 0xC2, 0x58, 0xC2, 0x59, 0x0A, 0x65 }; // A_Link_Response

    // CEMOLMS2.SEQ

    //# Case G
    //# A_Link_Write service by supervisor for LT supervised correct device
    //#
    //# OUT, Supervisor writes "C270" in the correct device to the object 4 with s flag to 1
    //#    D9 95 01 89 01 78 03 E7 04 01 C2 70 08 B3
    tp0_data = { 0xD9, 0x95, 0x01, 0x89, 0x01, 0x78, 0x03, 0xE7, 0x04, 0x01, 0xC2, 0x70, 0x08, 0xB3 }; // A_Link_Write

    //# IN, Expected service A_Link_Response from correct device :
    //    1000,i,D9 95 10 89 01 78 03 E6 04 11 C2 70 08 B3
    tp0_data = { 0xD9, 0x95, 0x10, 0x89, 0x01, 0x78, 0x03, 0xE6, 0x04, 0x11, 0xC2, 0x70, 0x08, 0xB3 }; // A_Link_Response

    //# OUT, verify it answers to this new Group Address (ON, then OFF)
    //#
    //# Case H
    //# OUT, Supervisor deletes C270 from the product
    //        1000,i,D9 95 01 89 01 78 03 E7 04 03 C2 70 08 B3
    tp0_data = { 0xD9, 0x95, 0x01, 0x89, 0x01, 0x78, 0x03, 0xE7, 0x04, 0x03, 0xC2, 0x70, 0x08, 0xB3 }; // A_Link_Write

    //# IN, Expected service A_Link_Response :
    //        1000,i,D9 93 10 89 01 78 03 E6 04 00 08 B3
    tp0_data = { 0xD9, 0x93, 0x10, 0x89, 0x01, 0x78, 0x03, 0xE6, 0x04, 0x00, 0x08, 0xB3 }; // A_Link_Response

    //#Case G and H may be repeated
}

/**
 * 4.7 CEMO/LTSS/NS (mandatory if supported)
 *
 * @ingroup KNX_08_TSSA_04_07
 * @todo Test_4_7
 */
TEST(Logical_Tag_Tests, Test_4_7)
{
    std::vector<uint8_t> tp0_data;

    // CEMONSS1.SEQ

    //# Reduced interface object with A_NetworkParameter_Read
    //# For supervisor x/y = 8/9
    //#
    //# Case A
    //# OUT, Serial number read request
    //#     (request coming from the supervisor 01 78 to 01 AA) (APCI = 03 DA)
    //#    CB 95 01 AA 01 78 03 DA 00 00 0B 00 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xDA, 0x00, 0x00, 0x0B, 0x00, 0x0A, 0x65 }; // A_NetworkParameter_Read

    //# As this service is optional in the correct device, maybe no response
    //# or IN, Expected service A_NetworkParameter_Response:
    //    1000,i,CB 9A 01 78 01 AA 03 DB 00 00 0B (then serial number on 6 bytes) 0A 65
    tp0_data = { 0xCB, 0x9A, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xDB, 0x00, 0x00, 0x0B, 0x0A, 0x65 }; // A_NetworkParameter_Response

    //#
    //# Case C
    //# OUT, Serial number read request
    //#     (request coming from the supervisor 01 78 to 01 BB) (APCI = 03 DA)
    //#    CB 95 01 BB 01 78 03 DA 00 00 0B 00 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0xBB, 0x01, 0x78, 0x03, 0xDA, 0x00, 0x00, 0x0B, 0x00, 0x0A, 0x65 }; // A_NetworkParameter_Read

    //# IN, Expected service A_NetworkParameter_Response with no serial number:
    //    1000,i,CB 9A 01 78 01 BB 03 DB 00 00 0B 00 00 00 00 00 00 0A 65
    tp0_data = { 0xCB, 0x9A, 0x01, 0x78, 0x01, 0xBB, 0x03, 0xDB, 0x00, 0x00, 0x0B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x65 }; // A_NetworkParameter_Response

    // CEMONSS2.SEQ

    //# Reduced interface object with A_NetworkParameter_Read
    //# For supervisor x/y = 8/9
    //#
    //# Case A
    //# OUT, Interface Object Type read request
    //#     (request coming from the supervisor 01 78 to 01AA) (APCI = 03 DA)
    //#    CB 95 01 AA 01 78 03 DA 00 32 01 00 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xDA, 0x00, 0x32, 0x01, 0x00, 0x0A, 0x65 }; // A_NetworkParameter_Read

    //# This service can also be sent to all (not tested here)
    //# IN, Expected service A_NetworkParameter_Response: (test info 00, test result 01)
    //    1000,i,CB 96 01 78 01 AA 03 DB 00 32 01 00 01 0A 65
    tp0_data = { 0xCB, 0x96, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xDB, 0x00, 0x32, 0x01, 0x00, 0x01, 0x0A, 0x65 }; // A_NetworkParameter_Response
}

/**
 * 4.8 CEMO/LTSS/ECH-PA
 *
 * @ingroup KNX_08_TSSA_04_08
 * @todo Test_4_8
 */
TEST(Logical_Tag_Tests, Test_4_8)
{
    std::vector<uint8_t> tp0_data;

    // CMOPAS1.SEQ

    //# For LT supervised correct device, x/y = 8/9
    //# Parameters
    //# Preliminary = read size of blocks by the supervisor
    //# OUT, PropertyDescriptor size read request on property "Channel parameter" channel 1
    //#    CB 94 01 AA 01 78 03 D8 00 65 00 0A 65
    tp0_data = { 0xCB, 0x94, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD8, 0x00, 0x65, 0x00, 0x0A, 0x65 }; // A_PropertyDescription_Read

    //# IN, Expected A_PropertyDescription_Response:
    //    1000,i,CB 97 01 78 01 AA 03 D9 00 65 00 03 02 00 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD9, 0x00, 0x65, 0x00, 0x03, 0x02, 0x00, 0x0A, 0x65 }; // A_PropertyDescription_Response

    //# OUT, PropertyDescriptor size read request on property "Channel parameter" channel 2
    //#    CB 94 01 AA 01 78 03 D8 00 66 00 0A 65
    tp0_data = { 0xCB, 0x94, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD8, 0x00, 0x66, 0x00, 0x0A, 0x65 }; // A_PropertyDescription_Read

    //# IN, Expected A_PropertyDescription_Response:
    //        1000,i,CB 97 01 78 01 AA 03 D9 00 66 00 03 04 00 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD9, 0x00, 0x66, 0x00, 0x03, 0x04, 0x00, 0x0A, 0x65 }; // A_PropertyDescription_Response

    //#
    //# First part = read by the supervisor
    //# OUT, Device Object read request on property "Channel parameter" channel 1
    //#    CB 95 01 AA 01 78 03 D5 00 65 20 01 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD5, 0x00, 0x65, 0x20, 0x01, 0x0A, 0x65 }; // A_PropertyValue_Read

    //# IN, Expected (block 11 12):
    //        1000,i,CB 97 01 78 01 AA 03 D6 00 65 20 01 11 12 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x00, 0x65, 0x20, 0x01, 0x11, 0x12, 0x0A, 0x65 }; // A_PropertyValue_Response

    //# OUT, Device Object read request on property "Channel parameter" channel 2
    //#    CB 95 01 AA 01 78 03 D5 00 66 20 01 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD5, 0x00, 0x66, 0x20, 0x01, 0x0A, 0x65 }; // A_PropertyValue_Read

    //# IN, Expected (block 21 22 23 24):
    //        1000,i,CB 99 01 78 01 AA 03 D6 00 65 40 01 21 22 23 24 0A 65
    tp0_data = { 0xCB, 0x99, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x00, 0x65, 0x40, 0x01, 0x21, 0x22, 0x23, 0x24, 0x0A, 0x65 }; // A_PropertyValue_Response

    //#
    //# Second part = Write by the supervisor
    //# OUT, Device Object write property "Channel parameter" channel 1, 2 bytes
    //#    CB 97 01 AA 01 78 03 D7 00 65 20 01 22 22 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD7, 0x00, 0x65, 0x20, 0x01, 0x22, 0x22, 0x0A, 0x65 }; // A_PropertyValue_Write

    //# IN, Expected:
    //        1000,i,CB 97 01 78 01 AA 03 D6 00 65 20 01 22 22 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x00, 0x65, 0x20, 0x01, 0x22, 0x22, 0x0A, 0x65 }; // A_PropertyValue_Response

    //# OUT, Device Object write property "Channel parameter" channel 2, 4 bytes
    //#    CB 99 01 AA 01 78 03 D7 00 66 40 01 33 33 44 44 0A 65
    tp0_data = { 0xCB, 0x99, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD7, 0x00, 0x66, 0x40, 0x01, 0x33, 0x33, 0x44, 0x44, 0x0A, 0x65 }; // A_PropertyValue_Write

    //# IN, Expected:
    //        1000,i,CB 99 01 78 01 AA 03 D6 00 66 40 01 33 33 44 44 0A 65
    tp0_data = { 0xCB, 0x99, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x00, 0x66, 0x40, 0x01, 0x33, 0x33, 0x44, 0x44, 0x0A, 0x65 }; // A_PropertyValue_Response

    //#
    //# Third part = Partial Write by the supervisor
    //# OUT, Device Object write property "Channel parameter" channel 2, 3 bytes
    //#    CB 98 01 AA 01 78 03 D7 00 66 30 01 55 66 77 0A 65
    tp0_data = { 0xCB, 0x98, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD7, 0x00, 0x66, 0x30, 0x01, 0x55, 0x66, 0x77, 0x0A, 0x65 }; // A_PropertyValue_Write

    //# IN, Expected:
    //        1000,i,CB 99 01 78 01 AA 03 D6 00 66 40 01 55 66 77 44 0A 65
    tp0_data = { 0xCB, 0x99, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x00, 0x66, 0x40, 0x01, 0x55, 0x66, 0x77, 0x44, 0x0A, 0x65 }; // A_PropertyValue_Response

    //# Case A
    //# OUT, Device Object write property "Channel parameter" channel 3, 1 byte
    //#    CB 96 01 AA 01 78 03 D7 01 67 10 01 0A 0A 65
    tp0_data = { 0xCB, 0x96, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD7, 0x01, 0x67, 0x10, 0x01, 0x0A, 0x0A, 0x65 }; // A_PropertyValue_Write

    //# IN, Expected:
    //        1000,i,CB 96 01 78 01 AA 03 D6 01 67 10 01 0A 0A 65
    tp0_data = { 0xCB, 0x96, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x01, 0x67, 0x10, 0x01, 0x0A, 0x0A, 0x65 }; // A_PropertyValue_Response

    //# Case B
    //# OUT, Device Object write property "Channel parameter" channel 3, 1 byte
    //#    CB 96 01 AA 01 78 03 D7 01 67 10 02 10 0A 65
    tp0_data = { 0xCB, 0x96, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD7, 0x01, 0x67, 0x10, 0x02, 0x10, 0x0A, 0x65 }; // A_PropertyValue_Write

    //# IN, Expected:
    //        1000,i,CB 97 01 78 01 AA 03 D6 01 67 20 01 0A 10 0A 65
    tp0_data = { 0xCB, 0x97, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x01, 0x67, 0x20, 0x01, 0x0A, 0x10, 0x0A, 0x65 }; // A_PropertyValue_Response

    //# Case C
    //# OUT, Device Object write property "Channel parameter" channel 3, 1 byte
    //#    CB 96 01 AA 01 78 03 D7 01 67 10 01 0A 0A 65
    tp0_data = { 0xCB, 0x96, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD7, 0x01, 0x67, 0x10, 0x01, 0x0A, 0x0A, 0x65 }; // A_PropertyValue_Write

    //# IN, Expected (negative answer):
    //        1000,i,CB 95 01 78 01 AA 03 D6 01 67 10 00 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x01, 0x67, 0x10, 0x00, 0x0A, 0x65 }; // A_PropertyValue_Response

    //# Case D
    //# OUT, Device Object read property "Channel parameter" channel 3, 1 byte
    //#    CB 95 01 AA 01 78 03 D5 01 67 10 01 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD5, 0x01, 0x67, 0x10, 0x01, 0x0A, 0x65 }; // A_PropertyValue_Read

    //# IN, Expected:
    //        1000,i,CB 96 01 78 01 AA 03 D6 01 67 10 01 0A 0A 65
    tp0_data = { 0xCB, 0x96, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x01, 0x67, 0x10, 0x01, 0x0A, 0x0A, 0x65 }; // A_PropertyValue_Response

    //# Case E
    //# OUT, Device Object read property "Channel parameter" channel 3, 1 byte
    //#    CB 95 01 AA 01 78 03 D5 01 67 10 02 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD5, 0x01, 0x67, 0x10, 0x02, 0x0A, 0x65 }; // A_PropertyValue_Read

    //# IN, Expected:
    //        1000,i,CB 96 01 78 01 AA 03 D6 01 67 10 02 10 0A 65
    tp0_data = { 0xCB, 0x96, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x01, 0x67, 0x10, 0x02, 0x10, 0x0A, 0x65 }; // A_PropertyValue_Response

    //# Case F
    //# OUT, Device Object read property "Channel parameter" channel 3, 1 byte
    //#    CB 95 01 AA 01 78 03 D5 01 67 10 01 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0xAA, 0x01, 0x78, 0x03, 0xD5, 0x01, 0x67, 0x10, 0x01, 0x0A, 0x65 }; // A_PropertyValue_Read

    //# IN, Expected (negative answer):
    //        1000,i,CB 95 01 78 01 AA 03 D6 01 67 10 00 0A 65
    tp0_data = { 0xCB, 0x95, 0x01, 0x78, 0x01, 0xAA, 0x03, 0xD6, 0x01, 0x67, 0x10, 0x00, 0x0A, 0x65 }; // A_PropertyValue_Response
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
