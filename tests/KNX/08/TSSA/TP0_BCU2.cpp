// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "TP0_BCU2.h"

#include <KNX/03/04/01/Property.h>
#include <KNX/03/05/01/00_Device/Device_Object.h>
#include <KNX/03/05/01/03_Application_Program/Application_Program.h>
#include <KNX/03/05/01/19_RF_Medium/RF_Medium_Object.h>
#include <KNX/03/05/01/Interface_Object_Type.h>
#include <KNX/03/07/03/Property_Datatype_ID.h>

namespace KNX {

TP0_BCU2::TP0_BCU2(asio::io_context & io_context, TP0_Bus_Simulation & tp0_bus_simulation) :
    EMI_PEI(io_context),
    tp0_physical_layer(io_context),
    tp0_data_link_layer(tp0_physical_layer, io_context)
{
    authorization_keys.resize(16); // 0..15
    *current_level = authorization_keys.size() - 1;

    // Interface Objects mainly for 1.6 test cases
    // (0) Device Object
    // created in EMI_PEI constructor
    interface_objects->device()->interface_object_name()->object_name = "Device Obj";
    interface_objects->device()->interface_object_name()->description.max_nr_of_elem = interface_objects->device()->interface_object_name()->object_name.length();
    interface_objects->device()->interface_object_name()->nr_of_elem = interface_objects->device()->interface_object_name()->object_name.length();
    (void) interface_objects->device()->service_control();
    (void) interface_objects->device()->firmware_revision();
    (void) interface_objects->device()->serial_number();
    (void) interface_objects->device()->manufacturer_identifier();
    (void) interface_objects->device()->device_control();
    (void) interface_objects->device()->order_info();
    (void) interface_objects->device()->version();
    (void) interface_objects->device()->routing_count();
    (void) interface_objects->device()->maximum_retry_count();
    (void) interface_objects->device()->programming_mode();
    (void) interface_objects->device()->product_id();
    (void) interface_objects->device()->max_apdu_length();
    interface_objects->device()->psu_type()->psu_type.unsigned_value = 10;
    interface_objects->device()->psu_status()->psu_status.b = true;
    interface_objects->device()->psu_enable()->psu_enable.field1 = 2;
    (void) interface_objects->device()->domain_address();
    (void) interface_objects->device()->device_descriptor();
    // (1) Group Address Table
    // created in EMI_PEI constructor
    interface_objects->group_address_table()->interface_object_name()->object_name = "GrAT";
    interface_objects->group_address_table()->interface_object_name()->description.max_nr_of_elem = interface_objects->group_address_table()->interface_object_name()->object_name.length();
    interface_objects->group_address_table()->interface_object_name()->nr_of_elem = interface_objects->group_address_table()->interface_object_name()->object_name.length();
    interface_objects->group_address_table()->load_control()->load_state_machine = interface_objects->group_address_table()->load_state_machine;
    (void) interface_objects->group_address_table()->table();
    // (2) Group Object Association Table
    // created in EMI_PEI constructor
    interface_objects->group_object_association_table()->interface_object_name()->object_name = "GrOAT";
    interface_objects->group_object_association_table()->interface_object_name()->description.max_nr_of_elem = interface_objects->group_object_association_table()->interface_object_name()->object_name.length();
    interface_objects->group_object_association_table()->interface_object_name()->nr_of_elem = interface_objects->group_object_association_table()->interface_object_name()->object_name.length();
    interface_objects->group_object_association_table()->load_control()->load_state_machine = interface_objects->group_object_association_table()->load_state_machine;
    (void) interface_objects->group_object_association_table()->table();
    // (3) Application Program
    (void) interface_objects->application_program()->interface_object_type();
    interface_objects->application_program()->interface_object_name()->object_name = "ApplPrg";
    interface_objects->application_program()->interface_object_name()->description.max_nr_of_elem = interface_objects->application_program()->interface_object_name()->object_name.length();
    interface_objects->application_program()->interface_object_name()->nr_of_elem = interface_objects->application_program()->interface_object_name()->object_name.length();
    interface_objects->application_program()->load_control()->load_state_machine = interface_objects->application_program()->load_state_machine;
    interface_objects->application_program()->run_control()->run_state_machine = interface_objects->application_program()->run_state_machine;
    interface_objects->application_program()->application_version()->description.write_enable = true;
    // (9) Group Object Table
    // created in EMI_PEI constructor
    interface_objects->group_object_table()->interface_object_name()->object_name = "GrObjT";
    interface_objects->group_object_table()->interface_object_name()->description.max_nr_of_elem = interface_objects->group_object_table()->interface_object_name()->object_name.length();
    interface_objects->group_object_table()->interface_object_name()->nr_of_elem = interface_objects->group_object_table()->interface_object_name()->object_name.length();
    interface_objects->group_object_table()->load_control()->load_state_machine = interface_objects->group_object_table()->load_state_machine;
    (void) interface_objects->group_object_table()->group_object_table();
    (void) interface_objects->group_object_table()->extended_group_object_reference();
    interface_objects->sort_by_object_type();

    // physical layer
    tp0_physical_layer.connect(tp0_bus_simulation);

    // network layer
    connect(tp0_data_link_layer);
    tp0_data_link_layer.L_Service_Information_ind = std::bind(&TP0_BCU2::local_L_Service_Information_ind, this);

    // transport layer

    // application layer
    A_IndividualAddress_Write_ind_initiator();
    A_IndividualAddress_Read_ind_initiator();
    A_IndividualAddressSerialNumber_Read_ind_initiator();
    A_IndividualAddressSerialNumber_Write_ind_initiator();
    A_NetworkParameter_Read_Acon_initiator();
    A_DeviceDescriptor_Read_ind_initiator();
    A_DomainAddress_Write_ind_initiator();
    A_DomainAddress_Read_ind_initiator();
    A_DomainAddressSelective_Read_ind_initiator();
    A_DomainAddressSerialNumber_Read_ind_initiator();
    A_DomainAddressSerialNumber_Write_ind_initiator();
    A_ADC_Read_ind_initiator();
    A_Memory_Read_ind_initiator();
    A_Memory_Write_ind_initiator();
    A_MemoryBit_Write_ind_initiator();
    A_UserMemory_Read_ind_initiator();
    A_UserMemory_Write_ind_initiator();
    A_UserMemoryBit_Write_ind_initiator();
    A_UserManufacturerInfo_Read_ind_initiator();
    A_Authorize_Request_ind_initiator();
    A_Key_Write_ind_initiator();
    A_Open_Routing_Table_ind_initiator();
    A_Read_Routing_Table_ind_initiator();
    A_Write_Routing_Table_ind_initiator();
    A_Read_Router_Memory_ind_initiator();
    A_Write_Router_Memory_ind_initiator();

    // application interface layer

    // EMI/PEI
    emi_version = EMI_Version::EMI; // use internal version
    emi_con = std::bind(&TP0_BCU2::local_emi_con, this, std::placeholders::_1);
    emi_ind = std::bind(&TP0_BCU2::local_emi_ind, this, std::placeholders::_1);
}

void TP0_BCU2::local_emi_con(const std::vector<uint8_t> data)
{
    pei_data_con.push_back(data);
}

void TP0_BCU2::local_emi_ind(const std::vector<uint8_t> data)
{
    pei_data_ind.push_back(data);
}

void TP0_BCU2::local_L_Service_Information_ind()
{
    // Only send A_ServiceInformation_Indication once. If SA==DA, it can result in a local loop, where sender sees own frames.
    if (!interface_objects->device()->device_control()->device_control.individual_address_duplication()) {
        interface_objects->device()->device_control()->device_control.set_individual_address_duplication(true);
        application_layer.A_ServiceInformation_Indication_req(Ack_Request::dont_care, Priority::system, Network_Layer_Parameter, 0x20000, [](const KNX::Status a_status) -> void {
            // do nothing
        });
    }
}

void TP0_BCU2::A_IndividualAddress_Write_ind_initiator()
{
    application_layer.A_IndividualAddress_Write_ind([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const Individual_Address newaddress) -> void {
        if (interface_objects->device()->programming_mode()->prog_mode) {
            // @todo individual address stored at two locations
            interface_objects->group_address_table()->individual_address = newaddress;
            //            interface_objects->device()->subnetwork_address()->subnetwork_address = newaddress.subnetwork_address();
            //            interface_objects->device()->device_address()->device_address = newaddress.device_address();
        }

        A_IndividualAddress_Write_ind_initiator();
    });
}

void TP0_BCU2::A_IndividualAddress_Read_ind_initiator()
{
    application_layer.A_IndividualAddress_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type) -> void {
        if (interface_objects->device()->programming_mode()->prog_mode) {
            application_layer.A_IndividualAddress_Read_res(Ack_Request::dont_care, priority, hop_count_type, interface_objects->group_address_table()->individual_address, [](const Status /*a_status*/) -> void {
                // Rcon
            });
        }

        A_IndividualAddress_Read_ind_initiator();
    });
}

void TP0_BCU2::A_IndividualAddressSerialNumber_Read_ind_initiator()
{
    application_layer.A_IndividualAddressSerialNumber_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const Serial_Number serial_number) -> void {
        if (interface_objects->device()->serial_number()->serial_number == serial_number) {
            application_layer.A_IndividualAddressSerialNumber_Read_res(Ack_Request::dont_care, priority, hop_count_type, serial_number, interface_objects->device()->domain_address()->domain_address, [](const Status /*a_status*/) -> void {
                // Rcon
            });
        }

        A_IndividualAddressSerialNumber_Read_ind_initiator();
    });
}

void TP0_BCU2::A_IndividualAddressSerialNumber_Write_ind_initiator()
{
    application_layer.A_IndividualAddressSerialNumber_Write_ind([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const Serial_Number serial_number, const Individual_Address newaddress) -> void {
        if (interface_objects->device()->serial_number()->serial_number == serial_number) {
            // @todo individual address stored at two locations
            interface_objects->group_address_table()->individual_address = newaddress;
            //            interface_objects->device()->subnetwork_address()->subnetwork_address = newaddress.subnetwork_address();
            //            interface_objects->device()->device_address()->device_address = newaddress.device_address();
        }

        A_IndividualAddressSerialNumber_Write_ind_initiator();
    });
}

void TP0_BCU2::A_NetworkParameter_Read_Acon_initiator()
{
    application_layer.A_NetworkParameter_Read_Acon([this](const ASAP_Individual /*asap*/, const Hop_Count_Type /*hop_count_type*/, const Individual_Address /*individual_address*/, const Parameter_Type parameter_type, const Priority /*priority*/, const Parameter_Test_Info_Result test_info_result) -> void {
        switch (parameter_type.object_type) {
        case OT_DEVICE:
            switch (parameter_type.pid) {
            case Device_Object::PID_SUBNET_ADDR: {
                if (test_info_result[0] == 0x00) {
                    interface_objects->device()->subnetwork_address()->subnetwork_address = test_info_result[1];
                }
            }
            break;
            }
            break;
        }

        A_NetworkParameter_Read_Acon_initiator();
    });
}

void TP0_BCU2::A_DeviceDescriptor_Read_ind_initiator()
{
    // @todo Device_Descriptor_Type_2 device_descriptor_type_2 instead of plain vector
    static const std::vector<uint8_t> device_descriptor_2_data { 0x00, 0xFD, 0x90, 0x01, 0x10, 0x3F, 0x1F, 0xF4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    application_layer.A_DeviceDescriptor_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Individual asap, const Descriptor_Type descriptor_type) -> void {
        switch (descriptor_type) {
        case 0: // Device Descriptor 0 (Mask Version)
            application_layer.A_DeviceDescriptor_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, descriptor_type, interface_objects->device()->device_descriptor()->device_descriptor.toData(), [](const KNX::Status a_status) -> void {
                // do nothing
            });
            break;
        case 2: // Device Descriptor 2
            application_layer.A_DeviceDescriptor_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, descriptor_type, device_descriptor_2_data, [](const KNX::Status a_status) -> void {
                // do nothing
            });
            break;
        default:
            // error code
            application_layer.A_DeviceDescriptor_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0x3F, {}, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        };

        A_DeviceDescriptor_Read_ind_initiator();
    });
}

void TP0_BCU2::A_DomainAddress_Write_ind_initiator()
{
    application_layer.A_DomainAddress_Write_ind([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const Domain_Address domain_address_new) -> void {
        const std::vector<uint8_t> data = domain_address_new.toData();
        if (interface_objects->device()->programming_mode()->prog_mode) {
            switch (domain_address_new.size()) {
            case 2:
                interface_objects->device()->domain_address()->domain_address.fromData(std::cbegin(data), std::cend(data));
                break;
            case 6:
                interface_objects->device()->rf_domain_address()->rf_domain_address.fromData(std::cbegin(data), std::cend(data));
                break;
            default:
                break;
            }
        }

        A_DomainAddress_Write_ind_initiator();
    });
}

void TP0_BCU2::A_DomainAddress_Read_ind_initiator()
{
    application_layer.A_DomainAddress_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type) -> void {
        if (interface_objects->device()->programming_mode()->prog_mode) {
            application_layer.A_DomainAddress_Read_res(Ack_Request::dont_care, priority, hop_count_type, interface_objects->device()->domain_address()->domain_address, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        }

        A_DomainAddress_Read_ind_initiator();
    });
}

void TP0_BCU2::A_DomainAddressSelective_Read_ind_initiator()
{
    application_layer.A_DomainAddressSelective_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASDU /*asdu*/) -> void {
        // 0x00, 0x05, 0x7F, 0xFE, 0x03
        // Domain Address=0005, StartAddr=7FFE, Range=03
        application_layer.A_DomainAddress_Read_res(Ack_Request::dont_care, priority, hop_count_type, interface_objects->device()->domain_address()->domain_address, [](const KNX::Status a_status) -> void {
            // do nothing
        });

        A_DomainAddressSelective_Read_ind_initiator();
    });
}

void TP0_BCU2::A_DomainAddressSerialNumber_Read_ind_initiator()
{
    application_layer.A_DomainAddressSerialNumber_Read_ind([this](const Hop_Count_Type hop_count_type, const Priority priority, const Serial_Number serial_number) -> void {
        if (interface_objects->device()->serial_number()->serial_number == serial_number) {
            application_layer.A_DomainAddressSerialNumber_Read_res(Ack_Request::dont_care, interface_objects->device()->domain_address()->domain_address, hop_count_type, priority, serial_number, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        }

        A_DomainAddressSerialNumber_Read_ind_initiator();
    });
}

void TP0_BCU2::A_DomainAddressSerialNumber_Write_ind_initiator()
{
    application_layer.A_DomainAddressSerialNumber_Write_ind([this](const Domain_Address /*domain_address*/, const Hop_Count_Type /*hop_count_type*/, const Priority /*priority*/, const Serial_Number /*serial_number*/) -> void {
        // @todo local_A_DomainAddressSerialNumber_Write_ind

        A_DomainAddressSerialNumber_Write_ind_initiator();
    });
}

void TP0_BCU2::A_ADC_Read_ind_initiator()
{
    application_layer.A_ADC_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const ADC_Channel_Nr channel_nr, const ADC_Read_Count read_count) -> void {
        if (channel_nr == 0) {
            if (read_count == 1) {
                // Correct Channel Number (Channel 0 and 1 count)
                const ADC_Sum sum = 0x1234;
                application_layer.A_ADC_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, channel_nr, read_count, sum, [](const KNX::Status a_status) -> void {
                    // do nothing
                });
            }
        } else {
            if (channel_nr == 8) {
                if (read_count == 1) {
                    // Incorrect Channel Number (Channel 8 and 1 count)
                    application_layer.A_ADC_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, channel_nr, 0, 0, [](const KNX::Status a_status) -> void {
                        // do nothing
                    });
                }
            }
        }

        A_ADC_Read_ind_initiator();
    });
}

void TP0_BCU2::A_Memory_Read_ind_initiator()
{
    application_layer.A_Memory_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address) -> void {
        if (number <= 10) {
            for (auto & mem : memory) {
                if ((mem.first <= memory_address) && (mem.first + mem.second.size() >= memory_address + number)) {
                    Level memory_level = authorization_keys.size() - 1;
                    if (memory_min_read_level.count(mem.first)) {
                        memory_level = memory_min_read_level[mem.first];
                    }
                    if (memory_level >= *current_level) {
                        Memory_Data data;
                        data.assign(std::cbegin(mem.second) + (memory_address - mem.first), std::cbegin(mem.second) + (memory_address - mem.first) + number);
                        application_layer.A_Memory_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, number, memory_address, data, [](const KNX::Status a_status) -> void {
                            // do nothing
                        });
                        A_Memory_Read_ind_initiator();
                        return;
                    }
                }
            }
        }

        // illegal length, (partly) protected memory
        application_layer.A_Memory_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0, memory_address, {}, [](const KNX::Status a_status) -> void {
            // do nothing
        });

        A_Memory_Read_ind_initiator();
    });
}

void TP0_BCU2::A_Memory_Write_ind_initiator()
{
    application_layer.A_Memory_Write_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Memory_Number number, const Memory_Address memory_address, const Memory_Data data) -> void {
        if ((number == data.size()) && (number <= 10)) {
            for (auto & mem : memory) {
                if ((mem.first <= memory_address) && (mem.first + mem.second.size() >= memory_address + number)) {
                    std::copy(std::cbegin(data), std::cend(data), std::begin(mem.second) + (memory_address - mem.first));
                    if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
                        application_layer.A_Memory_Write_res(Ack_Request::dont_care, priority, hop_count_type, asap, number, memory_address, data, [](const KNX::Status a_status) -> void {
                            // do nothing
                        });
                    }
                    A_Memory_Write_ind_initiator();
                    return;
                }
            }
        }

        // illegal length, (partly) protected memory
        if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
            application_layer.A_Memory_Write_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0, memory_address, {}, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        }

        A_Memory_Write_ind_initiator();
    });
}

void TP0_BCU2::A_MemoryBit_Write_ind_initiator()
{
    application_layer.A_MemoryBit_Write_ind([this](const Priority priority, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected asap, const MemoryBit_Number number, const MemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data) -> void {
        if ((number == and_data.size()) && (number == xor_data.size()) && (number <= 10)) {
            for (auto & mem : memory) {
                if ((mem.first <= memory_address) && (mem.first + mem.second.size() >= memory_address + number)) {
                    for (MemoryBit_Number i = 0; i < number; ++i) {
                        mem.second[(memory_address - mem.first) + i] = (and_data[i] & mem.second[(memory_address - mem.first) + i]) ^ xor_data[i];
                    }
                    if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
                        Memory_Data data;
                        data.assign(std::cbegin(mem.second) + (memory_address - mem.first), std::cbegin(mem.second) + (memory_address - mem.first) + number);
                        application_layer.A_MemoryBit_Write_res(asap, priority, number, memory_address, data, [](const KNX::Status a_status) -> void {
                            // do nothing
                        });
                    }
                    A_MemoryBit_Write_ind_initiator();
                    return;
                }
            }
        }

        // illegal length, (partly) protected memory
        if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
            application_layer.A_MemoryBit_Write_res(asap, priority, 0, memory_address, {}, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        }

        A_MemoryBit_Write_ind_initiator();
    });
}

void TP0_BCU2::A_UserMemory_Read_ind_initiator()
{
    application_layer.A_UserMemory_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address) -> void {
        if (number <= 10) {
            for (auto & mem : user_memory) {
                if ((mem.first <= memory_address) && (mem.first + mem.second.size() >= memory_address + number)) {
                    //                    Level memory_level = authorization_keys.size() - 1;
                    //                    if (memory_min_read_level.count(mem.first)) {
                    //                        memory_level = memory_min_read_level[mem.first];
                    //                    }
                    //                    if (memory_level >= current_level) {
                    Memory_Data data;
                    data.assign(std::cbegin(mem.second) + (memory_address - mem.first), std::cbegin(mem.second) + (memory_address - mem.first) + number);
                    application_layer.A_UserMemory_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, number, memory_address, data, [](const KNX::Status a_status) -> void {
                        // do nothing
                    });
                    A_UserMemory_Read_ind_initiator();
                    return;
                    //                    }
                }
            }
        }

        // illegal length, (partly) protected memory
        application_layer.A_UserMemory_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0, memory_address, {}, [](const KNX::Status a_status) -> void {
            // do nothing
        });

        A_UserMemory_Read_ind_initiator();
    });
}

void TP0_BCU2::A_UserMemory_Write_ind_initiator()
{
    application_layer.A_UserMemory_Write_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemory_Number number, const UserMemory_Address memory_address, const Memory_Data data) -> void {
        if ((number == data.size()) && (number <= 10)) {
            for (auto & mem : user_memory) {
                if ((mem.first <= memory_address) && (mem.first + mem.second.size() > memory_address + number)) {
                    std::copy(std::cbegin(data), std::cend(data), std::begin(mem.second) + (memory_address - mem.first));
                    if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
                        application_layer.A_UserMemory_Write_res(Ack_Request::dont_care, priority, hop_count_type, asap, number, memory_address, data, [](const KNX::Status a_status) -> void {
                            // do nothing
                        });
                    }
                    A_UserMemory_Write_ind_initiator();
                    return;
                }
            }
        }

        // illegal length, (partly) protected memory
        if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
            application_layer.A_UserMemory_Write_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0, memory_address, {}, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        }

        A_UserMemory_Write_ind_initiator();
    });
}

void TP0_BCU2::A_UserMemoryBit_Write_ind_initiator()
{
    application_layer.A_UserMemoryBit_Write_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const UserMemoryBit_Number number, const UserMemoryBit_Address memory_address, const Memory_Data and_data, const Memory_Data xor_data) -> void {
        if ((number == and_data.size()) && (number == xor_data.size()) && (number <= 10)) {
            for (auto & mem : user_memory) {
                if ((mem.first <= memory_address) && (mem.first + mem.second.size() >= memory_address + number)) {
                    for (UserMemoryBit_Number i = 0; i < number; ++i) {
                        mem.second[(memory_address - mem.first) + i] = (and_data[i] & mem.second[(memory_address - mem.first) + i]) ^ xor_data[i];
                    }
                    if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
                        Memory_Data data;
                        data.assign(std::cbegin(mem.second) + (memory_address - mem.first), std::cbegin(mem.second) + (memory_address - mem.first) + number);
                        application_layer.A_UserMemoryBit_Write_res(Ack_Request::dont_care, priority, hop_count_type, asap, number, memory_address, data, [](const KNX::Status a_status) -> void {
                            // do nothing
                        });
                    }
                    A_UserMemoryBit_Write_ind_initiator();
                    return;
                }
            }
        }

        // illegal length, (partly) protected memory
        if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
            application_layer.A_UserMemoryBit_Write_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0, memory_address, {}, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        }

        A_UserMemoryBit_Write_ind_initiator();
    });
}

void TP0_BCU2::A_UserManufacturerInfo_Read_ind_initiator()
{
    application_layer.A_UserManufacturerInfo_Read_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap) -> void {
        const MFact_Info mfact_info{0x12, 0x1234}; // Code=12, Type Number=1234
        application_layer.A_UserManufacturerInfo_Read_res(Ack_Request::dont_care, priority, hop_count_type, asap, mfact_info, [](const KNX::Status a_status) -> void {
            // do nothing
        });

        A_UserManufacturerInfo_Read_ind_initiator();
    });
}

void TP0_BCU2::A_Authorize_Request_ind_initiator()
{
    application_layer.A_Authorize_Request_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Key key) -> void {
        Level level_from_key = 0;
        for (const auto & authorization_key : authorization_keys) {
            if (authorization_key == key) {
                break;
            }
            ++level_from_key;
        }

        if (level_from_key < authorization_keys.size()) {
            *current_level = level_from_key;
            application_layer.A_Authorize_Request_res(Ack_Request::dont_care, priority, hop_count_type, asap, *current_level, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        } else {
            *current_level = authorization_keys.size() - 1;
        }

        A_Authorize_Request_ind_initiator();
    });
}

void TP0_BCU2::A_Key_Write_ind_initiator()
{
    application_layer.A_Key_Write_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const Level level, const Key key) -> void {
        if ((level >= *current_level) && (level < authorization_keys.size())) {
            authorization_keys[level] = key;
            application_layer.A_Key_Write_res(Ack_Request::dont_care, priority, hop_count_type, asap, level, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        } else {
            // rejection value
            application_layer.A_Key_Write_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0xFF, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        }

        A_Key_Write_ind_initiator();
    });
}

void TP0_BCU2::A_Open_Routing_Table_ind_initiator()
{
    application_layer.A_Open_Routing_Table_ind([this](const Priority /*priority*/, const Hop_Count_Type /*hop_count_type*/, const ASAP_Connected /*asap*/) -> void {

        A_Open_Routing_Table_ind_initiator();
    });
}

void TP0_BCU2::A_Read_Routing_Table_ind_initiator()
{
    application_layer.A_Read_Routing_Table_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address) -> void {
        if (count <= 10) {
            for (auto & mem : routing_table) {
                if ((mem.first <= address) && (mem.first + mem.second.size() >= address + count)) {
                    //                    Level memory_level = authorization_keys.size() - 1;
                    //                    if (memory_min_read_level.count(mem.first)) {
                    //                        memory_level = memory_min_read_level[mem.first];
                    //                    }
                    //                    if (memory_level >= current_level) {
                    std::vector<uint8_t> data;
                    data.assign(std::cbegin(mem.second) + (address - mem.first), std::cbegin(mem.second) + (address - mem.first) + count);
                    application_layer.A_Read_Routing_Table_res(Ack_Request::dont_care, priority, hop_count_type, asap, count, address, data, [](const KNX::Status a_status) -> void {
                        // do nothing
                    });
                    A_Read_Routing_Table_ind_initiator();
                    return;
                    //                    }
                }
            }
        }

        // illegal length, (partly) protected memory
        application_layer.A_Read_Routing_Table_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0, address, {}, [](const KNX::Status a_status) -> void {
            // do nothing
        });

        A_Read_Routing_Table_ind_initiator();
    });
}

void TP0_BCU2::A_Write_Routing_Table_ind_initiator()
{
    application_layer.A_Write_Routing_Table_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data) -> void {
        if ((count == data.size()) && (count <= 10)) {
            for (auto & mem : routing_table) {
                if ((mem.first <= address) && (mem.first + mem.second.size() >= address + count)) {
                    std::copy(std::cbegin(data), std::cend(data), std::begin(mem.second) + (address - mem.first));
                    if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
                        application_layer.A_Read_Routing_Table_res(Ack_Request::dont_care, priority, hop_count_type, asap, count, address, data, [](const KNX::Status a_status) -> void {
                            // do nothing
                        });
                    }
                    A_Write_Routing_Table_ind_initiator();
                    return;
                }
            }
        }

        // illegal length, (partly) protected memory
        if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
            application_layer.A_Read_Routing_Table_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0, address, {}, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        }

        A_Write_Routing_Table_ind_initiator();
    });
}

void TP0_BCU2::A_Read_Router_Memory_ind_initiator()
{
    application_layer.A_Read_Router_Memory_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address) -> void {
        if (count <= 10) {
            for (auto & mem : router_memory) {
                if ((mem.first <= address) && (mem.first + mem.second.size() >= address + count)) {
                    //                    Level memory_level = authorization_keys.size() - 1;
                    //                    if (memory_min_read_level.count(mem.first)) {
                    //                        memory_level = memory_min_read_level[mem.first];
                    //                    }
                    //                    if (memory_level >= current_level) {
                    std::vector<uint8_t> data;
                    data.assign(std::cbegin(mem.second) + (address - mem.first), std::cbegin(mem.second) + (address - mem.first) + count);
                    application_layer.A_Read_Router_Memory_res(Ack_Request::dont_care, priority, hop_count_type, asap, count, address, data, [](const KNX::Status a_status) -> void {
                        // do nothing
                    });
                    A_Read_Router_Memory_ind_initiator();
                    return;
                    //                    }
                }
            }
        }

        // illegal length, (partly) protected memory
        application_layer.A_Read_Router_Memory_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0, address, {}, [](const KNX::Status a_status) -> void {
            // do nothing
        });

        A_Read_Router_Memory_ind_initiator();
    });
}

void TP0_BCU2::A_Write_Router_Memory_ind_initiator()
{
    application_layer.A_Write_Router_Memory_ind([this](const Priority priority, const Hop_Count_Type hop_count_type, const ASAP_Connected asap, const uint8_t count, const uint16_t address, const std::vector<uint8_t> data) -> void {
        if ((count == data.size()) && (count <= 10)) {
            for (auto & mem : router_memory) {
                if ((mem.first <= address) && (mem.first + mem.second.size() >= address + count)) {
                    std::copy(std::cbegin(data), std::cend(data), std::begin(mem.second) + (address - mem.first));
                    if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
                        application_layer.A_Read_Router_Memory_res(Ack_Request::dont_care, priority, hop_count_type, asap, count, address, data, [](const KNX::Status a_status) -> void {
                            // do nothing
                        });
                    }
                    A_Write_Router_Memory_ind_initiator();
                    return;
                }
            }
        }

        // illegal length, (partly) protected memory
        if (interface_objects->device()->device_control()->device_control.verify_mode_on()) {
            application_layer.A_Read_Router_Memory_res(Ack_Request::dont_care, priority, hop_count_type, asap, 0, address, {}, [](const KNX::Status a_status) -> void {
                // do nothing
            });
        }

        A_Write_Router_Memory_ind_initiator();
    });
}

}
