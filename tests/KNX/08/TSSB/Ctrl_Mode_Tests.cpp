// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/09/04/01/TP1_BCU2.h>

class Ctrl_Mode_Tests : public ::testing::Test
{
    virtual ~Ctrl_Mode_Tests() = default;
};

/** BCU that can be used as Bus Device Under Test (BDUT) */
class BCU :
    public KNX::TP1_BCU2
{
public:
    explicit BCU(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        KNX::TP1_BCU2(io_context, tp1_bus_simulation) {
        application_layer.A_Restart_ind(std::bind(&BCU::local_A_Restart_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4, std::placeholders::_5));
    }

    /** counter of received A_Restart_ind */
    uint8_t A_Restart_ind_count{};

protected:
    void local_A_Restart_ind(const KNX::Restart_Erase_Code /*erase_code*/, const KNX::Restart_Channel_Number /*channel_number*/, const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/) {
        ++A_Restart_ind_count;
    }
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class BCU_Plain_Data
{
public:
    explicit BCU_Plain_Data(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context)
    {
        tp1_physical_layer.connect(tp1_bus_simulation);

        tp1_data_link_layer.group_address_table = group_address_table;
        tp1_data_link_layer.L_Busmon_ind = std::bind(&BCU_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/* 2 Test of (reduced) System Interface Objects */

/* 2.1 Introduction */

/* 2.1.1 Availability of Interface Object and respective properties */

/* 2.1.1.1 General */

/**
 * Device Object – Property type
 *
 * @ingroup KNX_08_TSSB_02_01_01_02
 */
TEST(Ctrl_Mode_Tests, Test_2_1_1_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Step 1: Check if BDUT sends property description with correct data

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=1; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x01, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=1; PropIndex=don’t care; property data type=PDT_Unsigned_Int (04h); max. nr. of elements=1; write =disabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Step 2: Check if BDUT sends property value with correct data.

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=1; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=1; Count =1; Start=001; Property Data=0=Object Type ID for "Device Object")
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x00, 0x01, 0x10, 0x01, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Device Object – Serial number
 *
 * @ingroup KNX_08_TSSB_02_01_01_03
 */
TEST(Ctrl_Mode_Tests, Test_2_1_1_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Step 1: Check if BDUT sends property description with correct data

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=0; PropID=11d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x00, 0x0B, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=11d; PropIndex=don’t care; property data type=PDT_Generic_06 (16h or 96h); max. nr. of elements=1; write =en/disabled)
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=0; PropID=1; PropIndex=don’t care; property data type=PDT_Unsigned_Int (04h); max. nr. of elements=1; write =disabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x00, 0x0B, 0x00, 0x16, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Step 2: Check if BDUT sends property value with correct data.

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=0; PropID=11d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x00, 0x0B, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): Test frame (OUT): A_PropertyValue_Response (ObjIndex=0; PropID=11d; Count =1; Start=001; Property Data=BDUT ́s Serial Number)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x6B, 0x03, 0xD6, 0x00, 0x0B, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Address Table, Association Table, Application Object – Property Type
 *
 * @ingroup KNX_08_TSSB_02_01_01_04
 */
TEST(Ctrl_Mode_Tests, Test_2_1_1_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // The test sequence as described in clause 2.1.1.2 applies, with adapted local Object Index and Object Type

    // ID=1 (Object Type ID for "Address Table Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x01, 0x01, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x01, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x01, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x01, 0x01, 0x10, 0x01, 0x00, 0x01, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ID=2 (Object Type ID for "Association Table Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x02, 0x01, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x02, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x02, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x02, 0x01, 0x10, 0x01, 0x00, 0x02, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ID=3 (Object Type ID for "Application Program Object")
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x03, 0x01, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x03, 0x01, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x01, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x03, 0x01, 0x10, 0x01, 0x00, 0x03, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Address Table, Association Table, Application Object – Load Control
 *
 * @ingroup KNX_08_TSSB_02_01_01_05
 */
TEST(Ctrl_Mode_Tests, Test_2_1_1_5)
{
    // Test Suite Supplement G
}

/**
 * Address Table, Association Table, Application Object – Table Pointer
 *
 * @ingroup KNX_08_TSSB_02_01_01_06
 */
TEST(Ctrl_Mode_Tests, Test_2_1_1_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    (void) bdut.interface_objects->group_address_table()->table_reference();
    (void) bdut.interface_objects->group_object_association_table()->table_reference();
    (void) bdut.interface_objects->application_program()->table_reference();

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // ID=1 (Object Type ID for "Address Table Object")

    // Step 1: Check if BDUT sends property description with correct data

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=x; PropID=7d; PropIndex=0) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x01, 0x07, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=x; PropID=7d; PropIndex=don’t care; property data type=PDT_UNSIGNED_INT (04h); max. nr. of elements=1; write =disabled) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x01, 0x07, 0x00, 0x09, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response // @note type changed to PDT_UNSIGNED_LONG (0x09)
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Step 2: Check if BDUT sends property value with correct data.

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=x; PropID=7d; Count =1; Start=001) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x01, 0x07, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=x; PropID=7d; Count =1; Start=001; Property Data=any address) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x69, 0x03, 0xD6, 0x01, 0x07, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ID=2 (Object Type ID for "Association Table Object")

    // Step 1: Check if BDUT sends property description with correct data

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=x; PropID=7d; PropIndex=0) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x02, 0x07, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=x; PropID=7d; PropIndex=don’t care; property data type=PDT_UNSIGNED_INT (04h); max. nr. of elements=1; write =disabled) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x02, 0x07, 0x00, 0x09, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response // @note type changed to PDT_UNSIGNED_LONG (0x09)
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Step 2: Check if BDUT sends property value with correct data.

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=x; PropID=7d; Count =1; Start=001) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x02, 0x07, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=x; PropID=7d; Count =1; Start=001; Property Data=any address) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x69, 0x03, 0xD6, 0x02, 0x07, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ID=3 (Object Type ID for "Application Program Object")

    // Step 1: Check if BDUT sends property description with correct data

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=x; PropID=7d; PropIndex=0) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x03, 0x07, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=x; PropID=7d; PropIndex=don’t care; property data type=PDT_UNSIGNED_INT (04h); max. nr. of elements=1; write =disabled) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x03, 0x07, 0x00, 0x09, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response // @note type changed to PDT_UNSIGNED_LONG (0x09)
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Step 2: Check if BDUT sends property value with correct data.

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=x; PropID=7d; Count =1; Start=001) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x07, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=x; PropID=7d; Count =1; Start=001; Property Data=any address) whereby x = 1 for address table, 2 for association table, 3 for application program
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x69, 0x03, 0xD6, 0x03, 0x07, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Parameter Pointer
 *
 * @ingroup KNX_08_TSSB_02_01_01_07
 */
TEST(Ctrl_Mode_Tests, Test_2_1_1_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    (void) bdut.interface_objects->application_program()->parameter_reference();

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Step 1: Check if BDUT sends property description with correct data

    // Stimuli: send A_PropertyDescription_Read to BDUT
    // Test frame (IN): A_PropertyDescription_Read (ObjIndex=3; PropID=51d; PropIndex=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x64, 0x03, 0xD8, 0x03, 0x33, 0x00, 0 }; // A_PropertyDescription_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyDescription_Response with correct data
    // Test frame (OUT): A_PropertyDescription_Response (ObjIndex=3; PropID=51d; PropIndex=don’t care; property data type=PDT_Unsigned_Int (04h); max. nr. of elements=1; write =disabled)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x68, 0x03, 0xD9, 0x03, 0x33, 0x00, 0x04, 0x00, 0x01, 0xFF, 0 }; // A_PropertyDescription_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Step 2: Check if BDUT sends property value with correct data.

    // Stimuli: send A_PropertyValue_Read to BDUT
    // Test frame (IN): A_PropertyValue_Read (ObjIndex=3; PropID=51d; Count =1; Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x11, 0x01, 0x65, 0x03, 0xD5, 0x03, 0x33, 0x10, 0x01, 0 }; // A_PropertyValue_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Acceptance: BDUT sends A_PropertyValue_Response with correct data
    // Test frame (OUT): A_PropertyValue_Response (ObjIndex=3; PropID=51d; Count =1; Start=001; Property Data=any address)
    tp1_data = { 0xBC, 0x11, 0x01, 0xAF, 0xFE, 0x67, 0x03, 0xD6, 0x03, 0x33, 0x10, 0x01, 0x00, 0x00, 0 }; // A_PropertyValue_Response
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 3 Test of Localisation Flag */

/* 4 Test of Localisation Channels - Server Test */

/**
 * Test of sensor channel
 *
 * @ingroup KNX_08_TSSB_04_01
 */
TEST(Ctrl_Mode_Tests, Test_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    std::function<void(void)> A_Memory_Write_ind_initiator;
    A_Memory_Write_ind_initiator = [&]() -> void {
        bdut.application_layer.A_Memory_Write_ind([&bdut, &A_Memory_Write_ind_initiator](const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Connected /*asap*/, const KNX::Memory_Number number, const KNX::Memory_Address memory_address, const KNX::Memory_Data data) {
            /* Address Table length (IA + GAs) */
            if ((memory_address == 0x0116) && (number == 1)) {
                // do nothing
            } else
            /* Address Table */
            if ((memory_address == 0x0119) && (number == 2)) {
                bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address((data[0] << 8) | (data[1]))); // TSAP 1
                bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address((data[2] << 8) | (data[3]))); // TSAP 2
            } else
            /* Association Table length */
            if ((memory_address == 0x0151) && (number == 1)) {
                // do nothing
            } else
            /* Association Table */
            if ((memory_address == 0x152) && (number == 2)) {
                bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(data[0]), KNX::ASAP_Group(data[1]));
                bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(data[2]), KNX::ASAP_Group(data[3]));
            }

            A_Memory_Write_ind_initiator();
        });
    };
    A_Memory_Write_ind_initiator();
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x0202);
    // Localization_State
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].config.transmit_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].config.communication_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].type = KNX::Group_Object_Type::Octet_1;
    // Channel_Activation
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].config.transmit_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].config.communication_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].type = KNX::Group_Object_Type::Octet_1;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xFFFF);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

#if 0
    // Case a) Easy Ctrl-Mode-Device "Fixed-DMA" AND Mask 0012h

    // Set Runtime Error Flags

    // DMP-MemWrite_Rco(010Dh, 010Dh, 00h)

#else
    // Case b) Easy Ctrl-Mode-Device "Relocatable-DMA" and Mask 0020h, 0021h

    // Set Group Address Table "Loading"
    // DMP_LoadStateMachineWrite_Rco_IO(object_index = 01h, PID==PID_LOAD_STATE_CONTROL, start_index = 1, nr_of_elem = 1, Load)
    bdut.interface_objects->group_address_table()->load_state_machine->state = KNX::Load_State_Machine::State::Loading;

    // IN BC FFFF 0202 60 80 :T-Connect(Addr=0202)
    tp1_data = { 0xBC, 0xFF, 0xFF, 0x02, 0x02, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC FFFF 0202 67 42 82 01 19 C1 00 C1 01 :MemoryWrite(Count=02, Addr=0119, Data=C1 00 C1 01 )
    tp1_data = { 0xBC, 0xFF, 0xFF, 0x02, 0x02, 0x67, 0x42, 0x82, 0x01, 0x19, 0xC1, 0x00, 0xC1, 0x01, 0 }; // A_Memory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(1)), 0xC100); // TSAP 1 -> 0xC100
    ASSERT_EQ(bdut.interface_objects->group_address_table()->group_address(KNX::TSAP_Group(2)), 0xC101); // TSAP 2 -> 0xC101

    // OUT B0 0202 FFFF 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x02, 0x02, 0xFF, 0xFF, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC FFFF 0202 64 46 81 01 16 03 :MemoryWrite(Count=01, Addr=0116, Data=03 )
    tp1_data = { 0xBC, 0xFF, 0xFF, 0x02, 0x02, 0x64, 0x46, 0x81, 0x01, 0x16, 0x03, 0 }; // A_Memory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 0202 FFFF 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x02, 0x02, 0xFF, 0xFF, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC FFFF 0202 67 4A 82 01 52 01 07 02 08 :MemoryWrite(Count=02, Addr=0152, Data=01 07 02 08 )
    tp1_data = { 0xBC, 0xFF, 0xFF, 0x02, 0x02, 0x67, 0x4A, 0x82, 0x01, 0x52, 0x01, 0x07, 0x02, 0x08, 0 }; // A_Memory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(1)), 7); // TSAP 1 -> ASAP 7
    ASSERT_EQ(bdut.interface_objects->group_object_association_table()->asap(KNX::TSAP_Group(2)), 8); // TSAP 2 -> ASAP 8

    // OUT B0 0202 FFFF 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x02, 0x02, 0xFF, 0xFF, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC FFFF 0202 64 4E 81 01 51 02 :MemoryWrite(Count=01, Addr=0151, Data=02 )
    tp1_data = { 0xBC, 0xFF, 0xFF, 0x02, 0x02, 0x64, 0x4E, 0x81, 0x01, 0x51, 0x02, 0 }; // A_Memory_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 0202 FFFF 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x02, 0x02, 0xFF, 0xFF, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN BC FFFF 0202 60 81 :T-Disconnect
    tp1_data = { 0xBC, 0xFF, 0xFF, 0x02, 0x02, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
#endif

    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    // Sensor_Channel
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xC800));
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(3), KNX::ASAP_Group(0)); // TSAP 3 -> ASAP 0
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.transmit_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.communication_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Octet_1;
    bcu.group_address_table->individual_address = KNX::Individual_Address(0x1041);

#if 0
    // Case a) Easy Ctrl-Mode-Device "Fixed-DMA" AND Mask 0012h

    // Clear Runtime Error Flags

    // DMP-MemWrite_Rco(010Dh, 010Dh, FFh)

#else
    // Case b) Easy Ctrl-Mode-Device "Relocatable-DMA" and Mask 0020h, 0021h

    // Set Group Address Table "Loaded"
    // DMP_LoadStateMachineWrite_Rco_IO(object_index = 01h, PID==PID_LOAD_STATE_CONTROL, start_index = 1, nr_of_elem = 1, LoadCompleted)
    bdut.interface_objects->group_address_table()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Stimulate the controller to send a group telegram on the Datapoint Localization_State with the value "1" (Localization State ON).
    // IN BC 1041 C100 E1 00 81
    tp1_data = { 0xBC, 0x10, 0x41, 0xC1, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].data->at(0), 0x01);

    // stimulate the respective sensor channel
    ASSERT_EQ(bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].config.communication_enable, true);
    ASSERT_EQ(bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].config.transmit_enable, true);
    bdut.application_interface_layer.group_object_server.U_Value_Write_req(KNX::Group_Object_Number(2), {true, false, false, true}, KNX::Group_Object_Communication_Flags(0x03), {0x02});
    ASSERT_EQ(bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].data->at(0), 0x02);
    ASSERT_EQ(bdut.interface_objects->group_object_table()->communication_flags[KNX::ASAP_Group(8)].data_request, false);
    ASSERT_EQ(bdut.interface_objects->group_object_table()->communication_flags[KNX::ASAP_Group(8)].transmission_status, KNX::Group_Object_Communication_Flags::Transmission_Status::transmitting);
    //bdut.application_layer.A_GroupValue_Write_req(KNX::Ack_Request::dont_care, 8, KNX::Priority::low, KNX::Network_Layer_Parameter, {0x02});
    io_context.poll();

    // Check whether the Datapoint Channel_Activation sends a telegram, of which the data width
    // corresponds to the number of device channels (1 byte for up to 8 channels, 2 byte for up to
    // 16 channels and 3 byte for up to 24 channels). Check whether the correct bit position is set
    // depending on the activated channel and whether the value is correct.
    // OUT BC 1101 C101 E2 00 80 02
    tp1_data = { 0xBC, 0x11, 0x01, 0xC1, 0x01, 0xE1, 0x00, 0x82, 0 }; // A_GroupValue_Write // @note changed from 0xE2, 0x00, 0x80, 0x02 to 0xE1, 0x00, 0x82
    ASSERT_EQ(*log++, tp1_data);
    // No L_Ack

    // Stimulate the controller to end the Localization state by sending a group telegram on the
    // Datapoint Localization_State with the value "0" (Localization State off) and subsequently
    // link the device.
    // IN BC 1041 C100 E1 00 80
    tp1_data = { 0xBC, 0x10, 0x41, 0xC1, 0x00, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].data->at(0), 0x00);

    // activate the respective sensor channel
    bdut.application_interface_layer.group_object_server.U_Value_Write_req(KNX::Group_Object_Number(0), {true, false, false, true}, KNX::Group_Object_Communication_Flags(0x03), {0x01});
    //bdut.application_layer.A_GroupValue_Write_req(KNX::Ack_Request::dont_care, 0, KNX::Priority::low, KNX::Network_Layer_Parameter, {0x01});
    io_context.poll();

    // Check whether a telegram is sent on a non-localization group address.
    // OUT BC 1101 C800 E1 00 81
    tp1_data = { 0xBC, 0x11, 0x01, 0xC8, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(log, std::cend(busmoni.log)); // No L_Ack
#endif
}

/**
 * Test of actuator channel
 *
 * @ingroup KNX_08_TSSB_04_02
 */
TEST(Ctrl_Mode_Tests, Test_4_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1101);
    // Localization_State
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xC200));
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(1), KNX::ASAP_Group(7)); // TSAP 1 -> ASAP 7
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].config.write_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].config.communication_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].type = KNX::Group_Object_Type::Octet_1;
    // Channel_Activation
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xC201));
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(2), KNX::ASAP_Group(8)); // TSAP 2 -> ASAP 8
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].config.write_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].config.communication_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].type = KNX::Group_Object_Type::Octet_1;
    // sensor channel
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::Group_Address(0xC900));
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(3), KNX::ASAP_Group(0)); // TSAP 3 -> ASAP 0
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].data = std::make_shared<std::vector<uint8_t>>(1);
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.write_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.communication_enable = true;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].config.transmission_priority = KNX::Priority::low;
    bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].type = KNX::Group_Object_Type::Octet_1;

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = 0x1041;

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Identify the actuator channel in the BDUT (check via supplied documentation)

    // stimulate the controller to attribute group addresses for the two localization Datapoints
    // (Localization_State and Channel_Activation)
    // See step 2 in clause 4.1.

    // stimulate the controller to send a group telegram on the Datapoint Localization_State with
    // the value "1" (Localization State ON).
    // IN BC 1041 C200 E1 00 81
    tp1_data = { 0xBC, 0x10, 0x41, 0xC2, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].data->at(0), 0x01);

    // generate according to PIXIT a frame with the appropriate data width (depending on the
    // number of channels, see above), correct bit position (according to the channel to be localized)
    // and value "1" (Localization ON).
    // IN BC 1041 C201 E2 00 80 04
    tp1_data = { 0xBC, 0x10, 0x41, 0xC2, 0x01, 0xE2, 0x00, 0x80, 0x04, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(8)].data->at(0), 0x04);

    // Check reaction of the actuator channel
    // @todo all reactions in this test requires actuator implementation

    // stimulate the controller to end the Localization state by sending a group telegram on the
    // Datapoint Localization_State with the value "0" (Localization State off) and subsequently
    // link the device.
    // IN BC 1041 C200 E1 00 80
    tp1_data = { 0xBC, 0x10, 0x41, 0xC2, 0x00, 0xE1, 0x00, 0x80, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(7)].data->at(0), 0x00);

    // send group frame with the now attributed group address to the respective actuator channel
    // and check reaction.
    // IN BC 1041 C900 E1 00 81
    tp1_data = { 0xBC, 0x10, 0x41, 0xC9, 0x00, 0xE1, 0x00, 0x81, 0 }; // A_GroupValue_Write
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    ASSERT_EQ(bdut.interface_objects->group_object_table()->group_object_descriptors[KNX::ASAP_Group(0)].data->at(0), 0x01);
}

/* 5 Test of Device Descriptor Type 0 support by client */

// @todo Ctrl_Mode_Tests 5

/* 6 Group Address Check Test set-up for Ctrl-Mode */

/**
 * Group Address Check Test set-up for Ctrl-Mode
 *
 * @ingroup KNX_08_TSSB_06
 */
TEST(Ctrl_Mode_Tests, Test_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x02FF);
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::TSAP_Group(0xC234));
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::TSAP_Group(0xC235));
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::TSAP_Group(0xC236));
    bdut.interface_objects->group_address_table()->group_addresses.insert(KNX::TSAP_Group(0xC237));
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(0x01), KNX::ASAP_Group(0x00));
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(0x03), KNX::ASAP_Group(0x01));
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(0x03), KNX::ASAP_Group(0x02));
    bdut.interface_objects->group_object_association_table()->add_entry(KNX::TSAP_Group(0x02), KNX::ASAP_Group(0x00));
    (void) bdut.interface_objects->application_program()->parameter_reference();

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0x0002);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // CASE A - out of range - no answer

    // (1) IN start BC 00.00.002 00/0000 E7 03 DA 00 01 17 01 C2 38 :NetworkParameterRead(ObjType=0001, PID=17, TestInfo=01 C2 38 )
    tp1_data = { 0xBC, 0x00, 0x02, 0x00, 0x00, 0xE7, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x01, 0xC2, 0x38, 0 }; // A_NetworkParameter_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // CASE B: one group address in range

    // (2) IN 00:00:02.0 BC 00.00.002 00/0000 E7 03 DA 00 01 17 05 C2 30 :NetworkParameterRead(ObjType=0001, PID=17, TestInfo=05 C2 30 )
    tp1_data = { 0xBC, 0x00, 0x02, 0x00, 0x00, 0xE7, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x05, 0xC2, 0x30, 0 }; // A_NetworkParameter_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // (3) OUT 00:00:00.0 BC 00.02.255 00.00.002 67 03 DB 00 01 17 05 C2 30 :NetworkParameterResponse(ObjType=0001, PID=17, TestData=05 C2 30 )
    tp1_data = { 0xBC, 0x02, 0xFF, 0x00, 0x00, 0xE7, 0x03, 0xDB, 0x00, 0x01, 0x17, 0x05, 0xC2, 0x30, 0 }; // A_NetworkParameter_Response // @note dst changed from 0x00 0x02 to 0x00 0x00. ctr changed from 0x67 to 0xE7
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // CASE C: 1 group address to more than one object

    // (4) IN 00:00:02.0 BC 00.00.002 00/0000 E7 03 DA 00 01 17 01 C2 36 :NetworkParameterRead(ObjType=0001, PID=17, TestInfo=01 C2 36 )
    tp1_data = { 0xBC, 0x00, 0x02, 0x00, 0x00, 0xE7, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x01, 0xC2, 0x36, 0 }; // A_NetworkParameter_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // (5) OUT 00:00:00.0 BC 00.02.255 00.00.002 67 03 DB 00 01 17 01 C2 36 :NetworkParameterResponse(ObjType=0001, PID=17, TestData=01 C2 36 )
    tp1_data = { 0xBC, 0x02, 0xFF, 0x00, 0x00, 0xE7, 0x03, 0xDB, 0x00, 0x01, 0x17, 0x01, 0xC2, 0x36, 0 }; // A_NetworkParameter_Response // @note dst changed from 0x00 0x02 to 0x00 0x00. ctr changed from 0x67 to 0xE7
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // CASE D: two group addresses to one Group Object

    // (6) IN 00:00:02.0 BC 00.00.002 00/0000 E7 03 DA 00 01 17 02 C2 34 :NetworkParameterRead(ObjType=0001, PID=17, TestInfo=02 C2 34 )
    tp1_data = { 0xBC, 0x00, 0x02, 0x00, 0x00, 0xE7, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x02, 0xC2, 0x34, 0 }; // A_NetworkParameter_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // (7) OUT 00:00:00.0 BC 00.02.255 00.00.002 67 03 DB 00 01 17 02 C2 34 :NetworkParameterResponse(ObjType=0001, PID=17, TestData=02 C2 34 )
    tp1_data = { 0xBC, 0x02, 0xFF, 0x00, 0x00, 0xE7, 0x03, 0xDB, 0x00, 0x01, 0x17, 0x02, 0xC2, 0x34, 0 }; // A_NetworkParameter_Response // @note dst changed from 0x00 0x02 to 0x00 0x00. ctr changed from 0x67 to 0xE7
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // CASE E: in range but not linked

    // (8) IN 00:00:02.0 BC 00.00.002 00/0000 E7 03 DA 00 01 17 01 C2 37 :NetworkParameterRead(ObjType=0001, PID=17, TestInfo=01 C2 37 )
    tp1_data = { 0xBC, 0x00, 0x02, 0x00, 0x00, 0xE7, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x01, 0xC2, 0x37, 0 }; // A_NetworkParameter_Read
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // (9) OUT 00:00:00.0 BC 00.02.255 00.00.002 67 03 DB 00 01 17 01 C2 37 :NetworkParameterResponse(ObjType=0001, PID=17, TestData=01 C2 37 )
    tp1_data = { 0xBC, 0x02, 0xFF, 0x00, 0x00, 0xE7, 0x03, 0xDB, 0x00, 0x01, 0x17, 0x01, 0xC2, 0x37, 0 }; // A_NetworkParameter_Response // @note dst changed from 0x00 0x02 to 0x00 0x00. ctr changed from 0x67 to 0xE7
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // CASE F: multicast with supported GA - no answer

    // (10) IN 00:00:02.0 BC 00.00.002 24/0566 E7 03 DA 00 01 17 01 C2 36 :Faulty multicast
    // @todo test segfaults
    // tp1_data = { 0xBC, 0x00, 0x02, 0xC2, 0x36, 0xE7, 0x03, 0xDA, 0x00, 0x01, 0x17, 0x01, 0xC2, 0x36, 0 }; // A_NetworkParameter_Read
    // bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    // io_context.poll();
    // ASSERT_EQ(*log++, tp1_data);
    // ASSERT_EQ(log, std::cend(busmoni.log)); // No L_Ack
}

/* 7 Test of Address Table */

/* 8 Test of structure of Association Table */

/* 9 Test of Object Table (Easy) */

// @todo Ctrl_Mode_Tests 9

/* 10 Programming of devices */

// @todo Ctrl_Mode_Tests 10

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
