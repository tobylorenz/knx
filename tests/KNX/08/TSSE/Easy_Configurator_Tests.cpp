// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

class Easy_Configurator_Tests : public ::testing::Test
{
    virtual ~Easy_Configurator_Tests() = default;
};

// @todo Easy_Configurator_Tests

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
