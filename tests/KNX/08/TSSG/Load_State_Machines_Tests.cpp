// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/09/04/01/TP1_BCU2.h>

class Load_State_Machines_Tests : public ::testing::Test
{
    virtual ~Load_State_Machines_Tests() = default;
};

/** BCU that can be used as Bus Device Under Test (BDUT) */
class BCU :
    public KNX::TP1_BCU2
{
public:
    explicit BCU(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        KNX::TP1_BCU2(io_context, tp1_bus_simulation) {
        interface_objects->group_address_table()->individual_address = 0x1001;
        interface_objects->group_address_table()->load_control()->description.access.write_level = 2;
        interface_objects->group_object_association_table()->load_control()->description.access.write_level = 2;
        interface_objects->application_program()->load_control()->description.access.write_level = 2;

        authorization_keys[0] = { 0xAA, 0xAA, 0xAA, 0xAA };

        memory[0x4030] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

        A_Restart_ind_initiator();
    }

protected:
    void A_Restart_ind_initiator() {
        application_layer.A_Restart_ind([this](const KNX::Restart_Erase_Code /*erase_code*/, const KNX::Restart_Channel_Number /*channel_number*/, const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/) -> void {
            interface_objects->group_address_table()->load_state_machine->device_restart();
            interface_objects->group_object_association_table()->load_state_machine->device_restart();
            interface_objects->application_program()->load_state_machine->device_restart();

            A_Restart_ind_initiator();
        });
    }
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class BCU_Plain_Data
{
public:
    explicit BCU_Plain_Data(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context)
    {
        tp1_physical_layer.connect(tp1_bus_simulation);

        tp1_data_link_layer.group_address_table = group_address_table;
        tp1_data_link_layer.L_Busmon_ind = std::bind(&BCU_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/* 2 Testing of Load Controls */

/* 2.1 Test Preparation */

/* 2.2 Tests with initial state LOAD_STATE_UNLOADED */

/**
 * Event: NO OPERATION and unknown Load event
 *
 * @ingroup KNX_08_TSSG_02_02_01
 */
TEST(Load_State_Machines_Tests, Test_2_2_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is unloaded

    // Send to association table object a LOAD_EVENT_NO OPERATION
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 00 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=00 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object an unknown LOAD_EVENT
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 05 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=05 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘25h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: LOAD_EVENT_START LOADING
 *
 * @ingroup KNX_08_TSSG_02_02_02
 */
TEST(Load_State_Machines_Tests, Test_2_2_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is unloaded

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect}
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: LOAD_EVENT_LOAD COMPLETED
 *
 * @ingroup KNX_08_TSSG_02_02_03
 */
TEST(Load_State_Machines_Tests, Test_2_2_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is unloaded

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT remains in load state LOAD_STATE_UNLOADED, alternatively ERROR
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 – alternatively 03)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h or alternatively 03h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: LOAD_SEGMENT
 *
 * @ingroup KNX_08_TSSG_02_02_04
 */
TEST(Load_State_Machines_Tests, Test_2_2_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is unloaded

    // Send to association table object a LOAD_SEGMENT
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 03 00 01 1A 00 7A 33 03 80 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 00 01 1A 00 7A 33 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x00, 0x01, 0x1A, 0x00, 0x7A, 0x33, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘23h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT remains in load state LOAD_STATE_UNLOADED, alternatively ERROR
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00, alternatively 03)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h, alternatively 03h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: UNLOAD
 *
 * @ingroup KNX_08_TSSG_02_02_05
 */
TEST(Load_State_Machines_Tests, Test_2_2_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is unloaded

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: DEVICE RESTART
 *
 * @ingroup KNX_08_TSSG_02_02_06
 */
TEST(Load_State_Machines_Tests, Test_2_2_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is unloaded

    // Send a device restart to BDUT
    // IN BC AFFE 1001 61 4B 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x4B, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // T-ACK is optional. It is depending on the device architecture.

    // ---> Acceptance: Connection breaks down, load state remains UNLOADED
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read load state of association table
    // IN BC AFFE 1001 65 43 D5 02 05 10 01 :PropertyValueRead(Obj=02, Prop=05, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x02, 0x05, 0x10, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 43 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 2.3 Tests with initial state LOAD_STATE_LOADED */

/**
 * Event: NO OPERATION and unknown Load event
 *
 * @ingroup KNX_08_TSSG_02_03_01
 */
TEST(Load_State_Machines_Tests, Test_2_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 }; // SeqNo=2
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Allocation record)
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 03 00 40 30 00 10 31 03 80 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 00 40 30 00 10 31 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ’23 00 00 42 00 00 10 ff 03 80 00 00h’ to address 0104h
    // Start address 4200h
    // Length 0010h
    // Read/Write Access: ffh no privilege
    // MemoryType 03h EEPROM
    // Memory Attributes 80h apply checksum]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Write data to association table
    // IN BC AFFE 1001 6A 52 87 40 30 03 01 00 02 01 03 02 :MemoryWrite(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6A, 0x52, 0x87, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Verify data
    // IN BC AFFE 1001 63 56 07 40 30 :MemoryRead(Count=07, Addr=4030)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x56, 0x07, 0x40, 0x30, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 AFFE 6A 52 47 40 30 03 01 00 02 01 03 02 :MemoryResponse(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6A, 0x52, 0x47, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 5B D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h
    // Contents manufacturer dependant
    // Pointer to table 4200h
    // PEI Type 80h
    // Manufacturer ID 002h
    // Device ID 0A4Ah]

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 5F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 5B D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loaded

    // Send to association table object a LOAD_EVENT_NO OPERATION
    // IN BC AFFE 1001 6F 63 D7 02 05 10 01 00 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=00 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘20h’ to address 0104h]

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 5F D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object an unknown LOAD_EVENT
    // IN BC AFFE 1001 6F 67 D7 02 05 10 01 00 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=05 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x67, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘25h’ to address 0104h]

    // OUT B0 1001 AFFE 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 63 D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x63, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: START LOADING
 *
 * @ingroup KNX_08_TSSG_02_03_02
 */
TEST(Load_State_Machines_Tests, Test_2_3_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Allocation record)
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 03 00 40 30 00 10 31 03 80 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 00 40 30 00 10 31 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0x30, 0x00, 0x10, 0x31, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 00 00 42 00 00 10 ff 03 80 00 00h’ to address 0104h – significance of bytes see 2.3.1]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Write data to association table
    // IN BC AFFE 1001 6A 52 87 40 30 03 01 00 02 01 03 02 :MemoryWrite(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6A, 0x52, 0x87, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Verify data
    // IN BC AFFE 1001 63 56 07 40 30 :MemoryRead(Count=07, Addr=4030)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x56, 0x07, 0x40, 0x30, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 AFFE 6A 52 47 40 30 03 01 00 02 01 03 02 :MemoryResponse(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6A, 0x52, 0x47, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 5B D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – significance of bytes see 2.3.1 ]

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 5F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 5B D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loaded

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 63 D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: LOAD COMPLETED
 *
 * @ingroup KNX_08_TSSG_02_03_03
 */
TEST(Load_State_Machines_Tests, Test_2_3_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Allocation record)
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 03 00 40 30 00 10 31 03 80 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 00 40 30 00 10 31 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0x30, 0x00, 0x10, 0x31, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 00 00 42 00 00 10 ff 03 80 00 00h’ to address 0104h – for significance see 2.3.1]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Write data to association table
    // IN BC AFFE 1001 6A 52 87 40 30 03 01 00 02 01 03 02 :MemoryWrite(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6A, 0x52, 0x87, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Verify data
    // IN BC AFFE 1001 63 56 07 40 30 :MemoryRead(Count=07, Addr=4030)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x56, 0x07, 0x40, 0x30, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 AFFE 6A 52 47 40 30 03 01 00 02 01 03 02 :MemoryResponse(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6A, 0x52, 0x47, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 5B D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – for significance see 2.3.1]

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 5F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 5B D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loaded

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 63 D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT remains in load state LOAD_STATE_LOADED, alternatively ERROR
    // OUT BC 1001 AFFE 66 5F D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h, alternatively 03h]

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: LOAD_SEGMENT
 *
 * @ingroup KNX_08_TSSG_02_03_04
 */
TEST(Load_State_Machines_Tests, Test_2_3_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Allocation record)
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 03 00 40 30 00 10 31 03 80 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 00 40 30 00 10 31 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0x30, 0x00, 0x10, 0x31, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 00 00 42 00 00 10 ff 03 80 00 00h’ to address 0104h – for significance see 2.3.1]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Write data to association table
    // IN BC AFFE 1001 6A 52 87 40 30 03 01 00 02 01 03 02 :MemoryWrite(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6A, 0x52, 0x87, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Verify data
    // IN BC AFFE 1001 63 56 07 40 30 :MemoryRead(Count=07, Addr=4030)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x56, 0x07, 0x40, 0x30, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 AFFE 6A 52 47 40 30 03 01 00 02 01 03 02 :MemoryResponse(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6A, 0x52, 0x47, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 5B D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – for significance see 2.3.1]

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 5F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 5B D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loaded

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC A001 1001 6F 63 D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – for significance see 2.3.1

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 5F D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: UNLOAD
 *
 * @ingroup KNX_08_TSSG_02_03_05
 */
TEST(Load_State_Machines_Tests, Test_2_3_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Allocation record)
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 03 00 40 30 00 10 31 03 80 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 00 40 30 00 10 31 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0x30, 0x00, 0x10, 0x31, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 00 00 42 00 00 10 ff 03 80 00h’ to address 0104h – for significance see 2.3.1

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Write data to association table
    // IN BC AFFE 1001 6A 52 87 40 30 03 01 00 02 01 03 02 :MemoryWrite(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6A, 0x52, 0x87, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Verify data
    // IN BC AFFE 1001 63 56 07 40 30 :MemoryRead(Count=07, Addr=4030)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x56, 0x07, 0x40, 0x30, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 AFFE 6A 52 47 40 30 03 01 00 02 01 03 02 :MemoryResponse(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6A, 0x52, 0x47, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 5B D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – for significance see 2.3.1

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 5F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 5B D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loaded

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC A001 1001 6F 63 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_UNLOADED 1
    // OUT BC 1001 AFFE 66 5F D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: DEVICE RESTART
 *
 * @ingroup KNX_08_TSSG_02_03_06
 */
TEST(Load_State_Machines_Tests, Test_2_3_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Allocation record)
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 03 00 40 30 00 10 31 03 80 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 00 40 30 00 10 31 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0x30, 0x00, 0x10, 0x31, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 00 00 42 00 00 10 ff 03 80 00h’ to address 0104h – for significance see 2.3.1

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Write data to association table
    // IN BC AFFE 1001 6A 52 87 40 30 03 01 00 02 01 03 02 :MemoryWrite(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6A, 0x52, 0x87, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Verify data
    // IN BC AFFE 1001 63 56 07 40 30 :MemoryRead(Count=07, Addr=4030)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x63, 0x56, 0x07, 0x40, 0x30, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 AFFE 6A 52 47 40 30 03 01 00 02 01 03 02 :MemoryResponse(Count=07, Addr=4030, Data=03 01 00 02 01 03 02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x6A, 0x52, 0x47, 0x40, 0x30, 0x03, 0x01, 0x00, 0x02, 0x01, 0x03, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 5B D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – for significance see 2.3.1

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 5F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 5B D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loaded

    // Send a device restart to BDUT
    // IN BC AFFE 1001 61 63 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x63, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // T-ACK is optional. It is depending on the device architecture.

    // ---> Acceptance: Connection breaks down, load state remains LOADED
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read load state of association table
    // IN BC AFFE 1001 65 43 D5 02 05 10 01 :PropertyValueRead(Obj=02, Prop=05, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x02, 0x05, 0x10, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 43 D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 2.4 Tests with initial state LOAD_STATE_LOADING */

/**
 * Event: NO OPERATION and unknown Load event
 *
 * @ingroup KNX_08_TSSG_02_04_01
 */
TEST(Load_State_Machines_Tests, Test_2_4_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loading

    // Send to association table object a LOAD_EVENT_NO OPERATION
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 00 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=00 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘20h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object an unknown LOAD_EVENT
    // IN BC AFFE 1001 6F 53 D7 02 05 10 01 05 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=05 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘25h’ to address 0104h]

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 53 D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: START LOADING
 *
 * @ingroup KNX_08_TSSG_02_04_02
 */
TEST(Load_State_Machines_Tests, Test_2_4_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loading

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: LOAD COMPLETED
 *
 * @ingroup KNX_08_TSSG_02_04_03
 */
TEST(Load_State_Machines_Tests, Test_2_4_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines

    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loading

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns theload state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: LOAD_SEGMENT
 *
 * @ingroup KNX_08_TSSG_02_04_04
 */
TEST(Load_State_Machines_Tests, Test_2_4_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loading

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – significance of bytes see 2.3.1 ]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: UNLOAD
 *
 * @ingroup KNX_08_TSSG_02_04_05
 */
TEST(Load_State_Machines_Tests, Test_2_4_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loading

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_UNLOADED 2
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: DEVICE RESTART
 *
 * @ingroup KNX_08_TSSG_02_04_06
 */
TEST(Load_State_Machines_Tests, Test_2_4_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is loading

    // Send a device restart to BDUT
    // IN BC AFFE 1001 61 4F 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x4F, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // T-ACK is optional. It is depending on the device architecture.

    // ---> Acceptance: Connection breaks down, load state remains in loading
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read load state of association table
    // IN BC AFFE 1001 65 43 D5 02 05 10 01 :PropertyValueRead(Obj=02, Prop=05, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x02, 0x05, 0x10, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING, optional ERROR
    // OUT BC 1001 AFFE 66 43 D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h, optional 03h]

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 2.5 Tests with initial state LOAD_STATE_ERROR */

/**
 * Event: NO OPERATION and unknown Load event
 *
 * @ingroup KNX_08_TSSG_02_05_01
 */
TEST(Load_State_Machines_Tests, Test_2_5_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 53 D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – significance of bytes see 2.3.1 ]

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 53 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is in state ERROR

    // Send to association table object a LOAD_EVENT_NO OPERATION
    // IN BC AFFE 1001 6F 57 D7 02 05 10 01 00 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=00 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘20h’ to address 0104h]

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object an unknown LOAD_EVENT
    // IN BC AFFE 1001 6F 5B D7 02 05 10 01 05 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=05 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘25h’ to address 0104h]

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 5B D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: START LOADING
 *
 * @ingroup KNX_08_TSSG_02_05_02
 */
TEST(Load_State_Machines_Tests, Test_2_5_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 53 D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – significance of bytes see 2.3.1 ]

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 53 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is in state ERROR

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 57 D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: LOAD COMPLETED
 *
 * @ingroup KNX_08_TSSG_02_05_03
 */
TEST(Load_State_Machines_Tests, Test_2_5_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 53 D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – significance of bytes see 2.3.1 ]

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 53 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is in state ERROR

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 57 D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT remains in load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: LOAD_SEGMENT
 *
 * @ingroup KNX_08_TSSG_02_05_04
 */
TEST(Load_State_Machines_Tests, Test_2_5_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 53 D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – significance of bytes see 2.3.1 ]

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 53 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is in state ERROR

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 57 D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – significance of bytes see 2.3.1 ]

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: UNLOAD
 *
 * @ingroup KNX_08_TSSG_02_05_05
 */
TEST(Load_State_Machines_Tests, Test_2_5_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 53 D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – significance of bytes see 2.3.1 ]

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 53 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is in state ERROR

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 57 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns load state LOAD_STATE_UNLOADED 3
    // OUT BC 1001 AFFE 66 57 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: DEVICE RESTART
 *
 * @ingroup KNX_08_TSSG_02_05_06
 */
TEST(Load_State_Machines_Tests, Test_2_5_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Assoc table)

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorization with highest key to access load state machines
    // IN BC AFFE 1001 66 43 D1 00 AA AA AA AA :AuthorizeRequest(AAAAAAAA)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x66, 0x43, 0xD1, 0x00, 0xAA, 0xAA, 0xAA, 0xAA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Authorize response for level 0 is returned
    // OUT BC 1001 AFFE 62 43 D2 00 :AuthorizeResponse(00)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x62, 0x43, 0xD2, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 02 05 10 01 00 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 00h]

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_START LOADING
    // IN BC AFFE 1001 6F 4B D7 02 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘21h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 02 05 10 01 02 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 02h]

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_EVENT_LOAD COMPLETED
    // IN BC AFFE 1001 6F 4F D7 02 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘22h’ to address 0104h]

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 4F D6 02 05 10 01 01 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 01h]

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to association table object a LOAD_SEGMENT (Segment control record)
    // IN BC AFFE 1001 6F 53 D7 02 05 10 01 03 02 40 30 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=03 02 40 30 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x03, 0x02, 0x40, 0x30, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write ’23 02 00 42 00 80 00 02 A0 4A 10h’ to address 0104h – significance of bytes see 2.3.1 ]

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 53 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Now test object is in state ERROR

    // Send a device restart to BDUT
    // IN BC AFFE 1001 61 57 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFe, 0x10, 0x01, 0x61, 0x57, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // T-ACK is optional. It is depending on the device architecture.

    // ---> Acceptance: Connection breaks down, load state changes to ERROR
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read load state of association table
    // IN BC AFFE 1001 65 43 D5 02 05 10 01 :PropertyValueRead(Obj=02, Prop=05, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x02, 0x05, 0x10, 0x01, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_ERROR
    // OUT BC 1001 AFFE 66 43 D6 02 05 10 01 03 :PropertyValueResponse(Obj=02, Prop=05, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x02, 0x05, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response 03h]

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 2.6 Test without access rights */

/**
 * Test without access rights
 *
 * @ingroup KNX_08_TSSG_02_06
 */
TEST(Load_State_Machines_Tests, Test_2_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Connect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // No authorization!

    // Send to association table object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 43 D7 02 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=02, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x02, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Write: write first byte ‘24h’ to address 0104h]

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT denies access to load state machine
    // OUT BC 1001 AFFE 65 43 D6 02 05 00 01 :PropertyValueResponse(Obj=02, Prop=05, Count=0, Start=001, Data=)
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x65, 0x43, 0xD6, 0x02, 0x05, 0x00, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // [with Memory Read: read one byte from address B6EBh – Memory Response --]

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
