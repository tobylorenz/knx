// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

class KNXnet_IP_Tests : public ::testing::Test
{
    virtual ~KNXnet_IP_Tests() = default;
};

// @todo KNXnet_IP_Tests

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
