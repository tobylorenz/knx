// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/09/04/01/TP1_BCU2.h>

class Run_State_Machine_Tests : public ::testing::Test
{
    virtual ~Run_State_Machine_Tests() = default;
};

/** BCU that can be used as Bus Device Under Test (BDUT) */
class BCU :
    public KNX::TP1_BCU2
{
public:
    explicit BCU(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        KNX::TP1_BCU2(io_context, tp1_bus_simulation) {
        interface_objects->application_program()->run_control()->description.access.write_level = 15; // minimum level

        A_Restart_ind_initiator();

        authorization_keys[0] = { 0xAA, 0xAA, 0xAA, 0xAA };
    }

protected:
    void A_Restart_ind_initiator() {
        application_layer.A_Restart_ind([this](const KNX::Restart_Erase_Code /*erase_code*/, const KNX::Restart_Channel_Number /*channel_number*/, const KNX::Priority /*priority*/, const KNX::Hop_Count_Type /*hop_count_type*/, const KNX::ASAP_Individual /*asap*/) ->void {
            interface_objects->application_program()->load_state_machine->device_restart();
            interface_objects->application_program()->run_state_machine->device_restart();

            A_Restart_ind_initiator();
        });
    }
};

/** Bus Coupling Unit (BCU) in TP1 Data Link Layer Plain/Busmonitor Mode */
class BCU_Plain_Data
{
public:
    explicit BCU_Plain_Data(asio::io_context & io_context, KNX::TP1_Bus_Simulation & tp1_bus_simulation) :
        group_address_table(std::make_shared<KNX::Group_Address_Table>()),
        tp1_physical_layer(io_context),
        tp1_data_link_layer(tp1_physical_layer, io_context)
    {
        tp1_physical_layer.connect(tp1_bus_simulation);

        tp1_data_link_layer.group_address_table = group_address_table;
        tp1_data_link_layer.L_Busmon_ind = std::bind(&BCU_Plain_Data::L_Busmon_ind, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);
    }

    /** group address table */
    std::shared_ptr<KNX::Group_Address_Table> group_address_table;

    /** TP1 physical layer simulation */
    KNX::TP1_Physical_Layer_Simulation tp1_physical_layer;

    /** TP1 data link layer */
    KNX::TP1_Data_Link_Layer tp1_data_link_layer;

    /** TP1 log */
    std::deque<std::vector<uint8_t>> log{};

protected:
    void L_Busmon_ind(const KNX::Status /*l_status*/, const uint16_t /*time_stamp*/, const std::vector<uint8_t> data) {
        log.push_back(data);
    }
};

/* 2 Testing of Run State Machines */

/* 2.1 Test Preparation */

/* 2.2 Tests with initial state RUNSTATE_HALTED */

/**
 * Event: Invalid RUNCONTROL and RUNCONTROL_NO_OPERATION
 *
 * @ingroup KNX_08_TSSI_02_02_01
 */
TEST(Run_State_Machine_Tests, Test_2_2_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Application)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Unloaded;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 43 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 43 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 47 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x47, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // First check if BDUT ignores invalid Run state event
    // IN BC 1041 1001 6F 4B D7 03 06 10 01 FF 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=FF 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x06, 0x10, 0x01, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 4B D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Now send to run state object a RUNCONTROL_NO_OPERATION
    // IN BC 1041 1001 6F 4F D7 03 06 10 01 00 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=00 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 4F D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: RUNCONTROL_RESTART and executable part loaded
 *
 * @ingroup KNX_08_TSSI_02_02_02
 */
TEST(Run_State_Machine_Tests, Test_2_2_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Set device to state ‘halted’ with executable part loaded as done in clause 2.3.5.
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Halted;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_RESTART
    // IN BC 1041 1001 6F 43 D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: RUNCONTROL_RESTART and executable part unloaded
 *
 * @ingroup KNX_08_TSSI_02_02_03
 */
TEST(Run_State_Machine_Tests, Test_2_2_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Application)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Unloaded;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_RESTART
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: RUNCONTROL_STOP
 *
 * @ingroup KNX_08_TSSI_02_02_04
 */
TEST(Run_State_Machine_Tests, Test_2_2_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Application)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Unloaded;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Unload to corresponding load state
 *
 * @ingroup KNX_08_TSSI_02_02_05
 */
TEST(Run_State_Machine_Tests, Test_2_2_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Load application object (executable part)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_RESTART
    // IN BC 1041 1001 6F 43 D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 47 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x47, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 4B D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 4B D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 4F D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x4F, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 4F D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Device restart and executable part loaded (Power Up)
 *
 * @ingroup KNX_08_TSSI_02_02_06
 */
TEST(Run_State_Machine_Tests, Test_2_2_6)
{
    // Not applicable as device is in run state „halted", which is for a loaded application not possible.
}

/**
 * Event: Device restart and executable part not loaded (Power Up)
 *
 * @ingroup KNX_08_TSSI_02_02_07
 */
TEST(Run_State_Machine_Tests, Test_2_2_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Application)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Unloaded;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 47 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x47, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Connection breaks down, run state is RUNSTATE_HALTED
    // @note T-Ack added
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/* 2.3 Tests with initial state RUNSTATE_RUNNING */

/**
 * Preparation
 *
 * @ingroup KNX_08_TSSI_02_03_01
 */
TEST(Run_State_Machine_Tests, Test_2_3_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Load application object (executable part)

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 43 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 43 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_START
    // IN BC AFFE 1001 6F 47 D7 03 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 47 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4B D7 03 05 10 01 03 00 07 00 00 F8 F1 02 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 07 00 00 F8 F1 02 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x07, 0x00, 0x00, 0xF8, 0xF1, 0x02, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4F D7 03 05 10 01 03 00 40 A4 01 5C F1 03 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 40 A4 01 5C F1 03 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0xA4, 0x01, 0x5C, 0xF1, 0x03, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 53 D7 03 05 10 01 03 00 42 00 01 00 22 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 42 00 01 00 22 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x42, 0x00, 0x01, 0x00, 0x22, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 53 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 57 D7 03 05 10 01 03 00 43 00 01 00 33 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 43 00 01 00 33 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x43, 0x00, 0x01, 0x00, 0x33, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5B D7 03 05 10 01 03 00 44 00 72 00 FF 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 44 00 72 00 FF 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x44, 0x00, 0x72, 0x00, 0xFF, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5F D7 03 05 10 01 03 02 41 34 00 00 C5 FF 12 11 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 02 41 34 00 00 C5 FF 12 11 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x02, 0x41, 0x34, 0x00, 0x00, 0xC5, 0xFF, 0x12, 0x11, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 63 D7 03 05 10 01 03 04 40 B7 03 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 04 40 B7 03 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x04, 0x40, 0xB7, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 63 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x63, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_COMPLETE
    // IN BC AFFE 1001 6F 67 D7 03 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x67, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 67 D6 03 05 10 01 01 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x67, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_RESTART
    // IN BC 1041 1001 6F 6B D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x6B, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 EA :T-Ack(Seq=A)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xEA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 6B D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x6B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 EA :T-Ack(Seq=A)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xEA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Invalid RUNCONTROL and RUNCONTROL_NO_OPERATION
 *
 * @ingroup KNX_08_TSSI_02_03_02
 */
TEST(Run_State_Machine_Tests, Test_2_3_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Precondition: The executable part is already loaded (see clause 2.3.1)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Preparation: Set run state to RUNSTATE_RUNNING
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Running;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // First check if BDUT ignores invalid Run State event
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 FF 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=FF 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Now send to run state object a RUNCONTROL_NO_OPERATION
    // IN BC 1041 1001 6F 4B D7 03 06 10 01 00 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=00 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 4B D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: RUNCONTROL_RESTART (executable part loaded)
 *
 * @ingroup KNX_08_TSSI_02_03_03
 */
TEST(Run_State_Machine_Tests, Test_2_3_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Precondition: The executable part is already loaded (see clause 2.3.1)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Preparation: Set run state to RUNSTATE_RUNNING
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Running;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_RESTART
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_RUNNING. It may intermediately return RUNSTATE_READY (this is marked in italics and is thus optional).
    //    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    //    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x02, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    //    // In case of an intermediate RUNSTATE_Ready, observe waiting period to ensure that application has started.

    //    // Read run state
    //    // IN BC 1041 1001 65 47 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    //    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x47, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    //    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    //    io_context.poll();
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    //    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    //    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: RUNCONTROL_RESTART and executable part unloaded
 *
 * @ingroup KNX_08_TSSI_02_03_04
 */
TEST(Run_State_Machine_Tests, Test_2_3_4)
{
    // Not applicable, the runstate „running" is not possible for an unloaded application.
}

/**
 * Event: RUNCONTROL_STOP
 *
 * @ingroup KNX_08_TSSI_02_03_05
 */
TEST(Run_State_Machine_Tests, Test_2_3_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Precondition: The executable part is already loaded (see clause 2.3.1)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Running;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_TERMINATED, optional HALTED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // (optional OUT BC 1001 AFFE 66 47 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00)
    //    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Unload corresponding load state
 *
 * @ingroup KNX_08_TSSI_02_03_06
 */
TEST(Run_State_Machine_Tests, Test_2_3_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Precondition: The executable part is already loaded (done in 2.)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Preparation: Set run state to RUNSTATE_RUNNING
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Running;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_RESTART
    // IN BC 1041 1001 6F 43 D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_RUNNING (it may optionally return the intermediate state ‘ready’).
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 47 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x47, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 4B D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 4B D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 4F D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x4F, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED, alternatively the intermediate step // ‘Shutting Down (05h)’
    // OUT BC 1001 AFFE 66 4F D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Device restart and executable part loaded (Power Up)
 *
 * @ingroup KNX_08_TSSI_02_03_07
 */
TEST(Run_State_Machine_Tests, Test_2_3_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Load application object (executable part)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 43 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 43 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_START
    // IN BC AFFE 1001 6F 47 D7 03 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 47 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // -------------------------------------------------------

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4B D7 03 05 10 01 03 00 07 00 00 F8 F1 02 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 07 00 00 F8 F1 02 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x07, 0x00, 0x00, 0xF8, 0xF1, 0x02, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4F D7 03 05 10 01 03 00 40 A4 01 5C F1 03 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 40 A4 01 5C F1 03 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0xA4, 0x01, 0x5C, 0xF1, 0x03, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 53 D7 03 05 10 01 03 00 42 00 01 00 22 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 42 00 01 00 22 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x42, 0x00, 0x01, 0x00, 0x22, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 53 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 57 D7 03 05 10 01 03 00 43 00 01 00 33 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 43 00 01 00 33 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x43, 0x00, 0x01, 0x00, 0x33, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5B D7 03 05 10 01 03 00 44 00 72 00 FF 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 44 00 72 00 FF 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x44, 0x00, 0x72, 0x00, 0xFF, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5F D7 03 05 10 01 03 02 41 34 00 00 C5 FF 12 11 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 02 41 34 00 00 C5 FF 12 11 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x02, 0x41, 0x34, 0x00, 0x00, 0xC5, 0xFF, 0x12, 0x11, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 63 D7 03 05 10 01 03 04 40 B7 03 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 04 40 B7 03 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x04, 0x40, 0xB7, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 63 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x63, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_COMPLETE
    // IN BC AFFE 1001 6F 67 D7 03 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x67, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 67 D6 03 05 10 01 01 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x67, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a RUNSTATE_RESTART
    // IN BC 1041 1001 6F 6B D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x6B, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 1001 AFFE 60 EA :T-Ack(Seq=A)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xEA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @note PropertyValueResponse added
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x6B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @note T-Ack added
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xEA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // -------------------------------------------------

    // @todo std::deque corrupts on the next lines, so we just clear it here. Weird.
    busmoni.log.clear();
    log = std::cbegin(busmoni.log);

    // Actual start of test

    // Preparation: Do reset of device

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 43 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: Connection breaks down, run state may intermediately return RUNSTATE_HALTED
    // or RUNSTATE_READY or may immediately return the run state RUNSTATE_RUNNING. The test
    // sequence in italics is thus optional.
    // @note T-Ack added
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    //    // BDUT may return the intermediate run state RUNSTATE_HALTED and/or RUNSTATE_READY
    //    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    //    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    //    // And/or

    //    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    //    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x02, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    // @note ... but in my case immediately returns RUNSTATE_RUNNING
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // In case of an intermediate RUNSTATE_HALTED and/or RUNSTATE_READY, observe waiting period to
    // ensure that application has started.

    // Read run state
    // IN BC 1041 1001 65 47 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x47, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Device restart and executable part unloaded (Power Up)
 *
 * @ingroup KNX_08_TSSI_02_03_08
 */
TEST(Run_State_Machine_Tests, Test_2_3_8)
{
    // Not applicable, marked as such in AN080.
}

/* 2.4 Tests with initial state RUNSTATE_READY */

/* 2.4.1 General */

/**
 * Event: Invalid RUNCONTROL and RUNCONTROL_NO_OPERATION
 *
 * @ingroup KNX_08_TSSI_02_04_02
 */
TEST(Run_State_Machine_Tests, Test_2_4_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Precondition: The executable part is already loaded
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Preparation: Set run state to RUNSTATE_READY
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Ready;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 43 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: Connection breaks down, run state is RUNSTATE_READY
    // @note T-Ack added
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=01.00.001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_READY (intermediate state)
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // First check if BDUT ignores invalid run state event

    // IN BC 1041 1001 6F 47 D7 03 06 10 01 FF 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=FF 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_READY
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Now send to run state object a RUNCONTROL_NO_OPERATION
    // IN BC 1041 1001 6F 4B D7 03 06 10 01 00 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=00 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_READY
    // OUT BC 1001 AFFE 66 4B D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Restart and executable part loaded
 *
 * @ingroup KNX_08_TSSI_02_04_03
 */
TEST(Run_State_Machine_Tests, Test_2_4_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Precondition: The executable part is already loaded (done in 2.)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Preparation: Set run state to RUNSTATE_READY
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Ready;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 43 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: Connection breaks down, run state is RUNSTATE_READY
    // @note T-Ack added
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_READY (intermediate state)
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_RESTART
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    //    // BDUT returns run state RUNSTATE_READY (intermediate state)
    //    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    //    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x02, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    // @note ... but in my case immediately returns RUNSTATE_RUNNING
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Here we wait a few seconds to be sure that the application has started.

    // Read run state
    // IN BC 1041 1001 65 4B D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x4B, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 4B D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Restart and executable part unloaded
 *
 * @ingroup KNX_08_TSSI_02_04_04
 */
TEST(Run_State_Machine_Tests, Test_2_4_4)
{
    // Not applicable (if application is unloaded and a restart is sent to the BDUT, the BDUT can acc. AN 080
    // never be set to the ready state. The case where the initial state is ready for an unloaded application can
    // never occur).
}

/**
 * Event: Stop run state machine
 *
 * @ingroup KNX_08_TSSI_02_04_05
 */
TEST(Run_State_Machine_Tests, Test_2_4_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Set run state to RUNSTATE_READY
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Ready;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 43 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: Connection breaks down, run state is RUNSTATE_READY
    // @note T-Ack added
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_READY (intermediate state)
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Unload to corresponding load state
 *
 * @ingroup KNX_08_TSSI_02_04_06
 */
TEST(Run_State_Machine_Tests, Test_2_4_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Set run state to RUNSTATE_READY
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Ready;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 43 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: Connection breaks down, run state is RUNSTATE_READY
    // @note T-Ack added
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_READY (intermediate state)
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 4B D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x4B, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 4B D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Device restart and executable part loaded (Power Up)
 *
 * @ingroup KNX_08_TSSI_02_04_07
 */
TEST(Run_State_Machine_Tests, Test_2_4_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Precondition: The executable part is already loaded
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Preparation: Load application object (executable part)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 43 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 43 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_START
    // IN BC AFFE 1001 6F 47 D7 03 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 47 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4B D7 03 05 10 01 03 00 07 00 00 F8 F1 02 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 07 00 00 F8 F1 02 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x07, 0x00, 0x00, 0xF8, 0xF1, 0x02, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4F D7 03 05 10 01 03 00 40 A4 01 5C F1 03 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 40 A4 01 5C F1 03 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0xA4, 0x01, 0x5C, 0xF1, 0x03, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 53 D7 03 05 10 01 03 00 42 00 01 00 22 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 42 00 01 00 22 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x42, 0x00, 0x01, 0x00, 0x22, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 53 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 57 D7 03 05 10 01 03 00 43 00 01 00 33 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 43 00 01 00 33 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x43, 0x00, 0x01, 0x00, 0x33, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5B D7 03 05 10 01 03 00 44 00 72 00 FF 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 44 00 72 00 FF 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x44, 0x00, 0x72, 0x00, 0xFF, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5F D7 03 05 10 01 03 02 41 34 00 00 C5 FF 12 11 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 02 41 34 00 00 C5 FF 12 11 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x02, 0x41, 0x34, 0x00, 0x00, 0xC5, 0xFF, 0x12, 0x11, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 63 D7 03 05 10 01 03 04 40 B7 03 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 04 40 B7 03 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x04, 0x40, 0xB7, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 63 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x63, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_COMPLETE
    // IN BC AFFE 1001 6F 67 D7 03 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x67, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 67 D6 03 05 10 01 01 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x67, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a RUNSTATE_RESTART
    // IN BC 1041 1001 6F 6B D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x6B, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 1001 AFFE 60 EA :T-Ack(Seq=A)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xEA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @note PropertyValueResponse added
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x6B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @note T-Ack added
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xEA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Start of actual test: Set run state to RUNSTATE_READY
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 43 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x43, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: Connection breaks down, run state is RUNSTATE_READY
    // @note T-Ack added
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @todo std::deque corrupts on the next lines, so we just clear it here. Weird.
    busmoni.log.clear();
    log = std::cbegin(busmoni.log);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_READY
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 47 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x47, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: Connection breaks down, run state is RUNSTATE_READY
    // @note T-Ack added
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_READY (intermediate state)
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 }; // @note States: Halted->Ready->Running
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Observe a short wait time to ensure that the application has started.

    // Read run state
    // IN BC 1041 1001 65 47 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x47, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Device restart and executable part unloaded (Power Up)
 *
 * @ingroup KNX_08_TSSI_02_04_08
 */
TEST(Run_State_Machine_Tests, Test_2_4_8)
{
    // Not applicable, marked as such in AN080.
}

/* 2.5 Tests with initial state RUNSTATE_TERMINATED */

/**
 * Event: Invalid RUNCONTROL and RUNCONTROL_NO_OPERATION
 *
 * @ingroup KNX_08_TSSI_02_05_01
 */
TEST(Run_State_Machine_Tests, Test_2_5_1)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Precondition: The executable part is already loaded
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Preparation: Set run state to RUNSTATE_TERMINATED
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Terminated;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 43 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // First check if BDUT ignores invalid run state event
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 FF 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=FF 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Now send to run state object a RUNCONTROL_NO_OPERATION
    // IN BC 1041 1001 6F 4B D7 03 06 10 01 00 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=00 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 4B D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: RUNCONTROL_RESTART ( executable part loaded)
 *
 * @ingroup KNX_08_TSSI_02_05_02
 */
TEST(Run_State_Machine_Tests, Test_2_5_2)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Precondition: The executable part is already loaded (done)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Preparation: Set run state to RUNSTATE_TERMINATED
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Terminated;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 43 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_RESTART
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_RUNNING (it may return the intermediate state ‘ready’).
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: RUNCONTROL_RESTART (executable part unloaded)
 *
 * @ingroup KNX_08_TSSI_02_05_03
 */
TEST(Run_State_Machine_Tests, Test_2_5_3)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Set run state to RUNSTATE_TERMINATED
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Terminated;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 43 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 43 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_RESTART
    // IN BC 1041 1001 6F 4B D7 03 06 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 4B D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: RUNCONTROL_STOP
 *
 * @ingroup KNX_08_TSSI_02_05_04
 */
TEST(Run_State_Machine_Tests, Test_2_5_4)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Set run state to RUNSTATE_TERMINATED
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Terminated;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 43 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Unload to corresponding load state
 *
 * @ingroup KNX_08_TSSI_02_05_05
 */
TEST(Run_State_Machine_Tests, Test_2_5_5)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Set run state to RUNSTATE_TERMINATED
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Terminated;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 43 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 47 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 47 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 4B D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x4B, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 4B D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Device restart and executable part loaded (Power Up)
 *
 * @ingroup KNX_08_TSSI_02_05_06
 */
TEST(Run_State_Machine_Tests, Test_2_5_6)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Load application object (executable part)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 43 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 43 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_START
    // IN BC AFFE 1001 6F 47 D7 03 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 47 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // -----------------------------------------------

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4B D7 03 05 10 01 03 00 07 00 00 F8 F1 02 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 07 00 00 F8 F1 02 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x07, 0x00, 0x00, 0xF8, 0xF1, 0x02, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4F D7 03 05 10 01 03 00 40 A4 01 5C F1 03 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 40 A4 01 5C F1 03 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0xA4, 0x01, 0x5C, 0xF1, 0x03, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 53 D7 03 05 10 01 03 00 42 00 01 00 22 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 42 00 01 00 22 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x42, 0x00, 0x01, 0x00, 0x22, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 53 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 57 D7 03 05 10 01 03 00 43 00 01 00 33 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 43 00 01 00 33 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x43, 0x00, 0x01, 0x00, 0x33, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5B D7 03 05 10 01 03 00 44 00 72 00 FF 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 44 00 72 00 FF 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x44, 0x00, 0x72, 0x00, 0xFF, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5F D7 03 05 10 01 03 02 41 34 00 00 C5 FF 12 11 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 02 41 34 00 00 C5 FF 12 11 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x02, 0x41, 0x34, 0x00, 0x00, 0xC5, 0xFF, 0x12, 0x11, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 63 D7 03 05 10 01 03 04 40 B7 03 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 04 40 B7 03 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x04, 0x40, 0xB7, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 63 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x63, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_COMPLETE
    // IN BC AFFE 1001 6F 67 D7 03 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x67, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 67 D6 03 05 10 01 01 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x67, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ----------------------------------------------------

    // Start of actual test

    // Precondition: The executable part is already loaded
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Loaded;

    // Preparation: Set run state to RUNSTATE_TERMINATED
    bdut.interface_objects->application_program()->run_state_machine->state = KNX::Run_State_Machine::State::Terminated;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 43 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 47 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x47, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: Connection breaks down, run state may intermediately return RUNSTATE_HALTED
    // and/or RUNSTATE_READY or may immediately return the run state RUNSTATE_RUNNING. The test
    // sequence in italics is thus optional.
    // @note T-Ack added
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @todo std::deque corrupts on the next lines, so we just clear it here. Weird.
    busmoni.log.clear();
    log = std::cbegin(busmoni.log);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    //    // BDUT returns run state RUNSTATE_HALTED and/or RUNSTATE_READY (intermediate state)
    //    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    //    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    //    // And/or

    //    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 02 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=02 )
    //    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x02, 0 };
    //    ASSERT_EQ(*log++, tp1_data);
    //    tp1_data = { 0xCC }; // L_Ack
    //    ASSERT_EQ(*log++, tp1_data);

    // @note ... but in my case immediately returns RUNSTATE_RUNNING
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // In case of an intermediate RUNSTATE_HALTED and/or RUNSTATE_Ready, observe a wait time to
    // ensure that the application has started.

    // Read run state
    // IN BC 1041 1001 65 47 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x47, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_RUNNING
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 01 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

/**
 * Event: Device restart and executable part not loaded (Power Up)
 *
 * @ingroup KNX_08_TSSI_02_05_07
 */
TEST(Run_State_Machine_Tests, Test_2_5_7)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    /* TP1 bus */
    KNX::TP1_Bus_Simulation tp1_bus(io_context);
    std::vector<uint8_t> tp1_data;

    /* local device under test (DUT) */
    BCU bdut(io_context, tp1_bus);
    bdut.interface_objects->group_address_table()->individual_address = KNX::Individual_Address(0x1001);

    /* remote BCU */
    BCU_Plain_Data bcu(io_context, tp1_bus);
    bcu.group_address_table->individual_address = KNX::Individual_Address(0xAFFE);

    /* BCU to observe the bus communication */
    BCU_Plain_Data busmoni(io_context, tp1_bus);
    busmoni.tp1_data_link_layer.mode = KNX::Data_Link_Layer::Mode::Busmonitor;
    std::deque<std::vector<uint8_t>>::const_iterator log = std::cbegin(busmoni.log);

    // Preparation: Unload test object (Application)
    bdut.interface_objects->application_program()->load_state_machine->state = KNX::Load_State_Machine::State::Unloaded;

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 43 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 43 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to run state object a RUNCONTROL_STOP
    // IN BC 1041 1001 6F 47 D7 03 06 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=06, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x06, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns run state RUNSTATE_TERMINATED
    // OUT BC 1001 AFFE 66 47 D6 03 06 10 01 03 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=03 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x03, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send a reset to BDUT
    // IN BC AFFE 1001 61 4B 80 :Restart()
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x61, 0x4B, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
    // T-ACK is optional. It is depending on the device architecture.

    // ---> Acceptance: Connection breaks down, load state remains UNLOADED
    // @note Simulate a reset by disconnect
    // IN B0 AFFE 1001 60 81 :T-Disconnect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Reconnect to BDUT
    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Read run state
    // IN BC 1041 1001 65 43 D5 03 06 10 01 :PropertyValueRead(Obj=03, Prop=06, Count=1, Start=001)
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x65, 0x43, 0xD5, 0x03, 0x06, 0x10, 0x01, 0 }; // @note Src changed from 0x1041 to 0xAFFE
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // ---> Acceptance: BDUT returns run state RUNSTATE_HALTED
    // OUT BC 1001 AFFE 66 43 D6 03 06 10 01 00 :PropertyValueResponse(Obj=03, Prop=06, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x06, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Restore device (Load application object)

    // IN B0 AFFE 1001 60 80 :T-Connect(Addr=1001)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x80, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_UNLOAD
    // IN BC AFFE 1001 6F 43 D7 03 05 10 01 04 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=04 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x43, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_UNLOADED
    // OUT BC 1001 AFFE 66 43 D6 03 05 10 01 00 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=00 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x43, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x00, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C2 :T-Ack(Seq=0)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_START
    // IN BC AFFE 1001 6F 47 D7 03 05 10 01 01 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=01 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x47, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xC6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 47 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x47, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 C6 :T-Ack(Seq=1)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xC6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4B D7 03 05 10 01 03 00 07 00 00 F8 F1 02 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 07 00 00 F8 F1 02 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x07, 0x00, 0x00, 0xF8, 0xF1, 0x02, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CA :T-Ack(Seq=2)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 4F D7 03 05 10 01 03 00 40 A4 01 5C F1 03 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 40 A4 01 5C F1 03 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x4F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x40, 0xA4, 0x01, 0x5C, 0xF1, 0x03, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xCE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 4F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x4F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 CE :T-Ack(Seq=3)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xCE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 53 D7 03 05 10 01 03 00 42 00 01 00 22 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 42 00 01 00 22 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x53, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x42, 0x00, 0x01, 0x00, 0x22, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 53 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x53, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D2 :T-Ack(Seq=4)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 57 D7 03 05 10 01 03 00 43 00 01 00 33 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 43 00 01 00 33 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x57, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x43, 0x00, 0x01, 0x00, 0x33, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xD6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 57 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x57, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 D6 :T-Ack(Seq=5)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xD6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5B D7 03 05 10 01 03 00 44 00 72 00 FF 03 80 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 00 44 00 72 00 FF 03 80 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5B, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x00, 0x44, 0x00, 0x72, 0x00, 0xFF, 0x03, 0x80, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDA, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5B D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5B, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DA :T-Ack(Seq=6)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDA, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 5F D7 03 05 10 01 03 02 41 34 00 00 C5 FF 12 11 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 02 41 34 00 00 C5 FF 12 11 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x5F, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x02, 0x41, 0x34, 0x00, 0x00, 0xC5, 0xFF, 0x12, 0x11, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xDE, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 5F D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x5F, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 DE :T-Ack(Seq=7)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xDE, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // @todo std::deque corrupts on the next lines, so we just clear it here. Weird.
    busmoni.log.clear();
    log = std::cbegin(busmoni.log);

    // Send to application program object a LOAD_EVENT_SEGMENT
    // IN BC AFFE 1001 6F 63 D7 03 05 10 01 03 04 40 B7 03 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=03 04 40 B7 03 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x63, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x03, 0x04, 0x40, 0xB7, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE2, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADING
    // OUT BC 1001 AFFE 66 63 D6 03 05 10 01 02 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=02 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x63, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x02, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E2 :T-Ack(Seq=8)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE2, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Send to application program object a LOAD_EVENT_COMPLETE
    // IN BC AFFE 1001 6F 67 D7 03 05 10 01 02 00 00 00 00 00 00 00 00 00 :PropertyValueWrite(Obj=03, Prop=05, Count=1, Start=001, Data=02 00 00 00 00 00 00 00 00 00 )
    tp1_data = { 0xBC, 0xAF, 0xFE, 0x10, 0x01, 0x6F, 0x67, 0xD7, 0x03, 0x05, 0x10, 0x01, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // OUT B0 1001 AFFE 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0x10, 0x01, 0xAF, 0xFE, 0x60, 0xE6, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // BDUT returns load state LOAD_STATE_LOADED
    // OUT BC 1001 AFFE 66 67 D6 03 05 10 01 01 :PropertyValueResponse(Obj=03, Prop=05, Count=1, Start=001, Data=01 )
    tp1_data = { 0xBC, 0x10, 0x01, 0xAF, 0xFE, 0x66, 0x67, 0xD6, 0x03, 0x05, 0x10, 0x01, 0x01, 0 };
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // IN B0 AFFE 1001 60 E6 :T-Ack(Seq=9)
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0xE6, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);

    // Close connection with BDUT
    // IN B0 AFFE 1001 60 81 :T-Disconnect
    tp1_data = { 0xB0, 0xAF, 0xFE, 0x10, 0x01, 0x60, 0x81, 0 };
    bcu.tp1_data_link_layer.L_Plain_Data_req(0, tp1_data);
    io_context.poll();
    ASSERT_EQ(*log++, tp1_data);
    tp1_data = { 0xCC }; // L_Ack
    ASSERT_EQ(*log++, tp1_data);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
