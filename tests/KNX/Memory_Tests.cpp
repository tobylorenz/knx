// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/Memory.h>

class Memory_Tests : public ::testing::Test
{
    virtual ~Memory_Tests() = default;

};

TEST(Memory_Tests, Tests)
{
    KNX::Memory memory;

    // observer 1+6
    memory.updated.push_back([&memory](const KNX::Memory::Address address, const KNX::Memory::Size size) {
        const KNX::Memory::Address observed_address = 1;
        const KNX::Memory::Size observed_size = 2;
        if ((address <= observed_address + observed_size) && (observed_address < address + size)) {
            std::vector<uint8_t> data = memory.get(observed_address, observed_size);
        }
    });

    // observer 6+7
    memory.updated.push_back([&memory](const KNX::Memory::Address address, const KNX::Memory::Size size) {
        const KNX::Memory::Address observed_address = 6;
        const KNX::Memory::Size observed_size = 7;
        if ((address <= observed_address + observed_size) && (observed_address < address + size)) {
            std::vector<uint8_t> data = memory.get(observed_address, observed_size);
        }
    });

    std::vector<uint8_t> expected_data;
    std::vector<uint8_t> data;

    memory.set(0, {0, 1, 2, 3});
    ASSERT_EQ(memory.memory_data.size(), 1);
    ASSERT_EQ(memory.memory_data[0].size(), 4);
    expected_data = {0, 1, 2, 3};
    ASSERT_EQ(memory.memory_data[0], expected_data);

    memory.set(5, {5, 6, 7, 8});
    ASSERT_EQ(memory.memory_data.size(), 2);
    ASSERT_EQ(memory.memory_data[5].size(), 4);
    expected_data = {5, 6, 7, 8};
    ASSERT_EQ(memory.memory_data[5], expected_data);

    memory.set(4, {4});
    ASSERT_EQ(memory.memory_data.size(), 1);
    ASSERT_EQ(memory.memory_data[0].size(), 9);
    expected_data = {0, 1, 2, 3, 4, 5, 6, 7, 8};
    ASSERT_EQ(memory.memory_data[0], expected_data);

    data = memory.get(2, 10);
    ASSERT_EQ(data.size(), 7);
    expected_data = {2, 3, 4, 5, 6, 7, 8};
    ASSERT_EQ(data, expected_data);

    data = memory.get(12, 2);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
