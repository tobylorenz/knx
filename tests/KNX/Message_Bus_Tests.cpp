// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/Message_Bus.h>

class Message_Bus_Tests : public ::testing::Test
{
    virtual ~Message_Bus_Tests() = default;

};

class Layer
{
public:
    explicit Layer(asio::io_context & io_context) :
        con_bus(io_context),
        ind_bus(io_context)
    {
    }

    void async_req(int src, int dst, std::function<void(bool status)> con) {
        con_bus.async_wait([con](int /*src*/, int /*dst*/, bool con_s){
            con(con_s);
        }, [src, dst](int pred_src, int pred_dst, int /*status*/){
            return (pred_src == src) && (pred_dst == dst);
        });
    }

    void async_ind(std::function<void(int src, int dst)> ind) {
        ind_bus.async_wait(ind, ind_bus.true_predicate);
    }

    void trigger_con(int src, int dst, bool status) {
        (void) con_bus.notify(src, dst, status);
    }

    void trigger_ind(int src, int dst) {
        (void) ind_bus.notify(src, dst);
    }

private:
    KNX::Message_Bus<int, int, bool> con_bus;
    KNX::Message_Bus<int, int> ind_bus;
};

TEST(Message_Bus_Tests, Tests)
{
    asio::io_context io_context;
    asio::executor_work_guard<asio::io_context::executor_type> work_guard = make_work_guard(io_context);
    (void) work_guard.owns_work(); // Fix cppcheck warning: Variable 'work_guard' is assigned a value that is never used.

    Layer layer(io_context);
    int con_count{0};
    int ind_count{0};
    layer.async_req(1, 2, [&con_count](bool status) -> void {
        ASSERT_EQ(status, true);
        con_count++;
    });

    // test con with wrong src/dst
    layer.trigger_con(3, 4, false);
    io_context.poll();
    ASSERT_EQ(con_count, 0);

    // test con with correct src/dst
    layer.trigger_con(1, 2, true);
    io_context.poll();
    ASSERT_EQ(con_count, 1);

    // test again con with correct src/dst
    layer.trigger_con(1, 2, true);
    io_context.poll();
    ASSERT_EQ(con_count, 1);

    layer.async_ind([&ind_count](int /*src*/, int /*dst*/) -> void {
        ind_count++;
    });

    // test ind
    layer.trigger_ind(1, 2);
    io_context.poll();
    ASSERT_EQ(ind_count, 1);

    // test ind again
    layer.trigger_ind(3, 4);
    io_context.poll();
    ASSERT_EQ(ind_count, 1);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
