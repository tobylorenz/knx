// SPDX-FileCopyrightText: 2021 Tobias Lorenz <tobias.lorenz@gmx.net>
//
// SPDX-License-Identifier: GPL-3.0-or-later

#include "gtest/gtest.h"

#include <KNX/types.h>

class Types_Tests : public ::testing::Test
{
    virtual ~Types_Tests() = default;
};

TEST(Types, Operators)
{
    KNX::uint4_t number;

    number = 15;
    ++number;
    ASSERT_EQ(number, 0);

    number = 15;
    number++;
    ASSERT_EQ(number, 0);

    number = 0;
    --number;
    ASSERT_EQ(number, 15);

    number = 0;
    number--;
    ASSERT_EQ(number, 15);

    number = 0;
    number = number & 0x0f;
    ASSERT_EQ(number, 0);

    number = 15;
    number = number & 0x0f;
    ASSERT_EQ(number, 15);
}

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
